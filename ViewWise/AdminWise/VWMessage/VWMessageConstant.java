package ViewWise.AdminWise.VWMessage;

import ViewWise.AdminWise.AdminWise;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;
public interface VWMessageConstant
{
    String ERR_SEARCHNAME_ALREADYEXIST=AdminWise.connectorManager.getString("MessageConstant.ERR_SEARCHNAME_ALREADYEXIST");
    String ERR_INDEXNAME_ALREADYEXIST=AdminWise.connectorManager.getString("MessageConstant.ERR_INDEXNAME_ALREADYEXIST");
    String ERR_DOCTYPENAME_ALREADYEXIST=AdminWise.connectorManager.getString("MessageConstant.ERR_DOCTYPENAME_ALREADYEXIST");
    String ERR_DOCTYPE_NONAME=AdminWise.connectorManager.getString("MessageConstant.ERR_DOCTYPE_NONAME");
    String ERR_EMPTY_INDEX_VALUE=AdminWise.connectorManager.getString("MessageConstant.ERR_EMPTY_INDEX_VALUE");
    String ERR_INDEX_NONAME=AdminWise.connectorManager.getString("MessageConstant.ERR_INDEX_NONAME");
    String ERR_DATEMASK_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_DATEMASK_NOTFOUND");
    //Purpose: Additinal issue - 662-01		Created By : C.Shanmugavalli Date 26 Sep 2006
    String ERR_DEFAULT_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_DEFAULT_NOTFOUND");
    /**
     * ERR_DEFAULT_FIXED_DATE constant Added By: Vijaypriya.B.K Dated 30 Dec 2008
     */
    String ERR_DEFAULT_FIXED_DATE=AdminWise.connectorManager.getString("MessageConstant.ERR_DEFAULT_FIXED_DATE");
    String ERR_SELECTIONVALUES_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_SELECTIONVALUES_NOTFOUND");
    String ERR_SEQUENCEDIGIT_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_SEQUENCEDIGIT_NOTFOUND");
    String ERR_BOOLDEFAULT_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_BOOLDEFAULT_NOTFOUND");
    String ERR_DOCTYPEKEY_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_DOCTYPEKEY_NOTFOUND");
    String ERR_INDEXDEFAULT_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_INDEXDEFAULT_NOTFOUND");
    String ERR_CHECKISADMIN_FAIL=AdminWise.connectorManager.getString("MessageConstant.ERR_CHECKISADMIN_FAIL");
    String ERR_DOCTYPE_NOINDICES=AdminWise.connectorManager.getString("MessageConstant.ERR_DOCTYPE_NOINDICES");
    String ERR_NOINDICES=AdminWise.connectorManager.getString("MessageConstant.ERR_NOINDICES");
    String ERR_STORAGE_NONAME=AdminWise.connectorManager.getString("MessageConstant.ERR_STORAGE_NONAME");
    String ERR_STORAGE_ALREADYEXIST=AdminWise.connectorManager.getString("MessageConstant.ERR_STORAGE_ALREADYEXIST");
    String ERR_STORAGE_NOPATH=AdminWise.connectorManager.getString("MessageConstant.ERR_STORAGE_NOPATH");
    String ERR_STORAGE_PATHNOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_STORAGE_PATHNOTFOUND");
    String ERR_DSN_NONAME=AdminWise.connectorManager.getString("MessageConstant.ERR_DSN_NONAME");
    String ERR_DSN_NORESULT=AdminWise.connectorManager.getString("MessageConstant.ERR_DSN_NORESULT");
    String ERR_NUMBER_NODECIMALPLACES=AdminWise.connectorManager.getString("MessageConstant.ERR_NUMBER_NODECIMALPLACES");
    
    String ERR_EMPTY_SEARCHWITHOUTCONDS=AdminWise.connectorManager.getString("MessageConstant.ERR_EMPTY_SEARCHWITHOUTCONDS");
    String ERR_SEARCH_EMPTYNAME=AdminWise.connectorManager.getString("MessageConstant.ERR_SEARCH_EMPTYNAME");
    String ERR_SEARCH_NOTSAVE=AdminWise.connectorManager.getString("MessageConstant.ERR_SEARCH_NOTSAVE");
    String ERR_CHECKSECURITY_FAIL=AdminWise.connectorManager.getString("MessageConstant.ERR_CHECKSECURITY_FAIL");
    String ERR_NOSERVER_FOUND=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" " + AdminWise.connectorManager.getString("MessageConstant.ERR_NOSERVER_FOUND");
    String ERR_ROOM_DOWN=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" " + AdminWise.connectorManager.getString("MessageConstant.ERR_ROOM_DOWN");
    String ERR_ROOM_NOTCONNECT=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" " + AdminWise.connectorManager.getString("MessageConstant.ERR_ROOM_NOTCONNECT");
    String ERR_NOROOM_FOUND=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" " + AdminWise.connectorManager.getString("MessageConstant.ERR_NOROOM_FOUND");
    String MSG_SEARCH_SAVESUSCC=AdminWise.connectorManager.getString("MessageConstant.MSG_SEARCH_SAVESUSCC");
    String MSG_PROPERTIES_LOAD=AdminWise.connectorManager.getString("MessageConstant.MSG_PROPERTIES_LOAD");
    String MSG_AUTHINTICATE_LOAD=AdminWise.connectorManager.getString("MessageConstant.MSG_AUTHINTICATE_LOAD");
    String MSG_CHECKROOM_LOAD=AdminWise.connectorManager.getString("MessageConstant.MSG_CHECKROOM_LOAD0")+" "+ResourceManager.getDefaultManager().getString("CVProduct.Name")+" " +AdminWise.connectorManager.getString("MessageConstant.MSG_CHECKROOM_LOAD1");
    String MSG_CHECKSECURITY_SECC=AdminWise.connectorManager.getString("MessageConstant.MSG_CHECKSECURITY_SECC");
    String MSG_FIND_RUN=AdminWise.connectorManager.getString("MessageConstant.MSG_FIND_RUN");
    String ERR_FIND=AdminWise.connectorManager.getString("MessageConstant.ERR_FIND");
    String MSG_DONE=AdminWise.connectorManager.getString("MessageConstant.MSG_DONE");
    String MSG_SECURITY_MISSING=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" " + AdminWise.connectorManager.getString("MessageConstant.MSG_SECURITY_MISSING");
    String ERR_ROOMFILES_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_ROOMFILES_NOTFOUND");
    String ERR_PRIVILAGE_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_PRIVILAGE_NOTFOUND");
    String ERR_HELP_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_HELP_NOTFOUND");
    String ERR_CONFIRM_FAIL=AdminWise.connectorManager.getString("MessageConstant.ERR_CONFIRM_FAIL");
    String MSG_RESTORECOMPLETED=AdminWise.connectorManager.getString("MessageConstant.MSG_RESTORECOMPLETED");
    String ERR_SERVER_NONAME=AdminWise.connectorManager.getString("MessageConstant.ERR_SERVER_NONAME");
    String ERR_SERVER_NOHOST=AdminWise.connectorManager.getString("MessageConstant.ERR_SERVER_NOHOST");
    String ERR_SERVER_ALREADYEXIST=AdminWise.connectorManager.getString("MessageConstant.ERR_SERVER_ALREADYEXIST");   
    String ERR_ARCHIVE_NOTFOUND=AdminWise.connectorManager.getString("MessageConstant.ERR_ARCHIVE_NOTFOUND");
    String ERR_ARCHIVE_NOTWRITABLE=AdminWise.connectorManager.getString("MessageConstant.ERR_ARCHIVE_NOTWRITABLE");
    String MSG_ATDEL_1=AdminWise.connectorManager.getString("MessageConstant.MSG_ATDEL_1");
    String MSG_ATDEL_2=AdminWise.connectorManager.getString("MessageConstant.MSG_ATDEL_2");
    String MSG_ATDEL_3=AdminWise.connectorManager.getString("MessageConstant.MSG_ATDEL_3");
    String MSG_ATARCHIVE_SUCCESS=AdminWise.connectorManager.getString("MessageConstant.MSG_ATARCHIVE_SUCCESS");
    String MSG_ATARCHIVE_FAILED=AdminWise.connectorManager.getString("MessageConstant.MSG_ATARCHIVE_FAILED");
    String MSG_ATSETTING_CHANGED=AdminWise.connectorManager.getString("MessageConstant.MSG_ATSETTING_CHANGED");
    String MSG_RETENTION_SETTING_CHANGED=AdminWise.connectorManager.getString("MessageConstant.MSG_RETENTION_SETTING_CHANGED");
    String MSG_SELECT_LOCATION =AdminWise.connectorManager.getString("MessageConstant.MSG_SELECT_LOCATION") ;
    String MSG_SELECT_From_To =AdminWise.connectorManager.getString("MessageConstant.MSG_SELECT_From_To");
    String ERR_LOGIN_EXCEEDED=AdminWise.connectorManager.getString("MessageConstant.ERR_LOGIN_EXCEEDED");
    
    //DRS Error messages
    
    String ERR_SELECTROUTE_NAME=AdminWise.connectorManager.getString("MessageConstant.ERR_SELECTROUTE_NAME0")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" " +AdminWise.connectorManager.getString("MessageConstant.ERR_SELECTROUTE_NAME1");
    String ERR_ENTER_EMAILIDS_NAME=AdminWise.connectorManager.getString("MessageConstant.ERR_ENTER_EMAILIDS_NAME");
    String ERR_SELECTACTION_LOCATION=AdminWise.connectorManager.getString("MessageConstant.ERR_SELECTACTION_LOCATION");
    
    
    //End of DRS Error messages
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Error Message to displayed in restore failure.
*/
    
    String ERR_RESTORE_CANCELLED=AdminWise.connectorManager.getString("MessageConstant.ERR_RESTORE_CANCELLED");
    int CheckDocumentTypeErr = -711;
    String E711 = AdminWise.connectorManager.getString("MessageConstant.E711");
    int CancelRestoreProcess = -712;
    String E712 = AdminWise.connectorManager.getString("MessageConstant.E712");
    
    final static int BROWSER_TYPE=0;
    final static int DIALOG_TYPE=1;
    final static int CONSOLE_TYPE=2;
    final static int INIT_TYPE=3;
    
    //Restore
        String WRN_RESTOTE_1=AdminWise.connectorManager.getString("MessageConstant.WRN_RESTOTE_1");
        String WRN_RESTOTE_2=" "+AdminWise.connectorManager.getString("MessageConstant.WRN_RESTOTE_2");
        String ERR_LOCATIONERROR=AdminWise.connectorManager.getString("MessageConstant.ERR_LOCATIONERROR");
  //Signature
    String MSG_SAVESIG=AdminWise.connectorManager.getString("MessageConstant.MSG_SAVESIG");
    String MSG_REGSUCCESS=AdminWise.connectorManager.getString("MessageConstant.MSG_REGSUCCESS");
    String MSG_REGFAIL=AdminWise.connectorManager.getString("MessageConstant.MSG_REGFAIL");
    String MSG_SAVETEMPLATE=AdminWise.connectorManager.getString("MessageConstant.MSG_SAVETEMPLATE");
    String MSG_REGTEMPLATESUCCESS=AdminWise.connectorManager.getString("MessageConstant.MSG_REGTEMPLATESUCCESS");
    String MSG_REGTEMPLATEFAIL=AdminWise.connectorManager.getString("MessageConstant.MSG_REGTEMPLATEFAIL");
	/****CV2019 merges from 10.2 line***/
    String ERR_SEARCH_NORESULT=AdminWise.connectorManager.getString("MessageConstant.ERR_SEARCH_NORESULT");
    String ERR_DOCINUSE=AdminWise.connectorManager.getString("MessageConstant.ERR_DOCINUSE");
}