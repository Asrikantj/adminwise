package ViewWise.AdminWise.VWMessage;
 
import javax.swing.JApplet;
import javax.swing.JOptionPane;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import java.awt.Component;

public class VWMessage implements VWMessageConstant,VWConstant
{
    public static void setMessageOptions (int type,JApplet parent)
    {
        applet=parent;
    }
//------------------------------------------------------------------------------
    public static void setMessageOptions(JApplet parent)
    {
        setMessageOptions(BROWSER_TYPE,parent);
    }
//------------------------------------------------------------------------------
    public static void showMessage(Component parent,String msg)
    {    	
        if(parent==null) parent=applet;        
        if(AdminWise.getCurrentMode()==APPLET_MODE)
            showMessage(parent,msg,DIALOG_TYPE);        	
        else
            showMessage(parent,msg,CONSOLE_TYPE);   
    }
//------------------------------------------------------------------------------
    public static void showInitMessage(String msg)
    {
        if(AdminWise.getCurrentMode()==APPLET_MODE)
            showMessage(null,msg,INIT_TYPE);
    }
//------------------------------------------------------------------------------
    public static void showMessage(java.awt.Component parent,String msg,int type)
    {
        switch (type)
        {
            case BROWSER_TYPE:
                showBrowserMsg(msg);
                break;
            case DIALOG_TYPE:
                showDialogMsg(parent,msg);
                break;
            case CONSOLE_TYPE:
                showConsoleMsg(msg);
                break;    
           case INIT_TYPE:
                ///showInitMsg(msg);
                break;         
        }
    }
//------------------------------------------------------------------------------
    private static void showBrowserMsg(String msg)
    {
       if (applet==null) return ;
       try{
            applet.getAppletContext().showStatus(msg);
       }
       catch(Exception e){};
    }
//------------------------------------------------------------------------------
    public static void showBrowserMsgWithAction(String msg)
    {
       if (applet==null) return ;
       try{
            applet.getAppletContext().showStatus(msg);
       }
       catch(Exception e){};
    }
//------------------------------------------------------------------------------
    private static void showDialogMsg(Component parent,String msg)
    {
        JOptionPane.showMessageDialog(parent,msg,MSG_TITLE,
            JOptionPane.ERROR_MESSAGE);
    }
//------------------------------------------------------------------------------
    private static void showConsoleMsg(String msg)
    {
        System.out.println(msg);
    }
//------------------------------------------------------------------------------
    public static int showConfirmDialog(Component parent,String msg)
    {
        int result = JOptionPane.showConfirmDialog(parent,msg,MSG_TITLE,
            JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null);
        ///if(result == JOptionPane.YES_OPTION)
        return result;
    }
    //------------------------------------------------------------------------------
    public static int showOptionDialog(Component parent,String msg,
        String[] options,String defVal)
    {
        int result = JOptionPane.showOptionDialog(parent, msg, "Warning", 
                 JOptionPane.YES_NO_OPTION, 
                 JOptionPane.QUESTION_MESSAGE, 
                 null, options, defVal);
        return result; 
        
    }
//------------------------------------------------------------------------------
    public static void showNoticeDialog(Component parent,String msg)
    {
        JOptionPane.showMessageDialog(parent,msg,MSG_TITLE,JOptionPane.ERROR_MESSAGE);
    }
//------------------------------------------------------------------------------
    public static void showNoticeDialog(String msg)
    {
        JOptionPane.showMessageDialog((Component)applet,msg,MSG_TITLE,JOptionPane.ERROR_MESSAGE);
    }
//------------------------------------------------------------------------------
    public static int showWarningDialog(Component parent,String msg)
    {
        return JOptionPane.showConfirmDialog(parent,msg,MSG_TITLE,
            JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
    }
//------------------------------------------------------------------------------
    public static void showInfoDialog(Component parent,String msg)
    {
        JOptionPane.showMessageDialog(parent,msg,MSG_TITLE,JOptionPane.INFORMATION_MESSAGE);
    }
//------------------------------------------------------------------------------
    public static void showInfoDialog(String msg)
    {
        JOptionPane.showMessageDialog((Component)applet,msg,MSG_TITLE,JOptionPane.INFORMATION_MESSAGE);
    }    
    private static JApplet applet=null;
}