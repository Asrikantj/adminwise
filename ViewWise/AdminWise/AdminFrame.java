/*
	A basic extension of the java.awt.Frame class
 */
package ViewWise.AdminWise;

import java.awt.*;
import javax.swing.JOptionPane;

import com.computhink.common.Constants;
import com.computhink.common.VWEvent;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;

public class AdminFrame extends Frame implements VWConstant
{
	public AdminFrame(){
		this(APPLET_MODE);
	}
	public AdminFrame(int mode)
	{
            // This code is automatically generated by Visual Cafe when you add
            // components to the visual environment. It instantiates and initializes
            // the components. To modify the code, only use code syntax that matches
            // what Visual Cafe can generate, or Visual Cafe may be unable to back
            // parse your Java file into its visual environment.
            //{{INIT_CONTROLS
            setLayout(new BorderLayout());
            setTitle(MSG_TITLE); 
            adminWise = new AdminWise(); 
            adminWise.init();
            if(mode == APPLET_MODE){                                    
            add("Center",adminWise);
            pack();
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            if(VWImages.AdminWiseIcon!=null)
                setIconImage(VWImages.AdminWiseIcon.getImage());            
            ///setExtendedState(Frame.MAXIMIZED_BOTH);
            ///setComponentTree((Component)this,java.awt.ComponentOrientation.RIGHT_TO_LEFT);
            setSize(900,600);
            setVisible(true);
            adminWise.start();
            }
            //}}
    }
    private void setComponentTree(Component c, java.awt.ComponentOrientation orientation) {
	if(c instanceof java.awt.Container) {
	    Component[] components = ((java.awt.Container)c).getComponents();
	    for(int i=0; i<components.length; i++) {
		components[i].setComponentOrientation(orientation);
		components[i].invalidate();
		setComponentTree(components[i], orientation);
	    }
	}
    }
//------------------------------------------------------------------------------
    /**
     * Shows or hides the component depending on the boolean flag b.
     * @param b  if true, show the component; otherwise, hide the component.
     * @see java.awt.Component#isVisible
     */
    public void setVisible(boolean b)
	{
            if(b)
            {
                //Dimension frameSize = getPreferredSize();
                //setSize(frameSize.width, frameSize.height);
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                Dimension size = getSize();
                int x = (screenSize.width - size.width) / 2;
                int y = (screenSize.height - size.height) / 2;
                setLocation(x, y);
            }
            super.setVisible(b);
	}
	/*
	public void addNotify()
	{
	    super.addNotify();

            Dimension frameSize = getPreferredSize();
            setSize(frameSize.width, frameSize.height);

            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension size = getSize();
            int x = (screenSize.width - size.width) / 2;
            int y = (screenSize.height - size.height) / 2;
            setLocation(x, y);
	}
*/
    // Used for addNotify check.
        boolean fComponentsAdjusted = false;
        AdminWise adminWise=null;
	//{{DECLARE_CONTROLS
	//}}

	//{{DECLARE_MENUS
	//}}
//------------------------------------------------------------------------------
class SymWindow extends java.awt.event.WindowAdapter
{
    public void windowClosed(java.awt.event.WindowEvent event)
    {
            Object object = event.getSource();
            if (object == AdminFrame.this)
                    AdminFrame_WindowClosed(event);
    }
//------------------------------------------------------------------------------
    public void windowClosing(java.awt.event.WindowEvent event)
    {   
    	Object object = event.getSource();
        if (object == AdminFrame.this)
                AdminFrame_WindowClosing(event);        
    }
}
//------------------------------------------------------------------------------
    void AdminFrame_WindowClosing(java.awt.event.WindowEvent event)
    {
    	VWRoom selRoom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	int isIdle = VWConnector.ViewWiseClient.isRoomIdle(selRoom.getId(), selRoom.getName());
    	if(isIdle != VWEvent.IDLE_DISCONNECT && VWConnector.serverInactive == false){
    		if(AdminWise.adminPanel.docTypePanel.curMode == 9 || AdminWise.adminPanel.docTypePanel.curMode == 2){    		
    			int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("adminframe.jmsg1"),AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
    			if(res != 0)
    				return;	
    		}
    		if(AdminWise.adminPanel.docTypePanel.curMode == 15){    		
	    		int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("adminframe.jmsg2"),AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
	    		if(res != 0)
	    			return;	
	    	}
    		if(AdminWise.adminPanel.routePanel.CURRENT_MODE!=0 && AdminWise.adminPanel.routePanel.CURRENT_MODE!=2){    		
	    		int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("adminframe.jmsg3")+ AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME") +"?",AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
	    		if(res != 0)
	    			return;	
	    	}
    	}  	   
        setVisible(false);
        adminWise.destroy();
        System.exit(0);			 // hide the Frame
    }
//------------------------------------------------------------------------------
    void AdminFrame_WindowClosed(java.awt.event.WindowEvent event)
    {
        setVisible(false);
        adminWise.destroy();
        System.exit(0);		 
    }
//------------------------------------------------------------------------------
	/**
	Desc   :This method returns the current screen location in Points, where adminwise is displayed.
			parameter dlgHeight is assigned as 30, because it should start at pixel 30 from top.
	Author : Nishad Nambiar
	Date   :21-May-2008
	*/
    public Point getCurrentLocation(int dlgWidth, int dlgHeight){
    	Point point = new Point();
    	point = AdminWise.adminFrame.getLocation();
    	point.x = point.x + ((AdminWise.adminFrame.getWidth()/2) - (dlgWidth/2));
    	point.y = point.y + ((AdminWise.adminFrame.getHeight()/2) - (dlgHeight/2));
    	return point;
    }
}