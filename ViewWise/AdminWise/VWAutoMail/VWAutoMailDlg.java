package ViewWise.AdminWise.VWAutoMail;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import  java.util.Vector;
import java.awt.Frame;
import java.util.prefs.*;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.awt.event.WindowEvent;
//--------------------------------------------------------------------------
public class VWAutoMailDlg extends javax.swing.JDialog implements  VWConstant {
    public VWAutoMailDlg(Frame parent) {
        super(parent,LBL_AUTOMAILOPTIONS_NAME,true);
        getContentPane().setBackground(AdminWise.getAWColor());
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(VWAutoMail,BorderLayout.CENTER);
        setVisible(false);
        VWAutoMail.setLayout(null);
        VWAutoMail.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWAutoMail.add(PanelCommand);
        PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,12,152,132);
        BtnSave.setText(BTN_SAVE_NAME);
        BtnSave.setActionCommand(BTN_SAVE_NAME);
        PanelCommand.add(BtnSave);
//        BtnSave.setBackground(java.awt.Color.white);
        BtnSave.setBounds(12,160,144, AdminWise.gBtnHeight);
        BtnSave.setIcon(VWImages.SaveIcon);
        BtnClose.setText(BTN_CLOSE_NAME);
        BtnClose.setActionCommand(BTN_CLOSE_NAME);
        PanelCommand.add(BtnClose);
//        BtnClose.setBackground(java.awt.Color.white);
        BtnClose.setBounds(12,195,144, AdminWise.gBtnHeight);
        BtnClose.setIcon(VWImages.CloseIcon);
        PanelTable.setLayout(new BorderLayout());
        VWAutoMail.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        SymComponent aSymComponent = new SymComponent();
        VWAutoMail.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.AMOptionsImage);
        Table.addData(autoMailNames);
        SymAction lSymAction = new SymAction();
        BtnSave.addActionListener(lSymAction);
        BtnClose.addActionListener(lSymAction);
        SymKey aSymKey = new SymKey();
        BtnSave.addKeyListener(aSymKey);
        BtnClose.addKeyListener(aSymKey);
        Table.addKeyListener(aSymKey);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        getDlgOptions();
        setResizable(false);
    }
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(WindowEvent event) {
            Object object = event.getSource();
            if (object == VWAutoMailDlg.this)
                BtnClose_actionPerformed(null);
        }
        /*
        public void windowDeactivated(WindowEvent e)
        {
            BtnClose_actionPerformed(null);
        }
         */
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnSave_actionPerformed(null);
            else if (event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnSave)
                BtnSave_actionPerformed(event);
            else if (object == BtnClose)
                BtnClose_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWAutoMail = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWAutoMailTable Table = new VWAutoMailTable();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWAutoMail)
                VWAutoMail_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWAutoMail_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWAutoMail) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        cancelFlag=false;
        saveDlgOptions();
        setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        cancelFlag=true;
        saveDlgOptions();
        setVisible(false);
    }
    //--------------------------------------------------------------------------
    private void saveDlgOptions() {
        if(!AdminWise.adminPanel.saveDialogPos) return;
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
        prefs.putInt("width", this.getSize().width);
        prefs.putInt("height", this.getSize().height);
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage( getClass() );
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        setSize(prefs.getInt("width",600),prefs.getInt("height",400));
    }
    //--------------------------------------------------------------------------
    public String[] getValues() {
        String[][] data= Table.getData();
        String[] retData=new String[3];
        for(int i=0;i<2;i++) {
            if(!data[i][1].equals("") && !data[i][2].equals("0"))
                retData[i]=data[i][1].trim() + "|" + data[i][2].trim();
            else if(!data[i][1].equals("") && data[i][2].equals("0"))
                retData[i]=data[i][1].trim();
            else
                retData[i]="";
        }
        if(data[2][2].indexOf("|")>0) {
            VWStringTokenizer nameTokens=new VWStringTokenizer(data[2][1],"|",false);
            VWStringTokenizer idsTokens=new VWStringTokenizer(data[2][2],"|",false);
            retData[2]="";
            while(idsTokens.hasMoreTokens()) {
                retData[2]+=nameTokens.nextToken()+"|"+idsTokens.nextToken()+"|";
            }
            if(!retData[2].equals(""))
                retData[2]=retData[2].substring(0,retData[2].length()-1);
        }
        else if(!data[2][1].equals("") && !data[2][2].equals("0"))
            retData[2]=data[2][1].trim() + "|" + data[2][2].trim();
        else if(!data[2][1].equals("") && data[2][2].equals("0")) {
            retData[2]=data[2][1].trim();
        }
        else
            retData[2]="";
        for(int i=0;i<3;i++)
            retData[i]=retData[i].equals(".")?"":retData[i];
            return retData;
    }
    //--------------------------------------------------------------------------
    public boolean getCancelFlag() {
        return cancelFlag;
    }
    //--------------------------------------------------------------------------
    public void setData(String[] autoMailData) {
        for(int i=0;i<2;i++) {
            if(autoMailData[i].indexOf("|")>0) {
                VWStringTokenizer tokens=new VWStringTokenizer(autoMailData[i],"|",false);
                try{
                    autoMailNames[i][1]=tokens.nextToken();
                }
                catch(Exception e1){autoMailNames[i][1]="";}
                try{
                    autoMailNames[i][2]=tokens.nextToken();
                    try{
                        int indexId=VWUtil.to_Number(autoMailNames[i][2]);
                    }
                    catch(Exception e3){
                        autoMailNames[i][1]=autoMailData[i];
                        autoMailNames[i][2]="0";
                    }
                }
                catch(Exception e1){autoMailNames[i][2]="0";}
            }
            else {
                autoMailNames[i][1]=autoMailData[i];
            }
        }
        VWStringTokenizer tokens=new VWStringTokenizer(autoMailData[2],"|",false);
        autoMailNames[2][1]="";
        autoMailNames[2][2]="";
        while(tokens.hasMoreTokens()) {
            String name="";
            String id="";
            try{
                name=tokens.nextToken();
            }
            catch(Exception e1){name="";}
            try{
                id=tokens.nextToken();
                int indexId=VWUtil.to_Number(id);
                if(indexId==0) {
                    name=autoMailData[2];
                    id="0";
                }
            }
            catch(Exception e2){id="0";}
            autoMailNames[2][1]+=(name.trim().equals("")?"":name+"|");
            autoMailNames[2][2]+=(id.equals("0")?"0":id+"|");
        }
        if(autoMailNames[2][1] !=null && !autoMailNames[2][1].trim().equals(""))
            autoMailNames[2][1]=autoMailNames[2][1].substring(0,autoMailNames[2][1].length()-1);
        Table.addData(autoMailNames);
    }
    //--------------------------------------------------------------------------
    public String[] getRowData(int rowNo) {
        return Table.getRowData(rowNo);
    }
    //--------------------------------------------------------------------------
    boolean cancelFlag=true;
}