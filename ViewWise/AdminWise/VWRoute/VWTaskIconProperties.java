
/**
 * 
 */
package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;

import org.jhotdraw.framework.Connector;
import org.jhotdraw.framework.Drawing;
import org.jhotdraw.framework.Figure;
import org.jhotdraw.framework.FigureEnumeration;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.figures.VWFigure;
import ViewWise.AdminWise.VWRoute.figures.VWLineConnection;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

import com.computhink.common.Constants;
import com.computhink.common.Principal;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.RouteUsers;
import com.computhink.common.util.JTextFieldLimit;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCConstants;
import com.computhink.vwc.VWCPreferences;

/**
 * @author nishad.n
 *
 */
public class VWTaskIconProperties extends JDialog implements VWConstant{
	private static final long serialVersionUID = 1L;
	JTextField txtTaskName = new JTextField();
	JTextField txtTaskDuration = new JTextField();	

	JTextArea taTaskDesc = new JTextArea();

	JScrollPane spTaskDesc = new JScrollPane(taTaskDesc);

	VWApproversListPanel routeUserListTable = new VWApproversListPanel();


	JLabel lblTaskName = new JLabel();
	JLabel lblTaskDesc = new JLabel();
	JLabel lblTaskUserList = new JLabel();
	JLabel lblTaskInterval = new JLabel();
	JLabel lblApproveRejectTitle = new JLabel();
	JLabel lblDays= new JLabel(AdminWise.connectorManager.getString("VWTaskIconProperties.lblDays"));

	JCheckBox chkTaskInterval =  new JCheckBox();
	JSpinner spinnerTaskInterval = null;

	public static VWLinkedButton btnAdd = new VWLinkedButton(0,true);
	public static VWLinkedButton btnRemove = new VWLinkedButton(0,true);
	VWLinkedButton btnDone = new VWLinkedButton(0,true);
	VWLinkedButton btnCancel = new VWLinkedButton(0,true);

	JPanel pnlComponents = new JPanel();

	JRadioButton rdoApproveByAllUsers = new JRadioButton();
	JRadioButton rdoApproveByAnyOne = new JRadioButton();
	JRadioButton rdoApproveByGivenNumUsers = new JRadioButton();
	static JRadioButton rdoUserSelectsTask = new JRadioButton();
	JRadioButton rdoSendToOriginator = new JRadioButton();
	JRadioButton rdoSelectTask = new JRadioButton();
	javax.swing.ButtonGroup grpApprovalCondition = new javax.swing.ButtonGroup();
	javax.swing.ButtonGroup grpRejectCondition = new javax.swing.ButtonGroup();
	 String languageLocale=AdminWise.getLocaleLanguage();
	JLabel lblApprovalCondition = new JLabel();
	JLabel lblAcceptLabel = new JLabel();
	JLabel lblRejectLabel = new JLabel();
	JTextField txtAcceptLabel = new JTextField();
	JTextField txtRejectLabel = new JTextField();
	
	JLabel lblAcceptSignature = new JLabel();
	VWComboBox cmbSignatureList = new VWComboBox();
	JLabel lblRejectCondition = new JLabel();

	JTextField txtApproverCount = new JTextField();
	VWComboBox cmbTaskList = new VWComboBox();
	VWComboBox cmbTaskUsersList = new VWComboBox();

	RouteTaskInfo thisRouteTaskInfo =null;
	public static boolean active = false;
	/**CV2019 merges from SIDBI line***/
	JCheckBox chkMailToPreviousTaskUsers =  new JCheckBox();
	JLabel lblMailToPreviousTaskUsers = new JLabel();
	
	


	public VWTaskIconProperties(Frame parent, String figureName){
		super(parent);		
		initComponents();
		txtTaskName.setText(figureName);
		//PR Comment
		loadTaskRouteInfo(figureName);
		setSize(476,589);
		setResizable(false);
		//PR Comment
		setCurrentLocation();
		setVisible(true);
	}
	public VWTaskIconProperties(){

	}

	public VWTaskIconProperties(int mode){
		initComponents();
		setSize(476,589);
		setResizable(false);
		//setCurrentLocation();
		setVisible(true);
	}

	public void initComponents(){
		getContentPane().setBackground(AdminWise.getAWColor());
		if (active)
		{
			requestFocus();
			toFront();
			return;
		} 
		pnlComponents.setLayout(null);
		setLayout(null);
		pnlComponents.setBackground(AdminWise.getAWColor());
		JLabel tasklineBorderLabel =  new JLabel("");
		JLabel taskLabel =  new JLabel(AdminWise.connectorManager.getString("TaskIconproperties.Task"));
		taskLabel.setForeground(Color.BLUE);
		taskLabel.setBounds(10, 8, 100, 13);
		pnlComponents.add(taskLabel);

		
		Border border = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		tasklineBorderLabel.setBorder(border);
		tasklineBorderLabel.setBounds(90, 14, 370, 2);
		pnlComponents.add(tasklineBorderLabel);

		lblTaskName.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblTaskName"));
		lblTaskName.setBounds(18,25,100,20);
		pnlComponents.add(lblTaskName);

		txtTaskName.setBounds(90,25,370,20);
		txtTaskName.setDocument(new JTextFieldLimit(50));
		txtTaskName.setToolTipText(AdminWise.connectorManager.getString("VWTaskIconProperties.txtTaskName"));
		pnlComponents.add(txtTaskName);

		lblTaskDesc.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblTaskDesc"));
		lblTaskDesc.setBounds(18,55,100,20);
		pnlComponents.add(lblTaskDesc);

		spTaskDesc.setBounds(90,55,370,30+6);
		spTaskDesc.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlComponents.add(spTaskDesc);
		spTaskDesc.getViewport().setBackground(AdminWise.getAWColor());
		taTaskDesc.setDocument(new JTextFieldLimit(255));
		taTaskDesc.setLineWrap(true);
		taTaskDesc.setToolTipText(AdminWise.connectorManager.getString("VWTaskIconProperties.taTaskDesc"));

		//chkTaskInterval.setBounds(10,95,15,20);
		chkTaskInterval.setBounds(10,95+8,15,20);
		chkTaskInterval.setBackground(AdminWise.getAWColor());
		pnlComponents.add(chkTaskInterval);

		lblTaskInterval.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblTaskInterval"));
		lblTaskInterval.setBounds(28,95+8,90,20);
		pnlComponents.add(lblTaskInterval);

		initTaskInterval();
		spinnerTaskInterval.setBounds(117, 95+8, 110, 20);
		pnlComponents.add(spinnerTaskInterval);

		/*txtTaskDuration.setBounds(110,95,60,20);
		pnlComponents.add(txtTaskDuration);

		lblDays.setBounds(155,95,60,20);
		pnlComponents.add(lblDays);*/

		lblTaskUserList.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblTaskUserList"));
		lblTaskUserList.setBounds(10,125,560,20);
		lblTaskUserList.setForeground(Color.BLUE);

		pnlComponents.add(lblTaskUserList);


		JLabel userListlineBorderLabel =  new JLabel("");
		Border border1 = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		userListlineBorderLabel.setBorder(border1);
		userListlineBorderLabel.setBounds(90, 135, 300, 2);
		pnlComponents.add(userListlineBorderLabel);
		
		btnAdd.setBounds(400, 120, 25, AdminWise.gBtnHeight);
		btnAdd.setIcon(VWImages.ApproverAddIcon);
		btnAdd.setMnemonic('A');
		btnAdd.addActionListener(new SymAction());
		pnlComponents.add(btnAdd);
		
		btnRemove.setBounds(435, 120, 25, AdminWise.gBtnHeight);
		btnRemove.setIcon(VWImages.ApproverRemoveIcon);
		btnRemove.setMnemonic('D');
		btnRemove.addActionListener(new SymAction());
		pnlComponents.add(btnRemove);


		routeUserListTable.setBounds(5,110,500,173);		
		pnlComponents.add(routeUserListTable);		


		lblApproveRejectTitle.setText("");
		TitledBorder titledBorder = new TitledBorder(AdminWise.connectorManager.getString("VWTaskIconProperties.TitledBorder"));
		titledBorder.setTitleColor(Color.BLUE);
		lblApproveRejectTitle.setBorder(titledBorder);
		lblApproveRejectTitle.setBounds(8,285,453,230);
		pnlComponents.add(lblApproveRejectTitle);



		lblApprovalCondition.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblApprovalCondition"));
		//20,302,100,20
		lblApprovalCondition.setBounds(20,302,100+10,20);//width increased for cv10 dutch localization.
		pnlComponents.add(lblApprovalCondition);

		
		JLabel acceptlineBorderLabel =  new JLabel("");
		Border acceptBorder = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		acceptlineBorderLabel.setBorder(acceptBorder);
		acceptlineBorderLabel.setBounds(147, 312, 75, 2);
		pnlComponents.add(acceptlineBorderLabel);
		
		lblAcceptLabel.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblAcceptLabel"));
		lblAcceptLabel.setBounds(230,302,80,20);
		pnlComponents.add(lblAcceptLabel);

		//300,302,100,20
		txtAcceptLabel.setBounds(300+20,302,100,20);//x position incremented with 20 for dutch localization.
		txtAcceptLabel.setDocument(new JTextFieldLimit(15));
		txtAcceptLabel.setToolTipText(AdminWise.connectorManager.getString("VWTaskIconProperties.txtAcceptLabel"));
		pnlComponents.add(txtAcceptLabel);
		
		rdoApproveByAllUsers.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.rdoApproveByAllUsers"));
		rdoApproveByAllUsers.setSelected(true);
		rdoApproveByAllUsers.setBounds(30,322,200,20);
		pnlComponents.add(rdoApproveByAllUsers);
		rdoApproveByAllUsers.addItemListener(new ItemAction());
		rdoApproveByAllUsers.addActionListener(new SymAction());
		rdoApproveByAllUsers.setSelected(true);

		rdoApproveByAnyOne.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.rdoApproveByAnyOne"));
		rdoApproveByAnyOne.setBounds(30,341,200,20);
		pnlComponents.add(rdoApproveByAnyOne);
		rdoApproveByAnyOne.addItemListener(new ItemAction());
		rdoApproveByAnyOne.addActionListener(new SymAction());

		rdoApproveByGivenNumUsers.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.rdoApproveByGivenNumUsers"));
		//30,360,200,20
		rdoApproveByGivenNumUsers.setBounds(30,360,200+40,20);//width increased for cv10 localization
		pnlComponents.add(rdoApproveByGivenNumUsers);
		rdoApproveByGivenNumUsers.addItemListener(new ItemAction());
		rdoApproveByGivenNumUsers.addActionListener(new SymAction());

		txtApproverCount.setText("");
		//230,360,63,20
		txtApproverCount.setBounds(230+40,360,63,20);//X position increased for cv10
		pnlComponents.add(txtApproverCount);
		txtApproverCount.setEnabled(false);

		//Added for Route enhancement - Signature type
		lblAcceptSignature.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblAcceptSignature"));
		lblAcceptSignature.setBounds(47,390,200,20);
		pnlComponents.add(lblAcceptSignature);
		//148,390,145,20
		cmbSignatureList.setBounds(148+42,390,145,20);//X position increase for cv10 dutch localization
		pnlComponents.add(cmbSignatureList);
		cmbSignatureList.addItems(DRSTaskSignatureList);

		lblRejectCondition.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblRejectCondition"));
		//20,420,100,20
		lblRejectCondition.setBounds(20,420,100+30,20);//width increased for cv10 dutch localization
		pnlComponents.add(lblRejectCondition);
		
		JLabel rejectlineBorderLabel =  new JLabel("");
		Border rejectBorder = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		rejectlineBorderLabel.setBorder(rejectBorder);
		rejectlineBorderLabel.setBounds(147, 430, 75, 2);
		pnlComponents.add(rejectlineBorderLabel);
		
		lblRejectLabel.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblRejectLabel"));
		//230,420,80,20
		lblRejectLabel.setBounds(230,420,80,20);//x position increased for cv10 dutch localization.
		pnlComponents.add(lblRejectLabel);

		//300,420,100,20
		txtRejectLabel.setBounds(300+20,420,100,20);//x position increased for cv10 dutch localization
		txtRejectLabel.setDocument(new JTextFieldLimit(15));
		txtRejectLabel.setToolTipText(AdminWise.connectorManager.getString("VWTaskIconProperties.txtRejectLabel"));
		pnlComponents.add(txtRejectLabel);

		rdoUserSelectsTask.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.rdoUserSelectsTask"));
		rdoUserSelectsTask.setSelected(true);
		rdoUserSelectsTask.setBounds(30,440,200,20);
		rdoUserSelectsTask.addItemListener(new ItemAction());
		pnlComponents.add(rdoUserSelectsTask);

		rdoSendToOriginator.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.rdoSendToOriginator_1")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWTaskIconProperties.rdoSendToOriginator_2"));
		//30,459,190,20
		rdoSendToOriginator.setBounds(30,459,190+30,20);//width increase for cv10 dutch localization
		rdoSendToOriginator.addItemListener(new ItemAction());
		pnlComponents.add(rdoSendToOriginator);

		rdoSelectTask.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.rdoSelectTask"));
		rdoSelectTask.setBounds(30,478,110,20);
		rdoSelectTask.addItemListener(new ItemAction());
		pnlComponents.add(rdoSelectTask);


		rdoApproveByAllUsers.setBackground(AdminWise.getAWColor());
		rdoApproveByAnyOne.setBackground(AdminWise.getAWColor());
		rdoApproveByGivenNumUsers.setBackground(AdminWise.getAWColor());
		rdoUserSelectsTask.setBackground(AdminWise.getAWColor());
		rdoSendToOriginator.setBackground(AdminWise.getAWColor());
		rdoSelectTask.setBackground(AdminWise.getAWColor());


		cmbTaskList.setBounds(148,482,145,20);
		pnlComponents.add(cmbTaskList);


		grpApprovalCondition.add(rdoApproveByAllUsers);
		grpApprovalCondition.add(rdoApproveByAnyOne);
		grpApprovalCondition.add(rdoApproveByGivenNumUsers);

		grpRejectCondition.add(rdoUserSelectsTask);
		grpRejectCondition.add(rdoSendToOriginator);
		grpRejectCondition.add(rdoSelectTask);
		
		/**CV2019 merges from SIDBI line***/
		chkMailToPreviousTaskUsers.setBounds(10, 525, 15, 20);
		chkMailToPreviousTaskUsers.setBackground(AdminWise.getAWColor());
		pnlComponents.add(chkMailToPreviousTaskUsers);
		lblMailToPreviousTaskUsers.setText(AdminWise.connectorManager.getString("VWTaskIconProperties.lblMailToPreviousTaskUsers"));
		lblMailToPreviousTaskUsers.setBounds(28,525,275,20);
		pnlComponents.add(lblMailToPreviousTaskUsers);
		AdminWise.printToConsole("AdminWise.adminPanel.routePanel.sidbiCustomizationFlag....."+AdminWise.adminPanel.routePanel.sidbiCustomizationFlag);
		if (AdminWise.adminPanel.routePanel.sidbiCustomizationFlag == true) {
			chkMailToPreviousTaskUsers.setVisible(true);
			chkMailToPreviousTaskUsers.setSelected(true);
			lblMailToPreviousTaskUsers.setVisible(true);
		} else {
			chkMailToPreviousTaskUsers.setVisible(false);
			chkMailToPreviousTaskUsers.setSelected(false);
			lblMailToPreviousTaskUsers.setVisible(false);
		}		
		/****************************************************/
		btnDone.setText(BTN_SAVE_NAME);
		btnDone.setMnemonic('S');
		if(languageLocale.equals("nl_NL")){
			btnDone.setBounds(297-20,525,AdminWise.gBtnWidth+10,AdminWise.gBtnHeight);	
		}else
			btnDone.setBounds(297,525,AdminWise.gBtnWidth,AdminWise.gBtnHeight);
		
		btnDone.setIcon(VWImages.SaveIcon);
		btnDone.addActionListener(new SymAction());
		pnlComponents.add(btnDone);

		btnCancel.setText(BTN_CANCEL_NAME);
		btnCancel.setMnemonic('C');
		if(languageLocale.equals("nl_NL")){
			btnCancel.setBounds(379-10,525,AdminWise.gBtnWidth+10,AdminWise.gBtnHeight);	
		}else
		btnCancel.setBounds(379,525,AdminWise.gBtnWidth,AdminWise.gBtnHeight);
		btnCancel.setIcon(VWImages.CloseIcon);
		btnCancel.addActionListener(new SymAction());
		pnlComponents.add(btnCancel);
		active = true;  
		//PR Comment
		viewMode();
		pnlComponents.setBounds(0,0,500,585);
		add(pnlComponents);
		setModal(true);
		setTitle(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWTaskIconProperties.setTitle"));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent evt){
				closeDialog();
			}
			public void windowLostFocus(WindowEvent e){
				if(active){
					requestFocus();
					toFront();
				}
			}
			public void windowDeactivated(WindowEvent e) {
				if(active){
					requestFocus();
					toFront();
				}
			}
			public void windowStateChanged(WindowEvent e) {
				toFront();
			}
			public void windowDeiconified(WindowEvent e) {
				toFront();
			}
		});
	}
	
	public int groupCount = 0;
	//vecApproversList is having Principals bean value
	public static Vector<Principal> vecApproversList = new Vector<Principal>();
	
	//allApproversList is having users and groups names (String)
	public static Vector<String> allApproversList = null;
	//allEscalationUsersList is having only users along with Auto Accept, Auto reject
	public static Vector<Principal> allEscalationUsersList = null;
	
	//vecApproversListFromDB is having Vector of dbstrings
	public static Vector<Principal> vecApproversListFromDB = new Vector<Principal>();
	
	


	public void initTaskInterval() {
		try{
			Calendar calendar = Calendar.getInstance();
			
			calendar.set(Calendar.SECOND, 00);
			calendar.set(Calendar.HOUR_OF_DAY, 00);
			calendar.set(Calendar.MINUTE, 00);
			
			SpinnerDateModel dateModel = new SpinnerDateModel();
			dateModel.setValue(calendar.getTime());
			
			if(spinnerTaskInterval == null){
				spinnerTaskInterval = new JSpinner(dateModel);	
			} else{
				dateModel.setValue(calendar.getTime());
				spinnerTaskInterval.setModel(dateModel);
			}
			
			JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(spinnerTaskInterval, "ss'day' HH'hr' mm'min'");
			spinnerTaskInterval.setEditor(dateEditor);
		}catch(Exception ex){
			//System.out.println("Exception in initTaskInterval is : "+ex.getMessage());
		}
	}
	private void setRemainderInterval(String reminderInterval){
		int reminderTime = Integer.parseInt(reminderInterval);
		
		try{
			Calendar calendar = Calendar.getInstance();

			int day = 0, min=0, hr = 0;

			if(reminderTime> 60){
				min = reminderTime % 60;

				day = (reminderTime /60) / 24;
				hr = (reminderTime /60) % 24;
			}else{
				min = reminderTime;
			}
			
			calendar.set(Calendar.SECOND, day);
			calendar.set(Calendar.HOUR_OF_DAY, hr);
			calendar.set(Calendar.MINUTE, min);

			Date initDate = calendar.getTime();

			SpinnerDateModel dateModel = new SpinnerDateModel();
			dateModel.setValue(initDate);
			JSpinner.DateEditor dateEditor;
			spinnerTaskInterval.setModel(dateModel);
			dateEditor = new JSpinner.DateEditor(spinnerTaskInterval, "ss'day' HH'hr' mm'min'");

			spinnerTaskInterval.setEditor(dateEditor);
		}catch(Exception ex){
			//System.out.println("setRemainderInterval : "+ex.getMessage());
		}
	}
	protected JRootPane createRootPane() { 
		JRootPane rootPane = new JRootPane();
		//Handle escape key.
		KeyStroke escapeStroke = KeyStroke.getKeyStroke("ESCAPE");
		Action escapeActionListener = new AbstractAction() { 
			public void actionPerformed(ActionEvent actionEvent) {
				btnCancel_ActionPerformed();
			} 
		} ;		
		InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(escapeStroke, "ESCAPE");
		rootPane.getActionMap().put("ESCAPE", escapeActionListener);
		//Handle enter key.
		KeyStroke enterStroke = KeyStroke.getKeyStroke("ENTER");
		Action enterActionListener = new AbstractAction() { 
			public void actionPerformed(ActionEvent actionEvent) {
			try {
					btnSave_ActionPerformed();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		} ;
		inputMap.put(enterStroke, "ENTER");
		rootPane.getActionMap().put("ENTER", enterActionListener);

		return rootPane;
	} 

	public void closeDialog() 
	{
		try{
			active = false;
			setVisible(false);
			dispose();
		}catch(Exception ex){

		}
	}
	private void viewMode(){
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2){
			btnDone.setEnabled(false);
			btnAdd.setEnabled(false);
			btnRemove.setEnabled(false);
		}
		else if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 1 || AdminWise.adminPanel.routePanel.CURRENT_MODE == 3
				|| AdminWise.adminPanel.routePanel.CURRENT_MODE == 4){
			btnDone.setEnabled(true);
			btnAdd.setEnabled(true);
		}
		else{
			btnDone.setEnabled(true);
			btnAdd.setEnabled(true);
		}
	}

	class ItemAction implements ItemListener{

		public void itemStateChanged(ItemEvent ie){

			if(ie.getSource()==rdoApproveByAllUsers){
				txtApproverCount.setEnabled(false);
			}
			else if(ie.getSource()==rdoApproveByAnyOne){
				txtApproverCount.setEnabled(false);			
			}
			else if(ie.getSource()==rdoApproveByGivenNumUsers){
				txtApproverCount.setEnabled(true);
			}
			else if(ie.getSource()==rdoUserSelectsTask){
				cmbTaskList.setEnabled(false);
			}
			else if(ie.getSource()==rdoSendToOriginator){
				cmbTaskList.setEnabled(false);
			}			
			else if(ie.getSource()==rdoSelectTask){
				cmbTaskList.setEnabled(true);
			}
			
		}
	}

	public RouteTaskInfo getSelectedTaskInfo(){
		RouteTaskInfo taskInfo = new RouteTaskInfo();

		try{

			int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
			if( (AdminWise.adminPanel.routePanel.routeInfoBean!=null) ){
				ArrayList routeTaskList = AdminWise.adminPanel.routePanel.alRouteTaskInfo;
				if(routeTaskList!=null && routeTaskList.size()>0){
					for(int count=0;count<routeTaskList.size();count++){
						RouteTaskInfo curTskInfo = (RouteTaskInfo) routeTaskList.get(count);
						if(curTskInfo.getTaskFigureId()==selFigureId){
							taskInfo = curTskInfo;
							break;
						}
					}
				}
			}
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,e.toString());
		}
		return taskInfo;
	}

	public void  removeSelectedTaskInfo(){
		try{
			int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
			if( (AdminWise.adminPanel.routePanel.alRouteTaskInfo!=null) ){
				ArrayList routeTaskList = AdminWise.adminPanel.routePanel.alRouteTaskInfo;
				if(routeTaskList!=null && routeTaskList.size()>0){
					for(int count=0;count<routeTaskList.size();count++){
						RouteTaskInfo curTskInfo = (RouteTaskInfo) routeTaskList.get(count);
						if(curTskInfo.getTaskFigureId()==selFigureId){
							AdminWise.adminPanel.routePanel.alRouteTaskInfo.remove(count);
							//AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(count,curTskInfo);
							break;
						}
					}
				}
			}
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,e.toString());	
		}
	}

	public boolean  updateSelectedTaskInfo(){
		boolean existing = false;
		int emailPrvTaskUsers = 0;
		try{
			RouteTaskInfo taskInfo = new RouteTaskInfo();
			int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
			AdminWise.adminPanel.routePanel.drsDrawing.setTaskName(txtTaskName.getText().trim());
			if( (AdminWise.adminPanel.routePanel.routeInfoBean!=null) ){
				ArrayList routeTaskList = AdminWise.adminPanel.routePanel.alRouteTaskInfo;
				/**
				 * Conditon added newly for updating the missing task information in workflow.
				 */
				Vector<Integer> figreIds = new Vector<Integer>();
				if(routeTaskList!=null && routeTaskList.size()>0){
					for(int taskCount=0;taskCount<routeTaskList.size();taskCount++){
						RouteTaskInfo curTskInfo=(RouteTaskInfo) routeTaskList.get(taskCount);
						figreIds.add(taskCount,curTskInfo.getTaskFigureId());
					}
					if(figreIds!=null&&!figreIds.contains(selFigureId))
					{
						RouteTaskInfo missingTaskInfo=new RouteTaskInfo();
						missingTaskInfo.setTaskFigureId(selFigureId);
						missingTaskInfo.setRouteTaskId(selFigureId);
						AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(missingTaskInfo);
					}
				}//End of condition for missing task 

				routeTaskList = AdminWise.adminPanel.routePanel.alRouteTaskInfo;
				
				if(routeTaskList!=null && routeTaskList.size()>0){
					for(int taskCount=0;taskCount<routeTaskList.size();taskCount++){
						RouteTaskInfo curTskInfo=(RouteTaskInfo) routeTaskList.get(taskCount);
						if (curTskInfo.getTaskFigureId()==selFigureId){
							//Remove the existing route task object and add updated one.
							AdminWise.adminPanel.routePanel.alRouteTaskInfo.remove(taskCount);
							existing = true;
							//Editing Of all Columns...
							curTskInfo.setTaskDescritpion(taTaskDesc.getText().trim());
							curTskInfo.setRouteId(curTskInfo.getRouteId());
							curTskInfo.setRouteTaskId(curTskInfo.getRouteTaskId());							
							curTskInfo.setTaskName(txtTaskName.getText().trim());
							curTskInfo.setTaskDescritpion(taTaskDesc.getText().trim());
							//Escalation
							int taskReminderTime = getMinutesForReminderInterval(spinnerTaskInterval.getModel().getValue().toString());
							String taskReminderTimeStr = String.valueOf(taskReminderTime);
							
							curTskInfo.setTaskReminder(taskReminderTimeStr);
							curTskInfo.setTaskReminderEnabled((chkTaskInterval.isSelected()==true?1:0));
							try{
								curTskInfo.setTasklength(0);
							}catch(Exception exception){
								//JOptionPane.showMessageDialog(null,"Exception 1:"+exception.toString());
							}
							try{
								int rejectedTaskId = 0;
								if(rdoUserSelectsTask.isSelected())
									rejectedTaskId = -1;
								else if(rdoSendToOriginator.isSelected())
									rejectedTaskId = 0;
								else if(rdoSelectTask.isSelected()){
									try{
										String selectedFigureName = String.valueOf(cmbTaskList.getSelectedItem());
										rejectedTaskId = getTaskSeqIdForReject(selectedFigureName.trim());
									}catch(Exception e){
										rejectedTaskId = -2;
										//JOptionPane.showMessageDialog(null,e.toString());
									}
								}
								curTskInfo.setOnRejectTaskId(rejectedTaskId);
								int approversCount = 0;
								if(rdoApproveByAllUsers.isSelected()){
									String userOrGrpName = routeUserListTable.Table.getValueAt(0,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
									if((userOrGrpName != null && userOrGrpName.trim().startsWith("<<") && userOrGrpName.trim().endsWith(">>"))){
										approversCount = 99;
									}else{
										approversCount = getSelectedApproversCount();
									}
								}else if(rdoApproveByAnyOne.isSelected())
									approversCount = 1;
								else if(rdoApproveByGivenNumUsers.isSelected())
									approversCount = Integer.parseInt(txtApproverCount.getText());
								curTskInfo.setApproversCount(approversCount);
								curTskInfo.setAcceptSignatureId(cmbSignatureList.getSelectedIndex());
							}catch(Exception e){
								//JOptionPane.showMessageDialog(null,"Ex 1:"+e.toString());
							}
							try{
								ArrayList aListRouteUsers = new ArrayList();
								for(int appCount=0;appCount<routeUserListTable.Table.getRowCount();appCount++){
									VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(appCount);
									String userMailId = routeUserListTable.Table.getValueAt(appCount,routeUserListTable.Table.m_data.COL_APPROVERMAILID).toString();
									String userOrGrpName = routeUserListTable.Table.getValueAt(appCount,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
									int userId = Integer.parseInt(routeUserListTable.Table.getValueAt(appCount,routeUserListTable.Table.m_data.COL_APPROVERID).toString());
									int usrGrpType = Principal.USER_TYPE;
									usrGrpType = Integer.parseInt(routeUserListTable.Table.getValueAt(appCount, routeUserListTable.Table.m_data.COL_APPROVERTYPE).toString());
									
									//Escalation
									String expiryPeriod = routeUserListTable.Table.getValueAt(appCount, routeUserListTable.Table.m_data.COL_EXPIRATION).toString();
									String escalationUserName= routeUserListTable.Table.getValueAt(appCount, routeUserListTable.Table.m_data.COL_ESCALATION).toString();
									String notifyMail = routeUserListTable.Table.getValueAt(appCount, routeUserListTable.Table.m_data.COL_NOTIFY).toString();
									int escalationUserId = Integer.parseInt(routeUserListTable.Table.getValueAt(appCount, routeUserListTable.Table.m_data.COL_ESCALATION_USER_ID).toString());
									
									if(usrGrpType ==  Principal.GROUP_TYPE){
										Vector principals = VWRouteConnector.getDRSPrincipals(1, userOrGrpName);
										for(int i = 0; principals!=null && i< principals.size(); i++){
											Principal members =  (Principal)principals.get(i);
											RouteUsers routeUsers = new RouteUsers();
											routeUsers.setRouteId(1);
											routeUsers.setGroupName(userOrGrpName);
											routeUsers.setUserName(members.getName());
											if(vwApproverInfo.getApproverActionFlag())
												routeUsers.setActionPermission(1);
											else
												routeUsers.setActionPermission(0);
											if(vwApproverInfo.getApproverEmailFlag())
												routeUsers.setSendEmail(1);
											else
												routeUsers.setSendEmail(0);
											routeUsers.setUserId(members.getUserGroupId());
											//Escalation
											int expiryPeriodTime = getTimeInMinutes(expiryPeriod);
											String expiryPeriodStr = String.valueOf(expiryPeriodTime);
											routeUsers.setExpiryPeriod(expiryPeriodStr);
											routeUsers.setEUserName(escalationUserName);
											routeUsers.setNotifyMail(notifyMail);
											routeUsers.setEUserid(escalationUserId);
											aListRouteUsers.add(routeUsers);
										}
									}else if(usrGrpType ==  Principal.USER_TYPE){
										RouteUsers routeUsers = new RouteUsers();
										routeUsers.setRouteId(1);
										routeUsers.setGroupName(userOrGrpName);
										routeUsers.setUserName(userOrGrpName);
										if(vwApproverInfo.getApproverActionFlag())
											routeUsers.setActionPermission(1);
										else
											routeUsers.setActionPermission(0);
										if(vwApproverInfo.getApproverEmailFlag())
											routeUsers.setSendEmail(1);
										else
											routeUsers.setSendEmail(0);
										routeUsers.setUserId(userId);
										routeUsers.setUserEmail(userMailId);
										//Escalation
										int expiryPeriodTime = getTimeInMinutes(expiryPeriod);
										String expiryPeriodStr = String.valueOf(expiryPeriodTime);
										routeUsers.setExpiryPeriod(expiryPeriodStr);
										routeUsers.setEUserName(escalationUserName);
										routeUsers.setNotifyMail(notifyMail);
										routeUsers.setEUserid(escalationUserId);
										
										aListRouteUsers.add(routeUsers);
									}
								}
								curTskInfo.setRouteUsersList(aListRouteUsers);	
							}catch(Exception e){
								AdminWise.printToConsole("Error in updateSelectedTaskInfo:"+e.toString());
							}
							//Set Route Task SequenceID
							if(VWStartIconProperties.listTasksModel!=null){
								AdminWise.adminPanel.routePanel.alRouteTaskSequence = new ArrayList(); 
								for(int i=0;VWStartIconProperties.listTasksModel!=null&&i<VWStartIconProperties.listTasksModel.size();i++){
									try{
										String currentTaskName = String.valueOf(VWStartIconProperties.listTasksModel.get(i));
										int taskId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(currentTaskName);
										AdminWise.adminPanel.routePanel.alRouteTaskSequence.add(taskId);
									}catch(Exception ex){
										//JOptionPane.showMessageDialog(null,"Saving Ex 1"+ex.toString());
									}
								}}
							//END... 
							curTskInfo.setAcceptLabel(txtAcceptLabel.getText().trim());
							curTskInfo.setRejectLabel(txtRejectLabel.getText().trim());
							/**CV2019 merges from SIDBI line***/
							emailPrvTaskUsers = chkMailToPreviousTaskUsers.isSelected()==true?1:0;
							curTskInfo.setEmailToPrvTaskUsers(emailPrvTaskUsers);
							
							AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(taskCount,curTskInfo);

						}
					}
				}
			}
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,e.toString());	
		}
		return existing;
	}

	public void loadTaskNames(){
		cmbTaskList.removeAllItems();
		int count=0;
		try{
			VWStartIconProperties.loadAvailableTasks();
			FigureEnumeration selFigureEnum = AdminWise.adminPanel.routePanel.drsDrawing.view().selection();
			VWFigure selectedFigure = (VWFigure)selFigureEnum.nextFigure();
			String selFigureName = selectedFigure.getFigureName();
			for(count=0;count<VWStartIconProperties.listTasksModel.size();count++){
				String curselFigureName = VWStartIconProperties.listTasksModel.get(count).toString();
				if(!selFigureName.trim().equalsIgnoreCase(curselFigureName.trim())){
					cmbTaskList.addItem(curselFigureName);
				}
				else{
					break;
				}
			}
			count++;
			if(count!=VWStartIconProperties.listTasksModel.size()){
				String curselFigureName = VWStartIconProperties.listTasksModel.get((count)).toString();
				cmbTaskList.addItem(curselFigureName);
			}else{
				cmbTaskList.addItem("End "+Constants.WORKFLOW_MODULE_NAME);
			}
		}catch(Exception e){
			//System.out.println("Error in loadTaskNames "+e.toString());
		}
		String taskName = resetPreviousRejectAction();
		if(taskName!=null && !taskName.trim().equals(""))
		{
			active = false;
			JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_1")+" "+ Constants.WORKFLOW_MODULE_NAME +AdminWise.connectorManager.getString("VWTaskIconProperties.msg_2")+taskName+"'.");
			active = true;
		}
	}

	public String resetPreviousRejectAction(){
		boolean reset = false;
		String taskName = "";
		if(AdminWise.adminPanel.routePanel.alRouteTaskInfo!=null && AdminWise.adminPanel.routePanel.alRouteTaskInfo.size()>1 &&
				VWStartIconProperties.listTasksModel!=null && VWStartIconProperties.listTasksModel.size()>1){
			for(int i = 0; i<VWStartIconProperties.listTasksModel.size()-1; i++){
				taskName = VWStartIconProperties.listTasksModel.get(i).toString();
				for(int j = 0; j<AdminWise.adminPanel.routePanel.alRouteTaskInfo.size(); j++){
					RouteTaskInfo taskInfo = (RouteTaskInfo)AdminWise.adminPanel.routePanel.alRouteTaskInfo.get(j);
					String routeTaskName = taskInfo.getTaskName();
					if(taskName.trim().equalsIgnoreCase(routeTaskName.trim())){
						if(taskInfo.getOnRejectTaskId() == -2){
							taskInfo.setOnRejectTaskId(-1);
							AdminWise.adminPanel.routePanel.alRouteTaskInfo.remove(j);
							AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(j, taskInfo);
							reset = true;
							break;
						}
					}
				}
				if(reset)
					break;
				else
					taskName ="";
			}
		}
		return taskName;
	}

	public String getLargestTaskFigureId(){
		String largestFigureName = "Task_";
		int largest=0;
		try{
			FigureEnumeration fEnum = AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().figures();	    	
			while(fEnum.hasNextFigure()){	        		
				VWFigure curFigure = (VWFigure) fEnum.nextFigure();	    		
				String curselFigureName = curFigure.getFigureName();
				if(curselFigureName.startsWith("Task")){
					StringTokenizer stokens2 = new StringTokenizer(curselFigureName,"_");
					stokens2.nextToken();
					int curselFigureId = Integer.parseInt(stokens2.nextToken());
					if(curselFigureId >largest){
						largest = curselFigureId;		    		
					}
				}	    		
			}
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,e.toString());
		}
		return largestFigureName+largest;
	}
	
	public int selectedRow=0;
	public int selectedColumn=0;
	private boolean validateData(){
		try{
			boolean valid = false;
			if(txtTaskName.getText().trim().equalsIgnoreCase("")){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_4"), VWMessage.DIALOG_TYPE);
				active = true;
				return false;
			}
			int selectedApproverCount = getSelectedApproversCount();
			
			if(routeUserListTable.Table.isEditing())
				routeUserListTable.Table.getCellEditor().stopCellEditing();

			if(chkTaskInterval.isSelected() && getMinutesForReminderInterval(spinnerTaskInterval.getModel().getValue().toString())==0){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_3"));
				active = true;
				return false;
			}
			int ret = validateApproverListTable();
			String validateMessage = "";
			switch(ret){
			case routeUserCheck:
				validateMessage = routeUserCheckMsg;
				break;
			case actionEnabledRouteUserCheck:
				validateMessage = actionEnabledRouteUserCheckMsg;
				break;
			case routeUserEscalatorMatch:
				validateMessage = routeUserEscalatorMatchMsg;
				break;
			case duplicateRouteUser:
				validateMessage = duplicateRouteUserMsg;
				break;
			case expirationGreater:
				validateMessage = expirationGreaterMsg;
				break;
			case escalationUserCheck:
				validateMessage = escalationUserCheckMsg;
				break;
			case selectExpirationTime:
				validateMessage = selectExpirationTimeMsg;
			}
			
			if(!(validateMessage.equals("")) && validateMessage.length()>0){
				active = false;				
				VWMessage.showMessage(this, validateMessage, VWMessage.DIALOG_TYPE);
				active = true;
				/**
				 * Issue: Same user updated twice to the task and got saved.
				 * Cause: routeUserListTable.Table.setRowSelectionInterval(selectedRow, selectedRow) throwing exception and was not handled.
				 * Solution: Before setting row selection interval check for number of rows(i.e. rowCount). If rowCount>selectedRow then add row selection interval.
				 * Date: May 10th, 2013. 
				 */
				int rowCount = routeUserListTable.Table.getRowCount();
				if(rowCount > selectedRow)
					routeUserListTable.Table.setRowSelectionInterval(selectedRow, selectedRow);
				return false;
			}

			
			if(rdoApproveByAllUsers.isSelected() && selectedApproverCount == 1){
				String approverName = routeUserListTable.Table.getValueAt(0,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
				if(!approverName.startsWith("<<") && !approverName.endsWith(">>")){
					active = false;
					VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_5"));
					active = true;
					rdoApproveByAnyOne.requestFocus();
					rdoApproveByAnyOne.setSelected(true);
					return false;
				}
			}
			if( (rdoApproveByGivenNumUsers.isSelected() && txtApproverCount.getText().trim().equals("1")) || (rdoApproveByGivenNumUsers.isSelected() && selectedApproverCount==1) ){
				if(routeUserListTable.Table!=null && routeUserListTable.Table.getRowCount() == 1){
					String approverUserName = routeUserListTable.Table.getValueAt(0, routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
					String escalationUserName = routeUserListTable.Table.getValueAt(0, routeUserListTable.Table.m_data.COL_ESCALATION).toString();
					
					if((approverUserName != null && approverUserName.startsWith("<<") && approverUserName.endsWith(">>"))
							|| (escalationUserName != null && escalationUserName.startsWith("<<") && escalationUserName.endsWith(">>"))){
						active = false;
						VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_6"));
						active = true;
						return false;
					}
				}
				
					active = false;
					VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_7"));
					active = true;
					rdoApproveByAnyOne.requestFocus();
					rdoApproveByAnyOne.setSelected(true);
					return false;
			}    	
			if(rdoApproveByGivenNumUsers.isSelected()){
				if(txtApproverCount.getText().trim().equalsIgnoreCase("") ){
					active = false;
					VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_8"), VWMessage.DIALOG_TYPE);
					active = true;
					txtApproverCount.requestFocus();
					return false;
				}
				else if(!txtApproverCount.getText().trim().equals("")){
					int approverCount = 0;
					try{
						approverCount = Integer.parseInt(txtApproverCount.getText());
					}catch(Exception ex){
						active = false;
						VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_9"), VWMessage.DIALOG_TYPE);
						active = true;
						txtApproverCount.requestFocus();
						return false;
					}
					if(selectedApproverCount == 1 && approverCount == 1){
						active = false;
						VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_10")+" " + selectedApproverCount + ".", VWMessage.DIALOG_TYPE);
						active = true;
						rdoApproveByAnyOne.setSelected(true);
						txtApproverCount.setText("");
						return false;
					}
					if(approverCount>=selectedApproverCount){
						active = false;
						VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_11")+" " + selectedApproverCount + ".", VWMessage.DIALOG_TYPE);
						active = true;
						txtApproverCount.requestFocus();
						return false;
					}
					if(approverCount<=0){
						active = false;
						VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_12"), VWMessage.DIALOG_TYPE);
						active = true;
						txtApproverCount.requestFocus();
						return false;
					}
				}
			}
			boolean isTaskNameExist = AdminWise.adminPanel.routePanel.drsDrawing.isTaskNameExist(txtTaskName.getText().trim(), AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId());
			if (isTaskNameExist){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_13")+" " + txtTaskName.getText().trim() +" "+ AdminWise.connectorManager.getString("VWTaskIconProperties.msg_14"), VWMessage.DIALOG_TYPE);
				active = true;
				txtTaskName.requestFocus();
				return false;    		
			}
			int selectedUserAvailableInGroup = isSelectedUserAvailableInGroup();
			if(selectedUserAvailableInGroup == 1){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_15"), VWMessage.DIALOG_TYPE);
				active = true;
				return false;
			}
			else if(selectedUserAvailableInGroup == -2){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_16"), VWMessage.DIALOG_TYPE);
				active = true;
				return false; 
			}
			else if(selectedUserAvailableInGroup == -3){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_17"), VWMessage.DIALOG_TYPE);
				active = true;
				return false; 
			}else if(selectedUserAvailableInGroup == -4){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWTaskIconProperties.msg_18"), VWMessage.DIALOG_TYPE);
				active = true;
				return false; 
			}
		}catch (Exception e) {
			AdminWise.printToConsole("Exception while validating data : "+e.getMessage());
		}

		return true;
	}
	
	public void loadTaskRouteInfo(){
		loadTaskRouteInfo("");
	}

	public void loadTaskRouteInfo(String taskName){
		RouteTaskInfo taskInfo = new RouteTaskInfo();
		viewMode();
		loadTaskNames();
		//System.out.println("routePanel.CURRENT_MODE:"+AdminWise.adminPanel.routePanel.CURRENT_MODE+":"+taskName);
		boolean newTask = true;
		//This is to identify the newly added task on updation of invalid route
		boolean addedTask = false;
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 1){
			//New Route Mode...
			taskInfo = getSelectedTaskInfo();
			try{
				//Desc :Users table is emptied if new task is added(If userlist is empty).
				ArrayList alUserList = null;
				alUserList = taskInfo.getRouteUsersList();
				if(alUserList==null){
					Vector vecEmptyUsers = new Vector();
					//VWApproversListPanel.Table.addData(vecEmptyUsers,1," ");
					//By default insert a row for the table.
					VWApproversListPanel.Table.insertRowOnPlusKeyPress();
					VWApproversListPanel.Table.setRowSelectionInterval(0, 0);
				}	            				            		
			}catch(Exception e){
				//System.out.println("Exception in loadTaskRouteInfo : "+e.getMessage());
				//AdminWise.printToConsole("size is zero "+e.toString());
			}

			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			if(VWRouteConnector.isVWSignInstalled(room.getId())!=1)
				cmbSignatureList.setEnabled(false);
			else
				cmbSignatureList.setEnabled(true);
		}
		else if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2){
			//Display Mode...
			int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
			if( (AdminWise.adminPanel.routePanel.routeInfoBean!=null) ){
				ArrayList routeTaskList = AdminWise.adminPanel.routePanel.routeInfoBean.getRouteTaskList();
				if(routeTaskList!=null && routeTaskList.size()>0){
					for(int count=0;count<routeTaskList.size();count++){
						RouteTaskInfo curTskInfo = (RouteTaskInfo) routeTaskList.get(count);
						if(curTskInfo.getTaskFigureId()==selFigureId){
							taskInfo = curTskInfo;
							newTask = false;
							break;
						}
					}
				}
			}
			cmbSignatureList.setEnabled(false);
		}
		else if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 3 || AdminWise.adminPanel.routePanel.CURRENT_MODE == 4){
			//Update Mode...
			try{
				int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
				if( (AdminWise.adminPanel.routePanel.routeInfoBean!=null) ){
					ArrayList routeTaskList = AdminWise.adminPanel.routePanel.routeInfoBean.getRouteTaskList();
					if(routeTaskList!=null && routeTaskList.size()>0){
						for(int count=0;count<routeTaskList.size();count++){
							RouteTaskInfo curTskInfo = (RouteTaskInfo) routeTaskList.get(count);
							if(curTskInfo.getTaskFigureId()==selFigureId){
								if(curTskInfo.getRouteTaskId()==0){
									addedTask = true;
								}
								taskInfo = curTskInfo;
								newTask = false;
								break;
							}
						}
					}
				}	
				VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
				if(VWRouteConnector.isVWSignInstalled(room.getId())!=1)
					cmbSignatureList.setEnabled(false);
				else
					cmbSignatureList.setEnabled(true);
			}catch (Exception e) {
				AdminWise.printToConsole("Exception in update mode : "+e.getMessage());
				e.printStackTrace();
			}
		}

		txtTaskName.setText(taskInfo.getTaskName());
		taTaskDesc.setText(taskInfo.getTaskDescritpion().trim());
		
		//Escalation taskDuration is removed and added TaskRemainder
		//String taskLength = String.valueOf(taskInfo.getTasklength());
		//txtTaskDuration.setText(taskLength.trim());
		/*if(taskLength.equalsIgnoreCase("0"))
			txtTaskDuration.setText("1");
		else
			txtTaskDuration.setText(taskLength);*/
		
		//Adding Users
		try{
			Vector vecUsers=new Vector();
			VWApproverInfo approverInfo = null;
			RouteUsers rUser = null;	        	
			Vector vecRouteUsers = new Vector();
			ArrayList alRouteUsers = new ArrayList();
			if(!newTask || AdminWise.adminPanel.routePanel.CURRENT_MODE == 1)
				alRouteUsers = taskInfo.getRouteUsersList();
			else{
				alRouteUsers = null;
			}

			for(int count=0; alRouteUsers!=null && count<alRouteUsers.size();count++){
				RouteUsers rUser1 = (RouteUsers) alRouteUsers.get(count);
				vecRouteUsers.add(rUser1);
			}
			String prevGrpName = "";
			for(int count=0;count<vecRouteUsers.size();count++){
				rUser = (RouteUsers) vecRouteUsers.get(count);
				approverInfo = new VWApproverInfo();
				approverInfo.setApproverEmailFlag((rUser.getSendEmail())==0?false:true);
				approverInfo.setApproverActionFlag((rUser.getActionPermission())==0?false:true);
				if(rUser.getUserName().equalsIgnoreCase(rUser.getGroupName())){
					approverInfo.setApproverName(rUser.getUserName());
					approverInfo.setApproverType(Principal.USER_TYPE+"");
				}else{
					approverInfo.setApproverName(rUser.getGroupName());
					approverInfo.setApproverType(Principal.GROUP_TYPE+"");
					if(prevGrpName.equalsIgnoreCase(rUser.getGroupName()))
						continue;
					prevGrpName = rUser.getGroupName();
				}
				approverInfo.setApproverEmail(rUser.getUserEmail());
				approverInfo.setApproverId(""+rUser.getUserId());
				approverInfo.setApproverAction("Reject");
				approverInfo.setApproverTaskDescription("Task desc");
				approverInfo.setApproverTaskLength("2");
				approverInfo.setApproverESignature("E-Sign");
				approverInfo.setApproverTask("Assign Task");
				String expriryPeriodStr = rUser.getExpiryPeriod();
				String spinnerValueStr = getSpinnerString(expriryPeriodStr);
				
				approverInfo.setApproverTaskExpiration(spinnerValueStr);
				approverInfo.setApproverTaskEscalationUserName(rUser.getEUserName());
				approverInfo.setApproverTaskEscalationId(String.valueOf(rUser.getEUserid()));
				approverInfo.setTaskNotify(rUser.getNotifyMail());
				vecUsers.add(approverInfo);
			}
			VWApproversListPanel.Table.addData(vecUsers,1," ");
		}catch(Exception ex){
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
		//END...
		try{
			int rejectedTaskId = taskInfo.getOnRejectTaskId();
			if(rejectedTaskId == -1)
				rdoUserSelectsTask.setSelected(true);
			else if(rejectedTaskId == 0)
				rdoSendToOriginator.setSelected(true);
			else if(rejectedTaskId > 0){
				rdoSelectTask.setSelected(true);
				if(VWStartIconProperties.listTasksModel!=null && VWStartIconProperties.listTasksModel.size()>1){
					String figName = VWStartIconProperties.listTasksModel.get(rejectedTaskId-1).toString().trim();
					cmbTaskList.setSelectedItem(figName);
				}
			}else if(rejectedTaskId == -2){
				rdoSelectTask.setSelected(true);
				cmbTaskList.setSelectedItem("End "+Constants.WORKFLOW_MODULE_NAME);
			}
			
			String approverName = routeUserListTable.Table.getValueAt(0,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
			String escalationName = routeUserListTable.Table.getValueAt(0,routeUserListTable.Table.m_data.COL_ESCALATION).toString();
			if((approverName.startsWith("<<") && approverName.endsWith(">>")) || 
					(escalationName.startsWith("<<") && escalationName.endsWith(">>")) ){
				btnAdd.setEnabled(false);					
			}
			
			int approversCount = taskInfo.getApproversCount();
			txtApproverCount.setText("");
			AdminWise.printToConsole("loadTaskRouteInfo :::: approversCount :: "+approversCount);
			if(approversCount == 99){ //99 is for Approve By All Users when task contains a dynamic Approver. 
				rdoApproveByAllUsers.setSelected(true);
			}

			int actualApproverCount = getSelectedApproversCount();
			if(approversCount  == actualApproverCount && approversCount!=1){
				rdoApproveByAllUsers.setSelected(true);
			}
			else if(approversCount==1){									
					rdoApproveByAnyOne.setSelected(true);				
			}
			else if (approversCount !=1 && approversCount <actualApproverCount){
				rdoApproveByGivenNumUsers.setSelected(true);
				txtApproverCount.setText(""+approversCount);
			}
			//Route enhacement
			int acceptSignatureId = taskInfo.getAcceptSignatureId();
			cmbSignatureList.setSelectedIndex(acceptSignatureId);
			
			//Escalation
			String taskRemainder = taskInfo.getTaskReminder();
		
			if(taskRemainder.length()>0){
				try{
					setRemainderInterval(taskRemainder);
				}catch(Exception ex){
					//log("Ex in spinner control is : "+ex.getMessage());
				}
			}
			int taskReminderEnabled = taskInfo.getTaskReminderEnabled();
			if(taskReminderEnabled==1){
				chkTaskInterval.setSelected(true);
			}else {
				chkTaskInterval.setSelected(false);
			}
			
			if(taskInfo.getAcceptLabel().equalsIgnoreCase("") || taskInfo.getAcceptLabel().equalsIgnoreCase("-"))
				txtAcceptLabel.setText(VWCConstants.DEFAULT_ACCEPT_LABEL);
			else
				txtAcceptLabel.setText(taskInfo.getAcceptLabel().trim());
			
			if(taskInfo.getRejectLabel().equalsIgnoreCase("") || taskInfo.getRejectLabel().equalsIgnoreCase("-"))
				txtRejectLabel.setText(VWCConstants.DEFAULT_REJECT_LABEL);
			else
				txtRejectLabel.setText(taskInfo.getRejectLabel().trim());
			/**CV2019 merges from SIDBI line***/
			AdminWise.printToConsole("newTask :"+newTask);
			if (!newTask) {
				AdminWise.printToConsole("taskInfo.getEmailToPrvTaskUsers() :"+taskInfo.getEmailToPrvTaskUsers());
				if (taskInfo.getEmailToPrvTaskUsers() == 1 && AdminWise.adminPanel.routePanel.sidbiCustomizationFlag == true)
					chkMailToPreviousTaskUsers.setSelected(true);
				else
					chkMailToPreviousTaskUsers.setSelected(false);
			}
			
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,"Ex 2:"+e.toString());
		}
		txtTaskName.setText(taskName);

		/**
		 * For Invalid Task need to disable the fields except the User list table.
		 * Properties has to be enabled for the newly added task in the invalid route.
		 */
		if(addedTask){
			//Send task id as 0 for new task so that
			setDisableInvalidTask("0"); 
		}else{
			setDisableInvalidTask();
		}
	}

	public String getSpinnerString(String expriryPeriodStr) {
		int expiryTime = Integer.parseInt(expriryPeriodStr);
		String expirySpinnerStr = "";
		int day = 0, min=0, hr = 0;
		try{
			if(expiryTime > 60){
				min = expiryTime % 60;
				
				day = (expiryTime /60) / 24;
				hr = (expiryTime /60) % 24;
			}else{
				min = expiryTime;
			}
			expirySpinnerStr = day+"day "+hr+"hr "+min+"min";
		}catch(Exception ex){
			//System.out.println("Exception in getSpinnerString is : "+ex.getMessage());
		}
		return expirySpinnerStr;
	}
	public void btnSave_ActionPerformed() throws UnsupportedEncodingException{
		//CURRENT_MODE = 1 is new route
		int emailPrvTaskUsers = 0;
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE!=3)
		{			
			if(!validateData()){
				return;
			}
			try{
				removeSelectedTaskInfo();
			}catch(Exception e){
				//JOptionPane.showMessageDialog(null,"Exception 0:"+e.toString());
			}
			/**
			 * Code added for Tab issue fix in workflow taskname
			 */
			thisRouteTaskInfo = new RouteTaskInfo();
			String routeTaskName=txtTaskName.getText().trim();
			if(routeTaskName.contains("\t")){
				routeTaskName=routeTaskName.replaceAll("\t"," ");
			}
			thisRouteTaskInfo.setTaskName(routeTaskName);
			AdminWise.adminPanel.routePanel.drsDrawing.setTaskName(routeTaskName);
			thisRouteTaskInfo.setTaskDescritpion(taTaskDesc.getText().trim());
			int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
			thisRouteTaskInfo.setTaskFigureId(selFigureId);
			try{
				//thisRouteTaskInfo.setTasklength(Integer.parseInt(txtTaskDuration.getText()));
				//Escalation enhacement - Task length is removed and introduced interval remainder
				thisRouteTaskInfo.setTasklength(0);
			}catch(Exception exception){
				//JOptionPane.showMessageDialog(null,"Exception 1:"+exception.toString());
			}
			try{
				int rejectedTaskId = 0;
				if(rdoUserSelectsTask.isSelected())
					rejectedTaskId = -1;
				else if(rdoSendToOriginator.isSelected())
					rejectedTaskId = 0;
				else if(rdoSelectTask.isSelected()){
					String selectedFigureName = String.valueOf(cmbTaskList.getSelectedItem());
					//int selectedFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(selectedFigureName);
					rejectedTaskId = getTaskSeqIdForReject(selectedFigureName.trim());
				}
				thisRouteTaskInfo.setOnRejectTaskId(rejectedTaskId);
				int approversCount = 0;
				if(rdoApproveByAllUsers.isSelected()){
					String userOrGrpName = routeUserListTable.Table.getValueAt(0,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
					if((userOrGrpName != null && userOrGrpName.trim().startsWith("<<") && userOrGrpName.trim().endsWith(">>"))){
						approversCount = 99;
					}else{
						approversCount = getSelectedApproversCount();
					}
				}else if(rdoApproveByAnyOne.isSelected())
					approversCount = 1;
				else if(rdoApproveByGivenNumUsers.isSelected())
					approversCount = Integer.parseInt(txtApproverCount.getText());
				
				thisRouteTaskInfo.setApproversCount(approversCount);
				//Route enhancement
				thisRouteTaskInfo.setAcceptSignatureId(cmbSignatureList.getSelectedIndex());

				thisRouteTaskInfo.setRouteId(0);
				thisRouteTaskInfo.setRouteTaskId(0);
				
				//Escalation
				int taskReminderTime = getMinutesForReminderInterval(spinnerTaskInterval.getModel().getValue().toString());
				
				String taskReminderTimeStr = String.valueOf(taskReminderTime);
				thisRouteTaskInfo.setTaskReminder(taskReminderTimeStr);
				
				thisRouteTaskInfo.setTaskReminderEnabled((chkTaskInterval.isSelected()==true?1:0));
				
			}catch(Exception e){
				//log("Exception is "+e.getMessage());
				//JOptionPane.showMessageDialog(null,"Ex 1:"+e.toString());
			}

			try{
				ArrayList aListRouteUsers = new ArrayList();
				for(int count=0;count<routeUserListTable.Table.getRowCount();count++){
					VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(count);
					
					String userOrGrpName = routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
					String userMail = routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERMAILID).toString();
					int usrGrpType = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_APPROVERTYPE).toString());
					int userId = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_APPROVERID).toString());
					
					//Escalation
					String expiryPeriod = routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_EXPIRATION).toString();
					String escalationUserName= routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_ESCALATION).toString();
					String notifyMail = routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_NOTIFY).toString();
					int escalationUserId = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_ESCALATION_USER_ID).toString());
					
					
					if(usrGrpType ==  Principal.GROUP_TYPE){// group
						Vector principals = new Vector();
						principals = VWRouteConnector.getDRSPrincipals(1, userOrGrpName);
						if(principals!=null && principals.size()>0){
							for(int i = 0; i< principals.size(); i++){
								Principal members =  (Principal)principals.get(i);
								RouteUsers routeUsers = new RouteUsers();
								routeUsers.setRouteId(1);
								routeUsers.setGroupName(userOrGrpName);
								routeUsers.setUserName(members.getName());
								if(vwApproverInfo.getApproverActionFlag())
									routeUsers.setActionPermission(1);
								else
									routeUsers.setActionPermission(0);
								if(vwApproverInfo.getApproverEmailFlag())
									routeUsers.setSendEmail(1);
								else
									routeUsers.setSendEmail(0);
								routeUsers.setUserEmail(members.getEmail());
								routeUsers.setUserId(members.getUserGroupId());
								//Escalation
								int expiryPeriodTime = getTimeInMinutes(expiryPeriod);
								String expiryPeriodStr = String.valueOf(expiryPeriodTime);
								routeUsers.setExpiryPeriod(expiryPeriodStr);
								routeUsers.setEUserName(escalationUserName);
								routeUsers.setNotifyMail(notifyMail);
								routeUsers.setEUserid(escalationUserId);
								
								aListRouteUsers.add(routeUsers);
							}
						}
					}else{// User
						RouteUsers routeUsers = new RouteUsers();
						routeUsers.setRouteId(1);
						routeUsers.setGroupName(userOrGrpName);
						routeUsers.setUserName(userOrGrpName);

						if(vwApproverInfo.getApproverActionFlag())
							routeUsers.setActionPermission(1);
						else
							routeUsers.setActionPermission(0);
						if(vwApproverInfo.getApproverEmailFlag())
							routeUsers.setSendEmail(1);
						else
							routeUsers.setSendEmail(0);
						routeUsers.setUserEmail(userMail);
						routeUsers.setUserId(userId);
						
						//Escalation
						int expiryPeriodTime = getTimeInMinutes(expiryPeriod);
						String expiryPeriodStr = String.valueOf(expiryPeriodTime);
						routeUsers.setExpiryPeriod(expiryPeriodStr);
						routeUsers.setEUserName(escalationUserName);
						routeUsers.setNotifyMail(notifyMail);
						routeUsers.setEUserid(escalationUserId);
						
						aListRouteUsers.add(routeUsers);
					}
				}
				thisRouteTaskInfo.setRouteUsersList(aListRouteUsers);
				thisRouteTaskInfo.setAcceptLabel(txtAcceptLabel.getText().trim());
				thisRouteTaskInfo.setRejectLabel(txtRejectLabel.getText().trim());
				/**CV2019 merges from SIDBI line***/
				emailPrvTaskUsers = chkMailToPreviousTaskUsers.isSelected()==true?1:0;
				thisRouteTaskInfo.setEmailToPrvTaskUsers(emailPrvTaskUsers);
				
				AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(thisRouteTaskInfo);
			}catch(Exception e){
				//log("btn save exception is : "+e.getMessage());
			}		       							
		}
		else{
			boolean existing = false;
			try{
				if(!validateData()){
					return;
				}
				existing = updateSelectedTaskInfo();
			}catch(Exception e){
				//JOptionPane.showMessageDialog(null,"Exception 0:"+e.toString());
				AdminWise.printToConsole("Exception in VWTaskIconProperties ::  "+e.getMessage());								
			}

			//Update mode add new tasks.
			if(!existing){

				thisRouteTaskInfo = new RouteTaskInfo();
				thisRouteTaskInfo.setTaskName(txtTaskName.getText());
				thisRouteTaskInfo.setTaskDescritpion(taTaskDesc.getText().trim());
				int selFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getSelectedFigureId();
				thisRouteTaskInfo.setTaskFigureId(selFigureId);
				try{
					thisRouteTaskInfo.setTasklength(Integer.parseInt(txtTaskDuration.getText()));
				}catch(Exception exception){
					//JOptionPane.showMessageDialog(null,"Exception 1:"+exception.toString());
				}

				try{
					int rejectedTaskId = 0;
					if(rdoUserSelectsTask.isSelected())
						rejectedTaskId = -1;
					else if(rdoSendToOriginator.isSelected())
						rejectedTaskId = 0;
					else if(rdoSelectTask.isSelected()){
						String selectedFigureName = String.valueOf(cmbTaskList.getSelectedItem());									
						//int selectedFigureId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(selectedFigureName);
						rejectedTaskId = getTaskSeqIdForReject(selectedFigureName);
					}
					thisRouteTaskInfo.setOnRejectTaskId(rejectedTaskId);

					int approversCount = 0;
					if(rdoApproveByAllUsers.isSelected()){
						String userOrGrpName = routeUserListTable.Table.getValueAt(0,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
						if((userOrGrpName != null && userOrGrpName.trim().startsWith("<<") && userOrGrpName.trim().endsWith(">>"))){
							approversCount = 99;
						}else{
							approversCount = getSelectedApproversCount();
						}					
					}else if(rdoApproveByAnyOne.isSelected())
						approversCount = 1;
					else if(rdoApproveByGivenNumUsers.isSelected())
						approversCount = Integer.parseInt(txtApproverCount.getText());

					thisRouteTaskInfo.setApproversCount(approversCount);
					//Route enhancement
					thisRouteTaskInfo.setAcceptSignatureId(cmbSignatureList.getSelectedIndex());

					thisRouteTaskInfo.setRouteId(0);
					thisRouteTaskInfo.setRouteTaskId(0);
					thisRouteTaskInfo.setTaskSequence(100);
					//thisRouteTaskInfo.setTaskReminder(spinnerTaskInterval.getValue().toString());
					String taskReminder = spinnerTaskInterval.getModel().getValue().toString();
					int taskReminderTime = getMinutesForReminderInterval(taskReminder);
					String taskReminderTimeStr = String.valueOf(taskReminderTime);
					thisRouteTaskInfo.setTaskReminder(taskReminderTimeStr);
					
					thisRouteTaskInfo.setTaskReminderEnabled((chkTaskInterval.isSelected()==true?1:0));
				}catch(Exception e){
					//JOptionPane.showMessageDialog(null,"Ex 1:"+e.toString());
				}

				try{
					ArrayList aListRouteUsers = new ArrayList();
					for(int count=0;count<routeUserListTable.Table.getRowCount();count++){
						VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(count);
						RouteUsers routeUsers = new RouteUsers();
						String userMail = routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERMAILID).toString();
						routeUsers.setRouteId(1);
						routeUsers.setGroupName(routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString());
						routeUsers.setUserName(routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString());
						
						//Escalation
						String expiryPeriod = routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_EXPIRATION).toString();
						String escalationUserName= routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_ESCALATION).toString();
						String notifyMail = routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_NOTIFY).toString();
						int escalationUserId = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_ESCALATION_USER_ID).toString());
						
						if(vwApproverInfo.getApproverActionFlag())
							routeUsers.setActionPermission(1);
						else
							routeUsers.setActionPermission(0);
						if(vwApproverInfo.getApproverEmailFlag())
							routeUsers.setSendEmail(1);
						else
							routeUsers.setSendEmail(0);
						routeUsers.setUserEmail(userMail);
						routeUsers.setUserId(Integer.parseInt(vwApproverInfo.getApproverId()));
						
						//Escalation
						int expiryPeriodTime = getTimeInMinutes(expiryPeriod);
						String expirtyPeriodStr = String.valueOf(expiryPeriodTime);
						routeUsers.setExpiryPeriod(expirtyPeriodStr);
						routeUsers.setEUserName(escalationUserName);
						routeUsers.setNotifyMail(notifyMail);
						routeUsers.setEUserid(escalationUserId);
						
						aListRouteUsers.add(routeUsers);
					}
					thisRouteTaskInfo.setRouteUsersList(aListRouteUsers);	
					thisRouteTaskInfo.setAcceptLabel(txtAcceptLabel.getText());
					thisRouteTaskInfo.setRejectLabel(txtRejectLabel.getText());
					/**CV2019 merges from SIDBI line***/
					emailPrvTaskUsers = chkMailToPreviousTaskUsers.isSelected()==true?1:0;
					thisRouteTaskInfo.setEmailToPrvTaskUsers(emailPrvTaskUsers);
					AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(thisRouteTaskInfo);
				}catch(Exception e){
					//JOptionPane.showMessageDialog(null,"Exception 2:"+e.toString());
				}

				//End....
			}
		}
		setVisible(false);
		dispose();
	}

	private int getTaskSeqIdForReject(String taskName){
		int rejectTaskSeq = -1;
		if(taskName.equalsIgnoreCase("End "+ Constants.WORKFLOW_MODULE_NAME)){
			rejectTaskSeq = -2;
		}else if(VWStartIconProperties.listTasksModel!=null && VWStartIconProperties.listTasksModel.size()>1){ 
			for(int i = 0; i<VWStartIconProperties.listTasksModel.size(); i++){
				if(taskName.equals(VWStartIconProperties.listTasksModel.get(i).toString().trim()) )
				{
					rejectTaskSeq = i+1;
					break;
				}
			}
		}else{
			rejectTaskSeq =  -1;
		}
		return rejectTaskSeq;
	}

	public void btnCancel_ActionPerformed(){
		setVisible(false);
		dispose();				
	}
	class SymAction implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(ae.getSource() == btnDone){
				try {
					btnSave_ActionPerformed();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(ae.getSource() == btnCancel){
				btnCancel_ActionPerformed();
			}

			else if(ae.getSource()==rdoApproveByGivenNumUsers){
				txtApproverCount.setEnabled(true);
				txtApproverCount.setText("");
			}
			else if(ae.getSource()==rdoApproveByAnyOne){
				txtApproverCount.setEnabled(false);
				txtApproverCount.setText("");
			}
			else if(ae.getSource()==rdoApproveByAllUsers){
				txtApproverCount.setEnabled(false);
				txtApproverCount.setText("");
			}
			else if(ae.getSource()==btnAdd){
				AdminWise.adminPanel.routePanel.routeUserListTable.Table.insertRowOnPlusKeyPress();
			}
			else if(ae.getSource()==btnRemove){
				btnAdd.setEnabled(true);
				AdminWise.adminPanel.routePanel.routeUserListTable.Table.deleteSelectedRow();
			}
		}
	}
	public void drawRejectConnections(String srcFigureName, int srcFigId, String distFigName, int destFigId){
		FigureEnumeration fEnum1 = AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().figures();
		Hashtable figuresList = new Hashtable();
		VWFigure startFig = null;
		VWFigure endFig  = null;
		System.out.println("coming in drawRejectConnections");
		System.out.println("srcFigureName" + srcFigureName+" distFigName "+ distFigName);
		//if (listTasksModel != null && listTasksModel.size() > 0)
		{
			Figure srcFigure = null;
			Figure destFigure = null;
			while(fEnum1.hasNextFigure()){
				try{
					Figure curFigure = fEnum1.nextFigure();
					VWFigure curVWFigure = (VWFigure)curFigure;
					String figName = curVWFigure.getFigureName();

					if(figName.equalsIgnoreCase(srcFigureName) && curVWFigure.getFigureId() == srcFigId){
						startFig = curVWFigure;
					}
					if(figName.equalsIgnoreCase(distFigName)&& curVWFigure.getFigureId() == destFigId){
						destFigure = curFigure;
					}
				}catch(Exception ex){
					//System.out.println("Error 1 ********************** " + ex.getMessage());
					ex.printStackTrace();
				}
			}
			try{	
				if(srcFigure!=null && destFigure!=null){
					Point originPoint = srcFigure.center();
					Point destinationPoint = destFigure.center();
					Connector startConnector1 = findConnectorAt(originPoint.x , originPoint.y, AdminWise.adminPanel.routePanel.drsDrawing.drawing());
					//srcFigure.connectorAt(originPoint.x , originPoint.y);	        
					Connector endConnector1 = findConnectorAt(destinationPoint.x , destinationPoint.y, AdminWise.adminPanel.routePanel.drsDrawing.drawing());
					//destFigure.connectorAt(destinationPoint.x , destinationPoint.y);
					VWLineConnection startFigure = new VWLineConnection();	        	
					startFigure.connectStart(startConnector1);
					startFigure.connectEnd(endConnector1);
					startFigure.startPoint(originPoint.x +15, originPoint.y);
					startFigure.endPoint(destinationPoint.x+15 , destinationPoint.y);
					startFigure.updateConnection();        	
					AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(startFigure);
				}
			}catch(Exception ex){
				System.out.println("Error 2 ********************** " + ex.getMessage());
				ex.printStackTrace();
			}

		}

	}
	protected static Connector findConnectorAt(int x, int y, Drawing drawing) {
		FigureEnumeration fe = drawing.figuresReverse();
		while (fe.hasNextFigure()) {
			Figure figure = fe.nextFigure();
			if (figure.canConnect() && figure.containsPoint(x, y)) {
				return figure.connectorAt(x, y);
			}
		}
		return null;
	}
	public boolean isTaskLengthNumeric(String taskLength){
		boolean isValid = false;
		int taskLen =0;
		try{
			taskLen = Integer.parseInt(taskLength);

			if( (taskLen>=1) && (taskLen<=180) ){
				isValid = true;
			}
			else{
				isValid = false;
			}
		}catch(NumberFormatException nfe){
			//JOptionPane.showMessageDialog(null,nfe.toString());
			return isValid;

		}		
		return isValid;
	}

//	----------------------------------------------------------------------------------------------------------------
	/**
	 * Desc   :This method returns the selected approvers count (count = usercount(action enabled)  + users in group count(action enabled))
	 * Author :Nishad Nambiar
	 * Date   :27-Mar-2008
	 */
	public int getSelectedApproversCount(){
		int approverCount =0;
		try{
			for(int count=0;(routeUserListTable.Table!=null)&&count<routeUserListTable.Table.getRowCount();count++){
				VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(count);
				if(vwApproverInfo.getApproverActionFlag()){
					String userOrGrpName = routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
					int usrGrpType = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_APPROVERTYPE).toString());
					if(usrGrpType == Principal.GROUP_TYPE){// group
						Vector principals = new Vector();
						principals = VWRouteConnector.getDRSPrincipals(1, userOrGrpName);
						//AdminWise.gConnector.ViewWiseClient.getDRSPrincipals(roomId, 1, UserOrGrpName, principals);
						if(principals!=null && principals.size()>0){
							approverCount += principals.size();
						}
					}else{// User
						approverCount++;
					}
				}
			}
			//eND 
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,"Exception 2:"+e.toString());
		}		       			
		return approverCount;				
	}
//	----------------------------------------------------------------------------------------------------------------

	/**
	 * Desc   :This method returns the HashSet which contains selected approvers names.
	 * Author :Nishad Nambiar
	 * Date   :28-Mar-2008
	 */

	public HashSet getSelectedUsers(){
		HashSet hsUsers = new HashSet();		
		try{
			for(int count=0;count<routeUserListTable.Table.getRowCount();count++){
				String userOrGrpName = routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
				int usrGrpType = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_APPROVERTYPE).toString());
				if(usrGrpType ==  Principal.USER_TYPE){// User
					boolean added = hsUsers.add(userOrGrpName);
					if(!added)
						hsUsers = null;

				}
			}
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,"Exception 2:"+e.toString());
		}	
		return hsUsers;
	}

//	----------------------------------------------------------------------------------------------------------------

	/**
	 * Desc   :This method returns true if any of the selected user is available in any of the selected grtoup, false otherwise.
	 * Author :Nishad Nambiar
	 * Date   :28-Mar-2008
	 */

	public int isSelectedUserAvailableInGroup(){
		int userAvailableInGroup = -1;
		HashSet hsUsers = getSelectedUsers();
		try{
			if(hsUsers == null)
				userAvailableInGroup = -3;//Duplicate user name
			else{
				for(int count=0;count<routeUserListTable.Table.getRowCount();count++){
					VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(count);
					String userOrGrpName = routeUserListTable.Table.getValueAt(count,routeUserListTable.Table.m_data.COL_APPROVERNAME).toString();
					int usrGrpType = Integer.parseInt(routeUserListTable.Table.getValueAt(count, routeUserListTable.Table.m_data.COL_APPROVERTYPE).toString());
					//AdminWise.printToConsole(" userOrGrpName "+userOrGrpName+" usrGrpType "+usrGrpType);
					if(usrGrpType ==  Principal.GROUP_TYPE){// group
						Vector principals = new Vector();
						principals = VWRouteConnector.getDRSPrincipals(1, userOrGrpName);
						if(principals!=null && principals.size()>0){
							for(int i = 0; i< principals.size(); i++){
								Principal members =  (Principal)principals.get(i);
								String curUser = members.getName();
								if(!hsUsers.add(curUser.trim())){
									userAvailableInGroup = 1;
									break;
								}
							}
						}
						else if(principals == null || (principals!=null && principals.size() == 0)){
							userAvailableInGroup = -2; //one of the group is not having members
							break;
						}
					}
				}
			}
		}catch(Exception e){
			userAvailableInGroup = -4;
		}		       			
		return userAvailableInGroup;				
	}

	//Set the current location where this dialog box should be loaded.
	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width, this.getSize().height);
		setLocation(point);
	}

	//For Invalid Route
	public void setDisableInvalidTask(String taskId){

		boolean flag = true;//Default it is valid task

		//For Invalid Route need to disable the fields except the User list table.
		if(AdminWise.adminPanel.routePanel.routeValidity==INVALIDROUTE){ 
			flag = false;
		}else{//For Valid Route
			flag = true;
		}

		if(taskId.equalsIgnoreCase("0")){//For newly added task
			flag = true;
		}

		txtTaskName.setEnabled(flag);
		taTaskDesc.setEnabled(flag);
		txtTaskDuration.setEnabled(flag);

		rdoApproveByAllUsers.setEnabled(flag);
		rdoApproveByAnyOne.setEnabled(flag);
		rdoApproveByGivenNumUsers.setEnabled(flag);
		//ApproverCount is enable based on selection of approver condition
		if(!flag)
			txtApproverCount.setEnabled(flag);
		cmbSignatureList.setEnabled(flag);

		rdoUserSelectsTask.setEnabled(flag);
		rdoSendToOriginator.setEnabled(flag);
		rdoSelectTask.setEnabled(flag);
		//Task list is enable based on selection of rejection condition
		if(!flag)
			cmbTaskList.setEnabled(flag);
	}

	public void setDisableInvalidTask(){
		setDisableInvalidTask("");
	}
	/*public void log(String info){
		System.out.println(info);
	}*/

	public String getIntervalRemainder(String str){
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		String spinnerValue = "";
		try{
			SimpleDateFormat timeFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			date = timeFormat.parse(str);
			calendar.setTime(date);
			spinnerValue = calendar.get(Calendar.DAY_OF_MONTH)+"day "+calendar.get(calendar.HOUR_OF_DAY)+"hr "+calendar.get(calendar.MINUTE)+"min";
		}catch (Exception e) {
		}
		return spinnerValue;
	}
//	----------------------------------------------------------------------------------------------------------------
	public int validateApproverListTable() {
		int ret = 0;
		try{
			
			//Check For Empty User and ActionEnabled user
			if(routeUserListTable.Table!=null && routeUserListTable.Table.getRowCount()==0){
				ret = routeUserCheck;
			}else if(routeUserListTable.Table!=null && routeUserListTable.Table.getRowCount()>0){
				int selectedApproverCount = getSelectedApproversCount();
				if(selectedApproverCount == 0){
					ret = actionEnabledRouteUserCheck;
				}else{
					try{
						int userGrpId = 0;
						int escalatorId = 0;
						ArrayList<Integer> routeUserGrpIdList = new ArrayList<Integer>();
						
						String remainderInterval = spinnerTaskInterval.getModel().getValue().toString();
						
						for(int index=0; (routeUserListTable.Table!=null)&&index<routeUserListTable.Table.getRowCount();index++){
							VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(index);
							
							boolean approverType = vwApproverInfo.getApproverActionFlag();
							
							if(vwApproverInfo.getApproverId()!=null){
								userGrpId = Integer.parseInt(vwApproverInfo.getApproverId());
							}
							if(vwApproverInfo.getApproverTaskEscalationId()!=null){
								escalatorId = Integer.parseInt(vwApproverInfo.getApproverTaskEscalationId());
							}
							
							if(!approverType){
								//Dummy user should not have expiry and none has to be selected.
								routeUserListTable.Table.setValueAt(String.valueOf(-1), index, routeUserListTable.Table.m_data.COL_ESCALATION_USER_ID);
								routeUserListTable.Table.setValueAt("<None>", index, routeUserListTable.Table.m_data.COL_ESCALATION);
								routeUserListTable.Table.setValueAt("00day 00hr 00min", index, routeUserListTable.Table.m_data.COL_EXPIRATION);
							}else{
								//For approvedUser mail id check has to be there
								if(vwApproverInfo.getApproverType().equals("2") && !vwApproverInfo.getApproverName().startsWith("<<") && !vwApproverInfo.getApproverName().endsWith(">>")){//For user and not for group
									if(vwApproverInfo.getApproverEmail()==null || vwApproverInfo.getApproverEmail().trim().equals("") || vwApproverInfo.getApproverEmail().trim().equals("-")){					
										//No Email id.
											vwApproverInfo.setApproverEmailFlag(false);	
									}
								}
							}
							
							//------------
							String approverName = vwApproverInfo.getApproverName();
							String escalatorName = vwApproverInfo.getApproverTaskEscalationUserName();
							if(userGrpId == 0){
								ret = routeUserCheck;//Select Route User
								break;
							}else if((approverName != null && !approverName.startsWith("<<") && !approverName.endsWith(">>"))
									&& (escalatorName != null && !escalatorName.startsWith("<<") && !escalatorName.endsWith(">>"))){
								if(approverType && userGrpId == escalatorId && vwApproverInfo.getApproverType().equals(String.valueOf(Principal.USER_TYPE))){
									ret = routeUserEscalatorMatch;//Approver user id and escalation users are same
									selectedRow = index;
									break;
								}else if(approverType && userGrpId > 0 && vwApproverInfo.getApproverType().equals(String.valueOf(Principal.GROUP_TYPE))){
									//Group got selected in route user column, so check for all users in the group with the escalator user Id
									Vector principals = new Vector();
									principals = VWRouteConnector.getDRSPrincipals(1, vwApproverInfo.getApproverName());
									ArrayList<Integer> GroupUsersIdList = new ArrayList<Integer>();

									if(principals!=null && principals.size()>0){
										for(int i = 0; i< principals.size(); i++){
											Principal members =  (Principal)principals.get(i);
											//users += members.getName()+", ";
											GroupUsersIdList.add(members.getUserGroupId());
										}
									}
									if(GroupUsersIdList!=null && GroupUsersIdList.contains(escalatorId)){
										ret = routeUserEscalatorMatch;
										selectedRow = index;
										break;
									}
								}
							}
							
							if(routeUserGrpIdList.contains(userGrpId)){
								//Duplicate Route user id
								ret = duplicateRouteUser;// Duplicate Route user id
								break;
							}else{
								routeUserGrpIdList.add(userGrpId);
							}

							int expiryTime = getTimeInMinutes(vwApproverInfo.getApproverTaskExpiration());
							int remainderTime = getMinutesForReminderInterval(remainderInterval);

							if(chkTaskInterval.isSelected() && escalatorId!=-1){//<none> -1
								//ExpiryTime has to be greater than the Remainder Interval
								//System.out.println("\nexpiryTime : "+expiryTime+", remainderInterval : "+remainderTime+"\n");
								if(approverType && expiryTime <= remainderTime){
									ret = expirationGreater;
									selectedRow = index;
									break;
								}
							}
							if(approverType && chkTaskInterval.isSelected() && expiryTime > 0 && escalatorId == -1){
								ret = escalationUserCheck;
								selectedRow = index;
								break;
							}
							if(approverType && (escalatorId>0 || escalatorId==-2 || escalatorId == -3) && expiryTime == 0){
								ret = selectExpirationTime;
								selectedRow = index;
								break;
							}
						}
					}catch(Exception ex){
						System.out.println("Exception in validateApproverListTable for all posibilities : "+ex.getMessage());
					}
				}
			}
		}catch(Exception ex){
			System.out.println("Exception in validateApproverListTable : "+ex.getMessage());
			AdminWise.printToConsole("Exception in validateApproverListTable : "+ex.getMessage());
		}
		return ret;
	}
	
	
	
	public int getMinutesForReminderInterval(String remainderInterval) {
		// Thu Jan 01 05:06:03 IST 1970 = 05:06:03
		// 01234567890123456789 
		int ret = 0;
		try{
			String tempStr = remainderInterval;
			
			String hr   = tempStr.substring(11, 13);//05
			String min   = tempStr.substring(14, 16); //06
			String day   = tempStr.substring(17, 19);//03
			
			int dayInt = Integer.parseInt(day);
			int hrInt = Integer.parseInt(hr);
			int minInt = Integer.parseInt(min);
			
			ret = (dayInt * 24 * 60) + (hrInt * 60) + minInt;
			
		}catch(Exception ex){
			//System.out.println("Exception in getMinutesForRemainderInterval : "+ex.getMessage());
		}
		return ret;
	}
	public int getTimeInMinutes(String dtValue) {
		//1day 0hr 20min
		Date dateObj = new Date();
		int ret = 0;
		try{
			if(dtValue.length()>0){
				
				String day = dtValue.substring(0, dtValue.indexOf("day"));
				String hr   = dtValue.substring(dtValue.indexOf("day") + 4, dtValue.indexOf("hr") );
				String min  = dtValue.substring(dtValue.indexOf("hr") + 3, dtValue.indexOf("min") );

				int dayInt = Integer.parseInt(day);
				int hrInt = Integer.parseInt(hr);
				int minInt = Integer.parseInt(min);
				
				ret = (dayInt * 24 * 60) + (hrInt * 60) + minInt;
			}

		}catch(Exception ex){
			//System.out.println("Exception in getTimeInMinutes : "+ex.getMessage());
		}
		return ret;
	}
	public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
			/**
			 * Reading the registry for setting the fontname and fontsize for diagram
			 * implemented for chinese localization
			 * Modified by Madhavan
			 */
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
	        	fontName = VWCPreferences.getFontName();
	            System.out.println("Font Name  inside DRSDrawing: " + fontName + " : " + VWCPreferences.getFontSize());            
	            setUIFont(new FontUIResource(fontName, Font.PLAIN, VWCPreferences.getFontSize()));
		}
		catch(Exception se){
		}
		new VWImages();
		JFrame frame = new JFrame();
		VWTaskIconProperties setting = new VWTaskIconProperties(1);
		setting.setVisible(true);
		//frame.add(setting);
		
		
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.pack();
		
		
	}
	 public static void setUIFont(javax.swing.plaf.FontUIResource f) {
	    	java.util.Enumeration keys = UIManager.getDefaults().keys();
	    	while (keys.hasMoreElements()) {
	    	    Object key = keys.nextElement();
	    	    Object value = UIManager.get(key);
	    	    
	    	    if (value instanceof javax.swing.plaf.FontUIResource){		
	    		UIManager.put(key, f);
	    	    }
	    	}
	        }
}
