/**@author apurba.m
 * 
 * This class is added for getting custom indices for Recycled Documents Panel, Audit Trail Panel and
 * Retention Panel
 */
package ViewWise.AdminWise.VWRoute;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.VWSLog;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWAuditTrail.VWATConnector;
import ViewWise.AdminWise.VWAuditTrail.VWATTable;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRecycle.VWRecycleConnector;
import ViewWise.AdminWise.VWRecycle.VWRecycleTable;
import ViewWise.AdminWise.VWRetention.VWRetentionConnector;
import ViewWise.AdminWise.VWRetention.VWRetentionTable;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;

public class VWAddCustomIndices extends javax.swing.JDialog implements
VWConstant{
	boolean cancelFlag = true;
	boolean loadedList = true;
	boolean frameSizeAdjusted = false;

	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	VWCheckList LstIndices = new VWCheckList();
	VWLinkedButton BtnAdd = new VWLinkedButton(0, true);
	VWLinkedButton BtnReset = new VWLinkedButton(0, true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0, true);
	String selectedIndexName = "";
	public Session session = null;
	private static int gCurRoom = 0;
	int sid=0;
	String userName = "";
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	String adminPanelName = "";
	List globalDataList=null;
	Vector globalATData=null;
	Vector globalRetentionList =null;
	Vector customIndexList= new Vector();
	Vector newCustomIndexList=new Vector();
	VWRecycleTable TableGlobal = new VWRecycleTable();
	VWATTable TableAtGlobal = new VWATTable();
	VWRetentionTable TableRetentionGlobal = new VWRetentionTable();
	int lastRecordIDGlobal;
	int actionTypeGlobal;
	int rowCountGlobal;
	int globalRetentionId;
	Vector filterGlobal= new Vector();
/**
 * Added for Recycle Panel
 * @param parent
 * @param selectedIndexName
 * @param panelName
 * @param data
 * @param lastRecID
 * @param actionType
 * @param rowCount
 * @param Table
 * @param finalCustomList
 */
	public VWAddCustomIndices(Frame parent, String selectedIndexName, String panelName, List data,int lastRecID,int actionType,int rowCount, VWRecycleTable Table, Vector finalCustomList, JDialog processDialog) {
		super(parent);
		AdminWise.printToConsole("VWAddCustomIndices() For RD!!!!");
		processDialog.setVisible(false);
		processDialog.dispose();
		gCurRoom= room.getId();
		session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
		userName= session.user;
		sid= session.sessionId;
		if(panelName.equalsIgnoreCase("RD")){
			adminPanelName = "2";
			globalDataList= data;
			lastRecordIDGlobal= lastRecID;
			actionTypeGlobal= actionType;
			rowCountGlobal= rowCount;
			TableGlobal = Table;
//			for(int i=0; i<globalDataList.size(); i++){
//				AdminWise.printToConsole("globalDataList.get("+i+") VWAddCustomIndices ::::"+globalDataList.get(i).toString());
//			}
		}
		
		getContentPane().setBackground(AdminWise.getAWColor());
		setTitle(LBL_ADDINDEX_NAME);
		setModal(true);
		getContentPane().setLayout(null);
		setLocation(489, 159);
		setSize(283, 438);
		setVisible(false);

		//Dynamic user index starts with < and ends with >
		if(selectedIndexName.startsWith("<<") && selectedIndexName.endsWith(">>")){
			this.selectedIndexName = selectedIndexName.substring(2, selectedIndexName.length() - 2);
		}

		JLabel1.setText(LBL_LISTINDEX_NAME);
		getContentPane().add(JLabel1);
		JLabel1.setBounds(6, 4, 194, AdminWise.gBtnHeight);

		getContentPane().add(LstIndices);
		LstIndices.setBounds(4, 28, 276, 366);

		BtnAdd.setText(BTN_ADDINDEX_NAME);
		getContentPane().add(BtnAdd);
		BtnAdd.setBounds(6, 410, 105, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnAdd.setIcon(VWImages.OkIcon);
		
		BtnReset.setText(BTN_RESET_NAME);
		getContentPane().add(BtnReset);
		BtnReset.setBounds(118, 410, 75, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnReset.setIcon(VWImages.reset);

		BtnCancel.setText(BTN_CANCEL_NAME);
		getContentPane().add(BtnCancel);
		BtnCancel.setBounds(198, 410, 75, AdminWise.gBtnHeight);
		// BtnCancel.setBackground(java.awt.Color.white);
		BtnCancel.setIcon(VWImages.CloseIcon);

		Vector textIndices = null;
		try {
			loadedList = false;
			textIndices = getIndices();
			AdminWise.printToConsole("textIndices RD ::::"+textIndices);
			LstIndices.loadData(textIndices);
			loadedList = true;
		} catch (Exception e) {
			loadedList = true;
		}
		if(finalCustomList == null){
			AdminWise.printToConsole("finalCustomList is Null Inside Load Indices of RD ::::"+finalCustomList);
			LstIndices.loadData(textIndices);
		} else{
			AdminWise.printToConsole("finalCustomList is not Null Inside Load Indices of RD ::::"+finalCustomList);
			LstIndices.setCheckItems(finalCustomList);
		}
		int itemIndex = getSelectedItemIndex(textIndices);
		if(itemIndex != -1)
			LstIndices.setItemCheck(itemIndex, true);

		SymAction lSymAction = new SymAction();
		BtnAdd.addActionListener(lSymAction);
		BtnCancel.addActionListener(lSymAction);
		BtnReset.addActionListener(lSymAction);

		SymKey aSymKey = new SymKey();
		addKeyListener(aSymKey);
		LstIndices.addKeyListener(aSymKey);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		setResizable(false);

//		setCurrentLocation();
		setVisible(true);
	}

	/**
	 * Added for AT Panel
	 * @param parent
	 * @param selectedIndexName
	 * @param panelName
	 * @param data
	 * @param Filter
	 * @param lastRecID
	 * @param Table
	 * @param finalCustomList
	 */
	public VWAddCustomIndices(Frame parent, String selectedIndexName, String panelName, Vector data,Vector Filter,int lastRecID,VWATTable Table,  Vector finalCustomList, JDialog processDialog) {
		super(parent);
		AdminWise.printToConsole("VWAddCustomIndices() For AT!!!!");
		processDialog.setVisible(false);
		processDialog.dispose();
		gCurRoom= room.getId();
		session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
		userName= session.user;
		sid= session.sessionId;
		if(panelName.equalsIgnoreCase("AT")){
			adminPanelName = "3";
			globalATData=data;
			filterGlobal=Filter;
			lastRecordIDGlobal=lastRecID;
			TableAtGlobal=Table;
		}
		
		getContentPane().setBackground(AdminWise.getAWColor());
		setTitle(LBL_ADDINDEX_NAME);
		setModal(true);
		getContentPane().setLayout(null);
		setLocation(489, 159);
		setSize(283, 438);
		setVisible(false);

		//Dynamic user index starts with < and ends with >
		if(selectedIndexName.startsWith("<<") && selectedIndexName.endsWith(">>")){
			this.selectedIndexName = selectedIndexName.substring(2, selectedIndexName.length() - 2);
		}

		JLabel1.setText(LBL_LISTINDEX_NAME);
		getContentPane().add(JLabel1);
		JLabel1.setBounds(6, 4, 194, AdminWise.gBtnHeight);

		getContentPane().add(LstIndices);
		LstIndices.setBounds(4, 28, 276, 366);

		BtnAdd.setText(BTN_ADDINDEX_NAME);
		getContentPane().add(BtnAdd);
		BtnAdd.setBounds(6, 410, 105, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnAdd.setIcon(VWImages.OkIcon);
		
		BtnReset.setText(BTN_RESET_NAME);
		getContentPane().add(BtnReset);
		BtnReset.setBounds(118, 410, 75, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnReset.setIcon(VWImages.reset);

		BtnCancel.setText(BTN_CANCEL_NAME);
		getContentPane().add(BtnCancel);
		BtnCancel.setBounds(198, 410, 75, AdminWise.gBtnHeight);
		// BtnCancel.setBackground(java.awt.Color.white);
		BtnCancel.setIcon(VWImages.CloseIcon);

		Vector textIndices = null;
		try {
			loadedList = false;
			textIndices = getIndices();
			LstIndices.loadData(textIndices);
			loadedList = true;
		} catch (Exception e) {
			loadedList = true;
		}
		if(finalCustomList == null){
			AdminWise.printToConsole("finalCustomList is Null Inside Load Indices of AT ::::"+finalCustomList);
			LstIndices.loadData(textIndices);
		} else{
			AdminWise.printToConsole("finalCustomList is not Null Inside Load Indices of AT ::::"+finalCustomList);
			LstIndices.setCheckItems(finalCustomList);
		}
		int itemIndex = getSelectedItemIndex(textIndices);
		if(itemIndex != -1)
			LstIndices.setItemCheck(itemIndex, true);

		SymAction lSymAction = new SymAction();
		BtnAdd.addActionListener(lSymAction);
		BtnCancel.addActionListener(lSymAction);
		BtnReset.addActionListener(lSymAction);

		SymKey aSymKey = new SymKey();
		addKeyListener(aSymKey);
		LstIndices.addKeyListener(aSymKey);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		setResizable(false);

		//setCurrentLocation();
		setVisible(true);
	}
	
	/**
	 * Added For Retention Panel
	 * @param parent
	 * @param selectedIndexName
	 * @param panelName
	 * @param data
	 * @param retentionId
	 * @param Table
	 * @param finalCustomList
	 */
	public VWAddCustomIndices(Frame parent, String selectedIndexName, String panelName, Vector data,int retentionId,VWRetentionTable Table,  Vector finalCustomList, JDialog processDialog) {
		super(parent);
		AdminWise.printToConsole("VWAddCustomIndices() For Retention!!!!");
		processDialog.setVisible(false);
		processDialog.dispose();
		gCurRoom= room.getId();
		session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
		userName= session.user;
		sid= session.sessionId;
		if(panelName.equalsIgnoreCase("RP")){
			adminPanelName = "4";
			globalRetentionList=data;
			globalRetentionId=retentionId;
			TableRetentionGlobal=Table;
		}

		getContentPane().setBackground(AdminWise.getAWColor());
		setTitle(LBL_ADDINDEX_NAME);
		setModal(true);
		getContentPane().setLayout(null);
		setLocation(489, 159);
		setSize(283, 438);
		setVisible(false);

		//Dynamic user index starts with < and ends with >
		if(selectedIndexName.startsWith("<<") && selectedIndexName.endsWith(">>")){
			this.selectedIndexName = selectedIndexName.substring(2, selectedIndexName.length() - 2);
		}

		JLabel1.setText(LBL_LISTINDEX_NAME);
		getContentPane().add(JLabel1);
		JLabel1.setBounds(6, 4, 194, AdminWise.gBtnHeight);

		getContentPane().add(LstIndices);
		LstIndices.setBounds(4, 28, 276, 366);

		BtnAdd.setText(BTN_ADDINDEX_NAME);
		getContentPane().add(BtnAdd);
		BtnAdd.setBounds(6, 410, 105, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnAdd.setIcon(VWImages.OkIcon);
		
		BtnReset.setText(BTN_RESET_NAME);
		getContentPane().add(BtnReset);
		BtnReset.setBounds(118, 410, 75, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnReset.setIcon(VWImages.reset);

		BtnCancel.setText(BTN_CANCEL_NAME);
		getContentPane().add(BtnCancel);
		BtnCancel.setBounds(198, 410, 75, AdminWise.gBtnHeight);
		// BtnCancel.setBackground(java.awt.Color.white);
		BtnCancel.setIcon(VWImages.CloseIcon);

		Vector textIndices = null;
		try {
			loadedList = false;
			textIndices = getIndices();
			LstIndices.loadData(textIndices);
			loadedList = true;
		} catch (Exception e) {
			loadedList = true;
		}
		if(finalCustomList == null){
			AdminWise.printToConsole("finalCustomList is Null Inside Load Indices of Retention ::::"+finalCustomList);
			LstIndices.loadData(textIndices);
		} else{
			AdminWise.printToConsole("finalCustomList is not Null Inside Load Indices of Retention ::::"+finalCustomList);
			LstIndices.setCheckItems(finalCustomList);
		}
		int itemIndex = getSelectedItemIndex(textIndices);
		if(itemIndex != -1)
			LstIndices.setItemCheck(itemIndex, true);

		SymAction lSymAction = new SymAction();
		BtnAdd.addActionListener(lSymAction);
		BtnCancel.addActionListener(lSymAction);
		BtnReset.addActionListener(lSymAction);

		SymKey aSymKey = new SymKey();
		addKeyListener(aSymKey);
		LstIndices.addKeyListener(aSymKey);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		setResizable(false);

		//setCurrentLocation();
		setVisible(true);
	}

	private int getSelectedItemIndex(Vector textIndices) {
		int itemIndex = -1;
		if(textIndices != null && textIndices.size() > 0){
			for(int i=0; i<textIndices.size(); i++){
				VWIndexRec indexRec = (VWIndexRec) textIndices.get(i);
				if(indexRec.getName().equalsIgnoreCase(selectedIndexName)){
					itemIndex = i;
					break;
				}
			}
		}
		return itemIndex;
	}

	// ------------------------------------------------------------------------------
	class VWListSelectionListener implements
	javax.swing.event.ListSelectionListener {
		public void valueChanged(javax.swing.event.ListSelectionEvent e) {
			if (!loadedList)
				return;
			VWIndexRec selItem = (VWIndexRec) LstIndices.getList().getSelectedValue();			
		}
	}

	// ------------------------------------------------------------------------------
	class SymWindow extends java.awt.event.WindowAdapter {
		public void windowClosing(java.awt.event.WindowEvent event) {
			Object object = event.getSource();
			if (object == VWAddCustomIndices.this)
				Dialog_windowClosing(event);
		}
	}

	// ------------------------------------------------------------------------------
	void Dialog_windowClosing(java.awt.event.WindowEvent event) {
		BtnCancel_actionPerformed(null);
	}

	// ------------------------------------------------------------------------------
	class SymKey extends java.awt.event.KeyAdapter {
		public void keyTyped(java.awt.event.KeyEvent event) {
			Object object = event.getSource();
			if (event.getKeyText(event.getKeyChar()).equals("Enter"))
				try {
					BtnAdd_actionPerformed(null);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else if (event.getKeyText(event.getKeyChar()).equals("Escape"))
				BtnCancel_actionPerformed(null);
		}
	}

	// ------------------------------------------------------------------------------
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnAdd)
				try {
					BtnAdd_actionPerformed(event);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else if (object == BtnCancel)
				BtnCancel_actionPerformed(event);
			else if(object == BtnReset)
				BtnReset_actionPerformed(event);

		}
	}

	// ------------------------------------------------------------------------------
	public void addNotify() {
		// Record the size of the window prior to calling parents addNotify.
		Dimension size = getSize();

		super.addNotify();

		if (frameSizeAdjusted)
			return;
		frameSizeAdjusted = true;

		// Adjust size of frame according to the insets
		Insets insets = getInsets();
		setSize(insets.left + insets.right + size.width, insets.top
				+ insets.bottom + size.height);
	}

	// ------------------------------------------------------------------------------

	// ------------------------------------------------------------------------------
	void BtnAdd_actionPerformed(java.awt.event.ActionEvent event) throws RemoteException {
		AdminWise.adminPanel.setWaitPointer();
		int selectedIemsCount = LstIndices.getselectedItemsCount();
		AdminWise.printToConsole("selectedIemsCount ::::"+selectedIemsCount);
		
		List selectedId= LstIndices.getSelectedItemIds();
		List selectedValues= LstIndices.getSelectedItems();
		Vector idVector= new Vector();
		Vector valueVector= new Vector();
		
		idVector.addAll(selectedId);
		valueVector.addAll(selectedValues);
		String valueVectorString = "";
		for(int i=0; i < valueVector.size(); i++){
			valueVectorString= valueVectorString+valueVector.get(i).toString()+"\t";
		}
		String valueIdString = "";
		for(int i=0; i < idVector.size(); i++){
			valueIdString= valueIdString+idVector.get(i).toString()+"\t";
		}
		if(selectedIemsCount>33){
			JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("AddCustomIdex.MaxSelectLimit"));
		}else {
			/** CV10.1Enhancement: This code is added to get the DTC path where reports have been
			 * saved.
			 * @author apurba.m
			 * 
			 */
			String[] monthName = {"January", "February",
    				"March", "April", "May", "June", "July",
    				"August", "September", "October", "November",
    		"December"};
			String panelName="";

    		Calendar cal = Calendar.getInstance();
    		String month = monthName[cal.get(Calendar.MONTH)];
    		AdminWise.printToConsole("Current Month Name cvCreateNodePath ::::"+month);
    		int year = Calendar.getInstance().get(Calendar.YEAR);
    		AdminWise.printToConsole("Current Year cvCreateNodePath ::::"+year);
    		int day = Calendar.getInstance().get(Calendar.DATE);
    		AdminWise.printToConsole("Current day cvCreateNodePath ::::"+day);
    		if(adminPanelName.equalsIgnoreCase("2")){
    			panelName = "Recyled Documents Panel";
    		} else if(adminPanelName.equalsIgnoreCase("3")){
    			panelName = "Audit Trail Panel";
    		} else if(adminPanelName.equalsIgnoreCase("4")){
    			panelName = "Retention Panel";
    		}

    		String dtcPath="CVReports\\"+panelName+"\\"+year+"\\"+month+"\\"+day;
    		AdminWise.printToConsole("dtcPath :::: "+dtcPath);
    		
    		//////////////////////////////////////////////////////////////////////////////////////////
    		
		if(adminPanelName.equalsIgnoreCase("2")){
			AdminWise.printToConsole("adminPanelName 2::::"+adminPanelName);
			String path= "";
			String htmlReportName= "";
			String folderPath="";
			int docTypeId=0;
			Vector docTypeInfo= new Vector();
			AdminWise.gConnector.ViewWiseClient.addCustomList(sid , 2, "2", valueVectorString, 0);
			AdminWise.gConnector.ViewWiseClient.addCustomList(sid , 2, "1", valueIdString, 0);
			/**
			 * For getting the custom index list on Recycle Panel of Adminwise
			 */
			Vector settingsValue= new Vector();
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			customIndexList= AdminWise.gConnector.ViewWiseClient.getCustomList(sid, 2, settingsValue);
			if (customIndexList.size() > 0) {
				String str= "";
				str= str+customIndexList.get(1).toString();
				String[] st1= str.split("\t");
				for(int i=1; i<st1.length;i++){
					newCustomIndexList.add(st1[i]);
				}
				AdminWise.printToConsole("newCustomIndexList VWAddCustomIndices::::"+newCustomIndexList);
			}
			globalDataList=VWRecycleConnector.getRecycle(lastRecordIDGlobal, actionTypeGlobal, rowCountGlobal);
			String homePath= System.getenv("APPDATA");//loadSaveDialog(parent);
			String documentNameinDTC="Recycled_Document_"+System.currentTimeMillis();
			htmlReportName=documentNameinDTC+".html";
			folderPath=homePath+"\\CV Reports"+"\\";
	    	AdminWise.printToConsole("homePath saveArrayInFileForRdAndAt 1 ::::"+homePath);
	    	path= folderPath+htmlReportName;
	    	AdminWise.printToConsole("path for Recycled Documents ::::	"+path);
	    	AdminWise.printToConsole("folderPath for Recycled Documents ::::	"+folderPath);
	    	/**
	    	 * CV10.1 Enhancement: Check if Document Type exists
	    	 * @author apurba.m
	    	 */
	    	DocType dt= new DocType("CVReports");
    			dt.setName("CVReports");
    			/**
    			 * isDocTypeFound method is calling for checking whether "CVReports" doctype exists
    			 * or not
    			 */
    			docTypeId=AdminWise.gConnector.ViewWiseClient.isDocTypeFound(sid,dt);
    			AdminWise.printToConsole("docTypeId in AdminWise.VWAddCustomIndices :::: "+docTypeId);
    			
    		if(docTypeId > 0){
    			AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(sid, docTypeId, docTypeInfo);
    			AdminWise.printToConsole("docTypeInfo in AdminWise.VWAddCustomIndices ::::"+docTypeInfo);
    			
    			if(docTypeInfo.size()>0 && docTypeInfo!=null){
	    			StringTokenizer st= new StringTokenizer(docTypeInfo.get(0).toString(), "\t");
	    			int newDocypeId= Integer.parseInt(st.nextToken());
	    			String docTypeName= st.nextToken();
	    			String indexName= st.nextToken();
	    			Index idxName= new Index(indexName);
	    			idxName.setName(indexName);
	    			int indexId=AdminWise.gConnector.ViewWiseClient.getIndexId(sid, idxName);
	    			AdminWise.printToConsole("indexId in AdminWise.VWAddCustomIndices ::::"+indexId);
	    			/**
	    	    	 * CV10.1 Enhancement: Check if Index with ReportName name exists
	    	    	 * @author apurba.m
	    	    	 */
	    			if(indexId > 0 && indexName.equals("ReportName")){
	    				
	    				if(VWSaveAsHtml.saveArrayInFileForRd(TableGlobal.getReportData(),globalDataList,newCustomIndexList,RecycleColumnNames,RecycleColumnWidths,
	    						path,(java.awt.Component)this,LBL_RECYCLEDDOC_NAME, true)){
	    					int docCreated= AdminWise.gConnector.ViewWiseClient.cvCreateNodePath(sid, "Recyled Documents Panel", path, htmlReportName, folderPath, documentNameinDTC);
	    					AdminWise.printToConsole("docCreated Successfully and returned to Adminwise :::: "+docCreated);
	    						if(docCreated == 0){
	    							Vector templateNameListVector = new Vector();
	    			    			String srcFile = "";
	    			    			
	    							File folder = new File(folderPath.trim());
	    							AdminWise.printToConsole("Folder Inside VWAddCustomIndices ::::" + folder);
	    							
	    							File[] listOfFiles = folder.listFiles();
	    							AdminWise.printToConsole("listOfFiles Inside VWAddCustomIndices :::: " + listOfFiles);

	    							String ext = null; // FilenameUtils.getExtension("/path/to/file/foo.txt");

	    							for (int i = 0; i < listOfFiles.length; i++) {
	    								if (listOfFiles[i].isFile()) {
	    									AdminWise.printToConsole("File ::::" + listOfFiles[i].getName());
	    									if (listOfFiles[i].getName().endsWith(".html") || listOfFiles[i].getName().endsWith(".HTML")) {
	    										templateNameListVector.add(listOfFiles[i].getName());
	    									}
	    								}
	    							}
	    							if(templateNameListVector!=null&&templateNameListVector.size()>0){
	    								for(int j=0; j<templateNameListVector.size(); j++){
	    									File reportFile= new File(folderPath+templateNameListVector.get(j).toString());
	    									AdminWise.printToConsole("reportFile :::: "+reportFile);
	    									if(reportFile.exists()){
	    										try {
	    											AdminWise.printToConsole("File Deleted !!!! "+Files.deleteIfExists(reportFile.toPath()));
	    										} catch (IOException e) {
	    											AdminWise.printToConsole("EXCEPTION WHILE DELETING FILE FROM APP DATA LOCATION :::: "+e.getMessage());
	    											e.printStackTrace();
	    										}
	    									}
	    								}
	    							}
	    							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReport.reportCreateSuccess")+" "+"\""+dtcPath+"\"");
	    						} else if(docCreated == -171){
	    							JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
	    						} else if(docCreated == -172){
	    							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
	    						} else if(docCreated == -1) {
	    							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.createDocGeneralError"));
	    						}
	    					
	    				} else {
	    					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.recycledReportGenerationFailed"));
	    				}
	    				
	    			} else {
	    				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
	    			}
    			}
    			
    		} else {
    			JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
    		}	    	
	    	//////////////////////////////////////////////////////////////////////
			
			
			//		panel.BtnSaveAs_actionPerformed(event);
		} else if(adminPanelName.equalsIgnoreCase("3")){
			AdminWise.printToConsole("adminPanelName 3::::"+adminPanelName);
			AdminWise.gConnector.ViewWiseClient.addCustomList(sid , 3, "2", valueVectorString, 0);
			AdminWise.gConnector.ViewWiseClient.addCustomList(sid , 3, "1", valueIdString, 0);
			/**
			 * For getting the custom index list on Audit Trail Panel of Adminwise
			 */
			Vector settingsValue= new Vector();
			String path= "";
			String htmlReportName= "";
			String folderPath="";
			int docTypeId=0;
			Vector docTypeInfo = new Vector();			
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			customIndexList= AdminWise.gConnector.ViewWiseClient.getCustomList(sid, 3, settingsValue);
			if (customIndexList.size() > 0) {
				String str= "";
				str= str+customIndexList.get(1).toString();
				String[] st1= str.split("\t");
				for(int i=1; i<st1.length;i++){
					newCustomIndexList.add(st1[i]);
				}
				AdminWise.printToConsole("newCustomIndexList VWAddCustomIndices AT::::"+newCustomIndexList);
			}
			globalATData=VWATConnector.getData(filterGlobal,0);
			String homePath= System.getenv("APPDATA");//loadSaveDialog(parent);
			String documentNameinDTC="Audit_Trail"+System.currentTimeMillis();
			htmlReportName=documentNameinDTC+".html";
			folderPath=homePath+"\\CV Reports"+"\\";
	    	AdminWise.printToConsole("homePath saveArrayInFileForRdAndAt 1 ::::"+homePath);
	    	path= folderPath+htmlReportName;
	    	AdminWise.printToConsole("path for AT Documents ::::	"+path);
	    	AdminWise.printToConsole("folderPath for AT Documents ::::	"+folderPath);
	    	
	    	/**
	    	 * CV10.1 Enhancement: Check if Document Type exists
	    	 * @author apurba.m
	    	 */
	    	DocType dt= new DocType("CVReports");
    			dt.setName("CVReports");
    			/**
    			 * isDocTypeFound method is calling for checking whether "CVReports" doctype exists
    			 * or not
    			 */
    			docTypeId=AdminWise.gConnector.ViewWiseClient.isDocTypeFound(sid,dt);
    			AdminWise.printToConsole("docTypeId in AdminWise.VWAddCustomIndices :::: "+docTypeId);
    			if(docTypeId > 0){
    				AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(sid, docTypeId, docTypeInfo);
        			AdminWise.printToConsole("docTypeInfo in AdminWise.VWAddCustomIndices ::::"+docTypeInfo);
        			if(docTypeInfo.size()>0 && docTypeInfo!=null){
    	    			StringTokenizer st= new StringTokenizer(docTypeInfo.get(0).toString(), "\t");
    	    			int newDocypeId= Integer.parseInt(st.nextToken());
    	    			String docTypeName= st.nextToken();
    	    			String indexName= st.nextToken();
    	    			Index idxName= new Index(indexName);
    	    			idxName.setName(indexName);
    	    			int indexId=AdminWise.gConnector.ViewWiseClient.getIndexId(sid, idxName);
    	    			AdminWise.printToConsole("indexId in AdminWise.VWAddCustomIndices ::::"+indexId);
    	    			/**
    	    	    	 * CV10.1 Enhancement: Check if Index with ReportName name exists
    	    	    	 * @author apurba.m
    	    	    	 */
    	    			if(indexId > 0 && indexName.equals("ReportName")){
    			
			if(VWSaveAsHtml.saveArrayInFileForAT(TableAtGlobal.getReportData(),globalATData,newCustomIndexList,ATColumnNames,ATColumnWidths,
					path,(java.awt.Component)this,LBL_AT_NAME, true)) {
				AdminWise.printToConsole("Report Generated for AT Panel!!!!");
				int docCreated= AdminWise.gConnector.ViewWiseClient.cvCreateNodePath(sid, "Audit Trail Panel", path, htmlReportName, folderPath, documentNameinDTC);
				AdminWise.printToConsole("docCreated Successfully and returned to Adminwise :::: "+docCreated);
				if(docCreated == 0){
					Vector templateNameListVector = new Vector();
	    			String srcFile = "";
	    			
					File folder = new File(folderPath.trim());
					AdminWise.printToConsole("Folder Inside VWAddCustomIndices ::::" + folder);
					
					File[] listOfFiles = folder.listFiles();
					AdminWise.printToConsole("listOfFiles Inside VWAddCustomIndices :::: " + listOfFiles);

					String ext = null; // FilenameUtils.getExtension("/path/to/file/foo.txt");

					for (int i = 0; i < listOfFiles.length; i++) {
						if (listOfFiles[i].isFile()) {
							AdminWise.printToConsole("File ::::" + listOfFiles[i].getName());
							if (listOfFiles[i].getName().endsWith(".html") || listOfFiles[i].getName().endsWith(".HTML")) {
								templateNameListVector.add(listOfFiles[i].getName());
							}
						}
					}
					if(templateNameListVector!=null&&templateNameListVector.size()>0){
						for(int j=0; j<templateNameListVector.size(); j++){
							File reportFile= new File(folderPath+templateNameListVector.get(j).toString());
							AdminWise.printToConsole("reportFile :::: "+reportFile);
							if(reportFile.exists()){
								try {
									AdminWise.printToConsole("File Deleted !!!! "+Files.deleteIfExists(reportFile.toPath()));
								} catch (IOException e) {
									AdminWise.printToConsole("EXCEPTION WHILE DELETING FILE FROM APP DATA LOCATION :::: "+e.getMessage());
									e.printStackTrace();
								}
							}
						}
					}
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReport.reportCreateSuccess")+" "+"\""+dtcPath+"\"");
				} else if(docCreated == -171){
					JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
				} else if(docCreated == -172){
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
				} else if(docCreated == -1) {
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.createDocGeneralError"));
				}
			}else {
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.auditTrailReportGenerationFailed"));
			}
    	    			}else {
    	    				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
    	    			}
		}
    			}	else {
    				JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
        		}	    			
		} else if(adminPanelName.equalsIgnoreCase("4")){
			AdminWise.printToConsole("adminPanelName 4::::"+adminPanelName);
			AdminWise.gConnector.ViewWiseClient.addCustomList(sid , 4, "2", valueVectorString, 0);
			AdminWise.gConnector.ViewWiseClient.addCustomList(sid , 4, "1", valueIdString, 0);
			/**
			 * For getting the custom index list on Retention Panel of Adminwise
			 */
			Vector settingsValue= new Vector();
			String path= "";
			String htmlReportName= "";
			String folderPath="";
			int docTypeId;
			Vector docTypeInfo= new Vector();
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			customIndexList= AdminWise.gConnector.ViewWiseClient.getCustomList(sid, 4, settingsValue);
			if (customIndexList.size() > 0) {
				String str= "";
				str= str+customIndexList.get(1).toString();
				String[] st1= str.split("\t");
				for(int i=1; i<st1.length;i++){
					newCustomIndexList.add(st1[i]);
				}
				AdminWise.printToConsole("newCustomIndexList VWAddCustomIndices Retention::::"+newCustomIndexList);
			}
			globalRetentionList=VWRetentionConnector.getRetentionCompletedDocs(globalRetentionId);
			AdminWise.printToConsole("\n\n");
			AdminWise.printToConsole("\n\n");
			String homePath= System.getenv("APPDATA");//loadSaveDialog(parent);
			String documentNameinDTC="Retention_Document_"+System.currentTimeMillis();
			htmlReportName=documentNameinDTC+".html";
			folderPath=homePath+"\\CV Reports"+"\\";
	    	AdminWise.printToConsole("homePath saveArrayInFileForRetention 1 ::::"+homePath);
	    	path= folderPath+htmlReportName;
	    	AdminWise.printToConsole("path for Retention Documents ::::	"+path);
	    	AdminWise.printToConsole("folderPath for Retention Documents ::::	"+folderPath);
	    	
	    	/**
	    	 * CV10.1 Enhancement: Check if Document Type exists
	    	 * @author apurba.m
	    	 */
	    	DocType dt= new DocType("CVReports");
    			dt.setName("CVReports");
    			/**
    			 * isDocTypeFound method is calling for checking whether "CVReports" doctype exists
    			 * or not
    			 */
    			docTypeId=AdminWise.gConnector.ViewWiseClient.isDocTypeFound(sid,dt);
    			AdminWise.printToConsole("docTypeId in AdminWise.VWAddCustomIndices :::: "+docTypeId);
    			if(docTypeId > 0){
    				AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(sid, docTypeId, docTypeInfo);
        			AdminWise.printToConsole("docTypeInfo in AdminWise.VWAddCustomIndices ::::"+docTypeInfo);
        			if(docTypeInfo.size()>0 && docTypeInfo!=null){
    	    			StringTokenizer st= new StringTokenizer(docTypeInfo.get(0).toString(), "\t");
    	    			int newDocypeId= Integer.parseInt(st.nextToken());
    	    			String docTypeName= st.nextToken();
    	    			String indexName= st.nextToken();
    	    			Index idxName= new Index(indexName);
    	    			idxName.setName(indexName);
    	    			int indexId=AdminWise.gConnector.ViewWiseClient.getIndexId(sid, idxName);
    	    			AdminWise.printToConsole("indexId in AdminWise.VWAddCustomIndices ::::"+indexId);
    	    			/**
    	    	    	 * CV10.1 Enhancement: Check if Index with ReportName name exists
    	    	    	 * @author apurba.m
    	    	    	 */
    	    			if(indexId > 0 && indexName.equals("ReportName")){
    				
    			
			if(VWSaveAsHtml.saveArrayInFileForRetention(TableRetentionGlobal.getReportDataForCustomIndex(),globalRetentionList,newCustomIndexList,RetentionsColumnNames,RetentionsColumnWidths,
					path,(java.awt.Component)this,LBL_RETENTION_NAME, true))
			{
				int docCreated= AdminWise.gConnector.ViewWiseClient.cvCreateNodePath(sid, "Retention Panel", path, htmlReportName, folderPath, documentNameinDTC);
				AdminWise.printToConsole("docCreated Successfully and returned to Adminwise :::: "+docCreated);
				if(docCreated == 0){
					Vector templateNameListVector = new Vector();
	    			String srcFile = "";
	    			
					File folder = new File(folderPath.trim());
					AdminWise.printToConsole("Folder Inside VWAddCustomIndices ::::" + folder);
					
					File[] listOfFiles = folder.listFiles();
					AdminWise.printToConsole("listOfFiles Inside VWAddCustomIndices :::: " + listOfFiles);

					String ext = null; // FilenameUtils.getExtension("/path/to/file/foo.txt");

					for (int i = 0; i < listOfFiles.length; i++) {
						if (listOfFiles[i].isFile()) {
							AdminWise.printToConsole("File ::::" + listOfFiles[i].getName());
							if (listOfFiles[i].getName().endsWith(".html") || listOfFiles[i].getName().endsWith(".HTML")) {
								templateNameListVector.add(listOfFiles[i].getName());
							}
						}
					}
					if(templateNameListVector!=null&&templateNameListVector.size()>0){					
						for(int j=0; j<templateNameListVector.size(); j++){
							File reportFile= new File(folderPath+templateNameListVector.get(j).toString());
							AdminWise.printToConsole("reportFile :::: "+reportFile);
							if(reportFile.exists()){
								try {
									AdminWise.printToConsole("File Deleted !!!! "+Files.deleteIfExists(reportFile.toPath()));
								} catch (IOException e) {
									AdminWise.printToConsole("EXCEPTION WHILE DELETING FILE FROM APP DATA LOCATION :::: "+e.getMessage());
									e.printStackTrace();
								}
							}
						}
					}
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReport.reportCreateSuccess")+" "+"\""+dtcPath+"\"");
				} else if(docCreated == -171){
					JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
				} else if(docCreated == -172){
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
				} else if(docCreated == -1) {
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.createDocGeneralError"));
				}
			} else {
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.retentionReportGenerationFailed"));
			}
        			} else {
        				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
	    			}
    			}
    			} else {
    				JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
        		}	  
		}
		
		cancelFlag = false;
		this.setVisible(false);
		}
	}

	// ------------------------------------------------------------------------------
	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
		//saveDlgOptions();
		cancelFlag = true;
		this.setVisible(false);
	}
	
	//---------------------------------------------------------------------------------
	void BtnReset_actionPerformed(java.awt.event.ActionEvent event){
		AdminWise.printToConsole("Inside BtnReset_actionPerformed !!!! ");
		LstIndices.deSelectAllItems();
	}

	// ------------------------------------------------------------------------------
	public List getValues() {
		List selIndices = new LinkedList();
		int count = LstIndices.getItemsCount();
		for (int i = 1; i < count; i++)
			if (LstIndices.getItemIsCheck(i))
				selIndices.add(LstIndices.getItem(i));
		return selIndices;
	}

	// ------------------------------------------------------------------------------
	public boolean getCancelFlag() {
		return cancelFlag;
	}

	// ------------------------------------------------------------------------------
	private Vector getIndices() {
		Vector textIndices = new Vector();
		VWIndexRec[] indices = VWDocTypeConnector.getIndicesForCustomReport(false);
		if (indices == null || indices.length == 0)
			return null;
		int count = indices.length;

		for (int i = 0; i < count; i++) {
				textIndices.add(indices[i]);
		}
		return textIndices;
	}

	// ------------------------------------------------------------------------------
	private void saveDlgOptions() {
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		if (AdminWise.adminPanel.saveDialogPos) {
			prefs.putInt("x", this.getX());
			prefs.putInt("y", this.getY());
		} else {
			prefs.putInt("x", 50);
			prefs.putInt("y", 50);
		}
	}

	// ------------------------------------------------------------------------------
	private void getDlgOptions() {
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		//setLocation(prefs.getInt("x", 50), prefs.getInt("y", 50));
		if((prefs.getInt("x", 0) == 0) || prefs.getInt("y", 0) == 0){
			Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width, 40);
			setLocation(point);
		}else{
			setLocation(prefs.getInt("x", 50), prefs.getInt("y", 50));
		}

	}

	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width+20, 180);
		setLocation(point);
	}

	// ------------------------------------------------------------------------------

	public VWIndexRec getSelectedIndex() {
		VWIndexRec selectedIndex = null;
		try{
			List selectedItemList = LstIndices.getSelectedItems();

			if(selectedItemList != null && selectedItemList.size() > 0){
				selectedIndex = (VWIndexRec) selectedItemList.get(0);

			}
		}catch (Exception e) {
			AdminWise.printToConsole("Exception in VWDlgAddDynamicUser : "+e.getMessage());
		}
		return selectedIndex;
	}
}
