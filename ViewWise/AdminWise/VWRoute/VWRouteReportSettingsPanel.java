package ViewWise.AdminWise.VWRoute;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.japura.gui.CheckList;
import org.japura.gui.event.ListCheckListener;
import org.japura.gui.event.ListEvent;
import org.japura.gui.model.DefaultListCheckModel;
import org.japura.gui.model.ListCheckModel;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import com.computhink.resource.ResourceManager;

public class VWRouteReportSettingsPanel extends JPanel {

	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();


	public static CheckList workFlowActionList = null;
	public static CheckList workFlowTaskList = null;
	public static CheckList availableWorkFlowList=null;
	public static CheckList	availableTaskList=null;
	public static CheckList	userGroupList=null;
	public static HashMap availableWorkFlowMap=new HashMap();
	public static HashMap availableTaskMap=new HashMap();
	public static HashMap userGroupMap=new HashMap();
	public static ListCheckModel workFlowActionListModel = new DefaultListCheckModel();
	public static ListCheckModel workFlowTaskListModel = new DefaultListCheckModel();
	public static ListCheckModel availableWorkFlowListModel = new DefaultListCheckModel();
	public static ListCheckModel availableTaskListModel = new DefaultListCheckModel();
	public static ListCheckModel userGroupListModel = new DefaultListCheckModel();
	 Vector selectAvailableWorkFlow=new Vector();
	JPanel panleActions=null;
	Vector dataVector=new Vector();
	public JScrollPane scWorkFlowAction;
	public JScrollPane scWorkFlowTask;
	public JScrollPane scAvailableWorkFlow;
	public JScrollPane scUserGroup;
	public JScrollPane scAvailableTask;
	public static boolean isWebClient = false;
	public static int wfSelectedCount =0;
	public static int availableWorkFlowselectAllFlag=0;
	public static int availableTaskselectAllFlag=0;
	public static int availablUserSelectAllFlag=0;


	public static String prevRoom;
	public VWRouteReportSettingsPanel() {	
		try{
			
			
		}catch(Exception e){
			AdminWise.printToConsole("Exception in VWNotificationSettingsPanel() constructor : "+e.getMessage());
		}
	}
	

	private void initComponents() {
		try{
			scWorkFlowAction=new JScrollPane(workFlowActionList);
			scWorkFlowAction.setBounds(5,50,100,400);   //0,10,100,400
			//add(scWorkFlowAction);
			workFlowActionList.setFixedCellWidth(130);
			workFlowActionList.setFixedCellHeight(25);
			VWRouteReportPanel.actionsWorkFlowPanel.add(scWorkFlowAction);
			workFlowActionList.setVisible(true);
			workFlowActionList.setBorder(new EmptyBorder(0, 4, 0, 4));
			workFlowActionList.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					AdminWise.printToConsole("Mouse clicked");
					if(workFlowActionList.getModel().getChecksCount()>=1){
						workFlowTaskList.setEnabled(false);	
						workFlowTaskList.getModel().lockAll();
					}else{
						workFlowTaskList.setEnabled(true);	
						workFlowTaskList.getModel().removeLocks();
					}
				}
			});
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while Initializing Notification Settings Panel : "+ex.getMessage());
			ex.printStackTrace();
		}
	}

private void initWorkFlowTaskComponents(){
	try{
		scWorkFlowTask=new JScrollPane(workFlowTaskList);
		scWorkFlowTask.setBounds(115,50,400,400);   //0,300,400,400
		//add(scWorkFlowTask);
		workFlowTaskList.setFixedCellWidth(130);
		workFlowTaskList.setFixedCellHeight(25);
		VWRouteReportPanel.actionsWorkFlowPanel.add(scWorkFlowTask);
		workFlowTaskList.setBorder(new EmptyBorder(0, 4, 0, 4));
		workFlowTaskList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				AdminWise.printToConsole("Mouse clicked");
				if(workFlowTaskList.getModel().getChecksCount()>=1){
					workFlowActionList.setEnabled(false);	
					workFlowActionList.getModel().lockAll();
				}else{
					workFlowActionList.setEnabled(true);	
					workFlowActionList.getModel().removeLocks();
				}
			}
		});

	}catch(Exception ex){
		AdminWise.printToConsole("Exception while Initializing Notification Settings Panel : "+ex.getMessage());
		ex.printStackTrace();
	}

}

private void initAvailableWorkFlowComponents(){

	try{

		scAvailableWorkFlow=new JScrollPane(availableWorkFlowList);
		scAvailableWorkFlow.setBounds(300,400,400,300);
		availableWorkFlowList.setSize(140, 200);
		availableWorkFlowList.setFixedCellWidth(130);
		availableWorkFlowList.setFixedCellHeight(25);
		VWRouteReportPanel.availableWorkFlowPanel.add(scAvailableWorkFlow);
		//scAvailableWorkFlow.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scAvailableWorkFlow.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scAvailableWorkFlow.setVisible(true);
		availableWorkFlowList.setVisible(true);
		availableWorkFlowList.setBorder(new EmptyBorder(0, 4, 0, 4));
		availableTaskMap.clear();


		
		availableWorkFlowList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				
				if(availableWorkFlowList.getModel().getChecksCount()==1) {
					AdminWise.printToConsole("selected count is one:::");
					java.util.List lst=availableWorkFlowListModel.getCheckeds();
					VWRoom room=AdminWise.adminPanel.roomPanel.getSelectedRoom();
					Vector<String> availableTaskVector1=new Vector();
					int routeId=Integer.parseInt(availableWorkFlowMap.get(lst.get(0)).toString());
					String routeName=lst.get(0).toString();
					AdminWise.printToConsole("routeId is:::"+routeId);
					AdminWise.printToConsole("routeName is:::"+routeName);
					
					AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(),4,String.valueOf(routeId),availableTaskVector1);
					if(availableTaskVector1.size()>0){
						availableTaskList.getModel().clear();

						for (String actionData : availableTaskVector1) {	
							StringTokenizer st = new StringTokenizer(actionData, "\t");
							int id=Integer.parseInt(st.nextToken());
							String taskName=st.nextToken();
							availableTaskMap.put(taskName,id);
							availableTaskList.getModel().addElement(taskName);
						}
						if (availableWorkFlowList.getModel().isChecked("Select All")==true) {
							availableWorkFlowselectAllFlag=1;
							availableTaskList.getModel().setCheck("Select All");
							availableWorkFlowList.getModel().checkAll();
						}
						//Condition added to fix the uncheck issue of single checkbox with select all Modified by :- Madhavan Date :-24/05/2016
						if ((availableWorkFlowList.getModel().isChecked("Select All")==false)){
							if (availableWorkFlowselectAllFlag==1) {
								availableWorkFlowList.getModel().removeChecks();
								availableTaskList.getModel().removeChecks();
								availableWorkFlowselectAllFlag=0;
							}
						}
						

					}
				}
				else if(availableWorkFlowList.getModel().getChecksCount()>1){
					if(availableWorkFlowList.getModel().isChecked("Select All")==true){
						availableWorkFlowselectAllFlag=1;
						availableWorkFlowList.getModel().checkAll();
						//Code added to fix the select All uncheck issue for available tasks  Date:-03/05/2016
						availableTaskList.getModel().clear();
						availableTaskList.getModel().addElement("Select All");
						availableTaskList.getModel().setCheck("Select All");
					}
					else if ((availableWorkFlowList.getModel().isChecked("Select All")==false)){
						if (availableWorkFlowselectAllFlag==1) {
							availableWorkFlowList.getModel().removeChecks();
							availableTaskList.getModel().removeChecks();
							availableWorkFlowselectAllFlag=0;
						}	//Else condition added to fix the select All uncheck issue for available tasks  Date:-03/05/2016
						else{
							availableTaskList.getModel().clear();
							availableTaskList.getModel().addElement("Select All");
							availableTaskList.getModel().setCheck("Select All");
						}

					}
					//Commented on  Date:-03/05/2016
					/*availableTaskList.getModel().clear();
					availableTaskList.getModel().addElement("Select All");
					availableTaskList.getModel().setCheck("Select All");*/
				}
			}

		});

	}catch(Exception ex){
		AdminWise.printToConsole("Exception while Initializing Notification Settings Panel : "+ex.getMessage());
		ex.printStackTrace();
	}

}

private void removeListElements(){
	DefaultListModel model = (DefaultListModel) availableTaskList.getModel();
	model.removeAllElements();
	availableTaskList.repaint();
}

private void initAvailableTaskComponents(){

	try{
	
		scAvailableTask=new JScrollPane(availableTaskList);
		availableTaskList.setBounds(300,400,400,260);
		scAvailableTask.setBounds(300,400,400,300);
		availableTaskList.setSize(140, 250);
		availableTaskList.setFixedCellWidth(130);
		availableTaskList.setFixedCellHeight(25);
		VWRouteReportPanel.availableWorkFlowPanel.add(scAvailableTask);
	
		scAvailableTask.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scAvailableTask.setVisible(true);
		availableTaskList.setVisible(true);
		availableTaskList.setBorder(new EmptyBorder(0, 4, 0, 4));
		
		availableTaskList.addMouseListener(new MouseAdapter() {
		      public void mouseClicked(MouseEvent e) {
		    	  if(availableTaskList.getModel().getChecksCount()==1){
		    		  if(availableTaskList.getModel().isChecked("Select All")==true){
		    			  availableTaskselectAllFlag=1;
		    			  availableTaskList.getModel().checkAll();
		    		  }
		    		//Condition added to fix the uncheck issue of single checkbox with select all Modified by :- Madhavan Date :-24/05/2016
		    		  if ((availableTaskList.getModel().isChecked("Select All")==false)){
		    			  if (availableTaskselectAllFlag==1) {
		    				  availableTaskList.getModel().removeChecks();
		    				  availableTaskselectAllFlag=0;
		    			  }
		    		  }
		    	  }
		    			else if(availableTaskList.getModel().getChecksCount()>1){
		    				 if(availableTaskList.getModel().isChecked("Select All")==true){
		    					 availableTaskselectAllFlag=1;
		    					 availableTaskList.getModel().checkAll();
				    		  }
							if (availableTaskList.getModel().isChecked("Select All")==false){
								if(availableTaskselectAllFlag==1){
								availableTaskList.getModel().removeChecks();
								availableTaskselectAllFlag=0;
								}
							}
		    			}
		    	 
		    	  
		      }
		    });
		scAvailableTask.repaint();
	}catch(Exception ex){
		AdminWise.printToConsole("Exception while Initializing Notification Settings Panel : "+ex.getMessage());
		ex.printStackTrace();
	}

}

private void initUserGroupComponents(){

	try{
	
		scUserGroup=new JScrollPane(userGroupList);

		VWRouteReportPanel.userGroupPanel.add(scUserGroup);
		scUserGroup.setBounds(0,0,200,200);
		userGroupList.setSize(140, 200);
		userGroupList.setFixedCellWidth(130);
		userGroupList.setFixedCellHeight(25);
		scUserGroup.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scUserGroup.setVisible(true);
		userGroupList.setVisible(true);
		userGroupList.setBorder(new EmptyBorder(0, 4, 0, 0));
		userGroupList.addMouseListener(new MouseAdapter() {
		      public void mouseClicked(MouseEvent e) {
		    	  if(userGroupList.getModel().getChecksCount()==1){
		    		  if(userGroupList.getModel().isChecked("Select All")==true){
		    			  availablUserSelectAllFlag=1;
		    			  userGroupList.getModel().checkAll();
		    		  }
		    	  }
		    			else if(userGroupList.getModel().getChecksCount()>1){
		    				 if(userGroupList.getModel().isChecked("Select All")==true){
		    					 availablUserSelectAllFlag=1;
				    			  userGroupList.getModel().checkAll();
				    		  }
							if (userGroupList.getModel().isChecked("Select All")==false){
								if(availablUserSelectAllFlag==1){
								userGroupList.getModel().removeChecks();
								availablUserSelectAllFlag=0;
								}
							}
		    			}
		    	 
		    	  
		      }
		    });
		
	}catch(Exception ex){
		AdminWise.printToConsole("Exception while Initializing Notification Settings Panel : "+ex.getMessage());
		ex.printStackTrace();
	}

}



	public void loadUI(Vector<String> workFlowActionData,Vector<String> workTaskActionVector,Vector availableWorkFlowVector,Vector availableTaskVector,Vector usersGroupsVector) {
		try{
			VWRoom room=AdminWise.adminPanel.roomPanel.getSelectedRoom();
			if(workFlowActionData!=null&&workFlowActionData.size()>0){
				if(workFlowActionList!=null  || !room.getName().equals(prevRoom)){
					workFlowActionList.getModel().clear();
					for (String actionData : workFlowActionData) {
						workFlowActionListModel.addElement(actionData);
					}
					workFlowActionList.setModel(workFlowActionListModel);
				}
				else if(workFlowActionList==null){
					workFlowActionList=new CheckList();
					
					for (String actionData : workFlowActionData) {
						workFlowActionListModel.addElement(actionData);
					}
					workFlowActionList.setModel(workFlowActionListModel);
					initComponents();
				}}
				if(workTaskActionVector!=null&&workTaskActionVector.size()>0){
					if(workFlowTaskList!=null  || !room.getName().equals(prevRoom)){
						workFlowTaskList.getModel().clear();
						for (String workFlowTaskData : workTaskActionVector) {
							workFlowTaskListModel.addElement(workFlowTaskData);
						}
						workFlowTaskList.setModel(workFlowTaskListModel);
					}
					else if(workFlowTaskList==null){
						workFlowTaskList=new CheckList();
						workFlowTaskList.getModel().clear();
						for (String workFlowTaskData : workTaskActionVector) {
							workFlowTaskListModel.addElement(workFlowTaskData);
						}
						
						workFlowTaskList.setModel(workFlowTaskListModel);
						initWorkFlowTaskComponents();
					}
				}
				
			

				if(availableWorkFlowVector!=null&&availableWorkFlowVector.size()>0){
					if(availableWorkFlowList!=null  || !room.getName().equals(prevRoom)){
						AdminWise.printToConsole("Inside model clear ");
						availableWorkFlowList.getModel().clear();
						Vector availableWorkFlow=new Vector();
						if(availableWorkFlowMap.size()>0)
							availableWorkFlowMap.clear();
						for(int i=0;i<availableWorkFlowVector.size();i++){
							StringTokenizer st = new StringTokenizer((String) availableWorkFlowVector.get(i), "\t");
							int id=Integer.parseInt(st.nextToken());
							String workFlowName=st.nextToken();
							availableWorkFlowMap.put(workFlowName, id);
							availableWorkFlowListModel.addElement(workFlowName);
						}
						availableWorkFlowList.setModel(availableWorkFlowListModel);

					}
					else if(availableWorkFlowList==null){
						AdminWise.printToConsole("Inside else");
						availableWorkFlowList=new CheckList();
						Vector availableWorkFlow=new Vector();
						if(availableWorkFlowMap.size()>0)
							availableWorkFlowMap.clear();
						for(int i=0;i<availableWorkFlowVector.size();i++){
							StringTokenizer st = new StringTokenizer((String) availableWorkFlowVector.get(i), "\t");
							int id=Integer.parseInt(st.nextToken());
							String workFlowName=st.nextToken();
							availableWorkFlowMap.put(workFlowName, id);
							availableWorkFlowListModel.addElement(workFlowName);
						}
						availableWorkFlowList.setModel(availableWorkFlowListModel);
						AdminWise.printToConsole("before init worklfow");
						initAvailableWorkFlowComponents();
					}
				}
				
			
			AdminWise.printToConsole("Available task vector:::"+availableTaskVector);
			if(availableTaskVector!=null&&availableTaskVector.size()>0){
				if(availableTaskList!=null  || !room.getName().equals(prevRoom)){
					availableTaskList.getModel().clear();
					Vector availableTask=new Vector();
					for(int i=0;i<availableTaskVector.size();i++){
						StringTokenizer st = new StringTokenizer((String) availableTaskVector.get(i), "\t");
						int id=Integer.parseInt(st.nextToken());

						String taskName=st.nextToken();
						availableTaskListModel.addElement(taskName);
					}
					availableTaskList.setModel(availableTaskListModel);
				}
				else if(availableTaskList==null){
					availableTaskList=new CheckList();
					Vector availableTask=new Vector();
					for(int i=0;i<availableTaskVector.size();i++){
						StringTokenizer st = new StringTokenizer((String) availableTaskVector.get(i), "\t");
						int id=Integer.parseInt(st.nextToken());
						
						String taskName=st.nextToken();
						availableTaskListModel.addElement(taskName);
					}
					availableTaskList.setModel(availableTaskListModel);
					initAvailableTaskComponents();
				}
			}
			if(usersGroupsVector!=null&&usersGroupsVector.size()>0){
				if(userGroupList!=null  || !room.getName().equals(prevRoom)){
					userGroupMap.clear();
					userGroupList.getModel().clear();
					Vector userGroup=new Vector();
					for(int i=0;i<usersGroupsVector.size();i++){
						StringTokenizer st = new StringTokenizer((String) usersGroupsVector.get(i), "\t");
						int id=Integer.parseInt(st.nextToken());
						String userData= st.nextToken();
						userGroupMap.put(userData, id);
						userGroupListModel.addElement(userData);
						userGroupList.setModel(userGroupListModel);
					}

				
				}
			
				else if(userGroupList==null){
					userGroupList=new CheckList();
					userGroupMap.clear();
					Vector userGroup=new Vector();
					for(int i=0;i<usersGroupsVector.size();i++){
						StringTokenizer st = new StringTokenizer((String) usersGroupsVector.get(i), "\t");
						int id=Integer.parseInt(st.nextToken());
						String userData= st.nextToken();
						userGroupMap.put(userData, id);
						userGroupListModel.addElement(userData);
						userGroupList.setModel(userGroupListModel);
					}

					initUserGroupComponents();
				}
			}
	
		
			VWRouteReportSettingsPanel.workFlowActionList.getModel().removeChecks();
			VWRouteReportSettingsPanel.workFlowTaskList.getModel().removeChecks();
			VWRouteReportSettingsPanel.availableWorkFlowList.getModel().removeChecks();
			VWRouteReportSettingsPanel.availableTaskList.getModel().removeChecks();
			VWRouteReportSettingsPanel.userGroupList.getModel().removeChecks();
			
		}catch(Exception e){
			AdminWise.printToConsole("Exception in loadUI(settings) : "+e.getMessage());
		}
		

	}
	



}

