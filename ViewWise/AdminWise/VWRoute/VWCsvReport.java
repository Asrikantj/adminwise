package ViewWise.AdminWise.VWRoute;

import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.vwc.VWCUtil;

import ViewWise.AdminWise.AdminWise;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;

public class VWCsvReport {
	public VWCsvReport() {
		
	}
	public boolean generateCSVReport(String path, Vector<String> resultSet, String columnDescription, String selectedReport) {
		boolean reportGenerated = false;
		/*String fileName = "UserSearchReport_"+new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date())+".csv";
		String csvFilePath = path +"\\"+fileName;*/
		
		AdminWise.printToConsole("inside generateCSVReport....."+path);
		try {
			if (path.toLowerCase().endsWith("csv")) {
	           // DatabaseConnection dbcon=new DatabaseConnection();
				AdminWise.printToConsole("Before initializing the csv writter object...");
				String title = null;
				//if (selectedReport != null && selectedReport != ""  && !selectedReport.equalsIgnoreCase("<Available Reports>") )
				if (selectedReport != null && selectedReport != ""  && !selectedReport.equalsIgnoreCase("<System Reports>") && !selectedReport.equalsIgnoreCase("<Custom Reports>") )
					title = selectedReport;
				else
					title = "General Report";
	            CSVWriter writer = new CSVWriter(new FileWriter(path));

	            /*String tabChar = "\t";
	            AdminWise.printToConsole("columnDescription before replacing tab with comma...."+columnDescription.contains(tabChar));
	            if (columnDescription != null && columnDescription.contains(tabChar))
	            	columnDescription = columnDescription.replaceAll(tabChar, ",").replace(",,", ",");*/
	            AdminWise.printToConsole("columnDescription before writing in csv file...."+columnDescription);
	            writer.writeNext(title);
	            writer.writeNext(columnDescription);
	            String value = null;
	            for (int count = 0; count < resultSet.size(); count ++) {
	            	value = resultSet.get(count);
	            	AdminWise.printToConsole("Resultset before writing in csv file...."+value);
					if (value != null && value.trim().length() > 0) {
		            	/*if (value.contains(tabChar))
		            		 value = value.replaceAll(tabChar, ",");*/
		            	writer.writeNext(value); //And the second argument is boolean which represents whether you want to write header columns (table column names) to file or not.
	            	}
	            }
	            writer.close();
	            reportGenerated = true;
	            AdminWise.printToConsole("CSV file created succesfully.");
			} else {
				AdminWise.printToConsole("inside html...");
				int rowCount = 0;
				String[] columnHeader = columnDescription.split("\t");
				AdminWise.printToConsole("::::::columnHeader::::"+columnHeader);
				Vector htmlContents = new Vector();
				String err = "";
				String title ="";
				AdminWise.printToConsole("selectedReport..."+selectedReport);
				//if (selectedReport != null && selectedReport != ""  && !selectedReport.equalsIgnoreCase("<Available Reports>"))
				if (selectedReport != null && selectedReport != ""  && !selectedReport.equalsIgnoreCase("<System Reports>") && !selectedReport.equalsIgnoreCase("<Custom Reports>"))
					title = selectedReport;
				else
					title = "General Report";
				htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
				htmlContents.add("<html>");
				htmlContents.add("<head>");
				htmlContents.add("<title>" + title + "</title>");
				htmlContents.add("<style>");
				htmlContents.add("TH {");
				htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#D3D3D3'; font-weight:bold");
				htmlContents.add("}");
				htmlContents.add("TD {");
				htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
				htmlContents.add("}");
				htmlContents.add("</style>");
				htmlContents.add("</head>");
				htmlContents.add("<body BGCOLOR='#F7F7E7'>");
				htmlContents.add("<table border=1 bordercolor='#D3D3D3' width=100% cellspacing=0 cellpadding=0 ><tr><td align=center><font size='4'><b>"+ title + "</b></font></td></tr></table>");
				htmlContents.add("<p></p>");    
				htmlContents.add("<table border = 1   bordercolor='#D3D3D3' width=100% cellspacing=0 cellpadding=0>");
				htmlContents.add("<tr>");
				for (String columnName : columnHeader) {
					htmlContents.add("<td  BGCOLOR=#cccc99>"+columnName+"</td>");
				}
				htmlContents.add("</tr><tr>");
				AdminWise.printToConsole("resultSet ::: "+resultSet);
				if (resultSet != null && resultSet.size() > 0) {
					rowCount = resultSet.size();
					AdminWise.printToConsole("rowCount ::: "+rowCount);
					for (int curRouteInfo = 0; curRouteInfo < resultSet.size(); curRouteInfo++)  {
						try{
							htmlContents.add("<tr>");
							String[] st = (resultSet.get(curRouteInfo).toString()).split("\t");
							for (String columnValue : st) {
								htmlContents.add("<td>"+columnValue+"</td>");
							}
							htmlContents.add("</tr>");
						}catch(Exception ex){
							err = "Error generating report - " + ex.getMessage();
							AdminWise.printToConsole("Error in generating report::"+ex.getMessage());
	
						}	
					}
				}
				htmlContents.add("</table>");
				
				AdminWise.printToConsole("htmlContents  111::: "+htmlContents);
				htmlContents.add("<table><tr><td>Row Count : </td><td>"+rowCount+"</td></tr></table>");
				htmlContents.add("<table><tr><td><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></td></tr></table>");
				htmlContents.add("</body></html>");
				AdminWise.printToConsole("htmlContents  222::: "+htmlContents);
				try{
					//String reportLocation="";
						//reportLocation=loadSaveASHTMLDialog(this);
					VWCUtil.writeListToFile(htmlContents,path, err, "UTF-8");
				}catch(Exception ex){

				}
				reportGenerated = true;
			}
        } catch (Exception e) {
        	AdminWise.printToConsole("Exception  in generating Csv/Html report:" + e.getMessage());
        }
		return reportGenerated;
	}
}
