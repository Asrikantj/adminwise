/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.5 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.common.RouteInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.util.VWRefCreator;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCConstants;
//--------------------------------------------------------------------------
public class VWDocRouteTable extends JTable implements VWConstant ,FocusListener{
    protected VWDocRouteTableData m_data;
    private int type = 0; 
    static VWRoutePanel vwRoutePanel = new VWRoutePanel();
    public VWDocRouteTable(){
        super();
        this.addFocusListener(this);
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWDocRouteTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        //setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       	setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWDocRouteTableData.m_columns.length; k++) {
            TableCellRenderer renderer;
            
            DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWDocRouteTableData.m_columns[k].m_alignment);
            renderer = textRenderer;
            VWDocRouteRowEditor editor=new VWDocRouteRowEditor(this);
            TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWDocRouteTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);        
        SymMouseMotion aSymMouseMotion = new SymMouseMotion();
        addMouseMotionListener(aSymMouseMotion);
        SymKeyListener aSymKeyListener = new SymKeyListener();
        addKeyListener(aSymKeyListener);		
        
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        
        setRowHeight(TableRowHeight);
        
     
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);      
        getColumnModel().getColumn(0).setPreferredWidth(194);
        getColumnModel().getColumn(1).setPreferredWidth(194);
    	getColumnModel().getColumn(2).setPreferredWidth(90);
    	getColumnModel().getColumn(3).setPreferredWidth(90);
    	getColumnModel().getColumn(4).setPreferredWidth(194);    	    	
        setTableResizable();
    }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    /*
	 * Added if and else condition to optimize by passing the routeId to load the documents in route.
	 * zero is passed for available workflow and for other workflows routeid is passed
	 * Enhancement:- Update WorkFlow Date:-05 Jan 2015
	 * 
	 */
    public void focusGained(FocusEvent fe){
    	if(getType()==1){
    		String selectComboValue=AdminWise.adminPanel.routePanel.cmbAvailableRoute.getSelectedItem().toString();
    		if(selectComboValue.equalsIgnoreCase(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")))){
    			Vector docsInRouteDummyAppRemoved = vwRoutePanel.RefreshLoadDocumentsInRoute(0);	    	
    			if( (docsInRouteDummyAppRemoved!=null) && (docsInRouteDummyAppRemoved.size()>0) ){
    				this.addData(docsInRouteDummyAppRemoved);
    			}
    		}else{
    			int selectedRow = getSelectedRow();
    			RouteMasterInfo routeMasterInfo = getRowData(selectedRow);
    			Vector docsInRouteDummyAppRemoved = vwRoutePanel.RefreshLoadDocumentsInRoute(routeMasterInfo.getRouteId());	    	
    			if( (docsInRouteDummyAppRemoved!=null) && (docsInRouteDummyAppRemoved.size()>0) ){
    				this.addData(docsInRouteDummyAppRemoved);
    			}
    		}
    		this.m_data.fireTableDataChanged();
    	}
    }

   /* public void focusGained(FocusEvent fe){
    	//Code modified to get the routeId from combo selected value.
    	RouteInfo selectedRouteObject = (RouteInfo) AdminWise.adminPanel.routePanel.cmbAvailableRoute.getSelectedItem();
    	int routeId = selectedRouteObject.getRouteId();
    	if(getType()==1){
    		Vector docsInRouteDummyAppRemoved = vwRoutePanel.RefreshLoadDocumentsInRoute(routeId);	    	
    		if( (docsInRouteDummyAppRemoved!=null) && (docsInRouteDummyAppRemoved.size()>0) ){
    			this.addData(docsInRouteDummyAppRemoved);
    		}
    		this.m_data.fireTableDataChanged();
    	}
    }*/
	public void focusLost(FocusEvent fe){
		
	}
    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWDocRouteTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWDocRouteTable_LeftMouseClicked(event);
        }
    }    
    //--------------------------------------------------------------------------
    class SymMouseMotion extends MouseMotionAdapter{
        public void mouseDragged(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable){
            	if(event.getModifiers()==event.BUTTON1_MASK){                	
                	try{
            	    	int selectedRow = getSelectedRow();
            	   		if(selectedRow>=0){
            	   			//For Invalid Route
            	   			int routeValidity = getSelectedRowRouteValidity(selectedRow);
            	   			if( routeValidity == VALIDROUTE)
            	   				AdminWise.adminPanel.routePanel.setEnableMode(MODE_ENABLE);
            	   			else if( routeValidity == INVALIDROUTE)
            	   				AdminWise.adminPanel.routePanel.setEnableMode(MODE_ROUTE_DISABLE);
            	   			
            	   			setAcceptRejectButtonsText();
            	   		}
                	}catch(Exception e){
                		System.out.println("Exception in mouseDragged() Method :"+e.toString());
                	}
                }
            }
        }
    }
    //--------------------------------------------------------------------------
    class SymKeyListener extends KeyAdapter{
    	
    	public void keyReleased(KeyEvent ke){
			if((ke.getKeyCode() == KeyEvent.VK_UP) || (ke.getKeyCode() == KeyEvent.VK_DOWN)){
				//AdminWise.printToConsole("up/down");			
				int selectedRow = getSelectedRow();
				if(selectedRow>=0){
		    		//For Invalid Route
		    		int routeValidity = getSelectedRowRouteValidity(selectedRow);
		    		
		    		if(routeValidity!=-1){
		    			AdminWise.adminPanel.routePanel.routeValidity=routeValidity;
		    		}
		    		if( routeValidity == VALIDROUTE)
		    			AdminWise.adminPanel.routePanel.setEnableMode(MODE_ENABLE);
		    		else if( routeValidity == INVALIDROUTE)
		    			AdminWise.adminPanel.routePanel.setEnableMode(MODE_ROUTE_DISABLE);
		    		
		    		setAcceptRejectButtonsText();		    		
		    	}
			}
		}
    	
    }
    //--------------------------------------------------------------------------
    void VWDocRouteTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
    public void setAcceptRejectButtonsText() {
		try{
			int selectedRow = getSelectedRow();
			int selectedRowCount = getSelectedRowCount();
			//AdminWise.printToConsole("selectedRowCount :: "+selectedRowCount);
			if(selectedRowCount > 1){
    			AdminWise.adminPanel.routePanel.BtnAccept.setText(VWCConstants.DEFAULT_ACCEPT_LABEL);
    			AdminWise.adminPanel.routePanel.BtnReject.setText(VWCConstants.DEFAULT_REJECT_LABEL);
    		}else{
    			RouteMasterInfo routeMasterInfo = getRowData(selectedRow);
    			AdminWise.adminPanel.routePanel.BtnAccept.setText(routeMasterInfo.getAcceptLabel());
    			AdminWise.adminPanel.routePanel.BtnReject.setText(routeMasterInfo.getRejectLabel());
    		}
		}catch (Exception e) {
			AdminWise.adminPanel.routePanel.BtnAccept.setText(VWCConstants.DEFAULT_ACCEPT_LABEL);
			AdminWise.adminPanel.routePanel.BtnReject.setText(VWCConstants.DEFAULT_REJECT_LABEL);			
		}
	}

	//--------------------------------------------------------------------------
    void VWDocRouteTable_RightMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {    	
    	//AdminWise.adminPanel.routePanel.setEnableMode(MODE_ENABLE);
    }
    //--------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
    	int selectedRow = getSelectedRow();
    	if(selectedRow>=0){
    		//For Invalid Route
    		int routeValidity = getSelectedRowRouteValidity(selectedRow);
    		if(routeValidity!=-1){
    			AdminWise.adminPanel.routePanel.routeValidity=routeValidity;
    		}
    		if( routeValidity == VALIDROUTE)
    			AdminWise.adminPanel.routePanel.setEnableMode(MODE_ENABLE);
    		else if( routeValidity == INVALIDROUTE)
    			AdminWise.adminPanel.routePanel.setEnableMode(MODE_ROUTE_DISABLE);
    		
    		setAcceptRejectButtonsText();
    	}
    }
    //--------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
    	int selectedRow = getSelectedRow();
    	getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
    	//For Invalid Route
    	int routeValidity = getSelectedRowRouteValidity(selectedRow);
    	if(routeValidity!=-1){
    		AdminWise.adminPanel.routePanel.routeValidity=routeValidity;
    	}
    	if(selectedRow>=0){
    		if(routeValidity == VALIDROUTE)
   				AdminWise.adminPanel.routePanel.setEnableMode(MODE_ENABLE);
   			else if(routeValidity == INVALIDROUTE)
   				AdminWise.adminPanel.routePanel.setEnableMode(MODE_ROUTE_DISABLE);
    		loadDocumentInDTC(row);
    	}
    }
    
    //--------------------------------------------------------------------------
    private void loadDocumentInDTC(int row)
    {
    	int docId = this.getRowDocLockId(row);
        VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        String room = vwroom.getName();
    	try {      
            //create a temp folder to place reference file in it
        	File folder = new File("c:\\VWReferenceFile\\");
            folder.mkdirs();
            //create a reference file to open document
            File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);
            
            //open reference file in ViewWise thus opening doc                        
            try{
                Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
                //String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
                String dtcPath = VWUtil.getHome();
                Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
            }catch(Exception ex){
            	ex.printStackTrace();
            	//JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
            }
        }
        catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    }
    //--------------------------------------------------------------------------
    /*public void addData(List list) {
        m_data.setData(list);
        m_data.fireTableDataChanged();
    }*/
    public void addData(Vector docs )
    {
          //m_data.setData(docs,roomId,roomName);
    		m_data.setData(docs);
    		/*if(type == 2){
            	//task name and task description are not applicable for this type
    	        getColumnModel().getColumn(3).setPreferredWidth(0); 
    			getColumnModel().getColumn(3).setMinWidth(0);
    			getColumnModel().getColumn(3).setMaxWidth(0);
    			setBackground(Color.white);
    			getColumnModel().getColumn(4).setPreferredWidth(0); 
    			getColumnModel().getColumn(4).setMinWidth(0);
    			getColumnModel().getColumn(4).setMaxWidth(0);
    			removeColumn(getColumnModel().getColumn(4));
    			removeColumn(getColumnModel().getColumn(3));
    			m_data.fireTableStructureChanged();
    			setBackground(Color.white);
            }else{
            	try{
	            	System.out.println("coming here add data");
	            	getColumnModel().getColumn(3).setPreferredWidth(194);
	            	getColumnModel().getColumn(3).setMinWidth(15);
	            	getColumnModel().getColumn(4).setPreferredWidth(194);
	            	getColumnModel().getColumn(4).setMinWidth(15);
	            	setAutoCreateColumnsFromModel(true);
	            	addColumn(getColumnModel().getColumn(3));
	            	addColumn(getColumnModel().getColumn(4));
	            	m_data.fireTableStructureChanged();
            	}catch(Exception e){
            		System.out.println("Error"+e.toString());
            	}
            }*/
    		m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    /*public void addData(String[][] data) {
        m_data.setData(data);
        m_data.fireTableDataChanged();
    }*/
    //--------------------------------------------------------------------------
    /*public String[][] getData() {
        return m_data.getData();
    }*/
    public Vector getData()
    {
          return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    /*public String[] getRowData(int rowNum) {
        return m_data.getRowData(rowNum);
    }*/
    public RouteMasterInfo getRowData(int rowNum)
    {
          return m_data.getRowData(rowNum);
    }
    /*public RouteMasterInfo getRowData(int rowNum) {
        
        try
        {
            return (RouteMasterInfo)m_vector.elementAt(rowNum);
        }
        catch(Exception e)
        {
            return null;
        } 
      }*/

    //--------------------------------------------------------------------------
    public int getRowDocLockId(int rowNum) {
    	RouteMasterInfo row=m_data.getRowData(rowNum);
        if(row==null) return 0;
        return row.getDocId();
        //String[] row=m_data.getRowData(rowNum);
        //return VWUtil.to_Number((row[0]));
    }
    //--------------------------------------------------------------------------
    public String getRowDocName(int rowNum) {
    	RouteMasterInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getDocName();
        //String[] row=m_data.getRowData(rowNum);
        //return row[1];
    }
    //--------------------------------------------------------------------------
    //For Invalid Route
    public int getSelectedRowRouteValidity(int rowNum) {
    	RouteMasterInfo routeMasterInfo =m_data.getRowData(rowNum);
    	if(routeMasterInfo==null) return -1;
    	return routeMasterInfo.getRouteValidity();
    }
    //--------------------------------------------------------------------------
    public int getRowType() {
        return getRowType(getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public int getRowType(int rowNum) {
        //String[] row=m_data.getRowData(rowNum);
    	RouteMasterInfo row=m_data.getRowData(rowNum);
    	//change it later
    	return row.getLevelSeq();
        //return VWUtil.to_Number((row[6]));
    }
    //--------------------------------------------------------------------------
    public void clearData() {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------

	/**
	 * @return Returns the type.
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type The type to set.
	 */
	public void setType(int type) {
		this.type = type;
	}
}

//--------------------------------------------------------------------------
class DocRouteColumnData {
    public String  m_title;
    float m_width;
    int m_alignment;
    
    public DocRouteColumnData(String title, float width, int alignment) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
    }
}
//--------------------------------------------------------------------------
class VWDocRouteTableData extends AbstractTableModel {
    public static final DocRouteColumnData m_columns[] = {
        new DocRouteColumnData(VWConstant.RouteColNames[0],0.3F,JLabel.LEFT ),
        new DocRouteColumnData(VWConstant.RouteColNames[1],0.1F, JLabel.LEFT),
        new DocRouteColumnData(VWConstant.RouteColNames[2],0.1F, JLabel.LEFT),
        new DocRouteColumnData(VWConstant.RouteColNames[3],0.1F, JLabel.LEFT),
        new DocRouteColumnData(VWConstant.RouteColNames[4],0.1F,JLabel.LEFT),
        new DocRouteColumnData(VWConstant.RouteColNames[5],0.2F,JLabel.LEFT),
        new DocRouteColumnData(VWConstant.RouteColNames[6],0.2F,JLabel.LEFT),
        new DocRouteColumnData(VWConstant.RouteColNames[7],0.2F,JLabel.LEFT)
        //new DocRouteColumnData(VWConstant.RouteColNames[7],0.2F,JLabel.LEFT),//COMMENTS
        //new DocRouteColumnData(VWConstant.RouteColNames[5],0.2F,JLabel.LEFT)
    };
    
    public static final int COL_DOCNAME = 0;
    public static final int COL_ROUTENAME = 1;
    public static final int COL_USERNAME = 2;
    public static final int COL_TASK = 5;
    public static final int COL_TASK_DESC = 6;
    //public static final int COL_LOCATION = 3;
    public static final int COL_RECEIVEDDATE = 3;
    public static final int COL_STATUS = 4;
    //public static final int COL_COMMENTS = 7;
    public static final int COL_USERSTATUS = 7;
    
    protected int m_sortCol = 0;
    protected boolean m_sortAsc = true;
    
    protected VWDocRouteTable m_parent;
    protected Vector m_vector;
    
    public VWDocRouteTableData(VWDocRouteTable parent) {
        m_parent = parent;
        m_vector = new Vector();
    }
    //--------------------------------------------------------------------------
    /*public void setData(List data) {
        m_vector.removeAllElements();
        if (data==null) return;
        int count =data.size();
        for(int i=0;i<count;i++){
            //m_vector.addElement(new DocRouteRowData((String)data.get(i)));
        	m_vector.addElement(new RouteMasterInfo((String)data.get(i)));
        }
    }
    //--------------------------------------------------------------------------
    public void setData(String[][] data) {
        m_vector.removeAllElements();
        int count =data.length;
        for(int i=0;i<count;i++) {
        	DocRouteRowData row =new DocRouteRowData(data[i]);
            if(i==2)
                m_vector.addElement(new Boolean(true));
            else
                m_vector.addElement(row);
        }
    }*/
    public void setData(Vector docs) {
        m_vector.removeAllElements();
        if(docs==null || docs.size()==0)    return;
        int count =docs.size();
        int freezed = -1;
        for(int i=0;i<count;i++)
        {
        	RouteMasterInfo docInRoute=(RouteMasterInfo)docs.get(i);
        	//docInRoute.setSessionId(roomId);
        	//docInRoute.setRoomName(roomName);
            //freezed = VWWeb.gConnector.ViewWiseClient.isDocFreezed(roomId,docInRoute);
            //if(freezed == 1)
            //doc.setFreezed(true);        
            //doc.setTag(searchString);
            m_vector.addElement(docInRoute);
        }
      }
    //--------------------------------------------------------------------------
    /*public String[][] getData() {
        int count=getRowCount();
        String[][] data=new String[count][7];
        for(int i=0;i<count;i++) {
        	DocRouteRowData row=(DocRouteRowData)m_vector.elementAt(i);            
            data[i][0]=row.m_DocName;
            data[i][1]=row.m_RouteName;
            data[i][2]=row.m_UserName;
            data[i][3]=row.m_Location;
            data[i][4]=row.m_Status;
            data[i][5]=row.m_Comments;
            
        }
        
        return data;
    }*/
    public Vector getData() {
        return m_vector;
      }
    //--------------------------------------------------------------------------
    /*public String[] getRowData(int rowNum) {
        String[] DocLockRowData=new String[7];
        DocRouteRowData row=(DocRouteRowData)m_vector.elementAt(rowNum);        
        DocLockRowData[0]=row.m_DocName;
        DocLockRowData[1]=row.m_RouteName;
        DocLockRowData[2]=row.m_UserName;
        DocLockRowData[3]=row.m_Location;
        DocLockRowData[4]=row.m_Status;
        DocLockRowData[5]=row.m_Comments;

        return DocLockRowData;
    }*/
    public RouteMasterInfo getRowData(int rowNum) {
        
        try
        {
            return (RouteMasterInfo)m_vector.elementAt(rowNum);
        }
        catch(Exception e)
        {
            return null;
        } 
      }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return m_vector==null ? 0 : m_vector.size();
    }
    //--------------------------------------------------------------------------
    public int getColumnCount() {
        return m_columns.length;
    }
    //--------------------------------------------------------------------------
    public String getColumnName(int column) {
    	String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
        return str;
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
        return true;
    }
    //--------------------------------------------------------------------------
    public Object getValueAt(int nRow, int nCol) {
        if (nRow < 0 || nRow>=getRowCount())
            return "";
        //DocRouteRowData row = (DocRouteRowData)m_vector.elementAt(nRow);
        RouteMasterInfo row = (RouteMasterInfo)m_vector.elementAt(nRow);
        switch (nCol) {        	
            case COL_DOCNAME:     return row.getDocName();
            case COL_ROUTENAME:     return row.getRouteName();
            case COL_USERNAME:    return row.getRouteUsername();
            case COL_TASK:    return row.getTaskName();
            case COL_TASK_DESC:    return row.getTaskDescription();
            case COL_RECEIVEDDATE:    return row.getReceivedDate();
            case COL_STATUS:    return row.getStatus();
            case COL_USERSTATUS: 	return row.getUserStatus();
        }
        return "";
    }
    //--------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
        if (nRow < 0 || nRow >= getRowCount())
            return;
        //DocRouteRowData row = (DocRouteRowData)m_vector.elementAt(nRow);
        RouteMasterInfo row = (RouteMasterInfo)m_vector.elementAt(nRow);
        String svalue = value.toString();
        
        switch (nCol) {
            case COL_DOCNAME:
                row.setDocName(svalue);
                break;
            case COL_ROUTENAME:
                row.setRouteName(svalue);
                break;                
            case COL_USERNAME:
                row.setRouteUsername(svalue);
                break;
            case COL_TASK:
                row.setTaskName(svalue);
                break;
            case COL_TASK_DESC:
                row.setTaskDescription(svalue);
                break;
            case COL_RECEIVEDDATE:
                row.setReceivedDate(svalue);
                break;
            case COL_STATUS:
                row.setStatus(svalue);
                break;
            case COL_USERSTATUS:
            	row.setUserStatus(svalue);
            	break;

            //case COL_COMMENTS:
             //   row.m_Comments = svalue;
              //  break;
                
        }
    }
    //--------------------------------------------------------------------------
    public void clear() {
        m_vector.removeAllElements();
    }
    //--------------------------------------------------------------------------
    /*public void insert(DocRouteRowData AdvanceSearchRowData)
    {
        m_vector.addElement(AdvanceSearchRowData);
    }*/
    public void insert(int row,RouteMasterInfo document){
        if (row < 0)
          row = 0;
        if (row > m_vector.size())
          row = m_vector.size();
        m_vector.insertElementAt(document, row);
      }
      //----------------------------------------------------------------------------
      public void insert(RouteMasterInfo rowData) {
        
        m_vector.addElement(rowData);
      }
    //--------------------------------------------------------------------------
    public boolean remove(int row){
        if (row < 0 || row >= m_vector.size())
            return false;
        m_vector.remove(row);
        return true;
    }
    //--------------------------------------------------------------------------
    class DocRouteComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public DocRouteComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof RouteMasterInfo) || !(o2 instanceof RouteMasterInfo))
                return 0;
            RouteMasterInfo s1 = (RouteMasterInfo)o1;
            RouteMasterInfo s2 = (RouteMasterInfo)o2;
            int result = 0;
            double d1, d2;
            switch (m_sortCol) {           
            	case COL_DOCNAME:
                    result = s1.getDocName().toLowerCase().compareTo(s2.getDocName().toLowerCase());
                    break;
                case COL_ROUTENAME:
                   result = s1.getRouteName().toLowerCase().compareTo(s2.getRouteName().toLowerCase());
                   break;            	
                case COL_USERNAME:
               	result = s1.getRouteUsername().toLowerCase().compareTo(s2.getRouteUsername().toLowerCase());
                   break;
                case COL_RECEIVEDDATE:
               	result = s1.getReceivedDate().toLowerCase().compareTo(s2.getReceivedDate().toLowerCase());  
                   break;
                case COL_STATUS:
               	result = s1.getStatus().toLowerCase().compareTo(s2.getStatus().toLowerCase());  
                   break;
                case COL_USERSTATUS:
                	result = s1.getUserStatus().toLowerCase().compareTo(s2.getUserStatus().toLowerCase());
                	break;
                case COL_TASK:
                	result = s1.getTaskName().toLowerCase().compareTo(s2.getTaskName().toLowerCase());
                	break;
                case COL_TASK_DESC:
                	result = s1.getTaskDescription().toLowerCase().compareTo(s2.getTaskDescription().toLowerCase());
                	break;
            }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof DocRouteComparator) {
            	DocRouteComparator compObj = (DocRouteComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    } 
    
    class ColumnListener extends MouseAdapter {
        protected VWDocRouteTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWDocRouteTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
            
   /*          if(e.getModifiers()==e.BUTTON3_MASK)
                selectViewCol(e);
            else */
        	if(e.getModifiers()==e.BUTTON1_MASK){
        		//Desc   :Getting the selected document id, and after sorting retaining the selection for the same document.
        		//Author :Nishad Nambiar
        		//Date   :10-Oct-2007
                
        		try{
	            	int selectedRow = m_table.getSelectedRow();
	            	RouteMasterInfo curRouteMasterInfo = m_table.m_data.getRowData(selectedRow);
	            	sortCol(e);
	            	int docId = curRouteMasterInfo.getDocId();
	            	int userId = curRouteMasterInfo.getRouteUserId();
	            	highlightRouteDocumentsTableRow(docId, userId);
        		}catch(Exception ex){
        			//System.out.println("Exception in VWDocRouteTable.mouseClicked() method :"+ex.getMessage());
        		}
            }
        }

		//Desc   :This method gets the document id, and retaining the selection for the same document in the table.
		//Author :Nishad Nambiar
		//Date   :10-Oct-2007
        public void highlightRouteDocumentsTableRow(int docId, int userId) {
      	  int rowToSelect=0;
      	    for(int docCount = 0; docCount <  m_table.m_data.m_vector.size(); docCount++){
      	    	RouteMasterInfo curRouteMasterInfo = (RouteMasterInfo)  m_table.m_data.getRowData(docCount);
      	    	if(curRouteMasterInfo.getDocId() == docId && curRouteMasterInfo.getRouteUserId() == userId){
      	    		rowToSelect = docCount;
      	    		break;
      	    	}
      	    }
      	    ListSelectionModel selectionModel = m_table.getSelectionModel();
      	    selectionModel.setSelectionInterval(rowToSelect, rowToSelect);
        }  
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {    	 
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new DocRouteComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWDocRouteTableData.this));
            m_table.repaint();
        }
   }    
}