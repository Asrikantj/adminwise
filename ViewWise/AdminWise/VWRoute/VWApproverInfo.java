/**
 * 
 */
package ViewWise.AdminWise.VWRoute;

import java.io.Serializable;

/**
 * @author Nishad Nambiar
 *
 */
public class VWApproverInfo implements Serializable {
	public boolean approverEmailFlag=false;
	public boolean approverActionFlag=false;
	public String approverName=null;
	public String approverEmail=null;
	public String approverId=null;
	public String approverAction=null;
	public String approverTaskDescription=null;
	public String approverTaskLength=null;
	public String approverESignature=null;
	public String approverTask=null;
	public String approverType=null;
	public String approverPrincipalId=null;
	
	//Escalation enhacement
	public String approverTaskExpiration=null;
	public String approverTaskEscalationUserName = null;
	public String taskNotify=null;
	public String approverTaskEscalationId = null;
	/**
	 * @return Returns the approverAction.
	 */
	public String getApproverAction() {
		return approverAction;
	}
	/**
	 * @param approverAction The approverAction to set.
	 */
	public void setApproverAction(String approverAction) {
		this.approverAction = approverAction;
	}
	/**
	 * @return Returns the approverEmail.
	 */
	public String getApproverEmail() {
		return approverEmail;
	}
	/**
	 * @param approverEmail The approverEmail to set.
	 */
	public void setApproverEmail(String approverEmail) {
		this.approverEmail = approverEmail;
	}
	/**
	 * @return Returns the approverId.
	 */
	public String getApproverId() {
		return approverId;
	}
	/**
	 * @param approverId The approverId to set.
	 */
	public void setApproverId(String approverId) {
		this.approverId = approverId;
	}
	/**
	 * @return Returns the approverName.
	 */
	public String getApproverName() {
		return approverName;
	}
	/**
	 * @param approverName The approverName to set.
	 */
	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}
	/**
	 * @return Returns the approverESignature.
	 */
	public String getApproverESignature() {
		return approverESignature;
	}
	/**
	 * @param approverESignature The approverESignature to set.
	 */
	public void setApproverESignature(String approverESignature) {
		this.approverESignature = approverESignature;
	}
	/**
	 * @return Returns the approverTaskDescription.
	 */
	public String getApproverTaskDescription() {
		return approverTaskDescription;
	}
	/**
	 * @param approverTaskDescription The approverTaskDescription to set.
	 */
	public void setApproverTaskDescription(String approverTaskDescription) {
		this.approverTaskDescription = approverTaskDescription;
	}
	/**
	 * @return Returns the approverTaskLength.
	 */
	public String getApproverTaskLength() {
		return approverTaskLength;
	}
	/**
	 * @param approverTaskLength The approverTaskLength to set.
	 */
	public void setApproverTaskLength(String approverTaskLength) {
		this.approverTaskLength = approverTaskLength;
	}
	/**
	 * @return Returns the approverActionFlag.
	 */
	public boolean getApproverActionFlag() {
		return approverActionFlag;
	}
	/**
	 * @param approverActionFlag The approverActionFlag to set.
	 */
	public void setApproverActionFlag(boolean approverActionFlag) {
		this.approverActionFlag = approverActionFlag;
	}
	/**
	 * @return Returns the approverEmailFlag.
	 */
	public boolean getApproverEmailFlag() {
		return approverEmailFlag;
	}
	/**
	 * @param approverEmailFlag The approverEmailFlag to set.
	 */
	public void setApproverEmailFlag(boolean approverEmailFlag) {
		this.approverEmailFlag = approverEmailFlag;
	}
	/**
	 * @return Returns the approverTask.
	 */
	public String getApproverTask() {
		return approverTask;
	}
	/**
	 * @param approverTask The approverTask to set.
	 */
	public void setApproverTask(String approverTask) {
		this.approverTask = approverTask;
	}
	/**
	 * @return Returns the approverType.
	 */
	public String getApproverType() {
		return approverType;
	}
	/**
	 * @param approverType The approverType to set.
	 */
	public void setApproverType(String approverType) {
		this.approverType = approverType;
	}
	/**
	 * @return Returns the approverPrincipalId.
	 */
	public String getApproverPrincipalId() {
		return approverPrincipalId;
	}
	/**
	 * @param approverPrincipalId The approverPrincipalId to set.
	 */
	public void setApproverPrincipalId(String approverPrincipalId) {
		this.approverPrincipalId = approverPrincipalId;
	}
	public String getApproverTaskEscalationId() {
		return approverTaskEscalationId;
	}
	public void setApproverTaskEscalationId(String approverTaskEscalationId) {
		this.approverTaskEscalationId = approverTaskEscalationId;
	}
	public String getApproverTaskEscalationUserName() {
		return approverTaskEscalationUserName;
	}
	public void setApproverTaskEscalationUserName(String approverTaskEscalationUserName) {
		this.approverTaskEscalationUserName = approverTaskEscalationUserName;
	}
	public String getApproverTaskExpiration() {
		return approverTaskExpiration;
	}
	public void setApproverTaskExpiration(String approverTaskExpiration) {
		this.approverTaskExpiration = approverTaskExpiration;
	}
	public String getTaskNotify() {
		return taskNotify;
	}
	public void setTaskNotify(String taskNotify) {
		this.taskNotify = taskNotify;
	}
	}
