/*
 A basic implementation of the JDialog class.
 */

package ViewWise.AdminWise.VWRoute;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Point;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.prefs.Preferences;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWCheckListListener;
import ViewWise.AdminWise.VWUtil.VWItemCheckEvent;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

public class VWDlgAddDynamicUserIndex extends javax.swing.JDialog implements
VWConstant {

	boolean cancelFlag = true;
	boolean loadedList = true;
	boolean frameSizeAdjusted = false;

	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	VWCheckList LstIndices = new VWCheckList();
	VWLinkedButton BtnAdd = new VWLinkedButton(0, true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0, true);
	String selectedIndexName = "";


	public VWDlgAddDynamicUserIndex(Frame parent, String selectedIndexName) {
		super(parent);
		//AdminWise.printToConsole("VWDlgAddDynamicUserIndex().....");
		getContentPane().setBackground(AdminWise.getAWColor());
		setTitle(LBL_ADDINDEX_NAME);
		setModal(true);
		getContentPane().setLayout(null);
		setSize(283, 438);
		setVisible(false);

		//Dynamic user index starts with < and ends with >
		if(selectedIndexName.startsWith("<<") && selectedIndexName.endsWith(">>")){
			this.selectedIndexName = selectedIndexName.substring(2, selectedIndexName.length() - 2);
		}

		JLabel1.setText(LBL_LISTINDEX_NAME);
		getContentPane().add(JLabel1);
		JLabel1.setBounds(6, 4, 194, AdminWise.gBtnHeight);

		getContentPane().add(LstIndices);
		LstIndices.setBounds(4, 28, 276, 366);

		BtnAdd.setText(BTN_ADD_NAME);
		getContentPane().add(BtnAdd);
		BtnAdd.setBounds(136, 410, 72, AdminWise.gBtnHeight);
		// BtnAdd.setBackground(java.awt.Color.white);
		BtnAdd.setIcon(VWImages.OkIcon);

		BtnCancel.setText(BTN_CANCEL_NAME);
		getContentPane().add(BtnCancel);
		BtnCancel.setBounds(208, 410, 72, AdminWise.gBtnHeight);
		// BtnCancel.setBackground(java.awt.Color.white);
		BtnCancel.setIcon(VWImages.CloseIcon);

		Vector textIndices = null;
		try {
			loadedList = false;
			textIndices = getIndices();
			LstIndices.loadData(textIndices);
			loadedList = true;
		} catch (Exception e) {
			loadedList = true;
		}

		int itemIndex = getSelectedItemIndex(textIndices);
		if(itemIndex != -1)
			LstIndices.setItemCheck(itemIndex, true);

		SymAction lSymAction = new SymAction();
		BtnAdd.addActionListener(lSymAction);
		BtnCancel.addActionListener(lSymAction);

		SymKey aSymKey = new SymKey();
		addKeyListener(aSymKey);
		LstIndices.addKeyListener(aSymKey);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		setResizable(false);

		LstIndices.getList().getSelectionModel().addListSelectionListener(
				new VWListSelectionListener());


		LstIndices.addVWEventListener(new VWCheckListListener() {
			public void VWItemChecked(VWItemCheckEvent event) {
				if (!loadedList)
					return;

				int itemIndex = event.getItemIndex();
				int selectedItemsCount = LstIndices.getselectedItemsCount();
				if(selectedItemsCount > 1){
					LstIndices.setItemCheck(itemIndex, false);
				}				
			}

			public void VWItemUnchecked(VWItemCheckEvent event) {
				if (!loadedList)
					return;
			}
		});

		setCurrentLocation();
		setVisible(true);
	}

	private int getSelectedItemIndex(Vector textIndices) {
		int itemIndex = -1;
		if(textIndices != null && textIndices.size() > 0){
			for(int i=0; i<textIndices.size(); i++){
				VWIndexRec indexRec = (VWIndexRec) textIndices.get(i);
				if(indexRec.getName().equalsIgnoreCase(selectedIndexName)){
					itemIndex = i;
					break;
				}
			}
		}
		return itemIndex;
	}

	// ------------------------------------------------------------------------------
	class VWListSelectionListener implements
	javax.swing.event.ListSelectionListener {
		public void valueChanged(javax.swing.event.ListSelectionEvent e) {
			if (!loadedList)
				return;
			VWIndexRec selItem = (VWIndexRec) LstIndices.getList().getSelectedValue();			
		}
	}

	// ------------------------------------------------------------------------------
	class SymWindow extends java.awt.event.WindowAdapter {
		public void windowClosing(java.awt.event.WindowEvent event) {
			Object object = event.getSource();
			if (object == VWDlgAddDynamicUserIndex.this)
				Dialog_windowClosing(event);
		}
	}

	// ------------------------------------------------------------------------------
	void Dialog_windowClosing(java.awt.event.WindowEvent event) {
		BtnCancel_actionPerformed(null);
	}

	// ------------------------------------------------------------------------------
	class SymKey extends java.awt.event.KeyAdapter {
		public void keyTyped(java.awt.event.KeyEvent event) {
			Object object = event.getSource();
			if (event.getKeyText(event.getKeyChar()).equals("Enter"))
				BtnAdd_actionPerformed(null);
			else if (event.getKeyText(event.getKeyChar()).equals("Escape"))
				BtnCancel_actionPerformed(null);
		}
	}

	// ------------------------------------------------------------------------------
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnAdd)
				BtnAdd_actionPerformed(event);
			else if (object == BtnCancel)
				BtnCancel_actionPerformed(event);

		}
	}

	// ------------------------------------------------------------------------------
	public void addNotify() {
		// Record the size of the window prior to calling parents addNotify.
		Dimension size = getSize();

		super.addNotify();

		if (frameSizeAdjusted)
			return;
		frameSizeAdjusted = true;

		// Adjust size of frame according to the insets
		Insets insets = getInsets();
		setSize(insets.left + insets.right + size.width, insets.top
				+ insets.bottom + size.height);
	}

	// ------------------------------------------------------------------------------

	// ------------------------------------------------------------------------------
	void BtnAdd_actionPerformed(java.awt.event.ActionEvent event) {
		//saveDlgOptions();
		int selectedIemsCount = LstIndices.getselectedItemsCount();

		if(selectedIemsCount == 0){
			VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWDlgAddDynamicUser.msg_0"),VWMessage.DIALOG_TYPE);
			return;
		}
		cancelFlag = false;
		this.setVisible(false);
	}

	// ------------------------------------------------------------------------------
	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
		//saveDlgOptions();
		cancelFlag = true;
		this.setVisible(false);
	}

	// ------------------------------------------------------------------------------
	public List getValues() {
		List selIndices = new LinkedList();
		int count = LstIndices.getItemsCount();
		for (int i = 1; i < count; i++)
			if (LstIndices.getItemIsCheck(i))
				selIndices.add(LstIndices.getItem(i));
		return selIndices;
	}

	// ------------------------------------------------------------------------------
	public boolean getCancelFlag() {
		return cancelFlag;
	}

	// ------------------------------------------------------------------------------
	private Vector getIndices() {
		Vector textIndices = new Vector();
		VWIndexRec[] indices = VWDocTypeConnector.getIndices(false);
		if (indices == null || indices.length == 0)
			return null;
		int count = indices.length;

		for (int i = 0; i < count; i++) {
			if (indices[i].getType() == TEXT_CONDS_INDEX){
				textIndices.add(indices[i]);
			}
		}
		return textIndices;
	}

	// ------------------------------------------------------------------------------
	private void saveDlgOptions() {
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		if (AdminWise.adminPanel.saveDialogPos) {
			prefs.putInt("x", this.getX());
			prefs.putInt("y", this.getY());
		} else {
			prefs.putInt("x", 50);
			prefs.putInt("y", 50);
		}
	}

	// ------------------------------------------------------------------------------
	private void getDlgOptions() {
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		//setLocation(prefs.getInt("x", 50), prefs.getInt("y", 50));
		if((prefs.getInt("x", 0) == 0) || prefs.getInt("y", 0) == 0){
			Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width, 40);
			setLocation(point);
		}else{
			setLocation(prefs.getInt("x", 50), prefs.getInt("y", 50));
		}

	}

	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width+20, 180);
		setLocation(point);
	}

	// ------------------------------------------------------------------------------

	public VWIndexRec getSelectedIndex() {
		VWIndexRec selectedIndex = null;
		try{
			List selectedItemList = LstIndices.getSelectedItems();

			if(selectedItemList != null && selectedItemList.size() > 0){
				selectedIndex = (VWIndexRec) selectedItemList.get(0);

			}
		}catch (Exception e) {
			AdminWise.printToConsole("Exception in VWDlgAddDynamicUser : "+e.getMessage());
		}
		return selectedIndex;
	}

}