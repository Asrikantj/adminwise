package ViewWise.AdminWise.VWRoute;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.computhink.common.Constants;
import com.computhink.common.DocType;
import com.computhink.common.Index;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWRoom.VWRoom;

public class VWRouteConnector implements VWConstant {
	
	public static Vector getDRSPrincipals(int members, String userOrGrpName) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector principals = new Vector();
		AdminWise.gConnector.ViewWiseClient.getDRSPrincipals(room.getId(), members, userOrGrpName, principals);
        if(principals==null || principals.size()==0) return null;
        return principals;
    }
	
	public static boolean isUniqueStartLocation(String nodeId, int routeId){
		boolean result = true;
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		int isExist = AdminWise.gConnector.ViewWiseClient.isRouteLocationFound(room.getId(), nodeId);
		if (isExist == 0) return true;
		if (isExist != routeId) return false;
		if (routeId == 0 && isExist > 0) return false;
		return result;
	}
	
	public static boolean isUniqueRouteName(String routeName, int routeId){
		boolean result = true;
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		int isExist = AdminWise.gConnector.ViewWiseClient.isRouteNameFound(room.getId(), routeName);
		if (isExist == 0) return true;
		if (isExist != routeId) return false;
		if (routeId == 0 && isExist > 0) return false;			 
		return result;
	}
	
	public static int isVWSignInstalled(int roomId)
	{ 
		return AdminWise.gConnector.ViewWiseClient.isVWSignInstalled(roomId);
	}

	public static List getDTIndices(int docTypeId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        DocType dt=new DocType(docTypeId);
        List<VWIndexRec> indices=new LinkedList<VWIndexRec>();
        try
        {
            AdminWise.adminPanel.setWaitPointer();    
            AdminWise.gConnector.ViewWiseClient.getDocTypeIndices(room.getId(), dt, Constants.WORKFLOW_MODULE_NAME);
            if(dt.getIndices()==null || dt.getIndices().size()==0) return null;
            int count=dt.getIndices().size();
            for(int i =0;i<count;i++) indices.add(new VWIndexRec((Index)dt.getIndices().get(i), false));
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return indices;
    }
	public static VWIndexRec[] getIndices(boolean withFixedFields) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector indicesList=new Vector();
        VWIndexRec[] indices=null;
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getIndices(room.getId(),0, indicesList);
            if(indicesList==null || indicesList.size()==0) return null;
            List recList=new LinkedList();
            int count=indicesList.size();
            int begin=0;
            /*
            if(withFixedFields)
            {
                begin=FixedFields.length;
                indices=new VWIndexRec[count+begin];
                for(int i=0;i<FixedFields.length;i++) recList.add(FixedFields[i]);
            }
            else
            {
             */
            indices=new VWIndexRec[count];
                /*
            }
                 */
            for(int i=0;i<count;i++) recList.add(new VWIndexRec((String)indicesList.get(i), true, false));
            recList=sortList(recList);
            for(int i=0;i<recList.size();i++) indices[i]=(VWIndexRec)recList.get(i);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return indices;
    }
	private static List sortList(List sList) {
        List newList=new LinkedList();
        int count = sList.size();
        String str="",strTmp="";
        for(int i=0;i<count;i++) {
            str=((VWIndexRec)sList.get(i)).getName();
            if (str!=null) {
                str=str.trim();
                if(newList.size()==0) {
                    newList.add(sList.get(i));
                }
                else {
                    boolean inserted=false;
                    strTmp=str.toLowerCase();
                    for(int j=0;j<newList.size();j++)
                        if(strTmp.compareTo((((VWIndexRec)newList.get(j)).
                            getName()).toLowerCase()) < 0) {
                            newList.add(j,sList.get(i));
                            inserted=true;
                            break;
                        }
                    if(!inserted) newList.add(sList.get(i));
                }
            }
        }
        return newList;
    }
}
