/**
 * 
 */
package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;

import com.computhink.common.Constants;
import com.computhink.common.Node;
import com.computhink.common.RouteInfo;
import com.computhink.common.Util;
import com.computhink.common.VWEmailOptionsInfo;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDlgSaveLocation;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import com.computhink.resource.ResourceManager;
/**
 * @author nishad.n
 *
 */
public class VWEndIconProperties  extends JDialog implements VWConstant, Constants{
	
	JComboBox cmbRouteAction = new JComboBox();
	JComboBox cmbIndexName = new JComboBox();
	VWLinkedButton btnDone = new VWLinkedButton(0,true);
	VWLinkedButton btnCancel = new VWLinkedButton(0,true);
	 String languageLocale=AdminWise.getLocaleLanguage();
	JLabel lblActionList = new JLabel(AdminWise.connectorManager.getString("VWEndIconProperties.lblActionList"));
	JLabel lblStatusReport = new JLabel(AdminWise.connectorManager.getString("VWEndIconProperties.lblStatusReport"));
	JLabel actionListlineBorderLabel =  new JLabel("");
	JLabel stausReportlineBorderLabel =  new JLabel("");
	VWLinkedButton btnFinalActionLocation = new VWLinkedButton(2);
	VWLinkedButton btnRejctFoldLocation = new VWLinkedButton(2);
	VWLinkedButton btnFinalReportLocation = new VWLinkedButton(2);
	JTextField txtLocation = new JTextField();
	JPanel pnlComponents = new JPanel();

	JCheckBox chkSendOriginatorNotification =  new JCheckBox();
	/****CV2019 merges from 10.2 line--------------------------------***/
	public static JCheckBox chkCompressDocPages =  new JCheckBox();
	public static JCheckBox chkPdfConversion =  new JCheckBox();
	
	JLabel lblSendOriginatorNotification = new JLabel();
	JLabel lblCompressDocPage = new JLabel();
	JLabel lblConvertoPdf = new JLabel();
	JLabel lblRoomLocation = new JLabel();
	JLabel lblReportLocation = new JLabel();
	JLabel lblRejectFoldLocation = new JLabel();
	JLabel lblCreateNewFolder = new JLabel();
	JTextField txtRejectFoldLocation = new JTextField();
	JTextField txtActionLocation = new JTextField();
	JTextField txtReportLocation = new JTextField();
	JLabel lblUNCInfo = new JLabel();
	JLabel lblAvailableRoute2 = new JLabel();
	JComboBox cmbAvailableAssignRoute = new JComboBox();
	public VWDlgSaveLocation dlgLocation = null;
	private int startLocationNodeId = -1;
	private int rejectLocationNodeId=-1;
	private int selectedEmailOriginator=-1;
	private int selectedCompressDoc=-1;
	private int selectedpdfConversion=-1;
	public static boolean active = false;
	private static int gCurRoom = 0;
	private JCheckBox chkSendMail = new JCheckBox(AdminWise.connectorManager.getString("VWEndIconProperties.chkSendMail"));
	JLabel lblEmailOptions = new JLabel(AdminWise.connectorManager.getString("VWEndIconProperties.lblEmailOptions"));
	JLabel emailOptionslineBorderLabel =  new JLabel("");
	private JTextArea taEMailIds = new JTextArea(2,4);
	private JScrollPane spEMailIds = new JScrollPane(taEMailIds);
	public static ArrayList<VWEmailOptionsInfo> emailOptionsList = null;
	public static Vector routeEmailOptionsInfoList = new Vector();
	
	
	VWEmailOptionsListPanel vwEmailOptionsListPanel = new VWEmailOptionsListPanel();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AdminWise.printToConsole("inside EndIconProperties.....");
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}
		catch(Exception se){
		}
		new VWEndIconProperties();
	}
	
	public VWEndIconProperties(){
		AdminWise.printToConsole("inside EndIconProperties constructor.....");
		setSize(412,415);
		//setSize(412,500);
		initComponents();
		//if(AdminWise.adminPanel.routePanel.CURRENT_MODE==1)
		//PR Comment
		setCurrentLocation();
		setVisible(true);             
		setResizable(false);
	}
	
	public void initComponents(){
		AdminWise.printToConsole("inside initComponents.....");
		getContentPane().setBackground(AdminWise.getAWColor());
		if (active)
		{
			requestFocus();
			toFront();
			return;
		}
		//this.setBackground(Color.orange);
		pnlComponents.setSize(412,400);
		setLayout(null);
		pnlComponents.setLayout(null);
		pnlComponents.setBackground(AdminWise.getAWColor());
		int left = 5;
		lblActionList.setBounds(left,0,75,20);
		lblActionList.setForeground(new Color(7, 38, 58));
		pnlComponents.add(lblActionList);
		
		Border border = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		actionListlineBorderLabel.setBorder(border);
		actionListlineBorderLabel.setBounds(80, 10, 310, 2);
		pnlComponents.add(actionListlineBorderLabel);
		
		loadRouteActions();
		cmbRouteAction.setBounds(left,20,385,20);
		cmbRouteAction.addItemListener(new SymListAction());
		pnlComponents.add(cmbRouteAction);
					
		Border borderStatusReport = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		stausReportlineBorderLabel.setBorder(borderStatusReport);
		stausReportlineBorderLabel.setBounds(88, 100, 302, 2);
		pnlComponents.add(stausReportlineBorderLabel);
		
		lblStatusReport.setBounds(left,90,100,20);
		lblStatusReport.setForeground(new Color(7, 38, 58));
		pnlComponents.add(lblStatusReport);
		
		lblRejectFoldLocation.setText(AdminWise.connectorManager.getString("VWEndIconProperties.lblRejectFold"));
		lblRejectFoldLocation.setBounds(left,45,375,20);
		pnlComponents.add(lblRejectFoldLocation);
		lblRejectFoldLocation.setVisible(true);
		
		
		chkSendOriginatorNotification.setBounds(left,308,15,20);
		chkSendOriginatorNotification.setBackground(AdminWise.getAWColor());
		pnlComponents.add(chkSendOriginatorNotification);
		chkSendOriginatorNotification.setVisible(true);
		chkSendOriginatorNotification.setSelected(true);
		
		
		

		
		lblSendOriginatorNotification.setText(AdminWise.connectorManager.getString("VWEndIconProperties.lblSendOriginatorNotif"));
		lblSendOriginatorNotification.setBounds(left+18,308,375,20);
		pnlComponents.add(lblSendOriginatorNotification);
		lblSendOriginatorNotification.setVisible(true);
		
		lblReportLocation.setText(AdminWise.connectorManager.getString("VWEndIconProperties.setText_1"));
		lblReportLocation.setBounds(left,105,375,20);
		pnlComponents.add(lblReportLocation);
		lblReportLocation.setVisible(true);
		
		

		chkCompressDocPages.setBounds(left+104+27,308,15,20);
		chkCompressDocPages.setBackground(AdminWise.getAWColor());
		pnlComponents.add(chkCompressDocPages);
		chkCompressDocPages.setVisible(true);
		/****CV2019 merges from 10.2 line--------------------------------***/
		chkCompressDocPages.setSelected(false);
		chkCompressDocPages.setEnabled(false);
		
		chkPdfConversion.setBounds(left+104+115+49,308,15,20);
		chkPdfConversion.setBackground(AdminWise.getAWColor());
		pnlComponents.add(chkPdfConversion);
		chkPdfConversion.setVisible(true);
		/****CV2019 merges from 10.2 line--------------------------------***/
		chkPdfConversion.setSelected(false);
		chkPdfConversion.setEnabled(false);
		
		
		
		
		
		lblCompressDocPage.setBounds(125+27,308,250,20);
		lblCompressDocPage.setText("Compress page(s)");
		pnlComponents.add(lblCompressDocPage);
		lblCompressDocPage.setVisible(true);
		
		lblConvertoPdf.setBounds(180+60+49,308,375,20);
		/****CV2019 merges from 10.2 line--------------------------------***/
		lblConvertoPdf.setText("Convert to PDF");
		pnlComponents.add(lblConvertoPdf);
		lblConvertoPdf.setVisible(true);
		
		lblRoomLocation.setText(LBL_UNC_PATH);
		lblRoomLocation.setBounds(5,65,200,20);
		pnlComponents.add(lblRoomLocation);
		lblRoomLocation.setVisible(false);
		txtActionLocation.setBounds(5,77,360,20);
		txtActionLocation.setBackground(Color.WHITE);
		pnlComponents.add(txtActionLocation);
		txtActionLocation.setVisible(false);
		
		lblCreateNewFolder.setText(AdminWise.connectorManager.getString("VWEndIconProperties.setText_2"));
		lblCreateNewFolder.setVisible(false);
		pnlComponents.add(lblCreateNewFolder);
		
		cmbIndexName.setVisible(false);
		cmbIndexName.addItemListener(new SymIndexListAction());
		pnlComponents.add(cmbIndexName);
		//VP Comment 
		loadIndicesList();
		
		txtReportLocation.setText("");
		txtReportLocation.setBounds(left,122,360,20);
		pnlComponents.add(txtReportLocation);
		txtReportLocation.setVisible(true);
		
		
		txtRejectFoldLocation.setText("");
		txtRejectFoldLocation.setBounds(left,62,360,20);
		pnlComponents.add(txtRejectFoldLocation);
		txtRejectFoldLocation.setVisible(true);
		
		btnFinalActionLocation.setIcon(VWImages.BrowseRouteImage);
		btnFinalActionLocation.addActionListener(new SymAction());
		btnFinalActionLocation.setBounds(366, 122, 25,20);
		pnlComponents.add(btnFinalActionLocation);
		btnFinalActionLocation.setVisible(false);
		
		btnFinalReportLocation.setIcon(VWImages.BrowseRouteImage);
		btnFinalReportLocation.addActionListener(new SymAction());
		btnFinalReportLocation.setBounds(366, 122, 25,20);
		pnlComponents.add(btnFinalReportLocation);
		btnFinalReportLocation.setVisible(true);
		
		btnRejctFoldLocation.setIcon(VWImages.BrowseRouteImage);
		btnRejctFoldLocation.addActionListener(new SymAction());
		btnRejctFoldLocation.setBounds(366, 62, 25,20);
		pnlComponents.add(btnRejctFoldLocation);
		btnRejctFoldLocation.setVisible(true);
		
		
		lblUNCInfo.setText(AdminWise.connectorManager.getString("VWEndIconProperties.setText_3"));
		lblUNCInfo.setBounds(left, 85,300,20);
		pnlComponents.add(lblUNCInfo);
		lblUNCInfo.setVisible(false);

		lblAvailableRoute2.setText(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("EndIcon.Name"));
		lblAvailableRoute2.setBounds(left, 45,220,20);
		pnlComponents.add(lblAvailableRoute2);
		lblAvailableRoute2.setVisible(false);
		active = true;
		loadAvailableRoutes(cmbAvailableAssignRoute);
		cmbAvailableAssignRoute.setBounds(left,62,385,20);
		pnlComponents.add(cmbAvailableAssignRoute);
		cmbAvailableAssignRoute.setVisible(false);
				
		/*chkSendMail.setBounds(left,108,300,20);
		pnlComponents.add(chkSendMail);
		chkSendMail.setVisible(true);*/
		
		/*spEMailIds.setBounds(left,130,385,67);
		taEMailIds.setToolTipText("Delimit multiple email ids with �;�");
		taEMailIds.setDocument(new JTextFieldLimit(255));
		pnlComponents.add(spEMailIds);
		spEMailIds.setVisible(true);*/
		
		lblEmailOptions.setBounds(left, 148, 75, 20);
		lblEmailOptions.setForeground(new Color(7, 38, 58));
		pnlComponents.add(lblEmailOptions);
		
		Border borderEmailOptons = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		emailOptionslineBorderLabel.setBorder(borderEmailOptons);
		emailOptionslineBorderLabel.setBounds(80, 158, 310, 2);
		pnlComponents.add(emailOptionslineBorderLabel);

		vwEmailOptionsListPanel.setBounds(left,165,390,143);
		pnlComponents.add(vwEmailOptionsListPanel);
		
		
		
		btnDone.setText(BTN_SAVE_NAME);
		btnDone.setMnemonic('S');
		btnDone.setIcon(VWImages.SaveIcon);
		if(languageLocale.equals("nl_NL")){
			btnDone.setBounds(228-20,308+30,AdminWise.gBtnWidth+10,AdminWise.gBtnHeight);
		}else
			btnDone.setBounds(228,308+30,AdminWise.gBtnWidth,AdminWise.gBtnHeight);	
		btnDone.addActionListener(new SymAction());
		pnlComponents.add(btnDone);
		
		btnCancel.setText(BTN_CANCEL_NAME);
		btnCancel.setMnemonic('C');
		btnCancel.setIcon(VWImages.CloseIcon);
		if(languageLocale.equals("nl_NL")){
			btnCancel.setBounds(311-10,308+30,AdminWise.gBtnWidth+10,AdminWise.gBtnHeight);
		}else
			btnCancel.setBounds(311,308+30,AdminWise.gBtnWidth,AdminWise.gBtnHeight);

		btnCancel.addActionListener(new SymAction());
		pnlComponents.add(btnCancel);
		
		pnlComponents.setBounds(5,10,400,450);
		add(pnlComponents);
		
		setTitle(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWEndIconProperties.setTitle"));
		//PR Comment
		viewMode();
		setModal(true);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent evt){
				closeDialog();
			}
			public void windowLostFocus(WindowEvent e){
				if(active){
					requestFocus();
					toFront();
				}
			}
			public void windowDeactivated(WindowEvent e) {
				if(active){
					requestFocus();
					toFront();
				}
			}
			public void windowStateChanged(WindowEvent e) {
				toFront();
			}
			public void windowDeiconified(WindowEvent e) {
				toFront();
			}
		});
		//PR Comment		
		loadEndRouteInfo();		
	}
	
	protected JRootPane createRootPane() { 
		JRootPane rootPane = new JRootPane();
		//Handle escape key.
		KeyStroke escapeStroke = KeyStroke.getKeyStroke("ESCAPE");
		Action escapeActionListener = new AbstractAction() { 
			public void actionPerformed(ActionEvent actionEvent) {
				/****CV2019 merges from 10.2 line--------------------------------***/
				BtnCancel_actionPerformed();
			} 
		} ;		
		InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(escapeStroke, "ESCAPE");
		rootPane.getActionMap().put("ESCAPE", escapeActionListener);
		//Handle enter key.
		KeyStroke enterStroke = KeyStroke.getKeyStroke("ENTER");
		Action enterActionListener = new AbstractAction() { 
			public void actionPerformed(ActionEvent actionEvent) {
				BtnDone_actionPerformed(actionEvent);
			} 
		} ;
		inputMap.put(enterStroke, "ENTER");
		rootPane.getActionMap().put("ENTER", enterActionListener);

		return rootPane;
	} 

	private void viewMode(){
		AdminWise.printToConsole("current mode in end route:"+AdminWise.adminPanel.routePanel.CURRENT_MODE);
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2){
			VWRoutePanel.viewRouteEndIcon = true;
			btnDone.setEnabled(false);
		}
		else if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 1 || AdminWise.adminPanel.routePanel.CURRENT_MODE == 3
			|| AdminWise.adminPanel.routePanel.CURRENT_MODE == 4)
			btnDone.setEnabled(true);
		else
			btnDone.setEnabled(true);
	}
	
	class SymAction implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(ae.getSource() == btnCancel){
				/****CV2019 merges from 10.2 line--------------------------------***/
				BtnCancel_actionPerformed();
			}
			else if(ae.getSource() == btnDone){
				BtnDone_actionPerformed(ae); 
			}
			else if(ae.getSource()==btnFinalActionLocation){
				BtnRouteLocation_actionPerformed(ae);
			}else if(ae.getSource() == btnFinalReportLocation){
				BtnReportLocation_actionPerformed(ae);
			}else if(ae.getSource()==btnRejctFoldLocation){
				BtnRejectFoldLocation_actionPerformed(ae);
			}
			
		}
	}
	public void closeDialog() 
	{
		/****CV2019 merges from 10.2 line--------------------------------***/
		try{
			BtnCancel_actionPerformed();
			active = false;
			/*setVisible(false);
			dispose();*/
		}catch(Exception ex){
			
		}
	}
	void BtnDone_actionPerformed(java.awt.event.ActionEvent event){
		if (!validateEndIconProperties()){
			return;
		}
		int selectedFinalActionId = cmbRouteAction.getSelectedIndex();
		routeEmailOptionsInfoList = (Vector)VWEmailOptionsListPanel.Table.getData();
		if(routeEmailOptionsInfoList==null || routeEmailOptionsInfoList.size()<=0){
			if(chkCompressDocPages.isSelected()==true||chkPdfConversion.isSelected()==true||selectedFinalActionId == 6){
				//VWMessage.showMessage(this,"Please add one or more email address to compress/pdf conversion.");
				VWMessage.showMessage(VWEmailOptionsListPanel.Table, AdminWise.connectorManager.getString("VWEndIconProperties.msg_1"));
			}
		}
		
		
	/*	if(selectedFinalActionId == 6){//Email Documents
			if(routeEmailOptionsInfoList==null || (routeEmailOptionsInfoList!=null && routeEmailOptionsInfoList.size()==0)){
				VWMessage.showMessage(VWEmailOptionsListPanel.Table, AdminWise.connectorManager.getString("VWEndIconProperties.msg_1"));
				return;
			}
		}*/

		
		String selectedActionLocation  = txtActionLocation.getText().trim();
		int selIndexId = -1;
		if (!cmbIndexName.getSelectedItem().equals("<Index Name>") && !cmbIndexName.getSelectedItem().equals("<Refresh>")) {
			selIndexId =((VWIndexRec)cmbIndexName.getSelectedItem()).getId();
		}
		
		AdminWise.adminPanel.routePanel.routeInfoBean.setRouteActionId(String.valueOf(selectedFinalActionId).trim());
		if(selectedFinalActionId == 1){
			AdminWise.adminPanel.routePanel.routeInfoBean.setIndexId(selIndexId);
			AdminWise.adminPanel.routePanel.routeInfoBean.setActionLocation(startLocationNodeId+"");			
		}
		else if(selectedFinalActionId == 4){
			
			String selectedRoute = cmbAvailableAssignRoute.getSelectedItem().toString();
			if (!selectedRoute.equals("<"+AdminWise.connectorManager.getString("RouteEndIconProps.Available")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("RouteEndIconProps.s")+">")) {
				RouteInfo selectedRouteObject = (RouteInfo) cmbAvailableAssignRoute.getSelectedItem();				
				AdminWise.adminPanel.routePanel.routeInfoBean.setActionLocation(selectedRouteObject.getRouteId()+"");
			}else{
				active = false;
				VWMessage.showMessage(this,VWMessage.ERR_SELECTROUTE_NAME, VWMessage.DIALOG_TYPE);
				active = true;
				cmbAvailableAssignRoute.requestFocus();
				return;
			}
		}else
			AdminWise.adminPanel.routePanel.routeInfoBean.setActionLocation(txtActionLocation.getText().trim());
		
		if(( (selectedFinalActionId >= 1 && selectedFinalActionId < 4)) && selectedActionLocation.length() == 0){
			active = false;
			VWMessage.showMessage(this,VWMessage.ERR_SELECTACTION_LOCATION, VWMessage.DIALOG_TYPE);
			active = true;
			btnFinalActionLocation.requestFocus();
			return;
		}
		//commented for,Bug 391 - Administration : DO NOT generate a status report at end of workflow if the Location is left blank.
		/*if(txtReportLocation.getText().trim().length() == 0){
			active = false;
			VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWEndIconProperties.msg_3"), VWMessage.DIALOG_TYPE);
			active = true;
			btnFinalReportLocation.requestFocus();
			return;
		}*/
		AdminWise.adminPanel.routePanel.routeInfoBean.setReportLocation(txtReportLocation.getText().trim().equalsIgnoreCase("")?"-":txtReportLocation.getText().trim());
		if(rejectLocationNodeId!=-1){
			AdminWise.adminPanel.routePanel.routeInfoBean.setRejectLocation(txtRejectFoldLocation.getText().trim().equalsIgnoreCase("")?"-":rejectLocationNodeId+"");
		}
				
		//Sow: Implement optional notification for Email Originator
		//AdminWise.printToConsole("Flag of Email Originator: "+chkSendOriginatorNotification.isSelected());
		selectedEmailOriginator = chkSendOriginatorNotification.isSelected()==true?1:0;
		selectedCompressDoc = chkCompressDocPages.isSelected() == true?1:0;
		selectedpdfConversion=chkPdfConversion.isSelected()==true?1:0;
		if (selectedEmailOriginator!=-1) {
			//AdminWise.printToConsole("selectedEmailOriginator"+selectedEmailOriginator);
			AdminWise.adminPanel.routePanel.routeInfoBean.setEmailOriginator(selectedEmailOriginator);
			
		}
		if(selectedCompressDoc != -1){
			AdminWise.adminPanel.routePanel.routeInfoBean.setCompressdoc(selectedCompressDoc);
		}
		if(selectedpdfConversion!=-1) {
			AdminWise.adminPanel.routePanel.routeInfoBean.setPdfConversion(selectedpdfConversion);
		}
		
		//Old functionality
		/*if(chkSendMail.isSelected()){
			if(taEMailIds.getText().trim().equals("")){
				active = false;
				VWMessage.showMessage(this,VWMessage.ERR_ENTER_EMAILIDS_NAME, VWMessage.DIALOG_TYPE);
				active = true;
				return;
			}
			AdminWise.adminPanel.routePanel.routeInfoBean.setSendEmailOnRptGen(1);
		}
		else{
			AdminWise.adminPanel.routePanel.routeInfoBean.setSendEmailOnRptGen(0);
		}
		AdminWise.adminPanel.routePanel.routeInfoBean.setEmailIDs(taEMailIds.getText());
		*/
		
		if((routeEmailOptionsInfoList!=null && routeEmailOptionsInfoList.size()>0) && (txtReportLocation.getText().trim().length() == 0)  ){
				active = false;
				//VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWEndIconProperties.msg_3"), VWMessage.DIALOG_TYPE);
				if(VWMessage.showWarningDialog((java.awt.Component) this,AdminWise.connectorManager.getString("VWEndIconProperties.msg_3"))!=javax.swing.JOptionPane.NO_OPTION) return;				
				active = true;
				btnFinalReportLocation.requestFocus();
		}
		
		//Route Email options enhancement
		if(routeEmailOptionsInfoList!=null && routeEmailOptionsInfoList.size()>0){
			AdminWise.adminPanel.routePanel.routeInfoBean.setRouteEmailOptionsInfoList(routeEmailOptionsInfoList);
			AdminWise.adminPanel.routePanel.routeInfoBean.setSendEmailOnRptGen(1);//for old functionality
		}else{
			routeEmailOptionsInfoList = new Vector();
			AdminWise.adminPanel.routePanel.routeInfoBean.setRouteEmailOptionsInfoList(routeEmailOptionsInfoList);
			AdminWise.adminPanel.routePanel.routeInfoBean.setSendEmailOnRptGen(0);
		}
		//Route Email options enhancement end
		
		//Other than final action move location 1, setting the index id to -1
		if(selectedFinalActionId != 1)
			AdminWise.adminPanel.routePanel.routeInfoBean.setIndexId(selIndexId);
		
		setVisible(false);
		dispose();	
	}
	
	public static boolean validateEndIconProperties() {
		try{
			boolean flag = true;
			routeEmailOptionsInfoList = (Vector)VWEmailOptionsListPanel.Table.getData();
			if(routeEmailOptionsInfoList!=null && routeEmailOptionsInfoList.size()>0){
				VWEmailOptionsInfo vwEmailOptionsInfo = null;
				String tempUserEmailStr = "";
				for(int i=0; i<routeEmailOptionsInfoList.size(); i++){//Get the info for each row and column 0
					vwEmailOptionsInfo = (VWEmailOptionsInfo)routeEmailOptionsInfoList.get(i);
					tempUserEmailStr = vwEmailOptionsInfo.getUserEmailAddress();
					if(tempUserEmailStr.length()==0){
						flag = false;
						break;
					}
				}
			}
			if(!flag){
				VWMessage.showMessage(VWEmailOptionsListPanel.Table, AdminWise.connectorManager.getString("VWEndIconProperties.msg_2"));
				return false;
			}
		}catch(Exception ex){
			System.out.println("Exception while validating End icon properties : "+ex.getMessage());
			return false;
		}
		return true;
	}
	/****CV2019 merges - 10.2 Issue fixes***/
	void BtnCancel_actionPerformed(){
		//This should be only for view...
		if(VWRoutePanel.viewRouteEndIcon){
			int selectedFinalActionId = cmbRouteAction.getSelectedIndex();
			if(selectedFinalActionId == 1){
				AdminWise.adminPanel.routePanel.routeInfoBean.setIndexId(AdminWise.adminPanel.routePanel.routeInfoBean.getIndexId());
				AdminWise.adminPanel.routePanel.routeInfoBean.setActionLocation(AdminWise.adminPanel.routePanel.routeInfoBean.getActionLocation().trim());
			}			
		}
		/****CV2019 merges - 10.2 cancel button Issue fixes***/
		AdminWise.adminPanel.routePanel.routeInfoBean.setEmailOriginator(AdminWise.adminPanel.routePanel.routeInfoBean.getEmailOriginator());
		AdminWise.adminPanel.routePanel.routeInfoBean.setCompressdoc(AdminWise.adminPanel.routePanel.routeInfoBean.getCompressdoc());
		AdminWise.adminPanel.routePanel.routeInfoBean.setPdfConversion(AdminWise.adminPanel.routePanel.routeInfoBean.getPdfConversion());			
		AdminWise.adminPanel.routePanel.routeInfoBean.setSendEmailOnRptGen(AdminWise.adminPanel.routePanel.routeInfoBean.getSendEmailOnRptGen());
		if (emailOptionsList != null) {
			setOldEmailOptionsListOnClickOfCancel();
		} else {
			AdminWise.adminPanel.routePanel.routeInfoBean.setRouteEmailOptionsInfoList(new Vector<VWEmailOptionsInfo>());
		}
		setVisible(false);
		dispose();
	}
	
	void BtnRouteLocation_actionPerformed(java.awt.event.ActionEvent event) {
		String selectedOption=this.cmbRouteAction.getSelectedItem().toString();
		//For File Dialog
		if( (selectedOption.equalsIgnoreCase(VWConstant.lstAction[2])) || (selectedOption.equalsIgnoreCase(VWConstant.lstAction[3])) || (selectedOption.equalsIgnoreCase(VWConstant.lstAction[5]))){
			active = false;
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = fileChooser.showOpenDialog(this);
			if(returnVal==JFileChooser.APPROVE_OPTION){
				// the path selection should be relative to VWS server or AminWise and VWS server should be installed in same machine. Valli
				this.txtActionLocation.setText(fileChooser.getSelectedFile().toString());
			}
			active = true;
		}else{//Tree Find Dialog
			if (dlgLocation != null) {
				active = false;
				dlgLocation.loadDlgData();
				setCurrentLocation(dlgLocation);
				dlgLocation.setVisible(true);
			} else {
				active = false;
				dlgLocation = new VWDlgSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWEndIconProperties.dlgLocation"));
				setCurrentLocation(dlgLocation);
				dlgLocation.setVisible(true);
			}
			if (dlgLocation.cancelFlag || !dlgLocation.cancelFlag ){
				active = true;
			}
			if (dlgLocation.cancelFlag)
				return;
			txtActionLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
			startLocationNodeId = dlgLocation.tree.getSelectedNodeId();
		}
	}
	
	void BtnReportLocation_actionPerformed(java.awt.event.ActionEvent event) {
		active = false;
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fileChooser.showOpenDialog(this);
		if(returnVal==JFileChooser.APPROVE_OPTION){
			// the path selection should be relative to VWS server or AminWise and VWS server should be installed in same machine. Valli
			this.txtReportLocation.setText(fileChooser.getSelectedFile().toString());
		}
		active = true;
		
	}
	void BtnRejectFoldLocation_actionPerformed(java.awt.event.ActionEvent event){
		if (dlgLocation != null) {
			active = false;
			dlgLocation.loadDlgData();
			setCurrentLocation(dlgLocation);
			dlgLocation.setVisible(true);
		} else {
			active = false;
			dlgLocation = new VWDlgSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWEndIconProperties.dlgLocation"));
			setCurrentLocation(dlgLocation);
			dlgLocation.setVisible(true);
		}
		if (dlgLocation.cancelFlag || !dlgLocation.cancelFlag ){
			active = true;
		}
		if (dlgLocation.cancelFlag)
			return;
		txtRejectFoldLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
		rejectLocationNodeId = dlgLocation.tree.getSelectedNodeId();
		//AdminWise.adminPanel.routePanel.routeInfoBean.setRejectLocation(rejectLocationNodeId+"");			
	
	}
	public String getLocationPath(int roomId,int actionId){
		Vector locationInfo = new Vector();	
		AdminWise.gConnector.ViewWiseClient.getNodeParents(roomId, new Node(actionId), locationInfo);
		StringBuffer path = new StringBuffer(AdminWise.adminPanel.roomPanel.getSelectedRoom().getName());
		Node node = null;
		for(int i=locationInfo.size()-1;i>=0; i--){
			try{
				node = (Node)locationInfo.get(i);
				path.append("\\" + node.getName());
			}catch(Exception e){
				path = new StringBuffer();
			}
		}
		return path.toString();
	}
	public void loadEndRouteInfo(){
		AdminWise.printToConsole("inside loadEndRouteInfo....");
		viewMode();
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		String actionLocation = "";
		int indexOfSlash = 0;
		String actionId = "";
		String indexId = "";
		boolean indexIdPresent = false;
		if (room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		gCurRoom = room.getId();
		/****CV2019 merges - 10.2 cancel button Issue fixes***/
		AdminWise.printToConsole("AdminWise.adminPanel.routePanel.routeInfoBean....."+AdminWise.adminPanel.routePanel.routeInfoBean);
		
		chkCompressDocPages.setSelected(false);
		chkPdfConversion.setSelected(false);
		chkCompressDocPages.setEnabled(false);
		chkPdfConversion.setEnabled(false);
		
		AdminWise.printToConsole("AdminWise.adminPanel.routePanel.routeInfoBean....."+AdminWise.adminPanel.routePanel.routeInfoBean);
		if( (AdminWise.adminPanel.routePanel.routeInfoBean!=null) ){
			//AdminWise.adminPanel.routePanel.routeInfoBeanForEmailList = AdminWise.adminPanel.routePanel.routeInfoBean;
			//emailListInfo = AdminWise.adminPanel.routePanel.routeInfoBean.getRouteEmailOptionsInfoList();
			setEmailOptionsInGlobalVariable(AdminWise.adminPanel.routePanel.routeInfoBean.getRouteEmailOptionsInfoList());
			try{
				int routeActionId = Integer.parseInt(AdminWise.adminPanel.routePanel.routeInfoBean.getRouteActionId());
				cmbRouteAction.setSelectedIndex(routeActionId);
				AdminWise.printToConsole("routeActionId....."+routeActionId);
				Vector getRouteInfo = new Vector(); 
				int roomId = AdminWise.adminPanel.roomPanel.getSelectedRoom().getId();//Using Node Id get the location Path
				if(routeActionId==1){
					Vector locationInfo = new Vector();				
					actionId = AdminWise.adminPanel.routePanel.routeInfoBean.getActionLocation();
					if(!actionId.equals("-1")){
						actionLocation = actionId;
						if(actionLocation.contains("\\")){
							indexIdPresent = true;
							indexOfSlash = actionLocation.indexOf("\\");
							actionId = actionLocation.substring(0,indexOfSlash);
							/**
							 * This is to set actionLocation to actionId alone. 
							 * else it will concatinate one more index string and the result will be 28\45\45
							 */
							AdminWise.adminPanel.routePanel.routeInfoBean.setActionLocation(actionId);
							indexId = actionLocation.substring(indexOfSlash+1,actionLocation.length());
							if(indexId.equals("0")){
								indexIdPresent = false;
							}
						}else if(AdminWise.adminPanel.routePanel.routeInfoBean.getIndexId()>0){
							indexId = String.valueOf(AdminWise.adminPanel.routePanel.routeInfoBean.getIndexId());
							indexIdPresent = true;
						}
						String actionLocationPath=getLocationPath(roomId, Integer.parseInt(actionId));
						txtActionLocation.setText(actionLocationPath);
						startLocationNodeId = Integer.parseInt(actionId);

						if(indexId!=null && !indexId.equals("") && !indexId.equals("0")){
							int indexIdInt = Integer.parseInt(indexId);
							if(indexIdPresent){
								try{
									VWIndexRec vwIndexRec = VWDocTypeConnector.getIndex(indexIdInt);
									cmbIndexName.getModel().setSelectedItem(vwIndexRec);
								}catch(Exception ex){
									//System.out.println("On load of IndexName :: "+ex.getMessage());
								}
							}
							AdminWise.adminPanel.routePanel.routeInfoBean.setIndexId(indexIdInt);
						}
					}
				}else if(routeActionId==4){
					//need to get selected route from script and need to set in the bean to avoid the getRouteInfo() call here. 
					loadAvailableRoutes(cmbAvailableAssignRoute);
					
					AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, 
							Util.to_Number(AdminWise.adminPanel.routePanel.routeInfoBean.getActionLocation()),
							getRouteInfo);														
					if((getRouteInfo!=null)&&getRouteInfo.size() > 0)
						cmbAvailableAssignRoute.getModel().setSelectedItem((RouteInfo)getRouteInfo.get(0));
				}
				else{
					txtActionLocation.setText(AdminWise.adminPanel.routePanel.routeInfoBean.getActionLocation());	
				}				
				String reportLocation = AdminWise.adminPanel.routePanel.routeInfoBean.getReportLocation().trim();
				if(reportLocation.equals("-"))
					txtReportLocation.setText("");
				else
					txtReportLocation.setText(reportLocation);
				
				String rejectLocation = AdminWise.adminPanel.routePanel.routeInfoBean.getRejectLocation();
				if(rejectLocation.equals("-")||(txtRejectFoldLocation.getText().equals("")&&AdminWise.adminPanel.routePanel.CURRENT_MODE != 2)){
					txtRejectFoldLocation.setText("");
				}
				else{
					String rejectLocationPath=getLocationPath(roomId,Integer.parseInt(rejectLocation));
					txtRejectFoldLocation.setText(rejectLocationPath);
				}

				
				
				
				int selectEmailOriginator = AdminWise.adminPanel.routePanel.routeInfoBean.getEmailOriginator();
				//AdminWise.printToConsole("getting email originator status from DB "+selectEmailOriginator);
				if(selectEmailOriginator==1){
					chkSendOriginatorNotification.setSelected(true);
					//AdminWise.printToConsole("If Part chkEmailOriginator.isselected(): "+chkSendOriginatorNotification.isSelected());
					
				}else {
					chkSendOriginatorNotification.setSelected(false);
					//AdminWise.printToConsole("Else part chkEmailOriginator.isselected(): "+chkSendOriginatorNotification.isSelected());
				}

				
				
				if(AdminWise.adminPanel.routePanel.routeInfoBean.getSendEmailOnRptGen() == 1)
					chkSendMail.setSelected(true);
				else
					chkSendMail.setSelected(false);
				//Old functionality
				/*String eMailIds = AdminWise.adminPanel.routePanel.routeInfoBean.getEmailIDs().trim();
				if(eMailIds.equals("-"))
					taEMailIds.setText("");
				else
					taEMailIds.setText(eMailIds);
				*/
				
				//Route Email options enhancement
				Vector vwEmailOptionsList= new Vector();
				vwEmailOptionsList = AdminWise.adminPanel.routePanel.routeInfoBean.getRouteEmailOptionsInfoList();
				/****CV2019 merges - 10.2 cancel button Issue fixes***/
				AdminWise.printToConsole("vwEmailOptionsList....."+vwEmailOptionsList);
				vwEmailOptionsListPanel.Table.clearData();
				if(vwEmailOptionsList!=null && vwEmailOptionsList.size()>0) {
					/*for(int i=0; i<vwEmailOptionsList.size(); i++){
						AdminWise.printToConsole("loading getDocumentMetaDataFlag()....."+((VWEmailOptionsInfo)vwEmailOptionsList.get(i)).getDocumentMetaDataFlag());
						AdminWise.printToConsole("loading getRouteSummaryReportFlag()....."+((VWEmailOptionsInfo)vwEmailOptionsList.get(i)).getRouteSummaryReportFlag());
						AdminWise.printToConsole("loading getDocumentPagesFlag()....."+((VWEmailOptionsInfo)vwEmailOptionsList.get(i)).getDocumentPagesFlag());
					}*/
					vwEmailOptionsListPanel.Table.addData(vwEmailOptionsList, gCurRoom, "");
					setConvertToPDFAndCompressPages(vwEmailOptionsList);							
				} 
				
			}catch(Exception e){
				//System.out.println("Exception ex is : "+e.getMessage());
				clearFields();
			}
		}else{
			clearFields();
		}
	}
	public void loadIndicesList(){
		AdminWise.printToConsole("inside loadIndicesList....");
		VWIndexRec[] indices = VWRouteConnector.getIndices(false);
		try{
			if((cmbIndexName!=null)&& cmbIndexName.getItemCount()>0)
				cmbIndexName.removeAllItems();
		}catch(Exception ex){
			//System.out.println("Error in loadIndicesList : VWEndIconProperties :: "+ex.toString());
		}
		try{
			cmbIndexName.addItem("<Index Name>");
			for(int count=0; count<indices.length;count++){
				VWIndexRec vwIndexRec = (VWIndexRec)indices[count];
				if(vwIndexRec.getType() == 5)
					continue;
				cmbIndexName.addItem(indices[count]);
			}			
			cmbIndexName.addItem("<Refresh>");
		}catch(Exception ex){
			//System.out.println("<<Error in loadIndicesList in VWEndIconProperties :: "+ex.toString());
		}			
		if(cmbIndexName.getItemCount()>0)
			cmbIndexName.setSelectedIndex(0);
	}
	
	public void clearFields(){
		cmbRouteAction.setSelectedIndex(0);
		txtActionLocation.setText("");
		txtLocation.setText("");
		txtReportLocation.setText("");
		txtRejectFoldLocation.setText("");
		cmbAvailableAssignRoute.setSelectedIndex(0);
		cmbIndexName.setSelectedIndex(0);
		taEMailIds.setText("");
		chkSendMail.setSelected(false);		
		//AdminWise.adminPanel.routePanel.routeInfoBeanForEmailList = null;
		//emailOptionsList = null;
	}
	/****CV2019 merges - Route action issue fix after SIDBI and 10.2 merge***/
	public void loadRouteActions(){
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		if(room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		int gCurRoom = room.getId();
		boolean sidbiCustomizationFlg = VWRoutePanel.sidbiCustomizationFlag;   
		AdminWise.printToConsole("sidbiCustomizationFlg...."+sidbiCustomizationFlg);
		for(int count = 0;count<lstAction.length;count++){			
			if (!sidbiCustomizationFlg && count == 7)
				continue;
			cmbRouteAction.addItem(lstAction[count]);	
		}
	}
	
//	Main ItemState Changed Event...	
	class SymListAction implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent iEvent){
			Object object=iEvent.getSource();
			if(object==cmbRouteAction){
				cmbRouteAction_ItemStateChanged();
			}
		}
	}
	//
	class SymIndexListAction implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent event){
			Object obj = event.getSource();
			if(obj == cmbIndexName){
				cmbIndexName_ItemStateChanged();
			}
		}
	}
	
	private void loadAvailableRoutes(JComboBox jcb) {
		try {
			Vector routeList = new Vector();
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			if(room == null || room.getConnectStatus() != Room_Status_Connect)
				return;
			int gCurRoom = room.getId();
			//GET only Manual Routes...
			AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, 0,routeList);
			jcb.removeAllItems();
			jcb.addItem(new String("<Available "+ Constants.WORKFLOW_MODULE_NAME +"s>"));
			String currentRouteName = AdminWise.adminPanel.routePanel.routeInfoBean.getRouteName();
			for (int i = 0; (routeList!=null)&& i < routeList.size(); i++){
				//Only other than current route
				if((currentRouteName!=null)&&!currentRouteName.trim().equals("")){
					if(!String.valueOf(routeList.get(i)).trim().equalsIgnoreCase(currentRouteName.trim())){
						jcb.addItem(routeList.get(i));
					}
				}else
					jcb.addItem(routeList.get(i));
			}
		} catch (Exception ex) {
			AdminWise.printToConsole(" loadAvailableRoutes Exception "
					+ ex.getMessage());
		}
	}
	
	/**
	 * On selection of refresh need to load the index
	 */
	public void cmbIndexName_ItemStateChanged(){
		try{
			String selIndex = cmbIndexName.getSelectedItem().toString();
			if(selIndex.equalsIgnoreCase("<Refresh>")){
				cmbIndexName.removeAllItems();
				loadIndicesList();
			}
		}catch(Exception ex){
			//System.out.println("Exception while refresh the indices list : "+ex.getMessage());
		}
	}

	//	Item State Changed Event for RouteAction Combo...	
	public void cmbRouteAction_ItemStateChanged(){
		try{
			txtActionLocation.setText("");
			String selectedAction = cmbRouteAction.getSelectedItem().toString().trim();
			if( selectedAction.equalsIgnoreCase(VWConstant.lstAction[1]) || selectedAction.equalsIgnoreCase(VWConstant.lstAction[2]) || 
			    selectedAction.equalsIgnoreCase(VWConstant.lstAction[3]) || selectedAction.equalsIgnoreCase(VWConstant.lstAction[4]) || 
			    selectedAction.equalsIgnoreCase(VWConstant.lstAction[5]) || selectedAction.equalsIgnoreCase(VWConstant.lstAction[6]))
			{
				if(selectedAction.equalsIgnoreCase(VWConstant.lstAction[1])){
					lblCreateNewFolder.setVisible(true);
					cmbIndexName.setVisible(true);
					lblRejectFoldLocation.setBounds(5,115,375,20);
					//stausReportlineBorderLabel.setBounds(80, 55, 310, 2);  
					txtRejectFoldLocation.setBounds(5,132,360,20);
					btnRejctFoldLocation.setBounds(366, 132, 25,20);
					stausReportlineBorderLabel.setBounds(80, 168, 310, 2);//(80, 155, 310, 2);
				    lblStatusReport.setBounds(5,158,150,20);//(5,145,150,20);
					lblReportLocation.setBounds(5,176,375,20);//(5,165,375,20);
					txtReportLocation.setBounds(5,193,360,20);//(5,182,360,20);
					btnFinalReportLocation.setBounds(366, 193, 25,20);//(366, 182, 25,20);
					
					//chkSendMail.setBounds(5,182,300,20);//(5,207,300,20);
					//spEMailIds.setBounds(5,207,385,67);//(5,232,385,67);
					
					lblEmailOptions.setBounds(5, 216, 75, 20);
					emailOptionslineBorderLabel.setBounds(80, 226, 310, 2);
					vwEmailOptionsListPanel.setBounds(5,233,390,143);

					chkSendOriginatorNotification.setBounds(5,376,15,20);
					lblSendOriginatorNotification.setBounds(23,376,375,20);
					
					chkCompressDocPages.setBounds(5+104+27,376,15,20);
					chkPdfConversion.setBounds(5+104+115+49,376,15,20);
					lblCompressDocPage.setBounds(125+27,376,250,20);
					lblConvertoPdf.setBounds(180+60+49,376,250,20);
					
					btnDone.setBounds(228,376+30,80,24);
					btnCancel.setBounds(311,376+30,80,24);
					

					
					loadIndicesList();
					setSize(412,485);
					
					
					
				}else if(selectedAction.equalsIgnoreCase(VWConstant.lstAction[6]) || selectedAction.equalsIgnoreCase(VWConstant.lstAction[5])){
					
					lblRejectFoldLocation.setBounds(5,45,375,20);
					//stausReportlineBorderLabel.setBounds(80, 55, 310, 2);  
					txtRejectFoldLocation.setBounds(5,62,360,20);
					btnRejctFoldLocation.setBounds(366, 62, 25,20);
					lblStatusReport.setBounds(5,90,100,20);
					stausReportlineBorderLabel.setBounds(80, 100, 310, 2);  
					lblReportLocation.setBounds(5, 105,375,20);  
					txtReportLocation.setBounds(5,122,360,20);  
					btnFinalReportLocation.setBounds(366, 122, 25,20);  
					//chkSendMail.setBounds(5,108,300,20);  
					//spEMailIds.setBounds(5,130,385,67);   
					lblEmailOptions.setBounds(5, 148, 75, 20);  
					emailOptionslineBorderLabel.setBounds(80, 158, 310, 2);    
					vwEmailOptionsListPanel.setBounds(5,165,390,143);  

					chkSendOriginatorNotification.setBounds(5,308,15,20);
					lblSendOriginatorNotification.setBounds(23,308,375,20);
					
					chkCompressDocPages.setBounds(5+104+27,308,15,20);
					chkPdfConversion.setBounds(5+104+115+49,308,15,20);
					lblCompressDocPage.setBounds(125+27,308,250,20);
					lblConvertoPdf.setBounds(180+60+49,308,375,20);
					btnDone.setBounds(228,308+30,80,24);   
					btnCancel.setBounds(311,308+30,80,24); 
					/****CV2019 merges - 10.2 Issue fix***/
					setSize(412,383+35);    
					//setSize(412,530);    
				}else{
					lblCreateNewFolder.setVisible(false);
					cmbIndexName.setVisible(false);

					lblRejectFoldLocation.setBounds(5,90,375,20);
					txtRejectFoldLocation.setBounds(5,107,360,20);
					btnRejctFoldLocation.setBounds(366, 107, 25,20);

				    lblStatusReport.setBounds(5,135,150,20);
					stausReportlineBorderLabel.setBounds(80, 145, 310, 2);
				    lblReportLocation.setBounds(5,155,375,20);
					txtReportLocation.setBounds(5,172,360,20);
					btnFinalReportLocation.setBounds(366, 172, 25,20);
					
					//chkSendMail.setBounds(5,157,300,20);
					//spEMailIds.setBounds(5,182,385,67);
					
					lblEmailOptions.setBounds(5, 197, 75, 20);
					emailOptionslineBorderLabel.setBounds(80, 207, 310, 2);
					vwEmailOptionsListPanel.setBounds(5,214,390,143);

					chkSendOriginatorNotification.setBounds(5,358,15,20);
					lblSendOriginatorNotification.setBounds(23,358,375,20);
					chkCompressDocPages.setBounds(5+104+27,358,15,20);
					chkPdfConversion.setBounds(5+104+115+49,358,15,20);
					lblCompressDocPage.setBounds(125+27,358,250,20);
					lblConvertoPdf.setBounds(180+60+49,358,375,20);
					btnDone.setBounds(228,358+30,80,24);
					btnCancel.setBounds(311,358+30,80,24);
					/****CV2019 merges - 10.2 Issue fix***/
					setSize(412,465);
				}
			}if( selectedAction.equalsIgnoreCase(VWConstant.lstAction[0]) || selectedAction.equalsIgnoreCase(VWConstant.lstAction[6])){

				lblRejectFoldLocation.setBounds(5,45,375,20);
				//stausReportlineBorderLabel.setBounds(80, 55, 310, 2);  
				txtRejectFoldLocation.setBounds(5,62,360,20);
				btnRejctFoldLocation.setBounds(366, 62, 25,20);
				lblStatusReport.setBounds(5,90,100,20);
				stausReportlineBorderLabel.setBounds(80, 100, 310, 2);  
				lblReportLocation.setBounds(5, 105,375,20);  
				txtReportLocation.setBounds(5,122,360,20);  
				btnFinalReportLocation.setBounds(366, 122, 25,20);  
				//chkSendMail.setBounds(5,108,300,20);  
				//spEMailIds.setBounds(5,130,385,67);   
				lblEmailOptions.setBounds(5, 148, 75, 20);  
				emailOptionslineBorderLabel.setBounds(80, 158, 310, 2);    
				vwEmailOptionsListPanel.setBounds(5,165,390,143);  

				chkSendOriginatorNotification.setBounds(5,308,15,20);
				lblSendOriginatorNotification.setBounds(23,308,375,20);

				chkCompressDocPages.setBounds(5+104+27,308,15,20);
				chkPdfConversion.setBounds(5+104+115+49,308,15,20);
				lblCompressDocPage.setBounds(125+27,308,250,20);

				lblConvertoPdf.setBounds(180+60+49,308,375,20);
				btnDone.setBounds(228,308+30,80,24);   
				btnCancel.setBounds(311,308+30,80,24);  
				/****CV2019 merges - 10.2 Issue fix***/
				setSize(412,383+35);    
				
				
				lblRoomLocation.setVisible(false);
				txtActionLocation.setVisible(false);
			}
			if( (selectedAction.equalsIgnoreCase(VWConstant.lstAction[4]))) {
				loadAvailableRoutes(cmbAvailableAssignRoute);
				lblAvailableRoute2.setVisible(true);
				cmbAvailableAssignRoute.setVisible(true);
				lblCreateNewFolder.setVisible(false);
				cmbIndexName.setVisible(false);
			}
			else{
				lblAvailableRoute2.setVisible(false);
				cmbAvailableAssignRoute.setVisible(false);
				lblCreateNewFolder.setVisible(false);
				cmbIndexName.setVisible(false);
			}
			if( (selectedAction.equalsIgnoreCase(VWConstant.lstAction[2])) 
					|| (selectedAction.equalsIgnoreCase(VWConstant.lstAction[3]))
					|| (selectedAction.equalsIgnoreCase(VWConstant.lstAction[1])) ) {
				
				if( (selectedAction.equalsIgnoreCase(VWConstant.lstAction[2])) 
						|| (selectedAction.equalsIgnoreCase(VWConstant.lstAction[3])) ){
					
					lblRoomLocation.setText(LBL_UNC_PATH +" "+AdminWise.connectorManager.getString("VWEndIconProperties.lblRoomLocation_0"));
					lblRoomLocation.setVisible(true);
					lblRoomLocation.setBounds(5,45,375,20);
					txtActionLocation.setVisible(true);
					txtActionLocation.setBounds(5,62,360,20);
					txtRejectFoldLocation.setVisible(true);
					btnFinalActionLocation.setVisible(true);
					btnRejctFoldLocation.setVisible(true);
					btnFinalActionLocation.setBounds(366,62,25,20);
					lblCreateNewFolder.setVisible(false);
					cmbIndexName.setVisible(false);
				}else if((selectedAction.equalsIgnoreCase(VWConstant.lstAction[1]))){
					lblRoomLocation.setText(LBL_ROOM_LOCATION +" "+ AdminWise.connectorManager.getString("VWEndIconProperties.lblRoomLocation_1")+" "+AdminWise.connectorManager.getString("Product.Name") +" "+ AdminWise.connectorManager.getString("VWEndIconProperties.lblRoomLocation_2"));
					lblRoomLocation.setVisible(true);
					txtRejectFoldLocation.setVisible(true);
					lblRoomLocation.setBounds(5,47,375,20);
					txtActionLocation.setVisible(true);
					txtActionLocation.setBounds(5,64,360,20);
					lblCreateNewFolder.setVisible(true);
					lblCreateNewFolder.setText(AdminWise.connectorManager.getString("VWEndIconProperties.lblCreateNewFolder"));
					lblCreateNewFolder.setBounds(85,94,150,20);//(5,94,150,20);
					cmbIndexName.setVisible(true);
					cmbIndexName.setBounds(190,96,175,20);//(5,112,385,20);
					btnFinalActionLocation.setVisible(true);
					btnRejctFoldLocation.setVisible(true);
					btnFinalActionLocation.setBounds(366,64,25,20);
				}else if( (selectedAction.equalsIgnoreCase(VWConstant.lstAction[4]))) {
					lblRoomLocation.setVisible(false);
					txtRejectFoldLocation.setVisible(true);
					txtActionLocation.setVisible(false);
					btnFinalActionLocation.setVisible(false);
					lblCreateNewFolder.setVisible(false);
					cmbIndexName.setVisible(false);
					btnRejctFoldLocation.setVisible(true);
				}
				if(selectedAction.equalsIgnoreCase(VWConstant.lstAction[2])
					||(selectedAction.equalsIgnoreCase(VWConstant.lstAction[3]))){
					txtActionLocation.setEditable(true);
					txtRejectFoldLocation.setVisible(true);
					lblCreateNewFolder.setVisible(false);
					cmbIndexName.setVisible(false);
					btnRejctFoldLocation.setVisible(true);
					//lblUNCInfo.setText("* Select file system path relative to DRS Service.");
					//lblUNCInfo.setVisible(true);
				}else if(selectedAction.equalsIgnoreCase(VWConstant.lstAction[1])){
					lblCreateNewFolder.setVisible(true);
					txtRejectFoldLocation.setVisible(true);
					cmbIndexName.setVisible(true);
					txtActionLocation.setEditable(false);
					btnRejctFoldLocation.setVisible(true);
					//lblUNCInfo.setText("* Select the destination ViewWise folder to move.");
					//lblUNCInfo.setVisible(true);
				}else
					lblUNCInfo.setVisible(false);
			}else{
				lblUNCInfo.setVisible(false);
				lblRoomLocation.setVisible(false);
				txtActionLocation.setVisible(false);
				btnFinalActionLocation.setVisible(false);
				lblCreateNewFolder.setVisible(false);
				cmbIndexName.setVisible(false);
				txtRejectFoldLocation.setVisible(true);
				btnRejctFoldLocation.setVisible(true);
			}
			
			if(selectedAction.equalsIgnoreCase(VWConstant.lstAction[6])){
				txtActionLocation.setVisible(false);
				txtLocation.setVisible(false);
				txtRejectFoldLocation.setVisible(true);
				btnFinalActionLocation.setVisible(false);
				lblAvailableRoute2.setVisible(false);
				cmbAvailableAssignRoute.setVisible(false);
				lblCreateNewFolder.setVisible(false);
				cmbIndexName.setVisible(false);
				btnRejctFoldLocation.setVisible(true);
			}
			
			stausReportlineBorderLabel.setVisible(true);
			lblStatusReport.setVisible(true);
			lblReportLocation.setVisible(true);
			lblRejectFoldLocation.setVisible(true);;
			txtReportLocation.setVisible(true);
			txtRejectFoldLocation.setVisible(true);
			btnFinalReportLocation.setVisible(true);
			btnRejctFoldLocation.setVisible(true);
			chkSendOriginatorNotification.setVisible(true);
			chkSendOriginatorNotification.setSelected(true);
			chkCompressDocPages.setVisible(true);
			/****CV2019 merges - 10.2 Issue fix***/
			//chkCompressDocPages.setSelected(true);
			//chkPdfConversion.setBounds(5+104+115,308,15,20);
			chkPdfConversion.setVisible(true);
			//chkPdfConversion.setSelected(true);
		}catch(Exception ex){
			//AdminWise.printToConsole("Err in 752 "+ ex.getMessage());
		}
	}
	
	//Set the current location where this dialog box should be loaded.
	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width, this.getSize().height);
		setLocation(point);
		setResizable(false);
	}
	//Set the current location where this dialog box should be loaded.
	public void setCurrentLocation(VWDlgSaveLocation dlg){
		Point point = AdminWise.adminFrame.getCurrentLocation(dlg.getSize().width, dlg.getSize().height);
		dlg.setLocation(point);
	}
	/****CV2019 merges - 10.2 - Document pages issue fix***/
	public static void setConvertToPDFAndCompressPages(Vector emailList) {
		AdminWise.printToConsole("setConvertToPDFAndCompressPages.....");
		Vector vwEmailOptionsList = null;
		if (emailList == null)
			vwEmailOptionsList = (Vector)VWEmailOptionsListPanel.Table.getData();
		else
			vwEmailOptionsList =  emailList;
		if(vwEmailOptionsList != null && vwEmailOptionsList.size() > 0){
			VWEmailOptionsInfo vwEmailOptionsInfo = null;
			boolean isChkEnable = false;
			for(int i=0; i<vwEmailOptionsList.size(); i++){
				vwEmailOptionsInfo = (VWEmailOptionsInfo)vwEmailOptionsList.get(i);
				if (vwEmailOptionsInfo.getDocumentPagesFlag() == 1) {
					isChkEnable = true;
					break;
				}
			}
			if (isChkEnable) {
				chkCompressDocPages.setEnabled(true);
				chkPdfConversion.setEnabled(true);
				/**CV2019 - Added for Workflow conver to pdf checkbox selection issue ***/
				if (emailList != null) {
					AdminWise.printToConsole("pdf convertion....."+AdminWise.adminPanel.routePanel.routeInfoBean.getPdfConversion());
					AdminWise.printToConsole("compress document...."+AdminWise.adminPanel.routePanel.routeInfoBean.getCompressdoc());
					if (AdminWise.adminPanel.routePanel.routeInfoBean.getPdfConversion() == 1) {
						chkPdfConversion.setSelected(true);
					} else {
						chkPdfConversion.setSelected(false);
					}
					if(AdminWise.adminPanel.routePanel.routeInfoBean.getCompressdoc() == 1) {
						chkCompressDocPages.setSelected(true);
					} else {
						chkCompressDocPages.setSelected(false);
					}					
				} else {
					chkCompressDocPages.setSelected(true);
					chkPdfConversion.setSelected(true);
				}
				/**End of CV2019 convert to pdf checkbox selection issue fix***/
			} else {
				chkCompressDocPages.setSelected(false);
				chkPdfConversion.setSelected(false);
				chkCompressDocPages.setEnabled(false);
				chkPdfConversion.setEnabled(false);
			}
		} else {
			chkCompressDocPages.setSelected(false);
			chkPdfConversion.setSelected(false);
			chkCompressDocPages.setEnabled(false);
			chkPdfConversion.setEnabled(false);
		}
		
		/*ArrayList<VWEmailOptionsInfo> emailInfoList = emailOptionsList;
		AdminWise.printToConsole("after click.................."+emailInfoList);
		if (emailInfoList != null) {
			for(int j=0; j<emailInfoList.size(); j++){
				AdminWise.printToConsole("after click getDocumentMetaDataFlag()....."+((VWEmailOptionsInfo)emailInfoList.get(j)).getDocumentMetaDataFlag());
				AdminWise.printToConsole("after click getRouteSummaryReportFlag()....."+((VWEmailOptionsInfo)emailInfoList.get(j)).getRouteSummaryReportFlag());
				AdminWise.printToConsole("after click getDocumentPagesFlag()....."+((VWEmailOptionsInfo)emailInfoList.get(j)).getDocumentPagesFlag());
			}
		}*/
	}
	
	/****CV2019 merges - 10.2 cancel button Issue fix***/
	/**
	 * Method is used to store the email options list in to global list variable. The
	 * purpose of adding this global variable is, when we click on any of the flag(meta data/summary
	 * report/document pages), then the flag value is directly updating to the
	 * AdminWise.adminPanel.routePanel.routeInfoBean. On click of cancel not able to retain old values.
	 **/
	public void setEmailOptionsInGlobalVariable(Vector<VWEmailOptionsInfo> emailInfoList) {
		//emailOptionsList = new ArrayList<VWEmailOptionsInfo>(emailInfoList); // This type of assigning the value to global variable is taking the reference of emailInfoList object, onclick of cancel old value is not retaining
		emailOptionsList = new ArrayList<VWEmailOptionsInfo>();
 		VWEmailOptionsInfo emailInfo = null;
 		AdminWise.printToConsole("onload.................."+emailInfoList);
 		int metaData = 0, summaryReport = 0, pagesFlag = 0, vwrFlag = 0;
 		String emailID = null;
 		if (emailInfoList != null) {
 			for(int j=0; j<emailInfoList.size(); j++){
 				emailInfo = new VWEmailOptionsInfo();
 				emailID = ((VWEmailOptionsInfo)emailInfoList.get(j)).getUserEmailAddress();
 				vwrFlag = ((VWEmailOptionsInfo)emailInfoList.get(j)).getDocumentVwrFlag();
 				metaData = ((VWEmailOptionsInfo)emailInfoList.get(j)).getDocumentMetaDataFlag();
 				summaryReport = ((VWEmailOptionsInfo)emailInfoList.get(j)).getDocumentMetaDataFlag();
 				pagesFlag = ((VWEmailOptionsInfo)emailInfoList.get(j)).getDocumentPagesFlag();
 				AdminWise.printToConsole("onload getDocumentVWRFlag() in load....."+vwrFlag);
 				AdminWise.printToConsole("onload getDocumentMetaDataFlag() in load....."+metaData);
 				AdminWise.printToConsole("onload getRouteSummaryReportFlag() in load....."+summaryReport);
 				AdminWise.printToConsole("onload getDocumentPagesFlag() in load....."+pagesFlag);
 				emailInfo.setUserEmailAddress(emailID);
 				emailInfo.setDocumentVwrFlag(vwrFlag);
 				emailInfo.setDocumentMetaDataFlag(metaData);
 				emailInfo.setRouteSummaryReportFlag(summaryReport);
 				emailInfo.setDocumentPagesFlag(pagesFlag);
 				emailOptionsList.add(emailInfo);
 			}
 		}
 	}
	
	public void setOldEmailOptionsListOnClickOfCancel() {
		Vector<VWEmailOptionsInfo> emailList = new Vector<VWEmailOptionsInfo>(emailOptionsList);
		AdminWise.adminPanel.routePanel.routeInfoBean.setRouteEmailOptionsInfoList(emailList);
	}
}
