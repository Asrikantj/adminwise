package ViewWise.AdminWise.VWRoute;

import java.awt.Component;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWUtil.VWComboBox;

import com.computhink.common.Principal;

/**
 * @author Nishad Nambiar
 *
 */

public class VWApproversListRowEditor implements TableCellEditor, VWConstant{
	  
	protected TableCellEditor editor, defaultEditor, checkboxEditor;
	protected VWApproversListTable gTable=null;
	public int selectedColumn;
	public int selectedRow;
	JSpinner spinner;
	JSpinner.DateEditor dateEditor;
	SpinnerDateModel dateModel;

	/**
	 * Constructs a EachRowEditor.
	 * create default editor 
	 *
	 * @see TableCellEditor
	 * @see DefaultCellEditor
	 */ 
	public VWApproversListRowEditor(VWApproversListTable table) {
		defaultEditor = new DefaultCellEditor(new JTextField());
	    //checkboxEditor = new DefaultCellEditor(checkbox);
		gTable=table;
	}

	/**
	 * @param row    table row
	 * @param editor table cell editor
	 */

	public Component getTableCellEditorComponent(JTable table,
			Object value, boolean isSelected, int row, int column) {

		editor=defaultEditor;
		
		Principal dynamicUser = null;

		Calendar calendar = Calendar.getInstance();

		Date initDate = calendar.getTime();
		calendar.add(Calendar.YEAR, -100);
		Date earliestDate = calendar.getTime();
		calendar.add(Calendar.YEAR, 200);
		Date latestDate = calendar.getTime();
		dateModel = new SpinnerDateModel();// ignored for user input
		spinner = new JSpinner(dateModel);
		selectedColumn = column;
		if(column==3){			
			Calendar calendar1 = Calendar.getInstance();

			//02day 04hr 05min
			String dtValue = value.toString();
			String date = dtValue.substring(0, dtValue.indexOf("day"));
			String hr = dtValue.substring(dtValue.indexOf("day") + 4, dtValue.indexOf("hr") );
			String min = dtValue.substring(dtValue.indexOf("hr") + 3, dtValue.indexOf("min") );
			
			calendar1.set(Calendar.SECOND, Integer.parseInt(date));
			calendar1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hr));
			calendar1.set(Calendar.MINUTE, Integer.parseInt(min));
			dateModel.setValue(calendar1.getTime());
			dateEditor = new JSpinner.DateEditor(spinner, "ss'day' HH'hr' mm'min'");
			spinner.setEditor(dateEditor);
			spinner.addChangeListener(new ChangeListener(){
				public void stateChanged(ChangeEvent ce){				
					try{
						spinner.commitEdit();
					}catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}					
				}
			});
			return spinner;

		}
		else if(column==2 ){
			try{
				selectedColumn = column;
				selectedRow = row;
				int approversCount = VWRoutePanel.vecApproversList.size();
				
				int rowCount = table.getRowCount();
				
				try{
					if(VWRoutePanel.vecApproversList!=null && approversCount>0){
						
						int count = 0;
						Principal[] principalObject = null;
						if(rowCount <= 1){
//							 -4 is used as ID for Dynamic User in both Approver list and Escalation list.
							dynamicUser = new Principal(-4, DYNAMIC_USER, Principal.USER_TYPE, -4);
							dynamicUser.setTrimType(true);
							
							principalObject = new Principal[approversCount+1];
							principalObject[0] = dynamicUser;
							count++;
						}else{
							principalObject = new Principal[approversCount];
						}
						
						int selectedIndex = 0;
						for(int i=0; i<approversCount; i++){
							Principal principal  = VWRoutePanel.vecApproversList.get(i);
							principal.setTrimType(true);
							principalObject[count] = principal;							
							if (principal.getName().equalsIgnoreCase(value.toString().trim())){
								selectedIndex = i;	
							}
							count++;
						}
						VWComboBox routeUserCombo = new VWComboBox(principalObject);						
						//routeUserCombo.setSelectedItem(value.toString().trim());
						routeUserCombo.setSelectedIndex(selectedIndex);
						editor = new DefaultCellEditor(routeUserCombo);
					}
				}
				catch(Exception ex){
					//System.out.println("Exception ex is column 2 : "+ex.getMessage());
				}

			}catch(Exception ex){
				//System.out.println("Exception in getTableCellEditorComponent for column 2 is : "+ex.getMessage());
			}
		}else if (column == 4){
			try{
				selectedColumn = column;
				selectedRow = row;
				
				int eUsersCount = VWRoutePanel.allEscalationUsersList.size();
				try{
					Principal none = new Principal(-1, NONE, Principal.USER_TYPE, -1);
					Principal autoAccept = new Principal(-2, AUTOACCEPT, Principal.USER_TYPE, -2);
					Principal autoReject = new Principal(-3, AUTOREJECT, Principal.USER_TYPE, -3);
					dynamicUser = new Principal(-4, DYNAMIC_USER, Principal.USER_TYPE, -4);

					int rowCount = table.getRowCount();
					
					none.setTrimType(true);
					autoAccept.setTrimType(true);
					autoReject.setTrimType(true);
					dynamicUser.setTrimType(true);
					int loopCount = eUsersCount;
					if(VWRoutePanel.allEscalationUsersList != null && loopCount > 0){
						int count = 0;
						Principal[] principalObject = null;
						if(rowCount <= 1){
							loopCount = eUsersCount+4;
							principalObject = new Principal[loopCount]; 
							principalObject[0] = none;
							principalObject[1] = autoAccept;
							principalObject[2] = autoReject;
							principalObject[3] = dynamicUser;
							count = 4;
						}else{
							loopCount = eUsersCount+3;
							principalObject = new Principal[loopCount]; 
							principalObject[0] = none;
							principalObject[1] = autoAccept;
							principalObject[2] = autoReject;
							count = 3;
						}
						
						int selectedIndex = 0;
						for(int i=0; i<eUsersCount; i++){
							Principal principal  = VWRoutePanel.allEscalationUsersList.get(i);
							principal.setTrimType(true);
							principalObject[count] = principal;
							if(value.toString().trim().equals(AUTOACCEPT)){
								selectedIndex = 1;
							}else if(value.toString().trim().equals(AUTOREJECT)){
								selectedIndex = 2;
							}else if(value.toString().trim().startsWith("<<") && value.toString().trim().endsWith(">>")){
								selectedIndex = 3;
							}else if (principal.getName().equalsIgnoreCase(value.toString().trim())){
								selectedIndex = count;	
							}
							count++;
						}
						VWComboBox routeEUserCombo = new VWComboBox(principalObject);						
						routeEUserCombo.setSelectedIndex(selectedIndex);
						
						editor = new DefaultCellEditor(routeEUserCombo);
					}
				}catch(Exception ex){
					//System.out.println("Exception in column 4 is : "+ex.getMessage());
				}
			}catch(Exception ex){
				//System.out.println("Exception in getTableCellEditorComponent for column 4 is : "+ex.getMessage());
			}
		}
		return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

	public Object getCellEditorValue() {

		
		if(selectedColumn==3){//Spinner Component
			String spinnerValue = "";
			try{
				//spinner.getValue.toString() = Thu Jan 01 05:05:03 IST 1970
				String tempStr = spinner.getValue().toString();
				String hr   = tempStr.substring(11, 13);				
				String min   = tempStr.substring(14, 16);
				String day   = tempStr.substring(17, 19);


				spinnerValue = day+"day "+hr+"hr "+min+"min";
			}catch (Exception e) {
			}
			return spinnerValue;
		}else if(selectedColumn == 2){
			Principal principal = null;
			String principalName = "";
			try{
				principal = (Principal)editor.getCellEditorValue();				
			}catch(Exception e){}

			if(principal!=null){
				principalName = principal.getName();
				try{
					if(principalName.equalsIgnoreCase(DYNAMIC_USER)){
						//AdminWise.printToConsole("Selected Dynamic user in Approver list....");
						String selectedIndexName = gTable.getValueAt(selectedRow, gTable.m_data.COL_APPROVERNAME).toString();
						VWDlgAddDynamicUserIndex dynamicUserIndexDlg = new VWDlgAddDynamicUserIndex(AdminWise.adminFrame, selectedIndexName);
						
						if(dynamicUserIndexDlg.cancelFlag){
							principalName = selectedIndexName;
						}else{
							VWIndexRec index = dynamicUserIndexDlg.getSelectedIndex();
							if(index != null){
								principalName = "<<" + index.getName() + ">>";
								gTable.setValueAt(String.valueOf(index.getId()), selectedRow, gTable.m_data.COL_APPROVERID);
								gTable.setValueAt(String.valueOf(principal.getType()), selectedRow, gTable.m_data.COL_APPROVERTYPE);
								gTable.setValueAt(String.valueOf(principal.getEmail()), selectedRow, gTable.m_data.COL_APPROVEREMAIL);								
							}						
						}
						
						if(principalName.trim().length() > 0 && principalName.startsWith("<<") && principalName.endsWith(">>")){
							VWTaskIconProperties.btnAdd.setEnabled(false);							
						}
					}else{
						String selectedEscalationIndexName = gTable.getValueAt(selectedRow, gTable.m_data.COL_ESCALATION).toString();
						if(!selectedEscalationIndexName.startsWith("<<") && !selectedEscalationIndexName.endsWith(">>")){
							VWTaskIconProperties.btnAdd.setEnabled(true);						
						}
						gTable.setValueAt(String.valueOf(principal.getUserGroupId()), selectedRow, gTable.m_data.COL_APPROVERID);
						gTable.setValueAt(String.valueOf(principal.getType()), selectedRow, gTable.m_data.COL_APPROVERTYPE);
						gTable.setValueAt(String.valueOf(principal.getEmail()), selectedRow, gTable.m_data.COL_APPROVEREMAIL);
					}
				}catch (Exception e) {
					AdminWise.printToConsole("Exc : "+e.getMessage());
				}
				return principalName;
			}else{
				return "";
			}
		}else if(selectedColumn==4){
			//Escalation user always of user type
			Principal principal = null;
			String principalName = "";
			try{
				principal = (Principal)editor.getCellEditorValue();
			}catch(Exception e){}
			if(principal!=null){
				principalName = principal.getName();
				try{
					if(principal.getName().equalsIgnoreCase(DYNAMIC_USER)){
						//AdminWise.printToConsole("Selected Dynamic user in Escalation list....");
						String selectedIndexName = gTable.getValueAt(selectedRow, gTable.m_data.COL_ESCALATION).toString();
						VWDlgAddDynamicUserIndex dynamicUserIndexDlg = new VWDlgAddDynamicUserIndex(AdminWise.adminFrame, selectedIndexName);

						if(dynamicUserIndexDlg.cancelFlag){
							principalName = selectedIndexName;
						}else{
							VWIndexRec index = dynamicUserIndexDlg.getSelectedIndex();
							if(index != null){
								principalName = "<<" + index.getName() + ">>";
								gTable.setValueAt(String.valueOf(index.getId()), selectedRow, gTable.m_data.COL_ESCALATION_USER_ID);								
							}						
						}
						if(principalName.trim().length() > 0 && principalName.startsWith("<<") && principalName.endsWith(">>")){
							VWTaskIconProperties.btnAdd.setEnabled(false);							
						}
					}else{
						String selectedApproverIndexName = gTable.getValueAt(selectedRow, gTable.m_data.COL_APPROVERNAME).toString();
						if(!selectedApproverIndexName.startsWith("<<") && !selectedApproverIndexName.endsWith(">>")){
							VWTaskIconProperties.btnAdd.setEnabled(true);
						}
						gTable.setValueAt(String.valueOf(principal.getUserGroupId()), selectedRow, gTable.m_data.COL_ESCALATION_USER_ID);
					}
				}catch (Exception e) {
					AdminWise.printToConsole("Exc : "+e.getMessage());
				}
				return principalName;
			}else{
				return "";
			}
		}
		else
			return editor.getCellEditorValue();
	}
	public boolean stopCellEditing() {
		return editor.stopCellEditing();
	}
	public void cancelCellEditing() {
		editor.cancelCellEditing();
	}

	public boolean isCellEditable(EventObject anEvent) {
		/*int column =0;
		    int row =0;
		    if (anEvent instanceof MouseEvent) {
			    Point point=((MouseEvent)anEvent).getPoint();
		        column = gTable.columnAtPoint(point);
		        row = gTable.rowAtPoint(point);
		    }
		    boolean ret=false;
		    if(column==1) ret = true;

		    if(gTable.getSelectedColumn() >= 2)
			  return true;
		  else
			  return false;*/
		return true;
	}
	public void addCellEditorListener(CellEditorListener l) {
		editor.addCellEditorListener(l);
	}
	public void removeCellEditorListener(CellEditorListener l) {
		editor.removeCellEditorListener(l);
	}
	public boolean shouldSelectCell(EventObject anEvent) {
		return editor.shouldSelectCell(anEvent);
	}
}
