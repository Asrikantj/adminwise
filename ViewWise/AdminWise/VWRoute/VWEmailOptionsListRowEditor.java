package ViewWise.AdminWise.VWRoute;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

/**
 * @author Vijaypriya.B.K
 * December 07, 2010
 */

public class VWEmailOptionsListRowEditor implements TableCellEditor {
	  
	  protected TableCellEditor editor, defaultEditor, checkboxEditor;
	  protected VWEmailOptionsListTable gTable=null;
	  public VWEmailOptionsListRowEditor(VWEmailOptionsListTable table) {
	    defaultEditor = new DefaultCellEditor(new JTextField());
	    gTable=table;
	  }
	  public Component getTableCellEditorComponent(JTable table,
	      Object value, boolean isSelected, int row, int column) {
				 editor=defaultEditor;     
		    return editor.getTableCellEditorComponent(table,
		             value, isSelected, row, column);
		    
	  }

	  public Object getCellEditorValue() {
	    return editor.getCellEditorValue();
	  }
	  public boolean stopCellEditing() {
	    return editor.stopCellEditing();
	  }
	  public void cancelCellEditing() {
	    editor.cancelCellEditing();
	  }
	  public boolean isCellEditable(EventObject anEvent) {
		  return true;
	  }
	  public void addCellEditorListener(CellEditorListener l) {
	    editor.addCellEditorListener(l);
	  }
	  public void removeCellEditorListener(CellEditorListener l) {
	    editor.removeCellEditorListener(l);
	  }
	  public boolean shouldSelectCell(EventObject anEvent) {
	    return editor.shouldSelectCell(anEvent);
	  }

}