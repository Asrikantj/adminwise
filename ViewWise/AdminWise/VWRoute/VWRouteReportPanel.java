package ViewWise.AdminWise.VWRoute;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.computhink.common.Creator;
import com.computhink.common.CustomList;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.Index;
import com.computhink.common.Node;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteReportSettings;
import com.computhink.common.Search;
import com.computhink.common.SearchCond;
import com.computhink.common.Util;
import com.computhink.common.VWRetention;
import com.computhink.common.util.JTextFieldLimit;
import com.computhink.common.util.VWDateComboBox;
import com.computhink.common.util.VWRefCreator;
import com.computhink.common.vwfind.ui.VWFindDlg;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWCPreferences;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.notification.VWNotificationConstants;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWFind.VWDTIndicesDlg;
import ViewWise.AdminWise.VWFind.VWDlgLocation;
import ViewWise.AdminWise.VWFind.VWFindConnector;
import ViewWise.AdminWise.VWFind.VWFindPanel;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRetention.VWRetentionConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWRouteReportPanel extends JPanel implements VWConstant, VWNotificationConstants {
	int nTop, nLeft, nHeight, nWidth, nCtrlDiff, nLblDiff;
	/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
	public int staticRepDatePanelLeft = 0, staticRepDatePanelTop = 0, staticRepLocPanelLeft = 0,
			staticRepLocPanelTop = 0, staticRepOutputPanelLeft = 0, staticRepOutputPanelTop = 0;
	public int staticRepCabinetPanelTop = 0, staticRepCabinetPanelLeft = 0, titleLeft = 0, titleTop = 0;
	private static final long serialVersionUID = 1L;
	public static int selectedIndexWith = 1, selectedTextWith = 1;
	VWLinkedButton btnReportLocation = null;
	// JTextField txtReportLocation = null;
	public VWDTIndicesDlg docTypeIndicesDlg = null;
	private Vector searchDTIndices = null;
	VWFindPanel findPanel = null;
	static VWCheckList columnsList = new VWCheckList();
	static ArrayList<CustomList> defaultColumnList = null;
	String columnDescription = null;
	String staticReportID = null, staticReportType = null;
	JLabel lblCaption = new JLabel(LBL_ROUTINGTITLE);
	int mouseEventCount = 0;

	public VWRouteReportPanel() {
		setupUI();
	}

	public void setupUI() {
		try {
			int top = 116;
			int hGap = 28;
			if (UILoaded)
				return;
			int height = 24, width = 144, left = 12, heightGap = 30;
			setLayout(new BorderLayout());
			VWNotificationSetting.setLayout(null);
			add(VWNotificationSetting, BorderLayout.CENTER);
			PanelCommand.setLayout(null);
			VWNotificationSetting.add(PanelCommand);
			PanelCommand.setBackground(AdminWise.getAWColor());
			PanelCommand.setBounds(0, 0, 168, 432);

			PanelCommand.add(picPanel);
			picPanel.setBounds(8, 16, 149, 92);
			AdminWise.printToConsole("LBL_AVAILABLEReport :" + LBL_AVAILABLEReport);
			lblAvailableReport.setText(LBL_AVAILABLEReport);
			PanelCommand.add(lblAvailableReport);
			lblAvailableReport.setBackground(java.awt.Color.white);
			lblAvailableReport.setBounds(left, top, width, height);

			top += hGap;
			PanelCommand.add(cmbAvailebReportsList);
			cmbAvailebReportsList.setBackground(java.awt.Color.white);
			cmbAvailebReportsList.setBounds(left - 1, top, width + 4, height - 2);
			String localLanguage = AdminWise.getLocaleLanguage();
			if (localLanguage.equals("nl_NL")) {
				String workflowReportString = LBL_WFREPORTNAME;
				String str1 = workflowReportString.substring(0, 30);
				String str2 = workflowReportString.substring(30, workflowReportString.length());
				lblWFReportName.setText("<html><br>" + str1 + "<br>" + str2 + "</html>");
			} else
				lblWFReportName.setText(LBL_WFREPORTNAME);
			PanelCommand.add(lblWFReportName);
			if (localLanguage.equals("nl_NL")) {
				lblWFReportName.setBounds(left - 1, top - 50, width + 4, height + 20);
			} else
				lblWFReportName.setBounds(left - 1, top - 25, width + 4, height - 2);
			// top += hGap ;
			PanelCommand.add(txtReportName);
			txtReportName.setBackground(java.awt.Color.white);
			txtReportName.setBounds(left - 1, top, width + 4, height - 2);
			txtReportName.setDocument(new JTextFieldLimit(50));

			top += hGap;
			BtnNewReport.setText(BTN_NEWREPORT_NAME);
			BtnNewReport.setIcon(VWImages.NewRouteIcon);
			BtnNewReport.setActionCommand(BTN_NEWREPORT_NAME);
			PanelCommand.add(BtnNewReport);
			BtnNewReport.setBounds(left, top, width, height);
			BtnNewReport.setVisible(true);

			top += hGap;
			/**
			 * CV2020 - Commented because there is no use of Update button in Workflow
			 * Report tab
			 */
			/*
			 * BtnUpdate.setText(BTN_UPDATE_NAME);
			 * BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
			 */
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setActionCommand(BTN_CANCEL_NAME);
			PanelCommand.add(BtnUpdate);
			BtnUpdate.setBounds(left, top, width, height);
			// BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnUpdate.setIcon(VWImages.CloseIcon);

			top += hGap;
			BtnDelete.setText(BTN_DELETE_NAME);
			BtnDelete.setActionCommand(BTN_DELETE_NAME);
			PanelCommand.add(BtnDelete);
			BtnDelete.setBounds(left, top, width, height);
			BtnDelete.setIcon(VWImages.DelIcon);
			top += hGap + 20;

			BtnGenerateCVReport.setText(BTN_GENERATECVREPORT);
			BtnGenerateCVReport.setIcon(VWImages.DocumentInRouteIcon);
			BtnGenerateCVReport.setActionCommand(BTN_GENERATECVREPORT);
			PanelCommand.add(BtnGenerateCVReport);
			BtnGenerateCVReport.setBounds(left, top, width, height);
			BtnGenerateCVReport.setVisible(true);

			top += hGap + 20;
			BtnGenerateReport.setText(BTN_GENERATEREPORT);
			BtnGenerateReport.setIcon(VWImages.DocumentInRouteIcon);
			BtnGenerateReport.setActionCommand(BTN_GENERATEREPORT);
			PanelCommand.add(BtnGenerateReport);
			BtnGenerateReport.setBounds(left, top, width, height);
			BtnGenerateReport.setVisible(false);
			// THIS BUTTON IS USED TO VIEW JASPER REPORTS
			top += hGap + 20;
			/*BtnPreviewReport.setText(BTN_PREVIEWREPORT);
			BtnPreviewReport.setIcon(VWImages.PreviewInRouteIcon);
			BtnPreviewReport.setActionCommand(BTN_PREVIEWREPORT);
			PanelCommand.add(BtnPreviewReport);
			BtnPreviewReport.setBounds(left, 310, width, height);
			BtnPreviewReport.setVisible(false);*/

			top += heightGap;
			PanelTable.setLayout(new BorderLayout());
			VWNotificationSetting.add(PanelTable);
			PanelTable.setBounds(168, 0, 700, 659);
			SPTable.setBounds(0, 0, 902, 669);
			SPTable.setOpaque(true);
			PanelTable.add(SPTable, BorderLayout.CENTER);
			SPTable.getViewport().add(Table);
			Table.getParent().setBackground(java.awt.Color.white);
			// CV10 modified from mode unconnect to connect

			BtnProgressClick.setText("");
			PanelCommand.add(BtnProgressClick);
			BtnProgressClick.setBounds(left, top, width, height);
			BtnProgressClick.setVisible(false);
			BtnProgressClick.addActionListener(new SymAction());
			setEnableMode(MODE_CONNECT);

			initNotification();

			PanelRouteSetting.setVisible(false);
			PanelRouteSetting.setEnabled(false);

			SymComponent aSymComponent = new SymComponent();
			VWNotificationSetting.addComponentListener(aSymComponent);

			SymAction lSymAction = new SymAction();
			BtnNewReport.addActionListener(lSymAction);
			BtnGenerateReport.addActionListener(lSymAction);
			//BtnPreviewReport.addActionListener(lSymAction);
			BtnGenerateCVReport.addActionListener(lSymAction);
			BtnProgressClick.addActionListener(lSymAction);
			BtnUpdate.addActionListener(lSymAction);
			BtnDelete.addActionListener(lSymAction);
			menuGenerateReport.addActionListener(lSymAction);
			rbuttondate1.addActionListener(lSymAction);
			rbuttondate2.addActionListener(lSymAction);
			/** CV2019 code merges from CV10.2 workflow report enhancement ***/
			BtnNewFindLocation.addActionListener(lSymAction);

			BtnDTRefresh.addActionListener(lSymAction);
			BtnDocTypeIndices.addActionListener(lSymAction);
			BtnDeSelectDT.addActionListener(lSymAction);
			BtnAuthorDeSelect.addActionListener(lSymAction);
			BtnAuthorSelectAll.addActionListener(lSymAction);
			BtnAuthorRefresh.addActionListener(lSymAction);

			SymItem symItem = new SymItem();
			cmbAvailebReportsList.addItemListener(symItem);
			cmbModule.addItemListener(symItem);
			cmbName.addItemListener(symItem);

			Table.getParent().setBackground(java.awt.Color.white);
			PanelRouteSetting.setAutoscrolls(true);
			PanelRouteSetting.setPreferredSize(new Dimension(900, 700));

			SPTable.setPreferredSize(new Dimension(700, 500));

			statusLabel.setVisible(true);
			repaint();
			doLayout();
			UILoaded = true;
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while setting UI : " + ex.getMessage());
		}
	}

	private void initNotification() {
		try {

			PanelRouteSetting.setLayout(null);
			PanelTable.add(PanelRouteSetting, BorderLayout.CENTER);

			PanelRouteSetting.setBackground(Color.WHITE);
			int x = 160, y = 100, h = 20, gap = 10, x1 = 250;

			lblModule.setText(AdminWise.connectorManager.getString("VWNotificationPanel.lblModule"));
			lblModule.setBounds(x, y, 80, h);
			// PanelNotificationSetting.add(lblModule);

			cmbModule.setBounds(x1, y, 150, h);
			cmbModule.setBackground(Color.white);

			for (int i = 2; i < NotificationModulesAdminWise.length; i++) // i=2 - To avoid Folder and Docment modules
																			// in AdminWise
				cmbModule.addItem(NotificationModulesAdminWise[i]);

			y += h + gap;
			lblName.setText(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") + " "
					+ AdminWise.connectorManager.getString("Notification.LBL.Name"));
			lblName.setBounds(x, y, 100, h);

			cmbName.setBounds(x1, y, 150, h);
			cmbName.setBackground(Color.white);

			/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
			nTop = 0;

			y += h + gap;
			nTop = nTop + 20;
			lblTitle.setText("Workflow Report");
			lblTitle.setBounds(45, nTop - 10, 800, 20);
			lblTitle.setFont(new Font("ARIAL", Font.BOLD, 16));
			PanelRouteSetting.add(lblTitle);
			titleLeft = 45;
			titleTop = nTop - 10;
			/** CV2019 code merges from CV10.2 line ***/
			AdminWise.printToConsole("Before report type  :" + nTop);
			loadReportType();
			nLeft = 44;
			lblReportType.setText(AdminWise.connectorManager.getString("RouteReport.lblReportType"));
			lblReportType.setBounds(nLeft + 1, nTop + 35, 180, 20);
			// lblReportType.setBounds(45, nTop + 25, 100, 20);
			PanelRouteSetting.add(lblReportType);

			cmbReportType.setBounds(nLeft + 247, nTop + 35, 200, 20);
			// cmbReportType.setBounds(120, nTop + 25, 200, 20);
			cmbReportType.addItemListener(new SymReportListAction());
			PanelRouteSetting.add(cmbReportType);

			nTop = nTop + 20;
			int top = nTop + 20;
			AdminWise.printToConsole("Before panleAvailable  :" + nTop);
			panleAvailable.setBounds(45, top, 301, 25); // 25,20,209,25 //258,60,242,25
			panleAvailable.setBackground(new Color(112, 146, 190));

			panleActions.setBounds(386, top, 285, 25); // 258,20,242,25 //25,60,209,25
			panleActions.setBackground(new Color(112, 146, 190));

			panleUserGroup.setBounds(713, top, 152, 25); // 525,60,140,25
			panleUserGroup.setBackground(new Color(112, 146, 190));

			routePanel.setLayout(null);
			routePanel.setBounds(0, nTop, 850, 435);
			routePanel.setBackground(Color.WHITE);

			generalPanel.setLayout(null);
			generalPanel.setBounds(0, nTop, 850, 435);
			generalPanel.setBackground(Color.WHITE);
			loadGeneralPanelData();

			routePanel.add(panleActions);
			routePanel.add(panleAvailable);
			routePanel.add(panleUserGroup);

			/*
			 * panleTimePeriod.setBounds(45,355,301,25); panleTimePeriod.setBackground(new
			 * Color(112,146,190));
			 */

			/*
			 * txtMonth.setBounds(58,360,20,20); txtMonth.setBorder(new EtchedBorder());
			 * txtDay.setBounds(118,360,20,20); txtDay.setBorder(new EtchedBorder());
			 * txtHour.setBounds(178,360,20,20); txtHour.setBorder(new EtchedBorder());
			 * 
			 * txtMonth.setVisible(false); txtDay.setVisible(false);
			 * txtHour.setVisible(false);
			 */
			Rectangle rectangle = panleActions.getBounds();
			nTop = rectangle.y + 30;
			AdminWise.printToConsole("Before available workflow panel :" + nTop);
			availableWorkFlowPanel.setBounds(30, nTop, 330, 360); // 250,100,260,170 //250,90,260,200
			routePanel.add(availableWorkFlowPanel);
			availableWorkFlowPanel.setBackground(Color.white);
			availableWorkFlowPanel.setVisible(true);

			actionsWorkFlowPanel.setBounds(375, nTop, 310, 360); // 15,100,230,170 //250,90,260,200
			routePanel.add(actionsWorkFlowPanel);
			actionsWorkFlowPanel.setBackground(Color.white); // white
			actionsWorkFlowPanel.setVisible(true);

			userGroupPanel.setBounds(700, nTop, 181, 360); // 505,100,181,170 //501,100,191,170
			routePanel.add(userGroupPanel);
			userGroupPanel.setBackground(Color.white);
			userGroupPanel.setVisible(true);

			/**** Cabinet panel *****************************************************/
			nLeft = 45;
			AdminWise.printToConsole("before cabinetPanel nTop  :" + nTop);
			nTop = routePanel.getX() + routePanel.getHeight() + 60;
			AdminWise.printToConsole("cabinetPanel top :" + nTop);
			staticRepCabinetPanelTop = nTop;
			staticRepCabinetPanelLeft = nLeft;
			cabinetPanel.setLayout(null);
			cabinetPanel.setBounds(nLeft, nTop, 850, 30);
			cabinetPanel.setBackground(Color.WHITE);
			cabinetPanel.setVisible(true);
			PanelRouteSetting.add(cabinetPanel, BorderLayout.CENTER);

			nLeft = 0;
			nTop = 0;
			nWidth = 386;
			nHeight = 20;
			String selectedPath = "";

			JLblLocation.setBounds(nLeft, nTop, 72, nHeight);
			JLblLocation.setText(LBL_CABINET_LOCATION);
			nLeft = nLeft + 60;
			TxtFieldFindLocation.setBounds(nLeft, nTop, nWidth, nHeight);
			TxtFieldFindLocation.setEditable(false);
			TxtFieldFindLocation.setText(selectedPath);
			nLeft = (nLeft + nWidth) - 2;
			BtnNewFindLocation.setBounds(nLeft, nTop, 24, nHeight + 1);
			BtnNewFindLocation.setIcon(VWImages.BrowseIcon);

			cabinetPanel.add(JLblLocation);
			cabinetPanel.add(TxtFieldFindLocation);
			cabinetPanel.add(BtnNewFindLocation);
			/****
			 * End of Cabinet panel
			 *****************************************************/

			AdminWise.printToConsole("Top before datepanel :" + (cabinetPanel.getX() + cabinetPanel.getHeight()));
			nLeft = 45;
			rectangle = cabinetPanel.getBounds();
			nTop = rectangle.x + rectangle.y + 5;
			staticRepDatePanelLeft = nLeft;
			staticRepDatePanelTop = nTop;

			AdminWise.printToConsole("datePanel top :" + nTop);
			datePanel.setLayout(null);
			datePanel.setBounds(nLeft, nTop, 850, 70);
			datePanel.setBackground(Color.WHITE);

			datePanel.setVisible(true);
			PanelRouteSetting.add(datePanel, BorderLayout.CENTER);

			// nLeft = 45; nTop = 420;
			nLeft = 0;
			nTop = 0;
			nWidth = 20;
			nHeight = 20;
			rbuttondate1.setText("");
			rbuttondate1.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(rbuttondate1);
			rbuttondate1.setBackground(Color.white);
			rbuttondate1.setSelected(true);

			nLeft = nLeft + 20;
			nWidth = 30;
			lblStartDate.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(lblStartDate);

			nLeft = nLeft + nWidth + 10;
			nWidth = 90;
			TxtMFrom.setBounds(nLeft, nTop, nWidth, 24);// 70,395,105,22
			TxtMFrom.setDateFormat("MM/dd/yyyy");
			datePanel.add(TxtMFrom);

			nLeft = nLeft + nWidth + 3;
			nWidth = 84;
			Date date = new Date();
			spinnerdate = new Date();
			sm = new SpinnerDateModel(spinnerdate, null, null, Calendar.HOUR_OF_DAY);
			timeSpinnerFrom = new JSpinner(sm);
			datespinner = new JSpinner.DateEditor(timeSpinnerFrom, "hh:mm:ss a");
			timeSpinnerFrom.setEditor(datespinner);
			timeSpinnerFrom.setBounds(nLeft, nTop, nWidth, 24);
			datePanel.add(timeSpinnerFrom);

			nLeft = nLeft + nWidth + 23;
			nWidth = 30;
			// lblEndDate.setBounds(210+150-30,380+50,90,20);//222,370,90,20
			lblEndDate.setBounds(nLeft, nTop, nWidth, 20);// 222,370,90,20
			datePanel.add(lblEndDate);

			nLeft = nLeft + nWidth;
			nWidth = 90;
			TxtMTo.setBounds(nLeft, nTop, nWidth, 24);// 222,395,105,22
			TxtMTo.setDateFormat("MM/dd/yyyy");
			datePanel.add(TxtMTo);

			nLeft = nLeft + nWidth + 3;
			nWidth = 84;
			sm1 = new SpinnerDateModel(spinnerdate, null, null, Calendar.HOUR_OF_DAY);
			timeSpinnerTo = new JSpinner(sm1);
			datespinner1 = new JSpinner.DateEditor(timeSpinnerTo, "hh:mm:ss a");
			timeSpinnerTo.setEditor(datespinner1);
			;
			timeSpinnerTo.setBounds(nLeft, nTop, nWidth, 24);
			datePanel.add(timeSpinnerTo);

			// Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			String strDate = sdf.format(date);
			AdminWise.printToConsole("strDate TxtMFrom 1 :::: " + strDate);
			TxtMFrom.setText(strDate);
			TxtMTo.setText(strDate);

			// nLeft = 45; nTop = 460;
			nLeft = 0;
			nTop = nTop + TxtMFrom.getHeight() + 20;
			nWidth = 20;
			rbuttondate2.setText("");
			rbuttondate2.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(rbuttondate2);
			rbuttondate2.setBackground(Color.white);
			rbuttondate2.setSelected(false);
			lbllast.setVisible(true);
			lblMonth.setVisible(true);
			lblDay.setVisible(true);
			lblHour.setVisible(true);
			// -----------------------------------------------------------------------------
			nLeft = nLeft + nWidth;
			nWidth = 70;
			lbllast.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(lbllast);

			nLeft = nLeft + nWidth + 10;
			nWidth = 40;
			initSpinners1();
			spinnerTaskInterval1.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(spinnerTaskInterval1);

			nLeft = nLeft + nWidth + 2;
			lblMonth.setBounds(nLeft, nTop, nWidth, 20);// 122
			datePanel.add(lblMonth);

			nLeft = nLeft + nWidth + 18;
			initSpinners2();
			spinnerTaskInterval2.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(spinnerTaskInterval2);

			nLeft = nLeft + nWidth + 2;
			lblDay.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(lblDay);

			nLeft = nLeft + nWidth + 18;
			initSpinners3();
			spinnerTaskInterval3.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(spinnerTaskInterval3);

			nLeft = nLeft + nWidth + 2;
			lblHour.setBounds(nLeft, nTop, nWidth, 20);
			datePanel.add(lblHour);
			// -----------------------------------------------------------------------------

			nLeft = 45;
			AdminWise.printToConsole("datePanel.getX() :" + datePanel.getX());
			nTop = staticRepDatePanelTop + datePanel.getHeight() + 15;
			staticRepLocPanelLeft = nLeft;
			staticRepLocPanelTop = nTop;
			AdminWise.printToConsole("reportInfoPanel top position : " + nTop);
			reportInfoPanel.setLayout(null);
			reportInfoPanel.setBounds(nLeft, nTop, 850, 30);
			reportInfoPanel.setBackground(Color.WHITE);

			// ------------------Report info panel components------------------------------
			AdminWise.printToConsole("Report location X :" + nLeft);
			nTop = 0;
			nLeft = 0;
			nWidth = 85;
			nHeight = 20;
			
			AdminWise.printToConsole("Output format :" + nLeft);
			AdminWise.printToConsole("Output format top :" + nTop);
			lblOutputformat.setText(AdminWise.connectorManager.getString("RouteReport.outputFormat"));
			lblOutputformat.setBounds(nLeft, nTop, nWidth, nHeight);
			reportInfoPanel.add(lblOutputformat);
			nLeft = nLeft + nWidth + 15;
			nWidth = 20;
			rButtonHtml.setBounds(nLeft, nTop, nWidth, nHeight);
			rButtonHtml.addActionListener(new SymAction());
			rButtonHtml.setBackground(Color.white);
			rButtonHtml.setSelected(true);
			reportInfoPanel.add(rButtonHtml);
			nLeft = nLeft + nWidth;
			lblHtml.setText("Html");
			lblHtml.setBounds(nLeft, nTop, nWidth, nHeight);
			reportInfoPanel.add(lblHtml);

			nLeft = nLeft + nWidth + 27;
			rButtonXls.setBounds(nLeft, nTop, nWidth, nHeight);
			rButtonXls.addActionListener(new SymAction());
			rButtonXls.setBackground(Color.white);
			reportInfoPanel.add(rButtonXls);
			nLeft = nLeft + nWidth;
			lblXls.setText("Xls");
			lblXls.setBounds(nLeft, nTop, nWidth, nHeight);
			reportInfoPanel.add(lblXls);

			nLeft = nLeft + nWidth + 27;
			rButtonCsv.setBounds(nLeft, nTop, nWidth, nHeight);
			rButtonCsv.addActionListener(new SymAction());
			rButtonCsv.setBackground(Color.white);
			reportInfoPanel.add(rButtonCsv);

			nLeft = nLeft + nWidth;
			lblCsv.setText("Csv");
			lblCsv.setBounds(nLeft, nTop, nWidth, nHeight);
			reportInfoPanel.add(lblCsv);

			nLeft = nLeft + nWidth + 27;
			rButtonPdf.setBounds(nLeft, nTop, nWidth, nHeight);
			rButtonPdf.addActionListener(new SymAction());
			rButtonPdf.setBackground(Color.white);
			//reportInfoPanel.add(rButtonPdf);
			nLeft = nLeft + nWidth;
			lblPdf.setText("Pdf");
			lblPdf.setBounds(nLeft, nTop, nWidth, nHeight);
			//reportInfoPanel.add(lblPdf);

			reportInfoPanel.setVisible(false);
			PanelRouteSetting.add(reportInfoPanel, BorderLayout.CENTER);
			// ------------------End of Report info panel
			// components------------------------------
			/*
			 * settingsPanel.setBounds(0,90,220,320);
			 * settingsPanel.setBackground(Color.white); routePanel.add(settingsPanel);
			 */
			panleActions.setVisible(true);

			routePanel.setVisible(true);
			PanelRouteSetting.add(routePanel, BorderLayout.CENTER);
			generalPanel.setVisible(false);
			PanelRouteSetting.add(generalPanel, BorderLayout.CENTER);

			lblAction.setText(AdminWise.connectorManager.getString("constant.LBL_SELECT_NAME"));
			lblAction.setBounds(95, 5, 60, 10);
			lblAction.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblAction.setForeground(Color.white);
			panleActions.add(lblAction);
			lblAction.setVisible(true);

			lblWorkFlow.setBounds(250, 200, 50, 20);
			lblWorkFlow.setText("WorkFlow");
			lblWorkFlow.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblWorkFlow.setForeground(Color.white);
			panleActions.add(lblWorkFlow);
			lblWorkFlow.setVisible(true);

			lblAvailableWorkFlow.setBounds(50, 5, 50, 20);
			lblAvailableWorkFlow.setText(AdminWise.connectorManager.getString("constant.LBL_SELECT_NAME") + " "
					+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") + "s" + " "
					+ AdminWise.connectorManager.getString("RouteReport.lblavailableWFTask"));
			lblAvailableWorkFlow.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblAvailableWorkFlow.setForeground(Color.white);
			panleAvailable.add(lblAvailableWorkFlow);
			lblAvailableWorkFlow.setVisible(true);

			spinnerTaskInterval1.setEnabled(false);
			spinnerTaskInterval2.setEnabled(false);
			spinnerTaskInterval3.setEnabled(false);

			lblTask.setBounds(140, 30, 50, 20);

			lblTask.setText(AdminWise.connectorManager.getString("RouteReport.lbltask"));
			lblTask.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblTask.setForeground(Color.white);
			panleActions.add(lblTask);
			lblTask.setVisible(true);

			lblUserGroup.setText(AdminWise.connectorManager.getString("RouteReport.lblUsers"));
			lblUserGroup.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblUserGroup.setForeground(Color.white);
			panleUserGroup.add(lblUserGroup);
			lblUserGroup.setVisible(true);

			lblTimePeriod.setBounds(0, 50 + 35, 50, 20);
			lblTimePeriod.setText(AdminWise.connectorManager.getString("RouteReport.timeperiod"));
			lblTimePeriod.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblTimePeriod.setForeground(Color.white);
			panleTimePeriod.add(lblTimePeriod);
			lblTimePeriod.setVisible(true);

			/*
			 * txtMonth.setVisible(false); txtDay.setVisible(false);
			 * txtHour.setVisible(false);
			 */
			lblMonth.setVisible(true);
			lblDay.setVisible(true);
			lblHour.setVisible(true);
			findPanel = new VWFindPanel(docTypeIndicesDlg, gCurRoom, LblDocType, LstAuthors);
			/** End of CV10.2 code merges ****/
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while initialising notification settings panel :: " + ex.getMessage());
		}
	}

	private void loadInitSettings() {
		try {
			int selIndex = (cmbModule.getSelectedIndex() + 2); // +2 is to avoid Document and Folder Notification
																// Settings
			AdminWise.printToConsole("loadinitsettings selIndex:   " + selIndex);
			switch (selIndex) {

			case DOCUMENT_TYPE:
				loadDocTypes();
				break;

			case RETENTION:
				loadRetentionNames();
				break;

			case ROUTE:
				loadRouteNames();
				break;

			case STORAGE_MANAGEMENT:
			case AUDIT_TRAIL:
			case RECYCLE_DOCUMENTS:
			case RESTORE_DOCUMENTS:
			case REDACTIONS:
				lblName.setVisible(false);
				cmbName.setVisible(false);
				break;
			}
			cmbName.addItem(LBL_REFRESH_NAME);
			int nodeId = getNodeId(selIndex);
			loadSettings();
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in loadInitSettings() : " + e.getMessage());
		}
	}

	private void loadDocTypes() {
		try {
			cmbName.removeAllItems();
			cmbName.setVisible(true);
			lblName.setText(AdminWise.connectorManager.getString("VWNotificationPanel.DoclblName"));
			lblName.setVisible(true);
			Vector docTypes = VWDocTypeConnector.getDocTypes();
			if (docTypes != null && docTypes.size() > 0) {
				for (int i = 0; i < docTypes.size(); i++) {
					cmbName.addItem(docTypes.get(i));
				}
			} else {
				cmbName.addItem("");
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception while loading document types : " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void loadRouteNames() {
		try {
			Vector vRouteList = new Vector();
			lblName.setText(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") + " "
					+ AdminWise.connectorManager.getString("Notification.LBL.Name"));
			lblName.setVisible(true);

			// To get all the route names pass -1 for routeId
			AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, -1, vRouteList);

			cmbName.removeAllItems();
			cmbName.setVisible(true);
			if (vRouteList != null && vRouteList.size() > 0) {
				for (int i = 0; i < vRouteList.size(); i++) {
					cmbName.addItem(vRouteList.get(i));
				}
			} else {
				cmbName.addItem("");
			}
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while loading route names :: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void initSpinners1() {
		int min = 0;
		int max = 99;
		int step = 1;
		int initValue = 0;
		SpinnerModel model1 = new SpinnerNumberModel(initValue, min, max, step);
		spinnerTaskInterval1 = new JSpinner(model1);
	}

	private void initSpinners2() {
		int min = 0;
		int max = 365;
		int step = 1;
		int initValue = 0;
		SpinnerModel model2 = new SpinnerNumberModel(initValue, min, max, step);
		spinnerTaskInterval2 = new JSpinner(model2);

	}

	private void initSpinners3() {
		int min = 0;
		int max = 24;
		int step = 1;
		int initValue = 0;
		SpinnerModel model3 = new SpinnerNumberModel(initValue, min, max, step);
		spinnerTaskInterval3 = new JSpinner(model3);
	}

	private void loadRetentionNames() {
		try {
			Vector vRetList = new Vector();
			lblName.setText(AdminWise.connectorManager.getString("VWNotificationPanel.loadRetentionNames"));
			lblName.setVisible(true);
			// settingsPanel.setBounds(150, 160, 388, 150);
			// settingsPanel.setBounds(150, 160, 388, 315);
			vRetList = VWRetentionConnector.getRetentionSettings(0);// AdminWise.gConnector.ViewWiseClient.getRetentionSettings(gCurRoom,
																	// 0, vRetList);
			cmbName.removeAllItems();
			cmbName.setVisible(true);

			if (vRetList != null && vRetList.size() > 0) {
				for (int i = 0; i < vRetList.size(); i++) {
					cmbName.addItem(vRetList.get(i));
				}
			} else {
				cmbName.addItem("");
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception while loading retention names :: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * CV10.2 workflow report enhancement changes
	 * 
	 * @author vanitha.s
	 */
	private void clearStaticReportSettings() {
		AdminWise
				.printToConsole("staticRepDatePanelTop......." + staticRepDatePanelLeft + ", " + staticRepDatePanelTop);
		AdminWise.printToConsole("staticRepLocPanelTop......." + staticRepLocPanelLeft + ", " + staticRepLocPanelTop);
		if (staticRepDatePanelLeft != 0 && staticRepLocPanelTop != 0) {
			lblTitle.setBounds(titleLeft, titleTop, lblTitle.getWidth(), lblTitle.getHeight());
			lblTitle.setText("Workflow Report");
			datePanel.setBounds(staticRepDatePanelLeft, staticRepDatePanelTop, datePanel.getWidth(),
					datePanel.getHeight());
			reportInfoPanel.setBounds(staticRepLocPanelLeft, staticRepLocPanelTop, reportInfoPanel.getWidth(),
					reportInfoPanel.getHeight());
			cabinetPanel.setBounds(staticRepCabinetPanelLeft, staticRepCabinetPanelTop, cabinetPanel.getWidth(),
					cabinetPanel.getHeight());
		}
	}

	public void setEnableMode(int mode) {
		AdminWise.printToConsole("Mode :: " + mode);
		switch (mode) {
		case MODE_CONNECT:
			CURRENT_MODE = 0;
			cmbAvailebReportsList.setEnabled(true);
			txtReportName.setEnabled(false);
			txtReportName.setVisible(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(false);			
			/**
			 * CV2020 - Commented because there is no use of Update button in Workflow
			 * Report tab
			 */
			/*
			 * BtnUpdate.setText(BTN_UPDATE_NAME); BtnUpdate.setIcon(VWImages.UpdateIcon);
			 */
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(true);
			BtnDelete.setEnabled(false);
			
			lblAvailableReport.setVisible(true);
			cmbAvailebReportsList.setVisible(true);
			BtnNewReport.setEnabled(true);			
			BtnNewReport.setText(BTN_NEWREPORT_NAME);
			BtnNewReport.setIcon(VWImages.NewRouteIcon);
			BtnGenerateReport.setVisible(false);
			BtnGenerateReport.setEnabled(false);
			BtnGenerateCVReport.setEnabled(false);
			//BtnPreviewReport.setEnabled(false);			
			loadWFReports();
			break;

		case MODE_UNSELECTED:
			BtnNewReport.setText(BTN_NEWREPORT_NAME);
			BtnNewReport.setIcon(VWImages.NewRouteIcon);
			BtnNewReport.setEnabled(true);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			//BtnPreviewReport.setEnabled(false);
			/**
			 * CV2020 - Commented because there is no use of Update button in Workflow
			 * Report tab
			 */
			/*
			 * BtnUpdate.setText(BTN_UPDATE_NAME); BtnUpdate.setIcon(VWImages.UpdateIcon);
			 */
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setEnabled(false);
			BtnDelete.setVisible(true);
			lblAvailableReport.setVisible(true);
			cmbAvailebReportsList.setEnabled(true);
			cmbAvailebReportsList.setVisible(true);
			txtReportName.setEnabled(false);
			txtReportName.setVisible(false);
			BtnGenerateReport.setVisible(false);
			BtnGenerateReport.setEnabled(false);
			BtnGenerateCVReport.setEnabled(false);
			loadWFReports();
			// viewMode(false);
			break;

		case MODE_SELECT:
			BtnNewReport.setEnabled(true);
			BtnNewReport.setText(BTN_NEWREPORT_NAME);
			BtnNewReport.setIcon(VWImages.NewRouteIcon);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setVisible(true);
			//BtnPreviewReport.setVisible(false);
			/**
			 * CV2020 - Commented because there is no use of Update button in Workflow
			 * Report tab
			 */
			/*
			 * BtnUpdate.setText(BTN_UPDATE_NAME); BtnUpdate.setIcon(VWImages.UpdateIcon);
			 */
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setEnabled(true);
			BtnDelete.setVisible(true);
			// viewMode(false);
			lblAvailableReport.setVisible(true);
			cmbAvailebReportsList.setVisible(true);
			txtReportName.setEnabled(false);
			txtReportName.setVisible(false);
			BtnGenerateReport.setVisible(false);
			BtnGenerateCVReport.setEnabled(false);

			RouteReportSettings settings = Table.getRowData(Table.getSelectedRow());
			if (settings != null && (settings.getNodeType() == FOLDER || settings.getNodeType() == DOCUMENT))
				BtnUpdate.setEnabled(false);

			break;

		case MODE_DELETE:
			BtnNewReport.setEnabled(true);
			BtnNewReport.setText(BTN_NEWREPORT_NAME);
			BtnNewReport.setIcon(VWImages.NewRouteIcon);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			//BtnPreviewReport.setVisible(false);
			/**
			 * CV2020 - Commented because there is no use of Update button in Workflow
			 * Report tab
			 */
			/*
			 * BtnUpdate.setText(BTN_UPDATE_NAME); BtnUpdate.setIcon(VWImages.UpdateIcon);
			 */
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setEnabled(true);
			BtnDelete.setVisible(true);
			// viewMode(false);
			lblAvailableReport.setVisible(true);
			cmbAvailebReportsList.setVisible(true);
			txtReportName.setEnabled(false);
			txtReportName.setVisible(false);
			BtnGenerateReport.setVisible(false);
			BtnGenerateCVReport.setEnabled(false);
			break;

		case MODE_UNCONNECT:
			CURRENT_MODE = 0;
			cmbAvailebReportsList.removeAllItems();
			cmbAvailebReportsList.addItem(LBL_AVAILABLE_ROUTEREPORT);
			cmbAvailebReportsList.setVisible(true);

			BtnNewReport.setText(BTN_NEWREPORT_NAME);
			BtnNewReport.setIcon(VWImages.NewRouteIcon);
			BtnNewReport.setEnabled(false);
			BtnNewReport.setVisible(true);

			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			/**
			 * CV2020 - Commented because there is no use of Update button in Workflow
			 * Report tab
			 */
			/*
			 * BtnUpdate.setText(BTN_UPDATE_NAME); BtnUpdate.setIcon(VWImages.UpdateIcon);
			 */
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setEnabled(false);
			BtnDelete.setVisible(true);

			lblAvailableReport.setVisible(true);
			cmbAvailebReportsList.setEnabled(false);
			txtReportName.setEnabled(true);
			txtReportName.setVisible(true);
			BtnGenerateReport.setVisible(false);
			BtnGenerateReport.setEnabled(false);
			BtnGenerateCVReport.setEnabled(false);
			gCurRoom = 0;
			break;

		case MODE_NEW:
			BtnNewReport.setEnabled(true);
			BtnNewReport.setText(BTN_SAVE_NAME);
			BtnNewReport.setIcon(VWImages.SaveIcon);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(true);
			BtnDelete.setEnabled(true);
			// BtnNotifications.setIcon(null);
			lblAvailableReport.setVisible(false);
			cmbAvailebReportsList.setVisible(false);
			cmbModule.setEnabled(true);
			cmbName.setEnabled(true);
			txtReportName.setEnabled(true);
			txtReportName.setVisible(true);
			BtnGenerateReport.setVisible(false);
			BtnGenerateReport.setEnabled(true);
			//BtnPreviewReport.setEnabled(true);
			BtnGenerateCVReport.setEnabled(true);
			// viewMode(true);
			break;

		case MODE_UPDATE:
			BtnNewReport.setEnabled(true);
			BtnNewReport.setText(BTN_SAVE_NAME);
			BtnNewReport.setIcon(VWImages.SaveIcon);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.DelIcon);
			BtnDelete.setVisible(true);
			BtnDelete.setEnabled(true);
			lblAvailableReport.setVisible(false);
			cmbAvailebReportsList.setVisible(false);
			cmbModule.setEnabled(false);
			cmbName.setEnabled(false);
			txtReportName.setEnabled(true);
			txtReportName.setVisible(true);
			BtnGenerateReport.setVisible(false);
			BtnGenerateReport.setEnabled(false);
			//BtnPreviewReport.setEnabled(false);
			BtnGenerateCVReport.setEnabled(true);
			// viewMode(true);
			break;
		case MODE_STATIC_REPORT:
			BtnNewReport.setEnabled(true);
			BtnNewReport.setText(BTN_SAVE_NAME);
			BtnNewReport.setIcon(VWImages.SaveIcon);
			BtnNewReport.setEnabled(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);

			cmbAvailebReportsList.setVisible(false);
			txtReportName.setVisible(true);
			txtReportName.setText(cmbAvailebReportsList.getSelectedItem().toString());
			txtReportName.setEnabled(false);
			txtReportName.setEditable(false);

			cmbReportType.setVisible(false);
			lblReportType.setVisible(false);
			PanelRouteSetting.setVisible(true);
			routePanel.setVisible(false);
			generalPanel.setVisible(false);
			reportInfoPanel.setVisible(true);
			datePanel.setVisible(true);
			BtnGenerateCVReport.setEnabled(true);
		}
	}

	public void cmbModule_actionPerformed(ItemEvent event) {
		loadInitSettings();
	}

	public void loadTabData(VWRoom newRoom) {
		try {
			if (newRoom.getConnectStatus() != Room_Status_Connect) {
				Table.clearData();
				return;
			}
			if (newRoom.getId() == gCurRoom)
				return;
			gCurRoom = newRoom.getId();
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			AdminWise.printToConsole("loadtabdata method call:::" + gCurRoom);
			loadWFReports();
			loadInitSettings();
			/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
			loadGeneralSettings();
			SPTable.getViewport().add(Table);
			Table.clearData();
			UILoaded = true;
			setEnableMode(MODE_UNSELECTED);
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while loading tab data : " + ex.getMessage());
		}
	}

	private void loadWFReports() {
		try {
			AdminWise.printToConsole("inside loadWFReports.....");
			cmbAvailebReportsList.removeAllItems();
			Vector reports = new Vector();

			cmbAvailebReportsList.addItem(LBL_AVAILABLE_ROUTEREPORT);
			ArrayList<String> staticReports = AdminWise.gConnector.ViewWiseClient.getStaticReportsList(gCurRoom);
			if (staticReports != null) {
				AdminWise.printToConsole("Before adding the static list :" + reports);
				reports = new Vector<>(staticReports);
			}
			AdminWise.printToConsole("Before calling getWFReport method:" + reports);
			AdminWise.gConnector.ViewWiseClient.getWFReport(gCurRoom, 1, "", reports);
			AdminWise.printToConsole("BeAfter calling getWFReport method:" + reports);
			if (reports == null || reports.size() == 0)
				return;
			if (reports != null && reports.size() > 0) {
				for (int i = 0; i < reports.size(); i++) {
					StringTokenizer st = new StringTokenizer(reports.get(i).toString(), "\t");
					String id = st.nextToken();
					String reportName = st.nextToken();
					cmbAvailebReportsList.addItem(reportName);
				}
			}
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while loading users : " + ex.getMessage());
		}
	}

	public void unloadTabData() {
		try {
			SPTable.getViewport().add(Table);
			Table.clearData();
			repaint();
			gCurRoom = 0;
			setEnableMode(MODE_UNCONNECT);
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while unloading tab data: " + ex.getMessage());
		}
	}

	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnNewReport) {
				BtnNewReport_actionPerformed(event);
			} else if (object == BtnUpdate) {
				BtnUpdate_actionPerformed(event);
			} else if (object == BtnDelete) {
				BtnDelete_actionPerformed(event);
			} else if (object == BtnGenerateReport) {
				AdminWise.printToConsole("inside BtnGenerateReport......");
				BtnGenerateReport_actionPerformed(event);
			} else if (object == BtnPreviewReport) {// niki
				AdminWise.printToConsole("preview was called......");
				BtnGenerateJasperReport_actionPerformed(true);
			} else if (object == BtnGenerateCVReport) {
				AdminWise.printToConsole("inside BtnGenerateCVReport......");
				String selectedType = cmbReportType.getSelectedItem().toString().trim();
				AdminWise.printToConsole("Report Type.........." + selectedType);
				if (selectedType.equalsIgnoreCase(VWConstant.lstReportType[0])) {
					AdminWise.printToConsole("inside workflow report creation....");
					BtnGenerateCVReport_actionPerformed(event);
				} else {
					AdminWise.printToConsole("inside jasper creation....");
					BtnGenerateJasperReport_actionPerformed(false);
				}
			} else if (object == menuGenerateReport) {
				BtnGenerateCVReport_actionPerformed(event);
			} else if (object == rbuttondate1) {
				rbuttondate1_actionPerformed(event);
			} else if (object == rbuttondate2) {
				rbuttondate2_actionPerformed(event);
			} else if (object == BtnProgressClick) {
				BtnProgressClick_actionPerformed(event);
			} /*
				 * else if(object == btnReportLocation){
				 * BtnWFReportLocation_actionPerformed(event); }
				 */ else if (object == rButtonPdf || object == rButtonHtml || object == rButtonXls
					|| object == rButtonCsv) {
				outputFormat_actionPerformed(event);
			}
			/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
			else if (object == BtnDocTypeIndices) {
				BtnDocTypeIndicesForReport_actionPerformed(event);
			} else if (object == BtnDTRefresh) {
				BtnDTRefresh_workflowActionPerformed(event);
			} else if (object == BtnDeSelectDT) {
				BtnDocumentDeSelectDT_actionPerformed(event);
			} else if (object == BtnAuthorRefresh) {
				BtnAuthorRefresh_workflowActionPerformed(event);
			} else if (object == BtnAuthorDeSelect) {
				BtnAuthorDeSelect_actionPerformed(event);
			} else if (object == BtnAuthorSelectAll) {
				BtnAuthorSelectAll_actionPerformed(event);
			} else if (object == BtnIndicesRefresh) {
				BtnIndicesRefresh_workflowActionPerformed(event);
			} else if (object == BtnIndicesDeSelect) {
				BtnIndicesDeSelect_actionPerformed(event);
			} else if (object == BtnIndicesSelectAll) {
				BtnIndicesSelectAll_actionPerformed(event);
			} else if (object == BtnNewFindLocation) {
				BtnNewFindLocationt_actionPerformed(event);
			} else if (object == BtnupClick) {
				BtnUpClick_actionPerformed(event, "UP");
			} else if (object == JrxmlBtn) {
				BtnJrxmlLocation_actionPerformed(event);
			} else if (object == BtndownClick) {
				BtnUpClick_actionPerformed(event, "DOWN");
			}
			/** End of CV10.2 code merges ******/
		}
	}

	public void BtnJrxmlLocation_actionPerformed(ActionEvent evt) {
		String com = evt.getActionCommand();
		/*try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

		int r = j.showOpenDialog(null);
		String jrxmlLocationPath = TxtFieldJrxml.getText();
		// if the user selects a file
		if (r == JFileChooser.APPROVE_OPTION)

		{
			// set the label to the path of the selected file
			TxtFieldJrxml.setText(j.getSelectedFile().getAbsolutePath());
			jrxmlLocationPath = TxtFieldJrxml.getText();
		}
		// if the user cancelled the operation
		else {
			jrxmlLocationPath = j.getSelectedFile().getAbsolutePath();
			if (jrxmlLocationPath.equalsIgnoreCase("") || jrxmlLocationPath.equalsIgnoreCase("<Default>"))
				TxtFieldJrxml.setText("<Default>");
		}
		TxtFieldJrxml.setText(jrxmlLocationPath);
	} 

	private void rbuttondate1_actionPerformed(ActionEvent event) {
		/* TODO Auto-generated method stub */
		AdminWise.printToConsole("rbuttondate1_actionPerformed.......");
		if (rbuttondate1.isSelected()) {
			rbuttondate1.setSelected(true);
			TxtMFrom.setEnabled(true);
			TxtMTo.setEnabled(true);
			timeSpinnerFrom.setEnabled(true);
			datespinner.setEnabled(true);
			timeSpinnerTo.setEnabled(true);
			datespinner1.setEnabled(true);
			lblMonth.setEnabled(false);
			lblDay.setEnabled(false);
			lblHour.setEnabled(false);
			spinnerTaskInterval1.setEnabled(false);
			spinnerTaskInterval2.setEnabled(false);
			spinnerTaskInterval3.setEnabled(false);
			rbuttondate2.setSelected(false);
		}
		if (!rbuttondate1.isSelected()) {
			rbuttondate1.setSelected(true);
		}
	}

	private void rbuttondate2_actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		if (rbuttondate2.isSelected()) {
			rbuttondate2.setSelected(true);

			TxtMFrom.setEnabled(false);
			TxtMTo.setEnabled(false);
			AdminWise.printToConsole("before setenable ...");
			timeSpinnerFrom.setEnabled(false);
			datespinner.setEnabled(false);
			timeSpinnerTo.setEnabled(false);
			datespinner1.setEnabled(false);
			AdminWise.printToConsole("after setenable ...");
			lblMonth.setEnabled(true);
			lblDay.setEnabled(true);
			lblHour.setEnabled(true);
			spinnerTaskInterval1.setEnabled(true);
			spinnerTaskInterval2.setEnabled(true);
			spinnerTaskInterval3.setEnabled(true);

			rbuttondate1.setSelected(false);
		}
		if (!rbuttondate2.isSelected()) {
			rbuttondate2.setSelected(true);
		}
	}

	/**
	 * CV10.2 workflow report enhancement changes. Added by Vanitha.S
	 * 
	 * @param event
	 */
	private void outputFormat_actionPerformed(ActionEvent event) {
		Object object = event.getSource();
		if (object == rButtonPdf) {
			rButtonHtml.setSelected(false);
			rButtonXls.setSelected(false);
			rButtonCsv.setSelected(false);
		} else if (object == rButtonHtml) {
			rButtonXls.setSelected(false);
			rButtonCsv.setSelected(false);
			rButtonPdf.setSelected(false);
		} else if (object == rButtonXls) {
			rButtonHtml.setSelected(false);
			rButtonCsv.setSelected(false);
			rButtonPdf.setSelected(false);
		} else if (object == rButtonCsv) {
			rButtonHtml.setSelected(false);
			rButtonXls.setSelected(false);
			rButtonPdf.setSelected(false);
		}
	}

	class SymItem implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent event) {
			try {
				Object object = event.getSource();
				int state = event.getStateChange();
				if (state == ItemEvent.SELECTED) {
					if (object == cmbAvailebReportsList) {
						cmbAvailebReportsList_actionPerformed(event);
					} else if (object == cmbModule) {
						cmbModule_actionPerformed(event);
					} else if (object == cmbName) {
						cmbName_actionPerformed(event);
					}
				}
			} catch (Exception e) {
				AdminWise.printToConsole("Exception in item state changed :: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	class SymWindow extends java.awt.event.WindowAdapter {
		public void windowClosing(java.awt.event.WindowEvent event) {
			try {
				PanelRouteSetting.repaint();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.toString());
			}
		}
	}

	class SymComponent extends java.awt.event.ComponentAdapter {
		public void componentResized(java.awt.event.ComponentEvent event) {
			Object object = event.getSource();
			if (object == VWNotificationSetting)
				VWNotification_componentResized(event);
		}
	}

	void VWNotification_componentResized(java.awt.event.ComponentEvent event) {
		if (event.getSource() == VWNotificationSetting) {
			Dimension mainDimension = this.getSize();
			Dimension panelDimension = mainDimension;
			Dimension commandDimension = PanelCommand.getSize();
			commandDimension.height = mainDimension.height;
			panelDimension.width = mainDimension.width - commandDimension.width;
			PanelTable.setSize(panelDimension);
			PanelCommand.setSize(commandDimension);
			SPTable.setSize(panelDimension);
			SPTable.revalidate();
			PanelTable.doLayout();
		}
	}

	private String getMonthDateDifference(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		int increment = 0;
		int year, month, day;

		if (fromDate.get(Calendar.DAY_OF_MONTH) > toDate.get(Calendar.DAY_OF_MONTH)) {
			increment = fromDate.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		// DAY CALCULATION
		if (increment != 0) {
			day = (toDate.get(Calendar.DAY_OF_MONTH) + increment) - fromDate.get(Calendar.DAY_OF_MONTH);
			increment = 1;
		} else {
			day = toDate.get(Calendar.DAY_OF_MONTH) - fromDate.get(Calendar.DAY_OF_MONTH);
		}

		// MONTH CALCULATION
		/*
		 * if ((fromDate.get(Calendar.MONTH) + increment) > toDate.get(Calendar.MONTH))
		 * { month = (toDate.get(Calendar.MONTH) + 12) - (fromDate.get(Calendar.MONTH) +
		 * increment); increment = 1; } else { month = (toDate.get(Calendar.MONTH)) -
		 * (fromDate.get(Calendar.MONTH) + increment); increment = 0; }
		 */

		int m1 = from.getYear() * 12 + from.getMonth();
		int m2 = to.getYear() * 12 + to.getMonth();
		int months = m2 - m1;

		// YEAR CALCULATION
		year = toDate.get(Calendar.YEAR) - (fromDate.get(Calendar.YEAR) + increment);
		return String.valueOf(months) + Util.SepChar + day;
	}

	void BtnProgressClick_actionPerformed(ActionEvent event) {
		try {
			AdminWise.printToConsole("Inside progress message......");
			lock1.lock();
			statusLabel.paintImmediately(this.getBounds());
			statusLabel.setVisible(true);
			statusLabel.setLayout(new BorderLayout());
			String fontName = "Arial";
			if (VWCPreferences.getFontName().trim().length() > 0)
				fontName = VWCPreferences.getFontName();
			statusLabel.setFont(new Font(fontName, Font.PLAIN, 14));
			statusLabel.setForeground(Color.black);
			statusLabel.validate();
			/*
			 * if(scroll.isVisible()==true) processDialog.setModal(true); else
			 * processDialog.setModal(false);
			 */
			processDialog.getContentPane().add(statusLabel, BorderLayout.CENTER);
			processDialog.setLocationRelativeTo(null);
			processDialog.setTitle("Workflow Report");
			processDialog.setAlwaysOnTop(true);
			processDialog.setSize(390, 70);
			processDialog.setResizable(false);
			processDialog.setDefaultCloseOperation(processDialog.DO_NOTHING_ON_CLOSE);
			processDialog.setVisible(true);
			processDialog.setOpacity(1);
			// processDialog.pack();
			lock1.unlock();
			// processDialog.repaint();

		} catch (Exception e) {
			AdminWise.printToConsole("Exception while reviewUpdate action::::" + e.getMessage());
		}
	}

	/**
	 * CV10.2 workflow report enhancement changes. Added by Vanitha.S
	 * 
	 * @param path
	 * @param reportName
	 * @param folderPath
	 * @param documentNameinDTC
	 * @param isReportGenerated
	 */
	public void generateDocumentForDTC(String path, String reportName, String folderPath, String documentNameinDTC,
			boolean isReportGenerated) {
		if (isReportGenerated) {
			AdminWise.printToConsole("generateDocumentForDTC reportType :" + path);
			AdminWise.printToConsole("generateDocumentForDTC reportName :" + reportName);
			AdminWise.printToConsole("generateDocumentForDTC folderPath :" + folderPath);
			AdminWise.printToConsole("generateDocumentForDTC documentNameinDTC :" + documentNameinDTC);
			Vector docTypeInfo = new Vector();
			try {
				/**
				 * CV2018 Enhancement: Check if Document Type exists
				 * 
				 * @author madhavan.b
				 */
				AdminWise.printToConsole("session >>>>>>" + session);
				DocType dt = new DocType("CVReports");
				dt.setName("CVReports");
				/**
				 * isDocTypeFound method is calling for checking whether "CVReports" doctype
				 * exists or not
				 */
				AdminWise.printToConsole("session.sessionId ::::" + session.sessionId);
				AdminWise.printToConsole("Before calling doctype....");
				docTypeId = AdminWise.gConnector.ViewWiseClient.isDocTypeFound(session.sessionId, dt);
				AdminWise.printToConsole("docTypeId in AdminWise.VWAddCustomIndices 5:::: " + docTypeId);
				if (docTypeId > 0) {
					AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(session.sessionId, docTypeId, docTypeInfo);
					AdminWise.printToConsole("docTypeInfo in AdminWise.VWAddCustomIndices 6::::" + docTypeInfo);

					if (docTypeInfo.size() > 0 && docTypeInfo != null) {
						String[] st = (docTypeInfo.get(0).toString()).split("\t");
						AdminWise.printToConsole("st 0:" + st[0]);
						AdminWise.printToConsole("st 1:" + st[1]);
						AdminWise.printToConsole("st 2:" + st[2]);
						String indexName = st[2];
						Index idxName = new Index(indexName);
						idxName.setName(indexName);
						int indexId = AdminWise.gConnector.ViewWiseClient.getIndexId(session.sessionId, idxName);
						AdminWise.printToConsole("indexId in AdminWise.VWAddCustomIndices 7::::" + indexId);
						/**
						 * CV2018 Enhancement: Check if Index with ReportName name exists
						 * 
						 * @author madhavan.b
						 */
						// File dtcFilePath = null;
						if (indexId > 0 && indexName.equals("ReportName")) {

							AdminWise.printToConsole("reportName before calling cvCreateNodePath:::: " + reportName);
							int docid = AdminWise.gConnector.ViewWiseClient.cvCreateNodePath(session.sessionId,
									"Report Panel", path, reportName, folderPath, documentNameinDTC);
							AdminWise
									.printToConsole("docCreated Successfully and returned to Adminwise 9:::: " + docid);
							if (docid > 0) {
								AdminWise.printToConsole("docid is greater than zero...." + docid);
								Document doc = new Document(docid);
								Vector docInfo = new Vector();
								AdminWise.gConnector.ViewWiseClient.getDocumentInfo(session.sessionId, doc, docInfo);
								AdminWise.printToConsole("docInfo ::::::" + docInfo);
								Document document = (Document) docInfo.get(0);
								AdminWise.printToConsole("document.getName():::::::" + document.getName());
								String documentName = document.getName();
								// if(documentName.length()>0) {
								AdminWise.printToConsole("documentName:::" + documentName);
								DefaultTableModel table_model1 = null;
								if (routeReportTable2 != null)
									table_model1 = (DefaultTableModel) routeReportTable2.getModel();
								if (table_model1 == null)
									table_model1 = new DefaultTableModel();
								table_model1.setRowCount(0);
								if (table_model1.getColumnCount() == 0) {
									String colDocumentName = "DocumentName";
									table_model1.addColumn(colDocumentName);
									table_model1.addColumn("Doc ID");
								}
								AdminWise.printToConsole("before adding data into the table docid :" + docid);
								table_model1.addRow(new Object[] { documentName, String.valueOf(docid) });
								if (routeReportTable2 == null) {
									AdminWise.printToConsole("table_model1 ::::" + table_model1);
									routeReportTable2 = new JTable(table_model1) {
										// Method added to stop route report table cell editing Date:-03/05/2016
										public boolean isCellEditable(int row, int column) {
											return false;
										}
									};
									routeReportTable2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
								}
								if (scroll == null)
									scroll = new JScrollPane();
								AdminWise.printToConsole("staticRepOutputPanelLeft :" + staticRepOutputPanelLeft);
								AdminWise.printToConsole("staticRepOutputPanelTop :" + staticRepOutputPanelTop);
								if (staticRepOutputPanelLeft != 0 && staticRepOutputPanelTop != 0)
									scroll.setBounds(staticRepOutputPanelLeft, staticRepOutputPanelTop, 450, 100);
								else
									scroll.setBounds(45, 660, 450, 80);
								scroll.getViewport().add(routeReportTable2);
								scroll.setEnabled(true);
								AdminWise.printToConsole("Before setting the visibility of scrollpane");
								scroll.setVisible(true);
								routeReportTable2.getTableHeader().setReorderingAllowed(false);
								// To adjust the column width
								TableColumnModel columnModel = routeReportTable2.getColumnModel();
								columnModel.getColumn(0).setMinWidth(350);
								columnModel.getColumn(0).setMaxWidth(350);
								routeReportTable2.setRowHeight(23);
								PanelRouteSetting.add(scroll);
								tablePanel.setVisible(true);	
								mouseEventCount = 0;
								routeReportTable2.addMouseListener(new MouseAdapter() {
									@Override
									public void mouseClicked (MouseEvent e)  {
										AdminWise.printToConsole("inside mouseClicked....."+e.getClickCount());
									}
									@Override
									public void mousePressed(MouseEvent event) {
										AdminWise.printToConsole("inside mousePressed....."+mouseEventCount);
										mouseEventCount = 0;
									}
									@Override
									public void mouseReleased(MouseEvent event) {
										AdminWise.printToConsole("inside mouseReleased....."+mouseEventCount);
										if (mouseEventCount == 0) {
											mouseEventCount = 1;
											Object object = event.getSource();
											if (object instanceof JTable) {
												if (event.getModifiers() == event.BUTTON1_MASK) {
													setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
													AdminWise.printToConsole("inside mouse release event.....");
													VWReportTable2_LeftMouseClicked(event);
												}
											} else {
												setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
											}
										}
									}
								});
							}
						}
					}
				}
			} catch (Exception e) {
				AdminWise.printToConsole("Exception in generateDocumentForDTC method :" + e.getMessage());
			}
			showProcessMessageDialog("Report generated");
		}
		clearMessageDialog();
	}

	private void deleteFolderLocation(String folderPath) {
		AdminWise.printToConsole("folderPath before deleting files in folder " + folderPath);
		File folderFile = new File(folderPath);
		if (folderFile.exists()) {
			File list[] = folderFile.listFiles();
			if (list != null && list.length > 0) {
				for (int index = 0; index < list.length; index++) {
					list[index].delete();
				}
			}
		}
		AdminWise.printToConsole("After deleting files in folder ");
	}

	public void BtnGenerateCVReport_actionPerformed(ActionEvent event) {
		if (scroll != null)
			scroll.setVisible(false);
		String htmlReportName = "";
		String folderPath = "";
		String path = "";
		String homePath = System.getenv("APPDATA");// loadSaveDialog(parent);
		// String homePath = VWUtil.getHome();
		String documentNameinDTC = "WFReport_" + System.currentTimeMillis();
		htmlReportName = documentNameinDTC + ".html";
		Vector docTypeInfo = new Vector();
		folderPath = homePath + "\\CV Reports" + "\\";
		AdminWise.printToConsole("homePath saveArrayInFileForRdAndAt 2 ::::" + homePath);
		path = folderPath + htmlReportName;
		AdminWise.printToConsole("path for Recycled Documents 3::::	" + path);
		AdminWise.printToConsole("folderPath for Recycled Documents 4::::	" + folderPath);
		/**
		 * CV2018 Enhancement: Check if Document Type exists
		 * 
		 * @author madhavan.b
		 */
		DocType dt = new DocType("CVReports");
		dt.setName("CVReports");
		/**
		 * isDocTypeFound method is calling for checking whether "CVReports" doctype
		 * exists or not
		 */
		AdminWise.printToConsole("session.sessionId ::::" + session.sessionId);
		docTypeId = AdminWise.gConnector.ViewWiseClient.isDocTypeFound(session.sessionId, dt);
		AdminWise.printToConsole("docTypeId in AdminWise.VWAddCustomIndices 5:::: " + docTypeId);
		if (docTypeId > 0) {
			AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(session.sessionId, docTypeId, docTypeInfo);
			AdminWise.printToConsole("docTypeInfo in AdminWise.VWAddCustomIndices 6::::" + docTypeInfo);

			if (docTypeInfo.size() > 0 && docTypeInfo != null) {
				StringTokenizer st = new StringTokenizer(docTypeInfo.get(0).toString(), "\t");
				int newDocypeId = Integer.parseInt(st.nextToken());
				String docTypeName = st.nextToken();
				String indexName = st.nextToken();
				Index idxName = new Index(indexName);
				idxName.setName(indexName);
				int indexId = AdminWise.gConnector.ViewWiseClient.getIndexId(session.sessionId, idxName);
				AdminWise.printToConsole("indexId in AdminWise.VWAddCustomIndices 7::::" + indexId);
				/**
				 * CV2018 Enhancement: Check if Index with ReportName name exists
				 * 
				 * @author madhavan.b
				 */
				if (indexId > 0 && indexName.equals("ReportName")) {
					boolean isWorkflowReportGenerated = generateReportAction();
					if (isWorkflowReportGenerated) {
						Vector finalRes1 = new Vector();
						if (reportResult != null && reportResult.size() > 0) {
							for (int i = 0; i < reportResult.size(); i++) {
								StringTokenizer st1 = new StringTokenizer(reportResult.get(i).toString(), "\t");
								st1.nextToken();// Added to skip the routeMasterInfo from DB
								String documentName = st1.nextToken();
								String workFlowName = st1.nextToken();
								finalRes1.add(reportResult.get(i));
							}
							AdminWise.printToConsole("finalResult10::::" + finalRes1);
							generateHtmlReport(finalRes1, path);

						}
						// if(VWSaveAsHtml.saveArrayInFileForRd(TableGlobal.getReportData(),globalDataList,newCustomIndexList,RecycleColumnNames,RecycleColumnWidths,
						// path,(java.awt.Component)this,LBL_RECYCLEDDOC_NAME, true)){
						AdminWise.printToConsole("path :::" + path);
						AdminWise.printToConsole("htmlReportName::::" + htmlReportName);
						AdminWise.printToConsole("folderPath :::" + folderPath);
						AdminWise.printToConsole("documentNameinDTC :::" + documentNameinDTC);
						int docid = AdminWise.gConnector.ViewWiseClient.cvCreateNodePath(session.sessionId,
								"Report Panel", path, htmlReportName, folderPath, documentNameinDTC);
						AdminWise.printToConsole("docCreated Successfully and returned to Adminwise 9:::: " + docid);
						if (docid > 0) {
							AdminWise.printToConsole("docid is greater than zero...." + docid);
							Document doc = new Document(docid);
							Vector docInfo = new Vector();
							AdminWise.gConnector.ViewWiseClient.getDocumentInfo(session.sessionId, doc, docInfo);
							AdminWise.printToConsole("docInfo ::::::" + docInfo);
							Document document = (Document) docInfo.get(0);
							AdminWise.printToConsole("document.getName():::::::" + document.getName());
							String documentName = document.getName();
							// if(documentName.length()>0) {
							AdminWise.printToConsole("documentName:::" + documentName);
							DefaultTableModel table_model1 = null;
							if (routeReportTable2 != null)
								table_model1 = (DefaultTableModel) routeReportTable2.getModel();
							if (table_model1 == null)
								table_model1 = new DefaultTableModel();
							table_model1.setRowCount(0);
							if (table_model1.getColumnCount() == 0) {
								String colDocumentName = "DocumentName";
								// String
								// colCount=AdminWise.connectorManager.getString("RoutReportTable.Count");
								table_model1.addColumn(colDocumentName);
								table_model1.addColumn("Doc ID");
							}
							AdminWise.printToConsole("before adding data into the table docid :" + docid);
							table_model1.addRow(new Object[] { documentName, String.valueOf(docid) });
							if (routeReportTable2 == null) {
								AdminWise.printToConsole("table_model1 ::::" + table_model1);
								routeReportTable2 = new JTable(table_model1) {
									// Method added to stop route report table cell editing Date:-03/05/2016
									public boolean isCellEditable(int row, int column) {
										return false;
									}
								};
								routeReportTable2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
							}
							if (scroll == null)
								scroll = new JScrollPane();
							scroll.setBounds(45, 620, 450, 80);
							scroll.getViewport().add(routeReportTable2);
							scroll.setEnabled(true);
							scroll.setVisible(true);
							routeReportTable2.getTableHeader().setReorderingAllowed(false);
							// To adjust the column width
							TableColumnModel columnModel = routeReportTable2.getColumnModel();
							columnModel.getColumn(0).setMinWidth(485 - 185);
							columnModel.getColumn(0).setMaxWidth(485 - 185);
							// columnModel.getColumn(0).setPreferredWidth(180);
							routeReportTable2.setRowHeight(23);
							PanelRouteSetting.add(scroll);
							tablePanel.setVisible(true);
							// }

							// }

							routeReportTable2.addMouseListener(new MouseAdapter() {
								@Override
								public void mouseReleased(MouseEvent event) {
									Object object = event.getSource();
									if (object instanceof JTable) {
										if (event.getModifiers() == event.BUTTON1_MASK) {
											setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
											VWReportTable2_LeftMouseClicked(event);
										}

										/*
										 * else if(event.getModifiers()==event.BUTTON3_MASK){ //
										 * VWReportTable_RightMouseClicked(event); }
										 */
									} else {
										setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
									}
								}
							});

							AdminWise.printToConsole("folderPath before deleting files in folder " + folderPath);
							File folderFile = new File(folderPath);
							if (folderFile.exists()) {
								File list[] = folderFile.listFiles();
								if (list != null && list.length > 0) {
									for (int index = 0; index < list.length; index++) {
										list[index].delete();
									}
								}
							}
							AdminWise.printToConsole("After deleting files in folder ");

						}
					}
				}
				Vector finalRes = new Vector();

				// int Selectrow=routeReportTable1.getSelectedRow();
				// String routeName=routeReportTable1.getModel().getValueAt(Selectrow,
				// 0).toString();
				// int count=Integer.parseInt(routeReportTable1.getModel().getValueAt(Selectrow,
				// 1).toString());
				// AdminWise.printToConsole("routeName::::"+routeName);
				// AdminWise.printToConsole("count::::"+count);

			}

		}
	}

	public void VWReportTable2_LeftMouseClicked(java.awt.event.MouseEvent event) {
		AdminWise.printToConsole("VWReportTable2_LeftMouseClicked...... ");
		Point origin = event.getPoint();
		int row = routeReportTable2.rowAtPoint(origin);
		int column = routeReportTable2.columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if (event.getClickCount() == 1)
			ldoubleClick(event, row, column);
	}

	public boolean generateReportAction() {
		AdminWise.printToConsole("generateReportAction:: ");
		// TODO Auto-generated method stub

		String actionListSelectValue = "";
		String month = "";
		String day = "";
		String hour = "";
		String totalDate = "";

		java.util.List actionWorkFlowList = VWRouteReportSettingsPanel.workFlowActionList.getModel().getCheckeds();
		if ((actionWorkFlowList != null) && actionWorkFlowList.size() > 0) {
			if (VWRouteReportSettingsPanel.workFlowActionList.isEnabled()) {
				AdminWise.printToConsole("actionWorkFlowList:::::" + actionWorkFlowList);
				if (actionWorkFlowList.contains("New") || actionWorkFlowList.contains("Started")) {
					actionListSelectValue = actionListSelectValue + ";" + "1";
				}
				if (actionWorkFlowList.contains("Pending")) {
					actionListSelectValue = actionListSelectValue + ";" + "2";
				}
				if (actionWorkFlowList.contains("Completed")) {
					actionListSelectValue = actionListSelectValue + ";" + "3";
				}
				if (actionWorkFlowList.contains("To be reviewed")) {
					actionListSelectValue = actionListSelectValue + ";" + "4";
				}
			}
			if (actionListSelectValue.length() > 0)
				actionListSelectValue = actionListSelectValue.substring(1, actionListSelectValue.length());
		} else if (VWRouteReportSettingsPanel.workFlowTaskList.isEnabled()) {
			java.util.List actionTaskList = VWRouteReportSettingsPanel.workFlowTaskList.getModel().getCheckeds();
			if (actionTaskList != null && actionTaskList.size() > 0) {
				for (int i = 0; i < actionTaskList.size(); i++) {
					actionListSelectValue = actionListSelectValue + ";" + actionTaskList.get(i);
				}

				if (actionListSelectValue.length() > 0)
					actionListSelectValue = actionListSelectValue.substring(1, actionListSelectValue.length());
				AdminWise.printToConsole("the actionListSelectValue::::" + actionListSelectValue);
			}
		}
		java.util.List availableWorkFlowList = VWRouteReportSettingsPanel.availableWorkFlowList.getModel()
				.getCheckeds();
		String availableWorkFlowValues = "";
		if (availableWorkFlowList != null && availableWorkFlowList.size() > 0) {
			if (availableWorkFlowList.contains("Select All")) {
				availableWorkFlowValues = availableWorkFlowValues + "0";
				AdminWise.printToConsole("availableWorkFlowValues inside if::::::::" + availableWorkFlowValues);
			} else {
				for (int i = 0; i < availableWorkFlowList.size(); i++) {

					availableWorkFlowValues = availableWorkFlowValues + ";"
							+ VWRouteReportSettingsPanel.availableWorkFlowMap
									.get(availableWorkFlowList.get(i).toString());

				}

				availableWorkFlowValues = availableWorkFlowValues.substring(1, availableWorkFlowValues.length());
				AdminWise.printToConsole("availableWorkFlowValues inside else::::::::" + availableWorkFlowValues);
			}

		}

		java.util.List availableTaskList = VWRouteReportSettingsPanel.availableTaskList.getModel().getCheckeds();
		String availableTaskValues = "";
		if (availableTaskList != null && availableTaskList.size() > 0) {
			if (availableTaskList != null && availableTaskList.size() > 0) {
				if (availableTaskList.contains("Select All")) {
					availableTaskValues = availableTaskValues + "0";
				} else {
					for (int i = 0; i < availableTaskList.size(); i++) {
						if (availableTaskList.get(i).equals("Select All")) {
							availableTaskValues = "0";
						} else
							availableTaskValues = availableTaskValues + ";" + availableTaskList.get(i).toString();
					}
					availableTaskValues = availableTaskValues.substring(1, availableTaskValues.length());
				}

			}

		}
		String availableUserGroupValues = "";

		java.util.List userGroupList = VWRouteReportSettingsPanel.userGroupList.getModel().getCheckeds();
		if (userGroupList != null && userGroupList.size() > 0) {

			if (userGroupList.contains("Select All")) {
				availableUserGroupValues = availableUserGroupValues + "Null";
			} else {
				for (int i = 0; i < userGroupList.size(); i++) {
					availableUserGroupValues = availableUserGroupValues + ";" + userGroupList.get(i).toString();
				}
				availableUserGroupValues = availableUserGroupValues.substring(1, availableUserGroupValues.length());
			}
		}

		AdminWise.printToConsole("availableUserGroupValues:::::::::::::" + availableUserGroupValues);
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();

		if (availableWorkFlowList.size() == 1 && availableWorkFlowList.get(0).equals("Select All")) {
			VWMessage.showMessage(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFEmptyAction"));
			return false;
		}

		if (actionListSelectValue.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFAction"));
			return false;
		}
		if (availableWorkFlowValues.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFNotSelected"));
			return false;
		}
		if (availableTaskValues.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFTaskNotSelected"));
			return false;
		}
		if (availableUserGroupValues.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_UserNotSelected"));
			return false;
		}
		if (rbuttondate2.isSelected() == true) {
			month = spinnerTaskInterval1.getValue().toString();
			day = spinnerTaskInterval2.getValue().toString();
			hour = spinnerTaskInterval3.getValue().toString();

			if ((Integer.parseInt(month) == 0) && (Integer.parseInt(day) == 0) && (Integer.parseInt(hour) == 0)) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
				return false;
			}
			totalDate = "2" + Util.SepChar + month + Util.SepChar + day + Util.SepChar + hour;
		} else if (rbuttondate1.isSelected() == true) {
			String dateFrom = TxtMFrom.getSelectedItem().toString();
			dateFrom = dateFrom + " " + simpDate.format(timeSpinnerFrom.getValue());

			String dateTo = TxtMTo.getSelectedItem().toString();
			dateTo = dateTo + " " + simpDate.format(timeSpinnerTo.getValue());
			if (dateTo.equals("") || dateFrom.equals("") || dateTo.length() == 0 || dateFrom.length() == 0) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
				return false;
			}
			if ((TxtMFrom.getSelectedItem().toString() == "") || (TxtMTo.getSelectedItem().toString() == "")) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
				return false;
			} else {
				String pattern = "MM/dd/yyyy";
				totalDate = "1" + Util.SepChar + dateFrom + Util.SepChar + dateTo + Util.SepChar + "0";
				SimpleDateFormat format = new SimpleDateFormat(pattern);
				try {
					Date date1 = format.parse(dateFrom);
					Date date2 = format.parse(dateTo);
					totalDate = "1" + Util.SepChar + getMonthDateDifference(date1, date2) + Util.SepChar + dateTo;
					StringTokenizer st = new StringTokenizer(getMonthDateDifference(date1, date2), "\t");
					String month1 = st.nextToken();
					String day1 = st.nextToken();
					if ((Integer.parseInt(month1) == 0) && (Integer.parseInt(day1) == 0)) {
						JOptionPane.showMessageDialog(AdminWise.adminFrame,
								AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
						return false;
					}
					AdminWise.printToConsole("totalDate::::" + totalDate);
				} catch (Exception e) {

				}
			}
		}

		lock1.lock();
		BtnProgressClick.doClick();
		try {

			BtnProgressClick.doClick();
			// Thread.sleep(5000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reportResult.clear();
		Vector agregateResult = new Vector();
		if (VWRouteReportSettingsPanel.workFlowActionList.isEnabled()) {
			String tasks = "New;Pending;Review";
			AdminWise.gConnector.ViewWiseClient.generateWFReport(room.getId(), 1, actionListSelectValue, tasks,
					availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate, reportResult);
			if (reportResult == null || reportResult.size() == 0) {
				processDialog.setVisible(false);
				processDialog.dispose();
				JOptionPane.showMessageDialog(this,
						AdminWise.connectorManager.getString("RouteReport.MSG_WFNotInSerach"));
				return false;
			} else {
				AdminWise.gConnector.ViewWiseClient.getAgregateReport(room.getId(), 1, actionListSelectValue, tasks,
						availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate,
						agregateResult);
				AdminWise.printToConsole("reportResult value:::" + reportResult);
			}
		} else if (VWRouteReportSettingsPanel.workFlowTaskList.isEnabled()) {
			String task = "1;2;3;4";
			AdminWise.gConnector.ViewWiseClient.generateWFReport(room.getId(), 2, task, actionListSelectValue,
					availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate, reportResult);
			AdminWise.printToConsole("else if condition and reportResult.size()" + reportResult.size());
			if (reportResult == null || reportResult.size() == 0) {
				processDialog.setVisible(false);
				processDialog.dispose();
				JOptionPane.showMessageDialog(this,
						AdminWise.connectorManager.getString("RouteReport.MSG_WFNotInSerach"));
				return false;
			} else {
				AdminWise.gConnector.ViewWiseClient.getAgregateReport(room.getId(), 2, task, actionListSelectValue,
						availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate,
						agregateResult);
			}
		}
		// AdminWise.printToConsole("agregate result::::"+agregateResult);

		Vector tableData = new Vector();
		if (reportResult == null || reportResult.size() == 0 || agregateResult == null || agregateResult.size() == 0) {
			scroll.setVisible(false);
			processDialog.setVisible(false);
			processDialog.dispose();
			JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("RouteReport.MSG_WFNotInSerach"));
			return false;
		}
		if (agregateResult != null && agregateResult.size() > 0) {
			DefaultTableModel table_model = null;
			if (routeReportTable1 != null)
				table_model = (DefaultTableModel) routeReportTable1.getModel();
			if (table_model == null)
				table_model = new DefaultTableModel();
			table_model.setRowCount(0);
			if (table_model.getColumnCount() == 0) {
				String colWorkFlowName = AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME") + " "
						+ AdminWise.connectorManager.getString("RoutReportTable.Name");
				String colCount = AdminWise.connectorManager.getString("RoutReportTable.Count");
				table_model.addColumn(colWorkFlowName);
				table_model.addColumn(colCount);
			}
			// AdminWise.printToConsole("Table Data Vector::"+tableData);
			for (int i = 0; i < agregateResult.size(); i++) {
				StringTokenizer st = new StringTokenizer(agregateResult.get(i).toString(), "\t");
				String workFlowName = st.nextToken();
				String count = st.nextToken();
				// tableData.addElement(workFlowName);
				// tableData.addElement(count);

				// table_model.addRow( new Object[] {workFlowName, count });
			}

			// table_model.get
			/*
			 * table_model.fireTableDataChanged(); if(routeReportTable1==null) {
			 * routeReportTable1 = new JTable(table_model){ //Method added to stop route
			 * report table cell editing Date:-03/05/2016 public boolean isCellEditable(int
			 * row,int column){ return false;} };
			 * 
			 * routeReportTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			 * 
			 * }
			 */
			/*
			 * routeReportTable1.addMouseListener(new MouseAdapter() {
			 * 
			 * @Override public void mouseReleased(MouseEvent event) { Object object =
			 * event.getSource(); if (object instanceof JTable){
			 * if(event.getModifiers()==event.BUTTON1_MASK){
			 * setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			 * VWReportTable_LeftMouseClicked(event); }
			 * 
			 * else if(event.getModifiers()==event.BUTTON3_MASK){ //
			 * VWReportTable_RightMouseClicked(event); } }else{
			 * setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); } } });
			 */

			/*
			 * if(scroll==null) scroll=new JScrollPane(); scroll.setBounds(386, 320, 485,
			 * 290); //(230,300,245,100) scroll.getViewport().add(routeReportTable1);
			 * scroll.setEnabled(true); scroll.setVisible(true);
			 * routeReportTable1.getTableHeader().setReorderingAllowed(false); //To adjust
			 * the column width TableColumnModel columnModel =
			 * routeReportTable1.getColumnModel();
			 * columnModel.getColumn(1).setMinWidth(150);
			 * columnModel.getColumn(1).setMaxWidth(180);
			 * columnModel.getColumn(1).setPreferredWidth(180);
			 * routeReportTable1.setRowHeight(23); PanelRouteSetting.add(scroll);
			 * tablePanel.setVisible(true);
			 */
			processDialog.setVisible(false);
			processDialog.dispose();
			lock1.unlock();
		}
		return true;
	}

	public void BtnGenerateReport_actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub

		String actionListSelectValue = "";
		String month = "";
		String day = "";
		String hour = "";
		String totalDate = "";

		java.util.List actionWorkFlowList = VWRouteReportSettingsPanel.workFlowActionList.getModel().getCheckeds();
		if ((actionWorkFlowList != null) && actionWorkFlowList.size() > 0) {
			if (VWRouteReportSettingsPanel.workFlowActionList.isEnabled()) {

				AdminWise.printToConsole("actionWorkFlowList:::::" + actionWorkFlowList);

				if (actionWorkFlowList.contains("New") || actionWorkFlowList.contains("Started")) {
					actionListSelectValue = actionListSelectValue + ";" + "1";
				}
				if (actionWorkFlowList.contains("Pending")) {
					actionListSelectValue = actionListSelectValue + ";" + "2";
				}
				if (actionWorkFlowList.contains("Completed")) {
					actionListSelectValue = actionListSelectValue + ";" + "3";
				}
				if (actionWorkFlowList.contains("To be reviewed")) {
					actionListSelectValue = actionListSelectValue + ";" + "4";
				}
			}
			if (actionListSelectValue.length() > 0)
				actionListSelectValue = actionListSelectValue.substring(1, actionListSelectValue.length());
		} else if (VWRouteReportSettingsPanel.workFlowTaskList.isEnabled()) {
			java.util.List actionTaskList = VWRouteReportSettingsPanel.workFlowTaskList.getModel().getCheckeds();
			if (actionTaskList != null && actionTaskList.size() > 0) {
				for (int i = 0; i < actionTaskList.size(); i++) {
					actionListSelectValue = actionListSelectValue + ";" + actionTaskList.get(i);
				}

				if (actionListSelectValue.length() > 0)
					actionListSelectValue = actionListSelectValue.substring(1, actionListSelectValue.length());
				AdminWise.printToConsole("the actionListSelectValue::::" + actionListSelectValue);
			}
		}
		java.util.List availableWorkFlowList = VWRouteReportSettingsPanel.availableWorkFlowList.getModel()
				.getCheckeds();
		String availableWorkFlowValues = "";
		if (availableWorkFlowList != null && availableWorkFlowList.size() > 0) {
			if (availableWorkFlowList.contains("Select All")) {
				availableWorkFlowValues = availableWorkFlowValues + "0";
				AdminWise.printToConsole("availableWorkFlowValues inside if::::::::" + availableWorkFlowValues);
			} else {
				for (int i = 0; i < availableWorkFlowList.size(); i++) {

					availableWorkFlowValues = availableWorkFlowValues + ";"
							+ VWRouteReportSettingsPanel.availableWorkFlowMap
									.get(availableWorkFlowList.get(i).toString());

				}

				availableWorkFlowValues = availableWorkFlowValues.substring(1, availableWorkFlowValues.length());
				AdminWise.printToConsole("availableWorkFlowValues inside else::::::::" + availableWorkFlowValues);
			}

		}

		java.util.List availableTaskList = VWRouteReportSettingsPanel.availableTaskList.getModel().getCheckeds();
		String availableTaskValues = "";
		if (availableTaskList != null && availableTaskList.size() > 0) {
			if (availableTaskList != null && availableTaskList.size() > 0) {
				if (availableTaskList.contains("Select All")) {
					availableTaskValues = availableTaskValues + "0";
				} else {
					for (int i = 0; i < availableTaskList.size(); i++) {
						if (availableTaskList.get(i).equals("Select All")) {
							availableTaskValues = "0";
						} else
							availableTaskValues = availableTaskValues + ";" + availableTaskList.get(i).toString();
					}
					availableTaskValues = availableTaskValues.substring(1, availableTaskValues.length());
				}

			}

		}
		String availableUserGroupValues = "";

		java.util.List userGroupList = VWRouteReportSettingsPanel.userGroupList.getModel().getCheckeds();
		if (userGroupList != null && userGroupList.size() > 0) {

			if (userGroupList.contains("Select All")) {
				availableUserGroupValues = availableUserGroupValues + "Null";
			} else {
				for (int i = 0; i < userGroupList.size(); i++) {
					availableUserGroupValues = availableUserGroupValues + ";" + userGroupList.get(i).toString();
				}
				availableUserGroupValues = availableUserGroupValues.substring(1, availableUserGroupValues.length());
			}
		}

		AdminWise.printToConsole("availableUserGroupValues:::::::::::::" + availableUserGroupValues);
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();

		if (availableWorkFlowList.size() == 1 && availableWorkFlowList.get(0).equals("Select All")) {
			VWMessage.showMessage(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFEmptyAction"));
			return;
		}

		if (actionListSelectValue.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFAction"));
			return;
		}
		if (availableWorkFlowValues.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFNotSelected"));
			return;
		}
		if (availableTaskValues.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_WFTaskNotSelected"));
			return;
		}
		if (availableUserGroupValues.length() == 0) {
			JOptionPane.showMessageDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("RouteReport.MSG_UserNotSelected"));
			return;
		}
		if (rbuttondate2.isSelected() == true) {
			month = spinnerTaskInterval1.getValue().toString();
			day = spinnerTaskInterval2.getValue().toString();
			hour = spinnerTaskInterval3.getValue().toString();

			if ((Integer.parseInt(month) == 0) && (Integer.parseInt(day) == 0) && (Integer.parseInt(hour) == 0)) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
				return;
			}
			totalDate = "2" + Util.SepChar + month + Util.SepChar + day + Util.SepChar + hour;
		} else if (rbuttondate1.isSelected() == true) {
			String dateFrom = TxtMFrom.getSelectedItem().toString();
			String dateTo = TxtMTo.getSelectedItem().toString();

			if (dateTo.equals("") || dateFrom.equals("") || dateTo.length() == 0 || dateFrom.length() == 0) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
				return;
			}
			if ((TxtMFrom.getSelectedItem().toString() == "") || (TxtMTo.getSelectedItem().toString() == "")) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
				return;
			} else {
				String pattern = "MM/dd/yyyy";
				totalDate = "1" + Util.SepChar + dateFrom + Util.SepChar + dateTo + Util.SepChar + "0";
				SimpleDateFormat format = new SimpleDateFormat(pattern);
				try {
					Date date1 = format.parse(dateFrom);
					Date date2 = format.parse(dateTo);
					totalDate = "1" + Util.SepChar + getMonthDateDifference(date1, date2) + Util.SepChar + dateTo;
					StringTokenizer st = new StringTokenizer(getMonthDateDifference(date1, date2), "\t");
					String month1 = st.nextToken();
					String day1 = st.nextToken();
					if ((Integer.parseInt(month1) == 0) && (Integer.parseInt(day1) == 0)) {
						JOptionPane.showMessageDialog(AdminWise.adminFrame,
								AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
						return;
					}
					AdminWise.printToConsole("totalDate::::" + totalDate);
				} catch (Exception e) {

				}
			}
		}

		lock1.lock();
		BtnProgressClick.doClick();
		try {

			BtnProgressClick.doClick();
			// Thread.sleep(5000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reportResult.clear();
		Vector agregateResult = new Vector();
		if (VWRouteReportSettingsPanel.workFlowActionList.isEnabled()) {
			String tasks = "New;Pending;Review";
			AdminWise.gConnector.ViewWiseClient.generateWFReport(room.getId(), 1, actionListSelectValue, tasks,
					availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate, reportResult);
			AdminWise.printToConsole("reportResult ::::::::" + reportResult);

			if (reportResult == null || reportResult.size() == 0) {
				processDialog.setVisible(false);
				processDialog.dispose();
				JOptionPane.showMessageDialog(this,
						AdminWise.connectorManager.getString("RouteReport.MSG_WFNotInSerach"));
				return;
			} else {
				AdminWise.gConnector.ViewWiseClient.getAgregateReport(room.getId(), 1, actionListSelectValue, tasks,
						availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate,
						agregateResult);
				AdminWise.printToConsole("reportResult value:::" + reportResult);
			}
		} else if (VWRouteReportSettingsPanel.workFlowTaskList.isEnabled()) {
			String task = "1;2;3;4";
			AdminWise.gConnector.ViewWiseClient.generateWFReport(room.getId(), 2, task, actionListSelectValue,
					availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate, reportResult);
			AdminWise.printToConsole("else if condition and reportResult.size()" + reportResult.size());
			if (reportResult == null || reportResult.size() == 0) {
				processDialog.setVisible(false);
				processDialog.dispose();
				JOptionPane.showMessageDialog(this,
						AdminWise.connectorManager.getString("RouteReport.MSG_WFNotInSerach"));
				return;
			} else {
				AdminWise.gConnector.ViewWiseClient.getAgregateReport(room.getId(), 2, task, actionListSelectValue,
						availableWorkFlowValues, availableTaskValues, availableUserGroupValues, totalDate,
						agregateResult);
			}
		}
		// AdminWise.printToConsole("agregate result::::"+agregateResult);

		Vector tableData = new Vector();
		if (reportResult == null || reportResult.size() == 0 || agregateResult == null || agregateResult.size() == 0) {
			scroll.setVisible(false);
			processDialog.setVisible(false);
			processDialog.dispose();
			JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("RouteReport.MSG_WFNotInSerach"));
			return;
		}
		if (agregateResult != null && agregateResult.size() > 0) {
			DefaultTableModel table_model = null;
			if (routeReportTable1 != null)
				table_model = (DefaultTableModel) routeReportTable1.getModel();
			if (table_model == null)
				table_model = new DefaultTableModel();
			table_model.setRowCount(0);
			if (table_model.getColumnCount() == 0) {
				String colWorkFlowName = AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME") + " "
						+ AdminWise.connectorManager.getString("RoutReportTable.Name");
				String colCount = AdminWise.connectorManager.getString("RoutReportTable.Count");
				table_model.addColumn(colWorkFlowName);
				table_model.addColumn(colCount);
			}
			// AdminWise.printToConsole("Table Data Vector::"+tableData);
			for (int i = 0; i < agregateResult.size(); i++) {
				StringTokenizer st = new StringTokenizer(agregateResult.get(i).toString(), "\t");
				String workFlowName = st.nextToken();
				String count = st.nextToken();
				tableData.addElement(workFlowName);
				tableData.addElement(count);

				table_model.addRow(new Object[] { workFlowName, count });
			}

			// table_model.get
			table_model.fireTableDataChanged();
			if (routeReportTable1 == null) {
				routeReportTable1 = new JTable(table_model) {
					// Method added to stop route report table cell editing Date:-03/05/2016
					public boolean isCellEditable(int row, int column) {
						return false;
					}
				};

				routeReportTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			}
			routeReportTable1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent event) {
					Object object = event.getSource();
					if (object instanceof JTable) {
						if (event.getModifiers() == event.BUTTON1_MASK) {
							setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
							VWReportTable_LeftMouseClicked(event);
						}

						/*
						 * else if(event.getModifiers()==event.BUTTON3_MASK){ //
						 * VWReportTable_RightMouseClicked(event); }
						 */
					} else {
						setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					}
				}
			});

			if (scroll == null)
				scroll = new JScrollPane();
			scroll.setBounds(386, 320, 485, 290); // (230,300,245,100)
			scroll.getViewport().add(routeReportTable1);
			scroll.setEnabled(true);
			scroll.setVisible(true);
			routeReportTable1.getTableHeader().setReorderingAllowed(false);
			// To adjust the column width
			TableColumnModel columnModel = routeReportTable1.getColumnModel();
			columnModel.getColumn(1).setMinWidth(150);
			columnModel.getColumn(1).setMaxWidth(180);
			columnModel.getColumn(1).setPreferredWidth(180);
			routeReportTable1.setRowHeight(23);
			PanelRouteSetting.add(scroll);
			tablePanel.setVisible(true);
			processDialog.setVisible(false);
			processDialog.dispose();
			lock1.unlock();
		}
	}

	public void VWReportTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = routeReportTable1.rowAtPoint(origin);
		int column = routeReportTable1.columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if (event.getClickCount() == 1)
			lSingleClick(event, row, column);
	}

	private void ldoubleClick(java.awt.event.MouseEvent event, int row, int col) {

		if (routeReportTable2.getSelectedRow() == -1)
			return;
		String selectdocName = (String) routeReportTable2.getValueAt(0, 0);
		AdminWise.printToConsole("selectdocName ::::::::" + selectdocName);
		String selectdocId = (String) routeReportTable2.getValueAt(0, 1);
		AdminWise.printToConsole("selectdocId ::::::::" + selectdocId);
		loadDocumentInDTC(Integer.parseInt(selectdocId));
	}

	private void lSingleClick(java.awt.event.MouseEvent event, int row, int col) {

		if (routeReportTable1.getSelectedRow() == -1)
			return;
		JPopupMenu popupMenu = new JPopupMenu();

		popupMenu.add(menuGenerateReport);
		menuGenerateReport.setText("Save AS HTML Report");
		popupMenu.show(routeReportTable1, event.getX(), event.getY());
		Vector adminGroup = new Vector();
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();

	}

	public void menuGenerateReport_actionPerformed(java.awt.event.ActionEvent evt) {
		AdminWise.printToConsole("evt.getCommand::::" + evt.getActionCommand());
		if (evt.getActionCommand().equals("Save AS HTML Report")) {
			Vector finalRes = new Vector();

			int Selectrow = routeReportTable1.getSelectedRow();
			String routeName = routeReportTable1.getModel().getValueAt(Selectrow, 0).toString();
			int count = Integer.parseInt(routeReportTable1.getModel().getValueAt(Selectrow, 1).toString());
			AdminWise.printToConsole("routeName::::" + routeName);
			AdminWise.printToConsole("count::::" + count);
			finalRes.clear();
			if (reportResult != null && reportResult.size() > 0) {
				for (int i = 0; i < reportResult.size(); i++) {
					StringTokenizer st = new StringTokenizer(reportResult.get(i).toString(), "\t");
					st.nextToken();// Added to skip the routeMasterInfo from DB
					String documentName = st.nextToken();
					String workFlowName = st.nextToken();
					if (workFlowName.equalsIgnoreCase(routeName)) {
						finalRes.add(reportResult.get(i));
					}
				}
				AdminWise.printToConsole("finalResult::::" + finalRes);
				generateHtmlReport(finalRes, "");
			}
		}

	}

	// Location finder action event for RoutReport Cabinatewise
	/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
	void BtnNewFindLocationt_actionPerformed(java.awt.event.ActionEvent event) {
		AdminWise.printToConsole("inside BtnNewFindLocationt_actionPerformed :" + dlgLocation);
		try {
			if (dlgLocation != null) {
				dlgLocation.setCabinetSearch();
				dlgLocation.loadDlgData();
				AdminWise.printToConsole("nodeId :" + nodeId);
				AdminWise.printToConsole(
						"dlgLocation.tree.getSelectedNodeId() :" + dlgLocation.tree.getSelectedNodeId());
				if (nodeId != dlgLocation.tree.getSelectedNodeId())
					dlgLocation.tree.setSelectionPath(nodeId, true);
				dlgLocation.setVisible(true);
			} else {

				dlgLocation = new VWDlgLocation(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("FindPanel.dlgLocation"));
				dlgLocation.setCabinetSearch();
				dlgLocation.loadDlgData();
				dlgLocation.tree.setSelectionPath(nodeId, true);
				dlgLocation.setVisible(true);
			}
			BtnNewFindLocation.setFocusPainted(false);
			if (dlgLocation.cancelFlag)
				return;
			TxtFieldFindLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
			nodeId = dlgLocation.tree.getSelectedNodeId();
		} catch (Exception e) {
			AdminWise.printToConsole("error in BtnNewFindLocationt_actionPerformed" + e.getMessage());
		}
	}

	void BtnUpClick_actionPerformed(java.awt.event.ActionEvent event, String btnAction) {
		AdminWise.printToConsole("inside BtnUpClick_actionPerformed.....");
		try {
			List<String> selectedColumnList = columnsList.getSelectedItemIds();
			
			columnsList.setIndicesSort(btnAction);
			
			AdminWise.printToConsole("selectedColumnList :" + selectedColumnList);
			if (selectedColumnList != null && selectedColumnList.size() > 0) {
				AdminWise.printToConsole("selectedColumnList.size()  :" + selectedColumnList.size());
				String checkedItems[] = new String[selectedColumnList.size()];
				checkedItems = selectedColumnList.toArray(checkedItems);
				AdminWise.printToConsole("checkedItems :" + checkedItems);
				columnsList.setCheckItems(checkedItems);
			}

		} catch (Exception e) {
			AdminWise.printToConsole("error in BtnUpClick_actionPerformed" + e.getMessage());
		}
	}

	/**
	 * Enhancement :- Enable save as html report in workflow CV10 Date:-11-02-2016
	 */
	public void generateHtmlReport(Vector finalResult, String path) {

		AdminWise.printToConsole("::::::finalResult::::" + finalResult);
		int rowCount = finalResult.size();
		Vector htmlContents = new Vector();
		String err = "";
		String title = "";
		title = AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME") + " "
				+ AdminWise.connectorManager.getString("RoutePanel.Report");
		htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContents.add("<html>");
		htmlContents.add("<head>");
		htmlContents.add("<title>" + title + "</title>");
		htmlContents.add("<style>");
		htmlContents.add("TH {");
		htmlContents.add(
				"align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#D3D3D3'; font-weight:bold");
		htmlContents.add("}");
		htmlContents.add("TD {");
		htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
		htmlContents.add("}");
		htmlContents.add("</style>");
		htmlContents.add("</head>");
		htmlContents.add("<body BGCOLOR='#F7F7E7'>");
		htmlContents.add(
				"<table border=1 bordercolor='#D3D3D3' width=100% cellspacing=0 cellpadding=0 ><tr><td align=center><font size='4'><b>"
						+ title + "</b></font></td></tr></table>");
		htmlContents.add("<p></p>");
		htmlContents.add("<table border = 1   bordercolor='#D3D3D3' width=100% cellspacing=0 cellpadding=0>");
		htmlContents.add(
				"<tr><td  BGCOLOR=#cccc99>Document Name </td><td  BGCOLOR=#cccc99>Workflow Name </td><td  BGCOLOR=#cccc99>User Name</td>"
						+ "<td BGCOLOR=#cccc99>Processed Date</td><td BGCOLOR=#cccc99>Status</td><td  BGCOLOR=#cccc99>Task</td>"
						+ "<td BGCOLOR=#cccc99>Task Description</td><td  BGCOLOR=#cccc99>User Status</td></tr>");
		AdminWise.printToConsole("finalResult count ::: " + finalResult.size());
		for (int curRouteInfo = 0; curRouteInfo < finalResult.size(); curRouteInfo++) {
			try {
				StringTokenizer st = new StringTokenizer(finalResult.get(curRouteInfo).toString(), "\t");
				AdminWise.printToConsole("StringTokenizer count ::: " + st.countTokens());
				st.nextToken();// Added to skip the routeMasterInfo from DB
				htmlContents.add("<tr><td >" + st.nextToken() + "</td><td >" + st.nextToken() + "</td><td >"
						+ st.nextToken() + "</td>" + "<td>&nbsp;" + st.nextToken() + "</td><td >" + st.nextToken()
						+ "</td>" + "<td >&nbsp;" + st.nextToken() + "</td><td >" + st.nextToken() + "</td><td >&nbsp;"
						+ st.nextToken() + "</td></tr>");

				htmlContents.add("<p></p>");
			} catch (Exception ex) {
				err = "Error generating report - " + ex.getMessage();
				AdminWise.printToConsole("Error in generating report::" + ex.getMessage());

			}
		}
		htmlContents.add("</table>");

		htmlContents.add("<table><tr><td>Row Count : </td><td>" + rowCount + "</td></tr></table>");
		htmlContents.add("<table><tr><td><span lang='en-us'><font size='2'>Date : " + (new Date()).toString()
				+ "</font></span></td></tr></table>");
		htmlContents.add("</body></html>");
		try {
			// String reportLocation="";
			// reportLocation=loadSaveASHTMLDialog(this);
			VWCUtil.writeListToFile(htmlContents, path, err, "UTF-8");
			finalResult.clear();
		} catch (Exception ex) {

		}
	}

	public static String loadSaveASHTMLDialog(Component parent) {
		JFileChooser chooser = new JFileChooser();
		VWFileFilter filter = new VWFileFilter();
		filter.addExtension("html");
		filter.setDescription("Html Files");
		chooser.setCurrentDirectory(new File("c:\\"));
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String path = chooser.getSelectedFile().getPath();
			if (!path.toUpperCase().endsWith(".HTML") && !path.toUpperCase().endsWith(".XML"))
				path += ".html";
			return path;
		}
		;
		return "";
	}

	public void BtnUpdate_actionPerformed(ActionEvent event) {
		if (BtnUpdate.getText().equals(BTN_UPDATE_NAME)) {
		} else if (BtnUpdate.getText().equals(BTN_CANCEL_NAME)) {
			try {
				AdminWise.printToConsole("Inside cancel....");
				tablePanel.setVisible(false);
				SPTable.getViewport().add(Table);
				cmbAvailebReportsList.setSelectedIndex(0);
				PanelRouteSetting.setVisible(false);
				CURRENT_MODE = 0;
				setEnableMode(MODE_CONNECT);
				clearStaticReportSettings();
			} catch (Exception e) {
				AdminWise.printToConsole("Exception in process cancel :"+e);
			}
		}

	}

	public void cmbName_actionPerformed(ItemEvent event) {
		try {
			if (cmbName.getSelectedItem().toString().trim().equalsIgnoreCase(LBL_REFRESH_NAME)) {
				loadInitSettings();
			}

			int selIndex = (cmbModule.getSelectedIndex() + 2); // +2 is to avoid Document and Folder Notification
																// Settings
			int nodeId = getNodeId(selIndex);
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in cmbName_actionPerformed : " + e.getMessage());
		}
	}

	public void cmbAvailebReportsList_actionPerformed(ItemEvent event) {
		try {
			staticReportID = null;
			staticReportType = null;
			clearGeneralReportFields();
			/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
			// if (cmbAvailebReportsList.getSelectedItem().toString().equals("<Available
			// Reports>"))
			if ((cmbAvailebReportsList.getSelectedItem().toString().equals("<Custom Reports>"))
					|| (cmbAvailebReportsList.getSelectedItem().toString().equals("<System Reports>"))) {
				return;
			}

			ArrayList<String> staticReportDetails = AdminWise.gConnector.ViewWiseClient.checkStaticReport(gCurRoom,
					cmbAvailebReportsList.getSelectedItem().toString());
			AdminWise.printToConsole(
					"cmbAvailebReportsList 123....." + cmbAvailebReportsList.getSelectedItem().toString());
			lblTitle.setText(cmbAvailebReportsList.getSelectedItem().toString());
			AdminWise.printToConsole("staticReportDetails....." + staticReportDetails);
			if (staticReportDetails != null && staticReportDetails.size() > 0) {
				String[] reportDetails = (staticReportDetails.get(0)).split("\t");
				staticReportID = reportDetails[0];
				if (reportDetails.length > 1)
					staticReportType = reportDetails[1];
			}
			AdminWise.printToConsole("staticReportID :" + staticReportID + " staticReportType :" + staticReportType);
			if (staticReportID != null && !staticReportID.equals("0")) {
				AdminWise.printToConsole("inside static jasper reports......");
				// SPTable.getViewport().add(lablePanel);
				SPTable.getViewport().add(PanelRouteSetting);
				spinnerTaskInterval1.setEnabled(false);
				spinnerTaskInterval2.setEnabled(false);
				spinnerTaskInterval3.setEnabled(false);
				TxtMFrom.setEnabled(true);
				TxtMTo.setEnabled(true);
				rbuttondate1.setSelected(true);
				rbuttondate2.setSelected(false);

				/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
				nLeft = 60;
				nTop = 40;
				lblTitle.setBounds(nLeft, nTop, lblTitle.getWidth(), lblTitle.getHeight());
				nTop = nTop + 60;
				cabinetPanel.setBounds(nLeft, nTop, cabinetPanel.getWidth(), cabinetPanel.getHeight());
				nTop = nTop + cabinetPanel.getHeight() + 15;
				datePanel.setBounds(nLeft, nTop, datePanel.getWidth(), datePanel.getHeight());
				nTop = nTop + datePanel.getHeight() + 15;
				reportInfoPanel.setBounds(nLeft, nTop, reportInfoPanel.getWidth(), reportInfoPanel.getHeight());
				cmbReportType.setSelectedIndex(1); // After this settings, cmbReportType change event will trigger.Title
													// should be placed after this change event.
				// loadStaticReportSettings();
				setEnableMode(MODE_STATIC_REPORT);
				staticRepOutputPanelLeft = nLeft;
				staticRepOutputPanelTop = nTop + reportInfoPanel.getHeight() + 20;
				lblTitle.setText(cmbAvailebReportsList.getSelectedItem().toString());
				/** End of CV10.2 code merges ****/

			} else {
				staticRepOutputPanelLeft = 0;
				staticRepOutputPanelTop = 0;

				cmbReportType.setEnabled(false);
				String[] inputData = null;
				VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
				Vector storedResult = new Vector();
				AdminWise.gConnector.ViewWiseClient.getWFReport(room.getId(), 2,
						cmbAvailebReportsList.getSelectedItem().toString(), storedResult);
				if (storedResult != null && storedResult.size() > 0) {
					loadSettings();
					inputData = (storedResult.get(0).toString()).split("\t");
					// BtnNewReport.setEnabled(false);
					int flag = Integer.parseInt(inputData[0]); // inputData[0]
					AdminWise.printToConsole("flag::::::" + flag);
					// inputData.nextToken(); //inputData[1]
					if (flag != 3) {
						routePanel.setVisible(true);
						generalPanel.setVisible(false);
						reportInfoPanel.setVisible(false);
						cmbReportType.setSelectedIndex(0); // After this settings, cmbReportType change event will
															// trigger.Title should be placed after this change event.
						/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
						lblTitle.setText(cmbAvailebReportsList.getSelectedItem().toString());
						if (flag == 1) {
							try {
								VWRouteReportSettingsPanel.workFlowActionList
										.setModel(VWRouteReportSettingsPanel.workFlowActionListModel);
								VWRouteReportSettingsPanel.workFlowActionList.getModel().removeLocks();
								VWRouteReportSettingsPanel.workFlowActionList.setEnabled(true);
								VWRouteReportSettingsPanel.workFlowTaskList.setEnabled(false);
								VWRouteReportSettingsPanel.workFlowTaskList.getModel().lockAll();
								String selectedIds = inputData[2];
								String[] workFlowStatus = selectedIds.split(";");
								java.util.List valueList = new ArrayList();
								for (int i = 0; i < workFlowStatus.length; i++) {
									valueList.add(workFlowStatus[i]);
								}
								VWRouteReportSettingsPanel.workFlowActionList.getModel().addLock(true);
								if (valueList.contains("1")) {
									VWRouteReportSettingsPanel.workFlowActionList.getModel().addCheck("New");
								}
								if (valueList.contains("2")) {
									VWRouteReportSettingsPanel.workFlowActionList.getModel().addCheck("Pending");
								}
								if (valueList.contains("3")) {
									VWRouteReportSettingsPanel.workFlowActionList.getModel().addCheck("Completed");
								}
								if (valueList.contains("4")) {
									VWRouteReportSettingsPanel.workFlowActionList.getModel().addCheck("To be reviewed");
								}
							} catch (Exception e) {
								AdminWise.printToConsole(
										"exceptin while getting workflowaction list:::" + e.getMessage());
							}
							String action_task = inputData[3];
						} else if (flag == 2) {
							String action_task = inputData[2];
							VWRouteReportSettingsPanel.workFlowActionList.setEnabled(false);
							VWRouteReportSettingsPanel.workFlowActionList.getModel().lockAll();
							VWRouteReportSettingsPanel.workFlowTaskList.getModel().removeLocks();
							VWRouteReportSettingsPanel.workFlowTaskList.setEnabled(true);
							String availableWFTasks = inputData[3];
							String[] taskStatus = availableWFTasks.split(";");
							for (int i = 0; i < taskStatus.length; i++) {
								VWRouteReportSettingsPanel.workFlowTaskList.getModel().addLock(true);
								VWRouteReportSettingsPanel.workFlowTaskList.getModel().addCheck(taskStatus[i]);
							}
						}

						String selectedRouteId = inputData[4];
						String[] selectedRouteIdStr = selectedRouteId.split(";");
						AdminWise.printToConsole("selectedRouteId  :::" + selectedRouteId);
						AdminWise.printToConsole("selectedRouteIdStr  :::" + selectedRouteIdStr);
						AdminWise.printToConsole("selectedRouteIdStr[0]  :::" + selectedRouteIdStr[0].toString());
						if (selectedRouteIdStr[0].equals("0")) {
							AdminWise.printToConsole("if check of selectedRouteidStr:::");
							VWRouteReportSettingsPanel.availableWorkFlowList.getModel().checkAll();

						} else {
							AdminWise.printToConsole("else check of selectedRouteidStr:::");
							for (int i = 0; i < selectedRouteIdStr.length; i++) {
								String routeName = getKeyFromValue(VWRouteReportSettingsPanel.availableWorkFlowMap,
										Integer.parseInt(selectedRouteIdStr[i])).toString();
								AdminWise.printToConsole("else check of routeName:::" + routeName);
								VWRouteReportSettingsPanel.availableWorkFlowList.getModel().addLock(true);
								VWRouteReportSettingsPanel.availableWorkFlowList.getModel().addCheck(routeName);
							}
						}

						AdminWise.printToConsole("after loading selectedRouteidStr:::");

						Vector<String> availableTaskData = new Vector();
						AdminWise.printToConsole("before loading getWorkFlowReportOptions(room.getId(),4,:::");
						if (selectedRouteIdStr.length <= 1)
							AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(), 4,
									selectedRouteId, availableTaskData);
						AdminWise.printToConsole("after loading getWorkFlowReportOptions(room.getId(),4,:::");
						if (availableTaskData.size() > 0) {
							AdminWise.printToConsole("inside if check of availableTaskData:::");
							VWRouteReportSettingsPanel.availableTaskList.getModel().clear();
							for (String actionData : availableTaskData) {
								StringTokenizer st = new StringTokenizer(actionData, "\t");
								int id = Integer.parseInt(st.nextToken());
								String taskName = st.nextToken();
								VWRouteReportSettingsPanel.availableTaskMap.put(taskName, id);
								VWRouteReportSettingsPanel.availableTaskList.getModel().addElement(taskName);
							}
							AdminWise.printToConsole("after loading of availableTaskData:::");

						}
						String selectedTaskName = inputData[5];
						if (selectedTaskName != null) {
							String[] selectedTaskNameStr = selectedTaskName.split(";");
							if (selectedTaskNameStr[0].equals("0")) {
								VWRouteReportSettingsPanel.availableTaskList.getModel().checkAll();
							} else {
								for (int i = 0; i < selectedTaskNameStr.length; i++) {
									VWRouteReportSettingsPanel.availableTaskList.getModel().addLock(true);
									VWRouteReportSettingsPanel.availableTaskList.getModel()
											.addCheck(selectedTaskNameStr[i]);
								}
							}
						}
						// AdminWise.printToConsole("selectedTaskName::::"+selectedTaskName);
						String selectedUserName = inputData[6]; // inputData[6]
						String[] selectedUserNameStr = selectedUserName.split(";");
						AdminWise.printToConsole("selectedUserNameStr::::" + selectedUserNameStr);
						if (selectedUserNameStr[0].equals("Null")) {
							AdminWise.printToConsole("inside null condition check::::");
							VWRouteReportSettingsPanel.userGroupList.getModel().checkAll();
						} else {
							AdminWise.printToConsole("inside else condition check::::");
							for (int i = 0; i < selectedUserNameStr.length; i++) {
								VWRouteReportSettingsPanel.userGroupList.getModel().addLock(true);
								VWRouteReportSettingsPanel.userGroupList.getModel().addCheck(selectedUserNameStr[i]);
							}
						}
					} else {
						routePanel.setVisible(false);
						generalPanel.setVisible(true);
						reportInfoPanel.setVisible(true);
						cmbReportType.setSelectedIndex(1); // After this settings, cmbReportType change event will
															// trigger.Title should be placed after this change event.
						lblTitle.setText(cmbAvailebReportsList.getSelectedItem().toString());
						AdminWise.printToConsole("inputData[5] :::: " + inputData[5]);
						if (inputData[5] != null && inputData[5] != "null" && inputData[5].trim().length() > 0) {
							AdminWise.printToConsole("SearchID Before converting to integer :" + inputData[5]);
							try {
								savedSearchID = Integer.parseInt(inputData[5]);
								loadGeneralSearchInfo(savedSearchID);
							} catch (Exception e) {
								AdminWise.printToConsole("Exception in loadGeneralSearchInfo :" + e.getMessage());
							}
						}
					}
					String rButtonSelected = inputData[7];
					AdminWise.printToConsole("rButtonSelected::::::::::" + rButtonSelected);
					if (rButtonSelected.equals("1")) {
						String selectedMonths = inputData[8];
						AdminWise.printToConsole("selectedMonths::::::::::" + selectedMonths);
						spinnerTaskInterval1.setValue(Integer.valueOf(selectedMonths));
						// txtMonth.setText(selectedMonths);
						String selectedDays = inputData[9];
						AdminWise.printToConsole("selectedDays::::::::::" + selectedDays);
						spinnerTaskInterval2.setValue(Integer.valueOf(selectedDays));
						String selectedHours = inputData[10];
						AdminWise.printToConsole("selectedHours::::::::::" + selectedHours);
						spinnerTaskInterval3.setValue(Integer.valueOf(selectedHours));
						String fromDate = inputData[11];
						AdminWise.printToConsole("fromDate::::::::::" + fromDate);
						String[] splittedDate = fromDate.split(" ");
						TxtMFrom.setText(splittedDate[0]);
						AdminWise.printToConsole("timespinner getValue:::" + timeSpinnerFrom.getValue());
						SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy hh:mm:ss a");
						timeSpinnerFrom.setValue(sdf.parseObject(fromDate));
						String toDate = inputData[12];
						splittedDate = toDate.split(" ");
						// AdminWise.printToConsole("splittedToDate[0]::::"+splittedToDate[0]);
						// AdminWise.printToConsole("splittedToDate[1]::::"+splittedToDate[1]+"
						// "+splittedToDate[2]);
						TxtMTo.setText(splittedDate[0]);
						timeSpinnerTo.setValue(sdf.parseObject(toDate));
						spinnerTaskInterval1.setEnabled(false);
						spinnerTaskInterval2.setEnabled(false);
						spinnerTaskInterval3.setEnabled(false);
						TxtMFrom.setEnabled(true);
						TxtMTo.setEnabled(true);
						rbuttondate1.setSelected(true);
						rbuttondate2.setSelected(false);
					} else {
						String selectedMonths = inputData[8];
						AdminWise.printToConsole("selectedMonths1::::::::::" + selectedMonths);
						spinnerTaskInterval1.setValue(Integer.valueOf(selectedMonths));

						String selectedDays = inputData[9];
						AdminWise.printToConsole("selectedDay1::::::::::" + selectedDays);
						spinnerTaskInterval2.setValue(Integer.valueOf(selectedDays));
						String selectedHours = inputData[10];
						AdminWise.printToConsole("selectedHours1::::::::::" + selectedHours);
						spinnerTaskInterval3.setValue(Integer.valueOf(selectedHours));
						Date date = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
						String strDate = sdf.format(date);
						AdminWise.printToConsole("strDate 2 ::::  " + strDate);
						TxtMFrom.setText(strDate);
						TxtMTo.setText(strDate);
						TxtMFrom.setEnabled(false);
						TxtMTo.setEnabled(false);
						spinnerTaskInterval1.setEnabled(true);
						spinnerTaskInterval2.setEnabled(true);
						spinnerTaskInterval3.setEnabled(true);
						rbuttondate1.setSelected(false);
						rbuttondate2.setSelected(true);
					}

					txtReportName.setText(cmbAvailebReportsList.getSelectedItem().toString());
					txtReportName.setEnabled(false);
					txtReportName.setEditable(false);
				}
			}
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while loading the available report  value:::" + ex.getMessage());
		}
	}

	public void BtnDelete_actionPerformed(ActionEvent event) {
		try {
			if (BtnDelete.getText().equals(BTN_DELETE_NAME)) {
				VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
				int retVal = VWMessage.showConfirmDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RoutePanel.Delete") + " "
								+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") + " " + "Report"
								+ "?");
				if (retVal == 0) {
					AdminWise.gConnector.ViewWiseClient.deleteWFReport(room.getId(), txtReportName.getText());
					PanelRouteSetting.setVisible(false);
					BtnUpdate.setEnabled(false);
					BtnDelete.setEnabled(false);
					CURRENT_MODE = 0;
					setEnableMode(MODE_CONNECT);
				}

			}
		}

		catch (Exception ex) {
			AdminWise.printToConsole("Exception while deleting notification : " + ex.getMessage());
		}
	}

	public static Object getKeyFromValue(Map hm, Object value) {
		for (Object o : hm.keySet()) {
			if (hm.get(o).equals(value)) {
				return o;
			}
		}
		return null;
	}

	public void BtnNewReport_actionPerformed(ActionEvent event) {
		try {
			staticReportID = null;
			staticReportType = null;
			if (BtnNewReport.getText().equals(BTN_NEWREPORT_NAME)) {
				/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
				savedSearchID = 0;
				nodeId = 0;
				clearStaticReportSettings();
				loadSettings();
				cmbReportType.setSelectedIndex(0);
				cmbReportType.setEnabled(true);
				cmbReportType_ItemStateChanged();
				VWRouteReportSettingsPanel.availableWorkFlowselectAllFlag = 0;
				VWRouteReportSettingsPanel.availableTaskselectAllFlag = 0;
				VWRouteReportSettingsPanel.availablUserSelectAllFlag = 0;
				VWRouteReportSettingsPanel.workFlowActionListModel.removeLocks();
				VWRouteReportSettingsPanel.workFlowTaskListModel.removeLocks();
				VWRouteReportSettingsPanel.workFlowActionList.setEnabled(true);
				VWRouteReportSettingsPanel.workFlowTaskList.setEnabled(true);
				txtReportName.setEditable(true);
				txtReportName.setEnabled(true);

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				String strDate = sdf.format(date);
				AdminWise.printToConsole("strDate BtnNewReport_actionPerformed ::::  " + strDate);
				TxtMFrom.setText(strDate);
				TxtMTo.setText(strDate);

				AdminWise.printToConsole("strDate BtnNewReport_actionPerformed After setting ::::  " + strDate);

				rbuttondate1.setSelected(true);
				rbuttondate2.setSelected(false);
				if (rbuttondate1.isSelected() == true) {
					TxtMFrom.setEnabled(true);
					TxtMTo.setEnabled(true);
				}

				if (rbuttondate2.isSelected() == false) {
					spinnerTaskInterval1.setEnabled(false);
					spinnerTaskInterval2.setEnabled(false);
					spinnerTaskInterval3.setEnabled(false);
				}
				/*
				 * txtMonth.setText("0"); txtDay.setText("0"); txtHour.setText("0");
				 */
				spinnerTaskInterval1.setValue(new Integer(0));
				spinnerTaskInterval2.setValue(new Integer(0));
				spinnerTaskInterval3.setValue(new Integer(0));

				BtnDelete.setEnabled(false);
				if (scroll != null)
					scroll.setVisible(false);
				staticRepOutputPanelLeft = 0;
				staticRepOutputPanelTop = 0;

			} else if (BtnNewReport.getText().equals(BTN_SAVE_NAME)) {
				String month = null, day = null, hour = null, totalDate = null;
				String reportName = txtReportName.getText().toString();
				String selectedType = cmbReportType.getSelectedItem().toString().trim();
				AdminWise.printToConsole("selectedType.........." + selectedType);
				AdminWise.printToConsole("report type 0.........." + VWConstant.lstReportType[0]);
				VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
				/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
				if (selectedType.equalsIgnoreCase(VWConstant.lstReportType[0])) {
					String actionListSelectValue = "";
					java.util.List actionWorkFlowList = VWRouteReportSettingsPanel.workFlowActionList.getModel()
							.getCheckeds();
					if ((actionWorkFlowList != null) && actionWorkFlowList.size() > 0) {
						if (VWRouteReportSettingsPanel.workFlowActionList.isEnabled()) {
							AdminWise.printToConsole("actionWorkFlowList:::::" + actionWorkFlowList);
							if (actionWorkFlowList.contains("New") || actionWorkFlowList.contains("Started")) {
								actionListSelectValue = actionListSelectValue + ";" + "1";
							}
							if (actionWorkFlowList.contains("Pending")) {
								actionListSelectValue = actionListSelectValue + ";" + "2";
							}
							if (actionWorkFlowList.contains("Completed")) {
								actionListSelectValue = actionListSelectValue + ";" + "3";
							}
							if (actionWorkFlowList.contains("To be reviewed")) {
								actionListSelectValue = actionListSelectValue + ";" + "4";
							}
						}

						if (actionListSelectValue.length() > 0)
							actionListSelectValue = actionListSelectValue.substring(1, actionListSelectValue.length());
					} else if (VWRouteReportSettingsPanel.workFlowTaskList.isEnabled()) {
						java.util.List actionTaskList = VWRouteReportSettingsPanel.workFlowTaskList.getModel()
								.getCheckeds();
						if (actionTaskList != null && actionTaskList.size() > 0) {
							for (int i = 0; i < actionTaskList.size(); i++) {
								actionListSelectValue = actionListSelectValue + ";" + actionTaskList.get(i);
							}

							if (actionListSelectValue.length() > 0)
								actionListSelectValue = actionListSelectValue.substring(1,
										actionListSelectValue.length());
							AdminWise.printToConsole("the actionListSelectValue::::" + actionListSelectValue);
						}
					}
					java.util.List availableWorkFlowList = VWRouteReportSettingsPanel.availableWorkFlowList.getModel()
							.getCheckeds();
					String availableWorkFlowValues = "";
					if (availableWorkFlowList != null && availableWorkFlowList.size() > 0) {
						if (availableWorkFlowList.contains("Select All")) {
							availableWorkFlowValues = availableWorkFlowValues + "0";
							AdminWise.printToConsole(
									"availableWorkFlowValues inside if::::::::" + availableWorkFlowValues);
						} else {
							for (int i = 0; i < availableWorkFlowList.size(); i++) {

								availableWorkFlowValues = availableWorkFlowValues + ";"
										+ VWRouteReportSettingsPanel.availableWorkFlowMap
												.get(availableWorkFlowList.get(i).toString());

							}

							availableWorkFlowValues = availableWorkFlowValues.substring(1,
									availableWorkFlowValues.length());
							AdminWise.printToConsole(
									"availableWorkFlowValues inside else::::::::" + availableWorkFlowValues);
						}

					}

					java.util.List availableTaskList = VWRouteReportSettingsPanel.availableTaskList.getModel()
							.getCheckeds();
					String availableTaskValues = "";
					if (availableTaskList != null && availableTaskList.size() > 0) {
						if (availableTaskList != null && availableTaskList.size() > 0) {
							if (availableTaskList.contains("Select All")) {
								availableTaskValues = availableTaskValues + "0";
							} else {
								for (int i = 0; i < availableTaskList.size(); i++) {
									if (availableTaskList.get(i).equals("Select All")) {
										availableTaskValues = "0";
									} else
										availableTaskValues = availableTaskValues + ";"
												+ availableTaskList.get(i).toString();
								}
								availableTaskValues = availableTaskValues.substring(1, availableTaskValues.length());
							}

						}

					}
					String availableUserGroupValues = "";

					java.util.List userGroupList = VWRouteReportSettingsPanel.userGroupList.getModel().getCheckeds();
					if (userGroupList != null && userGroupList.size() > 0) {
						if (userGroupList.contains("Select All")) {
							availableUserGroupValues = availableUserGroupValues + "Null";
						} else {
							for (int i = 0; i < userGroupList.size(); i++) {
								availableUserGroupValues = availableUserGroupValues + ";"
										+ userGroupList.get(i).toString();
							}
							availableUserGroupValues = availableUserGroupValues.substring(1,
									availableUserGroupValues.length());
						}
					}
					AdminWise.printToConsole("availableUserGroupValues:::::::::::::" + availableUserGroupValues);

					month = spinnerTaskInterval1.getValue().toString();
					day = spinnerTaskInterval2.getValue().toString();
					hour = spinnerTaskInterval3.getValue().toString();
					if (month.equals("")) {
						month = "0";
					}
					if (day.equals("")) {
						day = "0";
					}
					if (hour.equals("")) {
						hour = "0";
					}
					// String date=month+Util.SepChar+day+Util.SepChar+hour;

					/*
					 * Vector chkReport=new Vector();
					 * AdminWise.gConnector.ViewWiseClient.checkWFReport(gCurRoom, reportName,
					 * chkReport); int retVal=Integer.parseInt(chkReport.get(0).toString());
					 * if(retVal>0){ JOptionPane.showMessageDialog(AdminWise.adminFrame,
					 * AdminWise.connectorManager.getString("RouteReport.MSG_NamealreadyExist"));
					 * return; }
					 */
					if (reportName.equals("")) {
						AdminWise.printToConsole("Inise reportName check");
						JOptionPane.showMessageDialog(AdminWise.adminFrame,
								AdminWise.connectorManager.getString("RouteReport.MSG_WorkFlowReportName"));
						return;
					} else {
						AdminWise.printToConsole("Inise else of reportName check");
						if (actionListSelectValue.length() == 0) {
							JOptionPane.showMessageDialog(AdminWise.adminFrame,
									AdminWise.connectorManager.getString("RouteReport.MSG_WFAction"));
							return;
						}
						if (availableWorkFlowValues.length() == 0) {
							JOptionPane.showMessageDialog(AdminWise.adminFrame,
									AdminWise.connectorManager.getString("RouteReport.MSG_WFNotSelected"));
							return;
						}
						if (availableTaskValues.length() == 0) {
							JOptionPane.showMessageDialog(AdminWise.adminFrame,
									AdminWise.connectorManager.getString("RouteReport.MSG_WFTaskNotSelected"));
							return;
						}
						if (availableUserGroupValues.length() == 0) {
							JOptionPane.showMessageDialog(AdminWise.adminFrame,
									AdminWise.connectorManager.getString("RouteReport.MSG_UserNotSelected"));
							return;
						}

						if (rbuttondate2.isSelected() == true) {
							month = spinnerTaskInterval1.getValue().toString();
							day = spinnerTaskInterval2.getValue().toString();
							hour = spinnerTaskInterval3.getValue().toString();

							if ((Integer.parseInt(month) == 0) && (Integer.parseInt(day) == 0)
									&& (Integer.parseInt(hour) == 0)) {
								JOptionPane.showMessageDialog(AdminWise.adminFrame,
										AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
								return;
							}
							totalDate = "2" + Util.SepChar + month + Util.SepChar + day + Util.SepChar + hour;
						} else if (rbuttondate1.isSelected() == true) {
							String dateFrom = TxtMFrom.getSelectedItem().toString();
							dateFrom = dateFrom + " " + simpDate.format(timeSpinnerFrom.getValue());

							String dateTo = TxtMTo.getSelectedItem().toString();
							dateTo = dateTo + " " + simpDate.format(timeSpinnerTo.getValue());

							if ((TxtMFrom.getSelectedItem().toString() == "")
									|| (TxtMTo.getSelectedItem().toString() == "")) {
								JOptionPane.showMessageDialog(AdminWise.adminFrame,
										AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
								return;
							} else {
								String pattern = "MM/dd/yyyy";
								// totalDate="1"+Util.SepChar+dateFrom+Util.SepChar+dateTo+Util.SepChar+"0";
								SimpleDateFormat format = new SimpleDateFormat(pattern);
								try {
									Date date1 = format.parse(dateFrom);
									Date date2 = format.parse(dateTo);
									totalDate = "1" + Util.SepChar + "0" + Util.SepChar + "0" + Util.SepChar + "0"
											+ Util.SepChar + dateFrom + Util.SepChar + dateTo;
									StringTokenizer st = new StringTokenizer(getMonthDateDifference(date1, date2),
											"\t");
									String month1 = st.nextToken();
									String day1 = st.nextToken();
									if ((Integer.parseInt(month1) == 0) && (Integer.parseInt(day1) == 0)) {
										JOptionPane.showMessageDialog(AdminWise.adminFrame, AdminWise.connectorManager
												.getString("RouteReport.MSG_TimeperiodValidation1"));
										return;
									}
									AdminWise.printToConsole("totalDate::::" + totalDate);
								} catch (Exception e) {

								}
							}
						}

						/*
						 * if (!isValid(month) || (Integer.parseInt(month) > 99)) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_MonthValidation"));
						 * return; } else { if (!isValid(day) || (Integer.parseInt(day) > 31) ) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_DayValidation"));
						 * return; } else { if (!isValid(hour) || (Integer.parseInt(hour)>24)) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_HourValidation"));
						 * return; } else { if ((Integer.parseInt(month) ==0 ) && (Integer.parseInt(day)
						 * == 0) && (Integer.parseInt(hour) == 0 )) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"))
						 * ; return; } } }
						 * 
						 * int monthval = Integer.parseInt(month); if (monthval > 99) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_MonthValidation1"));
						 * return; } } if (!isValid(day) || (Integer.parseInt(day) > 31) ) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_DayValidation"));
						 * return; } else { int dayval = Integer.parseInt(day); if (dayval > 31) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_DayValidation1"));
						 * return; } } if (!isValid(hour) || (Integer.parseInt(hour)>24)) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_HourValidation"));
						 * return; } else { int hourval = Integer.parseInt(hour); if (hourval > 24) {
						 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
						 * AdminWise.connectorManager.getString("RouteReport.MSG_HourValidation1"));
						 * return; } }
						 */

					}

					if (VWRouteReportSettingsPanel.workFlowActionList.isEnabled()) {
						String tasks = "New;Pending;Review";
						AdminWise.gConnector.ViewWiseClient.saveWFReport(room.getId(), 1, reportName,
								actionListSelectValue, tasks, availableWorkFlowValues, availableTaskValues,
								availableUserGroupValues, totalDate, reportResult);
						AdminWise.printToConsole("reportResult value:::" + reportResult);
					} else if (VWRouteReportSettingsPanel.workFlowTaskList.isEnabled()) {
						String task = "1;2;3;4";
						AdminWise.gConnector.ViewWiseClient.saveWFReport(room.getId(), 2, reportName, task,
								actionListSelectValue, availableWorkFlowValues, availableTaskValues,
								availableUserGroupValues, totalDate, reportResult);
					}
					// Code added for closing the UI Settings and move back to New Report State on
					// 22 Apr 2016 by srikanth
					SPTable.getViewport().add(Table);
				} else {
					if (reportName.equals("")) {
						AdminWise.printToConsole("Inside reportName check");
						JOptionPane.showMessageDialog(AdminWise.adminFrame,
								AdminWise.connectorManager.getString("RouteReport.MSG_GeneralReportName"));
						return;
					}
					String reportLocation = "";// txtReportLocation.getText();
					/*
					 * if (reportLocation.trim().length() == 0) {
					 * JOptionPane.showMessageDialog(AdminWise.adminFrame,
					 * AdminWise.connectorManager.getString("RouteReport.MSG_LocationValidation"));
					 * return; }
					 */

					if (rbuttondate2.isSelected() == true) {
						month = spinnerTaskInterval1.getValue().toString();
						day = spinnerTaskInterval2.getValue().toString();
						hour = spinnerTaskInterval3.getValue().toString();

						if ((Integer.parseInt(month) == 0) && (Integer.parseInt(day) == 0)
								&& (Integer.parseInt(hour) == 0)) {
							JOptionPane.showMessageDialog(AdminWise.adminFrame,
									AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
							return;
						}
						totalDate = "2" + Util.SepChar + month + Util.SepChar + day + Util.SepChar + hour;
					} else if (rbuttondate1.isSelected() == true) {
						String dateFrom = TxtMFrom.getSelectedItem().toString();
						dateFrom = dateFrom + " " + simpDate.format(timeSpinnerFrom.getValue());

						String dateTo = TxtMTo.getSelectedItem().toString();
						dateTo = dateTo + " " + simpDate.format(timeSpinnerTo.getValue());

						if ((TxtMFrom.getSelectedItem().toString() == "")
								|| (TxtMTo.getSelectedItem().toString() == "")) {
							JOptionPane.showMessageDialog(AdminWise.adminFrame,
									AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
							return;
						} else {
							String pattern = "MM/dd/yyyy";
							// totalDate="1"+Util.SepChar+dateFrom+Util.SepChar+dateTo+Util.SepChar+"0";
							SimpleDateFormat format = new SimpleDateFormat(pattern);
							try {
								Date date1 = format.parse(dateFrom);
								Date date2 = format.parse(dateTo);
								totalDate = "1" + Util.SepChar + "0" + Util.SepChar + "0" + Util.SepChar + "0"
										+ Util.SepChar + dateFrom + Util.SepChar + dateTo;
								StringTokenizer st = new StringTokenizer(getMonthDateDifference(date1, date2), "\t");
								String month1 = st.nextToken();
								String day1 = st.nextToken();
								if ((Integer.parseInt(month1) == 0) && (Integer.parseInt(day1) == 0)) {
									JOptionPane.showMessageDialog(AdminWise.adminFrame, AdminWise.connectorManager
											.getString("RouteReport.MSG_TimeperiodValidation1"));
									return;
								}
								AdminWise.printToConsole("totalDate::::" + totalDate);
							} catch (Exception e) {

							}
						}
					}

					// -----------------------Code added by srikanth for saving into search table
					String pattern = "MM/dd/yyyy";
					// totalDate="1"+Util.SepChar+dateFrom+Util.SepChar+dateTo+Util.SepChar+"0";
					SimpleDateFormat format = new SimpleDateFormat(pattern);

					String dateFrom1 = TxtMFrom.getSelectedItem().toString();
					// dateFrom1=dateFrom1+" "+simpDate.format(timeSpinnerFrom.getValue());

					String dateTo1 = TxtMTo.getSelectedItem().toString();
					// dateTo1=dateTo1+" "+simpDate.format(timeSpinnerTo.getValue());

					// Date date1 = format.parse(dateFrom1);
					// Date date2 = format.parse(dateTo1);

					Search search = new Search(0, "");
					search.setCreationDate1(dateFrom1);
					search.setCreationDate2(dateTo1);
					// -----------------------Code added by srikanth for saving into search table

					search.setName(reportName);
					search.setTemp(false);
					AdminWise.printToConsole("nodeId......." + nodeId);
					search.setNodeId(nodeId);
					search.setId(savedSearchID);
					columnDescription = null;
					int searchId = SaveSearchActionForJasperReport(search);
					AdminWise.printToConsole("searchId......." + searchId);
					if (searchId > 0) {
						AdminWise.gConnector.ViewWiseClient.saveWFReport(room.getId(), 3, reportName, null, null, null,
								String.valueOf(searchId), null, totalDate, reportLocation, reportResult);
						clearGeneralReportFields();
						staticReportID = null;
						staticReportType = null;
					}
				}
				cmbAvailebReportsList.setSelectedIndex(0);
				PanelRouteSetting.setVisible(false);
				CURRENT_MODE = 0;
				BtnNewReport.setEnabled(true);
				setEnableMode(MODE_CONNECT);

			}

		} catch (Exception e) {
			AdminWise.printToConsole("Exception in BtnNotification_actionPerformed() :: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public boolean isValid(String val) {
		try {
			Integer.parseInt(val);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private void setCmbName(int moduleId, int referenceId) {
		try {
			switch (moduleId) {
			case DOCUMENT_TYPE:
				setDocTypeName(referenceId);
				break;

			case RETENTION:
				setRetentionName(referenceId);
				break;

			case ROUTE:
				setRouteName(referenceId);
				break;
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception while setting CmbName : " + e.getMessage());
		}
	}

	private void setRouteName(int referenceId) {
		try {
			int recId;
			int itemCount = cmbName.getItemCount();
			for (int i = 0; i < itemCount; i++) {
				RouteInfo routeInfo = (RouteInfo) cmbName.getItemAt(i);
				if (routeInfo != null) {
					recId = routeInfo.getRouteId();
					if (recId == referenceId) {
						cmbName.setSelectedIndex(i);
					}
				}
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception while setting route Name : " + e.getMessage());
		}
	}

	private void setRetentionName(int referenceId) {
		try {
			int recId;
			int itemCount = cmbName.getItemCount();
			for (int i = 0; i < itemCount; i++) {
				VWRetention retention = (VWRetention) cmbName.getItemAt(i);
				if (retention != null) {
					recId = retention.getId();
					if (recId == referenceId) {
						cmbName.setSelectedIndex(i);
					}
				}
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception while setting Retention Name : " + e.getMessage());
		}
	}

	private void setDocTypeName(int referenceId) {
		try {
			int recId;
			int itemCount = cmbName.getItemCount();
			for (int i = 0; i < itemCount; i++) {
				VWDocTypeRec docTypeRec = (VWDocTypeRec) cmbName.getItemAt(i);
				if (docTypeRec != null) {
					recId = docTypeRec.getId();
					if (recId == referenceId) {
						cmbName.setSelectedIndex(i);
					}
				}
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception while setting DocType Name : " + e.getMessage());
		}
	}

	private String getUserMailId() {
		int ret = 0;
		String userMailId = "";
		Vector mailId = new Vector();
		try {
			ret = AdminWise.gConnector.ViewWiseClient.getLoggedInUserMailId(gCurRoom, mailId);
			if (mailId != null && mailId.size() > 0) {
				if (mailId.get(0) != null) {
					userMailId = (mailId.get(0) != null ? mailId.get(0).toString().trim() : "");
				}
			}
		} catch (Exception e) {
			if (userMailId == null)
				userMailId = "";
		}
		return userMailId;
	}

	private String getUserName() {
		String userName = "";
		try {
			if (session != null)
				userName = session.user;
		} catch (Exception ex) {
			userName = "";
		}
		return userName;
	}

	private int getNodeId(int nodeType) {
		int nodeId = 0;
		try {
			switch (nodeType) {
			case DOCUMENT_TYPE:
				VWDocTypeRec docTypeRec = (VWDocTypeRec) cmbName.getSelectedItem();
				if (docTypeRec != null) {
					nodeId = docTypeRec.getId();
				}
				break;

			case RETENTION:
				VWRetention retention = (VWRetention) cmbName.getSelectedItem();
				if (retention != null) {
					nodeId = retention.getId();
				}
				break;

			case ROUTE:
				RouteInfo routeInfo = (RouteInfo) cmbName.getSelectedItem();
				if (routeInfo != null) {
					nodeId = routeInfo.getRouteId();
				}
				break;
			}
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while getting node id : " + ex.getMessage());
		}
		return nodeId;
	}

	public void loadSettings() {
		try {
			Vector workFlowActionVector = new Vector();
			Vector workTaskActionVector = new Vector();
			Vector availableWorkFlowVector = new Vector();
			Vector availableTaskVector = new Vector();
			Vector usersGroupsVector = new Vector();
			AdminWise.printToConsole("before getWorkFlowReportOptions call");
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(), 1, "0", workFlowActionVector);
			AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(), 2, "0", workTaskActionVector);
			AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(), 3, "0", availableWorkFlowVector);
			AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(), 4, "0", availableTaskVector);
			AdminWise.gConnector.ViewWiseClient.getWorkFlowReportOptions(room.getId(), 5, "0", usersGroupsVector);
			gCurRoom = room.getId();
			settingsPanel.loadUI(workFlowActionVector, workTaskActionVector, availableWorkFlowVector,
					availableTaskVector, usersGroupsVector);
			// added by srikanth for clearing on text boxes on click of New Report on 22 Apr
			// 2016

			if (!room.getName().equals(VWRouteReportSettingsPanel.prevRoom)) {
				VWRouteReportSettingsPanel.prevRoom = room.getName();
			}
			spinnerTaskInterval1.setValue(new Integer(0));
			spinnerTaskInterval2.setValue(new Integer(0));
			spinnerTaskInterval3.setValue(new Integer(0));
			txtReportName.setText("");

			cmbReportType.setVisible(true);
			lblReportType.setVisible(true);

			SPTable.getViewport().add(PanelRouteSetting);
			PanelRouteSetting.setVisible(true);
			routePanel.setVisible(true);
			generalPanel.setVisible(false);
			datePanel.setVisible(true);
			reportInfoPanel.setVisible(false);
			setEnableMode(MODE_NEW);

		} catch (Exception ex) {
			AdminWise.printToConsole("Exception while loading settings : " + ex.getMessage());
		}
	}

	private void stopGridEdit() {
		try {
			Table.getCellEditor().stopCellEditing();
		} catch (Exception e) {
		}
		;
	}

	// ------------------------------------------------------------------------------------------------------------

	private void loadDocumentInDTC(int docId) {
		AdminWise.printToConsole("loadDocumentInDTC.......");
		// int docId =0; /*this.getRowDocLockId(row);*/
		VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		String room = vwroom.getName();
		try {
			// create a temp folder to place reference file in it
			File folder = new File("c:\\VWReferenceFile\\");
			folder.mkdirs();
			// create a reference file to open document
			File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room, docId);

			// open reference file in ViewWise thus opening doc
			try {
				Preferences clientPref = Preferences.userRoot().node(GENERAL_PREF_ROOT);
				// String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise
				// Client\\");
				String dtcPath = VWUtil.getHome();
				Runtime.getRuntime().exec(dtcPath + "\\System\\ViewWise.exe " + vwrFile.getPath());
			} catch (Exception ex) {
				ex.printStackTrace();
				// JOptionPane.showMessageDialog(null,"DTC Installed Path not
				// found."+ex.toString());
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	// ------------------------------------------------------------------------------------------------------------
	/** CV10.2 workflow report enhancement changes. Added by Vanitha.S ****/
	public void loadReportType() {
		try {
			for (int count = 0; count < lstReportType.length; count++) {
				cmbReportType.addItem(lstReportType[count]);
			}
			cmbReportType.setSelectedIndex(0);
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in loadReportType :" + e.getMessage());
		}
	}

	class SymReportListAction implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent iEvent) {
			Object object = iEvent.getSource();
			if (object == cmbReportType) {
				cmbReportType_ItemStateChanged();
			}
		}
	}

	public void cmbReportType_ItemStateChanged() {

		String selectedType = cmbReportType.getSelectedItem().toString().trim();
		AdminWise.printToConsole("selectedType.........." + selectedType);
		AdminWise.printToConsole("report type 0.........." + VWConstant.lstReportType[0]);
		if (selectedType.equalsIgnoreCase(VWConstant.lstReportType[0])) {
			AdminWise.printToConsole("inside workflow type....");
			lblTitle.setText("Workflow Report");

			routePanel.setVisible(true);
			generalPanel.setVisible(false);
			reportInfoPanel.setVisible(false);
			cabinetPanel.setVisible(false);
			//BtnPreviewReport.setVisible(false);		
			if (scroll != null)
				scroll.setVisible(false);
		} else {
			AdminWise.printToConsole("inside general type....");
			lblTitle.setText("GENERAL REPORT");

			routePanel.setVisible(false);
			generalPanel.setVisible(true);
			reportInfoPanel.setVisible(true);
			cabinetPanel.setVisible(true);
			//BtnPreviewReport.setVisible(true);
			/*
			 * loadDocTypeFields(); loadAuthorFields(); loadIndices();
			 */
			clearGeneralSettings();
			clearGeneralReportFields();
		}
	}

	public void loadGeneralSettings() {
		AdminWise.printToConsole("loadGeneralSettings.......");
		loadDocTypeFields();
		loadAuthorFields();
		loadIndices();
		clearGeneralReportFields();
	}

	public void loadGeneralPanelData() {
		AdminWise.printToConsole("loadGeneralPanelData.......");
		// nTop = 20;
		nTop = 40;
		nLeft = 0;
		nHeight = 20;
		try {
			IndexContainsPanel.setLayout(null);
			// IndexContainsPanel.setBounds(4,0,695, 40);
			IndexContainsPanel.setBounds(45, nTop, 695, nHeight + 44);
			IndexContainsPanel.setBackground(Color.WHITE);

			// Ends ******************************
			nTop = 0;
			// JRXML
			// lblImgJrxmlPath.setText(AdminWise.connectorManager.getString("AdminFind.Lbl.IdxContainsJrxmlPath"));
			// lblIndexWith.setBounds(8,18, 241, 20);
			lblImgJrxmlPath.setBounds(nLeft, nTop + 10, 180, nHeight);
			// ImgJrxmlPathPanel.add(lblImgJrxmlPath);
			IndexContainsPanel.add(lblImgJrxmlPath);
			lblImgJrxmlPath.setForeground(Color.black);
			TxtFieldJrxml.setBounds(nLeft + 246, nTop + 10, 200, nHeight);// 270,20, 250, nHeight
			TxtFieldJrxml.setText("<Default>");
			IndexContainsPanel.add(TxtFieldJrxml);
			JrxmlBtn.setBounds(nLeft + 447, nTop + 10, 20, nHeight);
			JrxmlBtn.setIcon(VWImages.BrowseIcon);
			JrxmlBtn.addActionListener(new SymAction());
			JrxmlBtn.setVisible(true);
			IndexContainsPanel.add(JrxmlBtn);
			nTop = nTop + nHeight + 25;
			lblIndexWith.setText(AdminWise.connectorManager.getString("AdminFind.Lbl.IdxContainsExactPhrase"));
			// lblIndexWith.setBounds(8,18, 241, 20);
			lblIndexWith.setBounds(nLeft, nTop, 202, nHeight);
			IndexContainsPanel.add(lblIndexWith);
			lblIndexWith.setForeground(Color.black);
			AdminWise.printToConsole("loadGeneralPanelData.......2");
			lblIndexImage.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent me) {
					javax.swing.JPopupMenu menu = new javax.swing.JPopupMenu("");
					AdminWise.printToConsole("loadGeneralPanelData.......3");
					AdminWise.printToConsole("AdminFind.Menu.AllOfTheWords ::: "
							+ AdminWise.connectorManager.getString("AdminFind.Menu.AllOfTheWords"));
					JMenuItem subMenu1 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.AllOfTheWords"));
					JMenuItem subMenu2 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.TheExactPhrase"));
					JMenuItem subMenu3 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.AtLeastOneOfWords"));
					subMenu1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblIndexWith
									.setText(AdminWise.connectorManager.getString("AdminFind.Lbl.IdxContainsAllWords"));
							selectedIndexWith = 0;
						}
					});
					subMenu2.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblIndexWith.setText(
									AdminWise.connectorManager.getString("AdminFind.Lbl.IdxContainsExactPhrase"));
							selectedIndexWith = 1;
						}
					});

					subMenu3.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblIndexWith.setText(
									AdminWise.connectorManager.getString("AdminFind.Lbl.IdxContainsAtLeastOneOfWords"));
							selectedIndexWith = 2;
						}
					});
					menu.add(subMenu1);
					menu.add(subMenu2);
					menu.add(subMenu3);
					menu.show(lblIndexImage, me.getX(), me.getY());
				}
			});

			Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
			lblIndexImage.setCursor(handCursor);

			lblIndexImage.setIcon(VWImages.findIndexWithRightArrow);
			// lblIndexImage.setBounds(241, 18, 40, 20);
			lblIndexImage.setBounds(223, nTop, 40, nHeight);
			IndexContainsPanel.add(lblIndexImage);

			// TxtFieldIndex.setBounds(270,18, 153, nHeight);//270,20, 250, nHeight
			TxtFieldIndex.setBounds(nLeft + 246, nTop, 221, nHeight);// 270,20, 250, nHeight
			IndexContainsPanel.add(TxtFieldIndex);

			generalPanel.add(IndexContainsPanel);
			AdminWise.printToConsole("Before calling loadTextContainsPanel.......");
			// loadTextContainsPanel();
			loadDocumentPanel();
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in loadGeneralPanelData : " + e.getMessage());
		}
	}

	private void loadTextContainsPanel() {
		AdminWise.printToConsole("loadTextContainsPanel.......");
		try {
			nLeft = 0;
			nTop = 52;
			nHeight = 20;
			TextContainsPanel.setLayout(null);
			// TextContainsPanel.setBounds(4,40,695,40);
			TextContainsPanel.setBounds(45, nTop, 695, nHeight + 12);
			TextContainsPanel.setBackground(Color.WHITE);
			TextContainsPanel.setVisible(true);

			nTop = 0;
			AdminWise.printToConsole("TextContainsExactPhrase........"
					+ AdminWise.connectorManager.getString("AdminFind.Lbl.TextContainsExactPhrase"));
			lblTextWith.setText(AdminWise.connectorManager.getString("AdminFind.Lbl.TextContainsExactPhrase"));
			// lblTextWith.setBounds(8,18,595, nHeight);
			lblTextWith.setBounds(nLeft, nTop, 241, nHeight);
			lblTextWith.setForeground(Color.red);
			TextContainsPanel.add(lblTextWith);

			lblTextImage.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent me) {
					javax.swing.JPopupMenu menu = new javax.swing.JPopupMenu("");
					JMenuItem subMenu1 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.AllOfTheWords"));
					JMenuItem subMenu2 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.TheExactPhrase"));
					JMenuItem subMenu3 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.AtLeastOneOfWords"));
					JMenuItem subMenu4 = new JMenuItem(
							AdminWise.connectorManager.getString("AdminFind.Menu.AnyOfTheWords"));
					subMenu1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblTextWith.setText(
									AdminWise.connectorManager.getString("AdminFind.Lbl.TextContainsAllWords"));
							selectedTextWith = 0;
						}
					});
					subMenu2.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblTextWith.setText(
									AdminWise.connectorManager.getString("AdminFind.Lbl.TextContainsExactPhrase"));
							selectedTextWith = 1;
						}
					});

					subMenu3.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblTextWith.setText(AdminWise.connectorManager
									.getString("AdminFind.Lbl.TextContainsAtLeastOneOfWords"));
							selectedTextWith = 2;
						}
					});

					subMenu4.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							lblTextWith.setText(
									AdminWise.connectorManager.getString("AdminFind.Lbl.TextContainsAnyOfWords"));
							selectedTextWith = 3;
						}
					});
					menu.add(subMenu1);
					menu.add(subMenu2);
					menu.add(subMenu3);
					menu.add(subMenu4);
					menu.show(lblTextImage, me.getX(), me.getY());
				}
			});

			Cursor handCursorText = new Cursor(Cursor.HAND_CURSOR);
			lblTextImage.setCursor(handCursorText);

			lblTextImage.setIcon(VWImages.findIndexWithRightArrow);
			lblTextImage.setBounds(241, nTop, 40, nHeight);
			TextContainsPanel.add(lblTextImage);

			// TxtContaints.setBounds(270,18, 153, nHeight);//270,20, 250, nHeight
			TxtContaints.setBounds(270, nTop, 197, nHeight);// 270,20, 250, nHeight
			TextContainsPanel.add(TxtContaints);

			generalPanel.add(TextContainsPanel);
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in loadTextContainsPanel :" + e.getMessage());
		}
	}

	private int nodeId = 0, savedSearchID = 0;
	public int selDocTypeCount = 0;
	public int selCreatorCount = 0;

	private void loadDocumentPanel() {
		// nTop = 72;
		nTop = 62 + 45;
		nLeft = 0;
		nWidth = 350;
		// nTop = nTop + nCtrlDiff;
		documentPanelTable.setLayout(null);
		try {
			documentPanelTable.setVisible(true);
			documentPanelTable.setBounds(45, nTop, 695, 320);
			// documentPanelTable.setBounds(nLeft, 80, 695, 295);
			documentPanelTable.setBackground(Color.WHITE);
			Rectangle documentRect = documentPanelTable.getBounds();

			nTop = 5;
			LstDocType.setVisible(true);
			// LblDocType.setText(LBL_DOCTYPES);
			LblDocType.setText(LBL_DOCTYPES_NAME.trim());
			LblDocType.setForeground(java.awt.Color.blue);
			documentPanelTable.add(LblDocType);
			AdminWise.printToConsole(
					"Document type bounds : (" + (nLeft + 1) + "," + (nTop - 1) + "," + 355 + "," + 30 + ")");
			LblDocType.setBounds(nLeft, nTop - 1, 100, 25);// setBounds(nLeft, nTop, 355, 30);
			documentRect = LblDocType.getBounds();
			AdminWise.printToConsole(
					"Document type info bounds : (" + (150) + "," + (documentRect.y) + "," + 279 + "," + 30 + ")");
			LblDocTypeInfo.setBounds(115, documentRect.y, 279, 25);
			// LblDocTypeInfo.setText(LBL_NODOCTYPE_COND);
			LblDocTypeInfo.setForeground(Color.blue);
			LblDocTypeInfo.setText(LBL_NODOCTYPE_COND);
			// documentPanelTable.add(LblDocTypeInfo);

			nWidth = 429;
			AdminWise.printToConsole("Document type list bounds : (" + (nLeft + 1) + "," + (documentRect.y + 25) + ","
					+ (nWidth + 86) + "," + 64 + ")");
			LstDocType.setBounds(nLeft, documentRect.y + 25, nWidth, 80);// .setBounds(nLeft, documentRect.y + 25,
																			// nWidth, 64);
			documentPanelTable.add(LstDocType);

			documentRect = LstDocType.getBounds();
			documentPanelTable.add(BtnDTRefresh);
			// BtnDTRefresh.setBackground(java.awt.Color.white);
			BtnDTRefresh.setBounds(nWidth + 16, documentRect.y - 1, 22, 22);
			BtnDTRefresh.setVisible(true);
			BtnDTRefresh.setToolTipText(BTN_REFRESH_NAME);
			BtnDTRefresh.setActionCommand(BTN_REFRESH_NAME);

			BtnDocTypeIndices.setToolTipText(LBL_DOCTYPEINDICES);
			BtnDocTypeIndices.setActionCommand(BTN_DOCTYPEINDICES_NAME);
			documentPanelTable.add(BtnDocTypeIndices);

			documentRect = BtnDTRefresh.getBounds();
			BtnDocTypeIndices.setBounds(nWidth + 16, (documentRect.y - 1 + documentRect.height) + 7, 22, 22);
			// BtnDocTypeIndices.setBackground(java.awt.Color.white);
			BtnDocTypeIndices.setVisible(true);

			documentRect = BtnDocTypeIndices.getBounds();
			documentPanelTable.add(BtnDeSelectDT);
			// BtnDeSelectDT.setBackground(java.awt.Color.white);
			BtnDeSelectDT.setToolTipText(LBL_DESELECTALL);
			BtnDeSelectDT.setBounds(nWidth + 16, (documentRect.y - 1 + documentRect.height) + 7, 22, 22);
			BtnDeSelectDT.setVisible(true);
			// // Document Type is done.

			// / Creator Section Started...
			AdminWise.printToConsole("nTop before creating author lable  : " + nTop);
			/*
			 * documentRect = columnsList.getBounds(); nTop = nTop +
			 * columnsList.getHeight();
			 */
			nTop = nTop + LstDocType.getHeight() + 25;
			AdminWise.printToConsole("nTop after creating author lable  : " + nTop);
			// LblAuthor.setText(LBL_AUTHORS);
			LblAuthor.setText(LBL_AUTHOR);
			LblAuthor.setForeground(java.awt.Color.blue);
			AdminWise
					.printToConsole("LBL_AUTHORS bounds : (" + (nLeft + 1) + "," + (nTop) + "," + 355 + "," + 30 + ")");
			LblAuthor.setBounds(nLeft, nTop, 100, 25);
			documentPanelTable.add(LblAuthor);
			AdminWise.printToConsole("LBL_AUTHORS Info bounds : (" + 160 + "," + (nTop) + "," + 279 + "," + 30 + ")");
			LblAuthorInfo.setBounds(115, nTop, 279, 25);
			// LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
			LblAuthorInfo.setForeground(java.awt.Color.blue);
			LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
			// documentPanelTable.add(LblAuthorInfo);

			AdminWise.printToConsole("LBL_AUTHORS documentRect" + documentRect.y);
			documentRect = LblAuthor.getBounds();
			LstAuthors.setBounds(nLeft, documentRect.y + 25, nWidth, 80);
			documentPanelTable.add(LstAuthors);

			// / Creator Section Started...
			LstAuthors.setVisible(true);

			documentRect = LstAuthors.getBounds();
			documentPanelTable.add(BtnAuthorRefresh);
			BtnAuthorRefresh.setToolTipText(BTN_REFRESH_NAME);
			BtnAuthorRefresh.setBounds(nWidth + 16, documentRect.y, 22, 22);
			// BtnAuthorRefresh.setBackground(java.awt.Color.white);
			BtnAuthorRefresh.setVisible(true);
			BtnAuthorRefresh.addActionListener(new SymAction());

			documentRect = BtnAuthorRefresh.getBounds();
			documentPanelTable.add(BtnAuthorSelectAll);
			// BtnAuthorSelectAll.setBackground(java.awt.Color.white);
			BtnAuthorSelectAll.setBounds(nWidth + 16, (documentRect.y - 1 + documentRect.height) + 7, 22, 22);
			BtnAuthorSelectAll.setVisible(true);
			BtnAuthorSelectAll.setToolTipText(BTN_SELECTALL_NAME);
			BtnAuthorSelectAll.addActionListener(new SymAction());

			documentRect = BtnAuthorSelectAll.getBounds();
			documentPanelTable.add(BtnAuthorDeSelect);
			// BtnAuthorDeSelect.setBackground(java.awt.Color.white);
			BtnAuthorDeSelect.setToolTipText(BTN_DESELECTALL_NAME);
			BtnAuthorDeSelect.setBounds(nWidth + 16, (documentRect.y - 1 + documentRect.height) + 7, 22, 22);
			BtnAuthorDeSelect.setVisible(true);
			BtnAuthorDeSelect.addActionListener(new SymAction());

			// / Indices Section Started...
			nTop = nTop + LstAuthors.getHeight() + 25;

			LblColumn.setText(LBL_CUSTOMCOLUMN);
			LblColumn.setForeground(java.awt.Color.blue);
			AdminWise.printToConsole("LblColumn bounds : (" + (nLeft + 1) + "," + (nTop) + "," + 355 + "," + 30 + ")");
			LblColumn.setBounds(nLeft, nTop, 230, 25);
			documentPanelTable.add(LblColumn);
			AdminWise.printToConsole(
					"LBL_CUSTOMCOLUMN Info bounds : (" + 160 + "," + (nTop) + "," + 279 + "," + 30 + ")");
			//LblCustomColumnInfo.setBounds(115, nTop, 279, 25);
			// LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
			LblCustomColumnInfo.setForeground(java.awt.Color.blue);
			LblCustomColumnInfo.setText(LBL_COLUMN_COND);
			// documentPanelTable.add(LblCustomColumnInfo);

			AdminWise.printToConsole("columnsList nTop BF------>" + nTop);
			nTop = nTop + LblColumn.getHeight();
			AdminWise.printToConsole("columnsList nTop AF------>" + nTop);
			/*
			 * defaultColumnList = getDefaultColumnList(columnsList);
			 * columnsList.loadData(new Vector(defaultColumnList));
			 */
			nWidth = nWidth - 34;
			columnsList = new VWCheckList();
			AdminWise.printToConsole("1 :" + nTop);
			// LblCustomColumnInfo.setText(getInfoText("Indices",
			// columnsList.getselectedItemsCount()));
			AdminWise.printToConsole("2 :" + nTop);
			AdminWise.printToConsole("Column listbounds :" + nLeft + "," + nTop + "," + nWidth + "," + 80);
			columnsList.setBounds(nLeft, nTop, nWidth, 80);
			AdminWise.printToConsole("3 :" + nTop);
			documentPanelTable.add(columnsList);

			/*---Indices refresh image section started-----------------------------*/
			AdminWise.printToConsole("Column list left :" + nLeft);
			nLeft = nLeft + nWidth + 12;
			AdminWise.printToConsole("nLeft  :" + nLeft);
			documentRect = columnsList.getBounds();
			AdminWise.printToConsole("BtnupClick listbounds :" + (nLeft) + "," + 240 + "," + 22 + "," + 22);
			BtnupClick.setBounds(nLeft, documentRect.y, 22, 22);
			documentPanelTable.add(BtnupClick);
			BtnupClick.addActionListener(new SymAction());
			BtnupClick.setVisible(true);
			// nTop = documentRect.y + BtnupClick.getHeight()+ 5;
			nTop = (documentRect.y + 2) + 22 + 5;
			AdminWise.printToConsole("nTop :" + nTop);
			AdminWise.printToConsole("BtnupClick listbounds :" + (nLeft) + "," + nTop + "," + 22 + "," + 22);
			BtndownClick.setBounds(nLeft, nTop, 22, 22);
			BtndownClick.setVisible(true);
			documentPanelTable.add(BtndownClick);
			BtndownClick.addActionListener(new SymAction());

			documentRect = BtnupClick.getBounds();
			nLeft = nLeft + (int) documentRect.getHeight() + 16;
			AdminWise.printToConsole("columnsList refresh image top------>" + documentRect.y);
			BtnIndicesRefresh.setToolTipText(BTN_REFRESH_NAME);
			BtnIndicesRefresh.setBounds(nLeft, documentRect.y, 22, 22);
			// BtnAuthorRefresh.setBackground(java.awt.Color.white);
			BtnIndicesRefresh.setVisible(true);
			documentPanelTable.add(BtnIndicesRefresh);
			BtnIndicesRefresh.addActionListener(new SymAction());

			documentRect = BtnIndicesRefresh.getBounds();
			// BtnAuthorSelectAll.setBackground(java.awt.Color.white);
			BtnIndicesSelectAll.setBounds(nLeft, (documentRect.y - 1 + documentRect.height) + 7, 22, 22);
			BtnIndicesSelectAll.setVisible(true);
			BtnIndicesSelectAll.setToolTipText(BTN_SELECTALL_NAME);
			documentPanelTable.add(BtnIndicesSelectAll);
			BtnIndicesSelectAll.addActionListener(new SymAction());

			documentRect = BtnIndicesSelectAll.getBounds();
			// BtnAuthorDeSelect.setBackground(java.awt.Color.white);
			BtnIndicesDeSelect.setToolTipText(BTN_DESELECTALL_NAME);
			BtnIndicesDeSelect.setBounds(nLeft, (documentRect.y - 1 + documentRect.height) + 7, 22, 22);
			BtnIndicesDeSelect.setVisible(true);
			documentPanelTable.add(BtnIndicesDeSelect);
			BtnIndicesDeSelect.addActionListener(new SymAction());
			/*----------------------------Indices-----------------------------*/

			SymMouse aSymMouse = new SymMouse();
			LblDocType.addMouseListener(aSymMouse);
			LblAuthor.addMouseListener(aSymMouse);
			LblColumn.addMouseListener(aSymMouse);
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in loadTextContainsPanel :" + e.getMessage());
			AdminWise.printToConsole("Exception in loadTextContainsPanel :" + e);
		}
		generalPanel.add(documentPanelTable);
		// // Creator Section is end
	}

	private void loadIndices() {
		defaultColumnList = getDefaultColumnList(columnsList);
		AdminWise.printToConsole("defaultColumnList :" + defaultColumnList);
		columnsList.loadData(new Vector(defaultColumnList));
	}

	private ArrayList<CustomList> getDefaultColumnList(VWCheckList columnsList) {
		AdminWise.printToConsole("Before calling getDefaultColumns ");
		ArrayList<CustomList> defaultColumnList = columnsList.getDefaultColumns();
		AdminWise.printToConsole("After calling getDefaultColumns " + defaultColumnList);
		return defaultColumnList;
	}

	// --------------------------------------------------------------------------
	private String getInfoText(String info, int count) {
		if (count == 0)
			return "No " + info + " selected";
		if (count == 1)
			return "1 " + info + " selected";
		if (info.equalsIgnoreCase("Indices"))
			return "" + count + " " + info + " selected";
		else
			return "" + count + " " + info + "s" + " selected";
	}

	private boolean generateStaticReports(int staticRepID, String dateFrom, String dateTo, String reportLocation,
			String selectedReport) {
		boolean isReportGenerated = false;
		// if(nodeId==)
		dateTo = dateTo + Util.SepChar + nodeId;
		AdminWise.printToConsole("generateStaticReports after appeding node ID :" + dateTo);
		ArrayList resultSet = AdminWise.gConnector.ViewWiseClient.getStaticReportResult(gCurRoom,
				Integer.parseInt(staticReportID), dateFrom, dateTo);
		AdminWise.printToConsole("getStaticReportResult...." + resultSet);
		if (resultSet != null && resultSet.size() > 1) {
			VWCsvReport report = new VWCsvReport();
			AdminWise.printToConsole("reportLocation :" + reportLocation);
			columnDescription = resultSet.get(0).toString();
			AdminWise.printToConsole("columnDescription :" + columnDescription);
			resultSet.remove(0);
			AdminWise.printToConsole("Resultset :" + resultSet);
			Vector newResultSet = new Vector<>(resultSet);
			isReportGenerated = report.generateCSVReport(reportLocation, newResultSet, columnDescription,
					selectedReport);
		} else {
			clearMessageDialog();
			JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("RouteReport.MSG_NotInGenSerach"));
			isReportGenerated = false;
		}
		return isReportGenerated;
	}

	// --------------------------------------------------------------------------
	private String getInfoText(String frameDate, String toDate) {
		if (frameDate == null || frameDate.equals(""))
			frameDate = null;
		if (toDate == null || toDate.equals(""))
			toDate = null;
		if (frameDate == null && toDate == null)
			return LBL_NODATE_COND;
		if (frameDate != null && toDate == null)
			return "From  [" + frameDate + "]";
		if (frameDate == null && toDate != null)
			return "To  [" + toDate + "]";
		return "From  [" + frameDate + "]  To  [" + toDate + "]";
	}

	public void BtnGenerateJasperReport_actionPerformed(boolean isPreviewReport) {
		/*
		 * AdminWise.printToConsole("Before create object for jasper report class....");
		 * VWRouteJasperReport jsReport = new VWRouteJasperReport();
		 * AdminWise.printToConsole("Before calling generateJasperReport method....");
		 */
		boolean isValidPath = true;
		String jrxmlPath = TxtFieldJrxml.getText().replaceAll("\\s+", "");
		if (jrxmlPath != null &&  !jrxmlPath.equalsIgnoreCase("<Default>") && jrxmlPath.trim().length() > 0) {
			if (jrxmlPath.toLowerCase().endsWith(".jrxml")) {
				if (!new File(jrxmlPath).exists()) {
					isValidPath = false;
				}
			} else {
				isValidPath = false;
			}
		}
		if (!isValidPath) {
			JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("RouteReport.MSG_InvalidTempFormat"));
			return;
		}		
		List<String> selectedColumnList = columnsList.getSelectedItemIds();
		if (selectedColumnList.size() > 10) {
			JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("RouteReport.MSG_IndicesLmt"));
			return;
		}
		
		if (scroll != null)
			scroll.setVisible(false);
		String month = null, day = null, hour = null, totalDate = null, dateFrom = null, dateTo = null;
		if (rbuttondate2.isSelected() == true) {
			month = spinnerTaskInterval1.getValue().toString();
			day = spinnerTaskInterval2.getValue().toString();
			hour = spinnerTaskInterval3.getValue().toString();

			if (month.equals(""))
				month = "0";
			if (day.equals(""))
				day = "0";
			if (hour.equals(""))
				hour = "0";

			if ((Integer.parseInt(month) == 0) && (Integer.parseInt(day) == 0) && (Integer.parseInt(hour) == 0)) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
				return;
			}
			totalDate = "2" + Util.SepChar + month + Util.SepChar + day + Util.SepChar + hour;
			SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			Calendar calendar = Calendar.getInstance();
			AdminWise.printToConsole("dateTo bbefore format = " + calendar.getTime());
			dateTo = format1.format(calendar.getTime());
			AdminWise.printToConsole("dateTo after format = " + dateTo);
			calendar.add(Calendar.MONTH, -Integer.parseInt(month));
			// calendar.add(Calendar.DAY_OF_MONTH, - Integer.parseInt(day));
			calendar.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(day));
			calendar.add(Calendar.HOUR_OF_DAY, -Integer.parseInt(hour));
			AdminWise.printToConsole("From date before format = " + calendar.getTime());

			dateFrom = format1.format(calendar.getTime());
			AdminWise.printToConsole(dateFrom);

		} else if (rbuttondate1.isSelected() == true) {
			dateFrom = TxtMFrom.getSelectedItem().toString();
			dateFrom = dateFrom + " " + simpDate.format(timeSpinnerFrom.getValue());

			dateTo = TxtMTo.getSelectedItem().toString();
			dateTo = dateTo + " " + simpDate.format(timeSpinnerTo.getValue());
			if (dateTo.equals("") || dateFrom.equals("") || dateTo.length() == 0 || dateFrom.length() == 0) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
				return;
			}
			if ((TxtMFrom.getSelectedItem().toString() == "") || (TxtMTo.getSelectedItem().toString() == "")) {
				JOptionPane.showMessageDialog(AdminWise.adminFrame,
						AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation"));
				return;
			} else {
				String pattern = "MM/dd/yyyy";
				totalDate = "1" + Util.SepChar + dateFrom + Util.SepChar + dateTo + Util.SepChar + "0";
				SimpleDateFormat format = new SimpleDateFormat(pattern);
				try {
					Date date1 = format.parse(dateFrom);
					Date date2 = format.parse(dateTo);
					totalDate = "1" + Util.SepChar + getMonthDateDifference(date1, date2) + Util.SepChar + dateTo;
					StringTokenizer st = new StringTokenizer(getMonthDateDifference(date1, date2), "\t");
					String month1 = st.nextToken();
					String day1 = st.nextToken();
					if ((Integer.parseInt(month1) == 0) && (Integer.parseInt(day1) == 0)) {
						JOptionPane.showMessageDialog(AdminWise.adminFrame,
								AdminWise.connectorManager.getString("RouteReport.MSG_TimeperiodValidation1"));
						return;
					}
					AdminWise.printToConsole("totalDate::::" + totalDate);
				} catch (Exception e) {

				}
			}
		}

		try {

			BtnProgressClick.doClick();
			// Thread.sleep(5000);
		} catch (Exception e) {
			/* TODO Auto-generated catch block */
			e.printStackTrace();
		}

		String outputFormat = null;
		if (rButtonXls.isSelected() == true)
			outputFormat = "xls";
		else if (rButtonCsv.isSelected() == true)
			outputFormat = "csv";
		else if (rButtonPdf.isSelected() == true)
			outputFormat = "pdf";
		else
			outputFormat = "html";

		String selectedReport = cmbAvailebReportsList.getSelectedItem().toString();
		String homePath = System.getenv("APPDATA");// loadSaveDialog(parent);
		// String homePath = VWUtil.getHome();
		String documentNameinDTC = null;
		// if (selectedReport != null && selectedReport != "" &&
		// !selectedReport.equals("<Available Reports>"))
		if (selectedReport != null && selectedReport != "" && !selectedReport.equals("<System Reports>")
				&& !selectedReport.equals("<Custom Reports>"))
			documentNameinDTC = selectedReport + "_" + System.currentTimeMillis();
		else
			documentNameinDTC = "GeneralReport_" + System.currentTimeMillis();
		AdminWise.printToConsole("dateFrom xxxxxx :::" + dateFrom);
		AdminWise.printToConsole("dateTo xxxxxx :::" + dateTo);
		String reportName = documentNameinDTC + "." + outputFormat;
		Vector docTypeInfo = new Vector();
		String folderPath = homePath + "\\CV Reports" + "\\";
		AdminWise.printToConsole("homePath >>>" + homePath);
		String reportLocation = folderPath + reportName;
		AdminWise.printToConsole("gCurRoom......" + gCurRoom);
		AdminWise.printToConsole("reportLocation......" + reportLocation);
		AdminWise.printToConsole("selectedReport......" + selectedReport);
		boolean isReportGenerated = false;
		File dtcFilePath = new File(reportLocation);
		dtcFilePath.getParentFile().mkdirs();
		deleteFolderLocation(folderPath);
		AdminWise.printToConsole("staticReportID :" + staticReportID);
		if (staticReportID != null && !staticReportID.equals("0")) {
			try {
				AdminWise.printToConsole("staticReportType...." + staticReportType);
				if (staticReportType != null && staticReportType.equalsIgnoreCase("P")) {
					isReportGenerated = generateStaticReports(Integer.parseInt(staticReportID), dateFrom, dateTo,
							reportLocation, selectedReport);
				} else {

					AdminWise.printToConsole("Before calling generateJasperReport....");
					AdminWise.printToConsole("nodeId......." + nodeId);
					isReportGenerated = AdminWise.gConnector.ViewWiseClient.generateJasperReport(gCurRoom,
							selectedReport, outputFormat, dateFrom, dateTo, reportLocation, reportName, Integer.parseInt(staticReportID), nodeId);
					AdminWise.printToConsole("after calling generateJasperReport...." + isReportGenerated);
					if (!isReportGenerated) {
						isReportGenerated = generateStaticReports(Integer.parseInt(staticReportID), dateFrom, dateTo,
								reportLocation, selectedReport);
					}
				}
			} catch (Exception e) {
				clearMessageDialog();
				isReportGenerated = false;
			}

		} else {
			isReportGenerated = createSearchForJasperReport(selectedReport, outputFormat, dateFrom, dateTo,
					reportLocation, documentNameinDTC, isPreviewReport);

		}
		if (isReportGenerated) {
			generateDocumentForDTC(reportLocation, reportName, folderPath, documentNameinDTC, isReportGenerated);
		} else {
			clearMessageDialog();
		}
		deleteFolderLocation(folderPath);
	}

	private void clearMessageDialog() {
		processDialog.setVisible(false);
		processDialog.dispose();
		statusLabel.setText("		" + s1 + s1 + "		Generating workflow report. Please wait...");
	}

	private void showProcessMessageDialog(String message) {
		try {
			statusLabel.setText("		" + s1 + s1 + "	" + message);
			BtnProgressClick.doClick();
			Thread.sleep(500);
			processDialog.setVisible(false);
			processDialog.dispose();
			statusLabel.setText("		" + s1 + s1 + "		Generating workflow report. Please wait...");
			// Thread.sleep(5000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean createSearchForJasperReport(String selectedReport, String outputFormat, String dateFrom,
			String dateTo, String reportLocation, String documentNameinDTC, boolean isPreviewReport) {
		boolean isReportGenerated = false;
		Search search = new Search(0, "");
		search.setTemp(true);
		AdminWise.printToConsole("nodeId......." + nodeId);
		search.setNodeId(nodeId);
		AdminWise.printToConsole("savedSearchID bf createsearch......." + savedSearchID);
		search.setId(savedSearchID);

		// -----------------------Code added by srikanth for saving into search table
		String pattern = "MM/dd/yyyy";
		SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern);
		try {
			AdminWise.printToConsole("before parse fromDate >>>>" + dateFrom);
			AdminWise.printToConsole("before parse toDate >>>> >>>>" + dateTo);
			Date date1 = dateFormatter.parse(dateFrom);
			Date date2 = dateFormatter.parse(dateTo);
			AdminWise.printToConsole("after parse fromDate >>>>" + date1);
			AdminWise.printToConsole("after parse toDate >>>>" + date2);
			dateFrom = dateFormatter.format(date1);
			dateTo = dateFormatter.format(date2);
			AdminWise.printToConsole("after parse dateFrom >>>>" + dateFrom);
			AdminWise.printToConsole("after parse dateTo >>>>" + dateTo);
			search.setCreationDate1(dateFrom);
			search.setCreationDate2(dateTo);
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in setting fromdate and todate..." + e.getMessage());
		}
		columnDescription = null;
		
		int searchId = SaveSearchActionForJasperReport(search);
		AdminWise.printToConsole("savedSearchID af createsearch......." + searchId);
		if (searchId == 0)
			return false;
		savedSearchID = searchId;
		search.setId(searchId);
		Vector docs = new Vector();
		search.setWorkflowReport(true);
		search.setRowNo(1000);
		int resultCount = AdminWise.gConnector.ViewWiseClient.find(gCurRoom, search, true);
		AdminWise.printToConsole("Search resultCount :" + resultCount);

		if (resultCount > 0) {
			search.setLastDocId(0);
			Vector<String> resultSet = new Vector<String>();
			int result = AdminWise.gConnector.ViewWiseClient.getCustomisedSearchResult(gCurRoom, search, docs, 0, true,
					2, resultSet);
			AdminWise.printToConsole("getCustomisedSearchResult resultset : " + resultSet);
			if (resultSet.size() > 0) {
				VWCsvReport report = new VWCsvReport();
				AdminWise.printToConsole("Before calling generateCSVReport....");
				AdminWise.printToConsole("reportLocation :" + reportLocation);
				AdminWise.printToConsole("selectedReport :" + selectedReport);

				String JrxmlPath = TxtFieldJrxml.getText().replaceAll("\\s+", "");

				/*String previewSelected = lblPreviewclicked.getText();
				boolean isPreviewSelected = false;
				if (previewSelected.equalsIgnoreCase("true")) {
					isPreviewSelected = true;
				}*/
				AdminWise.printToConsole("isPreviewReport >>>>> "+isPreviewReport);
				AdminWise.printToConsole("text field value" + JrxmlPath + "length of the txt:::" + JrxmlPath.length());

				AdminWise.printToConsole("its working fine and text is " + TxtFieldJrxml.getText() + "testing");
				String reportName = documentNameinDTC + "." + outputFormat;
				isReportGenerated = AdminWise.gConnector.ViewWiseClient.generateDynamicJasperReport(gCurRoom,
						reportLocation, resultSet, columnDescription, selectedReport, reportName,
						TxtFieldJrxml.getText(), isPreviewReport, outputFormat);
				AdminWise.printToConsole("is jasper report generated :" + isReportGenerated);
				if (!isReportGenerated) {
					isReportGenerated = report.generateCSVReport(reportLocation, resultSet, columnDescription,
							selectedReport);
				}
				AdminWise.printToConsole("After calling generateCSVReport...." + isReportGenerated);
			}
		} else {
			clearMessageDialog();
			JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("RouteReport.MSG_NotInGenSerach"));
			isReportGenerated = false;
		}
		return isReportGenerated;
	}

	private int SaveSearchActionForJasperReport(Search search) {
		int searchID = 0;
		try {
			search.setIndex((TxtFieldIndex.getText() == null ? "" : TxtFieldIndex.getText()));
			search.setContents((TxtContaints.getText() == null ? "" : TxtContaints.getText()));

			search.setFindResultWith(selectedIndexWith);

			AdminWise.printToConsole("Index lable value....." + search.getIndex());
			AdminWise.printToConsole("index text box value....." + search.getContents());

			AdminWise.printToConsole("FromDate....." + TxtMFrom.getText());
			AdminWise.printToConsole("toDate....." + TxtMTo.getText());

			int count = LstDocType.getItemsCount();
			Vector<DocType> searcDocTypes = new Vector<DocType>();
			AdminWise.printToConsole("searcDocTypes Size -> :" + count);
			try {
				for (int i = 0; i < count; i++) {
					AdminWise.printToConsole("getItemIsCheck ->" + LstDocType.getItemIsCheck(i));
					if (LstDocType.getItemIsCheck(i)) {
						searcDocTypes.add((DocType) LstDocType.getItem(i));
					}
				}
			} catch (Exception e) {
				AdminWise.printToConsole("Exception in getSelectedDocType :" + e.getMessage());
			}
			AdminWise.printToConsole("searcDocTypes Size ->" + searcDocTypes.size());
			search.setDocTypes(searcDocTypes);
			count = LstAuthors.getItemsCount();
			AdminWise.printToConsole("Authors List Size ->" + count);
			Vector searchAuthors = new Vector();
			try {
				for (int i = 0; i < count; i++) {
					AdminWise.printToConsole("getItemIsCheck ->" + LstAuthors.getItemIsCheck(i));
					if (LstAuthors.getItemIsCheck(i))
						searchAuthors.add((Creator) LstAuthors.getItem(i));
				}
			} catch (Exception e) {
				AdminWise.printToConsole("Exception in getSelectedAuthors :" + e.getMessage());
			}
			AdminWise.printToConsole("searchAuthors size ->" + searchAuthors.size());
			search.setCreators(searchAuthors);
			deleteUnselectedDTIndicesForReport(LstDocType.getSelectedItemIds());
			if (searchDTIndices != null)
				search.setConds(searchDTIndices);

			AdminWise.printToConsole("searcDocTypes xxx->" + searcDocTypes.size());
			for (int i = 0; i < searcDocTypes.size(); i++)
				AdminWise.printToConsole("searcDocTypes ->" + ((DocType) searcDocTypes.get(i)).getId());
			AdminWise.printToConsole("Before calling createSearch......");
			searchID = VWFindConnector.createSearch(search, gCurRoom);
			AdminWise.printToConsole("After calling createSearch......" + search.getId());
			if (searchID == 0) {
				VWMessage.showMessage(this, VWMessage.ERR_SEARCH_NOTSAVE);
			} else {
				String settingsValue = null;
				count = columnsList.getItemsCount();
				try {
					if (count > 0) {
						for (int i = 0; i < count; i++) {
							AdminWise.printToConsole("getItemIsCheck ->" + columnsList.getItemIsCheck(i));
							if (columnsList.getItemIsCheck(i)) {
								if (settingsValue == null) {
									settingsValue = String.valueOf(((CustomList) columnsList.getItem(i)).getId());
									columnDescription = ((CustomList) columnsList.getItem(i)).getName();
								} else {
									settingsValue = settingsValue + "\t"
											+ String.valueOf(((CustomList) columnsList.getItem(i)).getId());
									columnDescription = columnDescription + "\t"
											+ ((CustomList) columnsList.getItem(i)).getName();
								}
							}
						}
						AdminWise.printToConsole("selected customColumn ->" + settingsValue);
						AdminWise.printToConsole("Selected Column Description ->" + columnDescription);
						if (settingsValue != null)
							AdminWise.gConnector.ViewWiseClient.addCustomList(gCurRoom, 2, "1", settingsValue,
									searchID);
						if (columnDescription != null)
							AdminWise.gConnector.ViewWiseClient.addCustomList(gCurRoom, 2, "2", columnDescription,
									searchID);
					}
				} catch (Exception e) {
					AdminWise.printToConsole("Exception in getSelectedCustomColumn :" + e.getMessage());
				}
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in SaveSearchActionForJasperReport :" + e.getMessage());
		}
		return searchID;
	}

	// --------------------------------------------------------------
	private void loadDocTypeFields() {
		AdminWise.printToConsole("loadDocTypeFields.....");

		LstDocType.removeItems();
		Vector docTypes = VWFindConnector.getDocTypes(gCurRoom);
		AdminWise.printToConsole("loadDocTypeFields types :" + docTypes);
		if (docTypes == null || docTypes.size() == 0) {
			docTypes.add(new DocType(0, LBL_NODATAFOUND));
		}
		LstDocType.loadData(docTypes);
		// LstDocType.loadDocumentTypeList(docTypes);
	}

	// --------------------------------------------------------------
	private void loadAuthorFields() {
		LstAuthors.removeItems();
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		if (room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		gCurRoom = room.getId();
		Vector data = new Vector();
		AdminWise.gConnector.ViewWiseClient.getCreators(gCurRoom, data);
		if (data == null || data.size() == 0) {
			data.add(new Creator(0, LBL_NOUSERFOUND));
		}
		LstAuthors.loadData(data);
		// LstAuthors.loadAuthorsList(data);
	}

	/*
	 * void BtnWFReportLocation_actionPerformed(java.awt.event.ActionEvent event) {
	 * JFileChooser fileChooser = new JFileChooser();
	 * fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); int
	 * returnVal = fileChooser.showOpenDialog(this);
	 * if(returnVal==JFileChooser.APPROVE_OPTION){
	 * this.txtReportLocation.setText(fileChooser.getSelectedFile().toString()); } }
	 */

	public void BtnDocTypeIndicesForReport_actionPerformed(java.awt.event.ActionEvent event) {
		AdminWise.printToConsole("docTypeIndicesDlg :" + docTypeIndicesDlg);
		if (docTypeIndicesDlg != null) {
			docTypeIndicesDlg.loadTabData(gCurRoom);
			// set the data into the document indices
			Vector vect = docTypeIndicesDlg.Table.getData();
			docTypeIndicesDlg.Table.clearData();
			List selectedDT = LstDocType.getSelectedItems();

			Vector resultVector = new Vector();
			StringBuffer selectedDocType = new StringBuffer();
			if (selectedDT.size() > 0) {
				for (int index = 0; index < selectedDT.size(); index++) {
					selectedDocType.append("\t" + selectedDT.get(index) + "\t");
				}
				for (int index = 0; index < vect.size(); index++) {
					String docType = ((SearchCond) vect.get(index)).getDocType();
					if (selectedDocType.indexOf("\t" + docType + "\t") != -1) {
						resultVector.add(vect.get(index));
					}
				}
			}
			deleteUnselectedDTIndicesForReport(selectedDT);
			docTypeIndicesDlg.loadDocTypeData(selectedDT);
			docTypeIndicesDlg.Table.insertData(searchDTIndices);

			// To retain the condition to document type indices table.
			docTypeIndicesDlg.Table.insertData(resultVector);
			docTypeIndicesDlg.setVisible(true);
		} else {
			docTypeIndicesDlg = new VWDTIndicesDlg(AdminWise.adminFrame);
			/// docTypeIndicesDlg.setCaller(caller);
			docTypeIndicesDlg.loadTabData(gCurRoom);
			docTypeIndicesDlg.Table.clearData();
			List selectedDT = LstDocType.getSelectedItems();
			deleteUnselectedDTIndicesForReport(selectedDT);
			docTypeIndicesDlg.loadDocTypeData(selectedDT);
			docTypeIndicesDlg.Table.insertData(searchDTIndices);
			docTypeIndicesDlg.setVisible(true);
		}
		if (docTypeIndicesDlg.cancelFlag)
			return;
		LstDocType.setCheckItems(docTypeIndicesDlg.LstDocType.getSelectedItems());
		searchDTIndices = docTypeIndicesDlg.Table.getData();
	}

	public void deleteUnselectedDTIndicesForReport(List selectedDT) {
		if (selectedDT == null || selectedDT.size() == 0 || searchDTIndices == null) {
			searchDTIndices = null;
			return;
		}
		int tableCount = searchDTIndices.size();
		int listCount = selectedDT.size();
		Vector newSearchDTIndices = new Vector();
		for (int i = 0; i < tableCount; i++) {

			int docTypeId = ((SearchCond) searchDTIndices.get(i)).getDocTypeId();
			if (selectedDT.contains(String.valueOf(docTypeId)))
				newSearchDTIndices.add(searchDTIndices.get(i));
		}
		searchDTIndices = newSearchDTIndices;
	}

	private void loadGeneralSearchInfo(int searchID) {
		AdminWise.printToConsole("loadGeneralSearchInfo searchID >>>>>" + searchID);
		Search searchInfo = VWFindConnector.getSearchInfo(searchID, gCurRoom);
		if (searchInfo == null)
			return;
		AdminWise.printToConsole("searchInfo.getIndex() :" + searchInfo.getIndex());
		nodeId = searchInfo.getNodeId();
		AdminWise.printToConsole("nodeId >>>>>" + nodeId);
		if (nodeId != 0) {
			TxtFieldFindLocation.setText(getLocationPath(gCurRoom, searchInfo.getNodeId()));
		}
		TxtFieldIndex.setText(searchInfo.getIndex());

		// cmbFindIndexWith.setSelectedIndex(searchInfo.getFindResultWith());
		if (searchInfo.getFindResultWith() == 0)
			lblIndexWith.setText(VWFindDlg.resManager.getString("CVFind.Lbl.IdxContainsAllWords"));
		else if (searchInfo.getFindResultWith() == 1)
			lblIndexWith.setText(VWFindDlg.resManager.getString("CVFind.Lbl.IdxContainsExactPhrase"));
		else if (searchInfo.getFindResultWith() == 2)
			lblIndexWith.setText(VWFindDlg.resManager.getString("CVFind.Lbl.IdxContainsAtLeastOneOfWords"));

		TxtContaints.setText(searchInfo.getContents());

		AdminWise.printToConsole("searchInfo.getCreationDate1() ::::" + searchInfo.getCreationDate1());
		TxtMFrom.setText(searchInfo.getCreationDate1());
		AdminWise.printToConsole("searchInfo.getCreationDate2() ::::" + searchInfo.getCreationDate2());
		TxtMTo.setText(searchInfo.getCreationDate2());
		LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
		if (searchInfo.getSearchAuthorCount() > 0) {
			if (LstAuthors.getItemsCount() == 0)
				loadAuthorFields();
			LstAuthors.setCheckItems(searchInfo.getCreators());
			LblAuthorInfo.setText(getInfoText(LBL_CREATOR, LstAuthors.getselectedItemsCount()));
		} else {
			LstAuthors.deSelectAllItems();
		}
		LblDocTypeInfo.setText(LBL_NODOCTYPE_COND);
		if (searchInfo.getSearchDTCount() > 0) {
			if (LstDocType.getItemsCount() == 0)
				loadDocTypeFields();
			LstDocType.setCheckItems(searchInfo.getDocTypes());
			LblDocTypeInfo.setText(getInfoText(LBL_DOCUMENTTYPE, LstDocType.getselectedItemsCount()));
			List<DocType> selectedList = LstDocType.getSelectedItems();
			VWRouteReportPanel.getSearchDocTypeIndices(selectedList);
		} else {
			LstDocType.deSelectAllItems();
		}
		if (searchInfo.getSearchDTCondCount() > 0)
			searchDTIndices = searchInfo.getConds();
		Vector<String> customColumnList = new Vector<String>();
		AdminWise.printToConsole("Before getCustomList call..." + searchID);
		customColumnList = AdminWise.gConnector.ViewWiseClient.getCustomList(gCurRoom, 2, null, searchID);
		AdminWise.printToConsole("return value of getCustomList call..." + customColumnList);
		if (customColumnList != null && customColumnList.size() > 0) {
			AdminWise.printToConsole("customColumnList.get(0)).toString() :: " + (customColumnList.get(0)).toString());
			String settingValue[] = (customColumnList.get(0)).toString().split("\t");
			AdminWise.printToConsole("settingValue :" + settingValue);
			columnsList.setCheckItems(settingValue);
			LblCustomColumnInfo.setText(getInfoText("Indices", columnsList.getselectedItemsCount()));
		}
	}

	private String getLocationPath(int sid, int nodeId) {
		Vector nodes = new Vector();
		String docPath = AdminWise.adminPanel.roomPanel.getSelectedRoom() + " \\";
		if (nodeId == 0)
			return docPath;
		AdminWise.gConnector.ViewWiseClient.getNodeParents(sid, new Node(nodeId), nodes);
		if (nodes == null || nodes.size() == 0)
			return docPath;
		int count = nodes.size();
		for (int i = count - 1; i >= 0; i--)
			docPath += ((Node) nodes.get(i)).getName() + "\\";
		return docPath.substring(0, docPath.length() - 1);
	}

	public static void getSearchDocTypeIndices(List<DocType> docTypeList) {
		AdminWise.printToConsole("inside getSearchDocTypeIndices......");
		List<String> selectedColumnList = columnsList.getSelectedItemIds();

		columnsList.removeItems();
		AdminWise.printToConsole("docTypeList in  getSearchDocTypeIndices......" + docTypeList);
		ArrayList<CustomList> newCustomList = new ArrayList<CustomList>();
		newCustomList.addAll(defaultColumnList);
		if (docTypeList != null && docTypeList.size() > 0) {
			AdminWise.printToConsole("docTypeList.size() in  getSearchDocTypeIndices......" + docTypeList.size());
			ArrayList<CustomList> customColumnList = AdminWise.gConnector.ViewWiseClient.getDocTypeIndices(gCurRoom,
					new ArrayList<DocType>(docTypeList));
			AdminWise.printToConsole("customColumnList......" + customColumnList);
			if (customColumnList != null) {
				AdminWise.printToConsole("customColumnList......" + customColumnList.size());
				newCustomList.addAll(customColumnList);
			}
		}
		AdminWise.printToConsole("newCustomList :" + newCustomList);
		columnsList.loadData(new Vector(newCustomList));

		AdminWise.printToConsole("selectedColumnList :" + selectedColumnList);
		if (selectedColumnList != null && selectedColumnList.size() > 0) {
			AdminWise.printToConsole("selectedColumnList.size()  :" + selectedColumnList.size());
			String checkedItems[] = new String[selectedColumnList.size()];
			checkedItems = selectedColumnList.toArray(checkedItems);
			AdminWise.printToConsole("checkedItems :" + checkedItems);
			columnsList.setCheckItems(checkedItems);
		}
	}

	public static void getSearchDocTypeIndices(DocType docType) {
		AdminWise.printToConsole("inside getSearchDocTypeIndices......");
		AdminWise.gConnector.ViewWiseClient.getDocTypeIndices(gCurRoom, docType);
		AdminWise.printToConsole("docType......" + docType);
		if (docType != null) {
			Vector<Index> indicesList = docType.getIndices();
			loadDocTypeIndices(indicesList);
		}
	}

	public static void loadDocTypeIndices(Vector<Index> indicesList) {
		ArrayList<CustomList> customIndicesList = null;
		CustomList customList = null;
		Index indices = null;
		AdminWise.printToConsole("indicesList......" + indicesList);
		if (indicesList != null) {
			customIndicesList = new ArrayList<CustomList>();
			AdminWise.printToConsole("indicesList.size()......" + indicesList.size());
			for (int listIndex = 0; listIndex < indicesList.size(); listIndex++) {
				indices = indicesList.get(listIndex);
				customList = new CustomList(indices.getId(), indices.getName(), String.valueOf(indices.getType()));
				customIndicesList.add(customList);
			}
		}
		AdminWise.printToConsole("customIndicesList......" + customIndicesList);
		ArrayList<CustomList> newCustomList = defaultColumnList;
		if (customIndicesList != null) {
			AdminWise.printToConsole("customIndicesList.size()......" + customIndicesList.size());
			newCustomList.addAll(customIndicesList);
		}
		columnsList.loadData(new Vector(newCustomList));
	}

	// ------------------------------------------------------------------------------
	class SymMouse extends java.awt.event.MouseAdapter {
		public void mouseClicked(java.awt.event.MouseEvent event) {
			Object object = event.getSource();
			if (object == LblDocType)
				listItem_mouseClicked(event, "DocType");
			else if (object == LblAuthor)
				listItem_mouseClicked(event, "Author");
			else if (object == LblColumn)
				listItem_mouseClicked(event, "CustomColumn");
		}
	}

	void listItem_mouseClicked(java.awt.event.MouseEvent event, String type) {
		AdminWise.printToConsole("type >>> " + type);
		if (type.equalsIgnoreCase("DocType")) {
			// LblDocTypeInfo.setVisible(true);
			LblDocTypeInfo.setText(getInfoText(LBL_DOCUMENTTYPE, LstDocType.getselectedItemsCount()));
		} else if (type.equalsIgnoreCase("Author")) {
			// LblAuthorInfo.setVisible(false);
			LblAuthorInfo.setText(getInfoText(LBL_CREATOR, LstAuthors.getselectedItemsCount()));
		} else if (type.equalsIgnoreCase("CustomColumn")) {
			LblCustomColumnInfo.setText(getInfoText(LBL_CUSTOMCOLUMN, columnsList.getselectedItemsCount()));
		}
	}
	// ------------------------------------------------------------------------------

	public void clearGeneralSettings() {
		LstDocType.deSelectAllItems();
		loadIndices();
		LstAuthors.deSelectAllItems();
	}

	public void clearGeneralReportFields() {
		try {
			nodeId = 0;
			AdminWise.printToConsole("inside clearGeneralReportFields.......");
			TxtFieldJrxml.setText("");
			TxtFieldIndex.setText("");
			lblIndexWith.setText(AdminWise.connectorManager.getString("AdminFind.Lbl.IdxContainsExactPhrase"));
			TxtContaints.setText("");
			selectedIndexWith = 0;

			clearGeneralSettings();

			// txtReportLocation.setText("");
			rButtonHtml.setSelected(true);
			rButtonCsv.setSelected(false);
			rButtonPdf.setSelected(false);
			rButtonXls.setSelected(false);
			if (scroll != null)
				scroll.setVisible(false);
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			String strDate = sdf.format(date);
			AdminWise.printToConsole("strDate 3 :::: " + strDate);
			TxtMFrom.setText(strDate);
			TxtMTo.setText(strDate);

			rbuttondate1.setSelected(true);
			TxtMFrom.setEnabled(true);
			TxtMTo.setEnabled(true);
			timeSpinnerFrom.setEnabled(true);
			timeSpinnerTo.setEnabled(true);
			lblMonth.setEnabled(false);
			lblDay.setEnabled(false);
			lblHour.setEnabled(false);
			spinnerTaskInterval1.setEnabled(false);
			spinnerTaskInterval2.setEnabled(false);
			spinnerTaskInterval3.setEnabled(false);
			rbuttondate2.setSelected(false);

			Date spinnerdate = new Date();
			SpinnerDateModel sm = new SpinnerDateModel(spinnerdate, null, null, Calendar.HOUR_OF_DAY);
			JSpinner timeSpinnerFrom = new JSpinner(sm);
			JSpinner.DateEditor datespinner = new JSpinner.DateEditor(timeSpinnerFrom, "hh:mm:ss a");
			timeSpinnerFrom.setEditor(datespinner);

			SpinnerDateModel sm1 = new SpinnerDateModel(spinnerdate, null, null, Calendar.HOUR_OF_DAY);
			JSpinner timeSpinnerTo = new JSpinner(sm1);
			JSpinner.DateEditor datespinner1 = new JSpinner.DateEditor(timeSpinnerTo, "hh:mm:ss a");
			timeSpinnerTo.setEditor(datespinner1);

			spinnerTaskInterval1.setValue(new Integer(0));
			spinnerTaskInterval2.setValue(new Integer(0));
			spinnerTaskInterval3.setValue(new Integer(0));
			TxtFieldFindLocation.setText("");
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in clearGeneralReportFields :" + e.getMessage());
		}
	}

	public void clearListBoxSelectedItems() {
		LstDocType.deSelectAllItems();
		LblDocTypeInfo.setText(getInfoText(LBL_CREATOR, LstDocType.getselectedItemsCount()));
		LstAuthors.deSelectAllItems();
		LblAuthorInfo.setText(getInfoText(LBL_CREATOR, LstAuthors.getselectedItemsCount()));
		columnsList.deSelectAllItems();
		LblCustomColumnInfo.setText(getInfoText("Indices", columnsList.getselectedItemsCount()));
	}

	// --------------------------------------------------------------
	public void BtnAuthorRefresh_workflowActionPerformed(java.awt.event.ActionEvent event) {
		List selectedAuthors = LstAuthors.getSelectedItemIds();
		loadAuthorFields();
		LstAuthors.setCheckItems(selectedAuthors);
	}

	// --------------------------------------------------------------
	public void BtnDTRefresh_workflowActionPerformed(java.awt.event.ActionEvent event) {
		List selectedDT = LstDocType.getSelectedItemIds();
		loadDocTypeFields();
		LstDocType.setCheckItems(selectedDT);
	}

	// --------------------------------------------------------------
	public void BtnIndicesRefresh_workflowActionPerformed(java.awt.event.ActionEvent event) {
		/*
		 * List selectedDT=columnsList.getSelectedItemIds();
		 * AdminWise.printToConsole("selectedDT :"+selectedDT); loadIndices();
		 * columnsList.setCheckItems(selectedDT);
		 */
		List<DocType> selectedList = LstDocType.getSelectedItems();
		// VWRouteReportPanel.getSearchDocTypeIndices((DocType)listItem);
		getSearchDocTypeIndices(selectedList);
	}

	// --------------------------------------------------------------
	public void BtnIndicesDeSelect_actionPerformed(java.awt.event.ActionEvent event) {
		AdminWise.printToConsole("before calling indices deselect");
		columnsList.deSelectAllItems();
	}

	// --------------------------------------------------------------
	public void BtnIndicesSelectAll_actionPerformed(java.awt.event.ActionEvent event) {
		AdminWise.printToConsole("Before calling setCheckAllItems....");
		columnsList.setCheckAllItems(0);
	}

	// --------------------------------------------------------------
	public void BtnDocumentDeSelectDT_actionPerformed(java.awt.event.ActionEvent event) {
		LstDocType.deSelectAllItems();
	}

	// --------------------------------------------------------------
	public void BtnAuthorDeSelect_actionPerformed(java.awt.event.ActionEvent event) {
		LstAuthors.deSelectAllItems();
	}

	// --------------------------------------------------------------
	public void BtnAuthorSelectAll_actionPerformed(java.awt.event.ActionEvent event) {
		LstAuthors.setCheckAllItems(0);
	}

	// --------------------------------------------------------------
	/*** End of CV10.2 code merges *****/

	public int CURRENT_MODE = 0;
	private static int gCurRoom = 0;
	int docTypeId = 0;
	private boolean UILoaded = false;
	public Session session = null;

	VWPicturePanel picPanel = new VWPicturePanel(TAB_ROUTE_REPORT_NAME, VWImages.DocRouteImage);
	JPanel VWNotificationSetting = new JPanel();
	JPanel PanelCommand = new JPanel();
	JPanel PanelRouteSetting = new JPanel();
	JPanel lablePanel = new JPanel();
	JPanel PanelTable = new JPanel();
	JPanel panleActions = new JPanel();
	JPanel panleAvailable = new JPanel();
	JPanel panleUserGroup = new JPanel();

	/** SIDBI - WorkFlow Report Enhancemet **/
	JPanel generalPanel = new JPanel();
	JPanel routePanel = new JPanel();
	JPanel cabinetPanel = new JPanel();
	JPanel datePanel = new JPanel();
	JPanel reportInfoPanel = new JPanel();
	/*************************************/

	JPanel panleTimePeriod = new JPanel();
	Vector reportResult = new Vector();
	SimpleDateFormat simpDate = new SimpleDateFormat("hh:mm:ss a");
	// Vector finalResult=new Vector();
	public static JPanel actionsWorkFlowPanel = new JPanel();
	public static JPanel availableWorkFlowPanel = new JPanel();
	public static JPanel userGroupPanel = new JPanel();
	VWLinkedButton BtnNewReport = new VWLinkedButton(1);
	VWLinkedButton BtnGenerateReport = new VWLinkedButton(1);
	VWLinkedButton BtnGenerateCVReport = new VWLinkedButton(1);
	VWLinkedButton BtnPreviewReport = new VWLinkedButton(1);
	VWLinkedButton BtnUpdate = new VWLinkedButton(1);
	VWLinkedButton BtnDelete = new VWLinkedButton(1);
	// JTable routeReportTable=new JTable();
	JLabel lblModule = new JLabel();
	JLabel lblName = new JLabel();
	JLabel lblAction = new JLabel();
	JLabel lblAvailableReport = new JLabel();
	JLabel lblWorkFlow = new JLabel();
	JLabel lblAvailableWorkFlow = new JLabel();
	JLabel lblWFReportName = new JLabel();
	JLabel lblUserGroup = new JLabel();
	JLabel lblTimePeriod = new JLabel();
	JLabel lblTask = new JLabel();
	JLabel lbllast = new JLabel("In the Last");
	JLabel lblMonth = new JLabel("Mon(s)");
	JLabel lblDay = new JLabel("Day(s)");
	JLabel lblHour = new JLabel("Hr(s)");
	/*
	 * JTextField txtMonth=new JTextField(); JTextField txtDay=new JTextField();
	 * JTextField txtHour=new JTextField();
	 */
	JLabel lblPreviewclicked = new JLabel();
	JTextField txtReportName = new JTextField();
	JLabel lblBetween = new JLabel("Between: ");
	JLabel lblStartDate = new JLabel("From: ");
	JLabel lblEndDate = new JLabel("To: ");

	VWDateComboBox TxtMFrom = new VWDateComboBox();
	VWDateComboBox TxtMTo = new VWDateComboBox();
	JRadioButton rbuttondate1 = new JRadioButton();
	JRadioButton rbuttondate2 = new JRadioButton();
	javax.swing.JTextField TxtFieldFindLocation = new javax.swing.JTextField();
	VWLinkedButton BtnNewFindLocation = new VWLinkedButton(1);
	JLabel JLblLocation = new JLabel();

	SpinnerDateModel sm = null;
	JSpinner.DateEditor datespinner = null;
	SpinnerDateModel sm1 = null;
	Date spinnerdate = null;
	JSpinner.DateEditor datespinner1 = null;
	JSpinner spinnerTaskInterval1 = null;
	JSpinner spinnerTaskInterval2 = null;
	JSpinner spinnerTaskInterval3 = null;

	JSpinner timeSpinnerFrom = null;
	JSpinner timeSpinnerTo = null;
	// JSpinner spinnerTaskInterval4 = null;

	String s1 = new String(new char[] { 32 });
	String s2 = new String(new char[] { 9 });
	JLabel statusLabel = new JLabel("		" + s1 + s1 + "		Generating workflow report. Please wait...");

	VWComboBox cmbAvailebReportsList = new VWComboBox();
	VWComboBox cmbModule = new VWComboBox();
	VWComboBox cmbName = new VWComboBox();
	JPanel tablePanel = new JPanel();
	JTable routeReportTable1 = null;
	JTable routeReportTable2 = null;
	JPopupMenu popupMenu = null;
	JScrollPane scroll = null;
	JScrollPane SPTable = new JScrollPane();
	VWRouteReportTable Table = new VWRouteReportTable();
	Lock lock1 = new ReentrantLock();
	JDialog processDialog = new JDialog();
	JButton BtnProgressClick = new JButton();
	// JButton BtnupClick = new JButton(VWImages.upIcon);
	JButton BtnupClick = new JButton(VWImages.upIcon);
	// JButton BtndownClick = new JButton(VWImages.downIcon);
	JButton BtndownClick = new JButton(VWImages.downIcon);
	JMenuItem menuGenerateReport = new JMenuItem();
	String languageLocale = AdminWise.getLocaleLanguage();
	VWRouteReportSettingsPanel settingsPanel = new VWRouteReportSettingsPanel();
	JComboBox cmbReportType = new JComboBox();
	JLabel lblReportType = new JLabel();
	// JLabel lblReportLocation = new JLabel();
	JLabel lblOutputformat = new JLabel();
	JRadioButton rButtonPdf = new JRadioButton();
	JRadioButton rButtonHtml = new JRadioButton();
	JRadioButton rButtonXls = new JRadioButton();
	JRadioButton rButtonCsv = new JRadioButton();

	JLabel lblPdf = new JLabel();
	JLabel lblHtml = new JLabel();
	JLabel lblXls = new JLabel();
	JLabel lblCsv = new JLabel();

	javax.swing.JPanel IndexContainsPanel = new javax.swing.JPanel();
	public static JLabel lblIndexWith = new JLabel();
	public JLabel lblIndexImage = new JLabel();

	// JRXML Location
	javax.swing.JPanel ImgJrxmlPathPanel = new javax.swing.JPanel();
	public static JLabel lblImgJrxmlPath = new JLabel("Template Format:");
	// Location to get the path of JRXML Template
	JButton JrxmlBtn = new JButton();
	final JLabel JrxmlLocation = new JLabel();

	javax.swing.JPanel TextContainsPanel = new javax.swing.JPanel();
	public static JLabel lblTextWith = new JLabel();
	public JLabel lblTextImage = new JLabel();
	javax.swing.JTextField TxtFieldJrxml = new javax.swing.JTextField();
	javax.swing.JTextField TxtFieldIndex = new javax.swing.JTextField();
	javax.swing.JTextField TxtContaints = new javax.swing.JTextField();

	javax.swing.JPanel documentPanelTable = new javax.swing.JPanel();
	VWCheckList LstDocType = new VWCheckList();
	JLabel LblDocTypeInfo = new JLabel();

	JLabel LblDocType = new JLabel();
	JLabel LblDocument = new JLabel();
	JLabel LblAuthor = new JLabel();
	JLabel LblColumn = new JLabel();

	VWLinkedButton BtnDTRefresh = new VWLinkedButton(VWImages.RefreshIcon);

	VWLinkedButton BtnDeSelectDT = new VWLinkedButton(VWImages.DeselectAllIcon);

	VWLinkedButton BtnDocTypeIndices = new VWLinkedButton(VWImages.DocTypesIcon);

	VWLinkedButton BtnAuthorRefresh = new VWLinkedButton(VWImages.RefreshIcon);

	VWLinkedButton BtnIndicesRefresh = new VWLinkedButton(VWImages.RefreshIcon);
	VWLinkedButton BtnIndicesDeSelect = new VWLinkedButton(VWImages.DeselectAllIcon);
	VWLinkedButton BtnIndicesSelectAll = new VWLinkedButton(VWImages.SelectAllIcon);

	VWLinkedButton BtnAuthorDeSelect = new VWLinkedButton(VWImages.DeselectAllIcon);

	VWLinkedButton BtnAuthorSelectAll = new VWLinkedButton(VWImages.SelectAllIcon);

	JLabel LblAuthorInfo = new JLabel();
	JLabel LblCustomColumnInfo = new JLabel();
	VWCheckList LstDocument = new VWCheckList();
	VWCheckList LstAuthors = new VWCheckList();
	JLabel lblTitle = new JLabel();

	private boolean gLoadingSavedSearch = false;
	// private int gCurRoom=0;
	// public VWDTIndicesDlg docTypeIndicesDlg = null;
	public VWDlgLocation dlgLocation = null;
	// private int nodeId=0;
	private String nodePath = "";
	private int curSearchId = 0;
	// private Vector searchDTIndices=null;
	private boolean enableSearchInContaints = false;
	private int caller = -1;
	private Search search = new Search(0, "");
}
