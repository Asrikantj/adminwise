/**
 * 
 */
package ViewWise.AdminWise.VWRoute;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.util.Vector;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;

import ViewWise.AdminWise.VWImages.VWImages;

import com.computhink.common.Principal;

/**
 * @author nishad.n
 *
 */
public class VWListBox extends JList{

	private int groupCount =0;
	
	public VWListBox(DefaultListModel defaultmodel){
		super(defaultmodel);
		setCellRenderer(new MyCellRenderer());
	}
	
	public VWListBox(Vector v){
		super(v);
		setCellRenderer(new MyCellRenderer());
	}
	
	public void setGroupCount(int count){
		groupCount = count;
	}
	
	//private Icon getIcon(String name)
    //{
        //java.net.URL url = getClass().getResource(name);
    	//ImageIcon icon = new ImageIcon();
    //    return new ImageIcon("C:\\JHotDrawImages\\images\\start.JPG");
    //}
	
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	    
	    
	    
	
	}

	public class MyCellRenderer extends DefaultListCellRenderer {
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			// Let superclass deal with most of it...
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	 
			ImageIcon imageIcon = null;
			imageIcon = VWImages.UsrIcon;//...; // add extra code here
			try{
				Principal p = (Principal)VWApproversListPanel.vecGroupsAndUsers.get(index);
				if(p.getType()==2)
					imageIcon = VWImages.UsrIcon;//...; // add extra code here
				else
					imageIcon = VWImages.UserGroupIcon;//...; // add extra code here
			}catch(Exception e){
				//JOptionPane.showMessageDialog(null,e.toString());
			}
			Image image = imageIcon.getImage();
			final Dimension dimension = this.getPreferredSize();
			final double height = dimension.getHeight();
			final double width = (height / imageIcon.getIconHeight()) * imageIcon.getIconWidth();
			image = image.getScaledInstance((int)width, (int)height, Image.SCALE_SMOOTH);
			final ImageIcon finalIcon = new ImageIcon(image);
			setIcon(finalIcon);
	 
			return this;
		}
	}

	
	
    
    
    
}
