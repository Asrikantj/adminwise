package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;

import org.jhotdraw.framework.Connector;
import org.jhotdraw.framework.Drawing;
import org.jhotdraw.framework.Figure;
import org.jhotdraw.framework.FigureEnumeration;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDlgSaveLocation;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoute.figures.VWEndFigure;
import ViewWise.AdminWise.VWRoute.figures.VWFigure;
import ViewWise.AdminWise.VWRoute.figures.VWLineConnection;
import ViewWise.AdminWise.VWRoute.figures.VWStartFigure;
import ViewWise.AdminWise.VWRoute.figures.VWTaskFigure;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.common.Constants;
import com.computhink.common.Node;
import com.computhink.common.RouteIndexInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCPreferences;
import com.computhink.vwc.VWClient;

public class VWStartIconProperties extends JDialog implements VWConstant{
	VWClient vwc = null;
	VWCheckList list = new VWCheckList();
	JPanel pnlTable = new JPanel();
	VWComboBox cmbDocTypesList = new VWComboBox();
	VWComboBox cmbIndicesList = new VWComboBox();
	VWComboBox cmbOperator = new VWComboBox();
	//VWComboBox cmbLogicalOperator = new VWComboBox();
	static JTextField txtvalue1 = new JTextField();
	static JTextField txtvalue2 = new JTextField();
	public JLabel lblDays= new JLabel(AdminWise.connectorManager.getString("VWStartIconProperties.lblDays"));
	VWDateComboBox dateValue1 = new VWDateComboBox();
	VWDateComboBox dateValue2 = new VWDateComboBox();

	VWComboBox cmbSelctionValues = new VWComboBox();
	JButton btnAdd = new JButton();
	JButton btnRemove = new JButton();
	JButton btnRefresh = new JButton();
	static VWRouteConditionsTable tblConditions = new VWRouteConditionsTable();
	static Vector vecTableData = new Vector();
	JScrollPane spTable = new JScrollPane(tblConditions);
	public static DefaultListModel listTasksModel = new DefaultListModel();
	JList listTasks = new JList(listTasksModel);
	JScrollPane spListTasks = new JScrollPane(listTasks);
	
	JButton btnUp = new JButton("");
	JButton btnDown = new JButton("");
	JLabel lblTaskSequenceTitle = new JLabel("");
	
	public static boolean active = false;
	private KeyBuffer keyBuffer = new KeyBuffer();	
	private boolean taskSequenceChanged = false;
	public boolean firstTime = false;
	public SymAction symAction = new SymAction();
	int curDocTypeSelected = -1; 		
	/**
	 * @param args
	 */
	public VWStartIconProperties(){
		
	}
		
	int dlgWidth = 492, dlgHeight = 480, routeId = 0;
	int defaultDlgHeight = 285; 
	public VWStartIconProperties(Frame parent,int routeId){
		super(parent);		
		getContentPane().setBackground(AdminWise.getAWColor());
		if (active)
		{
			requestFocus();
			toFront();
			return;
		} 
		vwc = (VWClient)AdminWise.gConnector.ViewWiseClient;
	    String languageLocale=AdminWise.getLocaleLanguage();
		setSize(dlgWidth, defaultDlgHeight);
		setModal(true);
		setLocation(350,250);
		this.routeId = routeId;
		getContentPane().setLayout(null);
		panelStartIconRoute.setLayout(null);
		//TitledBorder border = new javax.swing.border.TitledBorder("Route Type");
		JLabel startlineBorderLabel =  new JLabel("");
		JLabel startLabel =  new JLabel(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWStartIconProperties.startLabel"));
		startLabel.setForeground(new Color(7, 38, 58));
		startLabel.setBounds(5, 2, 140, 13);
		panelStartIconRoute.add(startLabel);
		
		Border border = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		startlineBorderLabel.setBorder(border);
		startlineBorderLabel.setBounds(136, 8, 230, 2);
		panelStartIconRoute.add(startlineBorderLabel);
		//panelStartIconRoute.setBorder(border);
		getContentPane().add(panelStartIconRoute);
		//PR Comment
		loadIndicesList();
		panelStartIconRoute.setBackground(AdminWise.getAWColor());
		panelSeqTask.setBackground(AdminWise.getAWColor());
		panelSeqTask.setLayout(null);
		getContentPane().add(panelSeqTask);
		panelSeqTask.setBounds(0, 110, 468, 145);
		
		JLabel seqlineBorderLabel =  new JLabel("");
		JLabel seqLabel =  new JLabel(AdminWise.connectorManager.getString("VWStartIconProperties.seqLabel"));
		if(languageLocale.equals("nl_NL")){
			seqLabel.setBounds(5, 2, 100+20, 13);
		}else
		seqLabel.setBounds(5, 2, 100, 13);
		panelSeqTask.add(seqLabel);
		
		Border border1 = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		seqlineBorderLabel.setBorder(border1);
		seqLabel.setForeground(new Color(7, 38, 58));
		if(languageLocale.equals("nl_NL")){
		seqlineBorderLabel.setBounds(90+30, 8, 360-30, 2);
		}else
			seqlineBorderLabel.setBounds(90, 8, 360, 2);	
		panelSeqTask.add(seqlineBorderLabel);
		
		
		//TitledBorder border1 = new javax.swing.border.TitledBorder("Task Sequence");
		//border1.setTitleColor(Color.BLUE);
		//panelSeqTask.setBorder(border1);
		initTablePanel();
		setTablePanelMode(false);
		pnlTable.setBounds(7, 68, 468,213);
		//pnlTable.setBackground(Color.gray);		
		getContentPane().add(pnlTable);
		
		/*lblTaskSequenceTitle.setBounds(10 , 2, 100, 22);		
		lblTaskSequenceTitle.setText("Task sequence:");
		panelSeqTask.add(lblTaskSequenceTitle);*/
		
		spListTasks.setBounds(10,20,425,80);
		panelSeqTask.add(spListTasks);
		
		btnUp.setBounds(436,20,20,20);
		btnUp.setIcon(VWImages.UpIcon);
		btnUp.addActionListener(new SymAction());
		panelSeqTask.add(btnUp);
		
		btnDown.setBounds(436,47,20,20);
		btnDown.setIcon(VWImages.DownIcon);
		btnDown.addActionListener(new SymAction());
		panelSeqTask.add(btnDown);
		

		
		BtnSave.setText(BTN_SAVE_NAME);
		BtnSave.setMnemonic('S');
		BtnSave.setBounds(293, 115, AdminWise.gBtnWidth, AdminWise.gBtnHeight);
		BtnSave.setIcon(VWImages.SaveIcon);
		getContentPane().add(BtnSave);
		
		BtnCancel.setText(BTN_CANCEL_NAME);
		BtnCancel.setMnemonic('C');
		BtnCancel.setBounds(380, 115, AdminWise.gBtnWidth, AdminWise.gBtnHeight);
		BtnCancel.setIcon(VWImages.CloseIcon);		
		getContentPane().add(BtnCancel);
		
		SymAction lSymAction = new SymAction();
		BtnSave.addActionListener(lSymAction);
		BtnCancel.addActionListener(lSymAction);
		
		vwDocTypeRec = null;
		initAssignRouteUI();
		setTitle(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWStartIconProperties.setTitle"));
		//PR Comment
		loadStartRouteInfo();		 
		//loadAvailableTasks();
		active = true;  
		SymKey aSymKey = new SymKey();
		keyBuffer.start();
		//PR Comment
		viewMode();
		setModal(true);
		addKeyListener(aSymKey);
		addWindowListener(new WindowAdapter(){
			public void windowLostFocus(WindowEvent e){
				if(active){
					requestFocus();
					toFront();
				}
			}
			public void windowDeactivated(WindowEvent e) {
				if(active){
					requestFocus();
					toFront();
				}
			}
			public void windowStateChanged(WindowEvent e) {
				if(active){	
					toFront();
				}
			}
			public void windowDeiconified(WindowEvent e) {
				if(active){
					toFront();
				}
			}
			public void windowClosing(WindowEvent e) {      
				closeDialog();
			}
			public void windowActivated(WindowEvent e) {
				Object object = e.getSource();
				if (object == VWStartIconProperties.this){
					//loadIndicesList();
				}
			}
		});
		setResizable(false);
		//PR Comment
		setCurrentLocation();
		setVisible(true);
	}
	private void viewMode(){
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2)
			BtnSave.setEnabled(false);
		else if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 1 || 
				AdminWise.adminPanel.routePanel.CURRENT_MODE == 3 || AdminWise.adminPanel.routePanel.CURRENT_MODE == 4){
			
			//For Invalid route should not allow the user to change any of the start properties
			if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 3 && AdminWise.adminPanel.routePanel.routeValidity==INVALIDROUTE)
				BtnSave.setEnabled(false);
			else
				BtnSave.setEnabled(true);
		}
		else
			BtnSave.setEnabled(true);
	}
	
	public JPanel initTablePanel(){
		int left = 10;
		pnlTable.setLayout(null);
		pnlTable.setBackground(AdminWise.getAWColor());
		JLabel indexlineBorderLabel =  new JLabel("");
		JLabel indexLabel =  new JLabel(AdminWise.connectorManager.getString("VWStartIconProperties.indexLabel"));
		indexLabel.setBounds(5, 2, 260, 13);
		pnlTable.add(indexLabel);
		
		Border border1 = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		indexlineBorderLabel.setBorder(border1);
		indexLabel.setForeground(new Color(7, 38, 58));
		indexlineBorderLabel.setBounds(265, 8, 85, 2);
		pnlTable.add(indexlineBorderLabel);
		
		//TitledBorder border = new javax.swing.border.TitledBorder("Index Condition");
		//border.setTitleColor(Color.BLUE);
		//pnlTable.setBorder(border);
		
		lblDocTypes.setText(AdminWise.connectorManager.getString("VWStartIconProperties.lblDocTypes")+" ");
		lblDocTypes.setBounds(left , 20, 85, 20);
		pnlTable.add(lblDocTypes);
		
		cmbDocTypesList.setBounds(left + 90, 20, 251, 20);
		//PR Comment
		loadDocTypes();
		cmbDocTypesList.addActionListener(symAction);
		pnlTable.add(cmbDocTypesList);

		btnRefresh.setBounds(left+345 , 20, 20, 20);
		btnRefresh.setToolTipText(AdminWise.connectorManager.getString("VWStartIconProperties.btnRefresh"));
		btnRefresh.setIcon(VWImages.RefreshIcon);
		btnRefresh.addActionListener(new SymAction());
		pnlTable.add(btnRefresh);
		
		
		lblIndexField.setText(AdminWise.connectorManager.getString("VWStartIconProperties.lblIndexField")+" ");
		lblIndexField.setBounds(left , 55, 85, 20);
		pnlTable.add(lblIndexField);
		
		cmbIndicesList.setBounds(left + 90 , 55 , 251, 20);
		//loadIndicesList();
		//panelStartIconRoute.add(cmbIndiceList);
		cmbIndicesList.addItemListener(new ListSymAction());		
		pnlTable.add(cmbIndicesList);
		
		int rowPosition = 81;
		lblCondition.setText(AdminWise.connectorManager.getString("VWStartIconProperties.lblCondition")+" ");
		lblCondition.setBounds(left , rowPosition, 85, 20);
		pnlTable.add(lblCondition);
		
		loadStringOperators();
		cmbOperator.setBounds(left + 90, rowPosition, 90, 20);
		cmbOperator.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent ie){				
				VWIndexRec selectedIndexObj = (VWIndexRec) cmbIndicesList.getSelectedItem();
				int type = selectedIndexObj.getType();				
				if(type==2){
					cmbSelctionValues.setVisible(false);
					if(cmbOperator.getSelectedIndex()==COND_STARTS_AFTER||cmbOperator.getSelectedIndex()==COND_STARTS_BEFORE){
						dateValue1.setVisible(false);
						dateValue1.setText("");
						dateValue2.setVisible(false);
						dateValue2.setText("");
						txtvalue1.setVisible(true);
						lblDays.setVisible(true);
						txtvalue2.setVisible(false);
					}
					else{
						lblDays.setVisible(false);
						dateValue1.setVisible(true);
						if(cmbOperator.getSelectedIndex()==COND_BETWEEN){							
							dateValue2.setVisible(true);
						}
						else{
							dateValue2.setVisible(false);
							dateValue2.setText("");
						}
						txtvalue1.setVisible(false);
						txtvalue2.setVisible(false);
					}
				}
				else{				
					cmbSelctionValues.setVisible(false);
					dateValue1.setVisible(false);
					dateValue1.setText("");
					dateValue2.setVisible(false);
					dateValue2.setText("");
					txtvalue1.setVisible(true);
					lblDays.setVisible(false);
					if(cmbOperator.getSelectedIndex()==COND_BETWEEN){
						txtvalue2.setVisible(true);
					}else{	
						txtvalue2.setVisible(false);
						txtvalue2.setText("");
					}
				}
				if(type==4){					
					if(cmbOperator.getSelectedIndex()==0){						
						txtvalue1.setVisible(false);
						dateValue1.setVisible(false);
						cmbSelctionValues.setVisible(true);						
					}
					else{
						txtvalue1.setVisible(true);
						dateValue1.setVisible(false);
						cmbSelctionValues.setVisible(false);
					}
				}
			}
		});
		pnlTable.add(cmbOperator);
		
		txtvalue1.setBounds(left + 182, rowPosition, 100,20);
		pnlTable.add(txtvalue1);
		
		lblDays.setBounds(left + 287, rowPosition, 100,20);
		pnlTable.add(lblDays);
		lblDays.setVisible(false);		
		
		dateValue1.setText("");
		dateValue1.setBounds(left + 182, rowPosition, 100,20);
		dateValue1.setDateFormat("MM/dd/yyyy");
		pnlTable.add(dateValue1);								
		dateValue1.setVisible(false);
		
		cmbSelctionValues.setBounds(left + 182, rowPosition, 158,20);		
		pnlTable.add(cmbSelctionValues);								
		cmbSelctionValues.setVisible(false);
				
		txtvalue2.setText("");
		txtvalue2.setBounds(left + 287, rowPosition, 100, 20);
		txtvalue2.setVisible(false);
		pnlTable.add(txtvalue2);
		
		dateValue2.setText("");
		dateValue2.setBounds(left + 287, rowPosition, 100, 20);
		dateValue2.setDateFormat("MM/dd/yyyy");
		pnlTable.add(dateValue2);								
		dateValue2.setVisible(false);

		
		//loadLogicalOperators();
		//cmbLogicalOperator.setBounds(268, 28, 60, 20);
		//pnlTable.add(cmbLogicalOperator);
		
		//btnAdd.setText("Add");
		btnAdd.setToolTipText(AdminWise.connectorManager.getString("VWStartIconProperties.btnAdd"));
		btnAdd.setBounds(415, rowPosition + 7, 20, 20);
		btnAdd.setMnemonic('A');
		pnlTable.add(btnAdd);
		btnAdd.setIcon(VWImages.plusIcon);
		btnAdd.addActionListener(new SymButton());
		
		//btnRemove.setText("Remove");
		btnRemove.setToolTipText(AdminWise.connectorManager.getString("VWStartIconProperties.btnRemove"));
		btnRemove.setBounds(438, rowPosition + 7, 20, 20);
		btnRemove.setMnemonic('R');
		btnRemove.setIcon(VWImages.minusIcon);
		pnlTable.add(btnRemove);
		btnRemove.addActionListener(new SymButton());
		
		spTable.setBounds(10, 110, 450, 105);
		pnlTable.add(spTable);
		spTable.getViewport().setBackground(Color.white); 
		setTitle(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWStartIconProperties.setTitle"));
		return pnlTable;
	}
	
	private void loadValues()
	{
	    cmbSelctionValues.removeAllItems();
	    VWIndexRec indexRec = (VWIndexRec) cmbIndicesList.getSelectedItem();
	    Vector strValues = indexRec.selectionValues;
	    if(strValues==null || strValues.equals("")) return;
	    for(int i=0;i<strValues.size();i++){
		cmbSelctionValues.addItem(strValues.get(i));
	    }
	}
	
	public void loadDocTypes() {
        cmbDocTypesList.removeAllItems();
        cmbDocTypesList.addItem(new VWDocTypeRec(-1,LBL_DOCTYPE_NAME_1,false));
        Vector data=VWDocTypeConnector.getDocTypes();
        if (data!=null)
            for (int i=0;i<data.size();i++)
                cmbDocTypesList.addItem(data.get(i));
    }
	public void loadIndicesForSelectedDocType(){
		if(cmbDocTypesList.getSelectedIndex()>0){
			VWDocTypeRec selDocType=(VWDocTypeRec) cmbDocTypesList.getSelectedItem();		
			List indicesList =  VWDocTypeConnector.getDTIndices(selDocType.getId());
		    if(indicesList==null || indicesList.size()==0) {
		        return;
		    }	  
		    try{
		    	if(cmbIndicesList.getItemCount()>0)
		    		cmbIndicesList.removeAllItems();
			}catch(Exception ex){
				//System.out.println("Error1 "+ex.toString());
			}		
			for(int count=0; count<indicesList.size();count++){
				try{
					VWIndexRec indexRec = (VWIndexRec) indicesList.get(count);
					String type = indexRec.getType()+"";
					//Exclude Sequence datatype from the display
					if(type.trim().equalsIgnoreCase("5")){
						continue;
					}
					cmbIndicesList.addItem(indexRec);
				}catch(Exception ex){
					//System.out.println(" addItem Error2 "+ex.toString());
				}
			}
		}
		else{
			loadIndicesList();
		}
		cmbIndicesList.setSelectedIndex(0);
	}	    
	public void closeDialog() 
	{
		BtnCancel_actionPerformed(null);
		active = false;
		keyBuffer.kill();		
		setVisible(false);
		dispose();
		
	}
	public static void loadAvailableSavedTasks(){
		Hashtable figureList = new Hashtable();
		if (AdminWise.adminPanel.routePanel.routeInfoBean != null && AdminWise.adminPanel.routePanel.alRouteTaskInfo != null && AdminWise.adminPanel.routePanel.alRouteTaskInfo.size() > 0) {
			for (int count = 0; count < AdminWise.adminPanel.routePanel.alRouteTaskInfo.size(); count++) {
				try {
					RouteTaskInfo curTaskInfo = (RouteTaskInfo) AdminWise.adminPanel.routePanel.alRouteTaskInfo.get(count);					
					figureList.put(curTaskInfo.getTaskSequence(), curTaskInfo.getTaskName());
					//AdminWise.printToConsole(" LoadAvailableSavedTasks  Task Name  :" + curTaskInfo.getTaskName() + " : " + curTaskInfo.getTaskSequence());  
				}catch(Exception ex){
					//AdminWise.printToConsole("Exception in loadAvailableSavedTasks Loop1 " + ex.getMessage());
				}
			}
			listTasksModel.removeAllElements();
			for (int count = 1; count <= figureList.size(); count++) {
				try {
					String curTaskName = String.valueOf((figureList.get(count)== null)?"":figureList.get(count));					
					//AdminWise.printToConsole(" Added to List Box :" + count + " : " + curTaskName);
					if (curTaskName != null && curTaskName.trim().length() != 0)
						listTasksModel.addElement(curTaskName);
				}catch(Exception ex){
					//AdminWise.printToConsole("Exception in loadAvailableSavedTasks Loop2 " + ex.getMessage());
				}
			}
			AdminWise.adminPanel.routePanel.taskSequenceAdded = true;
		}
		
		}
	public static void loadAvailableTasks(){
		if (listTasksModel != null && listTasksModel.size() == 0)
			loadAvailableSavedTasks();
		//System.out.println("loadAvailableTasks Task .......... " + listTasksModel);	
		//variable taskSequenceAdded can be removed. But need full testing.
		AdminWise.adminPanel.routePanel.removeUnUsedTasksIfAny();
		/*if (AdminWise.adminPanel.routePanel.removeUnUsedTasksIfAny()){
			AdminWise.adminPanel.routePanel.taskSequenceAdded = false;
		}*/
		int taskCount = AdminWise.adminPanel.routePanel.drsDrawing.getTaskCount() - 1;
		if(!AdminWise.adminPanel.routePanel.taskSequenceAdded || listTasksModel.size() == 0 || (listTasksModel.size() != taskCount)){
			listTasksModel.removeAllElements();
			try{
				FigureEnumeration fEnum = AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().figures();
				while(fEnum.hasNextFigure()){	    		
					try{
						Object curFigure =  fEnum.nextFigure();
						if (curFigure instanceof VWFigure){
							String curselFigureType= ((VWFigure)curFigure).getFigureType();
							if(curselFigureType.equalsIgnoreCase("Task")){
								listTasksModel.addElement(((VWFigure)curFigure).getFigureName());
							}
						}
					}catch(Exception ex){
						//System.out.println("Load Available task ::: " + ex.getMessage());
					}
				}	    			    	
			}catch(Exception e){
				//System.out.println("Exception in loadAvailble Tasks : " + e.getMessage());
				e.printStackTrace();
			}
			AdminWise.adminPanel.routePanel.taskSequenceAdded = true;
		}
		vecSequence.removeAllElements();
		for(int count=0;count<listTasksModel.size();count++){
			vecSequence.add(listTasksModel.get(count));
		}
	}
	
	public void loadStartRouteInfo(){
		int roomId = AdminWise.adminPanel.roomPanel.getSelectedRoom().getId();
		int retUpdate = AdminWise.gConnector.ViewWiseClient.isRouteInUse(roomId, AdminWise.adminPanel.routePanel.routeInfoBean.getRouteId(), 0);
		cmbRouteStart.setEnabled(retUpdate == 1 || retUpdate == 2 ? false:true);
		viewMode();
		loadAvailableTasks();
		if(firstTime){
			clearAll();
			clearTableAndRelatedData();
			firstTime = false;
		}
		if(cmbIndicesList!=null&& cmbIndicesList.getItemCount()>0)
			cmbIndicesList.setSelectedIndex(0);
		if( (AdminWise.adminPanel.routePanel.routeInfoBean!=null) ){
			try{
				startLocationNodeId = "";
				currentStartLocationNodepath = "";
				int startActionId = Integer.parseInt(AdminWise.adminPanel.routePanel.routeInfoBean.getStartAction());
				cmbRouteStart.setSelectedIndex(startActionId);
				if(startActionId == 0)// && AdminWise.adminPanel.routePanel.CURRENT_MODE !=4)
				{
					Vector locationInfo = new Vector();
					//roomId = AdminWise.adminPanel.roomPanel.getSelectedRoom().getId();//Using Node Id get the location Path
					String nodePath = AdminWise.adminPanel.routePanel.routeInfoBean.getStartLocation();
					if(nodePath!=null && !nodePath.trim().equals("")){
						AdminWise.gConnector.ViewWiseClient.getNodeParents(roomId, new Node(nodePath), locationInfo);
						StringBuffer path = new StringBuffer();
						if(locationInfo!=null && locationInfo.size()>0){
							path = new StringBuffer(AdminWise.adminPanel.roomPanel.getSelectedRoom().getName());
							Node node = null;
							for(int i=locationInfo.size()-1;i>=0; i--){
								try{
									node = (Node)locationInfo.get(i);
									path.append("\\" + node.getName());
								}catch(Exception e){
								}
							}
						}else{
							AdminWise.adminPanel.routePanel.routeInfoBean.setStartLocation("");
							nodePath = "0";
						}
						currentStartLocationNodeId = nodePath;
						currentStartLocationNodepath = path.toString().trim();
						txtRouteLocation.setText(path.toString().trim());
					}
				}
				ArrayList alIndexInfo = new ArrayList();		
				vecTableData = new Vector();
				if(AdminWise.adminPanel.routePanel.routeInfoBean!=null){
					alIndexInfo = AdminWise.adminPanel.routePanel.routeInfoBean.getRouteIndexList();
					if(alIndexInfo!=null && alIndexInfo.size()>0){
						for(int count = 0;count<alIndexInfo.size();count++){
							RouteIndexInfo routeIndexInfo = (RouteIndexInfo)alIndexInfo.get(count);
							//Set selected doctype
							int selDocTypeId = routeIndexInfo.getDocTypeId();
							int indexTypeId = routeIndexInfo.getIndexTypeId();
							AdminWise.printToConsole("indexTypeId VWStartIconProperties.loadStartRouteInfo::::"+indexTypeId+"::::"+routeIndexInfo.getIndexName());
							setSelectedDocType(selDocTypeId);							
							String compOp = routeIndexInfo.getComparisonOperator();//getCompOp(routeIndexInfo.getComparisonOperator());
							String logicalOp = routeIndexInfo.getLogicalOperator();//"";
							String curSelectedData = "";	
							String value1 = routeIndexInfo.getIndexValue1();
							String value2 = routeIndexInfo.getIndexValue2();
							/**
							 * SOW:-2099-961 Enhancement for Workflow Stars before 
							 * Condition added for starts before also like starts after
							 */
							if(indexTypeId == 2 && (!((compOp.trim().equalsIgnoreCase(COND_STARTS_AFTER_STR))||(compOp.trim().equalsIgnoreCase(COND_STARTS_BEFORE_STR)))))
								value1 = com.computhink.common.util.VWUtil.convertDate(value1,"yyyyMMdd","MM/dd/yyyy");
							if(indexTypeId == 2 && compOp.trim().equalsIgnoreCase(COND_BETWEEN_STR))
								value2 = com.computhink.common.util.VWUtil.convertDate(value2,"yyyyMMdd","MM/dd/yyyy");
							if(vecTableData.size()==0)
								curSelectedData = "	"+routeIndexInfo.getIndexName() + "\t" + compOp + "\t" +  value1 + "\t" +"-" + "\t"+
								value2+ "\t"+routeIndexInfo.getIndexId()+ "\t"+indexTypeId;
							else
								curSelectedData = "	"+routeIndexInfo.getIndexName() + "\t" + compOp + "\t" +  value1 + "\t" +logicalOp + "\t"+
								value2+ "\t"+routeIndexInfo.getIndexId()+ "\t"+indexTypeId;
							AdminWise.printToConsole("tblConditions.getRowCount() before printing curSelectedData::::"+tblConditions.getRowCount());
							AdminWise.printToConsole("curSelectedData::::"+curSelectedData);
							AdminWise.printToConsole("vecTableData Before Adding::::"+vecTableData);
							if(!curSelectedData.equalsIgnoreCase("\t"+"-"+"\t"+"-"+"\t"+"-"+"\t"+"-"+"\t"+"-"+"\t"+"0"+"\t"+"0")){
								vecTableData.add(curSelectedData);
							}
							AdminWise.printToConsole("vecTableData After Adding::::"+vecTableData);
						}
					}
				}
				
				tblConditions.addData(vecTableData);			
			}catch(Exception e){
				////System.out.println("Error on 219 " + e.getMessage());
			}
		}
	}
	
	public void setSelectedDocType(int docTypeId){
		for(int count=0;count<cmbDocTypesList.getItemCount();count++){
			try{
				VWDocTypeRec selDocType=(VWDocTypeRec) cmbDocTypesList.getItemAt(count);
				int curDocTypeId = selDocType.getId();
				if(curDocTypeId == docTypeId){
					cmbDocTypesList.setSelectedIndex(count);
					curDocTypeSelected = count;
					break;
				}
			}catch(Exception e){
				//System.out.println("EXCEPTION in setSelectedDocType() method :"+e.toString());
			}
		}
	}
	
	public String getCompOp(int operator){
		String op =">";
		if(operator==0)
			op=">";
		else if(operator==1)
			op=">=";
		else if(operator==2)
			op="<";
		else if(operator==3)
			op="<=";
		else if(operator==4)
			op="=";
		else if(operator==5)
			op="!=";
		return op;
	}
	
	public void setTablePanelMode(boolean mode){
		if(mode){
			pnlTable.setVisible(true);
		}
		else{
			pnlTable.setVisible(false);
		}
	}
	
	public void loadIndicesList(){
	    VWIndexRec[] indices = VWDocTypeConnector.getIndices(false);
	    try{
		if((cmbIndicesList!=null)&& cmbIndicesList.getItemCount()>0)
		    cmbIndicesList.removeAllItems();
	    }catch(Exception ex){
		System.out.println("Error in loadIndicesList "+ex.toString());
	    }
	    try{
		for(int count=0; count<indices.length;count++){
		    VWIndexRec vwIndexRec = (VWIndexRec)indices[count];
		    if(vwIndexRec.getType() == 5)
			continue;
		    cmbIndicesList.addItem(indices[count]);
		}			
	    }catch(Exception ex){
		//System.out.println("<<Error in loadIndicesList "+ex.toString());
	    }			
	    if(cmbIndicesList.getItemCount()>0)
		cmbIndicesList.setSelectedIndex(0);
	}

	public int getIndexId(String indiceName){
		int id = 1;
		VWIndexRec vwIndexRec = null;
		for(int count=0;count<cmbIndicesList.getItemCount();count++)
		{
			vwIndexRec = (VWIndexRec) cmbIndicesList.getItemAt(count);
			if(vwIndexRec.getName().trim().equalsIgnoreCase(indiceName.trim())){
				id = vwIndexRec.getId();
				break;
			}
		}
		return id;
	}
	
	public int getIndexType(String indiceName){
		int type = 0;
		VWIndexRec vwIndexRec = null;
		try{
			for(int count=0;count<cmbIndicesList.getItemCount();count++)
			{
				try{
					vwIndexRec = (VWIndexRec) cmbIndicesList.getItemAt(count);
					if(vwIndexRec.getName().trim().equalsIgnoreCase(indiceName)){
						type = vwIndexRec.getType();
						break;
					}
				}catch(Exception e){
					//JOptionPane.showMessageDialog(null,e.toString());
				}
			}
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,e.toString());
		}
		return type;
	}
	

	
//	------------------------------------------------------------------------------
	class SymKey extends java.awt.event.KeyAdapter
	{
		public void keyTyped(java.awt.event.KeyEvent event)
		{
			Object object = event.getSource();
			if(event.getKeyText(event.getKeyChar()).equals("Enter"))
				BtnSave_actionPerformed(null);
			else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
				BtnCancel_actionPerformed(null);
		}
	}
	class SymButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(ae.getSource() == btnAdd){
				//Allow maximum 10 conditions.
				if(tblConditions.getRowCount()>=10){
					active = false;
					JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_1"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
					active = true;					
					return;
				}
				curDocTypeSelected = cmbDocTypesList.getSelectedIndex();
				String selIndiceName = ((VWIndexRec)cmbIndicesList.getSelectedItem()).toString();
				//Check for duplicate indice entry.
				VWIndexRec vwIndexRec = (VWIndexRec) cmbIndicesList.getSelectedItem();
				String indexId = String.valueOf(vwIndexRec.getId());
				String indexTypeId = ""+vwIndexRec.getType();
				if(tblConditions.isDuplicate(indexId)){
					active = false;
					JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_2"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
					active = true;					
					return;
				}				
				String selOperator = cmbOperator.getSelectedItem().toString();
				String selValue1 = "";
				String selValue2 = "";

				int type = vwIndexRec.getType();//getIndexType(selIndiceName);
				int selectedOPIndex = cmbOperator.getSelectedIndex();

				//NUMERIC TYPE condition checking
				if(type == 1){
					if( (!VWUtil.isNumeric(txtvalue1.getText(), false)) ){
						active = false;
						JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_3"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
						txtvalue1.requestFocus();
						active = true;
						return;
					}
					if( (selectedOPIndex==COND_BETWEEN  && !VWUtil.isNumeric(txtvalue2.getText(), false)) ){
						active = false;
						JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_3"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
						txtvalue2.requestFocus();
						active = true;
						return;
					}
				}
				//End...						
				
				if (type == 2 && selectedOPIndex==COND_BETWEEN){
					String selDateValue1 = dateValue1.getSelectedItem().toString();					
					String selDateValue2 = dateValue2.getSelectedItem().toString();
					if(selDateValue1.trim().equals("")){
						active = false;
						JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_4"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
						active = true;
						dateValue1.requestFocus();
						return;
					}else if(selDateValue2.trim().equals("")){
						active = false;
						JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_5"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
						active = true;
						dateValue2.requestFocus();
						return;
					}
				}
			
				if (type == 2 && (selectedOPIndex==COND_STARTS_AFTER||selectedOPIndex==COND_STARTS_BEFORE)){
					if(!VWUtil.isNumeric(txtvalue1.getText(), true)){					
						active = false;
						JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_6"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
						active = true;
						txtvalue1.requestFocus();
						return;
					}									
				}
				
				/*if (type == 2 && ((selectedOPIndex!=COND_STARTS_AFTER)){
					selValue1 = dateValue1.getSelectedItem().toString();
					selValue2 = dateValue2.getSelectedItem().toString();
				}*/
				/**
				 * SOW:-2099-961 Enhancement for Workflow Stars before 
				 * Condition added for starts before also like starts after
				 */
				if (type == 2&&(!(selectedOPIndex==COND_STARTS_AFTER||selectedOPIndex==COND_STARTS_BEFORE))){
					selValue1 = dateValue1.getSelectedItem().toString();
					selValue2 = dateValue2.getSelectedItem().toString();
				}				
				else{
					selValue1 = txtvalue1.getText();
					selValue2 = txtvalue2.getText();					
				}				
				
				//Selection
				if (type == 4){
					 if(selectedOPIndex==0){
						 selValue1=cmbSelctionValues.getSelectedItem().toString();
					 }
					 else{
						 selValue1 = txtvalue1.getText();
					 }
				}
				else{
					
				}
				//End...
				
				if(selValue1.trim().equals("")){
					active = false;
					JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_7"), AdminWise.connectorManager.getString("VWStartIconProperties.info"), JOptionPane.OK_OPTION);
					active = true;
					if (type == 2)
						dateValue1.requestFocus();
					else
						txtvalue1.requestFocus();
				}else{
					String selLogicalOperator = "AND";
					if(selValue2.trim().equals("")){
						selValue2="-";
					}
					String curSelectedData = "";
					if(vecTableData.size()==0)
						curSelectedData = "	"+selIndiceName + "\t" + selOperator + "\t" +  selValue1 +
							"\t" +"-" + "\t"+selValue2 + "\t"+indexId+"\t"+indexTypeId;
					else
						curSelectedData = "	"+selIndiceName + "\t" + selOperator + "\t" +  selValue1 + 
							"\t" +selLogicalOperator + "\t"+selValue2 + "\t"+indexId+"\t"+indexTypeId;
					
					vecTableData.add(curSelectedData);
					tblConditions.addData(vecTableData);
				}
			}
			
			if(ae.getSource() == btnRemove){
				BtnRemove_actionPerformed(ae);
			}
		}
	}
	
	public void loadOperators(){
		cmbOperator.addItem(">");
		cmbOperator.addItem(">=");
		cmbOperator.addItem("<");
		cmbOperator.addItem("<=");
		cmbOperator.addItem("=");
		cmbOperator.addItem("!=");
	}
	public void loadStringOperators(){
		cmbOperator.addItem("=");
		cmbOperator.addItem("like");
	}
	public void loadIntDateOperators(boolean displayStartsAfter){
		cmbOperator.addItem(">");
		cmbOperator.addItem(">=");
		cmbOperator.addItem("<");
		cmbOperator.addItem("<=");
		cmbOperator.addItem("=");
		cmbOperator.addItem("!=");
		cmbOperator.addItem(COND_BETWEEN_STR);
		if(displayStartsAfter){
			cmbOperator.addItem(COND_STARTS_AFTER_STR);
			cmbOperator.addItem(COND_STARTS_BEFORE_STR);
		}
	}
	
	public void loadLogicalOperators(){
		//cmbLogicalOperator.addItem("AND");
	}
	
	private void initAssignRouteUI(){
		panelStartIconRoute.setSize(465, dlgHeight);
		int height = 22, width = 120, left = 5, top = 20, heightGap = 20, btnWidth = 80,
		leftGap = 10, lstWidth = 175, lstHeight = 170, brwBtnWidth = 24, txtWidth = 400;
		
		panelStartIconRoute.add(lblStartWhen);
		lblStartWhen.setText("");
		lblStartWhen.setBounds(leftGap, top, width, height);
		lblStartWhen.setText(LBL_ROUTESTART);
				
		cmbRouteStart.addItemListener(new ListSymAction());
		panelStartIconRoute.add(cmbRouteStart);
		left = left + width;
		//top = top + heightGap;
		cmbRouteStart.setBounds(left - 25, top, 251, height);
		
		panelStartIconRoute.add(lblTriggerAt);
		lblTriggerAt.setText(LBL_TRIGGER_AT);
		top = top + heightGap + 10;
		left = leftGap;
		lblTriggerAt.setBounds(left, (top-2), txtWidth, height);
		lblTriggerAt.setBackground(Color.white);		
		lblTriggerAt.setText("Location:");
		
		//panelStartIconRoute.add(lblDocTypes);
		//lblDocTypes.setText("Select Doc Types");
	//	top = top + heightGap + 5;
//		left = leftGap+300;
/*		lblDocTypes.setBounds((left-44), (top-2), txtWidth, height);
		lblDocTypes.setBackground(Color.white);		
		lblDocTypes.setText("Doc Types:");
*/		
		
//		top = top + heightGap;
		 
		panelStartIconRoute.add(txtRouteLocation);		
		txtRouteLocation.setBounds(100, top, 333, height);
		txtRouteLocation.setEditable(false);
		txtRouteLocation.setBackground(Color.white);
		
		panelStartIconRoute.add(BtnRouteLocation);
		BtnRouteLocation.setIcon(VWImages.BrowseRouteImage);
		BtnRouteLocation.addActionListener(new SymAction());
		BtnRouteLocation.setBounds(435, top, 24, 22);
		
		top = top + heightGap;
		left = leftGap;
/*		//panelStartIconRoute.add(lblIndexField);
		lblIndexField.setBounds(left, top, txtWidth, height);
		lblIndexField.setBackground(Color.white);
		lblIndexField.setVisible(false);
*/
		loadComboboxData(cmbRouteStart, lstStartWhen);
	}
	public void clearAll(){
		try{
			curDocTypeSelected = -1;
			this.cmbDocTypesList.setSelectedIndex(0);
			this.cmbIndicesList.setSelectedIndex(0);
			this.cmbOperator.setSelectedIndex(0);
		}catch(Exception ex){
			System.out.println("Error in clear :"+ex.toString());
		}
	}
	public void clearTableAndRelatedData(){
		try{
			vecTableData = new Vector();
			tblConditions.clearData();
			txtRouteLocation.setText("");			

		}catch(Exception ex){}
	}
	public void clearAllText(){
		txtvalue1.setText("");
		txtvalue2.setText("");
		dateValue1.setText("");
		dateValue2.setText("");
	}
	
	protected JRootPane createRootPane() { 
		JRootPane rootPane = new JRootPane();
		//Handle escape key.
		KeyStroke escapeStroke = KeyStroke.getKeyStroke("ESCAPE");
		Action escapeActionListener = new AbstractAction() { 
			public void actionPerformed(ActionEvent actionEvent) {
				BtnCancel_actionPerformed(actionEvent);
			} 
		};		
		InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(escapeStroke, "ESCAPE");
		rootPane.getActionMap().put("ESCAPE", escapeActionListener);
		//Handle enter key.
		KeyStroke enterStroke = KeyStroke.getKeyStroke("ENTER");
		Action enterActionListener = new AbstractAction() { 
			public void actionPerformed(ActionEvent actionEvent) {
				BtnSave_actionPerformed(actionEvent);
			} 
		} ;
		inputMap.put(enterStroke, "ENTER");
		rootPane.getActionMap().put("ENTER", enterActionListener);

		return rootPane;
	} 
	
	/*public void loadData(int routeId){
		// This method called directly when user clicks on 'Assign Route'. 
		// Assign the current selected docTypeId.  
		this.routeId = routeId;
		setTitle("testing");
	}*/
	private void loadComboboxData(JComboBox comboBox, String[] data) {
		
		if(data!=null && data.length>0){
			int ttlItems = comboBox.getItemCount();
			if(ttlItems==0){
				for (int i = 0; i < data.length; i++) {
					try{
						comboBox.addItem(data[i]);
					}catch(Exception e){
						comboBox.removeAll();
						//JOptionPane.showMessageDialog(null,e.toString());
					}
					
				}
			}
		}
	}
	class ListSymAction implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent iEvent){
			Object object=iEvent.getSource();
			if(object==cmbRouteStart){
				cmbRouteStart_ItemStateChanged();
			} else if(object==cmbIndicesList){
				cmbIndicesList_ItemStateChanged();
			}					
		}
	}
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if(object == BtnSave){
				BtnSave_actionPerformed(event);
			}else if(object == BtnCancel){
				BtnCancel_actionPerformed(event);
			}else if(object == BtnRouteLocation){
				BtnRouteLocation_actionPerformed(event, LBL_StartLocation);            
			}else if(object == btnUp){
				btnUp_ActionPerformed(event);
			}else if(object == btnDown){
				btnDown_ActionPerformed(event);        				
			}else if(object == btnRefresh){
				btnRefresh_ActionPerformed(event);
			}
			else if(object == cmbDocTypesList){
				cmbDocTypesList_ActionPerformed();
			}			
		}
	}
	
	public void btnUp_ActionPerformed(ActionEvent event){
		try{
			//CODE FOR MOVING UP...			
			int selectedIndex = listTasks.getSelectedIndex();
			if(selectedIndex>0){
				String selectedItem1 = String.valueOf(listTasksModel.get(selectedIndex-1));
				String selectedItem2 = String.valueOf(listTasksModel.get(selectedIndex));    				
				listTasksModel.remove(selectedIndex-1);
				listTasksModel.add(selectedIndex-1,selectedItem1);
				listTasksModel.remove(selectedIndex);
				listTasksModel.add(selectedIndex-1,selectedItem2);
				listTasks.setSelectedIndex(selectedIndex-1);
				taskSequenceChanged = true;
			}
		}catch(Exception e){
			//System.out.println("Exception in Up BTN Click :"+e.toString());
		}
		//END...		
	}
	
	
	public void btnDown_ActionPerformed(ActionEvent event){
		try{
			//CODE FOR MOVING DOWN...				
			int selectedIndex = listTasks.getSelectedIndex();
			if(selectedIndex>=0 && selectedIndex<listTasks.getModel().getSize()){
				String selectedItem1 = String.valueOf(listTasksModel.get(selectedIndex+1));
				String selectedItem2 = String.valueOf(listTasksModel.get(selectedIndex));
				listTasksModel.remove(selectedIndex+1);
				listTasksModel.add(selectedIndex+1,selectedItem1);
				listTasksModel.remove(selectedIndex);
				listTasksModel.add(selectedIndex+1,selectedItem2);
				listTasks.setSelectedIndex(selectedIndex+1);
				taskSequenceChanged = true;
			}
		}catch(Exception e){
			//System.out.println("Exception in Down BTN Click :"+e.toString());
		}
		//END...
	} 
	public void btnRefresh_ActionPerformed(ActionEvent event){
		try{
			int curselected = cmbDocTypesList.getSelectedIndex();
			if(curselected>0){
				loadIndicesForSelectedDocType();
			}else{
				loadDocTypes();
				loadIndicesList();
			}
		}catch(Exception ex){
		}
	
	}
	public void cmbDocTypesList_ActionPerformed(){
		try{
			int rowCount = tblConditions.getRowCount();
			int curselected = cmbDocTypesList.getSelectedIndex();
			if(curDocTypeSelected != curselected){
				if( (rowCount>0)  && (curDocTypeSelected!=-1) ){
					if(AdminWise.adminPanel.routePanel.CURRENT_MODE != 2 && curDocTypeSelected != 0 || 
							(curselected>0 && curDocTypeSelected == 0 && AdminWise.adminPanel.routePanel.CURRENT_MODE != 2)){
						active = false;
						VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("VWStartIconProperties.msg_8"));
						active = true;
						cmbDocTypesList.setSelectedIndex(curDocTypeSelected);
						return;
					}
				}
				else{
					clearAllText();
					loadIndicesForSelectedDocType();
				}
			}
		}catch(Exception e){
			//System.out.println("EXCEPTION :"+e.toString());
		}
	}
	public void cmbIndicesList_ItemStateChanged(){
		if(cmbIndicesList!=null && cmbIndicesList.getItemCount()>0){
			VWIndexRec selectedIndexObj = (VWIndexRec) cmbIndicesList.getSelectedItem();
			clearAllText();
			int type = selectedIndexObj.getType();
			//if selected index is Date object
			if(type==2){
				dateValue1.setVisible(true);
				txtvalue1.setVisible(false);
				txtvalue2.setVisible(false);
			}
			else{
				dateValue1.setVisible(false);
				dateValue2.setVisible(false);
				txtvalue1.setVisible(true);
			}
			cmbOperator.removeAllItems();
			if(type==0 || type == 3){
				loadStringOperators();
				txtvalue1.setVisible(true);
				txtvalue2.setVisible(false);
			}else if(type==1){
				loadIntDateOperators(false);
			}else if(type==2){
				loadIntDateOperators(true);
			}
			else if(type == 4){
				loadStringOperators();
				loadValues();
				cmbSelctionValues.setVisible(true);
			}
			else{
				loadOperators();
			}
		}
	}

//	Item State Changed Event for RouteStart Combo...
	public void cmbRouteStart_ItemStateChanged(){
			if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[1])){
				txtRouteLocation.setText("");
				lblTriggerAt.setVisible(false);
				//lblDocTypes.setVisible(false);
				txtRouteLocation.setVisible(false);
				BtnRouteLocation.setVisible(false);
				panelStartIconRoute.setBounds(7, 5, 472, 48);
				panelSeqTask.setBounds(7, 63, 472, 115);
				BtnSave.setBounds(305, 185,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				BtnCancel.setBounds(387, 185,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
/*				lblTaskSequenceTitle.setBounds(10, 60, 240, 22);
				spListTasks.setBounds(10, 80, 425, 80);
				btnUp.setBounds(436, 80, 24, 22);
				btnDown.setBounds(436, 105, 24, 22);				
				BtnSave.setBounds(293, 175, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(380, 175, 80, AdminWise.gBtnHeight);*/
				
				setTablePanelMode(false);
				setSize(dlgWidth, 250);
			}
			else if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[0])){
				lblTriggerAt.setVisible(true);
				//lblDocTypes.setVisible(false);
				//lblIndexField.setVisible(false);
				txtRouteLocation.setVisible(true);
				txtRouteLocation.setText("");
				BtnRouteLocation.setVisible(true);
				
				lblTriggerAt.setText(AdminWise.connectorManager.getString("RouteStartIcon.Location")+" ");
				panelStartIconRoute.setBounds(7, 5, 472, 85);
				panelSeqTask.setBounds(7, 90, 472, 115);
				BtnSave.setBounds(305, 212,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				BtnCancel.setBounds(387, 212,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				
				
/*				lblTaskSequenceTitle.setBounds(10, 105, 240, 22);
				spListTasks.setBounds(10, 125, 425, 80);
				btnUp.setBounds(436, 125, 24, 22);
				btnDown.setBounds(436, 150, 24, 22);				
				BtnSave.setBounds(300, 220, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(382, 220, 80, AdminWise.gBtnHeight);
*/				
				setTablePanelMode(false);
				setSize(dlgWidth, 277);
				
			}else if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[2])){
				//lblIndexField.setVisible(true);
				lblTriggerAt.setVisible(false);
				//lblDocTypes.setVisible(true);
				txtRouteLocation.setVisible(false);
				BtnRouteLocation.setVisible(false);
				clearAll();
				clearTableAndRelatedData();
				//lblTriggerAt.setText("Select document type: ");
				//lblIndexField.setText("Select index field: ");		
				panelStartIconRoute.setBounds(7, 5, 472, 58);
				pnlTable.setBounds(7, 63, 472, 228);
				panelSeqTask.setBounds(7, 293, 472, 115);
				BtnSave.setBounds(305, 415,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				BtnCancel.setBounds(387, 415,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				
/*				lblTaskSequenceTitle.setBounds(10, 250, 240, 22);
				spListTasks.setBounds(10, 270, 425,80);
				btnUp.setBounds(436, 270, 24, 22);
				btnDown.setBounds(436, 295, 24, 22);				
				BtnSave.setBounds(300, 365, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(382, 365, 80, AdminWise.gBtnHeight);*/
				setTablePanelMode(true);
				setSize(dlgWidth,dlgHeight);
			}
	}
	
	private boolean validateData(){
		int selectedAction = cmbRouteStart.getSelectedIndex();
		VWDocTypeRec selDocType = (VWDocTypeRec) cmbDocTypesList
				.getSelectedItem();
		AdminWise.printToConsole("selDocType Inside validateData()::::" + selDocType);
		int docTypeId = selDocType.getId();
		AdminWise.printToConsole("docTypeId Inside validateData()::::" + docTypeId);
		
		if(selectedAction == 0){
			if(txtRouteLocation.getText().trim().equalsIgnoreCase("")){
				active = false;
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWStartIconProperties.msg_9")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWStartIconProperties.msg_10"), VWMessage.DIALOG_TYPE);
				active = true;
				return false;
			}
		}
		else if(selectedAction == 2 && tblConditions.getRowCount() == 0 && docTypeId <= 0){
			active = false;
			VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWStartIconProperties.msg_18"), VWMessage.DIALOG_TYPE);
			active = true;
			return false;
		}
		
		return true;
	}
	
	public static void drawConnections(){
		FigureEnumeration fEnum1 = AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().figures();
		Hashtable figuresList = new Hashtable();
		VWFigure startFig = null;
		VWFigure endFig  = null;
		while(fEnum1.hasNextFigure()){    		    		
			Object currentFig = fEnum1.nextFigure();
			if (currentFig instanceof VWFigure) {
				VWFigure currentFigure = (VWFigure) currentFig;
				figuresList.put(currentFigure.getFigureName(), currentFigure);
				if (currentFigure != null  && currentFigure.getFigureName().startsWith("Start_1")){
					startFig = (VWStartFigure)currentFigure;
				}
				else if(currentFigure != null  && currentFigure.getFigureName().startsWith("End_1")){
					endFig = (VWEndFigure)currentFigure;
				}    			
			}else{
				AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().remove((Figure)currentFig);
			}			
			
		}
		//System.out.println("listTasksModel  " + listTasksModel );
		//System.out.println("listTasksModel  Size " + listTasksModel.size() );
		if (listTasksModel != null && listTasksModel.size() > 0){ 
			for (int index = 0; index < listTasksModel.size()-1; index++){
				try{
					Figure srcFigure = (Figure) figuresList.get(listTasksModel.get(index).toString());
					Figure destFigure = (Figure) figuresList.get(listTasksModel.get(index+1).toString());
					Point originPoint = srcFigure.center(); 
					Point destinationPoint = destFigure.center();
					Connector startConnector1 = srcFigure.connectorAt(originPoint.x , originPoint.y);//findConnectorAt(originPoint.x , originPoint.y, AdminWise.adminPanel.routePanel.drsDrawing.drawing());
					//srcFigure.connectorAt(originPoint.x , originPoint.y);	        
					Connector endConnector1 = destFigure.connectorAt(destinationPoint.x , destinationPoint.y);//findConnectorAt(destinationPoint.x , destinationPoint.y, AdminWise.adminPanel.routePanel.drsDrawing.drawing());
					//destFigure.connectorAt(destinationPoint.x , destinationPoint.y);
					VWLineConnection startFigure = new VWLineConnection();
					startFigure.connectStart(startConnector1);
					startFigure.connectEnd(endConnector1);
					startFigure.startPoint(originPoint.x , originPoint.y);
					startFigure.endPoint(destinationPoint.x , destinationPoint.y);
					startFigure.updateConnection();        	
					AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(startFigure);
				}catch(Exception ex){
					//System.out.println("Error 1 ********************** " + ex.getMessage());
					ex.printStackTrace();
				}
			}
		try{
			if (startFig != null){
				Point pointOrigin1 = startFig.center();
				Connector startConnector1 = startFig.connectorAt(pointOrigin1.x , pointOrigin1.y);
				Figure destFigure = (Figure) figuresList.get(listTasksModel.get(0).toString());
				Point destinationPoint = destFigure.center();
				Connector endConnector1 =destFigure.connectorAt(destinationPoint.x , destinationPoint.y);// findConnectorAt(destinationPoint.x , destinationPoint.y,  AdminWise.adminPanel.routePanel.drsDrawing.drawing());
				VWLineConnection startFigure = new VWLineConnection();	        	
				startFigure.connectStart(startConnector1);
				startFigure.connectEnd(endConnector1);
				startFigure.startPoint(pointOrigin1.x , pointOrigin1.y);
				startFigure.endPoint(destinationPoint.x , destinationPoint.y);
				startFigure.updateConnection();	    	
				AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(startFigure);
			}
			if (endFig != null){
				startFig = (VWTaskFigure) figuresList.get(listTasksModel.get(listTasksModel.size()-1).toString());
				Point pointOrigin2 = startFig.center();
				Connector startConnector2 =  startFig.connectorAt(pointOrigin2.x , pointOrigin2.y);//findConnectorAt(pointOrigin2.x , pointOrigin2.y,  AdminWise.adminPanel.routePanel.drsDrawing.drawing());	   	    	
				Point destinationPoint1 = endFig.center();
				Connector endConnector2 = endFig.connectorAt(destinationPoint1.x , destinationPoint1.y);//findConnectorAt(destinationPoint1.x , destinationPoint1.y,  AdminWise.adminPanel.routePanel.drsDrawing.drawing());
				VWLineConnection endFigure = new VWLineConnection();	        	
				endFigure.connectStart(startConnector2);
				endFigure.connectEnd(endConnector2);
				endFigure.startPoint(pointOrigin2.x , pointOrigin2.y);
				endFigure.endPoint(destinationPoint1.x , destinationPoint1.y);
				endFigure.updateConnection();	    	
				AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(endFigure);			
			}
		}catch(Exception ex){
			//System.out.println("Error 2 ********************** " + ex.getMessage());
			ex.printStackTrace();
		}
	}		
		
	}
	///Start
	public static void redrawFiguresAfterSeqUpdate(){
		FigureEnumeration fEnum1 = AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().figures();
		Hashtable figuresList = new Hashtable();
		VWFigure startFig = null;
		VWFigure endFig  = null;
		while(fEnum1.hasNextFigure()){    		    		
			Object currentFig = fEnum1.nextFigure();
			if (currentFig instanceof VWFigure) {
				VWFigure currentFigure = (VWFigure) currentFig;
				figuresList.put(currentFigure.getFigureName(), currentFigure);
				if (currentFigure != null  && currentFigure.getFigureName().startsWith("Start_1")){
					startFig = (VWStartFigure)currentFigure;
				}
				else if(currentFigure != null  && currentFigure.getFigureName().startsWith("End_1")){
					endFig = (VWEndFigure)currentFigure;
				}    			
			}else{
				AdminWise.adminPanel.routePanel.drsDrawing.view().drawing().remove((Figure)currentFig);
			}			
			
		}
		//System.out.println("listTasksModel  " + listTasksModel +"listTasksModel  Size " + listTasksModel.size() );
		if (listTasksModel != null && listTasksModel.size() > 0){ 
			
			try{
				
				
				int taskDistStartX = 150;
				int taskDistStartY = 50;
				int taskDistEndX = 250;
				int taskDistEndY = 350;
				Vector figureVector = new Vector();
				for (int indexNo = 0; indexNo < listTasksModel.size(); indexNo++){
					VWFigure taskFigure = (VWFigure) figuresList.get(listTasksModel.get(indexNo).toString());
					//System.out.println(" taskFigure "+taskFigure.getFigureName()+" index "+indexNo);
					int index = indexNo+1;
					if(index<=7){
						taskDistStartX = (index*100)+50;
						taskDistEndX = (index*100)+150;
						taskDistStartY = 50;
						taskDistEndY = 350;
					}else if(index == 8){
						taskDistStartX =(index*100)+50; //850;
						taskDistEndX =(index*100)-50; //750;
						taskDistStartY = 50+100; // 150;
						taskDistEndY = 350+400; // 750;
					}else if(index > 8 && index <= 15){
						taskDistStartX = (index*100)-(100*(index-8))-50;
						taskDistEndX = (index*100)-(200*(index-8))-50;
						taskDistStartY = 50+100;
						taskDistEndY = 350+400;
					}else if(index > 15 && index <= 22){
						taskDistStartX = ((index-15)*100)+50;
						taskDistEndX = ((index-15)*100)-50;
						taskDistStartY = 50+100+100;
						taskDistEndY = 350+400+400;
					}
					//System.out.println(" taskDistStartX:"+taskDistStartX+" taskDistEndX "+taskDistEndX);
					Point eStart = new Point(taskDistStartX, taskDistStartY);
					Point eEnd = new Point(taskDistEndX,taskDistEndY);
					taskFigure.displayBox(eStart, eEnd);
					AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(taskFigure);
					figureVector.add((Figure)taskFigure);
				}
				
				
				for (int index = 0; index < figureVector.size()-1; index++){
					try{
						Figure srcFigure = null;
						Figure destFigure = null;
						srcFigure = (Figure) figureVector.get(index);
						destFigure = (Figure) figureVector.get(index+1);
						Point destinationPoint = destFigure.center();
						Point originPoint = srcFigure.center(); 
						Connector startConnector1 = srcFigure.connectorAt(originPoint.x , originPoint.y);//findConnectorAt(originPoint.x , originPoint.y, AdminWise.adminPanel.routePanel.drsDrawing.drawing());
						//srcFigure.connectorAt(originPoint.x , originPoint.y);	        
						Connector endConnector1 = destFigure.connectorAt(destinationPoint.x , destinationPoint.y);//findConnectorAt(destinationPoint.x , destinationPoint.y, AdminWise.adminPanel.routePanel.drsDrawing.drawing());
						//destFigure.connectorAt(destinationPoint.x , destinationPoint.y);
						VWLineConnection startFigure = new VWLineConnection();
						startFigure.connectStart(startConnector1);
						startFigure.connectEnd(endConnector1);
						startFigure.startPoint(originPoint.x , originPoint.y);
						startFigure.endPoint(destinationPoint.x , destinationPoint.y);
						startFigure.updateConnection();        	
						AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(startFigure);
					}catch(Exception ex){
						//System.out.println("Error 1 ********************** " + ex.getMessage());
						ex.printStackTrace();
					}
				}
				if (startFig != null){
					Point pointOrigin1 = startFig.center();
					Connector startConnector1 = startFig.connectorAt(pointOrigin1.x , pointOrigin1.y);
					Figure destFigure = (Figure) figuresList.get(listTasksModel.get(0).toString());
					Point destinationPoint = destFigure.center();
					Connector endConnector1 =destFigure.connectorAt(destinationPoint.x , destinationPoint.y);// findConnectorAt(destinationPoint.x , destinationPoint.y,  AdminWise.adminPanel.routePanel.drsDrawing.drawing());
					VWLineConnection startFigure = new VWLineConnection();	        	
					startFigure.connectStart(startConnector1);
					startFigure.connectEnd(endConnector1);
					startFigure.startPoint(pointOrigin1.x , pointOrigin1.y);
					startFigure.endPoint(destinationPoint.x , destinationPoint.y);
					startFigure.updateConnection();	    	
					AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(startFigure);
				}
				if (endFig != null){
					startFig = (VWTaskFigure) figuresList.get(listTasksModel.get(listTasksModel.size()-1).toString());
					Point pointOrigin2 = startFig.center();
					Connector startConnector2 =  startFig.connectorAt(pointOrigin2.x , pointOrigin2.y);//findConnectorAt(pointOrigin2.x , pointOrigin2.y,  AdminWise.adminPanel.routePanel.drsDrawing.drawing());	   	    	
					Point destinationPoint1 = endFig.center();
					Connector endConnector2 = endFig.connectorAt(destinationPoint1.x , destinationPoint1.y);//findConnectorAt(destinationPoint1.x , destinationPoint1.y,  AdminWise.adminPanel.routePanel.drsDrawing.drawing());
					VWLineConnection endFigure = new VWLineConnection();	        	
					endFigure.connectStart(startConnector2);
					endFigure.connectEnd(endConnector2);
					endFigure.startPoint(pointOrigin2.x , pointOrigin2.y);
					endFigure.endPoint(destinationPoint1.x , destinationPoint1.y);
					endFigure.updateConnection();	    	
					AdminWise.adminPanel.routePanel.drsDrawing.drawing().add(endFigure);			
				}
			}catch(Exception ex){
				//System.out.println("Error 2 ********************** " + ex.getMessage());
				ex.printStackTrace();
			}
		}		
		
	}
	///End
	
	protected static Connector findConnectorAt(int x, int y, Drawing drawing) {
		FigureEnumeration fe = drawing.figuresReverse();
		while (fe.hasNextFigure()) {
			Figure figure = fe.nextFigure();
			if (figure.canConnect() && figure.containsPoint(x, y)) {
				return figure.connectorAt(x, y);
			}
		}
		return null;
	}
		
	public void setRejectTaskIdIfTaskSequenceChanged(){
		if(taskSequenceChanged){
			try{
				//START 
				if ((AdminWise.adminPanel.routePanel.routeInfoBean != null)) {
					if ((AdminWise.adminPanel.routePanel.alRouteTaskInfo) != null && (AdminWise.adminPanel.routePanel.alRouteTaskInfo.size() > 0)) {
						for (int count = 0; count < AdminWise.adminPanel.routePanel.alRouteTaskInfo.size(); count++) {
							RouteTaskInfo curTskInfo = (RouteTaskInfo) AdminWise.adminPanel.routePanel.alRouteTaskInfo.get(count);
							String taskName = curTskInfo.getTaskName();
							// Set OnRejectTaskId to [-1] if task sequence is changed.
							if(curTskInfo.getOnRejectTaskId()>0){
								active = false;
								JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("VWStartIconProperties.msg_16")+taskName+AdminWise.connectorManager.getString("VWStartIconProperties.msg_17"));
								active = true;
								curTskInfo.setOnRejectTaskId(-1);
								AdminWise.adminPanel.routePanel.alRouteTaskInfo.remove(count);
								AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(count, curTskInfo);
							}
						}
					}
				}
			}catch(Exception e){
				//JOptionPane.showMessageDialog(null,"Exception in setRejectTaskIdIfTaskSequenceChanged() Method :"+e.toString());
			}
			//END 
		}
	}
		
	//Need to look later that this method is required or not.
	// Nishad Nambiar
	public void updateCurrentRouteTaskSequence() {
		int curTaskSeqId = 0;
		if ((AdminWise.adminPanel.routePanel.routeInfoBean != null)) {
			if ((AdminWise.adminPanel.routePanel.alRouteTaskInfo) != null
					&& (AdminWise.adminPanel.routePanel.alRouteTaskInfo.size() > 0)) {
				for (int count = 0; count < AdminWise.adminPanel.routePanel.alRouteTaskInfo.size(); count++) {
					try {
						RouteTaskInfo curTskInfo = (RouteTaskInfo) AdminWise.adminPanel.routePanel.alRouteTaskInfo
						.get(count);
						int curTaskFigureId = curTskInfo.getTaskFigureId();
						curTaskSeqId = getTaskSequence(curTaskFigureId);
						curTskInfo.setTaskSequence(curTaskSeqId);
						
						// Set OnRejectTaskId to curTaskSeqId if any task is
						// selected for reject.
						/*if (curTskInfo.getOnRejectTaskId() > 0) {
							curTskInfo.setOnRejectTaskId(curTaskSeqId);
						}*/
						AdminWise.adminPanel.routePanel.alRouteTaskInfo.remove(count);
						AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(count, curTskInfo);
					} catch (Exception e) {
						//System.out.println("error :"+e.toString());
					}
				}
			}
		}
	}
	
	public int getTaskSequence(int figureId) {
		int taskSequence = 0;
		for (int count = 0; count < AdminWise.adminPanel.routePanel.alRouteTaskSequence.size(); count++) {
			int curTaskFigureId = Integer.parseInt(String
					.valueOf(AdminWise.adminPanel.routePanel.alRouteTaskSequence.get(count)));
			if (curTaskFigureId == figureId) {
				taskSequence = (count) + 1;
				break;
			}
		}
		return taskSequence;
	}
	
	void BtnSave_actionPerformed(java.awt.event.ActionEvent event){
		if(!validateData()){
			return;
		}
		setRejectTaskIdIfTaskSequenceChanged();
		updateCurrentRouteTaskSequence();
		String taskName = vwTaskIconProperties.resetPreviousRejectAction();
		if(taskName!=null && !taskName.trim().equals(""))
		{
			active = false;
    		JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("VWStartIconProperties.msg_12")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWStartIconProperties.msg_13")+taskName+"'.");
			active = true;
		}
		int selectedAction = cmbRouteStart.getSelectedIndex();
		try{
			AdminWise.adminPanel.routePanel.routeInfoBean.setRouteId(AdminWise.adminPanel.routePanel.routeInfoBean.getRouteId());
			AdminWise.adminPanel.routePanel.routeInfoBean.setStartAction(String.valueOf(selectedAction).trim());
			//int actionLocationNodeId = -1;	    	
			if(selectedAction==0){
				//actionLocationNodeId = dlgLocation.tree.getSelectedNodeId();
				System.out.println(" startLocationNodeId "+startLocationNodeId+ "  current "+currentStartLocationNodeId);
				if(!startLocationNodeId.trim().equals("") && !VWRouteConnector.isUniqueStartLocation(String.valueOf(startLocationNodeId), AdminWise.adminPanel.routePanel.routeInfoBean.getRouteId())){
					active = false;
					VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWStartIconProperties.msg_14")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".", VWMessage.DIALOG_TYPE);
					active = true;
					//startLocationNodeId = currentStartLocationNodeId;
					AdminWise.adminPanel.routePanel.routeInfoBean.setStartLocation(currentStartLocationNodeId.trim());	
					return;
				}
				if(startLocationNodeId.trim().equals("")){
						startLocationNodeId = currentStartLocationNodeId;
						AdminWise.printToConsole("startLocationNodeId::::"+startLocationNodeId);
				}
				AdminWise.adminPanel.routePanel.routeInfoBean.setStartLocation(String.valueOf(startLocationNodeId));
			}else if(selectedAction==1 || selectedAction==2){
				AdminWise.adminPanel.routePanel.routeInfoBean.setStartLocation("-1");
			}
			if(taskSequenceChanged){
				redrawFiguresAfterSeqUpdate();
				taskSequenceChanged = false;
			}
		}catch(Exception ex){
			System.out.println("Error in location "+ex.toString());
		}	
		if(selectedAction==2){
				RouteIndexInfo indexInfoBean = new RouteIndexInfo();
				ArrayList alRouteIndexInfo = new ArrayList();
				AdminWise.printToConsole("tblConditions.getRowCount()::::"+tblConditions.getRowCount());
				
			if (tblConditions.getRowCount() == 0) {

				tblConditions.clearData();
				tblConditions.clearSelection();
				indexInfoBean = new RouteIndexInfo();
				indexInfoBean.setRouteId(0);
				AdminWise.printToConsole("tblConditions::::" + tblConditions);
				AdminWise.printToConsole("tblConditions.getRowCount()::::"
						+ tblConditions.getRowCount());
				try {
					VWDocTypeRec selDocType = (VWDocTypeRec) cmbDocTypesList
							.getSelectedItem();
					AdminWise.printToConsole("selDocType::::" + selDocType);
					int docTypeId = selDocType.getId();
					AdminWise.printToConsole("docTypeId::::" + docTypeId);
					indexInfoBean.setDocTypeId(docTypeId);
					indexInfoBean.setIndexName("-");
					indexInfoBean.setIndexId(0);
					indexInfoBean.setIndexTypeId(0);
					indexInfoBean.setComparisonOperator("-");
					/**
					 * SOW:-2099-961 Enhancement for Workflow Stars before
					 * Condition added for starts before also like starts after
					 */
					indexInfoBean.setIndexValue1("-");

					String value2 = String.valueOf(tblConditions.getValueAt(0,
							4));
					AdminWise.printToConsole("value2::::" + value2);
					indexInfoBean.setIndexValue2("-");
					indexInfoBean.setLogicalOperator("");
					alRouteIndexInfo.add(indexInfoBean);
					AdminWise.printToConsole("alRouteIndexInfo::::"
							+ alRouteIndexInfo);
				} catch (Exception e) {
					System.out.println("Error in index route save "
							+ e.toString());
					AdminWise.printToConsole("Error in index route save "
							+ e.toString());
				}

			} else {
				for(int count=0;count<tblConditions.getRowCount();count++){
					indexInfoBean = new RouteIndexInfo();
					indexInfoBean.setRouteId(0);    			
					try{
						VWDocTypeRec selDocType=(VWDocTypeRec) cmbDocTypesList.getSelectedItem();
						int docTypeId = selDocType.getId();					
						indexInfoBean.setDocTypeId(docTypeId);
						String indexName = String.valueOf(tblConditions.getValueAt(count,1));
						AdminWise.printToConsole("indexName More Than 0::::"+indexName);
						if(indexName.equalsIgnoreCase("")){
							indexInfoBean.setIndexName("-");
						}else {
							indexInfoBean.setIndexName(indexName);	
						}
						
						int indexId = Integer.parseInt(""+tblConditions.getValueAt(count,tblConditions.m_data.COL_INDICEID));
						int indexTypeId = Integer.parseInt(""+tblConditions.getValueAt(count,tblConditions.m_data.COL_INDEXTYPEID));
						AdminWise.printToConsole("indexTypeId More Than 0::::"+indexTypeId);
						
						if(indexId <= 0) {
							indexInfoBean.setIndexId(0);
						} else {
							indexInfoBean.setIndexId(indexId);
						}
						 if(indexTypeId <= 0){
							 indexInfoBean.setIndexTypeId(0);	
						 } else {
							 indexInfoBean.setIndexTypeId(indexTypeId);	
						 }
						String comparisonOp = String.valueOf(tblConditions.getValueAt(count,2));
						AdminWise.printToConsole("comparisonOp More Than 0::::"+comparisonOp);
						if(comparisonOp.equalsIgnoreCase("")){
							indexInfoBean.setComparisonOperator("-");
						} else {
							indexInfoBean.setComparisonOperator(comparisonOp);
						}
						
						String value1 = String.valueOf(tblConditions.getValueAt(count,3));
						/**
						 * SOW:-2099-961 Enhancement for Workflow Stars before 
						 * Condition added for starts before also like starts after
						 */
						if(indexTypeId == 2 && (!((comparisonOp.trim().equalsIgnoreCase(COND_STARTS_AFTER_STR))||(comparisonOp.trim().equalsIgnoreCase(COND_STARTS_BEFORE_STR)))))
							value1 = com.computhink.common.util.VWUtil.convertDate(value1,"MM/dd/yyyy","yyyyMMdd");
						AdminWise.printToConsole("value1 Inside IndexType=2 More Than 0::::"+value1);
						
						if(value1.equalsIgnoreCase("")){
							indexInfoBean.setIndexValue1("");
						} else {
							indexInfoBean.setIndexValue1(value1);
						}
						
						String value2 = String.valueOf(tblConditions.getValueAt(count,4));
						if(indexTypeId == 2 && comparisonOp.trim().equalsIgnoreCase(COND_BETWEEN_STR))
							value2 = com.computhink.common.util.VWUtil.convertDate(value2,"MM/dd/yyyy","yyyyMMdd");
						if(value2.trim().equalsIgnoreCase(""))
							indexInfoBean.setIndexValue2("-");
						else
							indexInfoBean.setIndexValue2(value2);   			
						String logOp = String.valueOf(tblConditions.getValueAt(count,0));
						indexInfoBean.setLogicalOperator(logOp.trim());
						alRouteIndexInfo.add(indexInfoBean);
					}catch(Exception e){
						System.out.println("Error in index route save "+e.toString());
					}
				}
		}
				AdminWise.adminPanel.routePanel.routeInfoBean.setRouteIndexList(alRouteIndexInfo);
			}
			
			//Set Route Task SequenceID
			AdminWise.adminPanel.routePanel.alRouteTaskSequence = new ArrayList(); 
			for(int count=0;listTasksModel!=null&&count<listTasksModel.size();count++){
				try{
					String currentTaskName = String.valueOf(listTasksModel.get(count));
					int taskId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(currentTaskName);
					AdminWise.adminPanel.routePanel.alRouteTaskSequence.add(taskId);
				}catch(Exception ex){
					//JOptionPane.showMessageDialog(null,"Saving Ex 1"+ex.toString());
				}
			}
			vecSequence = new Vector();		
			if(vecSequence.size()==0){
				for(int count=0;count<listTasksModel.size();count++){
					vecSequence.add(listTasksModel.get(count));
				}
			}
			curDocTypeSelected = -1;
			setVisible(false);
			dispose();
	} 
	void BtnRemove_actionPerformed(java.awt.event.ActionEvent event){
		try{					
			if (tblConditions == null || tblConditions.getSelectedRows().length <= 0){
						active =false;
						JOptionPane.showMessageDialog(VWStartIconProperties.this, AdminWise.connectorManager.getString("VWStartIconProperties.msg_15"));
						active = true;
						return;
			}
			int selRows[] = tblConditions.getSelectedRows();
			for(int curRow=(selRows.length-1);curRow>=0;curRow--){
				vecTableData.remove(selRows[curRow]);
				tblConditions.m_data.delete(selRows[curRow]);						
				tblConditions.m_data.fireTableDataChanged();
			}
			if(tblConditions.getRowCount()==0){
				curDocTypeSelected = -1;
			}
		}catch(Exception e){
			//AdminWise.printToConsole("Error :"+e.toString());
		}			
	}
	
	public int getConditionalOperator(String operator){
		int result = -1;
		if(operator.trim().equalsIgnoreCase(">"))
			result =0;
		else if(operator.trim().equalsIgnoreCase(">="))
			result =1;
		else if(operator.trim().equalsIgnoreCase("<"))
			result =2;
		else if(operator.trim().equalsIgnoreCase("<="))
			result =3;
		else if(operator.trim().equalsIgnoreCase("="))
			result =4;
		else if(operator.trim().equalsIgnoreCase("!="))
			result =5;
		
		return result;
	}
	
	public int getLogicalOperator(String operator){
		int result = -1;
		if(operator.trim().equalsIgnoreCase("AND"))
			result =0;
		return result;
	}
	
	
	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event){
		try{
			startLocationNodeId = currentStartLocationNodeId;
			txtRouteLocation.setText(currentStartLocationNodepath);
			listTasksModel.clear();
			for(int count=0;count<vecSequence.size();count++){
				listTasksModel.addElement(vecSequence.get(count));
			}
		}catch(Exception e){
			//System.out.println("Exception in BtnCancel_actionPerformed() Method :"+e.toString());
		}
		curDocTypeSelected = -1;
		setVisible(false);
	}
	void BtnRouteLocation_actionPerformed(java.awt.event.ActionEvent event,String location) {
		String selectedOption=this.cmbRouteStart.getSelectedItem().toString();	
		//For File Dialog
		if( (selectedOption.equalsIgnoreCase(VWConstant.lstStartWhen[0]) )){
			if (dlgLocation != null) {
				active = false;
				dlgLocation.loadDlgData();
				setCurrentLocation(dlgLocation);
				dlgLocation.setVisible(true);
			} else {
				active = false;
				dlgLocation = new VWDlgSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("dlgretentionsetting.dlgLocation"));
				setCurrentLocation(dlgLocation);
				dlgLocation.setVisible(true);
			}
			if (dlgLocation.cancelFlag || !dlgLocation.cancelFlag){
				active = true;
			}
			if (dlgLocation.cancelFlag){
				return;
			}
			if (location.equals(LBL_StartLocation)){			
				txtRouteLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
				startLocationNodeId = dlgLocation.tree.getSelectedNodeId()+"";
				//startNodePath = dlgLocation.tree.getSelectedNodeStrPath();
				active = true;
			}
		}
	}
	
	private class KeyBuffer extends Thread
	{
		private String prefix = "";
		private boolean run = true;
		public KeyBuffer()
		{
			setPriority(Thread.MIN_PRIORITY);
		}
		public void appendKey(char c)
		{
			prefix+=c;
		}
		public void kill()
		{
			run = false;
		}
		public void run()
		{
			while (run)
			{
				Util.sleep(100);
				if (prefix.length() == 0) continue;
				prefix = "";
			}
		}
	}
	public VWDlgSaveLocation dlgLocation = null;
	JLabel lblStartWhen = new JLabel();
	JLabel lblAssignedRoutes = new JLabel();
	JLabel lblTriggerAt = new JLabel();
	JLabel lblDocTypes = new JLabel();
	JLabel lblIndexField = new JLabel();
	JLabel lblCondition = new JLabel();
	javax.swing.ButtonGroup group = new javax.swing.ButtonGroup();
	JRadioButton rdoRouteStartsInFolder = new JRadioButton();
	JRadioButton rdoRouteStartManual = new JRadioButton();
	JRadioButton rdoRouteStartIndexValue = new JRadioButton();
	JComboBox cmbRouteStart = new JComboBox();
	//JComboBox cmbIndexFields = new JComboBox();
	javax.swing.JScrollPane SPAvailableRoute = new javax.swing.JScrollPane();
	javax.swing.JScrollPane SPAssignedRoute = new javax.swing.JScrollPane();
	
	VWLinkedButton BtnSave = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
	VWLinkedButton BtnRouteLocation = new VWLinkedButton(2,true);
	static JTextField txtRouteLocation = new JTextField();
	
	VWDocTypeRec vwDocTypeRec = null;
	JPanel panelStartIconRoute = new JPanel();
	JPanel panelSeqTask = new JPanel();
	String startLocationNodeId = "";
	String currentStartLocationNodeId = "";
	String currentStartLocationNodepath = "";
	int currentDocTypeId = -1;
	public static Vector vecSequence = new Vector();
	public static VWTaskIconProperties vwTaskIconProperties = new VWTaskIconProperties();

	//Set the current location where this dialog box should be loaded.
	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width,this.getSize().height);
		setLocation(point);
	}
	
	//Set the current location where this dialog box should be loaded.
	public void setCurrentLocation(VWDlgSaveLocation dlg){
		Point point = AdminWise.adminFrame.getCurrentLocation(dlg.getSize().width,dlg.getSize().height);
		dlg.setLocation(point);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
			/**
			 * Reading the registry for setting the fontname and fontsize for diagram
			 * implemented for chinese localization
			 * Modified by Madhavan
			 */
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
	        	fontName = VWCPreferences.getFontName();
	            setUIFont(new FontUIResource(fontName, Font.PLAIN, VWCPreferences.getFontSize()));
		}
		catch(Exception se){
		}
		JFrame frame = new JFrame();
		new VWImages();
		VWStartIconProperties setting = new VWStartIconProperties(null, 12);
		//frame.add(setting);
		setting.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.pack();
	}
	public static void setUIFont(javax.swing.plaf.FontUIResource f) {
    	java.util.Enumeration keys = UIManager.getDefaults().keys();
    	while (keys.hasMoreElements()) {
    	    Object key = keys.nextElement();
    	    Object value = UIManager.get(key);
    	    
    	    if (value instanceof javax.swing.plaf.FontUIResource){		
    		UIManager.put(key, f);
    	    }
    	}
        }
	
}
