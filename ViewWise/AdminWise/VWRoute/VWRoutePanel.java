package ViewWise.AdminWise.VWRoute;

/**
 * @author Pandiya Raj.M
 * 
 *	VWRoutePanel class used to Create/Update/Delete route.  
 * 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;

import org.jhotdraw.framework.DrawingView;
import org.jhotdraw.framework.FigureAttributeConstant;
import org.jhotdraw.framework.FigureEnumeration;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDlgSaveLocation;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWPrincipal.VWPrincipalConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.figures.VWEndFigure;
import ViewWise.AdminWise.VWRoute.figures.VWStartFigure;
import ViewWise.AdminWise.VWRoute.figures.VWTaskFigure;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

import com.computhink.common.Constants;
import com.computhink.common.Creator;
import com.computhink.common.Index;
import com.computhink.common.Node;
import com.computhink.common.Principal;
import com.computhink.common.RouteIndexInfo;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteInvalidTaskInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.RouteUsers;
import com.computhink.common.Util;
import com.computhink.common.util.JTextFieldLimit;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCPreferences;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;

public class VWRoutePanel extends JPanel implements VWConstant, Constants {

	Lock lock1 = new ReentrantLock();
	int routeReturnValue=0;
	public static boolean sidbiCustomizationFlag = false;
	public VWRoutePanel() {
		setupUI();
	}
	
	public void setupUI() {
		int top = 116; int hGap = 28;
		if (UILoaded)
			return;
		int height = 24, width = 144, left = 12, heightGap = 30;
		setLayout(new BorderLayout());
		VWRouteOption.setLayout(null);
		add(VWRouteOption, BorderLayout.CENTER);
		VWRouteOption.setBounds(0, 0, 605, 433);
		PanelCommand.setLayout(null);
		VWRouteOption.add(PanelCommand);
		PanelCommand.setBackground(AdminWise.getAWColor());
		PanelCommand.setBounds(0, 0, 168, 432);
/*		Pic.setIconTextGap(0);
		Pic.setAutoscrolls(true);
		PanelCommand.add(Pic);
		Pic.setBounds(8,4,149,92);*/
		
	    PanelCommand.add(picPanel);
	    picPanel.setBounds(8,16,149,92);
	        
		lblAvailableRoute.setText(LBL_AVAILABLEROUTES);
		PanelCommand.add(lblAvailableRoute);
		lblAvailableRoute.setBackground(java.awt.Color.white);
		lblAvailableRoute.setBounds(left, top, width, height);
		
		top += hGap;
		PanelCommand.add(cmbAvailableRoute);
		cmbAvailableRoute.setBackground(java.awt.Color.white);
		cmbAvailableRoute.setBounds(left, top, width, height - 2);
		cmbAvailableRoute.addItem(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")));
		
		top += hGap + 12;
		BtnNewRoute.setText(BTN_NEWROUTE_NAME);
		BtnNewRoute.setIcon(VWImages.NewRouteIcon);
		BtnNewRoute.setActionCommand(BTN_NEWROUTE_NAME);
		PanelCommand.add(BtnNewRoute);
		BtnNewRoute.setBounds(left, top, width, height);
		BtnNewRoute.setIcon(VWImages.DocLockIcon);
		
		top += hGap;
		BtnUpdate.setText(BTN_UPDATE_NAME);
		BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
		PanelCommand.add(BtnUpdate);
		BtnUpdate.setBounds(left, top, width, height);
		BtnUpdate.setIcon(VWImages.DocLockIcon);
		
		top += hGap;
		BtnDelete.setText(BTN_DELETE_NAME);
		BtnDelete.setActionCommand(BTN_DELETE_NAME);
		PanelCommand.add(BtnDelete);
		BtnDelete.setBounds(left, top, width, height);
		BtnDelete.setIcon(VWImages.DelIcon);
		
		top += hGap;
		BtnSaveAs.setText(BTN_SAVE_AS_NAME);
		BtnSaveAs.setActionCommand(BTN_SAVE_AS_NAME);
		PanelCommand.add(BtnSaveAs);
		BtnSaveAs.setBounds(left, top, width, height);
		BtnSaveAs.setIcon(VWImages.SaveAsIcon);
		
		top += hGap;
		BtnReviewUpdate.setText(BTN_Review_Update);
		BtnReviewUpdate.setActionCommand(BTN_Review_Update);
		PanelCommand.add(BtnReviewUpdate);
		BtnReviewUpdate.setBounds(left, top, width, height);
		BtnReviewUpdate.setIcon(VWImages.SaveAsIcon);
		BtnReviewUpdate.setEnabled(false);
		BtnReviewUpdate.addActionListener(new SymAction());
		
		top += hGap;
		BtnProgressClick.setText("");
		PanelCommand.add(BtnProgressClick);
		BtnProgressClick.setBounds(left, top, width, height);
		BtnProgressClick.setVisible(false);
		BtnProgressClick.addActionListener(new SymAction());
		top += heightGap-20 ;
		BtnDocInRoute.setText(BTN_DOCINROUTE_NAME);
		BtnDocInRoute.setActionCommand(BTN_DOCINROUTE_NAME);
		PanelCommand.add(BtnDocInRoute);
		BtnDocInRoute.setBounds(left, top, width+6, height);
		BtnDocInRoute.setIcon(VWImages.DocumentInRouteIcon);
		
		top += hGap +7;
		BtnAccept.setText(BTN_ACCEPTNAME);
		BtnAccept.setActionCommand(BTN_ACCEPTNAME);
		PanelCommand.add(BtnAccept);
		BtnAccept.setBounds(left, top, width, height);
		BtnAccept.setIcon(VWImages.AcceptIcon);
		
		top += hGap;
		BtnReject.setText(BTN_REJECTNAME);
		BtnReject.setActionCommand(BTN_REJECTNAME);
		PanelCommand.add(BtnReject);
		BtnReject.setBounds(left, top, width, height);
		BtnReject.setIcon(VWImages.RejectIcon);
		
		top += hGap;
		BtnEndRoute.setText(BTN_ENDROUTENAME);
		BtnEndRoute.setActionCommand(BTN_ENDROUTENAME);
		PanelCommand.add(BtnEndRoute);
		BtnEndRoute.setBounds(left, top, width, height);
		BtnEndRoute.setIcon(VWImages.EndRouteIcon);
		
		top += hGap;
		BtnSummary.setText(BTN_SUMMARYNAME);
		BtnSummary.setActionCommand(BTN_SUMMARYNAME);
		PanelCommand.add(BtnSummary);
		BtnSummary.setBounds(left, top, width, height);
		BtnSummary.setIcon(VWImages.SummaryIcon);

		top += hGap + 12;
		BtnRouteCompletedDocs.setText(BTN_ROUTECOMPLETED_NAME);
		BtnRouteCompletedDocs.setActionCommand(BTN_ROUTECOMPLETED_NAME);
		PanelCommand.add(BtnRouteCompletedDocs);
		BtnRouteCompletedDocs.setBounds(left, top, width, height);
		BtnRouteCompletedDocs.setIcon(VWImages.SummaryIcon);
		BtnRouteCompletedDocs.setEnabled(false);
		
		top += hGap;
		BtnTaskCompletedDocs.setText(BTN_TASKCOMPLETED_NAME);
		BtnTaskCompletedDocs.setActionCommand(BTN_TASKCOMPLETED_NAME);
		PanelCommand.add(BtnTaskCompletedDocs);
		BtnTaskCompletedDocs.setBounds(left, top, width, height);
		BtnTaskCompletedDocs.setIcon(VWImages.SummaryIcon);
		BtnTaskCompletedDocs.setEnabled(false);
		
		top += hGap;
		BtnSaveAsHTML.setText(BTN_SAVE_AS_HTML);
		BtnSaveAsHTML.setActionCommand(BTN_SAVE_AS_HTML);
		PanelCommand.add(BtnSaveAsHTML);
		BtnSaveAsHTML.setBounds(left, top, width, height);
		BtnSaveAsHTML.setEnabled(false);
		BtnSaveAsHTML.setIcon(VWImages.SaveAsIcon);
		BtnSaveAsHTML.setToolTipText(AdminWise.connectorManager.getString("VWRoutePanel.BtnSaveAsHTMLTooltip"));
		
		top += heightGap;
		PanelTable.setLayout(new BorderLayout());
		VWRouteOption.add(PanelTable);
		PanelTable.setBounds(168, 0, 330, 330);
		SPTable.setBounds(0, 20, 1000, 740);
				
		PanelTable.add(lblTableHeader, BorderLayout.NORTH);
		lblTableHeader.setBackground(Color.WHITE);
		lblTableHeader.setForeground(new Color(7, 38, 58));
		PanelTable.setBackground(Color.WHITE);
		PanelTable.add(SPTable, BorderLayout.CENTER);
		initRoute();
		//getGroupsAndUsers();
		SPTable.getViewport().add(Table);
		PanelRouteConfig.setVisible(false);
		setEnableMode(MODE_UNCONNECT);
		SymComponent aSymComponent = new SymComponent();
		VWRouteOption.addComponentListener(aSymComponent);
		Pic.setIcon(VWImages.DocRouteImage);
		SymAction lSymAction = new SymAction();
		BtnNewRoute.addActionListener(lSymAction);
		BtnDelete.addActionListener(lSymAction);
		BtnSaveAs.addActionListener(lSymAction);
		BtnSaveAsHTML.addActionListener(lSymAction);
		BtnUpdate.addActionListener(lSymAction);
		BtnDocInRoute.addActionListener(lSymAction);
		BtnAccept.addActionListener(lSymAction);
		BtnReject.addActionListener(lSymAction);
		BtnSummary.addActionListener(lSymAction);
		BtnEndRoute.addActionListener(lSymAction);
		BtnRouteCompletedDocs.addActionListener(lSymAction);
		BtnTaskCompletedDocs.addActionListener(lSymAction);
		
		Table.getParent().setBackground(java.awt.Color.white);
		PanelRouteConfig.setAutoscrolls(true);
		PanelRouteConfig.setPreferredSize(new Dimension(900, 840));//1040
		SPTable.setPreferredSize(new Dimension(700, 500));
		
		repaint();
		doLayout();
		// Always need to load docs in route list on connection
		//PR Comment
		loadDocumentsInRoute();
		UILoaded = true;
	}
	
	private void initRoute() {
		int height = 20, width = 90, left = 0, top = 0, txtWidth = 400, heightGap = 30, leftGap = 60, lstWidth = 175, lstHeight = 170, brwBtnWidth = 24, brwBtnHeight = 20, topInvalid = 0;		
		PanelRouteConfig.setLayout(null);
		routeConfigLPanel.setLayout(null);
		routeConfigLPanel.setBounds(0, 0, 900, 50);
		JLabel routeconfigLbl = new JLabel();
		routeconfigLbl.setBounds(0, 0, 322, 57);
		routeconfigLbl.setIcon(VWImages.RouteConfigIcon);
		routeconfigLbl.setIconTextGap(0);
		routeConfigLPanel.add(routeconfigLbl);
		routeConfigLPanel.setBackground(Color.WHITE);
		PanelRouteConfig.add(routeConfigLPanel);
		PanelRouteConfig.setBackground(Color.WHITE);
		left = 27;
		top = 85;
		leftGap = 27;
		// left = leftGap;
		PanelRouteConfig.add(lblRouteName);
		lblRouteName.setBounds(left, top, width, height);
		lblRouteName.setText(LBL_ROUTENAME);
		String localLanguage=AdminWise.getLocaleLanguage();
		PanelRouteConfig.add(txtRouteName);
		left = left + width + 20;
		if(localLanguage.equals("nl_NL")){
			txtRouteName.setBounds(left+30, top, txtWidth, height);	
		}else
		txtRouteName.setBounds(left, top, txtWidth, height);
		txtRouteName.setDocument(new JTextFieldLimit(50));
		txtRouteName.setToolTipText(AdminWise.connectorManager.getString("VWRoutePanel.txtRouteName"));
		
		left = leftGap;
		top = top + heightGap;
		
		PanelRouteConfig.add(lblRouteDesc);
		
		if(localLanguage.equals("nl_NL")){
			lblRouteDesc.setBounds(left, top, width + 8+30, height);
		}else
		lblRouteDesc.setBounds(left, top, width + 8, height);
		lblRouteDesc.setText(LBL_ROUTEDESC);
		
		PanelRouteConfig.add(spRouteDesc);
		left = left + width + 10 + 10;
		if(localLanguage.equals("nl_NL")){
			spRouteDesc.setBounds(left+30, top, txtWidth, height + 20);
		}else
		spRouteDesc.setBounds(left, top, txtWidth, height + 20);
		spRouteDesc.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		taRouteDesc.setDocument(new JTextFieldLimit(255));
		taRouteDesc.setLineWrap(true);
		taRouteDesc.setToolTipText(AdminWise.connectorManager.getString("VWRoutePanel.taRouteDesc"));
	
		top = top + heightGap + 15	;
		left = left - (width+20);
		PanelRouteConfig.add(lblRouteInvalid);
		
		invalidX = left; invalidY = top+5 ; invalidWidth = width; invalidHeight = height; 
		validWidth = width; validHeight = height;
		lblRouteInvalid.setText("");
		
		PanelRouteConfig.add(lblTaskInvalid);
		lblTaskInvalid.setText("");
		
		int curTop = top + heightGap-15;
		
		SymAction lSymAction = new SymAction();
		cmbAvailableRoute.addActionListener(lSymAction);
		left = leftGap;
		top = top + heightGap;
		try {			
			pnlDRSDrawing = drsDrawing.getDRSDrawing();
			drsDrawing.init();
			//drsDrawing.setBorder(new LineBorder(Color.RED));			
			routeDiagramX = left; routeDiagramY = curTop + 28; routeDiagramWidth = 850; routeDiagramHeight = 450; stdRouteY = curTop+29;
			drsDrawing.setBounds(left, curTop + 28, 850, 450);
			drsDrawing.setBackground(Color.WHITE);
			PanelRouteConfig.add((JPanel) drsDrawing);
		} catch (Exception e) {
			// JOptionPane.showMessageDialog(null,e.toString());
		}
		btnEnlarge.setBounds(left+860, curTop + 20, 40, 24);
		btnEnlarge.addActionListener(new SymAction());
		//PanelRouteConfig.add(btnEnlarge);
	}
	
	private void loadAvailableUsers() {
		Vector users = new Vector();
		try {
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			
			if (room == null || room.getConnectStatus() != Room_Status_Connect)
				return;
			gCurRoom = room.getId();
			AdminWise.gConnector.ViewWiseClient
			.getCreators(room.getId(), users);
			availableListModel.removeAllElements();
			for (int i = 0; i < users.size(); i++) {
				availableListModel.addElement(users.get(i));
			}
			int approversSize = this.vApproversList.size();
			int availableSize = this.availableListModel.size();
			for (int x = 0; x < approversSize; x++) {
				String currentApprover = String.valueOf(
						this.vApproversList.get(x)).trim();
				for (int y = 0; y < availableListModel.size(); y++) {
					if (String.valueOf(availableListModel.get(y)).trim()
							.equalsIgnoreCase(currentApprover.trim())) {
						availableListModel.remove(y);
					}
				}
			}
			
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception ex " + ex.getMessage());
		}
	}
	
	private void loadAvailableRoutes() {
		routeLoaded = false;
		try {
			Vector routeList = new Vector();
			Vector vRouteList = new Vector();
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			if (room == null || room.getConnectStatus() != Room_Status_Connect)
				return;
			gCurRoom = room.getId();
			AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, -1,
					vRouteList);
			
			// called this method, to sort the routelist.
			routeList = sortRouteList(vRouteList);
			
			if (routeList != null) {
				cmbAvailableRoute.removeAllItems();
				// AdminWise.printToConsole(" Add Available Routes ");
				cmbAvailableRoute.addItem(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")));
				for (int i = 0; i < routeList.size(); i++)
					cmbAvailableRoute.addItem(routeList.get(i));
			} else {
				cmbAvailableRoute.removeAllItems();
				cmbAvailableRoute.addItem(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")));
			}
		} catch (Exception ex) {
		}
		routeLoaded = true;
	}
	
	// Desc :This method sorts the routelist, and returns back the sorted
	// routelist.
	// Author :Nishad Nambiar
	// Date :15-Oct-2007
	private Vector sortRouteList(Vector vRouteList) {
		Vector drsRouteList = new Vector();
		try {
			RouteInfo rInfo[] = new RouteInfo[vRouteList.size()];
			for (int count = 0; count < vRouteList.size(); count++) {
				rInfo[count] = (RouteInfo) vRouteList.get(count);
			}
			Arrays.sort(rInfo);
			for (int count = 0; count < rInfo.length; count++) {
				drsRouteList.add(rInfo[count]);
			}
		} catch (Exception e) {
			// JOptionPane.showMessageDialog(null,e.toString());
		}
		return drsRouteList;
	}
	
	private Object[] getAvailableRoutes() {
		Object objRouteList[] = null;
		try {
			Vector routeList = new Vector();
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			if (room == null || room.getConnectStatus() != Room_Status_Connect)
				// return;
				gCurRoom = room.getId();
			AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, -1,
					routeList);
			if (routeList != null) {
				objRouteList = new Object[routeList.size() + 1];
				objRouteList[0] = (Object) VWConstant.FILTER_ALL_ROUTES;
				for (int curRoute = 1; curRoute <= routeList.size(); curRoute++) {
					objRouteList[curRoute] = routeList.get((curRoute - 1));
				}
			}
		} catch (Exception e) {
			
		}
		return objRouteList;
	}
	
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnNewRoute) {
				BtnNewRoute_actionPerformed(event);
			} else if (object == BtnUpdate) {
				BtnUpdate_actionPerformed(event);
			} else if (object == BtnDelete) {
				BtnDelete_actionPerformed(event);
			} else if (object == cmbAvailableRoute) {
				cmbAvailableRoute_actionPerformed(event);
			} else if (object == BtnDocInRoute) {
				BtnDocInRoute_actionPerformed(event);
			} else if (object == BtnSaveAs) {
				BtnSaveAs_actionPerformed(event);
			} 
			else if(object==BtnSaveAsHTML){
				BtnSaveAsHTML_actionPerformed(event);
			}
			else if (object == BtnAccept) {
				BtnAccept_actionPerformed(event);
			} else if (object == BtnReject) {
				BtnReject_actionPerformed(event);
			} else if (object == BtnSummary) {
				BtnSummary_actionPerformed(event);
			} else if (object == BtnEndRoute) {
				BtnEndRoute_actionPerformed(event);
			}
			else if (object == BtnRouteCompletedDocs) {
				BtnRouteCompletedDocuments_actionPerformed(event);
			}
			else if (object == BtnTaskCompletedDocs) {
				BtnTaskCompletedDocuments_actionPerformed(event);
			}
			else if (object == btnEnlarge) {
				BtnEnlarge_actionPerformed(event);
			}
			else if(object==BtnReviewUpdate){
				BtnReviewUpdate_actionPerformed(event);
			}
			else if(object==BtnProgressClick){
				BtnProgressClick_actionPerformed(event);
			}
			
		}
	}
	
	public void repositionEndFigureWhileUpdate(){		
		//Desc   :Assigning the VWRoutePanel.rFigure object the current end icon, so that End icon can be moved to the 
		//		  further position when new Task buttons are added.
		//Date   :07/07/2008				
		FigureEnumeration fEnum = drsDrawing.view().drawing().figures();
		while(fEnum.hasNextFigure()){
			try{
				VWEndFigure endFigure = null;
				Object currenttFigure = fEnum.nextFigure();
				if (currenttFigure instanceof VWEndFigure)
					VWRoutePanel.rFigure = (VWEndFigure) currenttFigure;
			}catch(Exception e){
				System.out.println("Exception in repositionEndFigureWhileUpdate() Method :"+e.toString());
			}
		}
		//End...				
	}
	
	private void loadRouteInfo(RouteInfo routeInfo){
		Vector approversData= new Vector();
		try{
			ArrayList alUserList = new ArrayList();
			txtRouteName.setText(routeInfo.getRouteName());
			taRouteDesc.setText(routeInfo.getRouteDescription());
			Properties props = System.getProperties(); 
	     	//String imageFilepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + "ViewWise"+ File.separator + "Tested.draw";		
			//String imageFilepath =props.getProperty("user.home")+ File.separator + "RouteImage.draw";
			String imageFilePath = System.getProperty("user.home")+File.separator+"Application Data"
			+File.separator+PRODUCT_NAME+File.separator+"RouteImage"+File.separator+"RouteImage.draw";			
			drsDrawing.promptOpen(imageFilePath,null);	
			drsDrawing.taskCount = (drsDrawing.getTaskCount()-1);
			repositionEndFigureWhileUpdate();
			drsDrawing.setFigureDetails();		
		
		// Using Node Id get the location Path
		Vector locationInfo = new Vector();
		AdminWise.gConnector.ViewWiseClient.getNodeParents(gCurRoom, 
				new Node(routeInfo.getStartLocation()), locationInfo);
		StringBuffer path = new StringBuffer(AdminWise.adminPanel.roomPanel.getSelectedRoom().getName());
		Node node = null;
		for(int i=locationInfo.size()-1;i>=0; i--){
			node = (Node)locationInfo.get(i);
			path.append("\\" + node.getName());
		}		
		VWStartIconProperties.listTasksModel.removeAllElements();
		VWStartIconProperties.loadAvailableSavedTasks();
		
		// below code not used. check
		alUserList = routeInfo.getListOfRouteUsers();
		if(this.vApproversList!=null){
			if(this.vApproversList.size() > 0){
				this.vApproversList.removeAllElements();
			}
			Creator users = null;
			RouteUsers selectedUsers = null;
			try {
				if (alUserList != null && alUserList.size() > 0) {
					for (int index = 0; index < alUserList.size(); index++) {
						availableListModel.removeElement(alUserList.get(0));
						selectedUsers = (RouteUsers) alUserList.get(index);
						users = new Creator(selectedUsers.getUserId(),
								selectedUsers.getUserName(), selectedUsers
								.getUserEmail());
						this.vApproversList.add(users);
						// Load RouteInfo In To Table...
						VWApproverInfo approverInfo = new VWApproverInfo();
						if (selectedUsers.getSendEmail() == 0)
							approverInfo.setApproverEmailFlag(false);
						else
							approverInfo.setApproverEmailFlag(true);
						if (selectedUsers.getActionPermission() == 0)
							approverInfo.setApproverActionFlag(false);
						else
							approverInfo.setApproverActionFlag(true);
						//need to re-look in here
						approverInfo.setApproverName((selectedUsers
								.getUserName() == null) ? "" : selectedUsers
										.getUserName());
						approverInfo.setApproverEmail(selectedUsers
								.getUserEmail());
						approverInfo.setApproverId(String.valueOf(selectedUsers
								.getUserId()));
						approversData.add(approverInfo);
						// END...
					}
				}
				VWApproversListPanel.Table.addData(approversData, 1, "Room");
			} catch (Exception e) {
				AdminWise.printToConsole("Error in loadRouteInfo "+e.toString());
			}
		}
		} catch (Exception e) {
			AdminWise.printToConsole("Error in loadRouteInfo :"+e.toString());
		}
		
	}
	
	public Vector getSelectedDocs() {
		// Currently multiple document selection is not allowed
		// now multiple document selection is allowed
		Vector selDocs = new Vector();
		int[] selRows = Table.getSelectedRows();
		for (int i = 0; i < selRows.length; i++)
			selDocs.add(Table.getRowData(selRows[i]));
		return selDocs;
	}
	
	public boolean validateDRSData() {
		try {
			if(txtRouteName.getText() == null || txtRouteName.getText().trim().equals("")){
				VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRoutePanel.msg_1")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" " +AdminWise.connectorManager.getString("VWRoutePanel.msg_2"));
				txtRouteName.requestFocus();
				return false;
			}
			if (!VWRouteConnector.isUniqueRouteName(txtRouteName.getText(), routeInfoBean.getRouteId())){
				VWMessage.showMessage(this,ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWRoutePanel.msg_3"), VWMessage.DIALOG_TYPE);
				txtRouteName.requestFocus();
				return false;
			}
			int taskCount = drsDrawing.getTaskCount();
			if((taskCount-1)==0){
				VWMessage.showMessage(this,ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWRoutePanel.msg_4"));
				return false;
			}
			if(routeInfoBean.getStartAction().trim().equalsIgnoreCase("0") && routeInfoBean.getStartLocation().trim().equals("")){
				VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRoutePanel.msg_1")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWRoutePanel.msg_5"));
				return false;
			}else if(routeInfoBean.getStartAction().trim().equalsIgnoreCase("0") && !routeInfoBean.getStartLocation().trim().equals("")){
				String startLoction = routeInfoBean.getStartLocation();
				if(!VWRouteConnector.isUniqueStartLocation(startLoction, routeInfoBean.getRouteId())){
					VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWRoutePanel.msg_6")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"!.", VWMessage.DIALOG_TYPE);
					return false;
				}
			}
			
			String msg = drsDrawing.checkDrawing();
			if (msg != null && msg.trim().length() > 0){
				JOptionPane.showMessageDialog(this, msg);
				return false;
			}
			if (alRouteTaskInfo == null){
				VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRoutePanel.msg_1")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" " +AdminWise.connectorManager.getString("VWRoutePanel.msg_7"),VWMessage.DIALOG_TYPE);
				return false;
			}
			if (alRouteTaskInfo.size() == 0 || (taskCount > 0 && alRouteTaskInfo.size() < (taskCount -1))){
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWRoutePanel.msg_8"),VWMessage.DIALOG_TYPE);
				return false;
			}else{
				for(int i = 0; i<alRouteTaskInfo.size(); i++){
					RouteTaskInfo taskInfo = (RouteTaskInfo)alRouteTaskInfo.get(i);
					ArrayList alUserList = taskInfo.getRouteUsersList();
					if(alUserList==null || (alUserList!=null&& alUserList.size()==0)){
						VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRoutePanel.msg_9")+taskInfo.getTaskName()+"'.",VWMessage.DIALOG_TYPE);
						return false;
					}
					alUserList = null;
				}
			}
			//If final action is move to folder, folder should be specified.  Valli 2 Dec 2008
			if(routeInfoBean.getRouteActionId().trim().equals("") || 
					(routeInfoBean.getRouteActionId().equals("1") && 
					(routeInfoBean.getActionLocation().equals("-1")||routeInfoBean.getActionLocation().equals("")) ||  
					(routeInfoBean.getReportLocation().trim().equals("")) ) ){
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWRoutePanel.msg_1")+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" " +AdminWise.connectorManager.getString("VWRoutePanel.msg_10"),VWMessage.DIALOG_TYPE);
				return false;
			}
		} catch (Exception e) {
			return false;
			//System.out.println("Exception validateDRSData " + e.getMessage());
		}
		return true;
	}
	
	void BtnNewRoute_actionPerformed(java.awt.event.ActionEvent event) {
		lblTableHeader.setText("");
		if (BtnNewRoute.getText().equals(BTN_NEWROUTE_NAME)) {
			getGroupsAndUsers();
			//For the new route not to display the invalid route caption and tasks.
			hideInvalidRouteSettings();
			
			VWStartIconProperties.txtRouteLocation.setText("");
			VWStartIconProperties.vecSequence = new Vector();
			CURRENT_MODE = 1;
			drsDrawing.viewMode();
			routeInfoBean = new RouteInfo(100);
			routeInfoBean.setRouteId(0);
			alRouteTaskInfo = new ArrayList();
			drsDrawing.clearAllFigures();
			drsDrawing.taskId = 0;
			alRouteTaskSequence = new ArrayList();			
			VWStartIconProperties.listTasksModel.removeAllElements();
			taskSequenceAdded = false;
			// routeInfoBean = new RouteInfo(0);
			// routeIndexInfoBean = new RouteIndexInfo();
			
			inUpdateMode = true;
			VWApproversListPanel.Table.clearData();
			this.vApproversList.removeAllElements();
			
			VWEmailOptionsListPanel.Table.clearData();
			clearAll();
			// loadAvailableUsers();
			//need to look in again here. To clear values in Start icon properties should not make
			// all the variables static. Need to some more code cleaning. 12 Aug 08 Valli.
			//VWStartIconProperties.clearTableAndRelatedData();
			//VWStartIconProperties.clearAll();
			vwStartIconProperties.firstTime = true;
			placeStartIcons();
			SPTable.getViewport().add(PanelRouteConfig);
			PanelRouteConfig.setVisible(true);
			viewMode(true);
			setEnableMode(MODE_NEW);
		} else if (BtnNewRoute.getText().equals(BTN_SAVE_NAME)) {
			//System.out.println("\n*********** Route save starts ***********");
			
			lblRouteInvalid.setText("");
			lblTaskInvalid.setText("");
			
			if (routeUserListTable.Table.isEditing()) {
				int row = routeUserListTable.Table.getEditingRow();
				int column = routeUserListTable.Table.getEditingColumn();
				routeUserListTable.Table.editingStopped(new ChangeEvent(
						routeUserListTable.Table.getComponentAt(row, column)));
			}
			int routeId = 0;
			if (currentMode == MODE_UPDATE) {
				routeId = ((RouteInfo) cmbAvailableRoute.getSelectedItem())
				.getRouteId();
				
			}
			if (CURRENT_MODE == 3) {
				// routeId = routeInfoBean.getRouteId();
				routeId = ((RouteInfo) cmbAvailableRoute.getSelectedItem())
				.getRouteId();
			}
			
			if (currentMode == MODE_NEW) {
				usersModified = false;
				routeId = 0;
			}
			if(routeId !=0){
				retDelete = AdminWise.gConnector.ViewWiseClient.isRouteInUse(gCurRoom, routeId, 1);
				/**
				 * For Invalid route should not display this message; User can able to save the invalid routes
				 */
				if(retDelete == 1 && routeValidity!=INVALIDROUTE){
					VWMessage.showMessage(this,	ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" "+ AdminWise.connectorManager.getString("RoutePanel.InUse"));
					return;
				}
			}
			if (gCurRoom != 0) {
				if (!validateDRSData())
					return;
				inUpdateMode = false;	
				// routeInfoBean = new RouteInfo(100);				
				routeInfoBean.setRouteId(routeId);				
				
				routeInfoBean.setRouteName(txtRouteName.getText().trim());
				routeInfoBean.setRouteDescription(taRouteDesc.getText().trim());
				Properties props = System.getProperties();
				String imageFilepath = props.getProperty("user.home")
				+ File.separator + "Application Data" + File.separator
				+ PRODUCT_NAME + File.separator + "Test.draw";
				String fileContents = drsDrawing.promptSaveAs(imageFilepath);
				routeInfoBean.setRouteImage(fileContents);
				removeUnUsedTasksIfAny();
				updateCurrentRouteTaskSequence();
				removeInvalidRouteTaskInfo(alRouteTaskInfo);
				Collections.sort(alRouteTaskInfo, TaskSeqOrder);
				routeInfoBean.setRouteTaskList(alRouteTaskInfo);
				int markAsDelete = retUpdate == 1? 1:0;
				/**
				 * Enhancement:- WorkFlow Update CV83.3
				 * Code Added for displaying Progress Dialog for WorkFlow Update Procedure call.
				 * Procedure calls are called from a seperate thread.
				 * Date:-17/11/2015
				 * 
				 */
				if(routeId!=0){
					lock1.lock();
					BtnNewRoute.setEnabled(false);
					BtnProgressClick.doClick();
					try {

						BtnProgressClick.doClick();
						Thread.sleep(5000);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Vector resultVector=new Vector();
					int returnValue = AdminWise.gConnector.ViewWiseClient.setRouteInfo(gCurRoom, routeInfoBean, routeId, markAsDelete, routeValidity);
					routeReturnValue=returnValue;
					//Code added to sendEmail for workFlow update Date:-22/12/2015
					Vector resultVal=new Vector();
					AdminWise.gConnector.ViewWiseClient.workFlowUpdateSendEmail(gCurRoom, resultVal);
					AdminWise.printToConsole("resultValue:::"+resultVal);
					if(resultVal!=null&& resultVal.size()>0){
						for (int i = 0; i < resultVal.size(); i++)
						{
							StringTokenizer st = new StringTokenizer((String) resultVal.elementAt(i), "\t");
							int refId = Integer.parseInt(st.nextToken());
							int docId = Integer.parseInt(st.nextToken());
							String docName = st.nextToken();
							int routeId1=Integer.parseInt(st.nextToken());
							String routeName=st.nextToken();
							int taskSequence=Integer.parseInt(st.nextToken());
							AdminWise.gConnector.ViewWiseClient.sendWorkFlowUpdateMail(gCurRoom,docId,docName,routeId1,routeName,taskSequence,refId);
						}
					}//End of sendMail call
					if((returnValue==-971)||(returnValue==-336)){
						processDialog.setVisible(false);
						processDialog.dispose();
					}
					AdminWise.gConnector.ViewWiseClient.getDRSGetWorkflowStatus(gCurRoom,resultVector);
					String WorkFlowStatus=resultVector.get(0).toString();
					if(WorkFlowStatus.equalsIgnoreCase("1")||WorkFlowStatus.equalsIgnoreCase("0")){
						processDialog.setVisible(false);
						processDialog.dispose();
					}
					BtnNewRoute.setEnabled(true);
					lock1.unlock();
				}
				else if(routeId==0){
					int returnValue = AdminWise.gConnector.ViewWiseClient.setRouteInfo(gCurRoom, routeInfoBean, routeId, markAsDelete, routeValidity);
					routeReturnValue=returnValue;
				}
				if(routeReturnValue==-971)
					return;
				drsDrawing.clearAllFigures();
				vwStartIconProperties.clearTableAndRelatedData();
				SPTable.getViewport().add(Table);
				loadAvailableRoutes();
				PanelRouteConfig.setVisible(false);
				//PanelRouteConfig.setBorder(new LineBorder(Color.BLUE));
				setEnableMode(MODE_CONNECT);
				routeId = 0;
			}
			CURRENT_MODE = 0;
		}
		PanelRouteConfig.setVisible(true);
	}
	
	/**
	 * Enhancement:- WorkFlow Issue fix for Sequence mismatch
	 * Updated this method to fix the task sequence issue by  
	 * getting the task sequence from task sequence list in start icon properties 
	 * and comparing the task sequence  and set the route task sequence based on the 
	 * task sequence list in start icon properties 
	 * Date :-24/11/2014
	 * @param allRouteTaskInfo - contains all route task info beans.
	 */
	private void removeInvalidRouteTaskInfo(ArrayList allRouteTaskInfo) {
		try{
			if(allRouteTaskInfo != null && allRouteTaskInfo.size() > 0){
				for(int i=0;i<allRouteTaskInfo.size();i++){
					RouteTaskInfo routeTaskInfo = (RouteTaskInfo) allRouteTaskInfo.get(i);
					for(int j=0;j<vwStartIconProperties.listTasksModel.size();j++)
					{
						String taskName ="";
						taskName=(String) vwStartIconProperties.listTasksModel.get(j);
						if(routeTaskInfo.getTaskName().equalsIgnoreCase(taskName))
						{
							routeTaskInfo.setTaskSequence(j+1);
						}
					}
				}
			}			
		}catch (Exception e) {
			AdminWise.printToConsole("Exception while removing invalid route task info : "+e.getMessage());
		}
	}

	void placeStartIcons(){
		// Placing Start icon
		try{
			drsDrawing.btnTask.setEnabled(true);
			VWStartFigure eFigure = new VWStartFigure();
			eFigure.setFigureName("Start_1");
			Point eStart = new Point(50, 52);
			Point eEnd = new Point(150, 350);
			eFigure.displayBox(eStart, eEnd);
			FigureAttributeConstant figConst = new FigureAttributeConstant(
			"Start");
			eFigure.setAttribute(figConst, "Start_1");
			eFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, drsDrawing.createPopupMenu(false));
			drsDrawing.view().add(eFigure);
			
			//Placing one task icon
			VWTaskFigure taskFigure = new VWTaskFigure();
			drsDrawing.taskCount = 1;
			int taskId = 1;
			int taskDistStartX = 150;
			int taskDistStartY = 50;
			int taskDistEndX = 250;
			int taskDistEndY = 350;
			taskFigure.setFigureId(taskId);
			String taskname=AdminWise.connectorManager.getString("DRSDrawing.Task");
			taskFigure.setFigureName(taskname+"_"+ taskId);	
			eStart = new Point(taskDistStartX, taskDistStartY);
			eEnd = new Point(taskDistEndX,taskDistEndY);
			taskFigure.displayBox(eStart, eEnd);
			FigureAttributeConstant figureIdConst = new FigureAttributeConstant(
			"FigureId");
			taskFigure.setAttribute(figureIdConst, taskId);
			taskFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, drsDrawing.createPopupMenu(true));
			/*figConst = new FigureAttributeConstant(
			"Task");
			taskFigure.setAttribute(figConst, "Task_" + taskId);*/
			//taskFigure.size();
			drsDrawing.view().add(taskFigure);
			VWStartIconProperties.listTasksModel.addElement(taskFigure.figureName);
			RouteTaskInfo thisRouteTaskInfo = new RouteTaskInfo();
			thisRouteTaskInfo.setTaskFigureId(taskFigure.getFigureId());
			thisRouteTaskInfo.setTaskName(taskFigure.getFigureName());
			AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(thisRouteTaskInfo);

			//Placing End icon			
			rFigure.setFigureName("End_1");	
			eStart = new Point(endDistStartX, endDistStartY);
			eEnd = new Point(endDistEndX, endDistEndY);

			rFigure.displayBox(eStart, eEnd);
			figConst = new FigureAttributeConstant(
			"End");
			rFigure.setAttribute(figConst, "End_1");				
			rFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, drsDrawing.createPopupMenu(false));
			drsDrawing.view().add(rFigure);	
			VWStartIconProperties.drawConnections();
		}catch(Exception e){
			
		}
	}
	
	void BtnUpdate_actionPerformed(java.awt.event.ActionEvent event) {
		if (BtnUpdate.getText().equals(BTN_UPDATE_NAME)) {
			getGroupsAndUsers();
			VWStartIconProperties.vecSequence = new Vector();
			inUpdateMode = true;
			CURRENT_MODE = 3;
			drsDrawing.viewMode();
			if (routeUserListTable.Table.isEditing()) {
				routeUserListTable.Table.getCellEditor().cancelCellEditing();
			}
			
			if(VWEmailOptionsListPanel.Table.isEditing()){
				VWEmailOptionsListPanel.Table.getCellEditor().cancelCellEditing();
			}
			
			SPTable.getViewport().add(PanelRouteConfig);
			PanelRouteConfig.setVisible(true);
			int selectedIndex = cmbAvailableRoute.getSelectedIndex();
			if (selectedIndex > 0) {
				RouteInfo routeInfo = (RouteInfo) cmbAvailableRoute
				.getSelectedItem();
				Vector getRouteInfo = new Vector();
				AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom,
						routeInfo.getRouteId(), getRouteInfo);
				routeInfo.setImageFilePath("");
				AdminWise.gConnector.ViewWiseClient.getRouteImage(gCurRoom, routeInfo.getRouteId(),routeInfo);
				routeInfo = (RouteInfo) getRouteInfo.get(0);
				
				routeInfoBean = null;
				routeInfoBean = routeInfo;
				
				alRouteTaskInfo = routeInfoBean.getRouteTaskList();
/*				ArrayList aListTInfo = routeInfoBean.getRouteTaskList();
				alRouteTaskInfo = aListTInfo;
				RouteTaskInfo tInfo = (RouteTaskInfo) aListTInfo.get(0);
*/				loadRouteInfo(routeInfoBean);				
				// loadRouteInfo(routeInfo);
				viewMode(true);
			}
			setEnableMode(MODE_UPDATE);			
		} else if (BtnUpdate.getText().equals(BTN_CANCEL_NAME)) {
			lblTableHeader.setText(LBL_DOCS_IN_ROUTE_TABLE_HEADER);
			VWStartIconProperties.vecSequence = new Vector();
			inUpdateMode = false;
			// A cell is editable when not in update-
			// If user edits a cell in a table and clicked cancel button...
			// When user comes back to the same table with values, previously
			// user editing cell is still editable.
			if (routeUserListTable.Table.isEditing()) {
				routeUserListTable.Table.getCellEditor().cancelCellEditing();
			}
			
			if(VWEmailOptionsListPanel.Table.isEditing()){
				VWEmailOptionsListPanel.Table.getCellEditor().cancelCellEditing();
			}
			
			SPTable.getViewport().add(Table);
			cmbAvailableRoute.setSelectedIndex(0);
			PanelRouteConfig.setVisible(false);
			clearAll();
			vwStartIconProperties.clearTableAndRelatedData();
			CURRENT_MODE = 0;
			setEnableMode(MODE_CONNECT);
		}
	}
	
	void BtnDelete_actionPerformed(java.awt.event.ActionEvent event) {
		
		PanelRouteConfig.setVisible(true);
		int selectedIndex = cmbAvailableRoute.getSelectedIndex();
		//if isRouteNotUsed = 1 means physically it will be deleted and 0 means marked us deleted
		int isRouteNotUsed = -1;
		if(retDelete == 0)
			isRouteNotUsed = 1;
		else if(retDelete == 2)
			isRouteNotUsed = 0;
		
		if (selectedIndex > 0) {
			RouteInfo routeInfo = (RouteInfo) cmbAvailableRoute
			.getSelectedItem();
			int retVal = VWMessage.showConfirmDialog(AdminWise.adminFrame,
			AdminWise.connectorManager.getString("RoutePanel.Delete")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"?");
			if (retVal == 0) {
				retDelete = AdminWise.gConnector.ViewWiseClient.isRouteInUse(gCurRoom, routeInfo.getRouteId(), 1);
				if(retDelete == 1){
					VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRoutePanel.msg_11")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+".");
					return;
				}
				retVal = AdminWise.gConnector.ViewWiseClient.deleteRouteInfo(
						gCurRoom, routeInfo.getRouteId(), isRouteNotUsed);
				if (retVal == 0){
					drsDrawing.clearAllFigures();
					PanelRouteConfig.setVisible(false);
					setEnableMode(MODE_UNSELECTED);
				}
				setEnableMode(MODE_CONNECT);
				loadAvailableRoutes();	
			}
		}
	}
	
	private void removeAndReAssignAlRouteTaskSequence(){
		alRouteTaskSequence = new ArrayList();
		if(alRouteTaskInfo!=null && alRouteTaskInfo.size()>0){
			for(int count=0;count<alRouteTaskInfo.size();count++){
				RouteTaskInfo taskInfo = (RouteTaskInfo) alRouteTaskInfo.get(count);			
				alRouteTaskSequence.add(taskInfo.getTaskSequence());
			}
		}
	}
	
	void cmbAvailableRoute_actionPerformed(ActionEvent event) {
		if(cmbAvailableRoute.getSelectedIndex()>0)
			lblTableHeader.setText("");
		else if(cmbAvailableRoute.getSelectedIndex() == 0 && cmbAvailableRoute.getSelectedItem().toString().equalsIgnoreCase(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes"))))
			Table.clearData();

		Table.setType(1);
		Table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		if (!routeLoaded)
			return;
		try {
			String selectedRoute = new String(cmbAvailableRoute.getSelectedItem().toString());
			AdminWise.printToConsole("Selected Route:"+ selectedRoute);
			if (selectedRoute.trim().equals(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ WORKFLOW_MODULE_NAME +"s>")){
				BtnRouteCompletedDocs.setEnabled(false);
				BtnTaskCompletedDocs.setEnabled(false);
			}
			if (!selectedRoute.trim().equals(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ WORKFLOW_MODULE_NAME +"s>")) {
				//Added for the issue fix - Task squence getting updated with zero on update of route.
				alRouteTaskSequence = new ArrayList();
				
				BtnRouteCompletedDocs.setEnabled(true);
				BtnTaskCompletedDocs.setEnabled(true);
				RouteInfo selectedRouteObject = (RouteInfo) cmbAvailableRoute
				.getSelectedItem();
				int routeId = selectedRouteObject.getRouteId();
				retUpdate = AdminWise.gConnector.ViewWiseClient.isRouteInUse(gCurRoom, routeId, 0);
				retDelete = AdminWise.gConnector.ViewWiseClient.isRouteInUse(gCurRoom, routeId, 1);
				
				routeValidity = AdminWise.gConnector.ViewWiseClient.isRouteValid(gCurRoom, routeId);
				//System.out.println("routeValidity : "+routeValidity);
				
				SPTable.getViewport().add(PanelRouteConfig);
				PanelRouteConfig.setVisible(true);
				int selectedIndex = cmbAvailableRoute.getSelectedIndex();
				if (selectedIndex > 0) {
					RouteInfo routeInfo = (RouteInfo) cmbAvailableRoute
					.getSelectedItem();
					Vector getRouteInfo = new Vector();
					AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom,
							routeInfo.getRouteId(), getRouteInfo);
					String imageFilePath = System.getProperty("user.home")+File.separator+"Application Data"
					+File.separator+PRODUCT_NAME+File.separator+"RouteImage"+File.separator+"RouteImage.draw";
					routeInfo.setImageFilePath(imageFilePath);
					AdminWise.gConnector.ViewWiseClient.getRouteImage(gCurRoom, routeInfo.getRouteId(),routeInfo);
					routeInfo = (RouteInfo) getRouteInfo.get(0);
					
					routeInfoBean = null; 
					routeInfoBean = routeInfo;
					
					ArrayList aListTInfo = routeInfoBean.getRouteTaskList();
					//Assining to alRouteTaskInfo, the selected route task info.
					alRouteTaskInfo = aListTInfo;
					/*	* This call is unnessary when selecting route from combo box. Valli
					*/
					removeAndReAssignAlRouteTaskSequence();
					loadRouteInfo(routeInfoBean);
					CURRENT_MODE = 2;
					drsDrawing.viewMode();
					// loadRouteInfo(routeInfo);
				}
				//if retUpdate/retDelete = 0 new route, = 1 means document in route, = 2 means document route completed, =3 means route is linked with final action
				// 0 and 2 enable delete and update button , 1 disable delete and update button
				// When used route is deleted, it is not actually deleted and but marked as deleted.
				// When used route is updated, it is not actually  updated except routeInfo table, in other table new records are inserted
				viewMode(false);
				setEnableMode(MODE_SELECT);
				BtnSaveAs.setEnabled(true);
				
				if(routeValidity == VALIDROUTE){// 0 for Valid routes
					if(retUpdate == 0 || retUpdate == 2)
						BtnUpdate.setEnabled(true);
					else if(retUpdate == 1)
						BtnUpdate.setEnabled(false);
									
					BtnSaveAs.setEnabled(true);
					hideInvalidRouteSettings();
					
				}else if(routeValidity == INVALIDROUTE){// For Invalid Route
					showInvalidTasks(routeId);
				}
				
				if (retDelete == 0 || retDelete == 2) {
					BtnDelete.setEnabled(true);
					//BtnUpdate.setEnabled(true);
				} else if (retDelete == 1) {
					BtnDelete.setEnabled(false);
					//BtnUpdate.setEnabled(false);
				} else if (retDelete == 3) {
					BtnDelete.setEnabled(false);
					//BtnUpdate.setEnabled(true);
				} else {
					setEnableMode(MODE_UNSELECTED);
				}
				//BtnUpdate.setEnabled(retUpdate == 1? false:true);
				
				
			} else {
				this.vApproversList.removeAllElements();
				PanelRouteConfig.setVisible(false);
				setEnableMode(MODE_UNSELECTED);
			}
			
			try {
				//Procedure call to get the review update flag in CV83B3
				VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
				int flagValue=vwc.getDRSGetReviewUpdFlag(gCurRoom);
				if(flagValue==1)
					BtnReviewUpdate.setEnabled(true);
				else 
					BtnReviewUpdate.setEnabled(false);
			} catch (Exception e) {
				AdminWise
				.printToConsole("Exception Occured... cmbAvailableRoute_actionPerformed "
						+ e.toString());
			}
			
		} catch (Exception ex) {
			AdminWise
			.printToConsole("Exception 1... cmbAvailableRoute_actionPerformed "
					+ ex.toString());
			setEnableMode(MODE_CONNECT);
			AdminWise
			.printToConsole("Exception 2... cmbAvailableRoute_actionPerformed "
					+ ex.toString());
		}
	}
	
	//Show All columns.
	void showAllColumns(){		
		Table.getColumnModel().getColumn(Table.m_data.COL_TASK).setMinWidth(80);
		Table.getColumnModel().getColumn(Table.m_data.COL_TASK).setMaxWidth(1000);		
		Table.getColumnModel().getColumn(Table.m_data.COL_TASK_DESC).setMinWidth(85);
		Table.getColumnModel().getColumn(Table.m_data.COL_TASK_DESC).setMaxWidth(1000);		
		Table.getColumnModel().getColumn(Table.m_data.COL_USERSTATUS).setMinWidth(85);
		Table.getColumnModel().getColumn(Table.m_data.COL_USERSTATUS).setMaxWidth(1000);
		Table.m_data.fireTableDataChanged();
		Table.m_data.fireTableStructureChanged();
	}

	//hide specific columns.
	void hideColumn(int columnPos){	
		Table.getColumnModel().getColumn(columnPos).setMinWidth(0);
		Table.getColumnModel().getColumn(columnPos).setMaxWidth(0);
	}
	
	void BtnDocInRoute_actionPerformed(ActionEvent event) {
		showAllColumns();
		showAllColumns();			
		Table.setType(1);
		lblTableHeader.setText(LBL_DOCS_IN_ROUTE_TABLE_HEADER);		
		Table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		try {
			// loadDocumentsInRoute();
			Object routeList[] = this.getAvailableRoutes();
			
			Object selRoute=null;
			int selRouteId = 0;
			if(!cmbAvailableRoute.getSelectedItem().toString().trim().equalsIgnoreCase(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")))){
				RouteInfo selectedRouteObject = (RouteInfo) cmbAvailableRoute.getSelectedItem();  
				selRoute = selectedRouteObject.getRouteName();
				selRouteId = selectedRouteObject.getRouteId();
				
			}
			else{
				selRoute = FILTER_ALL_ROUTES;
			}
		//	Object selRoute = JOptionPane.showInputDialog(null,
		//			"Select A Route", "Filter By Route",
		//			JOptionPane.PLAIN_MESSAGE, null, routeList, routeList[0]);
			if ((selRoute != null) && (!selRoute.toString().trim().equals(""))) {
				selectedRouteName = String.valueOf(selRoute);
				selectedRouteId = selRouteId;
				this.loadDocumentsInFilteredRoute(selectedRouteId, selectedRouteName, Table.getType());
				SPTable.getViewport().add(Table);
				PanelRouteConfig.setVisible(false);
			}
			BtnAccept.setEnabled(false);
			BtnReject.setEnabled(false);
			BtnEndRoute.setEnabled(false);
			BtnSummary.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnUpdate.setEnabled(false);
			BtnDelete.setEnabled(false);		
			if(Table.getData().size()>0)
				BtnSaveAsHTML.setEnabled(true);
			else
				BtnSaveAsHTML.setEnabled(false);
			//cmbAvailableRoute.setSelectedIndex(0);
			
		} catch (Exception ex) {
			AdminWise
			.printToConsole("Exception ... BtnDocInRoute_actionPerformed "
					+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
		showAllColumns();
		showAllColumns();		
	}
	
	/*
	 * Desc   :Below method removeUnUsedTasksIfAny() removes Tasks from the bean (not adding to database) 
	 * which is not available in the drawing area(eg. if user deletes
	 *  	   one of the tasks already placed in the UI.
	 * Author :Nishad Nambiar
	 * Date   :4-Apr-2008  
	 *    	   
	 */
	public boolean removeUnUsedTasksIfAny(){
		//taskModified is not use. Need to remove later.
		boolean taskModified = false;
		if(routeInfoBean != null && alRouteTaskInfo != null){			
			int count = 0;
			RouteTaskInfo taskInfo = null;
			int initialCount = alRouteTaskInfo.size();
			int operationCount = 0;
			try{
				while(initialCount > operationCount){
					taskInfo = (RouteTaskInfo) alRouteTaskInfo.get(count);
					if (drsDrawing.isTaskNameExist(taskInfo.getTaskName(), -1)){
						count++;
					}
					else{
						alRouteTaskInfo.remove(count);
						taskModified = true;
					}
					operationCount++;			
				}
			}catch(Exception ex){
				System.out.println("Error in removeUnUsedTasksIfAny"+ex.toString());
			}
			if (initialCount > alRouteTaskInfo.size()){
				routeInfoBean.setRouteTaskList(alRouteTaskInfo);
			}
			
		}
		if (!taskModified){
			if (alRouteTaskInfo.size() == 0) return true;
			int taskCount = drsDrawing.getTaskCount();
			if (taskCount > 0 && alRouteTaskInfo.size() < (taskCount -1)) taskModified = true;
		}
		return taskModified;
	}
	
	void BtnSaveAsHTML_actionPerformed(ActionEvent event){
		AdminWise.printToConsole("Inside doExport");
		generateHtmlReport();
	}
	
	
	
	void BtnSaveAs_actionPerformed(ActionEvent event) {
		lblTableHeader.setText("");
		try {
			getGroupsAndUsers();
			if (routeUserListTable.Table.isEditing()) {
				routeUserListTable.Table.getCellEditor().cancelCellEditing();
			}
			VWEmailOptionsListPanel.Table.enableMode = true;
			
			CURRENT_MODE = 4;// save as
			SPTable.getViewport().add(PanelRouteConfig);
			PanelRouteConfig.setVisible(true);
			int selectedIndex = cmbAvailableRoute.getSelectedIndex();
			if (selectedIndex > 0) {
				RouteInfo routeInfo = (RouteInfo) cmbAvailableRoute
				.getSelectedItem();
				Vector getRouteInfo = new Vector();
				AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom,
						routeInfo.getRouteId(), getRouteInfo);
				routeInfo.setImageFilePath("");
				AdminWise.gConnector.ViewWiseClient.getRouteImage(gCurRoom, routeInfo.getRouteId(),routeInfo);
				routeInfo = (RouteInfo) getRouteInfo.get(0);
				routeInfo.setRouteId(0);
				loadRouteInfo(routeInfo);
				routeInfoBean = new RouteInfo(100);
				routeInfoBean.setRouteId(0);
				routeInfoBean = routeInfo;
				if(Integer.parseInt(routeInfoBean.getStartAction()) == 0){
					routeInfoBean.setStartLocation("");
					VWStartIconProperties.txtRouteLocation.setText("");
				}
				viewMode(true);
				setEnableMode(MODE_NEW);
			}
			
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception ... BtnSaveAs_actionPerformed "
					+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
	}
	
	void BtnAccept_actionPerformed(ActionEvent event) {
		
		/*
		 * public int updateRouteHistory(int sid, int docId, int routeId, int
		 * routeUserId, int routeUserLevel, String status, String comments,
		 * String documentName, String prevStatus)
		 */
		try {
			if (getSelectedDocs() != null && getSelectedDocs().size() == 1) {
				RouteMasterInfo master = (RouteMasterInfo) getSelectedDocs()
				.get(0);
				VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
				vwc.setRouteStatus(gCurRoom, master.getDocId(), master, 1,
						master.getDocName());				
				//this.Table.m_data.fireTableDataChanged();
				enableAction(false);
			} else
				VWMessage.showMessage(this,
						AdminWise.connectorManager.getString("VWRoutePanel.msg_12")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".",
						VWMessage.DIALOG_TYPE);
		} catch (Exception e) {
			VWMessage.showMessage(this, AdminWise.connectorManager.getString("VWRoutePanel.msg_12")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".",
					VWMessage.DIALOG_TYPE);
		}
	}
	
	void BtnReject_actionPerformed(ActionEvent event) {
		try {
			if (getSelectedDocs() != null && getSelectedDocs().size() == 1) {
				RouteMasterInfo master = (RouteMasterInfo) getSelectedDocs()
				.get(0);
				VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
				enableAction(false);
				vwc.setRouteStatus(gCurRoom, master.getDocId(), master, 2,
						master.getDocName());
				//this.Table.m_data.fireTableDataChanged();
				
			} else {
				VWMessage.showMessage(this,
						AdminWise.connectorManager.getString("RoutePanel.PleaseSelect")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".",
						VWMessage.DIALOG_TYPE);
			}
		} catch (Exception e) {
			VWMessage.showMessage(this, AdminWise.connectorManager.getString("RoutePanel.PleaseSelect")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".",
					VWMessage.DIALOG_TYPE);
		}
	}
	
	void BtnSummary_actionPerformed(ActionEvent event) {
		
		try {
			if (getSelectedDocs() != null && getSelectedDocs().size() == 1) {
				RouteMasterInfo master = (RouteMasterInfo) getSelectedDocs()
				.get(0);
				String docName = master.getDocName();
				//int mode = 1; // MODE_ALL_SUMMARY
				int mode = Table.getType()==1 ? 0 : 1;
				enableAction(false);
				AdminWise.gConnector.ViewWiseClient.setRouteSummary(
						gCurRoom, master.getDocId(), mode, docName);
				
			} else
				VWMessage.showMessage(this,
						AdminWise.connectorManager.getString("RoutePanel.PleaseSelect")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".",
						VWMessage.DIALOG_TYPE);
		} catch (Exception ex) {
			VWMessage.showMessage(this,AdminWise.connectorManager.getString("RoutePanel.PleaseSelect")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +".",
					VWMessage.DIALOG_TYPE);
			AdminWise
			.printToConsole("Exception ... BtnSummary_actionPerformed "
					+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
	}
	
	void BtnEndRoute_actionPerformed(ActionEvent event) {
		try {
			if (getSelectedDocs() != null) {
				Vector selectedDocs = getSelectedDocs();
				VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
				vwc.setRouteStatusForEndRoute(gCurRoom, selectedDocs);
				//this.Table.m_data.fireTableDataChanged();
				enableAction(false);
			}
		} catch (Exception e) {
		}
	}
	/**
	 * Enhancement :- Enable save as html report in workflow CV10 Date:-11-02-2016
	 */
	public void generateHtmlReport() {
		Vector tableResult=Table.getData();
		int rowCount = Table.getRowCount();
		Vector htmlContents = new Vector();
		String err = "";
		String title ="";
		if(Table.getType()==1)
			title = AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME")+" "+ AdminWise.connectorManager.getString("RoutePanel.TodoListReport");
		else if(Table.getType()==2)
			title = AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME")+" "+ AdminWise.connectorManager.getString("RoutePanel.WorkFlowCompletedReport");
		else 
			title =AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME")+" "+AdminWise.connectorManager.getString("RoutePanel.TaskCompletedReport");
		htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContents.add("<html>");
		htmlContents.add("<head>");
		htmlContents.add("<title>" + title + "</title>");
		htmlContents.add("<style>");
		htmlContents.add("TH {");
		htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#336699'; font-weight:bold");
		htmlContents.add("}");
		htmlContents.add("TD {");
		htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
		htmlContents.add("}");
		htmlContents.add("</style>");
		htmlContents.add("</head>");
		htmlContents.add("<body BGCOLOR='#F7F7E7'>");
		htmlContents.add("<table border=1 width=100% cellspacing=0 cellpadding=0 ><tr><td align=center><font size='4'><b>"+ title + "</b></font></td></tr></table>");
		htmlContents.add("<p></p>");    
		htmlContents.add("<table border = 1 width=100% cellspacing=0 cellpadding=0>");
		if(Table.getType()==1){
			htmlContents.add("<tr><td  BGCOLOR=#cccc99>Document Name </td><td  BGCOLOR=#cccc99>WorkFlow Name </td><td  BGCOLOR=#cccc99>User Name</td><td  BGCOLOR=#cccc99>Received Date</td>"
					+ "<td BGCOLOR=#cccc99>Status</td><td  BGCOLOR=#cccc99>Task</td>"
					+ "<td BGCOLOR=#cccc99>Task Description</td><td  BGCOLOR=#cccc99>User Status</td></tr>");
		}else if((Table.getType()==2)||(Table.getType()==3)){
			htmlContents.add("<tr><td  BGCOLOR=#cccc99>Document Name </td><td  BGCOLOR=#cccc99>WorkFlow Name </td><td  BGCOLOR=#cccc99>User Name</td><td  BGCOLOR=#cccc99>Received Date</td>"
					+ "<td BGCOLOR=#cccc99>Status</td></tr>");
		}
		for (int curRouteInfo = 0; curRouteInfo < tableResult.size(); curRouteInfo++)  {
			try{
				if(Table.getType()==1){
					RouteMasterInfo docInRoute = (RouteMasterInfo) tableResult.get(curRouteInfo);
					htmlContents.add("<tr><td >"+docInRoute.getDocName()+ "</td><td >"+docInRoute.getRouteName()+ "</td><td >"+docInRoute.getRouteUsername()+"</td><td >"+
							docInRoute.getReceivedDate()+ "</td><td >&nbsp;"+docInRoute.getStatus()+ "</td>"
							+ "<td >&nbsp;"+docInRoute.getTaskName()+ "</td><td >"+docInRoute.getTaskDescription()+ "</td><td >&nbsp;"+docInRoute.getUserStatus()+ "</td></tr>");
				}
				else if((Table.getType()==2)||(Table.getType()==3)){
					RouteMasterInfo docInRoute = (RouteMasterInfo) tableResult.get(curRouteInfo);
					htmlContents.add("<tr><td >"+docInRoute.getDocName()+ "</td><td >"+docInRoute.getRouteName()+ "</td><td >"+docInRoute.getRouteUsername()+ "</td><td >"+docInRoute.getReceivedDate()
							+ "</td><td >&nbsp;"+docInRoute.getStatus()+ "</td></tr>");
				}


				htmlContents.add("<p></p>");
			}catch(Exception ex){
				err = "Error generating report - " + ex.getMessage();

			}	
		}
		htmlContents.add("</table>");
		
		
		htmlContents.add("<table><tr><td>Row Count : </td><td>"+rowCount+"</td></tr></table>");
		htmlContents.add("<table><tr><td><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></td></tr></table>");
		htmlContents.add("</body></html>");
		try{
			String reportLocation="";
			if(Table.getType()==1)
				reportLocation=loadSaveASHTMLDialog(this);
			else if(Table.getType()==2)
				reportLocation=loadSaveASHTMLDialog(this);
			else
				reportLocation=loadSaveASHTMLDialog(this);
			VWCUtil.writeListToFile(htmlContents,reportLocation, err, "UTF-8");
		}catch(Exception ex){

		}
	}
	
	 public static String loadSaveASHTMLDialog(Component parent)
	    {
	        JFileChooser chooser = new JFileChooser();
	        VWFileFilter filter = new VWFileFilter();
	        filter.addExtension("html");
	        //filter.addExtension("xml");
	        filter.setDescription("Html Files");
	        chooser.setCurrentDirectory(new File("c:\\"));
	        chooser.setFileFilter(filter);
	        //chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        int returnVal = chooser.showSaveDialog(parent);
	        if(returnVal == JFileChooser.APPROVE_OPTION)
	        {
	            String path=chooser.getSelectedFile().getPath();
	            if(!path.toUpperCase().endsWith(".HTML") && !path.toUpperCase().endsWith(".XML")) path+=".html";
	            return path;
	        };
	        return "";
	    }
	
	void BtnRouteCompletedDocuments_actionPerformed(ActionEvent event) {
		if(cmbAvailableRoute.getSelectedItem().toString().trim().equalsIgnoreCase(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes"))))
			return;
		hideColumn(Table.m_data.COL_TASK);
		hideColumn(Table.m_data.COL_TASK_DESC);
		//Added user status for documents in route during task user escalation enhancement, For completed docs it is not required
		hideColumn(Table.m_data.COL_USERSTATUS);
		Table.setType(2);
		lblTableHeader.setText(LBL_DOCS_COMPLETED_ROUTE_TABLE_HEADER);
		Table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
		String selRoute = "";
		int selRouteId = 0;
		try {
			if(!cmbAvailableRoute.getSelectedItem().toString().trim().equalsIgnoreCase(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")))){
				RouteInfo selectedRouteObject = (RouteInfo) cmbAvailableRoute.getSelectedItem();			
				selRoute = selectedRouteObject.getRouteName();
				selRouteId = selectedRouteObject.getRouteId();
			}
			else{
				selRoute = FILTER_ALL_ROUTES;
			}
			if ((selRoute != null) && (!selRoute.toString().trim().equals(""))) {
				selectedRouteName = String.valueOf(selRoute);
				selectedRouteId = selRouteId;
				this.loadDocumentsInFilteredRoute(selectedRouteId, selectedRouteName, Table.getType());
				SPTable.getViewport().add(Table);
				PanelRouteConfig.setVisible(false);
			}
			BtnAccept.setEnabled(false);
			BtnReject.setEnabled(false);
			BtnEndRoute.setEnabled(false);
			BtnSummary.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnUpdate.setEnabled(false);
			if(Table.getData().size()>0)
				BtnSaveAsHTML.setEnabled(true);
			else
				BtnSaveAsHTML.setEnabled(false);
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception ... BtnRouteCompletedDocuments_actionPerformed "+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
	}
	
	void BtnTaskCompletedDocuments_actionPerformed(ActionEvent event) {
		if(cmbAvailableRoute.getSelectedItem().toString().trim().equalsIgnoreCase("<Available "+ WORKFLOW_MODULE_NAME +"s>"))
			return;
		hideColumn(Table.m_data.COL_TASK);
		hideColumn(Table.m_data.COL_TASK_DESC);
		hideColumn(Table.m_data.COL_USERSTATUS);
		Table.setType(3);
		lblTableHeader.setText(LBL_TASK_COMPLETED_DOCS_TABLE_HEADER);
		Table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);		
		String selRoute = "";
		int selRouteId = 0;
		try {
			if(!cmbAvailableRoute.getSelectedItem().toString().trim().equalsIgnoreCase(new String(AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoute")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +AdminWise.connectorManager.getString("VWRoutePanel.cmbAvailableRoutes")))){
				RouteInfo selectedRouteObject = (RouteInfo) cmbAvailableRoute.getSelectedItem();			
				selRoute = selectedRouteObject.getRouteName();
				selRouteId = selectedRouteObject.getRouteId();
			}
			else{
				selRoute = FILTER_ALL_ROUTES;
			}
			if ((selRoute != null) && (!selRoute.toString().trim().equals(""))) {
				selectedRouteName = String.valueOf(selRoute);
				selectedRouteId = selRouteId;
				this.loadDocumentsInFilteredRoute(selectedRouteId, selectedRouteName, Table.getType());
				SPTable.getViewport().add(Table);
				PanelRouteConfig.setVisible(false);
			}
			BtnAccept.setEnabled(false);
			BtnReject.setEnabled(false);
			BtnEndRoute.setEnabled(false);
			BtnSummary.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnUpdate.setEnabled(false);
			if(Table.getData().size()>0)
				BtnSaveAsHTML.setEnabled(true);
			else
				BtnSaveAsHTML.setEnabled(false);
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception ... BtnRouteCompletedDocuments_actionPerformed "+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
	}
	
	Rectangle drsDrawingBounds = new Rectangle();
	void BtnEnlarge_actionPerformed(ActionEvent event) {
		try{	
			Rectangle drsDrawingBounds = drsDrawing.getBounds(); 
			JFrame newFrame = new JFrame();
			drsDrawing.component.setBounds(1, 35, 900, 700);
			newFrame.add((JPanel) drsDrawing);
			newFrame.setBounds(0, 0, 1020, 735);
			//drsDrawing.sp.setBounds(newFrame.getBounds().x,newFrame.getBounds().y,newFrame.getBounds().x+newFrame.getBounds().width-10,newFrame.getBounds().y+newFrame.getBounds().height-35);
			newFrame.setVisible(true);
			SymWindow aSymWindow = new SymWindow();
			newFrame.addWindowListener(aSymWindow);
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e.toString());
		}
	}
		
	//Procedure call to perfrom review update in CV83B3 
	//Enahcement :- WorkFlow update
	void BtnReviewUpdate_actionPerformed(ActionEvent event){
		try{
			VWClient.printToConsole("Clicked review update button");
			VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
			vwc.getDRSReviewUpdWorkflow(gCurRoom);
			int flagValue=vwc.getDRSGetReviewUpdFlag(gCurRoom);
			if(flagValue==1)
				BtnReviewUpdate.setEnabled(true);
			else
				BtnReviewUpdate.setEnabled(false);
		}catch(Exception e){
			AdminWise.printToConsole("Exception while reviewUpdate action::::"+e.getMessage());
		}
	}
	/**
	 * Enhancement:- WorkFlow Update
	 * Method add to perform automatic button click to display the 
	 * WorkFlow update progess dialog.
	 * Date:-17/11/2015
	 * @param event
	 */
	void BtnProgressClick_actionPerformed(ActionEvent event){
		try{
			lock1.lock();
			statusLbl.paintImmediately(this.getBounds());
			statusLbl.setVisible(true);
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
				fontName = VWCPreferences.getFontName();
			statusLbl.setFont(new Font(fontName, Font.PLAIN, 14));
			statusLbl.validate();
			processDialog.getContentPane().add(statusLbl, null);
			processDialog.setLocationRelativeTo(null);
			processDialog.setTitle("Workflow Update");
			processDialog.setSize(390,70);
			processDialog.setResizable(false);
			processDialog.setDefaultCloseOperation(processDialog.DO_NOTHING_ON_CLOSE);
			processDialog.setVisible(true);
			processDialog.setOpacity(1);
			//processDialog.pack();
			lock1.unlock();
			//processDialog.repaint();			

		}catch(Exception e){
			AdminWise.printToConsole("Exception while reviewUpdate action::::"+e.getMessage());
		}
	}
	
	
	class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
        	try {
            	PanelRouteConfig.remove(drsDrawing);
            	drsDrawing.setBounds(drsDrawingBounds);
    		   	//PanelRouteConfig.add((JPanel) drsDrawing);
    			drsDrawing.setBounds(27, 145+20, 850, 450);
    			drsDrawing.component.setBounds(1, 35, 847, 411);
    			drsDrawing.setBackground(Color.WHITE);
    			PanelRouteConfig.add((JPanel) drsDrawing);
    			PanelRouteConfig.repaint();
        	} catch (Exception e) {
        		JOptionPane.showMessageDialog(null,e.toString());
        	}
        }
    }
	
	public void enableAction(boolean flag) {
		BtnReject.setEnabled(flag);
		BtnAccept.setEnabled(flag);
		BtnEndRoute.setEnabled(flag);
		BtnSummary.setEnabled(flag);
	}
	
	private void viewMode(boolean view) {
		try {
			routeUserListTable.Table.setEnabled(view);
			/*routeUserListTable.btnRemove.setEnabled(view);
			routeUserListTable.btnAdd.setEnabled(view);*/
			
			txtRouteName.setEnabled(view);
			taRouteDesc.setEnabled(view);
			userList.setEnabled(view);
			userList.listCheckBox.setEnabled(view);
			userList.listDescription.setEnabled(view);
			cmbAvailableAssignRoute.setEnabled(view);
			
			VWEmailOptionsListPanel.Table.setEnabled(view);
			VWEmailOptionsListPanel.btnAdd.setEnabled(view);
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception in view " + ex.getMessage());
		}
	}
	
	public void clearAll() {
		txtRouteName.setText("");
		taRouteDesc.setText("");
		availableListModel.removeAllElements();
		vApproversList.removeAllElements();
		routeInfoBean.setStartAction("0");
		routeInfoBean.setStartLocation("");
	}
	
	public void loadTabData(VWRoom newRoom) {
		if (newRoom.getConnectStatus() != Room_Status_Connect) {
			setEnableMode(MODE_UNCONNECT);
			return;
		}
		// Should not load again becos it will loose data when switching from
		// tab to tab. Valli
		if (newRoom.getId() == gCurRoom)
			return;
		loadAvailableRoutes();
		PanelRouteConfig.setVisible(false);
		SPTable.getViewport().add(Table);
		loadDocumentsInRoute();
		UILoaded = true;
		setEnableMode(MODE_UNSELECTED);
		lblTableHeader.setText(LBL_DOCS_IN_ROUTE_TABLE_HEADER);
		VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
		int flagValue=vwc.getDRSGetReviewUpdFlag(gCurRoom);
		if(flagValue==1)
			BtnReviewUpdate.setEnabled(true);
		else 
			BtnReviewUpdate.setEnabled(false);
		/****CV2019 - SIDBI customization check--------------------------------***/
		AdminWise.printToConsole("before calling SIDBI Customization....");
		Vector<String> result = new Vector<String>();
		int retValue = vwc.getSettingsInfo(gCurRoom, "SIDBI Customization", result);
		AdminWise.printToConsole("after calling SIDBI Customization...."+result);
		if(retValue != -1 && result!= null && !result.isEmpty()){
			AdminWise.printToConsole("result.get(0)..........."+result.get(0));
			if (String.valueOf(result.get(0)) != null && String.valueOf(result.get(0)).equalsIgnoreCase("YES")) {
				sidbiCustomizationFlag = true;
			}
			result = null;
		}
		/*------------------End of SIDBI check----------------------------*/
	}
	
	public Vector RefreshLoadDocumentsInRoute() {
		Vector documentsInRoute = new Vector();
		Vector RefreshDocsInRoute = new Vector();
		int adminUser = 1;
		//Sending 0 for routeId to get all the pending documents - code optimization
		int routeId = 0;
		AdminWise.gConnector.ViewWiseClient.getToDoRouteList(gCurRoom, adminUser, routeId, documentsInRoute);
		if (selectedRouteName.trim().equalsIgnoreCase(FILTER_ALL_ROUTES)) {
			for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
				RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
				.get(curRouteInfo);
				if (docInRoute.getActionPermission() == 1)
					RefreshDocsInRoute.addElement(docInRoute);
			}
		} else {
			for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
				RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
				.get(curRouteInfo);
				// No need to list dummy approvers here.
				if (docInRoute.getActionPermission() == 1
						&& docInRoute.getRouteName().trim().equalsIgnoreCase(
								selectedRouteName.trim())) {
					RefreshDocsInRoute.addElement(docInRoute);
				}
			}
		}
		
		return RefreshDocsInRoute;
	}
	/*
	 * RouteId is previously zero to pass the routeid and fix the combo loading issue
	 * while clicking the documents in route table. 
	 * 
	 */
	public Vector RefreshLoadDocumentsInRoute(int Routeid) {
		Vector documentsInRoute = new Vector();
		Vector RefreshDocsInRoute = new Vector();
		int adminUser = 1;
		//Sending the exact route id from combo selected value
		int routeId = Routeid;
		AdminWise.gConnector.ViewWiseClient.getToDoRouteList(gCurRoom, adminUser, routeId, documentsInRoute);
		if (selectedRouteName.trim().equalsIgnoreCase(FILTER_ALL_ROUTES)) {
			for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
				RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
						.get(curRouteInfo);
				if (docInRoute.getActionPermission() == 1)
					RefreshDocsInRoute.addElement(docInRoute);
			}
		} else {
			for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
				RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
						.get(curRouteInfo);
				// No need to list dummy approvers here.
				if (docInRoute.getActionPermission() == 1
						&& docInRoute.getRouteName().trim().equalsIgnoreCase(
								selectedRouteName.trim())) {
					RefreshDocsInRoute.addElement(docInRoute);
				}
			}
		}

		return RefreshDocsInRoute;
	}
	
	private void loadDocumentsInRoute() {
		Vector documentsInRoute = new Vector();
		Vector docsInRouteDummyAppRemoved = new Vector();
		int adminUser = 1;
		//Sending 0 for routeId to get all the pending documents - code optimization
		int routeId = 0;
		AdminWise.gConnector.ViewWiseClient.getToDoRouteList(gCurRoom, adminUser, routeId, documentsInRoute);
		for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
			RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
			.get(curRouteInfo);
			// No need to list dummy approvers here.
			if (docInRoute.getActionPermission() == 1)
				docsInRouteDummyAppRemoved.addElement(docInRoute);
		}
		Table.addData(docsInRouteDummyAppRemoved);
		Table.m_data.fireTableDataChanged();
	}
	
	// This function is Filtering by RouteName...
	private void loadDocumentsInFilteredRoute(int routeId, String routeName, int type) {
		int ret = 0;
		if (routeName.trim().equalsIgnoreCase(FILTER_ALL_ROUTES)) {
			Vector documentsInRoute = new Vector();
			Vector docsInRouteDummyAppRemoved = new Vector();
			int adminUser = 1;
			
			if(type==1)
				ret = AdminWise.gConnector.ViewWiseClient.getToDoRouteList(gCurRoom, adminUser, routeId, documentsInRoute);
			else if(type == 2)
				ret = AdminWise.gConnector.ViewWiseClient.getRouteCompletedDocuments(gCurRoom, 0, adminUser, documentsInRoute);
			else
				ret = AdminWise.gConnector.ViewWiseClient.getTaskCompletedDocuments(gCurRoom, routeId, documentsInRoute);
			
			for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
				RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
				.get(curRouteInfo);
				// No need to list dummy approvers here.
				if (docInRoute.getActionPermission() == 1)
					docsInRouteDummyAppRemoved.addElement(docInRoute);
			}
			Table.addData(docsInRouteDummyAppRemoved);
			Table.m_data.fireTableDataChanged();
		} else {
			Vector documentsInRoute = new Vector();
			Vector documentsInFilteredRoute = new Vector();
			int adminUser = 1;
			
			if(type==1)
				AdminWise.gConnector.ViewWiseClient.getToDoRouteList(gCurRoom, adminUser, routeId, documentsInRoute);
			else if(type == 2)
				ret = AdminWise.gConnector.ViewWiseClient.getRouteCompletedDocuments(gCurRoom, routeId, adminUser, documentsInRoute);
			else
				ret = AdminWise.gConnector.ViewWiseClient.getTaskCompletedDocuments(gCurRoom, routeId, documentsInRoute);
			
			for (int curRouteInfo = 0; curRouteInfo < documentsInRoute.size(); curRouteInfo++) {
				RouteMasterInfo docInRoute = (RouteMasterInfo) documentsInRoute
				.get(curRouteInfo);
				// No need to list dummy approvers here.
				if (docInRoute.getActionPermission() == 1
						&& docInRoute.getRouteName().trim().equalsIgnoreCase(
								routeName.trim())) {
					documentsInFilteredRoute.addElement(docInRoute);
				}
			}
			Table.addData(documentsInFilteredRoute);
			Table.m_data.fireTableDataChanged();
		}
	}
	
	// private void loadDocumentsInRoute(List list) not in use
	private void loadDocumentsInRoute(Vector documentsInRoute) {
		Table.clearData();
		Table.addData(documentsInRoute);
	}
	
	class SymComponent extends java.awt.event.ComponentAdapter {
		public void componentResized(java.awt.event.ComponentEvent event) {
			Object object = event.getSource();
			if (object == VWRouteOption)
				VWRoute_componentResized(event);
		}
	}
	
	// --------------------------------------------------------------------------
	void VWRoute_componentResized(java.awt.event.ComponentEvent event) {
		if (event.getSource() == VWRouteOption) {
			Dimension mainDimension = this.getSize();
			Dimension panelDimension = mainDimension;
			Dimension commandDimension = PanelCommand.getSize();
			commandDimension.height = mainDimension.height;
			panelDimension.width = mainDimension.width - commandDimension.width;
			PanelTable.setSize(panelDimension);
			PanelCommand.setSize(commandDimension);
			PanelTable.doLayout();						
		}
	}
	
	// --------------------------------------------------------------------------
	
	// --------------------------------------------------------------------------
	public void setEnableMode(int mode) {
		currentMode = mode;
		BtnReject.setEnabled(false);
		BtnAccept.setEnabled(false);
		BtnSummary.setEnabled(false);
		BtnEndRoute.setEnabled(false);
		BtnSaveAsHTML.setEnabled(false);
		cmbAvailableRoute.setEnabled(true);
		cmbAvailableRoute.setVisible(true);
		switch (mode) {
		case MODE_CONNECT:
			CURRENT_MODE = 0;
			cmbAvailableRoute.setEnabled(true);
			BtnDelete.setVisible(true);
			lblAvailableRoute.setVisible(true);
			cmbAvailableRoute.setVisible(true);
			BtnDocInRoute.setVisible(true);
			BtnSaveAs.setVisible(true);
			
			BtnNewRoute.setEnabled(true);
			BtnNewRoute.setIcon(VWImages.NewRouteIcon);
			BtnNewRoute.setText(BTN_NEWROUTE_NAME);
			BtnDelete.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnDocInRoute.setEnabled(true);
			cmbAvailableRoute.setEnabled(true);
			BtnReject.setVisible(true);
			BtnAccept.setVisible(true);
			BtnSummary.setVisible(true);
			BtnEndRoute.setVisible(true);
			BtnRouteCompletedDocs.setVisible(true);
			BtnTaskCompletedDocs.setVisible(true);
			lblTableHeader.setText(LBL_DOCS_IN_ROUTE_TABLE_HEADER);
			//BtnReviewUpdate.setVisible(true);
			BtnReviewUpdate.setEnabled(false);
			BtnSaveAsHTML.setEnabled(false);
			break;
			
		case MODE_UNSELECTED:
			// this.vApproversList.removeAllElements();
			BtnNewRoute.setText(BTN_NEWROUTE_NAME);
			BtnNewRoute.setIcon(VWImages.NewRouteIcon);
			BtnNewRoute.setEnabled(true);
			BtnDelete.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnDocInRoute.setVisible(true);
			BtnReviewUpdate.setVisible(true);
			BtnDocInRoute.setEnabled(true);
			BtnRouteCompletedDocs.setEnabled(false);
			BtnTaskCompletedDocs.setEnabled(false);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnUpdate.setEnabled(false);
			BtnSaveAs.setVisible(true);
			BtnDelete.setVisible(true);
			lblAvailableRoute.setVisible(true);
			cmbAvailableRoute.setEnabled(true);
			cmbAvailableRoute.setVisible(true);
			BtnReject.setVisible(true);
			BtnAccept.setVisible(true);
			BtnSummary.setVisible(true);
			BtnEndRoute.setVisible(true);
			BtnReject.setEnabled(false);
			BtnAccept.setEnabled(false);
			BtnSummary.setEnabled(false);
			BtnEndRoute.setEnabled(false);
			BtnReviewUpdate.setEnabled(false);
			BtnSaveAsHTML.setEnabled(false);
			break;
			
		case MODE_SELECT:
			BtnNewRoute.setEnabled(true);
			BtnNewRoute.setText(BTN_NEWROUTE_NAME);
			BtnNewRoute.setIcon(VWImages.NewRouteIcon);
			BtnDelete.setEnabled(true);
			BtnSaveAs.setVisible(true);
			BtnReviewUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnDelete.setVisible(true);
			lblAvailableRoute.setVisible(true);
			cmbAvailableRoute.setVisible(true);
			BtnDocInRoute.setVisible(true);
			BtnDocInRoute.setEnabled(true);
			BtnReject.setVisible(true);
			BtnAccept.setVisible(true);
			BtnSummary.setVisible(true);
			BtnEndRoute.setVisible(true);
			BtnRouteCompletedDocs.setVisible(true);
			BtnTaskCompletedDocs.setVisible(true);
			BtnReviewUpdate.setEnabled(false);
			BtnSaveAsHTML.setEnabled(false);
			showAllColumns();
			showAllColumns();
			break;
			
		case MODE_UNCONNECT:	
			CURRENT_MODE = 0;
			cmbAvailableRoute.setEnabled(false);
			BtnReviewUpdate.setVisible(true);
			BtnNewRoute.setText(BTN_NEWROUTE_NAME);
			BtnNewRoute.setIcon(VWImages.NewRouteIcon);
			BtnNewRoute.setEnabled(false);
			BtnDelete.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnDocInRoute.setEnabled(false);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnUpdate.setEnabled(false);
			BtnSaveAs.setVisible(true);
			BtnDelete.setVisible(true);
			lblAvailableRoute.setVisible(true);
			cmbAvailableRoute.setVisible(true);
			cmbAvailableRoute.setEnabled(false);
			//make the buttons visible, if 'Disconnect' is clicked in 'New Route' mode OR 'Update Route' mode. 
			BtnDocInRoute.setVisible(true);
			BtnReject.setVisible(true);
			BtnAccept.setVisible(true);
			BtnSummary.setVisible(true);
			BtnEndRoute.setVisible(true);
			BtnRouteCompletedDocs.setEnabled(false);
			BtnTaskCompletedDocs.setEnabled(false);
			BtnRouteCompletedDocs.setVisible(true);
			BtnTaskCompletedDocs.setVisible(true);
			BtnReviewUpdate.setEnabled(false);
			if(Table.getData().size()>0)
				BtnSaveAsHTML.setEnabled(true);
			lblTableHeader.setText(LBL_DOCS_IN_ROUTE_TABLE_HEADER);
			showAllColumns();
			showAllColumns();
			gCurRoom = 0;			
			break;
			
		case MODE_NEW:
			BtnNewRoute.setEnabled(true);
			BtnNewRoute.setText(BTN_SAVE_NAME);
			BtnNewRoute.setIcon(VWImages.SaveIcon);
			BtnSaveAs.setVisible(false);
			BtnReviewUpdate.setVisible(false);
			BtnDocInRoute.setVisible(false);
			BtnReject.setVisible(false);
			BtnAccept.setVisible(false);
			BtnSummary.setVisible(false);
			BtnEndRoute.setVisible(false);
			BtnRouteCompletedDocs.setVisible(false);
			BtnTaskCompletedDocs.setVisible(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnSaveAsHTML.setEnabled(false);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			lblAvailableRoute.setVisible(false);
			cmbAvailableRoute.setVisible(false);
			BtnReviewUpdate.setEnabled(false);
			BtnSaveAsHTML.setEnabled(false);
			txtRouteName.requestFocus();
			showAllColumns();
			showAllColumns();
			break;
			
		case MODE_UPDATE:
			BtnNewRoute.setEnabled(true);
			BtnReviewUpdate.setVisible(false);
			BtnNewRoute.setText(BTN_SAVE_NAME);
			BtnNewRoute.setIcon(VWImages.SaveIcon);
			BtnSaveAs.setVisible(false);
			BtnDocInRoute.setVisible(false);
			BtnReject.setVisible(false);
			BtnAccept.setVisible(false);
			BtnSummary.setVisible(false);
			BtnEndRoute.setVisible(false);
			BtnRouteCompletedDocs.setVisible(false);
			BtnTaskCompletedDocs.setVisible(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			lblAvailableRoute.setVisible(false);
			cmbAvailableRoute.setVisible(false);
			BtnReviewUpdate.setEnabled(false);
			BtnSaveAsHTML.setEnabled(false);
			showAllColumns();
			showAllColumns();
			break;
		case MODE_ENABLE:
			BtnAccept.setEnabled(true);
			BtnReject.setEnabled(true);
			BtnSummary.setEnabled(true);
			BtnEndRoute.setEnabled(true);
			if(Table.getType()==2){
				BtnReject.setEnabled(false);
				BtnAccept.setEnabled(false);
				BtnEndRoute.setEnabled(false);
			}else if(Table.getType()==3){
				BtnReject.setEnabled(false);
				BtnAccept.setEnabled(false);				
			}			
			VWClient vwc = (VWClient) AdminWise.gConnector.ViewWiseClient;
			int flagValue=vwc.getDRSGetReviewUpdFlag(gCurRoom);
			if(flagValue==1)
			BtnReviewUpdate.setEnabled(true);
			if(Table.getData().size()>0)
				BtnSaveAsHTML.setEnabled(true);
			break;
		case MODE_DISABLE:
			BtnAccept.setEnabled(false);
			BtnReject.setEnabled(false);
			BtnSummary.setVisible(false);
			BtnEndRoute.setVisible(false);
			BtnRouteCompletedDocs.setVisible(false);
			BtnTaskCompletedDocs.setVisible(false);
			BtnReviewUpdate.setEnabled(false);
			BtnSaveAsHTML.setEnabled(false);
			break;
		case MODE_ROUTE_ENABLE:
			BtnAccept.setEnabled(true);
			BtnReject.setEnabled(true);
			BtnEndRoute.setEnabled(true);
			BtnSummary.setEnabled(true);
			break;

		case MODE_ROUTE_DISABLE:
			BtnAccept.setEnabled(false);
			BtnReject.setEnabled(false);
			BtnEndRoute.setEnabled(true);
			BtnSummary.setEnabled(true);
			BtnReviewUpdate.setEnabled(false);
			break;
			
		}
		if (BtnUpdate.isEnabled() == false) {
			this.vApproversList.removeAllElements();
			this.userList.loadData(this.vApproversList);
			//Added to fix the empty task saving after workFlowupdate Enhancment
			if(BtnNewRoute.equals(BtnNewRoute.getText().equals(BTN_SAVE_NAME)))
			BtnNewRoute.setEnabled(false);
			BtnUpdate.setEnabled(false);
		}
		if(Table.getData().size()<0)
			BtnSaveAsHTML.setEnabled(false);
	}
	public void updateCurrentRouteTaskSequence() {
		int curTaskSeqId = 0;
		if (routeInfoBean != null && alRouteTaskInfo != null && alRouteTaskInfo.size() > 0) {
			for (int count = 0; count < alRouteTaskInfo.size(); count++) {
				try {
					RouteTaskInfo curTskInfo = (RouteTaskInfo) alRouteTaskInfo.get(count);		
					int curTaskFigureId = curTskInfo.getTaskFigureId();
					if(alRouteTaskSequence!=null && alRouteTaskSequence.size()>0){
						curTaskSeqId = getTaskSequence(curTaskFigureId);
						curTskInfo.setTaskSequence(curTaskSeqId);
					}
					alRouteTaskInfo.remove(count);
					alRouteTaskInfo.add(count, curTskInfo);
				} catch (Exception e) {
				}
			}
		}
	}
	public int getTaskSequence(int figureId) {
		int taskSequence = 0;
		for (int count = 0; count < alRouteTaskSequence.size(); count++) {
			int curTaskFigureId = Integer.parseInt(String.valueOf(alRouteTaskSequence.get(count)));
			if (curTaskFigureId == figureId) {
				taskSequence = (count) + 1;
				break;
			}
		}
		return taskSequence;
	}
	
	
	//For Invalid Routes
	
	public void showInvalidTasks(int routeId){
		BtnUpdate.setEnabled(true);
		BtnSaveAsHTML.setEnabled(false);
		BtnSaveAs.setEnabled(false);
		
		invalidTasks = "";
		String taskLbl = "";
		Vector tasks = new Vector();
		int ret = AdminWise.gConnector.ViewWiseClient.getRouteInvalidTasks(gCurRoom, routeId, tasks);
		int numOfTask = tasks.size();
		int taskCount = 1;
		int numOfLines = 0; 
		if(tasks!=null && numOfTask>0){
			RouteInvalidTaskInfo invalidTasksInfo = null;
			
			if(numOfTask==1){
				invalidTasksInfo = new RouteInvalidTaskInfo(tasks.get(0).toString());
				invalidTasks = invalidTasksInfo.getTaskName();
			}else if(numOfTask>1){
				//Following are the invalid tasks
				for(int i=0; i<numOfTask-1; i++){
					invalidTasksInfo = new RouteInvalidTaskInfo(tasks.get(i).toString());
					invalidTasks = invalidTasks+invalidTasksInfo.getTaskName()+", ";
					
					if(taskCount==4){
						invalidTasks = invalidTasks + "<br>";
						numOfLines = 1;
						taskCount =1; 
					}else{
						taskCount++;
					}
				}
				invalidTasksInfo = new RouteInvalidTaskInfo(tasks.get(numOfTask-1).toString());
				invalidTasks = invalidTasks + invalidTasksInfo.getTaskName();
			}
		}
		
		//Initialize to the original size.
		invalidWidth = validWidth*2;
		invalidHeight = validHeight;
		routeDiagramY = stdRouteY;
		
		int lengthOfTask = invalidTasks.length()*2;
		invalidWidth = invalidWidth + lengthOfTask + (LBL_ROUTEINVALID.length()*2);
		
		//need to reset the bounds for route diagram.
		while(numOfLines>0){
			routeDiagramY = routeDiagramY + stdIncreaseHeight;
			invalidHeight = invalidHeight + stdIncreaseHeight;
			numOfLines--;
		}
		
		//If the route has less than 4 task should have only one row
		if(numOfLines == 0){
			routeDiagramY = routeDiagramY + stdIncreaseHeight;
			invalidHeight = invalidHeight + stdIncreaseHeight;
		}
		
		lblRouteInvalid.setText(LBL_ROUTEINVALID);
		lblRouteInvalid.setBounds(invalidX, invalidY, invalidWidth, validHeight);
		lblRouteInvalid.setForeground(Color.red);
		
		taskLbl = "<html><br>"+invalidTasks+"</html>";
		lblTaskInvalid.setText(taskLbl);
		lblTaskInvalid.setForeground(Color.red);
		lblTaskInvalid.setBounds(invalidX, invalidY+20, invalidWidth, invalidHeight);
		
		drsDrawing.setBounds(routeDiagramX, routeDiagramY+20, routeDiagramWidth, routeDiagramHeight);
	}
	
	static final Comparator<RouteTaskInfo> TaskSeqOrder =
		new Comparator<RouteTaskInfo>() {
		public int compare(RouteTaskInfo routeTaskInfo1, RouteTaskInfo routeTaskInfo2) {
	        return (routeTaskInfo1.getTaskSequence() < routeTaskInfo2.getTaskSequence()? -1 :
	                (routeTaskInfo1.getTaskSequence() == routeTaskInfo2.getTaskSequence()? 0 : 1));

		}
	};

	public void hideInvalidRouteSettings(){
		invalidTasks = "";
		lblRouteInvalid.setText("");
		lblRouteInvalid.setBounds(invalidX, invalidY, 10, validHeight);
		lblTaskInvalid.setText("");
		lblTaskInvalid.setBounds(invalidX, invalidY, 10, validHeight);
		drsDrawing.setBounds(routeDiagramX, routeDiagramStdY, routeDiagramWidth, routeDiagramHeight);
		routeValidity = VALIDROUTE;
	}
	
	
	public int groupCount = 0;
	//vecApproversList is having Principals bean value
	public static Vector<Principal> vecApproversList = null;
	
	//allApproversList is having users and groups names (String)
	public static Vector<String> allApproversList = null;
	//allEscalationUsersList is having only users along with Auto Accept, Auto reject
	public static Vector<Principal> allEscalationUsersList = null;
	
	//vecApproversListFromDB is having Vector of dbstrings
	public static Vector<Principal> vecApproversListFromDB = null;
	
	//Email id updated from groupsToUser tab was not reflecting in the task users, so calling from create route, update route and Route save as.
	
	public Vector getGroupsAndUsers(){
		System.out.println("getGroupsAndUsers()");
		Vector<Principal> vecGroups = new Vector<Principal>();
		Vector principals = new Vector();
		vecApproversList = new Vector<Principal>();
		vecApproversListFromDB = new Vector<Principal>();
		try{
			principals = VWRouteConnector.getDRSPrincipals(0, "0");
			if(principals!=null && principals.size()>0){
				vecApproversListFromDB.addAll(principals);
				allApproversList = new Vector<String>();
				allEscalationUsersList = new Vector<Principal>();

				for (int i = 0; i < principals.size(); i++){
					Principal p = (Principal) principals.get(i);
					vecGroups.add(p);
					vecApproversList.add(p);
					
					groupCount++;
					allApproversList.add(p.getName());
					
					if(p.getType()==2)
						allEscalationUsersList.add(p);
				}
			}
		}catch(Exception ex){
			//System.out.println("Exception in getGroupsAndUsers()");
		}
		return vecGroups;
	}
	
	// --------------------------------------------------------------------------
	VWPicturePanel picPanel = new VWPicturePanel(TAB_ROUTE_NAME, VWImages.DocRouteImage);
	javax.swing.JPanel routeConfigLPanel = new javax.swing.JPanel();
	
	javax.swing.JPanel VWRouteOption = new javax.swing.JPanel();
	
	javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
	
	javax.swing.JPanel PanelRouteConfig = new javax.swing.JPanel();
	
	JLabel Pic = new JLabel();
	JLabel lblAvailableRoute = new JLabel();	
	JLabel lblAvailableRoute2 = new JLabel();	
	JLabel lblRouteName = new JLabel();	
	JTextField txtRouteName = new JTextField();
	JLabel lblTriggerAt = new JLabel();
	JLabel lblRoomLocation = new JLabel();
	JLabel lblCaption = new JLabel(LBL_ROUTINGTITLE);

	
	JComboBox cmbAvailableRoute = new JComboBox();
	JComboBox cmbAvailableAssignRoute = new JComboBox();
	JCheckBox chkReadOnly = new JCheckBox();
	
	DefaultListModel availableListModel = new DefaultListModel();
	// DefaultListModel approverListModel = new DefaultListModel();
	
	JList lstAvailableUsers = new JList(availableListModel);	
	VWLinkedButton BtnNewRoute = new VWLinkedButton(1);	
	VWLinkedButton BtnUpdate = new VWLinkedButton(1);	
	VWLinkedButton BtnDelete = new VWLinkedButton(1);	
	VWLinkedButton BtnSaveAs = new VWLinkedButton(1);	
	VWLinkedButton BtnSaveAsHTML = new VWLinkedButton(1);	
	VWLinkedButton BtnReviewUpdate = new VWLinkedButton(1);	
	JButton BtnProgressClick=new  JButton();
	VWLinkedButton BtnDocInRoute = new VWLinkedButton(1);	
	VWLinkedButton BtnAccept = new VWLinkedButton(1);	
	VWLinkedButton BtnReject = new VWLinkedButton(1);	
	VWLinkedButton BtnEndRoute = new VWLinkedButton(1);	
	VWLinkedButton BtnSummary = new VWLinkedButton(1);
	VWLinkedButton BtnRouteCompletedDocs = new VWLinkedButton(1);
	VWLinkedButton BtnTaskCompletedDocs = new VWLinkedButton(1);
	
	javax.swing.JPanel PanelTable = new javax.swing.JPanel();	
	javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();	
	javax.swing.JScrollPane SPAvailableUsers = new javax.swing.JScrollPane();	
	javax.swing.JScrollPane SPApprovers = new javax.swing.JScrollPane();
	
	VWDocRouteTable Table = new VWDocRouteTable();	
	VWCheckList userList = new VWCheckList();	
	Vector vApproversList = new Vector();	
	JLabel lblUNCInfo = null;
	JLabel lblTableHeader = new JLabel(LBL_DOCS_IN_ROUTE_TABLE_HEADER);
	
	public VWDlgSaveLocation dlgLocation = null;	
	private boolean routeLoaded = false;	
	private static int gCurRoom = 0;
	private int retUpdate;
	private int retDelete;
	private boolean UILoaded = false;
	
	public static boolean usersModified = false;
	public static String selectedRouteName = "";
	public static int selectedRouteId = 0;
	private int currentMode = -1;
	VWApproversListPanel routeUserListTable = new VWApproversListPanel();
	private JLabel lblRouteDesc = new JLabel();
	private JLabel lblRouteInvalid = new JLabel();
	private JLabel lblTaskInvalid = new JLabel();
	private JTextArea taRouteDesc = new JTextArea(2, 2);
	private JScrollPane spRouteDesc = new JScrollPane(taRouteDesc);
	JDialog processDialog = new JDialog();
	JLabel statusLbl = new JLabel(Util.SepChar+"  Validating and applying WorkFlow Changes. Please Wait...");
	public static boolean inUpdateMode = false;
	public String invalidTasks = "";
	
	DRSDrawing drsDrawing = new DRSDrawing();
	JPanel pnlDRSDrawing = new JPanel();
	RouteInfo routeInfoBean = new RouteInfo(100);
	RouteIndexInfo routeIndexInfoBean = new RouteIndexInfo();
	RouteTaskInfo routeTaskInfoBean = new RouteTaskInfo();	
	DrawingView view = null;	
	public int CURRENT_MODE = 0;	
	public int routeValidity;
	public int invalidX, invalidY, invalidWidth, invalidHeight, stdIncreaseHeight = 20, validHeight, validWidth; 
	public int routeDiagramX, routeDiagramY, routeDiagramWidth, routeDiagramHeight, routeDiagramStdY=175, stdRouteY;
	
	ArrayList alRouteTaskInfo = new ArrayList();
	ArrayList alRouteTaskSequence = new ArrayList();
	public static boolean taskSequenceAdded = false;		
	public static int endDistStartX = 250;
	public static int endDistStartY = 52;
	public static int endDistEndX = 350;
	public static int endDistEndY = 550;
	public static VWEndFigure rFigure = new VWEndFigure();
	public static VWStartIconProperties vwStartIconProperties = new VWStartIconProperties(); 
	VWLinkedButton btnEnlarge = new VWLinkedButton(1);
	public int tableWidth = 700;
	public static boolean viewRouteEndIcon = false;
	public static void main(String[] args) {
		try {
			String plasticLookandFeel = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		} catch (Exception se) {
		}
		JFrame frame = new JFrame();
		VWRoutePanel panel = new VWRoutePanel();
		frame.add(panel);
		panel.setupUI();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*
		 * JFrame.resize() is replaced with JFrame.setSize() as JFrame.resize() is deprecated
		 * Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
		frame.setSize(800, 600);
	}
	
	public void unloadTabData() {
		selectedRouteName = "";
		cmbAvailableAssignRoute.removeAllItems();
		SPTable.getViewport().add(Table);
		PanelRouteConfig.setVisible(false);
		Table.clearData();
		repaint();
		gCurRoom = 0;
		setEnableMode(MODE_UNCONNECT);
	}
}
