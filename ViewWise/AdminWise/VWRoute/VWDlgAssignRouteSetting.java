package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.computhink.common.RouteInfo;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

/**
 * @author Pandiya Raj.M
 *	
 * VWAssignRouteSetting class used to assign the created route to the document Type.  
 *
 */
public class VWDlgAssignRouteSetting extends JDialog implements VWConstant, ListDataListener{

	/**
	 * @param args
	 */
	public VWDlgAssignRouteSetting(){
		 
	}
	
	public void intervalAdded(ListDataEvent lDataEvent){
		loadAvailableRoutes();
		if(this.availableRouteListModel.size()==0){
			this.BtnMoveLeft.setEnabled(false);
			this.BtnMoveLeftAll.setEnabled(false);
		}
		else{
			this.BtnMoveLeft.setEnabled(true);
			this.BtnMoveLeftAll.setEnabled(true);
		}
		
		if(this.assignedRouteListModel.size()==0){
			this.BtnMoveRight.setEnabled(false);
			this.BtnMoveRightAll.setEnabled(false);
		}
		else{
			this.BtnMoveRight.setEnabled(true);
			this.BtnMoveRightAll.setEnabled(true);
		}
		
	}
	public void intervalRemoved(ListDataEvent lDataEvent){
		loadAvailableRoutes();
		if(this.availableRouteListModel.size()==0){
			this.BtnMoveLeft.setEnabled(false);
			this.BtnMoveLeftAll.setEnabled(false);
		}
		else{
			this.BtnMoveLeft.setEnabled(true);
			this.BtnMoveLeftAll.setEnabled(true);
		}
		
		if(this.assignedRouteListModel.size()==0){
			this.BtnMoveRight.setEnabled(false);
			this.BtnMoveRightAll.setEnabled(false);
		}
		else{
			this.BtnMoveRight.setEnabled(true);
			this.BtnMoveRightAll.setEnabled(true);
		}
		
	}
	public void contentsChanged(ListDataEvent lDataEvent){
	
	}

	int dlgWidth = 428, dlgHeight = 303, docTypeId = 0;
	public VWDlgAssignRouteSetting(Frame parent,int docTypeId, Vector routeList){
		 super(parent);
		 getContentPane().setBackground(AdminWise.getAWColor());
		 this.vecRouteInfo = routeList;
		 setSize(dlgWidth, dlgHeight);
		 setLocation(350,250);
		 this.docTypeId = docTypeId;
		 getContentPane().setLayout(null);
		 panelAssignRoute.setLayout(null);
		 panelAssignRoute.setBackground(AdminWise.getAWColor());
		 getContentPane().add(panelAssignRoute);
		 vwDocTypeRec = null;
		 initAssignRouteUI();
         //JOptionPane.showMessageDialog(null,"docTypeId: "+docTypeId,"",JOptionPane.OK_OPTION);
         vwDocTypeRec = VWDocTypeConnector.getDocTypeInfo(docTypeId);
		 setTitle(LBL_ASSIGNROUTESETTING_NAME + " for \"" + vwDocTypeRec.getName() + "\"");
		 currentDocTypeId = vwDocTypeRec.getId(); 
		 loadAvailableRoutes();
		 loadAssignedRoute();
		 setResizable(false);
		 setVisible(true);
	}
	private void initAssignRouteUI(){
		panelAssignRoute.setSize(dlgWidth, dlgHeight);
		int height = 22, width = 120, left = 15, top = 9, heightGap = 25, btnWidth = 80,
			leftGap = 12, lstWidth = 175, lstHeight = 185, brwBtnWidth = 26;
		//leftGap = 15, lstWidth = 175, lstHeight = 190, brwBtnWidth = 20;
		
		left = leftGap + 1;
		lblAvailableRoutes.setText(LBL_AVAILABLEROUTES);
		lblAvailableRoutes.setBounds(left, top, width, height);
		panelAssignRoute.add(lblAvailableRoutes);
		
		left += lstWidth + 48;
		lblAssignedRoutes.setText(LBL_ASSIGNEDROUTES);
		lblAssignedRoutes.setBounds(left, top, width, height);
		panelAssignRoute.add(lblAssignedRoutes);
		
		left = leftGap;	
		top += heightGap;
		SPAvailableRoute.getViewport().add(lstAvailableRoutes);
		SPAvailableRoute.setBounds(left, top, lstWidth, lstHeight);
		panelAssignRoute.add(SPAvailableRoute);
		
    	heightGap = 50;    	
    	panelAssignRoute.add(BtnMoveLeftAll);
    	left += lstWidth +10 ;    	
    	BtnMoveLeftAll.setBounds(left, top, brwBtnWidth, height);
    	BtnMoveLeftAll.setIcon(VWImages.MoveLeftAllIcon);
    	//BtnMoveLeftAll.setText(">>");
    	//BtnMoveLeftAll.setBackground(Color.WHITE);
    	BtnMoveLeftAll.setBorder(null);
    	BtnMoveLeftAll.setOpaque(false);
    	
    	panelAssignRoute.add(BtnMoveLeft);
    	top += heightGap;
    	BtnMoveLeft.setBounds(left, top, brwBtnWidth, height);
    	BtnMoveLeft.setIcon(VWImages.MoveLeftIcon);
    	//BtnMoveLeft.setText(">");
    	//BtnMoveLeft.setBackground(Color.WHITE);
    	BtnMoveLeft.setBorder(null);
    	BtnMoveLeft.setOpaque(false);
    	
    	panelAssignRoute.add(BtnMoveRight);
    	top += heightGap;
    	BtnMoveRight.setBounds(left, top, brwBtnWidth, height);
    	BtnMoveRight.setIcon(VWImages.MoveRightIcon);
    	//BtnMoveRight.setText("<");
    	//BtnMoveRight.setBackground(Color.WHITE);
    	BtnMoveRight.setBorder(null);
    	BtnMoveRight.setOpaque(false);
    	
    	panelAssignRoute.add(BtnMoveRightAll);
    	top += heightGap - 2;
    	BtnMoveRightAll.setBounds(left, top, brwBtnWidth, height);
    	BtnMoveRightAll.setIcon(VWImages.MoveRightAllIcon);
    	//BtnMoveRightAll.setText("<<");
    	//BtnMoveRightAll.setBackground(Color.WHITE);
    	BtnMoveRightAll.setBorder(null);
    	BtnMoveRightAll.setOpaque(false);
		
		left += 35; top -= (3 * heightGap) - 2;
		SPAssignedRoute.getViewport().add(lstAssignedRoutes);
		SPAssignedRoute.setBounds(left, top, lstWidth, lstHeight);	
		panelAssignRoute.add(SPAssignedRoute);
		
		this.assignedRouteListModel.addListDataListener(this);

		left = 151 ;top += (3 * heightGap) + heightGap;
		BtnRefresh.setText(BTN_RESET_NAME);
		if(languageLocale.equals("nl_NL")){
			BtnRefresh.setBounds(leftGap, top, AdminWise.gBtnWidth+12, AdminWise.gBtnHeight);
		}else
		BtnRefresh.setBounds(leftGap, top, AdminWise.gBtnWidth, AdminWise.gBtnHeight);
//        BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setIcon(VWImages.RefreshIcon);		
		panelAssignRoute.add(BtnRefresh);
		
		left += btnWidth + 5; 
		BtnSave.setText(BTN_SAVE_NAME);
		if(languageLocale.equals("nl_NL")){
			BtnSave.setBounds(left + 10-30, top, AdminWise.gBtnWidth+12, AdminWise.gBtnHeight);
		}else
		BtnSave.setBounds(left + 10, top, AdminWise.gBtnWidth, AdminWise.gBtnHeight);
//        BtnSave.setBackground(java.awt.Color.white);
        BtnSave.setIcon(VWImages.SaveIcon);
		panelAssignRoute.add(BtnSave);
		
		left += btnWidth + leftGap;
		BtnCancel.setText(BTN_CANCEL_NAME);
		if(languageLocale.equals("nl_NL")){
		BtnCancel.setBounds(left+2-15, top, AdminWise.gBtnWidth+12, AdminWise.gBtnHeight);
		}else
			BtnCancel.setBounds(left+2, top, AdminWise.gBtnWidth, AdminWise.gBtnHeight);	
//        BtnCancel.setBackground(java.awt.Color.white);
        BtnCancel.setIcon(VWImages.CloseIcon);		
		panelAssignRoute.add(BtnCancel);
		
		SymAction lSymAction = new SymAction();
    	BtnMoveLeft.addActionListener(lSymAction);
    	BtnMoveRight.addActionListener(lSymAction);
    	BtnMoveLeftAll.addActionListener(lSymAction);
    	BtnMoveRightAll.addActionListener(lSymAction);
    	BtnSave.addActionListener(lSymAction);
    	BtnCancel.addActionListener(lSymAction);
    	BtnRefresh.addActionListener(lSymAction);    

	}

	public void loadData(int docTypeId){
		// This method called directly when user clicks on 'Assign Route'. 
		// Assign the current selected docTypeId.  
		this.docTypeId = docTypeId;
        vwDocTypeRec = VWDocTypeConnector.getDocTypeInfo(docTypeId);
        setTitle(LBL_ASSIGNROUTESETTING_NAME + " for \""+vwDocTypeRec.getName()+"\"");
        currentDocTypeId = docTypeId; 
        loadAvailableRoutes();
        loadAssignedRoute();  
        removeAvailable();
	}
	
	private void removeAvailable(){
		int assignedSize=this.assignedRouteListModel.size();
		for(int x=0;x<assignedSize;x++){
			String currentApprover=String.valueOf(this.assignedRouteListModel.get(x)).trim();
			for(int y=0;y<this.availableRouteListModel.size();y++){
				if(String.valueOf(availableRouteListModel.get(y)).trim().equalsIgnoreCase(currentApprover.trim())){
					availableRouteListModel.remove(y);
				}
			}
		}
		AdminWise.printToConsole(" in load 3  available "+availableRouteListModel.size());
		AdminWise.printToConsole("in load 3 assignedSize "+assignedRouteListModel.size());
	}
	
	private void loadAvailableRoutes() {
		try {
			Vector routeList = new Vector();
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			AdminWise.printToConsole("room " + room);

			if(room == null || room.getConnectStatus() != Room_Status_Connect)
				return;
			int gCurRoom = room.getId();
			routeList = this.vecRouteInfo;
			AdminWise.printToConsole("routeList " + routeList.size());
			availableRouteListModel.removeAllElements();
			if (routeList != null && routeList.size()>0) {				
				//availableRouteListModel.removeAllElements();
				for (int i = 0; i < routeList.size(); i++)
					availableRouteListModel.addElement(routeList.get(i));
			}
			//else{
				//availableRouteListModel.removeAllElements();
				//AdminWise.printToConsole(" Add Available Routes ");				
			//}
				
			
			int assignedSize=this.assignedRouteListModel.size();
			
			for(int x=0;x<assignedSize;x++){
				String currentApprover=String.valueOf(this.assignedRouteListModel.get(x)).trim();
				for(int y=0;y<this.availableRouteListModel.size();y++){
					if(String.valueOf(availableRouteListModel.get(y)).trim().equalsIgnoreCase(currentApprover.trim())){
						availableRouteListModel.remove(y);
					}
				}
			}
			//AdminWise.printToConsole(" in load available "+availableRouteListModel.size());
			//AdminWise.printToConsole("in load assignedSize "+assignedSize);
			
		} catch (Exception ex) {
			AdminWise.printToConsole(" loadAvailableRoutes Exception "
					+ ex.getMessage());
		}
	}

	private void loadAssignedRoute(){
		Vector assignedRouteList = new Vector();
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		AdminWise.printToConsole("room " + room);

		if(room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		int gCurRoom = room.getId();
		AdminWise.gConnector.ViewWiseClient.getAssignedRoutesOfDocType(gCurRoom, currentDocTypeId,
				assignedRouteList);
		AdminWise.printToConsole(" assignedRouteList " + assignedRouteList.size());
		AdminWise.printToConsole(" assignedRouteList " + assignedRouteList.toString());
		assignedRouteListModel.removeAllElements();
		if (assignedRouteList.size() > 0) {
			
			for (int index = 0; index < assignedRouteList.size(); index++){
				assignedRouteListModel.addElement(assignedRouteList.get(index));
			}
		}
		//AdminWise.printToConsole(" in load 2  available "+availableRouteListModel.size());
		//AdminWise.printToConsole("in load 2 assignedSize "+assignedRouteListModel.size());
	}
	private void saveAssignedRoute(){
		try{
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		AdminWise.printToConsole("room " + room);

		if(room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		int gCurRoom = room.getId();
		//if (assignedRouteListModel.getSize() > 0){
			ArrayList routeId = new ArrayList(); 
			//AdminWise.printToConsole("assignedRouteListModel.getSize()  " + assignedRouteListModel.getSize());
			for (int index = 0; index < assignedRouteListModel.getSize(); index++){
				//AdminWise.printToConsole(" ((RouteInfo)assignedRouteListModel.elementAt(index)).getRouteId() "+  ((RouteInfo)assignedRouteListModel.elementAt(index)).getRouteId());
				routeId.add(((RouteInfo)assignedRouteListModel.elementAt(index)).getRouteId());
			}
			//AdminWise.printToConsole("Size of the routeId array " + routeId.size());
			int ret = AdminWise.gConnector.ViewWiseClient.setDocTypeRoutes(gCurRoom, routeId, currentDocTypeId);
			//AdminWise.printToConsole("return value Assign to Route " + ret);
		//}
		}
		catch(Exception ex){
			AdminWise.printToConsole("Exception " + ex.getMessage());
		}
	}
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if(object == BtnMoveLeft){
            	BtnMoveLeft_actionPerformed(event);
            }else if(object == BtnMoveRight){
            	BtnMoveRight_actionPerformed(event);
            }else if(object == BtnMoveLeftAll){
            	BtnMoveLeftAll_actionPerformed(event);
            }else if(object == BtnMoveRightAll){
            	BtnMoveRightAll_actionPerformed(event);
            }else if(object == BtnSave){
            	BtnSave_actionPerformed(event);
            }else if(object == BtnCancel){
            	BtnCancel_actionPerformed(event);
            }else if(object == BtnRefresh){
            	BtnRefresh_actionPerformed(event);
            }
        }
    }

    void BtnMoveLeft_actionPerformed(java.awt.event.ActionEvent event){
    	AdminWise.printToConsole("  lstAvailableUsers.getSelectedIndex()  " + availableRouteListModel.getSize());
    	/*
		 * Jlist.getSelectedValues() is replaced with JList.getSelectedValuesList().toArray() 
		 * as JList.getSelectedValues() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
    	Object[] selectedPosition = lstAvailableRoutes.getSelectedValuesList().toArray();
    	if (selectedPosition.length > 0){
    		for (int count = 0;count < selectedPosition.length; count++){
    			assignedRouteListModel.addElement(selectedPosition[count]);    		
    			availableRouteListModel.removeElement(selectedPosition[count]);
    		}
    	}    	
    }
    
    void BtnMoveRight_actionPerformed(java.awt.event.ActionEvent event){
    	AdminWise.printToConsole("  lstAvailableUsers.getSelectedIndex()  " + availableRouteListModel.getSize());
    	/*
		 * Jlist.getSelectedValues() is replaced with JList.getSelectedValuesList().toArray() 
		 * as JList.getSelectedValues() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
    	Object[] selectedPosition = lstAssignedRoutes.getSelectedValuesList().toArray();
    	if (selectedPosition.length > 0){
    		for (int count = 0;count < selectedPosition.length; count++){
    			availableRouteListModel.addElement(selectedPosition[count]);    		
    			assignedRouteListModel.removeElement(selectedPosition[count]);
    		}
    	}    	
    }
    void BtnMoveLeftAll_actionPerformed(java.awt.event.ActionEvent event){
    	try{
    		ArrayList alAvailableRouteLists=new ArrayList();
    		for(int countX=0;countX<availableRouteListModel.size();countX++){
    			alAvailableRouteLists.add(availableRouteListModel.get(countX));
    		}
    		for(int countY=0;countY<alAvailableRouteLists.size();countY++){
    			assignedRouteListModel.addElement(alAvailableRouteLists.get(countY));
    		}
    	}catch(Exception e){
    	//	JOptionPane.showMessageDialog(null,e.toString());
    	}
    	
    		
    }
    
    void BtnMoveRightAll_actionPerformed(java.awt.event.ActionEvent event){
		while(assignedRouteListModel.getSize() > 0){
			availableRouteListModel.addElement(assignedRouteListModel.getElementAt(0));    		
			assignedRouteListModel.removeElementAt(0);
		}    	
    }    
    
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event){
    	saveAssignedRoute();
    	setVisible(false);
    }   
    
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event){
    	setVisible(false);
    }   
    
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event){
    	availableRouteListModel.removeAllElements();
    	assignedRouteListModel.removeAllElements();
    	loadData(docTypeId);
    }

	JLabel lblAvailableRoutes = new JLabel();
	JLabel lblAssignedRoutes = new JLabel();
	
	DefaultListModel availableRouteListModel = new DefaultListModel();
	DefaultListModel assignedRouteListModel = new DefaultListModel();
	JList lstAvailableRoutes = new JList(availableRouteListModel);
	JList lstAssignedRoutes = new JList(assignedRouteListModel);
	
    javax.swing.JScrollPane SPAvailableRoute = new javax.swing.JScrollPane();
    javax.swing.JScrollPane SPAssignedRoute = new javax.swing.JScrollPane();
    
	VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    
    VWLinkedButton BtnMoveLeft = new VWLinkedButton(2,true);
    VWLinkedButton BtnMoveRight = new VWLinkedButton(2,true);
    VWLinkedButton BtnMoveLeftAll = new VWLinkedButton(2,true);
    VWLinkedButton BtnMoveRightAll = new VWLinkedButton(2,true);    
    String languageLocale=AdminWise.getLocaleLanguage();
    

	VWDocTypeRec vwDocTypeRec = null;
	JPanel panelAssignRoute = new JPanel();
	int currentDocTypeId = -1;
	public Vector vecRouteInfo = new Vector();
    public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame = new JFrame();
		VWDlgAssignRouteSetting setting = new VWDlgAssignRouteSetting(AdminWise.adminFrame, 12,new Vector());
		frame.add(setting);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
	}

	/**
	 * @return Returns the vecRouteInfo.
	 */
	public Vector getVecRouteInfo() {
		return vecRouteInfo;
	}

	/**
	 * @param vecRouteInfo The vecRouteInfo to set.
	 */
	public void setVecRouteInfo(Vector vecRouteInfo) {
		this.vecRouteInfo = vecRouteInfo;
	}

}
