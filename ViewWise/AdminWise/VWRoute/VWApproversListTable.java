package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;

import com.computhink.common.Document;
import com.computhink.common.Principal;
import com.computhink.common.RouteMasterInfo;
/**
 * @author Nishad Nambiar
 *
 */



public class VWApproversListTable extends JTable implements Transferable, VWConstant
{
	private static final long serialVersionUID = 1L;
	public static boolean emailIconFlag = false;
	public static boolean actionIconFlag = false;
	
	public static VWApproversListTableData m_data;
	
	TableColumn[] ColModel=new TableColumn[10];//now only 6 is there
	static boolean[] visibleCol={true, true, true, true, true, true, true};
	Timer keyTimer;    

	PopupMenu popupMenu = new PopupMenu();

	public VWApproversListTable(){
		super();
		m_data = new VWApproversListTableData(this);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data); 
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		JTextField readOnlyText=new JTextField();
		readOnlyText.setEditable(true);
		Dimension tableWith = getPreferredScrollableViewportSize();
		setFont(readOnlyText.getFont());
		for (int k = 0; k < VWApproversListTableData.m_columns.length; k++) {
			TableCellRenderer renderer;

			DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
			textRenderer.setHorizontalAlignment(VWApproversListTableData.m_columns[k].m_alignment);
			renderer = textRenderer;
			VWApproversListRowEditor editor=new VWApproversListRowEditor(this);
			TableColumn column = new TableColumn
            (k,Math.round(tableWith.width*VWApproversListTableData.m_columns[k].m_width), renderer, editor);

			addColumn(column);   
			ColModel[k]=column;
		}

		JTableHeader header = getTableHeader();
		header.setUpdateTableInRealTime(false);
		SymMouse aSymMouse = new SymMouse();
		addMouseListener(aSymMouse);
		//header.addMouseListener(m_data.new ColumnListener(this));
		header.setReorderingAllowed(true);
		// setVisibleCols();
		setBackground(java.awt.Color.white);
		SymKey aSymKey = new SymKey();
		addKeyListener(aSymKey);
		keyTimer = new Timer(500, new keyTimerAction());
		keyTimer.setRepeats(false);
		///setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setDragEnabled(true); 

		getColumnModel().getColumn(0).setPreferredWidth(30);
		getColumnModel().getColumn(1).setPreferredWidth(30);
		getColumnModel().getColumn(2).setPreferredWidth(96);
		getColumnModel().getColumn(3).setPreferredWidth(115);
		getColumnModel().getColumn(4).setPreferredWidth(96);
		getColumnModel().getColumn(5).setPreferredWidth(79);

		/*		getColumnModel().getColumn(3).setPreferredWidth(104);
		getColumnModel().getColumn(4).setPreferredWidth(104);
		getColumnModel().getColumn(5).setPreferredWidth(75);*/


		getColumnModel().getColumn(0).setResizable(false);
		getColumnModel().getColumn(1).setResizable(false);

		setShowGrid(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		getColumnModel().getColumn(2).setResizable(true);
		getColumnModel().getColumn(3).setResizable(true);
		getTableHeader().setReorderingAllowed(false);

		/*//		Column Task hidden.
		getColumnModel().getColumn(3).setPreferredWidth(0); 
		getColumnModel().getColumn(3).setMinWidth(0);
		getColumnModel().getColumn(3).setMaxWidth(0);
		setBackground(Color.white);

//		Column Task Desc hidden.
		getColumnModel().getColumn(4).setPreferredWidth(0); 
		getColumnModel().getColumn(4).setMinWidth(0);
		getColumnModel().getColumn(4).setMaxWidth(0);
		setBackground(Color.white);

//		Column Task Length hidden.
		getColumnModel().getColumn(5).setPreferredWidth(0); 
		getColumnModel().getColumn(5).setMinWidth(0);
		getColumnModel().getColumn(5).setMaxWidth(0);
		setBackground(Color.white);

		//Column E-Signature hidden.
		getColumnModel().getColumn(6).setPreferredWidth(0); 
		getColumnModel().getColumn(6).setMinWidth(0);
		getColumnModel().getColumn(6).setMaxWidth(0);
		setBackground(Color.white);

		//Column Approver ID hidden.
		getColumnModel().getColumn(7).setPreferredWidth(0); 
		getColumnModel().getColumn(7).setMinWidth(0);
		getColumnModel().getColumn(7).setMaxWidth(0);*/
		setBackground(Color.white);
		setRowHeight(20);

		ApproversColumnHeaderToolTips tips = new ApproversColumnHeaderToolTips();

		// Assign a tooltip for each of the columns
		for (int c=0; c<getColumnCount(); c++) {
			TableColumn col = getColumnModel().getColumn(c);
			if(c==0){
				tips.setToolTip(col, AdminWise.connectorManager.getString("VWApproversListTable.setToolTip_1"));
			}
			else if(c==1){
				tips.setToolTip(col, AdminWise.connectorManager.getString("VWApproversListTable.setToolTip_2"));
			} 
			else if(c==2){
				tips.setToolTip(col, AdminWise.connectorManager.getString("VWApproversListTable.setToolTip_3"));
			}
			else if(c==3){
				tips.setToolTip(col, AdminWise.connectorManager.getString("VWApproversListTable.setToolTip_4"));
			} 
			else if(c==4){
				tips.setToolTip(col, AdminWise.connectorManager.getString("VWApproversListTable.setToolTip_5"));
			} 
			else if(c==5){
				tips.setToolTip(col, AdminWise.connectorManager.getString("VWApproversListTable.setToolTip_6"));
			}

		}
		header.addMouseMotionListener(tips);

		// Set the text and icon values on the columns for the icon render
		getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(iconHeaderRenderer);	    
		getColumnModel().getColumn(0).setHeaderValue(new TextAndIcon("", VWImages.SendMailIcon));
		// Set the icon renderer on the columns
		getTableHeader().getColumnModel().getColumn(1).setHeaderRenderer(iconHeaderRenderer);	    
		getColumnModel().getColumn(1).setHeaderValue(new TextAndIcon("", VWImages.UserIcon));

		//this property will set the value on lost focus of field - earlier user have to clicks on enter key
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		

		ListSelectionModel selectionModel= getSelectionModel();
		selectionModel.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e) {
				ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
				if (!listSelectionModel.isSelectionEmpty()){
					if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2)
						VWTaskIconProperties.btnRemove.setEnabled(false);
					else
						VWTaskIconProperties.btnRemove.setEnabled(true);
				}
				else{
					VWTaskIconProperties.btnRemove.setEnabled(false);
				}
			}
		});
	
		setTableResizable();
		
	}
	public void setTableResizable() {
		// Resize Table
		new VWTableResizer(this);
	}


//	----------------------------------------------------------------------------------------------------------------
	/**
	 * Desc   :This method returns the approvers (Single user or users in the current row if row contains a group)
	 * Author :Nishad Nambiar
	 * Date   :27-Mar-2008
	 */
	public String getSelectedApproversInGroup(int row){
		String users = "";
		String formattedUsers="";
		try{
			//for(int count=0;count<routeUserListTable.Table.getRowCount();count++){
			//VWApproverInfo vwApproverInfo = (VWApproverInfo)routeUserListTable.Table.getData().elementAt(count);
			String userOrGrpName = getValueAt(row, m_data.COL_APPROVERNAME).toString();
			int usrGrpType = Integer.parseInt(getValueAt(row, m_data.COL_APPROVERTYPE).toString());
			if(usrGrpType ==  Principal.GROUP_TYPE){// group
				Vector principals = new Vector();
				principals = VWRouteConnector.getDRSPrincipals(1, userOrGrpName);
				if(principals!=null && principals.size()>0){
					for(int i = 0; i< principals.size(); i++){
						Principal members =  (Principal)principals.get(i);
						users += members.getName()+", ";
					}
				}
				else{
					users += AdminWise.connectorManager.getString("VWApproversListTable.msg_1");
				}
			}else{// User
				users = userOrGrpName+", ";
			}
			//}
			//eND 
		}catch(Exception e){
			//JOptionPane.showMessageDialog(null,"Exception 2:"+e.toString());
		}		    
		if(users.equals(AdminWise.connectorManager.getString("VWApproversListTable.msg_1"))){
		 formattedUsers = "["+users.substring(0,(users.length()-1))+"]";
		}
		else{
			 formattedUsers = "["+users.substring(0,(users.length()-2))+"]";	
		}
		return formattedUsers;
	}
//	----------------------------------------------------------------------------------------------------------------



	// This class is used to hold the text and icon values
	// used by the renderer that renders both text and icons
	class TextAndIcon {
		TextAndIcon(String text, Icon icon) {
			this.text = text;
			this.icon = icon;
		}
		String text;
		Icon icon;
	}
    
	// This customized renderer can render objects of the type TextandIcon
	TableCellRenderer iconHeaderRenderer = new DefaultTableCellRenderer() {
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			// Inherit the colors and font from the header component
			if (table != null) {
				JTableHeader header = table.getTableHeader();
				if (header != null) {
					setForeground(header.getForeground());
					setBackground(header.getBackground());
					setFont(header.getFont());
				}
			}

			if (value instanceof TextAndIcon) {
				setIcon(((TextAndIcon)value).icon);
				setText(((TextAndIcon)value).text);
			} else {
				setText((value == null) ? "" : value.toString());
				setIcon(null);
			}
			setBorder(UIManager.getBorder("TableHeader.cellBorder"));
			setHorizontalAlignment(JLabel.CENTER);
			return this;
		}
	};

	public String getToolTipText(MouseEvent e) {
		String tip = null;
		int selRows[] = getSelectedRows();    	   
		java.awt.Point p = e.getPoint();
		int rowIndex = rowAtPoint(p);
		int colIndex = columnAtPoint(p);
		
		if(rowIndex == -1 || colIndex == -1)
			return null;
			int realColumnIndex = convertColumnIndexToModel(colIndex);
			VWApproverInfo vwApproverInfo = (VWApproverInfo)getData().elementAt(rowIndex);
			if (realColumnIndex == m_data.COL_APPROVEREMAIL) {
				if(vwApproverInfo.getApproverEmail()!=null 
						&& !vwApproverInfo.getApproverEmail().trim().equals("")
						&& !vwApproverInfo.getApproverEmail().trim().equals("-")
						&& !vwApproverInfo.getApproverEmail().trim().equals("0"))
					tip = "" + vwApproverInfo.getApproverEmail();
				else
					tip = "No mail id";
			}else if (realColumnIndex == m_data.COL_APPROVERNAME) {
				String users = getSelectedApproversInGroup(rowIndex);
				tip = users;
			}else if (realColumnIndex == m_data.COL_NOTIFY){
				tip = vwApproverInfo.getTaskNotify();
			}
			try{
				if(selRows.length>1)
					setRowSelectionInterval(selRows[0],selRows[selRows.length-1]);
				else
					setRowSelectionInterval(0,0);
			}catch(Exception ex){
			//System.out.println("Exception in getToolTip() Method :"+ex.toString());
			}
			return tip;
	}

	public class ApproversColumnHeaderToolTips extends MouseMotionAdapter {
		// Current column whose tooltip is being displayed.
		// This variable is used to minimize the calls to setToolTipText().
		TableColumn curCol;

		// Maps TableColumn objects to tooltips
		Map tips = new HashMap();

		// If tooltip is null, removes any tooltip text.
		public void setToolTip(TableColumn col, String tooltip) {
			if (tooltip == null) {
				tips.remove(col);
			} else {
				tips.put(col, tooltip);
			}
		}

		public void mouseMoved(MouseEvent evt) {
			TableColumn col = null;
			JTableHeader header = (JTableHeader)evt.getSource();
			JTable table = header.getTable();
			TableColumnModel colModel = table.getColumnModel();
			int vColIndex = colModel.getColumnIndexAtX(evt.getX());

			// Return if not clicked on any column header
			if (vColIndex >= 0) {
				col = colModel.getColumn(vColIndex);
			}

			if (col != curCol) {
				header.setToolTipText((String)tips.get(col));
				curCol = col;
			}
		}
	}

//------------------------------------------------------------------------------
	public void addData(Vector docs, int roomId,String roomName)
	{
		m_data.setData(docs,roomId,roomName);
		m_data.fireTableDataChanged();
	}
//------------------------------------------------------------------------------
	public void clearData()
	{
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//------------------------------------------------------------------------------
	public int getRowCount()
	{
		if (m_data==null) return 0;
		return m_data.getRowCount();
	}
//------------------------------------------------------------------------------
	public void insertData(Vector docs,int roomId,String roomName)
	{
		if(docs==null || docs.size()==0)
			return;
		for(int i=0;i<docs.size();i++)
		{
			VWApproverInfo doc=(VWApproverInfo)docs.get(i);
			m_data.insert(doc);
		}
		m_data.fireTableDataChanged();
	}
//------------------------------------------------------------------------------
	public void deleteRow(int row)
	{
		m_data.delete(row);
		m_data.fireTableDataChanged();
	}
//------------------------------------------------------------------------------
	public Vector getData()
	{
		m_data.fireTableDataChanged();
		return m_data.getData();
	}
//------------------------------------------------------------------------------
	public VWApproverInfo getRowData(int rowNum)
	{
		return m_data.getRowData(rowNum);
	}
//------------------------------------------------------------------------------
	public String getRowApproverId(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverId();
	}
//------------------------------------------------------------------------------
	public String getRowRoomId(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverId();

	}
//------------------------------------------------------------------------------
	public String getRowApproverName(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverName();
	}
//------------------------------------------------------------------------------
	public String getRowApproverAction(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverAction();
	}
//------------------------------------------------------------------------------
	public String getRowApproverEmail(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverEmail();
	}
//------------------------------------------------------------------------------
	public String getRowApproverTaskDescription(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverTaskDescription();
	}
//------------------------------------------------------------------------------
	public String getRowApproverTaskLength(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverTaskLength();
	}

//------------------------------------------------------------------------------
	public String getRowApproverESignature(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverESignature();
	}

//------------------------------------------------------------------------------
	public String getRowApproverEmailIcon(int rowNum)
	{
		VWApproverInfo row=m_data.getRowData(rowNum);
		if(row==null) return "";
		return row.getApproverESignature();
	}

//------------------------------------------------------------------------------
	public DataFlavor[] getTransferDataFlavors()
	{
		int i=0;
		int[] selRows=getSelectedRows();
		int count = selRows.length;
		DataFlavor[] data= new DataFlavor[count];
		for(i=0;i<count;i++)
			data[i]=new DataFlavor(getDocument(selRows[i]).getClass(),"Document");
		return data;
	}
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException,IOException
	{
		/*int selRow=getSelectedRow();
      if(selRow>=0)
        return this.getDocument(selRow);*/
		return null;
	}
	//--------------------------------------------------------------------------
	public boolean isDataFlavorSupported(DataFlavor flavor)
	{
		int selRow=getSelectedRow();
		if(selRow>=0) return true;
		return false;
	}

	public RouteMasterInfo getDocument(int rowNum)
	{
		return m_data.getDocument(rowNum);
	}

//------------------------------------------------------------------------------ 
  private void setVisibleCols()
	{
		TableColumnModel model=getColumnModel();
		int count =visibleCol.length;
		int i=0;
		while(i<count)
		{
			if (!visibleCol[i])
			{
				TableColumn column = model.getColumn(i);
				model.removeColumn(column);
				count--;
			}
			else
			{
				i++;
			}
		}
		if(count==0)return;
		Dimension tableWith = getPreferredScrollableViewportSize();
		int colWidth=(int)Math.round(tableWith.getWidth()/count);
		for (i=0;i<count;i++)
		{
			model.getColumn(i).setPreferredWidth(colWidth);
		}
		setColumnModel(model);
}
//------------------------------------------------------------------------------
	class SymKey extends java.awt.event.KeyAdapter
	{
		public void keyPressed(java.awt.event.KeyEvent event)
		{
			Object object = event.getSource();
			if (object instanceof JTable)
				Table_keyPressed(event);
		}
	}
//------------------------------------------------------------------------------
	void Table_keyPressed(java.awt.event.KeyEvent event)
	{
		if(event.getKeyCode()==event.VK_DOWN)
		{
			keyTimer.stop();
			keyTimer.start();
		}   
		else if(event.getKeyCode()==event.VK_UP)
		{
			keyTimer.stop();
			keyTimer.start();
		}
		else if(event.getKeyCode()==event.VK_ENTER)
		{
			LoadRowData(getSelectedRow());
			event.setKeyCode(event.VK_UNDEFINED);
		}else if(event.getKeyCode() == 107 || event.getKeyCode() == 61){//+ in number lock is 107; shift + in main is 61  
			insertRowOnPlusKeyPress();
			
			//Insertion row has to get selected.
			if(getRowCount() != 0)
			setRowSelectionInterval(this.getRowCount()-1, this.getRowCount()-1);
		}else if(event.getKeyCode() == 127){//Delete key is 127
			//61 shift and plus - 107 is + of number lock
			deleteSelectedRow();
			//Last row has to get selected.
			if(getRowCount() != 0)
			setRowSelectionInterval(this.getRowCount()-1, this.getRowCount()-1);
		}
	}
	
	public void insertRowOnPlusKeyPress(){
		Vector v = null;
		try{
			v = new Vector();
			int principalIndex = 1;
			Principal selUser = new Principal(-2, " ", 1, -2);
			selUser.setEmail("admin@computhink.com");
			String approverName = selUser.getName();
			String approversEmailID = selUser.getEmail();
			String userID = String.valueOf(selUser.getUserGroupId());
			String approverType = String.valueOf(selUser.getType());
			String principalId = String.valueOf(selUser.getId());
			
			Principal escalationUser = new Principal(-1, NONE , 1, -1);//-1 none, -2 auto accept and -3 auto reject
			String approverTaskEscalationId = String.valueOf(escalationUser.getUserGroupId());
			String approverTaskEscalationUserName = escalationUser.getName();
			
			
			
			VWApproverInfo approverInfo = new VWApproverInfo();

			approverInfo.setApproverEmailFlag(false);
			approverInfo.setApproverType(approverType);
			approverInfo.setApproverActionFlag(true);
			approverInfo.setApproverName(approverName);
			approverInfo.setApproverEmail("");
			approverInfo.setApproverId(userID);
			approverInfo.setApproverAction("Reject");
			approverInfo.setApproverTaskDescription("Task Description");
			approverInfo.setApproverPrincipalId(principalId);
			approverInfo.setApproverTaskLength("2");
			approverInfo.setApproverESignature("E-Sign");
			approverInfo.setApproverTask("Assign Task");
			
			approverInfo.setApproverTaskExpiration("00day 00hr 00min");
			approverInfo.setApproverTaskEscalationId(approverTaskEscalationId);
			approverInfo.setApproverTaskEscalationUserName(approverTaskEscalationUserName);
			approverInfo.setTaskNotify("");
			v.add(approverInfo);
			//this.m_data.setData(v,1,"");
			if(isEditing())
				getCellEditor().stopCellEditing();
			
			insertData(v,1," ");
		}catch (Exception e) {
			//System.out.println("Exception in insertRowOnPlusKeyPress() is : "+e.getMessage());
			e.printStackTrace();
		}
	}
	public void deleteSelectedRow(){
		try{
			int selRows[] = getSelectedRows();
			int delRow=0;
			if(selRows!=null && selRows.length>0){
				for(int curRow=(selRows.length-1);curRow>=0;curRow--){
					if(isEditing())
						getCellEditor().stopCellEditing();
					m_data.delete(selRows[curRow]);
					m_data.fireTableDataChanged();
				}
			}
		}catch(Exception e){
			//System.out.println("Exception in delecteSelectedRow is : "+e.getMessage());
			//AdminWise.printToConsole("Error :"+e.toString());
		}		
	}
//	------------------------------------------------------------------------------
	protected class keyTimerAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent e) {
			LoadPropData(getSelectedRow());
			keyTimer.stop();
		}
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
		public void mouseClicked(java.awt.event.MouseEvent event)
		{
			Point origin = event.getPoint();
			int row = rowAtPoint(origin);
			int column = columnAtPoint(origin);
			
			if(getSelectedRowCount() == 0 || !isRowSelected(row)){
				if(row >= 0 && row < getRowCount())
					setRowSelectionInterval(row, row);
				else
					clearSelection();
			}
			
			if(event.getButton() == java.awt.event.MouseEvent.BUTTON3){
				rSingleClick(event);
			}
			if (row == -1 || column == -1)
				return; // no cell found
			if(event.getButton() == java.awt.event.MouseEvent.BUTTON1 && event.getClickCount() == 1){
				lSingleClick(event,row,column);
		}
			
		}
	}
//------------------------------------------------------------------------------
void VWToDoListTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{

		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found

		if(event.getClickCount() == 1)
			lSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
/*void VWToDoListTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			rSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			rDoubleClick(event,row,column);
}*/
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event)
{
	if(popupMenu == null)
		popupMenu = new PopupMenu();
	
	int rowCount = this.getRowCount();
	Vector data = getData();
	if(data != null && data.size() > 0){
		VWApproverInfo approverInfo = (VWApproverInfo) data.get(0);
		if(approverInfo != null){
			String approverName = approverInfo.getApproverName();
			String escalationName = approverInfo.getApproverTaskEscalationUserName();
			if((approverName.startsWith("<<") && approverName.endsWith(">>")) ||
					(escalationName.startsWith("<<") && escalationName.endsWith(">>")))
				popupMenu.menuAdd.setEnabled(false);
			else
				popupMenu.menuAdd.setEnabled(true);
		}
	}
	
	popupMenu.show(this, event.getX(), event.getY());
}

/**
 * Issue : Right click on empty JTable is not bringing the pop-up menu.
 * Reason : Empty JTables ViewPort hieght will be zero(0). So the Mouse events on JTable are not triggered.
 * Solution : Overide the JTable method : getScrollableTracksViewportHeight(). This will set the JTable ViewPort hieght to parent's height
 * URL : http://www.javalobby.org/java/forums/t45536.html
		 */
public boolean getScrollableTracksViewportHeight(){
	return getPreferredSize().height < getParent().getHeight();
}
//------------------------------------------------------------------------------
private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
}
//------------------------------------------------------------------------------
private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
		if(VWRoutePanel.inUpdateMode){
			if (row >= m_data.getData().size()) return;
			VWApproverInfo vwApproverInfo = (VWApproverInfo)m_data.getData().elementAt(row);
			//Desc   :getting the selected row, then firing table change event, and then selecting the same 
			//		 row back.
			//Issue  :cannot select a line item from the list, As soon as select, it gets un-selected
			//Author :Nishad Nambiar
			//Date   :18-Sep-2007
			int rowSelected = getSelectedRow();
			m_data.fireTableDataChanged();
			ListSelectionModel selectionModel = getSelectionModel();
			selectionModel.setSelectionInterval(rowSelected, rowSelected);		
			if(col==0){
				AdminWise.printToConsole(" email "+vwApproverInfo.getApproverEmailFlag());
				if(vwApproverInfo.getApproverEmailFlag()){
					vwApproverInfo.setApproverEmailFlag(false);
				}
				else{
					if(vwApproverInfo.getApproverType().equals("2") && !vwApproverInfo.getApproverName().startsWith("<<") && !vwApproverInfo.getApproverName().endsWith(">>")){
						if(vwApproverInfo.getApproverEmail()==null || vwApproverInfo.getApproverEmail().trim().equals("") || vwApproverInfo.getApproverEmail().trim().equals("-")){
								VWTaskIconProperties.active = false;										
								VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWApproversListTable.msg_2"), VWMessage.DIALOG_TYPE);
								VWTaskIconProperties.active = true;
								vwApproverInfo.setApproverEmailFlag(false);	
						}															
						else{
							vwApproverInfo.setApproverEmailFlag(true);
						}
					}
					else{
						vwApproverInfo.setApproverEmailFlag(true);
					}
				}
				AdminWise.printToConsole("email "+vwApproverInfo.getApproverEmailFlag());
			}
			else if(col==1){
				AdminWise.printToConsole(" Action "+vwApproverInfo.getApproverActionFlag());
				if(vwApproverInfo.getApproverActionFlag())
					vwApproverInfo.setApproverActionFlag(false);
				else
					vwApproverInfo.setApproverActionFlag(true);
				AdminWise.printToConsole(" Action "+vwApproverInfo.getApproverActionFlag());
			}
		}
		repaint();
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
		/*
    LoadRowData(row);
    LoadPropData(row);
		 */
	}
//------------------------------------------------------------------------------
	private void LoadPropData(int row)
{
		/*
	 RouteMasterInfo routeMasterInfo =getDocument(row);
	    int docId=getRowDocId(row);
	    int roomId=getRowRoomId(row);
	    if (roomId>0 && docId>0)
	    {
	    	Document doc = new Document(docId);
	    	doc.setRoomId(roomId);
	        VWWeb.webPanel.propertiesPanel.loadDocProperties(doc);
	        VWWeb.webPanel.commentPanel.loadDocComments(roomId,docId);
	    }
		 */

}
//------------------------------------------------------------------------------
private void LoadRowData(int row)
{
		/*
    int docId=getRowDocId(row);
    int roomId=getRowRoomId(row);
    //String searchString=getRowSearchString(row);
   IconData idata = (IconData)getValueAt(row,1);
    String docName = (String)idata.m_data;
    ///VWTreeConnector.setTreeNodeSelection(roomId,docId,false,3,false);
    //String docVerRev=getRowVerRev(row);
    String docVerRev="";
    if(docVerRev==null || docVerRev.equals("") || docVerRev.equals(".")) docVerRev="";
    VWTableConnector.getDocFile(roomId,docId,docName,VWTreeConnector.getFixTreePath(),
    		docVerRev,"");
    VWTreeConnector.setTreeNodeSelection(roomId,docId,false,3,false);
   	VWWeb.webPanel.documentPanel.Table.highlightDocumentTableRow(roomId, docId);
		 */

}

	public void updateRow(int rowId, boolean freeze){
		//m_data.updateFreezeRow(rowId,freeze);
		m_data.fireTableDataChanged();
	}
}
//------------------------------------------------------------------------------
class ToDoListColumnData
{
	public String  m_title;
	float m_width;
	int m_alignment;

	public ToDoListColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//------------------------------------------------------------------------------
class VWApproversListTableData extends DefaultTableModel
{
	public static final ToDoListColumnData m_columns[] = {
		new ToDoListColumnData( "",0.1F,JLabel.LEFT ),
		new ToDoListColumnData( "",0.2F,JLabel.LEFT ),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_1"),0.2F, JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_2"),0.2F,JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_3"),0.2F,JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_4"),0.2F,JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_5"),0.0F,JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_6"),0.0F,JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_7"),0.0F,JLabel.LEFT),
		new ToDoListColumnData( AdminWise.connectorManager.getString("VWApproversListTable.ToDoListColumnData_8"),0.0F,JLabel.LEFT),
	};

	//This is the place deciding the column position
	public static final int COL_APPROVEREMAIL=0;
	public static final int COL_APPROVERACTION=1;
	public static final int COL_APPROVERNAME=2;
	public static final int COL_EXPIRATION=3;
	public static final int COL_ESCALATION=4;
	public static final int COL_NOTIFY=5;
	public static final int COL_APPROVERMAILID=6;
	public static final int COL_APPROVERID=7;
	public static final int COL_APPROVERTYPE=8;
	public static final int COL_ESCALATION_USER_ID=9;

	protected VWApproversListTable m_parent;
	protected Vector m_vector;
	protected int m_sortCol = 0;
	protected boolean m_sortAsc = true;

	public VWApproversListTableData(VWApproversListTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}

	public Class getColumnClass(int columnIndex) {	
		//if(columnIndex == 0)
		//  return Boolean.class;
		//else
		return Object.class;
	}

//------------------------------------------------------------------------------
public void setData(Vector docs,int roomId,String roomName) {
		m_vector.removeAllElements();
		if(docs==null || docs.size()==0)    return;
		int count =docs.size();
		int freezed = -1;
		for(int i=0;i<count;i++)
		{
			VWApproverInfo approverInfo=(VWApproverInfo)docs.get(i);
			m_vector.addElement(approverInfo);
		}
	}

public RouteMasterInfo getDocument(int rowNum) {
		return (RouteMasterInfo)m_vector.elementAt(rowNum);
}
public Document getDocumentObject(int rowNum) {
		Document doc = new Document(((RouteMasterInfo)m_vector.elementAt(rowNum)).getDocId());    
		return doc;
}

//------------------------------------------------------------------------------
public Vector getData() {
		return m_vector;
	}
//------------------------------------------------------------------------------
public VWApproverInfo getRowData(int rowNum) {

		try
		{
			return (VWApproverInfo)m_vector.elementAt(rowNum);
		}
		catch(Exception e)
		{
			return null;
		} 
	}

//------------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size(); 
	}
//------------------------------------------------------------------------------
	public int getColumnCount() { 
		return m_columns.length; 
	} 
//------------------------------------------------------------------------------
	public String getColumnName(int column) { 
		///return m_columns[column].m_title; 
		String str = m_columns[column].m_title;
		if (column==m_sortCol)
			str += m_sortAsc ? " �" : " �";
		return str;
	}
//------------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return ( (nCol >= 2) ) ? true : false;
	}
//------------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		VWApproverInfo row = (VWApproverInfo)m_vector.elementAt(nRow);
		switch (nCol) {
		case COL_APPROVEREMAIL: return new IconData(getEmailIcon(row) , "", Color.BLACK);
		case COL_APPROVERACTION: return new IconData(getActionIcon(row) , "", Color.BLACK);
		case COL_APPROVERNAME: return new ColorData(row.getApproverName(),Color.BLACK);
		case COL_EXPIRATION: return new ColorData(row.getApproverTaskExpiration(),Color.BLACK);
		case COL_ESCALATION: return new ColorData(row.getApproverTaskEscalationUserName(),Color.BLACK);
		case COL_NOTIFY: return new ColorData(row.getTaskNotify(),Color.BLACK);              
		case COL_APPROVERID: return new ColorData(row.getApproverId(),Color.BLACK);
		case COL_APPROVERMAILID: return new ColorData(row.getApproverEmail(),Color.BLACK);
		case COL_APPROVERTYPE: return new ColorData(row.getApproverType(),Color.BLACK);
		case COL_ESCALATION_USER_ID: return new ColorData(row.getApproverTaskEscalationId(), Color.BLACK);
		}
		return "";
	}
	//-----------------------------------------------------------


	private ImageIcon getEmailIcon(VWApproverInfo row)
	{
		if(row.getApproverEmailFlag())
			return VWImages.SendMailIcon;
		else
			return VWImages.DontSendMailIcon;
	}
	private ImageIcon getActionIcon(VWApproverInfo row)
	{
		if(row.getApproverActionFlag())
			return VWImages.ApproverActionYesIcon;
		else
			return VWImages.ApproverActionNoIcon;
	}

	private ImageIcon getDocIcon(int signId, int status, boolean flag)
	{

		//Fix Start : Issue 496 ; Change the Icon when the document is check out
//		Fix Start : New issue 537 ; Change the Icon when the document is opened
		if(flag){
			//return VWImages.CopyIcon;
			return VWImages.UpIcon;
		}else {//if(status > 0 && signId == -2){
			//return VWImages.DelIcon;
			return VWImages.DownIcon;
		}
		//Fix End
//		Fix End
		/*if(signId==-1)
      {
          //return VWImages.AddIcon;
    	  System.out.println("ICON 4");
    	  return new ImageIcon("D:\\Projects\\ViewWise_MainLine\\IXRc\\ViewWise-DEV\\Source\\Build\\Source\\Java\\AdminWise\\ViewWise\\AdminWise\\VWImages\\images\\_UserAdd.gif");
      }
      else if(signId==0)
      {
    	  // Issue No 40 : Wrong image was loaded Created By : Valli Date 11 Oct 2006 
    	//  return VWImages.AllRoomIcon;
          //return VWImages.DocBCollapsedIcon;
    	  System.out.println("ICON 5");
    	  return new ImageIcon("D:\\Projects\\ViewWise_MainLine\\IXRc\\ViewWise-DEV\\Source\\Build\\Source\\Java\\AdminWise\\ViewWise\\AdminWise\\VWImages\\images\\_UserRemove.gif");
      }
      else if(signId>0)
      {
       //   return VWImages.BackupIcon;
    	  System.out.println("ICON 6");
    	  if(VWToDoListTable.emailIconFlag){
    		  return new ImageIcon("C:\\Inetpub\\wwwroot\\Save.gif");
    	  }else{
    		  return new ImageIcon("C:\\Inetpub\\wwwroot\\Ok.gif");
    	  }


      }



//    Fix Start : New issue 537 ; Change the Icon when the document is opened
      else if(signId == -2)
      {
         // return VWImages.AdvancedFindImage;
    	  System.out.println("ICON 7");
    	  return new ImageIcon("D:\\Projects\\ViewWise_MainLine\\IXRc\\ViewWise-DEV\\Source\\Build\\Source\\Java\\AdminWise\\ViewWise\\AdminWise\\VWImages\\images\\DelIcon.gif");
      }*/



		//Fix end
		//return null;
	}
	//-----------------------------------------------------------
//------------------------------------------------------------------------------
/*  public void setValueAt(Object value, int nRow, int nCol, int x) {
    if (nRow < 0 || nRow >= getRowCount())
      return;

     VWApproverInfo row = (VWApproverInfo)m_vector.elementAt(nRow);
    //Document row = (Document)m_vector.elementAt(nRow);

    String svalue = value.toString();

    if(nCol==COL_APPROVERNAME)row.setApproverName(svalue);    

 //-----------------------------------------------------------------------------
  }*/

	public void setValueAt(Object value, int nRow, int nCol){
		if (nRow < 0 || nRow >= getRowCount())
			return;
		VWApproverInfo row = (VWApproverInfo)m_vector.elementAt(nRow);
		String svalue = value.toString();
		switch (nCol) {
		case COL_APPROVEREMAIL: 
			row.setApproverEmail(svalue);
			//row.approverEmailFlag=Boolean.TRUE;
			//row.setApproverEmailFlag(VWApproversListTable.emailIconFlag);
			break;
			
		case COL_APPROVERACTION:
			row.setApproverAction(svalue);
			//row.setApproverActionFlag(VWApproversListTable.actionIconFlag);
			break;

		case COL_APPROVERNAME:
			row.approverName = svalue;
			//row.setApproverName(svalue);
			break;
			
		case COL_EXPIRATION:
			row.setApproverTaskExpiration(svalue);
			break;
			
		case COL_ESCALATION:
			row.setApproverTaskEscalationUserName(svalue);  
			break;
		
		case COL_APPROVERMAILID:
			row.setApproverEmail(svalue);
			break;
			
		case COL_NOTIFY:
			row.setTaskNotify(svalue);
			break;
			
		case COL_APPROVERID:
			row.setApproverId(svalue);
			break;
			
		case COL_APPROVERTYPE:
			row.setApproverType(svalue);
			break;
			
		case COL_ESCALATION_USER_ID:
			row.setApproverTaskEscalationId(svalue);
			break;
		}  
	}
//----------------------------------------------------------------------------
	public void insert(int row,VWApproverInfo approverinfo){
		if (row < 0)
			row = 0;
		if (row > m_vector.size())
			row = m_vector.size();
		m_vector.insertElementAt(approverinfo, row);
	}
	//----------------------------------------------------------------------------
	public void insert(VWApproverInfo rowData) {
		int count=m_vector.size();
		boolean found=false;
		for(int i=0;i<count;i++)
		{
			VWApproverInfo tmpRowData=(VWApproverInfo)m_vector.get(i);
			if(tmpRowData.getApproverId()==rowData.getApproverId() && 
					tmpRowData.getApproverId() == rowData.getApproverId())
			{
				found=true;
				break;
			}
		}
		if(!found) m_vector.addElement(rowData);
	}
//----------------------------------------------------------------------------
	public boolean delete(int row) {
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
	//----------------------------------------------------------------------------
public void clear(){
		m_vector.removeAllElements();
	}
//------------------------------------------------------------------------------
	class ColumnListener extends MouseAdapter
	{
		protected VWApproversListTable m_table;
//------------------------------------------------------------------------------
		public ColumnListener(VWApproversListTable table){
			m_table = table;
		}
//------------------------------------------------------------------------------
		public void mouseClicked(MouseEvent e){

			if(e.getModifiers()==e.BUTTON3_MASK)
				selectViewCol(e);
			else if(e.getModifiers()==e.BUTTON1_MASK)
				sortCol(e);
		}
//------------------------------------------------------------------------------
		private void sortCol(MouseEvent e)
		{
			TableColumnModel colModel = m_table.getColumnModel();
			int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
			int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

			if (modelIndex < 0)
				return;
			if (m_sortCol==modelIndex)
				m_sortAsc = !m_sortAsc;
			else
				m_sortCol = modelIndex;

			for (int i=0; i < colModel.getColumnCount();i++) {
				TableColumn column = colModel.getColumn(i);
				column.setHeaderValue(getColumnName(column.getModelIndex()));    
			}
			m_table.getTableHeader().repaint();  

			Collections.sort(m_vector, new 
					ToDoListComparator(modelIndex, m_sortAsc));
			m_table.tableChanged(
					new TableModelEvent(VWApproversListTableData.this)); 
			m_table.repaint();  
		}
//------------------------------------------------------------------------------
		private void selectViewCol(MouseEvent e)
		{
			/*
        javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
        for (int i=0; i < m_table.ToDoListColumnNames.length; i++){
         javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
                m_table.ToDoListColumnNames[i],m_table.visibleCol[i]);
        TableColumn column = m_table.ColModel[i];
        subMenu.addActionListener(new ColumnKeeper(column,VWToDoListTableData.m_columns[i]));
        menu.add(subMenu);
        }
        menu.show(m_table,e.getX(),e.getY());
			 */
		}
		class ColumnKeeper implements java.awt.event.ActionListener
		{
			protected TableColumn m_column;
			protected ToDoListColumnData  m_colData;

			public ColumnKeeper(TableColumn column,ToDoListColumnData colData){
				m_column = column;
				m_colData = colData;
			}

			public void actionPerformed(java.awt.event.ActionEvent e) {
				/*
    	javax.swing.JCheckBoxMenuItem item=(javax.swing.JCheckBoxMenuItem)e.getSource();
      TableColumnModel model = m_table.getColumnModel();
      boolean found=false;
      int i=0;
      int count=m_table.ToDoListColumnNames.length;
      int colCount=model.getColumnCount();
      while (i<count && !found){
        if(m_table.ToDoListColumnNames[i].equals(e.getActionCommand()))
            found=true;
        i++;
        }
      i--;
      if (item.isSelected()) {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.addColumn(m_column);
      }
      else {
        if(colCount>1)
        {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.removeColumn(m_column);
        }
      }
      m_table.tableChanged(new javax.swing.event.TableModelEvent(m_table.m_data)); 
      m_table.repaint();

				 */
			}

		}

	}
//------------------------------------------------------------------------------
class ToDoListComparator implements Comparator
{
		protected int     m_sortCol;
		protected boolean m_sortAsc;
//------------------------------------------------------------------------------
		public ToDoListComparator(int sortCol, boolean sortAsc) {
			m_sortCol = sortCol;
			m_sortAsc = sortAsc;
		}
//------------------------------------------------------------------------------
		//Not in use - Valli
		public int compare(Object o1, Object o2) {
			if(!(o1 instanceof RouteMasterInfo) || !(o2 instanceof RouteMasterInfo))
				return 0;
			RouteMasterInfo s1 = (RouteMasterInfo)o1;
			RouteMasterInfo s2 = (RouteMasterInfo)o2;
			/*if(!(o1 instanceof Document) || !(o2 instanceof Document))
        return 0;
      Document s1 = (Document)o1;
      Document s2 = (Document)o2;*/
			int result = 0;
			//double d1, d2;
			switch (m_sortCol) {
			case 0:
				result = s1.getRoomName().compareTo(s2.getRoomName());
				break;
			case 1:
				result = s1.getDocName().compareTo(s2.getDocName());
				break;
			case 2:
				result = s1.getRouteName().compareTo(s2.getRouteName());
				break;  
				/*case 3:
        result = s1.getDocType().getName().compareTo(s2.getDocType().getName());
        break;
      case 4:
        result = s1.getVersion().compareTo(s2.getVersion());
        break;*/
			case 3:
				result = s1.getCreatorName().compareTo(s2.getCreatorName());
				break;
			case 4:
				result = s1.getCreatedDate().compareTo(s2.getCreatedDate());
				break;
			case 5:
				result = s1.getModifiedDate().compareTo(s2.getModifiedDate());
				break;
			case 6:
				result = s1.getReceivedDate().compareTo(s2.getReceivedDate());
				break;
				/*case 7:
        result = String.valueOf(s1.getPageCount()).compareTo(String.valueOf(s2.getPageCount()));
        break;
      case 8:
        result = String.valueOf(s1.getCommentCount()).compareTo(String.valueOf(s2.getCommentCount()));
        break;*/
				/*case 9:
        result = String.valueOf(s1.getRefCount()).compareTo(String.valueOf(s2.getRefCount()));
        break;*/
			}
			if (!m_sortAsc)
				result = -result;
			return result;
		}
//------------------------------------------------------------------------------
		public boolean equals(Object obj) {
			if (obj instanceof ToDoListComparator) {
				ToDoListComparator compObj = (ToDoListComparator)obj;
				return (compObj.m_sortCol==m_sortCol) && 
				(compObj.m_sortAsc==m_sortAsc);
			}
			return false;
		}
}

}




class EmailCheckBoxCellRenderer extends JCheckBox implements TableCellRenderer,MouseListener {
	JCheckBox check = null;
	public EmailCheckBoxCellRenderer() {
		check = new JCheckBox();
	}

	public void mouseClicked(MouseEvent me){
		if(VWApproversListTable.actionIconFlag){
			VWApproversListTable.actionIconFlag = false;
		}
		else{
			VWApproversListTable.actionIconFlag = true;
		}
	}

	public void mouseEntered(MouseEvent me){

	}
	public void mouseExited(MouseEvent me){

	}
	public void mouseReleased(MouseEvent me){

	}
	public void mousePressed(MouseEvent me){

	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		setText(value.toString());

		super.setPreferredSize(new Dimension (100, 20));
		setSize(table.getColumnModel().getColumn(column).getWidth(), getPreferredSize().height);
		if (table.getRowHeight(row) != getPreferredSize().height) {
			table.setRowHeight(row, getPreferredSize().height);
		}
		return check;
	}
} 

class EmailCheckBoxEditor extends AbstractCellEditor implements TableCellEditor,ActionListener {
	protected JCheckBox check;
	String txt="Hello";

	public Object getCellEditorValue () {
		return Boolean.valueOf (check.isSelected ());
	}

	public EmailCheckBoxEditor() {
		check = new JCheckBox();
		check.addActionListener(this);
		check.setEnabled(true);
	}

	public void actionPerformed(ActionEvent ae){
		{
			if(VWApproversListTable.emailIconFlag){
				VWApproversListTable.emailIconFlag = false;
			}
			else{
				VWApproversListTable.emailIconFlag = true;
			}
		}
	}
	public Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
		txt = value.toString();
		return check;
	}
}

class ActionCheckBoxEditor1 extends AbstractCellEditor implements TableCellEditor,ActionListener {
	protected JCheckBox check;
	String txt="Hello";

	public Object getCellEditorValue () {
		return Boolean.valueOf (check.isSelected ());
	}

	public ActionCheckBoxEditor1() {
		check = new JCheckBox();
		check.addActionListener(this);
		check.setEnabled(true);
	}

	public void actionPerformed(ActionEvent ae){
		{
			if(VWApproversListTable.actionIconFlag){
				VWApproversListTable.actionIconFlag = false;
			}
			else{
				VWApproversListTable.actionIconFlag = true;
			}
		}
	}

	public Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
		txt = value.toString();
		return check;
	}
}

class PopupMenu extends JPopupMenu implements VWConstant, ActionListener{
	
	JMenuItem menuAdd;
	JMenuItem menuRemove;

	public PopupMenu(){
		menuAdd= new JMenuItem(ADD_MENU_ITEM);
		menuAdd.setIcon(getPopupMenuIcon(0));
		add(menuAdd);
		
		menuRemove = new JMenuItem(REMOVE_MENU_ITEM);
		menuRemove.setIcon(getPopupMenuIcon(1));
		add(menuRemove);
		
		menuAdd.addActionListener(this);
		menuRemove.addActionListener(this);		
	}
	private ImageIcon getPopupMenuIcon(int menuid){
		ImageIcon	popupIcon = null;
		switch (menuid){
		case 0:
			popupIcon = VWImages.addRow;
			break;
		case 1:
			popupIcon = VWImages.removeRow;
		
		}
		return popupIcon;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equalsIgnoreCase(ADD_MENU_ITEM)){
			try{
				AdminWise.adminPanel.routePanel.routeUserListTable.Table.insertRowOnPlusKeyPress();
			}catch(Exception ex){
				System.out.println("Exception in actionPerformed for Add :: "+ex.getMessage());
				ex.printStackTrace();
				
			}
			
		}else if(e.getActionCommand().equalsIgnoreCase(REMOVE_MENU_ITEM)){
			try{
			AdminWise.adminPanel.routePanel.routeUserListTable.Table.deleteSelectedRow();;
			}catch(Exception ex1){

				System.out.println("Exception in actionPerformed for Remove :: "+ex1.getMessage());
				ex1.printStackTrace();
			}
		}
		
	}
	
}
