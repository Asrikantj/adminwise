package ViewWise.AdminWise.VWRoute;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.computhink.common.VWEmailOptionsInfo;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

/**
 * @author Vijaypriya.B.K
 * December 07, 2010
 */

class VWEmailOptionsListPanel extends JPanel implements ActionListener{
	public static int x=0;
	public static VWEmailOptionsListTable Table = new VWEmailOptionsListTable();
	javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane(Table);
	public static VWLinkedButton btnAdd = new VWLinkedButton(2);
	public static VWLinkedButton btnRemove = new VWLinkedButton(2);
	Vector v=new Vector();
	
	public VWEmailOptionsListPanel(){
		initComponents();
	}
	public void initComponents(){

		setLayout(null);
		SPTable.setBounds(0,5,358,130);
		add(SPTable);
		setBackground(AdminWise.getAWColor());
		SPTable.getViewport().setBackground(AdminWise.getAWColor());
		btnAdd.setBounds(360,5,25,25);
		btnAdd.setToolTipText(AdminWise.connectorManager.getString("VWEmailOptionsListPanel.btnAdd"));
		btnAdd.setIcon(VWImages.ApproverAddIcon);
		btnAdd.addActionListener(this);
		add(btnAdd);
		
		btnRemove.setBounds(360,35,25,25);
		btnRemove.setToolTipText(AdminWise.connectorManager.getString("VWEmailOptionsListPanel.btnRemove"));
		btnRemove.setIcon(VWImages.ApproverRemoveIcon);
		btnRemove.addActionListener(this);
		btnRemove.setEnabled(false);
		add(btnRemove);
		
		ListSelectionModel selectionModel= Table.getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty()){
               		btnRemove.setEnabled(true);
                }
                else{
                	btnRemove.setEnabled(false);
                }
            }
        });
        
	}

	public void actionPerformed(ActionEvent ae) {
		AdminWise.printToConsole("Email option actionPerformed......");		
		if(ae.getSource()==btnAdd){
			VWEmailOptionsInfo vwEmailOptionsInfo = new VWEmailOptionsInfo();
			vwEmailOptionsInfo.setUserEmailAddress("");
			vwEmailOptionsInfo.setDocumentMetaDataFlag(1);
			vwEmailOptionsInfo.setRouteSummaryReportFlag(1);
			vwEmailOptionsInfo.setDocumentPagesFlag(1);
			/****CV2019 merges - SIDBI - VWR enhancement changes***/
			AdminWise.printToConsole("Bf calling setIncludeVwrDocument ");
			vwEmailOptionsInfo.setDocumentVwrFlag(0);
			vwEmailOptionsInfo.setEditableFlag(true);
			AdminWise.printToConsole("Af calling setIncludeVwrDocument ");
			/*------------------End of SIDBI merges----------------------------*/
			v.add(vwEmailOptionsInfo);
			if(VWEndIconProperties.validateEndIconProperties()){
				btnAdd.setEnabled(true);
				VWEmailOptionsListPanel.Table.insertData(v,1," ");
			}
			v.removeAllElements();
		}		
		else if(ae.getSource()==btnRemove){
			try{
				int selRows[] = Table.getSelectedRows();
				for(int curRow=(selRows.length-1);curRow>=0;curRow--){
					Table.m_data.delete(selRows[curRow]);
					Table.m_data.fireTableDataChanged();
				}
			}catch(Exception e){
				//AdminWise.printToConsole("Error :"+e.toString());
			}			
		}
		/****CV2019 merges - 10.2 - document pages issue fix***/
		VWEndIconProperties.setConvertToPDFAndCompressPages(null);
	}
}
