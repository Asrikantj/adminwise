package ViewWise.AdminWise.VWRoute;

/**
 * @author nishad.n
 *
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import org.jhotdraw.applet.DrawApplet;
import org.jhotdraw.application.DrawApplication;
import org.jhotdraw.contrib.Desktop;
import org.jhotdraw.contrib.DesktopListener;
import org.jhotdraw.contrib.JPanelDesktop;
import org.jhotdraw.figures.AttributeFigure;
import org.jhotdraw.framework.Drawing;
import org.jhotdraw.framework.DrawingEditor;
import org.jhotdraw.framework.DrawingView;
import org.jhotdraw.framework.Figure;
import org.jhotdraw.framework.FigureAttributeConstant;
import org.jhotdraw.framework.FigureChangeEvent;
import org.jhotdraw.framework.FigureChangeListener;
import org.jhotdraw.framework.FigureEnumeration;
import org.jhotdraw.framework.Tool;
import org.jhotdraw.framework.ViewChangeListener;
import org.jhotdraw.standard.AlignCommand;
import org.jhotdraw.standard.BufferedUpdateStrategy;
import org.jhotdraw.standard.ChangeAttributeCommand;
import org.jhotdraw.standard.DeleteCommand;
import org.jhotdraw.standard.SimpleUpdateStrategy;
import org.jhotdraw.standard.StandardDrawing;
import org.jhotdraw.standard.ToolButton;

import org.jhotdraw.util.CollectionsFactory;
import org.jhotdraw.util.ColorMap;
import org.jhotdraw.util.CommandChoice;
import org.jhotdraw.util.CommandMenu;
import org.jhotdraw.util.Iconkit;
import org.jhotdraw.util.PaletteButton;
import org.jhotdraw.util.PaletteLayout;
import org.jhotdraw.util.PaletteListener;
import org.jhotdraw.util.SerializationStorageFormat;
import org.jhotdraw.util.StandardStorageFormat;
import org.jhotdraw.util.StandardVersionControlStrategy;
import org.jhotdraw.util.StorageFormat;
import org.jhotdraw.util.StorageFormatManager;
import org.jhotdraw.util.UndoableCommand;
import org.jhotdraw.util.VersionControlStrategy;
import org.jhotdraw.util.VersionManagement;
import org.jhotdraw.util.VersionRequester;

import com.computhink.common.RouteTaskInfo;


import com.computhink.vwc.VWCPreferences;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoute.figures.DotDashBorder;
import ViewWise.AdminWise.VWRoute.figures.VWDrawingView;
import ViewWise.AdminWise.VWRoute.figures.VWEndFigure;
import ViewWise.AdminWise.VWRoute.figures.VWFigure;
import ViewWise.AdminWise.VWRoute.figures.VWLineConnection;
import ViewWise.AdminWise.VWRoute.figures.VWStartFigure;
import ViewWise.AdminWise.VWRoute.figures.VWSuspendFigure;
import ViewWise.AdminWise.VWRoute.figures.VWTaskFigure;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;


public class DRSDrawing extends DRSDrawingAreaPanel {
	public ToolButton toolBtnConnection = null;
	
	public ToolButton toolBtnEllipse = null;
	
	public ToolButton toolBtnRectangle = null;
	
	public ToolButton toolBtnTriangle = null;
	public final Charset UTF8_CHARSET = Charset.forName("UTF-8");
	public DRSDrawing() {
		listeners = CollectionsFactory.current().createList();
		drsDrawingAreaPanel = new DRSDrawingAreaPanel();

		KeyboardFocusManager.getCurrentKeyboardFocusManager()  
		.addKeyEventDispatcher(new KeyEventDispatcher(){  
			public boolean dispatchKeyEvent(KeyEvent ke){  
				//AdminWise.printToConsole("DRSDrawing ::: KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() :: "+KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner());
				if(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof VWDrawingView){ 
					if(ke.getID()==KeyEvent.KEY_PRESSED)  
					{  
						int key = ke.getKeyCode();  
						ke.consume();  
					}  
				}
				return false;
			}});
	}
	
	public DRSDrawing getDRSSample() {
		DRSDrawing drsSample = new DRSDrawing();
		drsSample.init();
		return drsSample;
	}
	
	public static void main(String ar[]) {
		JFrame frame = new JFrame();
		DRSDrawing app = new DRSDrawing();
		/**
		 * Reading the registry for setting the fontname and fontsize for diagram
		 * implemented for chinese localization
		 * Modified by Madhavan
		 */
		String fontName="Arial";
		if (VWCPreferences.getFontName().trim().length()> 0)
			fontName = VWCPreferences.getFontName();
		setUIFont(new FontUIResource(fontName, Font.PLAIN, VWCPreferences.getFontSize()));
		
		app.init();
		frame.add(app);
		frame.setDefaultCloseOperation(2);
		frame.setSize(400, 400);
		frame.setVisible(true);
	}
	
	public ToolButton toolBtnLine = null;
	
	//ToolButton toolBtnRect = null;
	
	/**
	 * Factory method which can be overriden by subclasses to
	 * create an instance of their type.
	 *
	 * @return	newly created application
	 */
	protected DRSDrawing createApplication() {
		return new DRSDrawing();
	}
	
	protected DrawingView createDrawingView(Drawing newDrawing) {
		Dimension d = getDrawingViewSize();
		DrawingView newDrawingView = new VWDrawingView(this, d.width,
				d.height);
		newDrawingView.setDrawing(newDrawing);
		// notify listeners about created view when the view is added to the desktop
		//fireViewCreatedEvent(newDrawingView);
		return newDrawingView;
	}
	
	protected Drawing createDrawing() {
		return new StandardDrawing();
	}

	public int getSelectedFigureId() {
		int figureId = -1;
		try {
			FigureEnumeration fEnum = view().selection();
			VWFigure selectedFigure = (VWFigure) fEnum.nextFigure();
			figureId = selectedFigure.getFigureId();
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
		return figureId;
	}
	
	public int getFigureId(String figureName) {
		int figureId = -2;
		try {
			FigureEnumeration fEnum = view().drawing().figures();
			while (fEnum.hasNextFigure()) {
				Object currenttFigure = fEnum.nextFigure();
				if (currenttFigure instanceof VWFigure) {
					VWFigure selectedFigure = (VWFigure) currenttFigure;
					if (selectedFigure.getFigureName().trim().equalsIgnoreCase(
							figureName)) {
						figureId = selectedFigure.getFigureId();
						break;
					}
				}
			}
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
		return figureId;
	}
	public String getFigureName(int figureId) {
		String figureName = "";
		try {
			FigureEnumeration fEnum = view().drawing().figures();
			while (fEnum.hasNextFigure()) {
				Object currenttFigure = fEnum.nextFigure();
				if (currenttFigure instanceof VWFigure) {
					VWFigure selectedFigure = (VWFigure) currenttFigure;
					if (selectedFigure.getFigureId() == figureId) {
						figureName = selectedFigure.getFigureName();
						break;
					}
				}
			}
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
		return figureName;
	}
	
	public Vector getAvailableFigureIds() {
		Vector vecFigureIds = new Vector();		
		int figureId = -2;
		try {
			FigureEnumeration fEnum = view().drawing().figures();
			while (fEnum.hasNextFigure()) {
				Object currenttFigure = fEnum.nextFigure();
				if (currenttFigure instanceof VWFigure) {
					VWFigure selectedFigure = (VWFigure) currenttFigure;
					if (selectedFigure.getFigureType().trim().equalsIgnoreCase("Task")) {
						figureId = selectedFigure.getFigureId();
						vecFigureIds.add(figureId);
						break;
					}
				}
			}
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
		return vecFigureIds;
	}
	
	// Need to check this methods return the figure name correctly 
	public String getSelectedFigureName() {
		String figureName = "";
		try {
			FigureEnumeration fEnum = view().selection();
			VWFigure selectedFigure = (VWFigure) fEnum.nextFigure();
			figureName = selectedFigure.getFigureName();
		} catch (Exception ex) {
			
		}
		return figureName;
	}

	public void setFigureZValue() {
		int id = 0;
		try {
			FigureEnumeration fEnum = view().drawing().figures();
			while (fEnum.hasNextFigure()) {
				Figure curFigure = fEnum.nextFigure();
				id++;
				curFigure.setZValue(id);
			}
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
	}
	/**
	 * EnhancementName:-Chinese localization
	 * Method added for decode the taskname from UTF-8 
	 * After getting the taskname from workflow diagram and 
	 * display it in the UI
	 */
	String decodeUTF8(byte[] bytes) {
	    return new String(bytes, UTF8_CHARSET);
	}
	public void setFigureDetails() {
		try {
			FigureEnumeration fEnum = view().drawing().figures();
			while (fEnum.hasNextFigure()) {
				VWFigure curFigure = (VWFigure) fEnum.nextFigure();
				String selected = "";				
				if (curFigure.getAttribute("Task") != null)
					selected = "Task";
				if (curFigure.getAttribute("Start") != null)
					selected = "Start";
				if (curFigure.getAttribute("End") != null)
					selected = "End";
				/*//String figureName = String.valueOf(curFigure.getAttribute(selected));
				String figureName = String.valueOf(curFigure.getAttribute(FigureAttributeConstant.getConstant("Task")));
				System.out.println("In setFiugreDetails() figreName is : "+figureName);
				*/
				/** 
				 * Enhancment:-Chinse Localization of WorkFlow Diagram
				 * Decoded the UTF encoding which is encoded in setAttribute method
				 * to get the Figure TaskName properly in chinese
				 * Created a new temporary JLabel(figureNamelabel) and and set the
				 * decoded figurename  and get it back in the required place to set the font
				 * through that label for setting the font for localization
				 * Modified By :Madhavan
				 */
				String figureName = String.valueOf(curFigure.getAttribute(selected));
				//String decodedTaskName=decodeUTF8(figureName.getBytes());
				JLabel figureNamelabel=new JLabel();
				String fontName="Arial";
				if (VWCPreferences.getFontName().trim().length()> 0)
					fontName = VWCPreferences.getFontName();
				figureNamelabel.setFont(new Font(fontName,Font.PLAIN,VWCPreferences.getFontSize()));
				figureNamelabel.setText(figureName);
				int figureId = 0;
				if (selected.equalsIgnoreCase("Task")){
					try{
						figureId = Integer.parseInt(curFigure.getAttribute(FigureAttributeConstant.getConstant("FigureId")).toString());
						//System.out.println("figure id in setFigureDetails " + figureId);
					}catch(Exception ex){
						//System.out.println("set figure details exception " + ex.getMessage());
					}
				}
				String strFigureName=figureNamelabel.getText();
				curFigure.setFigureName(strFigureName);
				// added the popup menu attribute to the existing figure
				
				if(selected.trim().equalsIgnoreCase("TASK"))
					curFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, createPopupMenu(true));
				else
					curFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, createPopupMenu(false));
				
				if (figureId != 0 )
				curFigure.setFigureId(figureId);
			}
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
	}	
}


class DRSDrawingAreaPanel extends JPanel implements DrawingEditor,
PaletteListener, VersionRequester {
	private DesktopListener fDesktopListener;	
	private Desktop fDesktop;	
	public static java.util.List listeners;	
	public static int RectId = 0;	
	private StorageFormatManager fStorageFormatManager;
	VWStartIconProperties vwStartIconProperties = null;
	VWTaskIconProperties  vwTaskIconProperties = null;
	VWEndIconProperties vwEndIconProperties = null;
	
	public DRSDrawingAreaPanel() {
	}
	
	public void showStatus(String status) {
		
	}
	
	protected void setDesktop(Desktop newDesktop) {
		newDesktop.addDesktopListener(getDesktopListener());
		fDesktop = newDesktop;
	}
	
	protected DesktopListener getDesktopListener() {
		return fDesktopListener;
	}
	
	/**
	 * Get the component, in which the content is embedded. This component
	 * acts as a desktop for the content.
	 */
	public Desktop getDesktop() {
		return fDesktop;
	}
	
	protected Desktop createDesktop() {
		return new JPanelDesktop(new DrawApplication());
		//	return new JScrollPaneDesktop();
	}
	
	
	/**
	 * Override to define the dimensions of the drawing view.
	 */
	protected Dimension getDrawingViewSize() {
		return new Dimension(800, 800);
	}
	
	/**
	 * Opens a new window with a drawing view.
	 */
	
	DRSDrawing app = null;
	
	public VWDrawingView getView() {
		return app.view();
	}
	
	public JPanel getDRSDrawing() {
		app = new DRSDrawing();
		app.init();
		
		return app;
	}
	
	
	protected void checkCommandMenus() {
		JMenuBar mb = new JMenuBar();
		
		for (int x = 0; x < mb.getMenuCount(); x++) {
			JMenu jm = mb.getMenu(x);
			if (CommandMenu.class.isInstance(jm)) {
				checkCommandMenu((CommandMenu) jm);
			}
		}
	}
	
	protected void setDesktopListener(DesktopListener desktopPaneListener) {
		fDesktopListener = desktopPaneListener;
	}
	
	protected void checkCommandMenu(CommandMenu cm) {
		cm.checkEnabled();
		for (int y = 0; y < cm.getItemCount(); y++) {
			JMenuItem jmi = cm.getItem(y);
			if (CommandMenu.class.isInstance(jmi)) {
				checkCommandMenu((CommandMenu) jmi);
			}
		}
	}
	
	protected void fireViewCreatedEvent(DrawingView view) {
		ListIterator li = listeners.listIterator(listeners.size());
		while (li.hasPrevious()) {
			ViewChangeListener vsl = (ViewChangeListener) li.previous();
			vsl.viewCreated(view);
		}
	}
	
	protected void fireViewDestroyingEvent(DrawingView view) {
		ListIterator li = listeners.listIterator(listeners.size());
		while (li.hasPrevious()) {
			ViewChangeListener vsl = (ViewChangeListener) li.previous();
			vsl.viewDestroying(view);
		}
	}
	
	protected Dimension defaultSize() {
		return new Dimension(600, 450);
	}
	
	protected void createTools(JToolBar palette) {
		//setDefaultTool(createDefaultTool());
		palette.add(fDefaultToolButton);
	}

	protected void setView(VWDrawingView newView) {
		DrawingView oldView = fView;
		fView = newView;		
		fireViewSelectionChangedEvent(oldView, view());
	}
	
	protected void fireViewSelectionChangedEvent(DrawingView oldView,
			DrawingView newView) {
		ListIterator li = listeners.listIterator(listeners.size());
		while (li.hasPrevious()) {
			ViewChangeListener vsl = (ViewChangeListener) li.previous();
			vsl.viewSelectionChanged(oldView, newView);
		}
	}
	
	VWLinkedButton btnStart = new VWLinkedButton(0);
	VWLinkedButton btnTask = new VWLinkedButton(0);
	VWLinkedButton btnSuspend = new VWLinkedButton(0);	
	VWLinkedButton btnEnd = new VWLinkedButton(0);
	VWLinkedButton btnProperties = new VWLinkedButton(0);
	JButton btnSave = new JButton();	
	JButton btnOpen = new JButton();
	
	public int btnClicked = -1;	
	public int taskId = 0;
	
	public JPanel createToolsPalette() {
		JPanel pnlTools = new JPanel();
		//pnlTools.setLayout(new GridLayout(1, 4));
		pnlTools.setLayout(null);
		//pnlTools.setBounds(1,1, 400, 100);
		
		pnlTools.setBackground(Color.WHITE);
		//pnlTools.add(btnStart);
		btnStart.setBounds(5 ,1, 80, 24);
		btnStart.setIcon(VWImages.RouteStartButtonIcon);
		btnStart.setToolTipText("Start");
		btnStart.setText("Start  ");
		btnStart.setMnemonic('S');
		
		pnlTools.add(btnTask);
		btnTask.setBounds(90, 4, 90, 24);/*btnTask.setBounds(90, 1, 80, 24);*/
		btnTask.setIcon(VWImages.RouteTaskButtonIcon);
		btnTask.setToolTipText(AdminWise.connectorManager.getString("WorkFlow.TaskToolTip"));
		btnTask.setText(AdminWise.connectorManager.getString("WorkFlow.AddTask"));
		btnTask.setMnemonic('T');
		
		//Comment the suspend button - later implement the functionality  
		//pnlTools.add(btnSuspend);
		btnSuspend.setBounds(175, 1, 80, 24);
		btnSuspend.setIcon(VWImages.RouteSuspendButtonIcon);
		btnSuspend.setToolTipText("Suspend");
		btnSuspend.setText("Suspend");
		//btnSuspend.setMnemonic('p');
		
		//pnlTools.add(btnEnd);
		btnEnd.setBounds(175, 1, 80, 24);
		//btnEnd.setIcon(VWImages.RouteEndIcon);
		btnEnd.setIcon(VWImages.RouteEndButtonIcon);
		btnEnd.setToolTipText("End    ");
		btnEnd.setText("End");
		btnEnd.setMnemonic('E');
		
		pnlTools.add(btnProperties);
		btnProperties.setBounds(185, 4, 100, 24);/*btnProperties.setBounds(315, 1, 90, 24);*/
		btnProperties.setIcon(VWImages.StatisticIcon);
		btnProperties.setToolTipText(AdminWise.connectorManager.getString("WorkFlow.PropertiesTooTip"));
		btnProperties.setText(AdminWise.connectorManager.getString("WorkFlow.Properties"));
		btnProperties.setMnemonic('P');
		
		btnStart.addActionListener(new SymAction());
		btnTask.addActionListener(new SymAction());
		btnSuspend.addActionListener(new SymAction());
		btnEnd.addActionListener(new SymAction());
		btnSave.addActionListener(new SymAction());
		btnOpen.addActionListener(new SymAction());
		btnProperties.addActionListener(new SymAction());
		
		return pnlTools;
	}

	public void openDialog(String figureType, String figureName){
		if(figureType.trim().equalsIgnoreCase("START")){
			if(vwStartIconProperties==null) {
				 vwStartIconProperties = new VWStartIconProperties(new Frame(),1000);
				 vwStartIconProperties.setCurrentLocation();
		     }else{
		    	 	vwStartIconProperties.setCurrentLocation();
		    	 	vwStartIconProperties.loadDocTypes();
		    	 	vwStartIconProperties.loadStartRouteInfo();		        	
		        	vwStartIconProperties.setVisible(true);
		     }
		}else if(figureType.trim().equalsIgnoreCase("TASK")){
			
			if(vwTaskIconProperties == null){
				vwTaskIconProperties = new VWTaskIconProperties(AdminWise.adminFrame, figureName);				
				vwTaskIconProperties.setCurrentLocation();
			}
			else{
				// Need to add load settings here. Valli
				vwTaskIconProperties.setCurrentLocation();
				//Task escalation enhancement
				vwTaskIconProperties.initTaskInterval();
				vwTaskIconProperties.loadTaskRouteInfo(figureName);
				vwTaskIconProperties.setVisible(true);
			}
		}else if(figureType.trim().equalsIgnoreCase("END")){
			if(vwEndIconProperties==null){
				vwEndIconProperties = new VWEndIconProperties();
				vwEndIconProperties.setCurrentLocation();
				//vwEndIconProperties.loadEndRouteInfo();
			}
			else{
				vwEndIconProperties.setCurrentLocation();
				vwEndIconProperties.loadIndicesList();
				vwEndIconProperties.loadEndRouteInfo();
				vwEndIconProperties.setVisible(true);
			}
		}
	}
	public void viewMode(){
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2){
			btnEnd.setEnabled(false);
			btnStart.setEnabled(false);
			btnTask.setEnabled(false);
			btnProperties.setEnabled(true);
		}else if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 1 || AdminWise.adminPanel.routePanel.CURRENT_MODE == 3
				|| AdminWise.adminPanel.routePanel.CURRENT_MODE == 4){
			btnEnd.setEnabled(true);
			btnStart.setEnabled(true);
			btnTask.setEnabled(true);
			btnProperties.setEnabled(true);
		}
	}
	
	class SymAction implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (ae.getSource() == btnStart) {
				btnClicked = 1;
				if (checkFigureExist("Start")){
					JOptionPane.showMessageDialog(AdminWise.adminPanel.routePanel ,AdminWise.connectorManager.getString("DRSDrawing.msg_0"));					
					return;
				}
				btnTask.setEnabled(true);
				VWStartFigure eFigure = new VWStartFigure();
				eFigure.setFigureName("Start_1");
				//Point eStart = new Point(100, 100);
				//Point eEnd = new Point(200, 400);
				Point eStart = new Point(50, 50);
				Point eEnd = new Point(150, 350);
				eFigure.displayBox(eStart, eEnd);
				FigureAttributeConstant figConst = new FigureAttributeConstant(
				"Start");
				eFigure.setAttribute(figConst, "Start_1");
				eFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, createPopupMenu(false));
				view().add(eFigure);
			} else if (ae.getSource() == btnTask) {
				btnClicked = 2;
				btnEnd.setEnabled(true);
				VWTaskFigure taskFigure = new VWTaskFigure();				
				int taskId = getLargestFigureId() + 1;
				if(taskId > 22){
					return;
				}
				if(taskCount == 15){
					JOptionPane.showMessageDialog(AdminWise.adminPanel.routePanel ,AdminWise.connectorManager.getString("DRSDrawing.msg_1")+" "+(taskCount)+".");
					return;
				}
				taskCount++;
				System.out.println("LargestFigureId ------------------------- " + taskId);
				boolean flag = isTaskNameExist("Task_" + taskId, -1);
				String Task=AdminWise.connectorManager.getString("DRSDrawing.Task");
				if (flag)					
					taskFigure.setFigureName(Task+"_"+ taskId + "_" +taskId);
				else
					taskFigure.setFigureName(Task+"_" + taskId);				
				taskFigure.setFigureId(taskId);
				int taskDistStartX = 0;
				int taskDistEndX = 0;
				int taskDistStartY = 0;
				int taskDistEndY = 0;
				
				taskDistStartX = 150;
				taskDistStartY = 50;
				taskDistEndX = 250;
				taskDistEndY = 350;
				if(taskId<=7){
					taskDistStartX = (taskId*100)+50;
					taskDistEndX = (taskId*100)+150;
					taskDistStartY = 50;
					taskDistEndY = 350;
				}else if(taskId == 8){
					taskDistStartX =(taskId*100)+50; //850;
					taskDistEndX =(taskId*100)-50; //750;
					taskDistStartY = 50+100; // 150;
					taskDistEndY = 350+400; // 750;
				}else if(taskId > 8 && taskId <= 15){
					taskDistStartX = (taskId*100)-(100*(taskId-8))-50;
					taskDistEndX = (taskId*100)-(200*(taskId-8))-50;
					taskDistStartY = 50+100;
					taskDistEndY = 350+400;
				}else if(taskId > 15 && taskId <= 22){
					taskDistStartX = ((taskId-15)*100)+50;
					taskDistEndX = ((taskId-15)*100)-50;
					taskDistStartY = 50+100+100;
					taskDistEndY = 350+400+400;
				}
				//System.out.println(" StartX, StartY "+taskDistStartX+","+taskDistStartY+" : EndX, EndY "+taskDistEndX+","+taskDistEndY);
				Point eStart = new Point(taskDistStartX, taskDistStartY);
				Point eEnd = new Point(taskDistEndX,taskDistEndY);
				taskFigure.displayBox(eStart, eEnd);
				taskFigure.addFigureChangeListener(new FCL());

				/**
				 * Desc   :Below code is used to move End icon along with Task Icon, when ever a new task is added.
				 * Author :Nishad Nambiar
				 * Date   :26-May-2008
				*/				
				Point endEnd = null; 
				Point endStart = null;
				System.out.println("taskId: " + taskId);
				//2 pixels increased or decreased because of center point of task and end,start is different.
				//task icon size is bigger than start and end icons
				if(taskId <7){
					endStart = new Point(taskDistStartX+80, taskDistStartY+2);
					endEnd = new Point(taskDistEndX+80, taskDistEndY+80);
				}else if(taskId ==7){
					endStart = new Point(taskDistStartX+80, taskDistStartY+100);
					endEnd = new Point(taskDistEndX-92, taskDistEndY+80);
				}
				else if(taskId > 7 && taskId < 15){
					endStart = new Point(taskDistStartX-80, taskDistStartY+2);
					endEnd = new Point(taskDistEndX-80, taskDistEndY+80);
				}
				else if(taskId ==15){
					endStart = new Point(taskDistStartX+10, taskDistStartY+100+2);
					endEnd = new Point(taskDistEndX+10-2, taskDistEndY);
				}
				else if(taskId >15 && taskId <= 22){
					endStart = new Point(taskDistStartX+80, taskDistStartY+2);
					endEnd = new Point(taskDistEndX+80, taskDistEndY+80);
				}
				VWRoutePanel.rFigure.displayBox(endStart, endEnd);
				//End...
				
				FigureAttributeConstant figureIdConst = new FigureAttributeConstant(
				"FigureId");
					taskFigure.setAttribute(figureIdConst, taskId);
					taskFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, createPopupMenu(true));
					/*
					 * Task name in the diagram is added twice; 
					 * so commenting the code for setting the task name into the figure attribute;
					 * Order corrected to FigureId, Popup menu and task name in the drawing.
					 */
				/*FigureAttributeConstant figConst = new FigureAttributeConstant(
				"Task");
					taskFigure.setAttribute(figConst, "Task_" + taskId);*/
				//taskFigure.size();
					view().add(taskFigure);
				//VWStartIconProperties.listTasksModel.removeAllElements();
				/*FigureEnumeration fEnum = view().drawing().figures();
				while(fEnum.hasNextFigure()){
					try{
						Object curFigure =  fEnum.nextFigure();
						if (curFigure instanceof VWFigure){
							String curselFigureType= ((VWFigure)curFigure).getFigureType();
							if(curselFigureType.equalsIgnoreCase("Task")){
								VWStartIconProperties.listTasksModel.addElement(((VWFigure)curFigure).getFigureName());
							}
						}
					}catch(Exception ex){
						System.out.println("Load Available task ::: " + ex.getMessage());
					}
				}*/
				VWStartIconProperties.listTasksModel.addElement(((VWFigure)taskFigure).getFigureName());
				RouteTaskInfo thisRouteTaskInfo = new RouteTaskInfo();
				thisRouteTaskInfo.setTaskFigureId(taskFigure.getFigureId());
				thisRouteTaskInfo.setTaskName(taskFigure.getFigureName().trim());
				AdminWise.adminPanel.routePanel.alRouteTaskInfo.add(thisRouteTaskInfo);
				//set task seq id in alRouteTaskSequence
				AdminWise.adminPanel.routePanel.alRouteTaskSequence = new ArrayList(); 
				for(int i=0;VWStartIconProperties.listTasksModel!=null && i<VWStartIconProperties.listTasksModel.size();i++){
					try{
						String currentTaskName = String.valueOf(VWStartIconProperties.listTasksModel.get(i));
						int CurTaskId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(currentTaskName);
						AdminWise.adminPanel.routePanel.alRouteTaskSequence.add(CurTaskId);
					}catch(Exception ex){
						//JOptionPane.showMessageDialog(null,"Saving Ex 1"+ex.toString());
					}
				}
				System.out.println(" alRouteTaskInfo size :"+AdminWise.adminPanel.routePanel.alRouteTaskInfo.size());
				VWStartIconProperties.drawConnections();
			} else if (ae.getSource() == btnSuspend) {
				btnClicked = 3;
				VWSuspendFigure sFigure = new VWSuspendFigure();
				sFigure.setFigureName("Suspend");
				Point eStart = new Point(100,100);
				Point eEnd = new Point(200,400);
				sFigure.displayBox(eStart,eEnd);
				view().add(sFigure);				
				executeAlignment();
			} else if (ae.getSource() == btnEnd) {
				btnClicked = 4;
				if (checkFigureExist("End")){
					JOptionPane.showMessageDialog(AdminWise.adminPanel.routePanel ,AdminWise.connectorManager.getString("DRSDrawing.msg_2"));					
					return;
				}
				VWEndFigure rFigure = new VWEndFigure();
				rFigure.setFigureName("End_1");	
				Point eStart = new Point(50, 200+50);
				Point eEnd = new Point(100, 350+400+400);
				rFigure.displayBox(eStart, eEnd);
				FigureAttributeConstant figConst = new FigureAttributeConstant(
				"End");
				rFigure.setAttribute(figConst, "End_1");				
				rFigure.setAttribute(FigureAttributeConstant.POPUP_MENU, createPopupMenu(false));
				view().add(rFigure);	
			} else if (ae.getSource() == btnProperties) {
				btnClicked = 5;
				FigureEnumeration figEnum = view().selection();
				if(figEnum.hasNextFigure()){
					VWFigure selectedFigure = (VWFigure)figEnum.nextFigure();
					String figType = selectedFigure.getFigureType();
					String figName = selectedFigure.getFigureName();
					openDialog(figType, figName);
				}
				/*try{			
									drsDrawingBounds = AdminWise.adminPanel.routePanel.drsDrawing.getBounds(); 
									AdminWise.adminPanel.routePanel.newFrame.add((JPanel) AdminWise.adminPanel.routePanel.drsDrawing);
									AdminWise.adminPanel.routePanel.newFrame.setBounds(0, 0, 1020, 735);
									AdminWise.adminPanel.routePanel.newFrame.setVisible(true);
									//SymWindow aSymWindow = new SymWindow();
									//AdminWise.adminPanel.routePanel.newFrame.addWindowListener(aSymWindow);
								}catch(Exception e){
									JOptionPane.showMessageDialog(null,e.toString());
					}
				*/		
				}
		}
	}
	
	class FCL implements FigureChangeListener{
    	public void figureInvalidated(FigureChangeEvent e){
    		//System.out.println("figureInvalidated");
    	}

    	public void figureChanged(FigureChangeEvent e){
    		///System.out.println("figureChanged");
    	}
    	public void figureRemoved(FigureChangeEvent e){
    		//System.out.println("figureRemoved"); 
    	}

    	public void figureRequestRemove(FigureChangeEvent e){
    		/*VWFigure vwFigure = (VWFigure)e.getFigure();
    		//System.out.println("figureRequestRemove");
    		if(vwFigure.getFigureType().trim().equalsIgnoreCase("TASK")){
	    		String figName = vwFigure.getFigureName();
				AdminWise.adminPanel.routePanel.drsDrawing.taskCount--;
				for(int i = 0; VWStartIconProperties.listTasksModel!=null && i<VWStartIconProperties.listTasksModel.getSize(); i++){
					String taskFigName = VWStartIconProperties.listTasksModel.get(i).toString();
					if(figName.trim().equalsIgnoreCase(taskFigName.trim()))
						VWStartIconProperties.listTasksModel.removeElementAt(i);
				}
    		}*/
    	}

    	public void figureRequestUpdate(FigureChangeEvent e){
    		//System.out.println("figureRequestUpdate");
    	}
    }
	
	public JPopupMenu createPopupMenu(boolean display) {
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopupMenuItems(popupMenu, display);
		popupMenu.setLightWeightPopupEnabled(true);
		return popupMenu;
	}
	/**
	 * Adds items to the popup menu
	 *
	 * @param popupMenu  The popup menu to add items to
	 */
	private void addPopupMenuItems(JPopupMenu popupMenu, boolean display) {
		JMenuItem menuItem1 = new JMenuItem(new AbstractAction("Properties") {
			public void actionPerformed(ActionEvent event) {
				FigureEnumeration selFigureEnum = view().selection();
				VWFigure selectedFigure = (VWFigure)selFigureEnum.nextFigure();
				String figureType = selectedFigure.getFigureType();
				if(figureType.trim().equalsIgnoreCase("START"))
				{
					openDialog(figureType, null);
				}
				if(figureType.trim().equalsIgnoreCase("TASK")){
					String figureName = "Task";
					int figureId = 0;
					FigureEnumeration figureEnum = AdminWise.adminPanel.routePanel.drsDrawing.view().selection();
					if (figureEnum.hasNextFigure()){
						VWFigure figure = (VWFigure) figureEnum.nextFigure();
						try{
							if (figure.getAttribute(FigureAttributeConstant.getConstant("Task")) == null)
									figureName = figure.getFigureName();
							else
									figureName = String.valueOf(figure.getAttribute(FigureAttributeConstant.getConstant("Task")));
							
							if (figure.getAttribute(FigureAttributeConstant.getConstant("FigureId")) == null)
								figureId = figure.getFigureId();
							else
								figureId = Integer.parseInt(figure.getAttribute(FigureAttributeConstant.getConstant("FigureId")).toString());
							figure.setFigureId(figureId);
						}catch(Exception ex){
							
						}
						
					}
					openDialog(figureType, figureName);
				}
				if(figureType.trim().equalsIgnoreCase("END")){
					openDialog(figureType, null);
				}
			}
		});

		JMenuItem menuItem2 = new JMenuItem("Delete");
		popupMenu.add(menuItem1);
		popupMenu.add(menuItem2);
		menuItem2.setEnabled(display);		
		if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2 || AdminWise.adminPanel.routePanel.CURRENT_MODE == 0)
			menuItem2.setEnabled(false);
		
		/**
		 * For Invalid route functionality
		 */
		boolean flag = true;
		for(int count=0;VWStartIconProperties.listTasksModel!=null&&count<VWStartIconProperties.listTasksModel.size();count++){
			try{
				String currentTaskName = String.valueOf(VWStartIconProperties.listTasksModel.get(count));
				int taskId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(currentTaskName);
				if(taskId == 0){
					flag = false;
				}
			}catch(Exception ex){
				//JOptionPane.showMessageDialog(null,"Saving Ex 1"+ex.toString());
			}
		}
		
		if(AdminWise.adminPanel.routePanel.routeValidity == 1){
			if(!flag){
				//For the existing task in the invalid route need to disable the delete button
				menuItem2.setEnabled(false);
			}else{
				//For newly added task in the invalid route need to enable the delete button.
				menuItem2.setEnabled(true);
			}
		}
		else
			menuItem2.setEnabled(true);
		//--
		menuItem2.addActionListener(new AbstractAction() {
			public void actionPerformed(ActionEvent event) {
				// update route mode - 3 , view mode - 2, new route mode - 1
				if(AdminWise.adminPanel.routePanel.CURRENT_MODE == 2)
					return;
				FigureEnumeration selFigureEnum = view().selection();
				boolean isNotTask = false;
				while(selFigureEnum.hasNextFigure()){
					Figure selectedFigure = (Figure)selFigureEnum.nextFigure();
					if (selectedFigure instanceof VWFigure) {
						VWFigure selectedVWFigure = (VWFigure)selectedFigure;
						String figureType = selectedVWFigure.getFigureType();
						if(!figureType.trim().equalsIgnoreCase("TASK")){
							isNotTask = true;
							JOptionPane.showMessageDialog(AdminWise.adminPanel.routePanel ,AdminWise.connectorManager.getString("DRSDrawing.msg_3"));
							break;
						}
					}
				}
				if(!isNotTask){
					int retVal = VWMessage.showConfirmDialog(AdminWise.adminFrame, AdminWise.connectorManager.getString("DRSDrawing.msg_4"));
					if (retVal == 0) {
						selFigureEnum = view().selection();
						while(selFigureEnum.hasNextFigure()){
							Figure selectedFigure = (Figure)selFigureEnum.nextFigure();
							if (selectedFigure instanceof VWFigure) {
								VWFigure selectedVWFigure = (VWFigure)selectedFigure;
								String figName = selectedVWFigure.getFigureName();
								AdminWise.adminPanel.routePanel.drsDrawing.taskCount--;
								for(int i = 0; VWStartIconProperties.listTasksModel!=null && i<VWStartIconProperties.listTasksModel.getSize(); i++){
									String taskFigName = VWStartIconProperties.listTasksModel.get(i).toString();
									if(figName.trim().equalsIgnoreCase(taskFigName.trim()))
										VWStartIconProperties.listTasksModel.remove(i);
								}
							}
						}
						DeleteCommand deleteCommand = new DeleteCommand("Delete", AdminWise.adminPanel.routePanel.drsDrawing);
						deleteCommand.execute();
						VWStartIconProperties.drawConnections();
						AdminWise.adminPanel.routePanel.removeUnUsedTasksIfAny();
//						Set Route Task SequenceID
						AdminWise.adminPanel.routePanel.alRouteTaskSequence = new ArrayList(); 
						for(int count=0;VWStartIconProperties.listTasksModel!=null&&count<VWStartIconProperties.listTasksModel.size();count++){
							try{
								String currentTaskName = String.valueOf(VWStartIconProperties.listTasksModel.get(count));
								int taskId = AdminWise.adminPanel.routePanel.drsDrawing.getFigureId(currentTaskName);
								AdminWise.adminPanel.routePanel.alRouteTaskSequence.add(taskId);
							}catch(Exception ex){
								//JOptionPane.showMessageDialog(null,"Saving Ex 1"+ex.toString());
							}
						}
						//System.out.println("after delete alRouteTaskSequence:"+AdminWise.adminPanel.routePanel.alRouteTaskSequence.toString());
					}
				}
			}
		});
	}
	
	public boolean checkFigureExist(String figureName) {
		boolean exist = false;
		try {
			FigureEnumeration figEnums = view().drawing().figures();
			while (figEnums != null && figEnums.hasNextFigure()) {
				Object currentObject = figEnums.nextFigure();
				if (currentObject instanceof VWFigure) {
					VWFigure figure = (VWFigure) currentObject;
					if (figure.getFigureType().equalsIgnoreCase(figureName))
						return true;
				}
			}
		} catch (Exception ex) {
		}
		return exist;
	}
	public void executeAlignment() {
		try {
			UndoableCommand alignLeftCmd = new UndoableCommand(
					new AlignCommand(AlignCommand.Alignment.MIDDLES, this));
			alignLeftCmd.execute();
		} catch (Exception ex) {
			//ex.printStackTrace();
		}
	}
	/**
	 * EnhancementName:-Chinese localization
	 * Method added for encoding the taskname to UTF-8 
	 * Before settint to the workflow diagram
	 */

	private final Charset UTF8_CHARSET = Charset.forName("UTF-8");
	byte[] encodeUTF8(String string) {
		return string.getBytes(UTF8_CHARSET);
	}
	public void setTaskName(String taskName) {
		FigureEnumeration figEnums = view().selection();
		String oldTaskName = "";
		if (figEnums.hasNextFigure()) {
			VWFigure figure = (VWFigure) figEnums.nextFigure();
			oldTaskName = figure.getFigureName();
			figure.setFigureName(taskName);
			FigureAttributeConstant figConst = FigureAttributeConstant.getConstant("Task");
			/**
			 * Enhancement:-Chinese Localization
			 * For workFlow Task name in chinese we encoded the taskname before setting the 
			 * diagram using setattribute method 
			 * A dummy label is added adn font is set from the Registry
			 * 
			 */
	/*		byte[] byteConverted=encodeUTF8(taskName);
			String encodedTaskName = new String(byteConverted);
			JLabel taskNameLabel=new JLabel();
			taskNameLabel.setText(encodedTaskName);
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
				fontName = VWCPreferences.getFontName();
			taskNameLabel.setFont(new Font(fontName,Font.PLAIN,12));*/
			figure.setAttribute(figConst,taskName);
		}
		view().repaint();
		for(int i = 0; i<VWStartIconProperties.listTasksModel.getSize(); i++){
			String listTaskName = VWStartIconProperties.listTasksModel.get(i).toString();
			if(oldTaskName.trim().equalsIgnoreCase(listTaskName.trim())){
				if(VWStartIconProperties.vecSequence!=null && VWStartIconProperties.vecSequence.size()>0){
					VWStartIconProperties.vecSequence.removeElementAt(i);
					VWStartIconProperties.vecSequence.insertElementAt(""+taskName,i);
				}
				VWStartIconProperties.listTasksModel.removeElementAt(i);
				VWStartIconProperties.listTasksModel.insertElementAt(""+taskName, i);
			}
		}
		System.out.println(" after setTaskName listTasksModel:"+VWStartIconProperties.listTasksModel);
	}
	
	public int getTaskCount() {
		int taskId = 0;
		try {
			FigureEnumeration figEnums = view().drawing().figures();
			while (figEnums.hasNextFigure()) {
				VWFigure figure = null;
				try {
					Object curFigure = figEnums.nextFigure();
					if (curFigure instanceof VWFigure) {
						figure = (VWFigure) curFigure;
						if (figure.getFigureType().equals("TASK")) {
							taskId++;
							//figure.setFigureId(taskId);
						}
					}
				} catch (Exception ex) {
				}
			}
		} catch (Exception ex) {
			//ex.printStackTrace();
		}
		return ++taskId;
	}
	
	public int getLargestFigureId() {
		int resFigureId = 0;
		try {
			FigureEnumeration fEnum = view().drawing().figures();
			while (fEnum.hasNextFigure()) {
				Object currenttFigure = fEnum.nextFigure();
				if (currenttFigure instanceof VWFigure) {
					VWFigure selectedFigure = (VWFigure) currenttFigure;
					if (selectedFigure.getFigureType().trim().equalsIgnoreCase(
					"Task")) {
						int figureId = selectedFigure.getFigureId();
						if (figureId > resFigureId) {
							resFigureId = figureId;
						}
					}
				}
			}
		} catch (Exception ex) {
			//JOptionPane.showMessageDialog(null,ex.toString());
		}
		return resFigureId;
	}
	
	public boolean isTaskNameExist(String taskName, int figureId) {
		boolean isExist = false;
		try {
			FigureEnumeration figEnums = view().drawing().figures();
			while (figEnums.hasNextFigure()) {
				VWFigure figure = null;
				try {
					Object curFigure = figEnums.nextFigure();
					if (curFigure instanceof VWFigure) {
						figure = (VWFigure) curFigure;
						if (figure.getFigureType().equals("TASK")
								&& figure.getFigureName().equalsIgnoreCase(
										taskName)
										&& figure.getFigureId() != figureId) {
							isExist = true;
							break;
						}
					}
				} catch (Exception ex) {
				}
			}
		} catch (Exception ex) {
			//ex.printStackTrace();
		}
		return isExist;
	}
	
	public String checkDrawing(){
		boolean isConnect = false;
		boolean isStartExist = false, isEndExist = false;
		String msg = "";
		FigureEnumeration figEnums =  view().drawing().figures();
		int nodeCount = 0, lineCount = 0;
		while (figEnums != null && figEnums.hasNextFigure()) {
			Object figure = figEnums.nextFigure();
			
			if (figure instanceof VWFigure){
				nodeCount++;
				VWFigure currentFigure = (VWFigure) figure;
				if (currentFigure.getFigureType().equalsIgnoreCase("Start")){
					isStartExist = true;
				}
				if (currentFigure.getFigureType().equalsIgnoreCase("End")){
					isEndExist = true;
				}

			}
			if (figure instanceof VWLineConnection){
				lineCount++;	
			}
		}
		if ((nodeCount -1) != lineCount)
			msg = AdminWise.connectorManager.getString("DRSDrawing.msg_5");
		if (lineCount == 0)
			msg = AdminWise.connectorManager.getString("DRSDrawing.msg_6");
		if (!isEndExist)
			msg = AdminWise.connectorManager.getString("DRSDrawing.msg_7");
		if (!isStartExist)
			msg = AdminWise.connectorManager.getString("DRSDrawing.msg_8");


		return msg;
	}
	public void clearAllFigures() {
		try {
			taskCount = 0;
			FigureEnumeration figEnums = view().drawing().figures();
			view().clearSelection();
			view().drawing().removeAll(figEnums);
		} catch (Exception ex) {
			 System.out.println("Error in clearing all figures " +ex.getMessage());
		}
	}
	
	
	public String promptSaveAs(String filePath) {
		String fileContents = "";
		if (view() != null) {
			try {
				toolDone();
				JFileChooser saveDialog = createSaveFileChooser();
				setStorageFormatManager(createStorageFormatManager());
				saveDialog
				.setFileFilter(new javax.swing.filechooser.FileFilter() {
					public String getDescription() {
						return "Drawing files";
					}
					
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						if (f.getName().endsWith(".draw"))
							return true;
						if (f.getName().endsWith(".ser"))
							return true;
						return false;
					}
				});
				StorageFormat foundFormat = getStorageFormatManager()
				.findStorageFormat(saveDialog.getFileFilter());
				if (foundFormat == null) {
					File file = new File(filePath);
					foundFormat = getStorageFormatManager().findStorageFormat(
							file);
				}
				if (foundFormat != null) {
					saveDrawing(foundFormat, filePath);
					try {
						BufferedReader bufferedReader = new BufferedReader(
								new FileReader(new File(filePath)));
						String line = null;
						while ((line = bufferedReader.readLine()) != null) {
							//Process the data, here we just print it out
							fileContents += line;
						}
					} catch (Exception ex) {
						
					}
				}
			} catch (Exception e) {
				
			}
		}
		return fileContents;
	}
	
	protected JFileChooser createSaveFileChooser() {
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogType(1);
		saveDialog.setDialogTitle("Save File...");
		return saveDialog;
	}
	
	public StorageFormatManager createStorageFormatManager() {
		StorageFormatManager storageFormatManager = new StorageFormatManager();
		storageFormatManager
		.setDefaultStorageFormat(new StandardStorageFormat());
		storageFormatManager.addStorageFormat(storageFormatManager
				.getDefaultStorageFormat());
		storageFormatManager.addStorageFormat(new SerializationStorageFormat());
		//	storageFormatManager.addStorageFormat(new JDOStorageFormat());
		return storageFormatManager;
	}
	
	protected final void setStorageFormatManager(
			StorageFormatManager newStorageFormatManager) {
		fStorageFormatManager = newStorageFormatManager;
	}
	
	public StorageFormatManager getStorageFormatManager() {
		return fStorageFormatManager;
	}
	
	protected void saveDrawing(StorageFormat storeFormat, String file) {
		if (view() == null)
			return;
		try {
			String name = storeFormat.store(file, view().drawing());
			view().drawing().setTitle(name);
			setDrawingTitle(name);
			//setDrawingTitle("Test.draw");
		} catch (IOException e) {
			showStatus(e.toString());
		}
	}
	
	protected void setDrawingTitle(String drawingTitle) {
		/* 
		 if(getDefaultDrawingTitle().equals(drawingTitle))
		 setTitle(getApplicationName());
		 else
		 setTitle(getApplicationName() + " - " + drawingTitle);
		 */
	}
	
	public DRSDrawingAreaPanel drsDrawingAreaPanel;
	
	
	public void promptOpen(String filePath, String fileContents) {
		toolDone();
		JFileChooser openDialog = createOpenFileChooser();
		openDialog.setFileFilter(new javax.swing.filechooser.FileFilter() {
			public String getDescription() {
				return "Image files";
			}
			
			public boolean accept(File f) {
				if (f.isDirectory())
					return true;
				if (f.getName().endsWith(".draw"))
					return true;
				if (f.getName().endsWith(".ser"))
					return true;
				return false;
			}
		});
		setStorageFormatManager(createStorageFormatManager());
		getStorageFormatManager().registerFileFilters(openDialog);
		StorageFormat foundFormat = getStorageFormatManager()
		.findStorageFormat(openDialog.getFileFilter());
		// try to find one that supports the file
		if (foundFormat == null) {
			foundFormat = getStorageFormatManager().findStorageFormat(
					openDialog.getSelectedFile());
		}
		if (foundFormat != null) {
			/*try {
				File file = new File(filePath);
				FileOutputStream fout = new FileOutputStream(file);
				fout.write(fileContents.getBytes());
			} catch (Exception e) {
			}*/
			loadDrawing(foundFormat, filePath);
		} else {
			showStatus("Not a valid file format: "
					+ openDialog.getFileFilter().getDescription());
		}
	}
	
	/**
	 * Load a Drawing from a file
	 */
	protected void loadDrawing(StorageFormat restoreFormat, String file) {
		try {
			Drawing restoredDrawing = restoreFormat.restore(file);
			if (restoredDrawing != null) {
				restoredDrawing.setTitle(file);
				//newWindow(restoredDrawing);
				fDrawing.release();
				fDrawing = restoredDrawing;
				view().setDrawing(restoredDrawing);
			} else {
				showStatus("Unknown file type: could not open file '" + file
						+ "'");
			}
		} catch (IOException e) {
			showStatus("Error: " + e);
		}
	}
	
	/**
	 * Create a file chooser for the open file dialog. Subclasses may override this
	 * method in order to customize the open file dialog.
	 */
	protected JFileChooser createOpenFileChooser() {
		JFileChooser openDialog = new JFileChooser();
		openDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		openDialog.setDialogTitle("Open File...");
		return openDialog;
	}
	
	public JPanel getDRSDrawingAreaPanel() {	
		DRSDrawingAreaPanel app = new DRSDrawingAreaPanel();		
		app.init();		
		return app;
	}
	
	public void init() {
		createIconkit();
		getVersionControlStrategy().assertCompatibleVersion();
		setUndoManager(new org.jhotdraw.util.UndoManager());
		setLayout(null);
		fView = createDrawingView();		
		JPanel attributes = createAttributesPanel();
		createAttributeChoices(attributes);
		
		JPanel toolPanel = createToolPalette();
		createTools(toolPanel);
		
		JPanel toolPanel1= createToolsPalette();
		toolPanel1.setBounds(10, 10, 420, 26);
		add(toolPanel1);	
		VWDrawingView view = view();
		
		view.setBorder(new DotDashBorder(DotDashBorder.DOT,Color.LIGHT_GRAY.brighter()));		
		component = (Component) view;
		component.setBounds(1, 35, 847, 411);
		component.setBackground(Color.WHITE);		
		add(component);
		initDrawing();
		setupAttributes();
		//setBorder(new LineBorder(Color.LIGHT_GRAY));
		DotDashBorder border = new DotDashBorder(DotDashBorder.DOT,new Color(120, 165, 245));
		setBorder(border);
	}

	public void addViewChangeListener(ViewChangeListener viewchangelistener) {
	}
	
	public void removeViewChangeListener(ViewChangeListener viewchangelistener) {
	}
	
	protected Iconkit createIconkit() {
		return new Iconkit(this);
	}
	
	protected JPanel createAttributesPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new PaletteLayout(2, new Point(2, 2), false));
		return panel;
	}
	
	protected void createAttributeChoices(JPanel panel) {
		panel.add(new JLabel("Fill"));
		fFillColor = createColorChoice(FigureAttributeConstant.FILL_COLOR);
		panel.add(fFillColor);
		panel.add(new JLabel("Text"));
		fTextColor = createColorChoice(FigureAttributeConstant.TEXT_COLOR);
		panel.add(fTextColor);
		panel.add(new JLabel("Pen"));
		fFrameColor = createColorChoice(FigureAttributeConstant.FRAME_COLOR);
		panel.add(fFrameColor);
		panel.add(new JLabel("Arrow"));
		CommandChoice choice = new CommandChoice();
		fArrowChoice = choice;
		FigureAttributeConstant arrowMode = FigureAttributeConstant.ARROW_MODE;
		choice.addItem(new ChangeAttributeCommand("none", arrowMode,
				new Integer(0), this));
		choice.addItem(new ChangeAttributeCommand("at Start", arrowMode,
				new Integer(1), this));
		choice.addItem(new ChangeAttributeCommand("at End", arrowMode,
				new Integer(2), this));
		choice.addItem(new ChangeAttributeCommand("at Both", arrowMode,
				new Integer(3), this));
		panel.add(fArrowChoice);
		panel.add(new JLabel("Font"));
		fFontChoice = createFontChoice();
		panel.add(fFontChoice);
	}
	
	protected JComboBox createColorChoice(FigureAttributeConstant attribute) {
		CommandChoice choice = new CommandChoice();
		for (int i = 0; i < ColorMap.size(); i++)
			choice.addItem(new ChangeAttributeCommand(ColorMap.name(i),
					attribute, ColorMap.color(i), this));
		
		return choice;
	}
	
	protected JComboBox createFontChoice() {
		CommandChoice choice = new CommandChoice();
		String fonts[] = Toolkit.getDefaultToolkit().getFontList();
		for (int i = 0; i < fonts.length; i++)
			choice.addItem(new ChangeAttributeCommand(fonts[i],
					FigureAttributeConstant.FONT_NAME, fonts[i], this));
		
		return choice;
	}
	
	protected JPanel createButtonPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new PaletteLayout(2, new Point(2, 2), false));
		return panel;
	}
	

	protected JPanel createToolPalette() {
		JPanel palette = new JPanel();
		palette.setLayout(new PaletteLayout(2, new Point(2, 2)));
		return palette;
	}
	
	protected void createTools(JPanel palette) {
		Tool tool = createSelectionTool();
		fDefaultToolButton = createToolButton("/CH/ifa/draw/images/SEL",
				"Selection Tool", tool);
		palette.add(fDefaultToolButton);
	}
	
	protected Tool createSelectionTool() {
		return new VWSelectionTool(this);
	}
	
	protected ToolButton createToolButton(String iconName, String toolName,
			Tool tool) {
		return new ToolButton(this, iconName, toolName, tool);
	}
	
	protected Drawing createDrawing() {
		return new StandardDrawing();
	}
	
	protected VWDrawingView createDrawingView() {
		return new VWDrawingView(this, 410, 370);
	}
	
	public void paletteUserSelected(PaletteButton button) {
		ToolButton toolButton = (ToolButton) button;
		setTool(toolButton.tool(), toolButton.name());
		setSelected(toolButton);
	}
	
	public void paletteUserOver(PaletteButton button, boolean inside) {
		if (inside)
			showStatus(button.name());
		else if (fSelectedToolButton != null)
			showStatus(fSelectedToolButton.name());
	}
	
	public Drawing drawing() {
		return fDrawing;
	}
	
	public Tool tool() {
		return fTool;
	}
	
	public VWDrawingView view() {
		return fView;
	}
	
	public DrawingView[] views() {
		return (new DrawingView[] { view() });
	}
	
	public void toolDone() {
		
		setTool(fDefaultToolButton.tool(), fDefaultToolButton.name());
		setSelected(fDefaultToolButton);
	}
	
	public void figureSelectionChanged(DrawingView view) {
		setupAttributes();
	}
	
	public void viewSelectionChanged(DrawingView drawingview,
			DrawingView drawingview1) {
	}
	
	private void initDrawing() {
		fDrawing = createDrawing();
		view().setDrawing(fDrawing);
		toolDone();
	}
	
	private void setTool(Tool t, String name) {
		if (fTool != null)
			fTool.deactivate();
		fTool = t;
		if (fTool != null) {
			showStatus(name);
			fTool.activate();
		}
	}
	
	private void setSelected(ToolButton button) {
		if (fSelectedToolButton != null)
			fSelectedToolButton.reset();
		fSelectedToolButton = button;
		if (fSelectedToolButton != null)
			fSelectedToolButton.select();
	}
	
	private void setupAttributes() {
		Color frameColor = (Color) AttributeFigure
		.getDefaultAttribute(FigureAttributeConstant.FRAME_COLOR);
		Color fillColor = (Color) AttributeFigure
		.getDefaultAttribute(FigureAttributeConstant.FILL_COLOR);
		Integer arrowMode = (Integer) AttributeFigure
		.getDefaultAttribute(FigureAttributeConstant.ARROW_MODE);
		String fontName = (String) AttributeFigure
		.getDefaultAttribute(FigureAttributeConstant.FONT_NAME);
		for (FigureEnumeration fe = view().selection(); fe.hasNextFigure();) {
			Figure f = fe.nextFigure();
			frameColor = (Color) f
			.getAttribute(FigureAttributeConstant.FRAME_COLOR);
			fillColor = (Color) f
			.getAttribute(FigureAttributeConstant.FILL_COLOR);
			arrowMode = (Integer) f
			.getAttribute(FigureAttributeConstant.ARROW_MODE);
			fontName = (String) f
			.getAttribute(FigureAttributeConstant.FONT_NAME);
		}
		
		fFrameColor.setSelectedIndex(ColorMap.colorIndex(frameColor));
		fFillColor.setSelectedIndex(ColorMap.colorIndex(fillColor));
		if (arrowMode != null)
			fArrowChoice.setSelectedIndex(arrowMode.intValue());
		if (fontName != null)
			fFontChoice.setSelectedItem(fontName);
	}
	
	protected void setSimpleDisplayUpdate() {
		view().setDisplayUpdate(new SimpleUpdateStrategy());
		fUpdateButton.setText("Simple Update");
		fSimpleUpdate = true;
	}
	
	protected void setBufferedDisplayUpdate() {
		view().setDisplayUpdate(new BufferedUpdateStrategy());
		fUpdateButton.setText("Buffered Update");
		fSimpleUpdate = false;
	}
	
	protected void setUndoManager(org.jhotdraw.util.UndoManager newUndoManager) {
		myUndoManager = newUndoManager;
	}
	
	public org.jhotdraw.util.UndoManager getUndoManager() {
		return myUndoManager;
		//return null;
	}
	
	protected VersionControlStrategy getVersionControlStrategy() {
		return new StandardVersionControlStrategy(this);
	}
	
	public String[] getRequiredVersions() {
		String requiredVersions[] = new String[1];
		requiredVersions[0] = VersionManagement
		.getPackageVersion((DrawApplet.class).getPackage());
		return requiredVersions;
	}
    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
    	java.util.Enumeration keys = UIManager.getDefaults().keys();
    	while (keys.hasMoreElements()) {
    	    Object key = keys.nextElement();
    	    Object value = UIManager.get(key);
    	    
    	    if (value instanceof javax.swing.plaf.FontUIResource){		
    		UIManager.put(key, f);
    	    }
    	}
        }
	
	private transient Drawing fDrawing;	
	private transient Tool fTool;
	public int taskCount = 0;
	
	public transient VWDrawingView fView;
	private transient ToolButton fDefaultToolButton;	
	private transient ToolButton fSelectedToolButton;	
	private transient boolean fSimpleUpdate;	
	private transient JButton fUpdateButton;	
	private transient JComboBox fFrameColor;	
	private transient JComboBox fFillColor;	
	private transient JComboBox fTextColor;	
	private transient JComboBox fArrowChoice;	
	private transient JComboBox fFontChoice;
	private transient org.jhotdraw.util.UndoManager myUndoManager;
	Component component = null;
	static String fgUntitled = "untitled";	
	public static final String IMAGES = "/CH/ifa/draw/images/";
	
}
