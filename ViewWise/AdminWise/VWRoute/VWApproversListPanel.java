/**
 * 
 */
package ViewWise.AdminWise.VWRoute;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.Collator;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;

import com.computhink.common.Principal;

/**
 * @author Nishad Nambiar
 *
 */

class VWApproversListPanel extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	public static int x=0;
	public static VWApproversListTable Table = new VWApproversListTable();
	javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane(Table);
	//JButton btnAdd=new JButton();
	//JButton btnRemove=new JButton();
	public static Vector vecGroupsAndUsers = new Vector();		

	Vector v=new Vector();

	/*public boolean sendMail(int row, int col){
		boolean send = true;
		try{
			DefaultCellEditor editor = (DefaultCellEditor) Table.getDefaultEditor(Boolean.class);
			JCheckBox check = (JCheckBox)editor.getComponent();
			BooleanIcon curIcon = (BooleanIcon) check.getIcon();
			send = Boolean.getBoolean(String.valueOf(Table.getValueAt(row, col)));
			Boolean bool = new Boolean(Table.getValueAt(row,col).toString());
			send = bool.booleanValue();
		}catch(Exception e){

		}
		return send;
	}*/

	public VWApproversListPanel(){
		initComponents();

	}

	public void initComponents(){
		setLayout(null);
		setBackground(AdminWise.getAWColor());
		SPTable.setBounds(5,38,450,130);
		add(SPTable);
		SPTable.getViewport().setBackground(AdminWise.getAWColor());
		/*btnAdd.setBounds(400,10,25,25);
		btnAdd.setToolTipText("Add User/Group");
		btnAdd.setIcon(VWImages.ApproverAddIcon);
		btnAdd.addActionListener(this);
		add(btnAdd);

		btnRemove.setBounds(429,10,25,25);
		btnRemove.setToolTipText("Remove User/Group");
		btnRemove.setIcon(VWImages.ApproverRemoveIcon);
		btnRemove.addActionListener(this);
		btnRemove.setEnabled(false);
		add(btnRemove);
		ListSelectionModel selectionModel= Table.getSelectionModel();
		selectionModel.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
				if (!listSelectionModel.isSelectionEmpty())
					btnRemove.setEnabled(true);
				else
					btnRemove.setEnabled(false);
			}
		});
*/	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}


	/*public void actionPerformed(ActionEvent ae){		
		if(ae.getSource()==btnAdd){
			Vector approvers=new Vector();
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			int gCurRoom = room.getId();
		try{
			Vector groupsAndUsers = new Vector();
			groupsAndUsers = DlgApproversList.getGroupsAndUsers();
			approvers = groupsAndUsers;
			vecGroupsAndUsers = groupsAndUsers;
		}catch(Exception e){
			System.out.println(e.toString());
		}
			Vector approversList = new Vector();
			VWTaskIconProperties.active = false; 
			DlgApproversList obj=new DlgApproversList(AdminWise.adminFrame, approvers);
			try{
				DefaultTableModel model = (DefaultTableModel)Table.getModel();
			}catch(Exception e){
				//System.out.println(e.toString());
			}			 
		}		
		else if(ae.getSource()==btnRemove){
			//Desc   :Removes/Deletes multiple selected Rows.
			//Author :Nishad Nambiar
			//Date   :18-Jul-2007
			try{
				int selRows[] = Table.getSelectedRows();
				int delRow=0;
				for(int curRow=(selRows.length-1);curRow>=0;curRow--){
					Table.m_data.delete(selRows[curRow]);
					Table.m_data.fireTableDataChanged();
				}
			}catch(Exception e){
				//AdminWise.printToConsole("Error :"+e.toString());
			}			

		}
	}*/
}

class DlgApproversList extends JDialog implements ActionListener{
	public static VWListBox approversList;
	public Vector allApproversList = null;	
	public static Vector vecApproversList = null;
	public static Vector vecApproversEmailList = new Vector();
	Principal users=null;
	JButton btnAdd, btnClose;
	JLabel lblApprovers=new JLabel(AdminWise.connectorManager.getString("VWApproversListPanel.lblApprovers"));
	JLabel lblUserNameFilter = new JLabel(AdminWise.connectorManager.getString("VWApproversListPanel.lblUserNameFilter"));
	JTextField txtUserNameFilter = new JTextField();
	DefaultListModel model = new DefaultListModel();

	public static int groupCount = 0;
	public boolean sorted = true;
	public DlgApproversList(){
		this(AdminWise.adminFrame, new Vector());
	}

	public static Vector getGroupsAndUsers(){
		Vector vecGroups = new Vector();

		Vector principals = new Vector();
		principals = VWRouteConnector.getDRSPrincipals(0, "0");
		//AdminWise.gConnector.ViewWiseClient.getDRSPrincipals(roomId, 0, "0",  principals);
		for (int i = 0; i < principals.size(); i++)
		{
			Principal p = (Principal) principals.get(i);
			vecGroups.add(p);
			groupCount++;
		}
		return vecGroups;
	}

	public void setModelApproversList(Vector vecApproversList, DefaultListModel model){
		for(int count=0;count<vecApproversList.size();count++){
			try{
				model.addElement(vecApproversList.get(count));
			}catch(Exception e){

			}
		}
	}

	private void sort() 
	{		 
		int count = vecApproversList.size();
		String[] a = new String[count];
		Principal[] p = new Principal[count];
		for (int i = 0; i < count; i++)
		{
			a[i] = (String) vecApproversList.get(i);
			p[i] = (Principal) this.allApproversList.get(i);
		}
		sortArray(Collator.getInstance(), a);
		sortPrincipalArray(Collator.getInstance(), p);          
		sorted = !sorted;
		for (int i = 0; i < count; i++) 
		{
			vecApproversList.setElementAt(a[i], i);
			this.allApproversList.setElementAt(p[i], i);
		}
	}

	private void sortArray(Collator collator, String[] strArray) 
	{

		if (strArray.length == 1) return;
		for (int i = 0; i < strArray.length; i++) 
		{
			for (int j = i + 1; j < strArray.length; j++) 
			{
				if(sorted){
					lblApprovers.setText(AdminWise.connectorManager.getString("VWApproversListPanel.lblApprovers_1"));
					if(collator.compare(strArray[i], strArray[j] ) > 0 ) 
					{
						String tmp = strArray[i];
						strArray[i] = strArray[j];
						strArray[j] = tmp;
					}
				}else{
					lblApprovers.setText(AdminWise.connectorManager.getString("VWApproversListPanel.lblApprovers_2"));
					if(collator.compare(strArray[i], strArray[j] ) < 0 ) 
					{
						String tmp = strArray[i];
						strArray[i] = strArray[j];
						strArray[j] = tmp;
					}            		
				}
			}
		}
	}

	private void sortPrincipalArray(Collator collator, Principal[] strArray) 
	{    	    	 
		if (strArray.length == 1) return;
		for (int i = 0; i < strArray.length; i++) 
		{
			for (int j = i + 1; j < strArray.length; j++) 
			{
				if(sorted){
					if(collator.compare(strArray[i].getName(), strArray[j].getName() ) > 0 ) 
					{
						Principal tmp = strArray[i];
						strArray[i] = strArray[j];
						strArray[j] = tmp;
					}
				}
				else{
					if(collator.compare(strArray[i].getName(), strArray[j].getName() ) < 0 ) 
					{
						Principal tmp = strArray[i];
						strArray[i] = strArray[j];
						strArray[j] = tmp;
					}            		
				}
			}
		}
	}

	public DlgApproversList(Frame parent, Vector vApproversList){
		super(parent);
		setLayout(null);
		setTitle(AdminWise.connectorManager.getString("VWApproversListPanel.Title"));
		getContentPane().setBackground(AdminWise.getAWColor());
		allApproversList=new Vector();
		vecApproversList = new Vector();
		this.allApproversList = vApproversList;	
		lblApprovers.setBounds(14,5,120,20);
		lblApprovers.setToolTipText(AdminWise.connectorManager.getString("VWApproversListPanel.setToolTipText"));
		add(lblApprovers);
		for(int count=0;count<vApproversList.size();count++){
			try{
				users = (Principal)(vApproversList.get(count));
				vecApproversList.add(users.getName());
				vecApproversEmailList.add(users.getEmail());
			}catch(Exception e){

			}
		}

		approversList=new VWListBox(model);
		approversList.addKeyListener(new KeyAdapter(){
			public void keyReleased(KeyEvent ke){
				if(ke.getKeyCode() == KeyEvent.VK_ENTER){
					btnAdd_ActionPerformed();
				}
			}			
		});

		setModelApproversList(vecApproversList, model);

		lblApprovers.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent me){
				sort();
				for(int count=0;count<model.size();count++){
					try{
						model.removeRange(0,(model.size())-1);
					}catch(Exception e){

					}	               			
				}
				for(int count=0;count<vecApproversList.size();count++){
					try{
						model.addElement(vecApproversList.get(count));
					}catch(Exception e){

					}	            
				}
				approversList.repaint();
			}
		});

		//Set the number of groups
		approversList.setGroupCount(groupCount);
		//reset the counter
		groupCount = 0;		

		JScrollPane scroll=new JScrollPane(approversList);
		scroll.setBounds(14,25,265,435);
		add(scroll);
		btnAdd=new JButton(AdminWise.connectorManager.getString("VWApproversListPanel.btnAdd"));
		btnAdd.setMnemonic('A');
		btnAdd.addActionListener(this);
		btnAdd.setBounds(120,500,80,AdminWise.gBtnHeight);

		lblUserNameFilter.setBounds(14,470,60,20);
		add(lblUserNameFilter);
		txtUserNameFilter.setBounds(80,470,199,20);
		txtUserNameFilter.addKeyListener(new TxtUsernameFilterListener());
		add(txtUserNameFilter);

		add(btnAdd);
		btnClose=new JButton(AdminWise.connectorManager.getString("VWApproversListPanel.btnClose"));
		btnClose.setMnemonic('C');
		btnClose.addActionListener(this);
		btnClose.setBounds(200,500,80,AdminWise.gBtnHeight);
		add(btnClose);		

		setSize(300,580);
		setCurrentLocation();
		setModal(true);			
		setResizable(false);
		setVisible(true);

	}

//------------------------------------------------------------------------------------------

	class TxtUsernameFilterListener extends KeyAdapter{    	
		public void keyReleased(java.awt.event.KeyEvent event){
			Object object = event.getSource();
			Vector userList = new Vector();
			vecApproversList = new Vector();
			userList = vecApproversList;

			if(object == txtUserNameFilter){
				String filter = txtUserNameFilter.getText();
				Vector users = getUsers(filter.trim(), userList);	
				for(int count=0;count<model.size();count++){
					try{
						model.removeRange(0,(model.size())-1);
					}catch(Exception e){
						//JOptionPane.showMessageDialog(null,"EX 1 :"+e.toString());
					}	               			
				}
				approversList.repaint();
				for(int count=0;count<users.size();count++){
					try{
						model.addElement(users.get(count));
					}catch(Exception e){
						//JOptionPane.showMessageDialog(null,"EX 2 :"+e.toString());
					}	            
				}
			}
		}
	}

	public Vector getUsers(String user, Vector users) {	
		Vector usersList1 = new Vector();  	
		if (user.equals("") ){
			VWApproversListPanel.vecGroupsAndUsers = getGroupsAndUsers();
			this.allApproversList = VWApproversListPanel.vecGroupsAndUsers;

			for (int count = 0; count < VWApproversListPanel.vecGroupsAndUsers.size(); count++) {
				try{
					Principal row = (Principal) VWApproversListPanel.vecGroupsAndUsers.get(count);
					vecApproversList.add(row.getName());
				}catch(Exception e){
					//JOptionPane.showMessageDialog(null,e.toString());
				} 
			}
			return users;
		} else{
			Vector tempVecGroupsAndUsers = getGroupsAndUsers();
			Vector vecGroupsAndUsers2 = new Vector();
			Vector vecApproversName = new Vector();
			for (int count = 0; count < tempVecGroupsAndUsers.size(); count++) {
				try{
					Principal row = (Principal) tempVecGroupsAndUsers.get(count);
					String upperRowName = row.getName().toUpperCase(); 
					String upperUser = user.toUpperCase();
					if(upperRowName.startsWith(upperUser)){
						usersList1.add(row.getName());
						vecGroupsAndUsers2.add(row);
						vecApproversName.add(row.getName());
					}
				}catch(Exception e){
					//JOptionPane.showMessageDialog(null,e.toString());
				}
			}
			VWApproversListPanel.vecGroupsAndUsers = vecGroupsAndUsers2;
			vecApproversList = vecApproversName;
			this.allApproversList = vecGroupsAndUsers2;
		}

		return usersList1;
	}

//----------------------------------------------------------------------------------------
	//Set the current location where this dialog box should be loaded.
	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(getSize().width, 60);
		setLocation(point);
	}
//	----------------------------------------------------------------------------------------
	public void btnAdd_ActionPerformed(){
		VWTaskIconProperties.active = true;
		Vector v=new Vector();
		Vector vecSelectedIndexes = new Vector();			
		for(int index = 0; index < vecApproversList.size();index++){
			if(approversList.isSelectedIndex(index)){
				vecSelectedIndexes.add(index);
			}
		}
		for(int count=0;count<vecSelectedIndexes.size();count++){
			int principalIndex=Integer.parseInt(String.valueOf(vecSelectedIndexes.get(count)));
			Principal selUser = (Principal)(this.allApproversList.get(principalIndex));
			String approverName = selUser.getName();
			String approversEmailID = selUser.getEmail();
			String userID = String.valueOf(selUser.getUserGroupId());
			String approverType = String.valueOf(selUser.getType());
			String principalId = String.valueOf(selUser.getId());
			VWApproverInfo approverInfo = new VWApproverInfo();

			approverInfo.setApproverEmailFlag(false);
			approverInfo.setApproverType(approverType);
			approverInfo.setApproverActionFlag(true);
			approverInfo.setApproverName(approverName);
			approverInfo.setApproverEmail(selUser.getEmail());
			approverInfo.setApproverId(userID);
			approverInfo.setApproverAction("Reject");
			approverInfo.setApproverTaskDescription("Task Description");
			approverInfo.setApproverPrincipalId(principalId);
			approverInfo.setApproverTaskLength("2");
			approverInfo.setApproverESignature("E-Sign");
			approverInfo.setApproverTask("Assign Task");
			v.add(approverInfo);
		}
		VWApproversListPanel.Table.insertData(v,1," ");
		setVisible(false);
		dispose();
	}
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource()==btnAdd){
			btnAdd_ActionPerformed();
		}
		else if(ae.getSource()==btnClose){
			VWTaskIconProperties.active = true;
			setVisible(false);
			dispose();
		}
	}
}

