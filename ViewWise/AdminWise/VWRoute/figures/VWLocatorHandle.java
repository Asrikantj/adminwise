package ViewWise.AdminWise.VWRoute.figures;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import org.jhotdraw.framework.Figure;
import org.jhotdraw.framework.Locator;
import org.jhotdraw.standard.LocatorHandle;

public class VWLocatorHandle  extends LocatorHandle{

	private int boxSize = 12;
	/**
	 * @param arg0
	 * @param arg1
	 */
	public VWLocatorHandle(Figure arg0, Locator arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the display box of the handle.
	 */
	public Rectangle displayBox() {
		Point p = locate();
		return new Rectangle(
				p.x - boxSize / 2,
				p.y - boxSize / 2,
				boxSize/2,
				boxSize/2);
	}

	/**
	 * Draws this handle.
	 */
	public void draw(Graphics g) {
		Rectangle r = displayBox();

		g.setColor(new Color(65, 140, 190));
		g.fillRect(r.x, r.y, r.width, r.height);

		g.setColor(Color.white);
		g.drawRect(r.x, r.y, r.width, r.height);
	}
	
	
}