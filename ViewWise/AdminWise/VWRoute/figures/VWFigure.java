package ViewWise.AdminWise.VWRoute.figures;

import org.jhotdraw.framework.Figure;

public interface VWFigure extends Figure{

	public void setFigureType(String figType);
	public String getFigureType();	
	public void setFigureName(String figName);
	public String getFigureName();
	public void setFigureId(int figId);
	public int getFigureId();

}
