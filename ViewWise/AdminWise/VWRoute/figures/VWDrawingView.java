package ViewWise.AdminWise.VWRoute.figures;

import java.awt.Graphics;

import org.jhotdraw.framework.DrawingEditor;
import org.jhotdraw.standard.StandardDrawingView;

public class VWDrawingView extends StandardDrawingView {

	/**
	 * @param args
	 */
	public VWDrawingView(DrawingEditor editor) {
		super(editor);
	}
	public VWDrawingView(DrawingEditor editor, int width, int height) {
		super(editor, width, height);
	}
	public void drawDrawing(Graphics g) {
		//checkMinimumSize();
		super.drawDrawing(g);
	}
	
	/** This method was overridden because it does not resize view immediately*/
/*	protected void checkMinimumSize() {
        Dimension d = getDrawingSize();
		Dimension v = getPreferredSize();
		
		if (v.height < d.height || v.width < d.width) {
			v.height = d.height + SCROLL_OFFSET;
			v.width = d.width + SCROLL_OFFSET;
			//setPreferredSize(v);
			setSize(v);
        }
    }
	protected java.awt.Dimension getDrawingSize() {
		java.awt.Dimension d = super.getDrawingSize();
		if (d.getWidth() > d.getHeight())
			return new java.awt.Dimension(d.width, d.width);
		else
			return new java.awt.Dimension(d.height, d.height);
	}*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
