package ViewWise.AdminWise.VWRoute.figures;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import org.jhotdraw.figures.ArrowTip;
import org.jhotdraw.figures.ElbowConnection;
import org.jhotdraw.figures.LineDecoration;
import org.jhotdraw.framework.HandleEnumeration;
import org.jhotdraw.standard.HandleEnumerator;
import org.jhotdraw.util.CollectionsFactory;

public class VWLineConnection extends ElbowConnection //implements VWFigure
{

	private String figureType = "LINE";
	private String figureName = "";
	private int figureId = -1;
	
	public VWLineConnection() {
		super();
		
		setFrameColor(new Color(120, 165, 245));
		setEndDecoration(new ArrowTip(/*0.45, 12, 12*/0.35, 15, 12));		
		setStartDecoration(null);
	}
	
	public HandleEnumeration handles() {
		List handles = CollectionsFactory.current().createList();		
		//handles.add(new ChangeConnectionStartHandle(this));
/*		for (int i = 1; i < fPoints.size()-1; i++) {
			handles.add(new NullHandle(this, locator(i)));
		}
		handles.add(new ChangeConnectionEndHandle(this));
		for (int i = 0; i < fPoints.size()-1; i++) {
			handles.add(new ElbowHandle(this, i));
		}
*/				
		//handles.add(new NullHandle(this, locator(2)));
		//handles.add(new ElbowHandle(this, 2));
		//handles.add(new ChangeConnectionEndHandle(this));
		return new HandleEnumerator(handles);
	}

	
	public void setFigureType(String figType){
		figureType = figType;
	}
	public String getFigureType(){
		return figureType;
	}
	public void setFigureName(String figName){
		figureName = figName;
	}
	public String getFigureName(){
		return figureName;
	}
	public void setFigureId(int figId){
		figureId = figId;
	}
	public int getFigureId(){
		return figureId;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
