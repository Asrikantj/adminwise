package ViewWise.AdminWise.VWRoute.figures;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.IOException;
import java.util.List;

import org.jhotdraw.figures.AttributeFigure;
import org.jhotdraw.figures.ChopEllipseConnector;
import org.jhotdraw.framework.Connector;
import org.jhotdraw.framework.FigureAttributeConstant;
import org.jhotdraw.framework.HandleEnumeration;
import org.jhotdraw.standard.HandleEnumerator;
import org.jhotdraw.standard.RelativeLocator;
import org.jhotdraw.util.CollectionsFactory;
import org.jhotdraw.util.StorableInput;
import org.jhotdraw.util.StorableOutput;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;

public class VWStartFigure extends AttributeFigure implements VWFigure{

	/**
	 * @param args
	 */
	private Rectangle   fDisplayBox;
	public String figureName = "";
	public String figureType = "START";
	public int figureId = 0;
	public VWStartFigure() {		
		this(new Point(0,0), new Point(0,0));
	}
	public VWStartFigure(Point origin, Point corner) {
		basicDisplayBox(origin,corner);
		setAttribute(FigureAttributeConstant.getConstant(FigureAttributeConstant.FILL_COLOR_STR), new Color(102,204,153));
	}
	public void basicDisplayBox(Point origin, Point corner) {
		fDisplayBox = new Rectangle(origin);
		fDisplayBox.add(corner);
	}
	protected void basicMoveBy(int x, int y) {
		fDisplayBox.translate(x,y);		
	}
	public Rectangle displayBox() {
		return new Rectangle(
			fDisplayBox.x,
			fDisplayBox.y, 48, 60);
			//(fDisplayBox.width>48?48:fDisplayBox.width),
			//(fDisplayBox.height>49?49:fDisplayBox.height));
	}
	public void drawFrame(Graphics g) {
        Rectangle r = displayBox();
       String Start= AdminWise.connectorManager.getString("DRSDrawing.Start");
        String display = Start;//getFigureName();
        int position = getTextStartPosition(display, r.width, g);
        g.drawString(display, r.x + position, r.y + r.height);        
	}
	public void drawBackground(Graphics g) {
		Rectangle r = displayBox();
	    Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);	
		//g.drawImage(img,r.x,r.y,r.width,r.height,null);
		g2d.drawImage(VWImages.StartImage, r.x,r.y,r.width,r.height - 12, null);
	}
    protected void setShape(Point origin) {
        basicDisplayBox(origin, new Point((int) (origin.getX() + 48),
                (int) (origin.getY() + 48)));
    }
    protected int getTextStartPosition(String text, int width, Graphics g) {
        byte[] by = text.getBytes();
        int length = g.getFontMetrics().bytesWidth(by, 0, by.length);
        return width / 2 - length / 2;
    }
	public Connector connectorAt(int x, int y) {
		return new ChopEllipseConnector(this);
	}
	public Insets connectionInsets() {
		Rectangle r = fDisplayBox;
		int cx = r.width/2;
		int cy = r.height/2;
		return new Insets(cy, cx, cy, cx);
	}
	public void read(StorableInput dr) throws IOException {
		super.read(dr);
		fDisplayBox = new Rectangle(
			dr.readInt(),
			dr.readInt(),
			dr.readInt(),
			dr.readInt());
	}
	public void write(StorableOutput dw) {
		super.write(dw);
		dw.writeInt(fDisplayBox.x);
		dw.writeInt(fDisplayBox.y);
		dw.writeInt(fDisplayBox.width);
		dw.writeInt(fDisplayBox.height);
	}
	public HandleEnumeration handles() {
		List handles = CollectionsFactory.current().createList();
		//BoxHandleKit.addHandles(this, handles);
/*        handles.add(new VWLocatorHandle(this, RelativeLocator.east()));
        handles.add(new VWLocatorHandle(this, RelativeLocator.west()));
        handles.add(new VWLocatorHandle(this, RelativeLocator.north()));
        handles.add(new VWLocatorHandle(this, RelativeLocator.south()));*/
        handles.add(new VWLocatorHandle(this, RelativeLocator.northEast()));
        handles.add(new VWLocatorHandle(this, RelativeLocator.northWest()));
        handles.add(new VWLocatorHandle(this, RelativeLocator.southEast()));
        handles.add(new VWLocatorHandle(this, RelativeLocator.southWest()));       
		return new HandleEnumerator(handles);
	}

	public void setFigureName(String figName){
		figureName = figName;
	}

	public String getFigureName(){
		return figureName;
	}
	public void setFigureType(String figType){
		figureType = figType;
	}
	public String getFigureType(){
		return figureType;
	}
	public void setFigureId(int figId){
		figureId = figId;
	}
	public int getFigureId(){
		return figureId;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
