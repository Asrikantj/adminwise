package ViewWise.AdminWise.VWRoute;

import java.awt.Frame;

import javax.swing.JDialog;

import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;

/**
 * @author Pandiya Raj.M
 *	
 * VWAssignRouteSetting class used to assign the created route to the document Type.  
 *
 */
public class VWAssignRouteSetting extends JDialog implements VWConstant{

	/**
	 * @param args
	 */
	public VWAssignRouteSetting(){
		 
	}
	public VWAssignRouteSetting(Frame parent,int docTypeId){
		 super(parent);
		 setSize(430, 345);
		 vwDocTypeRec = null;
         //JOptionPane.showMessageDialog(null,"docTypeId: "+docTypeId,"",JOptionPane.OK_OPTION);
         vwDocTypeRec = VWDocTypeConnector.getDocTypeInfo(docTypeId);
		 setTitle(LBL_ASSIGNROUTESETTING_NAME+" for \""+vwDocTypeRec.getName()+"\"");
	}
	
	VWDocTypeRec vwDocTypeRec = null;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
