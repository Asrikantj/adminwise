package ViewWise.AdminWise.VWRoute;

import java.awt.Component;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import org.jhotdraw.contrib.PopupMenuFigureSelection;
import org.jhotdraw.contrib.zoom.ZoomDrawingView;
import org.jhotdraw.framework.DrawingEditor;
import org.jhotdraw.framework.Figure;
import org.jhotdraw.framework.FigureAttributeConstant;
import org.jhotdraw.framework.Tool;
import org.jhotdraw.standard.DragTracker;
import org.jhotdraw.standard.SelectionTool;
import org.jhotdraw.util.UndoableTool;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoute.figures.VWFigure;

public class VWSelectionTool extends SelectionTool{

	/**
	 * @param args
	 */
	private boolean doubleclick;
	public String figureType = "";
	VWStartIconProperties vwStartIconProperties = null;
	VWTaskIconProperties  vwTaskIconProperties = null;
	VWEndIconProperties vwEndIconProperties = null;
	
	public VWSelectionTool(DrawingEditor newDrawingEditor) {
		super(newDrawingEditor);		
	}
	public void mouseUp(MouseEvent e, int x, int y) {
		if(e.getModifiers()==e.BUTTON3_MASK){
			//if(figureType.trim().equalsIgnoreCase("class ViewWise.AdminWise.VWRoute.figures.VWTaskFigure"))
			{				
				Figure figure = drawing().findFigure(e.getX(), e.getY());
				if(figure == null){
					
				}else{
					Object attribute = figure.getAttribute(FigureAttributeConstant.POPUP_MENU);
					if (attribute == null) {
						figure = drawing().findFigureInside(e.getX(), e.getY());
					}
					if (figure != null) {
						showPopupMenu(figure, e.getX(), e.getY(), e.getComponent());
					}
				}
			}
		}
        //Double click open dialog.    		
		if(e.getClickCount()==2){
			Figure figure = drawing().findFigure(e.getX(), e.getY());
			if(figure != null){
				if (figure instanceof VWFigure) {
					VWFigure selectedFigure = (VWFigure)figure;
					String figType = selectedFigure.getFigureType();
					String figName = selectedFigure.getFigureName();
					AdminWise.adminPanel.routePanel.drsDrawing.openDialog(figType, figName);
				}
			}
		}

		//System.out.println("VW Selection Tool Clicked mouse 1 ::::");
		
/*		try{
			if (e.getClickCount() == 2){
				System.out.println("Figure type in VWSelection Tool " + figureType);
				if(figureType.trim().equalsIgnoreCase("class ViewWise.AdminWise.VWRoute.figures.VWStartFigure")){
					 if(vwStartIconProperties==null) {
						 vwStartIconProperties = new VWStartIconProperties(new Frame(),1000);
				     }else{
				        	vwStartIconProperties.loadStartRouteInfo();
				        	vwStartIconProperties.setVisible(true);
				     }
					System.out.println("VWStartFigure Tool Clicked");
					
				}
				if(figureType.trim().equalsIgnoreCase("class ViewWise.AdminWise.VWRoute.figures.VWTaskFigure")){
					String figureName = "Task";
					int figureId = 0;
					FigureEnumeration figureEnum = AdminWise.adminPanel.routePanel.drsDrawing.view().selection();
					if (figureEnum.hasNextFigure()){
						VWFigure figure = (VWFigure) figureEnum.nextFigure();
						try{
							if (figure.getAttribute(FigureAttributeConstant.getConstant("Task")) == null)
									figureName = figure.getFigureName();
							else
									figureName = String.valueOf(figure.getAttribute(FigureAttributeConstant.getConstant("Task")));
							
							if (figure.getAttribute(FigureAttributeConstant.getConstant("FigureId")) == null)
								figureId = figure.getFigureId();
							else
								figureId = Integer.parseInt(figure.getAttribute(FigureAttributeConstant.getConstant("FigureId")).toString());
							System.out.println("getting the Figure id ************** " + figureId);
							figure.setFigureId(figureId);
						}catch(Exception ex){
							
						}
						
					}
					
					System.out.println("Task figureName " + figureName);
					if(vwTaskIconProperties == null){
						vwTaskIconProperties = new VWTaskIconProperties(figureName);						
					}
					else{
						// Need to add load settings here. Valli
						vwTaskIconProperties.loadTaskRouteInfo(figureName);
						vwTaskIconProperties.setVisible(true);
					}
					System.out.println("VWTaskFigure Tool Clicked");
					
				}
				if(figureType.trim().equalsIgnoreCase("class ViewWise.AdminWise.VWRoute.figures.VWEndFigure")){
					if(vwEndIconProperties==null)
						vwEndIconProperties = new VWEndIconProperties();
					else{
						vwEndIconProperties.loadEndRouteInfo();
						vwEndIconProperties.setVisible(true);
					}
					System.out.println("VWEndFigure Tool Clicked");
				}
			}
			else if(e.getClickCount()==1){				
				doubleclick = false;
			}					
		}catch(Exception ex){
			ex.printStackTrace();
		}*/
		if (getDelegateTool() != null) { // JDK1.1 doesn't guarantee mouseDown, mouseDrag, mouseUp
			getDelegateTool().mouseUp(e, x, y);
			getDelegateTool().deactivate();
			setDelegateTool(null);
		}
		if (view() != null) {
			//System.out.println();
			view().unfreezeView();
			editor().figureSelectionChanged(view());
		}
		figureType = "";	
		AdminWise.adminPanel.routePanel.drsDrawing.repaint();
	}
	
	protected Tool createDragTracker(Figure f) {
		//System.out.println("Figure :"+f.getDecoratedFigure());
		System.out.println("Figure :"+f.getZValue());
		DragTracker dragTracker = new DragTracker(editor(), f);						
		
		System.out.println("VWStool Figure :"+f.getDecoratedFigure().getClass());
		
		figureType = f.getDecoratedFigure().getClass().toString();
		return new UndoableTool(dragTracker);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	/**
	 * This method displays a popup menu, if there is one registered with the
	 * Figure (the Figure's attributes are queried for Figure.POPUP_MENU which
	 * is used to indicate an association of a popup menu with the Figure).
	 *
	 * @param   figure      Figure for which a popup menu should be displayed
	 * @param   x           x coordinate where the popup menu should be displayed
	 * @param   y           y coordinate where the popup menu should be displayed
	 * @param   comp        Component which invoked the popup menu
	 */
	protected void showPopupMenu(Figure figure, int x, int y, Component comp) {
		Object attribute = figure.getAttribute(FigureAttributeConstant.POPUP_MENU);
		if ((attribute != null) && (attribute instanceof JPopupMenu)) {
			JPopupMenu popup = (JPopupMenu)attribute;
			if (popup instanceof PopupMenuFigureSelection) {
				((PopupMenuFigureSelection)popup).setSelectedFigure(figure);
			}
			// Calculate position on physical screen based
			// on x,y coordinates
			Point newLocation;
			try {
				newLocation = comp.getLocationOnScreen();
			} catch (IllegalComponentStateException e) {
				// For some reason, the component
				// apparently isn't showing on the
				// screen (huh?). Never mind - don't
				// show the popup..
				return;
			}
			// If this is a ZoomDrawingView, we'll need to
			// compensate here too:
			if (comp instanceof ZoomDrawingView) {
				double scale = ((ZoomDrawingView) comp).getScale(); 
				x *= scale;
				y *= scale;
			}
			newLocation.translate(x,y);
			popup.setLocation(newLocation);
			popup.setInvoker(comp);
			popup.setVisible(true);
		}
	}
}
