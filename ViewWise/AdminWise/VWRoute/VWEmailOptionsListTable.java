package ViewWise.AdminWise.VWRoute;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;

import com.computhink.common.Constants;
import com.computhink.common.Document;
import com.computhink.common.VWEmailOptionsInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.resource.ResourceManager;

/**
 * @author Vijaypriya.B.K
 * December 07, 2010
 */


public class VWEmailOptionsListTable extends JTable implements VWConstant
{
	public static boolean emailIconFlag = false;
	public static boolean actionIconFlag = false;
	public static boolean enableMode = false;
	
    public static VWEmailOptionsListTableData m_data;
	/****CV2019 merges - SIDBI - VWR enhancement changes***/
    TableColumn[] ColModel=new TableColumn[6];
    static boolean[] visibleCol={true, true, true, true, true};
    Timer keyTimer;    
    public boolean isCellEditable(int row, int column) {
    	if(column==0){
    		return true;
    	} else{
    		return false;
    	}
    }
    
    public VWEmailOptionsListTable(){
        super();
        m_data = new VWEmailOptionsListTableData(this);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        JTextField readOnlyText=new JTextField();
        readOnlyText.setEditable(true);
        Dimension tableWith = getPreferredScrollableViewportSize();
        setFont(readOnlyText.getFont());
        
        for (int k = 0; k < VWEmailOptionsListTableData.m_columns.length; k++) {
	        TableCellRenderer renderer;
	        
	        DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
	        textRenderer.setHorizontalAlignment(VWEmailOptionsListTableData.m_columns[k].m_alignment);
	       	renderer = textRenderer;
	        VWEmailOptionsListRowEditor editor=new VWEmailOptionsListRowEditor(this);
	        TableColumn column = new TableColumn
	            (k,Math.round(tableWith.width*VWEmailOptionsListTableData.m_columns[k].m_width), 
	                renderer, editor);
	        addColumn(column);   
	        ColModel[k]=column;
        }
        putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        setBackground(java.awt.Color.white);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        
        keyTimer = new Timer(500, new keyTimerAction());
        keyTimer.setRepeats(false);
        /****CV2019 merges - SIDBI - VWR enhancement changes***/
        getColumnModel().getColumn(0).setPreferredWidth(227);
        getColumnModel().getColumn(1).setPreferredWidth(32);
		getColumnModel().getColumn(2).setPreferredWidth(32);
		getColumnModel().getColumn(3).setPreferredWidth(32);
		getColumnModel().getColumn(4).setPreferredWidth(32);
		
		getColumnModel().getColumn(0).setResizable(false);
		getColumnModel().getColumn(1).setResizable(false);
		getColumnModel().getColumn(2).setResizable(false);
		getColumnModel().getColumn(3).setResizable(false);
		getColumnModel().getColumn(4).setResizable(false);
		
		setShowGrid(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		getTableHeader().setReorderingAllowed(false);
		
		setRowHeight(20);
		
		EmailOptionsColumnHeaderToolTips tips = new EmailOptionsColumnHeaderToolTips();
	    
	    // Assign a tooltip for each of the columns
	    for (int c=0; c<getColumnCount(); c++) {
	        TableColumn col = getColumnModel().getColumn(c);
	        if(c==0){
	        	tips.setToolTip(col, AdminWise.connectorManager.getString("VWEmailOptionsListTable.setToolTip_1"));
	        }
	        else if(c==1){
	        	tips.setToolTip(col, AdminWise.connectorManager.getString("VWEmailOptionsListTable.setToolTip_2"));
	        } 
	        else if(c==2){
	        	tips.setToolTip(col,ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWEmailOptionsListTable.setToolTip_3"));
	        } 
	        else if(c==3){
	        	tips.setToolTip(col,ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("VWEmailOptionsListTable.setToolTip_4"));
	        } 
	        else if(c==4){/****CV2019 merges - SIDBI - VWR enhancement changes***/
	        	tips.setToolTip(col, AdminWise.connectorManager.getString("VWEmailOptionsListTable.setToolTip_5"));
	        } 
	    }
	    header.addMouseMotionListener(tips);
		
	    // Set the text and icon values on the columns for the icon render
	    getTableHeader().getColumnModel().getColumn(1).setHeaderRenderer(iconHeaderRenderer);	    
	    getColumnModel().getColumn(1).setHeaderValue(new TextAndIcon("", VWImages.documentVwrIcon));
	    
	    // Set the icon renderer on the columns
	    getTableHeader().getColumnModel().getColumn(2).setHeaderRenderer(iconHeaderRenderer);	    
	    getColumnModel().getColumn(2).setHeaderValue(new TextAndIcon("", VWImages.docMetaDataIcon));
	    
	    // Set the icon renderer on the columns
	    getTableHeader().getColumnModel().getColumn(3).setHeaderRenderer(iconHeaderRenderer);	    
	    getColumnModel().getColumn(3).setHeaderValue(new TextAndIcon("", VWImages.docRouteSummaryIcon));
	    
	    // Set the icon renderer on the columns
	    getTableHeader().getColumnModel().getColumn(4).setHeaderRenderer(iconHeaderRenderer);	    
	    getColumnModel().getColumn(4).setHeaderValue(new TextAndIcon("", VWImages.includePagesIcon));
	    
	 	setTableResizable();
  }
    

    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
        
    
    // This class is used to hold the text and icon values
    // used by the renderer that renders both text and icons
    class TextAndIcon {
        TextAndIcon(String text, Icon icon) {
            this.text = text;
            this.icon = icon;
        }
        String text;
        Icon icon;
    }
    
    // This customized renderer can render objects of the type TextandIcon
    TableCellRenderer iconHeaderRenderer = new DefaultTableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {
            // Inherit the colors and font from the header component
            if (table != null) {
                JTableHeader header = table.getTableHeader();
                if (header != null) {
                    setForeground(header.getForeground());
                    setBackground(header.getBackground());
                    setFont(header.getFont());
                }
            }
    
            if (value instanceof TextAndIcon) {
                setIcon(((TextAndIcon)value).icon);
                setText(((TextAndIcon)value).text);
            } else {
                setText((value == null) ? "" : value.toString());
                setIcon(null);
            }
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setHorizontalAlignment(JLabel.CENTER);
            return this;
  }
    };
    
    public String getToolTipText(MouseEvent e) {
    	   String tip = null;
    	   int selRows[] = getSelectedRows();    	   
	        java.awt.Point p = e.getPoint();
	        int rowIndex = rowAtPoint(p);
	        int colIndex = columnAtPoint(p);
	        int realColumnIndex = convertColumnIndexToModel(colIndex);
	        VWEmailOptionsInfo vwEmailOptionsInfo = (VWEmailOptionsInfo)getData().elementAt(rowIndex);
	        if (realColumnIndex == m_data.COL_USEREMAILADDRESS) {
	        	if(vwEmailOptionsInfo.getUserEmailAddress()!=null 
	        			&& !vwEmailOptionsInfo.getUserEmailAddress().trim().equals("")
	        			&& !vwEmailOptionsInfo.getUserEmailAddress().trim().equals("-")
	        			&& !vwEmailOptionsInfo.getUserEmailAddress().trim().equals("0"))
	        		tip = "" + vwEmailOptionsInfo.getUserEmailAddress();
	        	else
	        		tip = "No mail id";
	        }
	        try{
		        if(selRows.length>1)
		        	setRowSelectionInterval(selRows[0],selRows[selRows.length-1]);
		        else
		        	setRowSelectionInterval(selRows[0],selRows[0]);
	        }catch(Exception ex){
	        	//System.out.println("Exception in getToolTip() Method :"+ex.toString());
	        }
	        return tip;
    }

    public class EmailOptionsColumnHeaderToolTips extends MouseMotionAdapter {
        // Current column whose tooltip is being displayed.
        // This variable is used to minimize the calls to setToolTipText().
        TableColumn curCol;
        // Maps TableColumn objects to tooltips
        Map tips = new HashMap();
        // If tooltip is null, removes any tooltip text.
        public void setToolTip(TableColumn col, String tooltip) {
            if (tooltip == null) {
                tips.remove(col);
            } else {
                tips.put(col, tooltip);
            }
        }
    
        public void mouseMoved(MouseEvent evt) {
            TableColumn col = null;
            JTableHeader header = (JTableHeader)evt.getSource();
            JTable table = header.getTable();
            TableColumnModel colModel = table.getColumnModel();
            int vColIndex = colModel.getColumnIndexAtX(evt.getX());
            // Return if not clicked on any column header
            if (vColIndex >= 0) {
                col = colModel.getColumn(vColIndex);
            }
            if (col != curCol) {
                header.setToolTipText((String)tips.get(col));
                curCol = col;
            }
        }
    }
    
//------------------------------------------------------------------------------
  public void addData(Vector docs, int roomId,String roomName)
  {
        m_data.setData(docs,roomId,roomName);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public int getRowCount()
  {
      if (m_data==null) return 0;
      return m_data.getRowCount();
  }
//------------------------------------------------------------------------------
  public void insertData(Vector docs,int roomId,String roomName)
  {
     if(docs==null || docs.size()==0)
        return;
    for(int i=0;i<docs.size();i++)
    {
    	VWEmailOptionsInfo doc = (VWEmailOptionsInfo)docs.get(i);
        m_data.insert(doc);
    }
     m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteRow(int row)
  {
        m_data.delete(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public Vector getData()
  {
	  m_data.fireTableDataChanged();
      return m_data.getData();
  }
//------------------------------------------------------------------------------
  public VWEmailOptionsInfo getRowData(int rowNum)
  {
	  return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public String getRowUserEmailAddress(int rowNum)
  {
	  	VWEmailOptionsInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getUserEmailAddress();
  }
//------------------------------------------------------------------------------
  /****CV2019 merges - SIDBI - VWR enhancement changes***/
  public int getRowDocumentVWRData(int rowNum)
  {
	  	VWEmailOptionsInfo row=m_data.getRowData(rowNum);
        if(row==null) return -1;
        return row.getDocumentVwrFlag();
        
  }
//------------------------------------------------------------------------------
  public int getRowDocumentMetaData(int rowNum)
  {
	  	VWEmailOptionsInfo row=m_data.getRowData(rowNum);
        if(row==null) return -1;
        return row.getDocumentMetaDataFlag();
        
  }
//------------------------------------------------------------------------------
  public int getRowRouteSummaryReport(int rowNum)
  {
        VWEmailOptionsInfo row=m_data.getRowData(rowNum);
        if(row==null) return -1;
        return row.getRouteSummaryReportFlag();
  }
//------------------------------------------------------------------------------
  public int getRowDocumentPages(int rowNum)
  {
        VWEmailOptionsInfo row=m_data.getRowData(rowNum);
        if(row==null) return -1;
        return row.getDocumentPagesFlag();
  }

  public RouteMasterInfo getDocument(int rowNum)
  {
        return m_data.getDocument(rowNum);
  }

//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyPressed(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if (object instanceof JTable)
                    Table_keyPressed(event);
        }
    }
//------------------------------------------------------------------------------
    void Table_keyPressed(java.awt.event.KeyEvent event)
	{
            if(event.getKeyCode()==event.VK_DOWN)
            {
                keyTimer.stop();
                keyTimer.start();
            }   
            else if(event.getKeyCode()==event.VK_UP)
            {
                keyTimer.stop();
                keyTimer.start();
            }
            else if(event.getKeyCode()==event.VK_ENTER)
            {
                event.setKeyCode(event.VK_UNDEFINED);
            }
	}
    
//  ------------------------------------------------------------------------------
    protected class keyTimerAction implements java.awt.event.ActionListener {
    public void actionPerformed(java.awt.event.ActionEvent e) {
        keyTimer.stop();
        }
    }
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
    	  Point origin = event.getPoint();
    	    int row = rowAtPoint(origin);
    	    int column = columnAtPoint(origin);
    	    if (row == -1 || column == -1)
    	       return; // no cell found
    	    if(event.getClickCount() == 1)
    	    lSingleClick(event,row,column);
    }
  }
//------------------------------------------------------------------------------
void VWToDoListTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
	
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    
    if(event.getClickCount() == 1)
    	lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWToDoListTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
}
//------------------------------------------------------------------------------
private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
}
//------------------------------------------------------------------------------
/****CV2019 merges - SIDBI - VWR enhancement changes***/
private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
	boolean changePdfChkVisibility = false;
	if(VWRoutePanel.inUpdateMode || enableMode){
		if (row >= m_data.getData().size()) return;
		VWEmailOptionsInfo vwEmailOptionsInfo = (VWEmailOptionsInfo)m_data.getData().elementAt(row);
		if (vwEmailOptionsInfo.isEditableFlag == false && col != 1) return;
		int rowSelected = getSelectedRow();
		m_data.fireTableDataChanged();
		ListSelectionModel selectionModel = getSelectionModel();
		selectionModel.setSelectionInterval(rowSelected, rowSelected);
		
		if(col==0){
			vwEmailOptionsInfo.setUserEmailAddress(this.getValueAt(row, col).toString());
		}else if(col==1){
			AdminWise.printToConsole(" vwEmailOptionsInfo.getIncludeDwrDocument() Action "+vwEmailOptionsInfo.getDocumentVwrFlag());
			if(vwEmailOptionsInfo.getDocumentVwrFlag()==1) {				
				vwEmailOptionsInfo.setDocumentVwrFlag(0);
				vwEmailOptionsInfo.setEditableFlag(true);
			} else {
				vwEmailOptionsInfo.setDocumentVwrFlag(1);
				vwEmailOptionsInfo.setDocumentMetaDataFlag(0);
				vwEmailOptionsInfo.setRouteSummaryReportFlag(0);
				vwEmailOptionsInfo.setDocumentPagesFlag(0);		
				vwEmailOptionsInfo.setEditableFlag(false);	
				changePdfChkVisibility = true;
			}
			AdminWise.printToConsole(" Action "+vwEmailOptionsInfo.getDocumentVwrFlag());
		}else if(col==2){
			AdminWise.printToConsole(" Action "+vwEmailOptionsInfo.getDocumentMetaDataFlag());
			if(vwEmailOptionsInfo.getDocumentMetaDataFlag()==1)
				vwEmailOptionsInfo.setDocumentMetaDataFlag(0);
			else
				vwEmailOptionsInfo.setDocumentMetaDataFlag(1);
			AdminWise.printToConsole(" Action "+vwEmailOptionsInfo.getDocumentMetaDataFlag());
		}else if(col==3){
			AdminWise.printToConsole(" Action "+vwEmailOptionsInfo.getRouteSummaryReportFlag());
			if(vwEmailOptionsInfo.getRouteSummaryReportFlag()==1)
				vwEmailOptionsInfo.setRouteSummaryReportFlag(0);
			else
				vwEmailOptionsInfo.setRouteSummaryReportFlag(1);
			AdminWise.printToConsole(" Action "+vwEmailOptionsInfo.getRouteSummaryReportFlag());
		}else if(col==4){
			changePdfChkVisibility = true;
			AdminWise.printToConsole(" Action "+vwEmailOptionsInfo.getDocumentPagesFlag());
			if(vwEmailOptionsInfo.getDocumentPagesFlag()==1)
				vwEmailOptionsInfo.setDocumentPagesFlag(0);
			else
				vwEmailOptionsInfo.setDocumentPagesFlag(1);
		}
		
		repaint();
		AdminWise.printToConsole(" changePdfChkVisibility : "+changePdfChkVisibility);
		if (changePdfChkVisibility) {
			VWEndIconProperties.setConvertToPDFAndCompressPages(null);
		}
	}
	
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
 }
//------------------------------------------------------------------------------
 	public void updateRow(int rowId, boolean freeze){
		m_data.fireTableDataChanged();
	}
}
//------------------------------------------------------------------------------
class EmailToDoListColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public EmailToDoListColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWEmailOptionsListTableData extends DefaultTableModel
{
	public static final EmailToDoListColumnData m_columns[] = {
		new EmailToDoListColumnData( AdminWise.connectorManager.getString("WorkFlow.EamilTitle"),0.5F, JLabel.LEFT),//UserEmailAddress
	    new EmailToDoListColumnData( "",0.1F,JLabel.LEFT ),//DocPropIcon
	    new EmailToDoListColumnData( "",0.1F,JLabel.LEFT ),//DocPropIcon
	    new EmailToDoListColumnData( "",0.1F,JLabel.LEFT ),//RouteSummaryIcon
	    new EmailToDoListColumnData( "",0.1F,JLabel.LEFT ),//DocPages
	};
	
	/****CV2019 merges - SIDBI - VWR enhancement changes***/
	public static final int COL_USEREMAILADDRESS=0;
	public static final int COL_DOCUMENTVWRICON=1;
	public static final int COL_DOCUMENTMETADATAICON=2;
	public static final int COL_ROUTESUMMARYREPORTICON=3;
	public static final int COL_DOCUMENTPAGESICON=4;

	public static final int COL_ID=7;
  
	protected VWEmailOptionsListTable m_parent;
	protected Vector m_vector;
	protected int m_sortCol = 0;
	protected boolean m_sortAsc = true;

	public VWEmailOptionsListTableData(VWEmailOptionsListTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}

	public Class getColumnClass(int columnIndex) {	
		return Object.class;
	}

//	------------------------------------------------------------------------------
	public void setData(Vector docs,int roomId,String roomName) {
		m_vector.removeAllElements();
		if(docs==null || docs.size()==0)    return;
		int count =docs.size();
		int freezed = -1;
		for(int i=0;i<count;i++)
		{
			VWEmailOptionsInfo vwEmailOptionsInfo=(VWEmailOptionsInfo)docs.get(i);
			m_vector.addElement(vwEmailOptionsInfo);
		}
	}

	public RouteMasterInfo getDocument(int rowNum) {
		return (RouteMasterInfo)m_vector.elementAt(rowNum);
	}
	public Document getDocumentObject(int rowNum) {
		Document doc = new Document(((RouteMasterInfo)m_vector.elementAt(rowNum)).getDocId());    
		return doc;
	}

//	------------------------------------------------------------------------------
	public Vector getData() {
		int rowCount = getRowCount();
		Vector data = new Vector();
		//m_vector.removeAllElements();
		VWEmailOptionsInfo vwEmailOptionsInfo = null;
		for(int i=0; i<rowCount; i++){
			vwEmailOptionsInfo = new VWEmailOptionsInfo();
			vwEmailOptionsInfo = getRowData(i);
			data.add(vwEmailOptionsInfo);
		}
		return data;
		//return m_vector;
	}
//	------------------------------------------------------------------------------
	public VWEmailOptionsInfo getRowData(int rowNum) {

		try
		{
			return (VWEmailOptionsInfo)m_vector.elementAt(rowNum);
		}
		catch(Exception e)
		{
			return null;
		} 
	}

//	------------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size(); 
	}
//	------------------------------------------------------------------------------
	public int getColumnCount() { 
		return m_columns.length; 
	} 
//	------------------------------------------------------------------------------
	public String getColumnName(int column) { 
		String str = m_columns[column].m_title;
		if (column==m_sortCol)
			str += m_sortAsc ? " �" : " �";
		return str;
	}
//	------------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return ( (nCol == 0) ) ? true : false;
	}
//	------------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		VWEmailOptionsInfo row = (VWEmailOptionsInfo)m_vector.elementAt(nRow);
		switch (nCol) {
		case COL_USEREMAILADDRESS: return (row.getUserEmailAddress()!=null?row.getUserEmailAddress():"");
		case COL_DOCUMENTVWRICON: return new IconData(getDocumentVwrIcon(row) , "", Color.BLACK);
		case COL_DOCUMENTMETADATAICON: return new IconData(getDocumentMetaDataIcon(row) , "", Color.BLACK);
		case COL_ROUTESUMMARYREPORTICON: return new IconData(getRouteSummaryReporIcon(row) , "", Color.BLACK);
		case COL_DOCUMENTPAGESICON: return new IconData(getDocumentPagesIcon(row) , "", Color.BLACK);              
		}
		return "";
	}
	//-----------------------------------------------------------
	/****CV2019 merges - SIDBI - VWR enhancement changes***/
	public static ImageIcon getDocumentVwrIcon(VWEmailOptionsInfo row)
	{
		if(row.getDocumentVwrFlag()==1) {			
			return VWImages.ApproverActionYesIcon;
		} else
			return VWImages.ApproverActionNoIcon;
	}
	public static ImageIcon getDocumentMetaDataIcon(VWEmailOptionsInfo row)
	{
		if(row.getDocumentMetaDataFlag()==1)
			return VWImages.ApproverActionYesIcon;
		else
			return VWImages.ApproverActionNoIcon;
	}
	public static ImageIcon getRouteSummaryReporIcon(VWEmailOptionsInfo row)
	{
		if(row.getRouteSummaryReportFlag()==1)
			return VWImages.ApproverActionYesIcon;
		else
			return VWImages.ApproverActionNoIcon;
	}
	public static ImageIcon getDocumentPagesIcon(VWEmailOptionsInfo row){
		if(row.getDocumentPagesFlag()==1)
			return VWImages.ApproverActionYesIcon;
		else
			return VWImages.ApproverActionNoIcon;
	}

	/****CV2019 merges - SIDBI - VWR enhancement changes***/
	public void setValueAt(Object value, int nRow, int nCol){
		if (nRow < 0 || nRow >= getRowCount())
			return;
		
		VWEmailOptionsInfo row = (VWEmailOptionsInfo)m_vector.elementAt(nRow);
		String svalue = value.toString();
		switch (nCol) {
		case COL_USEREMAILADDRESS: 
			row.setUserEmailAddress(svalue.trim());
			break;
		case COL_DOCUMENTVWRICON:
			if(svalue.equalsIgnoreCase("true")) {				
				row.setDocumentVwrFlag(1);			
				row.setEditableFlag(false);
			} else if(svalue.equalsIgnoreCase("false")) {
				row.setDocumentVwrFlag(0);
				row.setEditableFlag(true);
			}
			break;
		case COL_DOCUMENTMETADATAICON:
			if(svalue.equalsIgnoreCase("true"))
				row.setDocumentMetaDataFlag(1);
			else if(svalue.equalsIgnoreCase("false"))
				row.setDocumentMetaDataFlag(0);
			break;
		case COL_ROUTESUMMARYREPORTICON:

			if(svalue.equalsIgnoreCase("true"))
				row.setRouteSummaryReportFlag(1);
			else if(svalue.equalsIgnoreCase("false"))
				row.setRouteSummaryReportFlag(0);
			break;
		case COL_DOCUMENTPAGESICON:

			if(svalue.equalsIgnoreCase("true"))
				row.setDocumentPagesFlag(1);
			else if(svalue.equalsIgnoreCase("false"))
				row.setDocumentPagesFlag(0);
			break;
		}
		//VWEmailOptionsInfo rowUpdated = (VWEmailOptionsInfo)m_vector.elementAt(nRow);
	}
//	----------------------------------------------------------------------------
	public void insert(int row,VWEmailOptionsInfo vwEmailOptionsInfo){
		if (row < 0)
			row = 0;
		if (row > m_vector.size())
			row = m_vector.size();
		m_vector.insertElementAt(vwEmailOptionsInfo, row);
	}
	//----------------------------------------------------------------------------
	public void insert(VWEmailOptionsInfo rowData) {
		int count=m_vector.size();
		boolean found=false;
		if(!found) m_vector.addElement(rowData);
	}
//	----------------------------------------------------------------------------
	public boolean delete(int row) {
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
	//----------------------------------------------------------------------------
	public void clear(){
		m_vector.removeAllElements();
	}
//	------------------------------------------------------------------------------
	class ColumnListener extends MouseAdapter
	{
		protected VWEmailOptionsListTable m_table;
//		------------------------------------------------------------------------------
		public ColumnListener(VWEmailOptionsListTable table){
			m_table = table;
		}
//		------------------------------------------------------------------------------
		public void mouseClicked(MouseEvent e){

			if(e.getModifiers()==e.BUTTON1_MASK)
				sortCol(e);
		}
//		------------------------------------------------------------------------------
		private void sortCol(MouseEvent e)
		{
			TableColumnModel colModel = m_table.getColumnModel();
			int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
			int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

			if (modelIndex < 0)
				return;
			if (m_sortCol==modelIndex)
				m_sortAsc = !m_sortAsc;
			else
				m_sortCol = modelIndex;

			for (int i=0; i < colModel.getColumnCount();i++) {
				TableColumn column = colModel.getColumn(i);
				column.setHeaderValue(getColumnName(column.getModelIndex()));    
			}
			m_table.getTableHeader().repaint();  

			m_table.tableChanged(
					new TableModelEvent(VWEmailOptionsListTableData.this)); 
			m_table.repaint();  
		}
	}
}