package ViewWise.AdminWise.VWDocLock;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;

import java.util.StringTokenizer;
import  java.util.Vector;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWPrint;

public class VWDocLockPanel extends JPanel implements  VWConstant {
    public VWDocLockPanel() {
    }
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWDocLock.setLayout(null);
        add(VWDocLock,BorderLayout.CENTER);
        VWDocLock.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWDocLock.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);
*/
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnUnLock.setText(BTN_UNLOCK_NAME);
        BtnUnLock.setActionCommand(BTN_UNLOCK_NAME);
        PanelCommand.add(BtnUnLock);
//      BtnUnLock.setBackground(java.awt.Color.white);
        BtnUnLock.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnUnLock.setIcon(VWImages.DocLockIcon);
        
        top += hGap;
        BtnUnLockAll.setText(BTN_UNLOCKALL_NAME);
        BtnUnLockAll.setActionCommand(BTN_UNLOCKALL_NAME);
        PanelCommand.add(BtnUnLockAll);
//      BtnUnLockAll.setBackground(java.awt.Color.white);
        BtnUnLockAll.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnUnLockAll.setIcon(VWImages.DocLockIcon);
        
        top += hGap + 12;
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        PanelCommand.add(BtnRefresh);
//      BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        
        top += hGap + 12;
        BtnSaveAs.setText(BTN_SAVEAS_NAME);
        BtnSaveAs.setActionCommand(BTN_SAVEAS_NAME);
        PanelCommand.add(BtnSaveAs);
//      BtnSaveAs.setBackground(java.awt.Color.white);
        BtnSaveAs.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSaveAs.setIcon(VWImages.SaveAsIcon);
        
        top += hGap;
        BtnPrint.setText(BTN_PRINT_NAME);
        BtnPrint.setActionCommand(BTN_PRINT_NAME);
        PanelCommand.add(BtnPrint);
//      BtnPrint.setBackground(java.awt.Color.white);
        BtnPrint.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnPrint.setIcon(VWImages.PrintIcon);
        
        top += hGap;
        PanelCommand.add(progress);
        progress.setBounds(12,top,144, AdminWise.gSmallBtnHeight);
        progress.setMinimum(0);
        progress.setVisible(false);
        
        PanelTable.setLayout(new BorderLayout());
        VWDocLock.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWDocLock.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.DocLockImage);
        SymAction lSymAction = new SymAction();
        BtnUnLock.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        BtnSaveAs.addActionListener(lSymAction);
        BtnUnLockAll.addActionListener(lSymAction);
        BtnPrint.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnUnLock)
                BtnUnLock_actionPerformed(event);
            else if (object == BtnUnLockAll)
                BtnUnLockAll_actionPerformed(event);
            else if (object == BtnRefresh)
                BtnRefresh_actionPerformed(event);
            else if (object == BtnSaveAs)
                BtnSaveAs_actionPerformed(event);
            else if (object == BtnPrint)
                BtnPrint_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWDocLock = new javax.swing.JPanel();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_DOCLOCK_NAME, VWImages.DocLockImage);
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnUnLock = new VWLinkedButton(1);
    VWLinkedButton BtnUnLockAll = new VWLinkedButton(1);
    VWLinkedButton BtnRefresh = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWDocLockTable Table = new VWDocLockTable();
    javax.swing.JProgressBar progress = new javax.swing.JProgressBar();
    //--------------------------------------------------------------------------
    
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWDocLock)
                VWDocLock_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWDocLock_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWDocLock) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        try{
            Table.addData(VWDocLockConnector.getDocLock(newRoom));
        }
        catch(Exception e){};
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWSaveAsHtml.saveArrayInFile(Table.getReportData(),LockColNames,LockColWidths,
            null,(java.awt.Component)this,AdminWise.connectorManager.getString("docLockPanel.LockedDocs"));
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    void BtnUnLock_actionPerformed(java.awt.event.ActionEvent event) {
        if(AdminWise.adminPanel.confirmUnlockDocument)
            if(VWMessage.showWarningDialog((java.awt.Component) this,
            WRN_UNLOCKDOC_1+Table.getRowDocName(Table.getSelectedRow())+
            WRN_UNLOCKDOC_2+" "+AdminWise.adminPanel.roomPanel.getSelectedRoom().getName()+ "?"
            )!=javax.swing.JOptionPane.YES_OPTION) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            unLockDocument(Table.getSelectedRow());
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        if(Table.getRowCount()>0) {
            Table.setRowSelectionInterval(0,0);
            setEnableMode(MODE_SELECT);
        }
        else {
            setEnableMode(MODE_UNSELECTED);
        }
    }
    //--------------------------------------------------------------------------
    void BtnUnLockAll_actionPerformed(java.awt.event.ActionEvent event) {
        if(AdminWise.adminPanel.confirmUnlockDocument)
            if(VWMessage.showWarningDialog((java.awt.Component) this,
            WRN_UNLOCKDOCS+
            AdminWise.adminPanel.roomPanel.getSelectedRoom().getName()+ " ?")!=
            javax.swing.JOptionPane.YES_OPTION) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            int i=0;
            int count =Table.getRowCount();
            viewProgress(count,"Unloak");
            while (i<Table.getRowCount()) {
                if(!unLockDocument(i)) i++;
                setProgressValue(i,"Purge");
            }
            if(Table.getRowCount()>0) {
                Table.setRowSelectionInterval(0,0);
                setEnableMode(MODE_SELECT);
            }
            else {
                setEnableMode(MODE_UNSELECTED);
            }
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
            hideProgress();
        }
    }
    //--------------------------------------------------------------------------
    /**
     * Method modified to disable unlock for folder level and 
     * enable for document level for create ownership
     * @param row
     * @return
     */
    private boolean unLockDocument(int row) {
    	if(Table.getRowType(row) == 1 || Table.getRowType(row) == 2)  return false;
    	try {
    		/*Vector result=VWDocLockConnector.getcheckOwnership(Table.getRowDocLockId(row));
    		String str = "";
    		int lockType1 =6;
    		int lockType2 =6;
    		String username="";
    		String type="";
    		if(result!=null && result.size()>0)
    			str = (String)result.get(0);
    		StringTokenizer st = new StringTokenizer(str, "\t");
    		if(st.hasMoreTokens())
    		{
    			lockType1 = Integer.parseInt(st.nextToken());
    			lockType2 = Integer.parseInt(st.nextToken());
    			username=st.nextToken();
        		type=st.nextToken();
    			if(type.equalsIgnoreCase("Folder")) return false;
    		}*/      	
    		VWDocLockConnector.unlockDoc(Table.getRowDocLockId(row));
    		Table.removeData(row);
    		return true;
    	}
    	catch(Exception e){}
    	return false;
    }
    //--------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event) {
        Table.clearData();
        Table.addData(VWDocLockConnector.getDocLock());
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event) {
        new VWPrint(Table);
    }
    //--------------------------------------------------------------------------
    /**
     * Added to check the folder level or document level for Take owner ship
     * @returnthis method is not used.
     */
    public String getDocDetails(){
    	int row=Table.getSelectedRow();
    	Vector resulttype=VWDocLockConnector.getcheckOwnership(Table.getRowDocLockId(row));
    	String str = "";
    	String username="";
    	String Type="";
    	int lockType1 =6;
    	int lockType2 =6;
    	if(resulttype!=null && resulttype.size()>0)
    		str = (String)resulttype.get(0);
    	StringTokenizer st = new StringTokenizer(str, "\t");
    	if(st.hasMoreTokens())
    	{
    		lockType1 = Integer.parseInt(st.nextToken());
    		lockType2 = Integer.parseInt(st.nextToken());
    		username=st.nextToken();
    		Type=st.nextToken();
    	}
    	return Type;      
    }
    
  //--------------------------------------------------------------------------
    
    public void setEnableMode(int mode) {
        BtnUnLock.setEnabled(true);
        BtnRefresh.setEnabled(true);
        BtnSaveAs.setEnabled(true);
        BtnPrint.setEnabled(true);
        BtnUnLockAll.setEnabled(true);
        switch (mode) {
            case MODE_UNSELECTED:
                BtnUnLock.setEnabled(false);
                if(Table.getRowCount()==0) {
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                    BtnUnLockAll.setEnabled(false);
                }
                break;
            case MODE_SELECT:
            	if ((Table.getRowType() == 1 || Table.getRowType() == 2))
            		//|| getDocDetails().equalsIgnoreCase("Folder")
            		BtnUnLock.setEnabled(false);
            	break;
            case MODE_UNCONNECT:
                BtnUnLock.setEnabled(false);
                BtnRefresh.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                BtnUnLockAll.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
    //--------------------------------------------------------------------------
    private void viewProgress(int max,String action) {
        progress.setMaximum(max);
        progress.setVisible(true);
        progress.setString(action+" [1] from ["+max+"]");
    }
    //--------------------------------------------------------------------------
    private void hideProgress() {
        progress.setVisible(false);
    }
    //--------------------------------------------------------------------------
    private void setProgressValue(int value,String action) {
        progress.setValue(value);
        progress.setString(action+" ["+value+"] from ["+progress.getMaximum()+"]");
        progress.paintImmediately(progress.getVisibleRect());
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    private boolean UILoaded=false;
}
