/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWDocLock;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;

import com.computhink.common.util.VWRefCreator;

import java.awt.Dimension;
import java.util.List;
import java.util.prefs.Preferences;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//--------------------------------------------------------------------------
public class VWDocLockTable extends JTable implements VWConstant {
    protected VWDocLockTableData m_data;
    public VWDocLockTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWDocLockTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ///setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWDocLockTableData.m_columns.length; k++) {
            TableCellRenderer renderer;
            
            DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWDocLockTableData.m_columns[k].m_alignment);
            renderer = textRenderer;
            VWDocLockRowEditor editor=new VWDocLockRowEditor(this);
            TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWDocLockTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                    AdminWise.adminPanel.docLockPanel.setEnableMode(MODE_SELECT);
                else
                    AdminWise.adminPanel.docLockPanel.setEnableMode(MODE_UNSELECTED);
            }
        });
        
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        
        setRowHeight(TableRowHeight);
        
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);      
        getColumnModel().getColumn(0).setPreferredWidth(194);
        getColumnModel().getColumn(1).setPreferredWidth(194);
    	getColumnModel().getColumn(2).setPreferredWidth(194);
    	getColumnModel().getColumn(3).setPreferredWidth(194);
    	getColumnModel().getColumn(4).setPreferredWidth(194);
        setTableResizable();
    }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWDocLockTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWDocLockTable_LeftMouseClicked(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWDocLockTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    void VWDocLockTable_RightMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;
         
        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
         */
    }
    //--------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
	loadDocumentInDTC(row);
    }
    private void loadDocumentInDTC(int row)
    {
    	int docId = this.getRowDocLockId(row);
        VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        String room = vwroom.getName();
    	try {      
            //create a temp folder to place reference file in it
        	File folder = new File("c:\\VWReferenceFile\\");
            folder.mkdirs();
            //create a reference file to open document
            File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);
            
            //open reference file in ViewWise thus opening doc                        
            try{
                Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
                //String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
                String dtcPath = VWUtil.getHome();
                Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
            }catch(Exception ex){
            	ex.printStackTrace();
            	//JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
            }
        }
        catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    }
    //--------------------------------------------------------------------------
    public void addData(List list) {
        m_data.setData(list);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void addData(String[][] data) {
        m_data.setData(data);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public String[][] getData() {
        return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public String[][] getReportData() {
        String[][] tableData=m_data.getData();
        int count=tableData.length;
        String[][] repData=new String[count][5];
        for(int i=0;i<count;i++) {
            repData[i][0]=tableData[i][1];
            repData[i][1]=tableData[i][2];
            repData[i][2]=tableData[i][3];
            repData[i][3]=tableData[i][4];
            repData[i][4]=tableData[i][5];
        }
        return repData;
    }
    //--------------------------------------------------------------------------
    public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    
    public void insertData(String[][] data) {
        for(int i=0;i<data.length;i++) {
            m_data.insert(new DocLockRowData(data[i]));
        }
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public String[] getRowData(int rowNum) {
        return m_data.getRowData(rowNum);
    }
    //--------------------------------------------------------------------------
    public int getRowDocLockId(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number((row[0]));
    }
    //--------------------------------------------------------------------------
    public String getRowDocName(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return row[1];
    }
    //--------------------------------------------------------------------------
    public int getRowType() {
        return getRowType(getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public int getRowType(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number((row[6]));
    }
    //--------------------------------------------------------------------------
    public void clearData() {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
}
class DocLockRowData {
    public int      m_DocLockId;
    public String   m_DocName;
    public String   m_UserName;
    public String   m_Client;
    public String   m_Time;
    public String   m_Location;
    public String   m_Type;
    public DocLockRowData() {
        m_DocLockId=0;
        m_DocName="";
        m_UserName="";
        m_Client="";
        m_Time="";
        m_Location="";
        m_Type="0";
    }
    //--------------------------------------------------------------------------------
    public DocLockRowData(int docLockId,String docLockName,String docLockUserName,
    String docLockClient,String docLockTime,String location,String type) {
        m_DocLockId=docLockId;
        m_DocName=docLockName;
        m_UserName=docLockUserName;
        m_Client=docLockClient;
        m_Time=docLockTime;
        m_Location=location;
        ///m_Location=VWUtil.fixNodePath(location);
        m_Type=type;
    }
    //--------------------------------------------------------------------------------
    public DocLockRowData(String str) {
        VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
        m_DocLockId=VWUtil.to_Number(tokens.nextToken());
        m_DocName=VWUtil.getFixedValue(tokens.nextToken(),AdminWise.connectorManager.getString("docLockTabel.DocLockRowData0"));
        m_UserName=VWUtil.getFixedValue(tokens.nextToken(),AdminWise.connectorManager.getString("docLockTabel.DocLockRowData1"));
        m_Client=VWUtil.getFixedValue(tokens.nextToken(),AdminWise.connectorManager.getString("docLockTabel.DocLockRowData2"));
        m_Time=VWUtil.getFixedValue(tokens.nextToken(),AdminWise.connectorManager.getString("docLockTabel.DocLockRowData3"));
        m_Location=VWUtil.getFixedValue(tokens.nextToken(),AdminWise.connectorManager.getString("docLockTabel.DocLockRowData4"));
        ///m_Location=VWUtil.fixNodePath(VWUtil.getFixedValue(tokens.nextToken(),"<Unknown location>"));
        m_Type=VWUtil.getFixedValue(tokens.nextToken(),"0");
    }
    //--------------------------------------------------------------------------------
    public DocLockRowData(String[] data) {
        int i=0;
        m_DocLockId=VWUtil.to_Number(data[i++]);
        m_DocName=data[i++];
        m_UserName=data[i++];
        m_Client=data[i++];
        m_Time=data[i++];
        m_Location=data[i++];
        ///m_Location=VWUtil.fixNodePath(data[i++]);
        m_Type=data[i];;
    }
}
//--------------------------------------------------------------------------
class DocLockColumnData {
    public String  m_title;
    float m_width;
    int m_alignment;
    
    public DocLockColumnData(String title, float width, int alignment) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
    }
}
//--------------------------------------------------------------------------
class VWDocLockTableData extends AbstractTableModel {
    public static final DocLockColumnData m_columns[] = {
        new DocLockColumnData(VWConstant.LockColNames[0],0.3F,JLabel.LEFT ),
        new DocLockColumnData(VWConstant.LockColNames[1],0.1F, JLabel.LEFT),
        new DocLockColumnData(VWConstant.LockColNames[2],0.1F,JLabel.LEFT),
        new DocLockColumnData(VWConstant.LockColNames[3],0.2F,JLabel.LEFT),
        new DocLockColumnData(VWConstant.LockColNames[4],0.2F,JLabel.LEFT)
    };
    public static final int COL_DOCNAME = 0;
    public static final int COL_USERNAME = 1;
    public static final int COL_Client = 2;
    public static final int COL_TIME = 3;
    public static final int COL_LOCATION = 4;
    
    protected VWDocLockTable m_parent;
    protected Vector m_vector;
    
    protected int m_sortCol = 0;
    protected boolean m_sortAsc = true;
    
    public VWDocLockTableData(VWDocLockTable parent) {
        m_parent = parent;
        m_vector = new Vector();
    }
    //--------------------------------------------------------------------------
    public void setData(List data) {
        m_vector.removeAllElements();
        if (data==null) return;
        int count =data.size();
        for(int i=0;i<count;i++)
            m_vector.addElement(new DocLockRowData((String)data.get(i)));
    }
    //--------------------------------------------------------------------------
    public void setData(String[][] data) {
        m_vector.removeAllElements();
        int count =data.length;
        for(int i=0;i<count;i++) {
            DocLockRowData row =new DocLockRowData(data[i]);
            if(i==2)
                m_vector.addElement(new Boolean(true));
            else
                m_vector.addElement(row);
        }
    }
    //--------------------------------------------------------------------------
    public String[][] getData() {
        int count=getRowCount();
        String[][] data=new String[count][7];
        for(int i=0;i<count;i++) {
            DocLockRowData row=(DocLockRowData)m_vector.elementAt(i);
            data[i][0]=String.valueOf(row.m_DocLockId);
            data[i][1]=row.m_DocName;
            data[i][2]=row.m_UserName;
            data[i][3]=row.m_Client;
            data[i][4]=row.m_Time;
            data[i][5]=row.m_Location;
            data[i][6]=row.m_Type;
        }
        return data;
    }
    //--------------------------------------------------------------------------
    public String[] getRowData(int rowNum) {
        String[] DocLockRowData=new String[7];
        DocLockRowData row=(DocLockRowData)m_vector.elementAt(rowNum);
        DocLockRowData[0]=String.valueOf(row.m_DocLockId);
        DocLockRowData[1]=row.m_DocName;
        DocLockRowData[2]=row.m_UserName;
        DocLockRowData[3]=row.m_Client;
        DocLockRowData[4]=row.m_Time;
        DocLockRowData[5]=row.m_Location;
        DocLockRowData[6]=row.m_Type;
        return DocLockRowData;
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return m_vector==null ? 0 : m_vector.size();
    }
    //--------------------------------------------------------------------------
    public int getColumnCount() {
        return m_columns.length;
    }
    //--------------------------------------------------------------------------
    public String getColumnName(int column) {
    	String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
        return str;
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
        return true;
    }
    //--------------------------------------------------------------------------
    public Object getValueAt(int nRow, int nCol) {
        if (nRow < 0 || nRow>=getRowCount())
            return "";
        DocLockRowData row = (DocLockRowData)m_vector.elementAt(nRow);
        switch (nCol) {
            case COL_DOCNAME:     return row.m_DocName;
            case COL_USERNAME:    return row.m_UserName;
            case COL_Client:      return row.m_Client;
            case COL_TIME:        return row.m_Time;
            case COL_LOCATION:    return row.m_Location;
        }
        return "";
    }
    //--------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
        if (nRow < 0 || nRow >= getRowCount())
            return;
        DocLockRowData row = (DocLockRowData)m_vector.elementAt(nRow);
        String svalue = value.toString();
        
        switch (nCol) {
            case COL_DOCNAME:
                row.m_DocName = svalue;
                break;
            case COL_USERNAME:
                row.m_UserName = svalue;
                break;
            case COL_Client:
                row.m_Client = svalue;
                break;
            case COL_TIME:
                row.m_Time= svalue;
                break;
            case COL_LOCATION:
                row.m_Location= svalue;
                break;
        }
    }
    //--------------------------------------------------------------------------
    public void clear() {
        m_vector.removeAllElements();
    }
    //--------------------------------------------------------------------------
    public void insert(DocLockRowData AdvanceSearchRowData) {
        m_vector.addElement(AdvanceSearchRowData);
    }
    //--------------------------------------------------------------------------
    public boolean remove(int row){
        if (row < 0 || row >= m_vector.size())
            return false;
        m_vector.remove(row);
        return true;
    }
    //--------------------------------------------------------------------------
    class DocLockComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public DocLockComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof DocLockRowData) || !(o2 instanceof DocLockRowData))
                return 0;
            DocLockRowData s1 = (DocLockRowData)o1;
            DocLockRowData s2 = (DocLockRowData)o2;
            int result = 0;
            double d1, d2;
            switch (m_sortCol) {
            	case COL_DOCNAME:
                    result = s1.m_DocName.toLowerCase().compareTo(s2.m_DocName.toLowerCase());
                    break;
                case COL_USERNAME:
                    result = s1.m_UserName.toLowerCase().compareTo(s2.m_UserName.toLowerCase());
                   break;            	
                case COL_Client:
                	result = s1.m_Client.toLowerCase().compareTo(s2.m_Client.toLowerCase());  
                    break;
                case COL_TIME:
                	result = s1.m_Time.toLowerCase().compareTo(s2.m_Time.toLowerCase());  
                   break;
                case COL_LOCATION:
                	result = s1.m_Location.toLowerCase().compareTo(s2.m_Location.toLowerCase());  
                   break;
                   
            }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof DocLockComparator) {
            	DocLockComparator compObj = (DocLockComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    } 
    
    class ColumnListener extends MouseAdapter {
        protected VWDocLockTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWDocLockTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
            
   /*          if(e.getModifiers()==e.BUTTON3_MASK)
                selectViewCol(e);
            else */if(e.getModifiers()==e.BUTTON1_MASK)
                sortCol(e);
        }
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {    	 
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new DocLockComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWDocLockTableData.this));
            m_table.repaint();
        }
   }    
}