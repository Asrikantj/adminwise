/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWDocLockConnector<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWDocLock;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import com.computhink.common.Document;
import com.computhink.common.Node;

//------------------------------------------------------------------------------
public class VWDocLockConnector implements VWConstant
{
    public static List getDocLock()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            return getDocLock(room);
        return null;
    }
//------------------------------------------------------------------------------
    public static Vector getDocLock(VWRoom room)
    {
        Vector data=new Vector();
        AdminWise.gConnector.ViewWiseClient.getLockDocList(room.getId(),data);
        return data;
    }
//------------------------------------------------------------------------------
    public static Vector getcheckOwnership(int docId)
    {
    	Vector nodes=new Vector();
    	Vector result=new Vector();
    	Node node=new Node(docId);
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	nodes.add(node);
    	AdminWise.gConnector.ViewWiseClient.checkOwnership(room.getId(),1,nodes,result);
    	return result;    	
    }    
  //------------------------------------------------------------------------------
    public static void unlockDoc(int docId)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
        {
            AdminWise.gConnector.ViewWiseClient.unlockDocument(room.getId(), 
                new Document(docId),1);
        }
    }
//------------------------------------------------------------------------------
}
