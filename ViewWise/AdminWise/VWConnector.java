/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWConnector<br>
 *
 * @version     $Revision: 1.9 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise;
//import com.computhink.vwc.VWClient;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.SwingUtilities;
import ViewWise.AdminWise.VWLogin.VWLoginScreen;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.VWEndIconProperties;
import ViewWise.AdminWise.VWRoute.VWStartIconProperties;
import ViewWise.AdminWise.VWRoute.VWTaskIconProperties;
import ViewWise.AdminWise.VWTemplate.VWTemplateConnector;
import ViewWise.AdminWise.VWUtil.VWInfoFile;
import ViewWise.AdminWise.VWUtil.VWProperties;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.common.Constants;
import com.computhink.common.VWEvent;
import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWAdmin;
import com.computhink.vwc.VWClientListener;
import com.computhink.vws.license.Base64Coder;
//------------------------------------------------------------------------------
public class VWConnector implements VWConstant, VWClientListener,Serializable
// Fix End
{
    private native String WTSgetUserName();
    private native String WTSgetClientIPAddress();
    private native String WTSgetClientName();
    private native String WTSgetDomainGroupName();
    public VWConnector()
    {       
        /*
        if(AdminWise.getCurrentMode()==APPLET_MODE)
        {
            setViewWiseRooms();
            ViewWiseClient = new VWClient(VWProperties.getCachDirPath(),
                Client.Admin_Client_Type);
        }
        else
        {
        */
        ///ViewWiseClient = new VWClient(VWProperties.getCachDirPath(),Client.Adm_Client_Type);
        ViewWiseClient = new VWAdmin(VWProperties.getCachDirPath());
        // Fix Start 485
        if (ViewWiseClient != null) 
        {
        		int retVal = ViewWiseClient.addListener(this);
        }
    	//Fix End
        /*}*/
    }
//------------------------------------------------------------------------------
    public Hashtable getViewWiseRooms()
    {
        Hashtable roomTable=null;
        String strCodeBase="";
        try{strCodeBase=VWUtil.getCodeBase().toString();}
        catch(Exception e){strCodeBase="http://127.0.0.1/VWOnline/";}
        roomTable= VWInfoFile.getURLProfileStrings("Rooms",strCodeBase+"Properties"+"/"+RoomFileName);
        if(roomTable==null||roomTable.size()==0)
        {
            ///VWMessage.showInitMsg(VWMessage.ERR_ROOMFILES_NOTFOUND);
            VWMessage.showMessage(null,VWMessage.ERR_ROOMFILES_NOTFOUND,VWMessage.DIALOG_TYPE);
            ///cleanup();
            System.exit(0);
        }
        return roomTable;
    }
//------------------------------------------------------------------------------
    private String[][] setViewWiseRooms(Hashtable roomTable)
    {
        if(roomTable==null && roomTable.size()==0) return null;
        int count =roomTable.size();
        String[][] VWParm=new String[count][2];
        int i =0;
        Enumeration e = roomTable.keys();
        while(e.hasMoreElements())
        {
            String str=(String)e.nextElement();
            VWParm[i][0]=str;
            VWParm[i][1]=(String)roomTable.get(str);
            i++;
        }
        return VWParm;
    }
//------------------------------------------------------------------------------
    public int connectRoom(VWRoom room)
    {
        if(ViewWiseClient.isHacker(room.getServer(), room.getName())==1)
        {
            VWMessage.showNoticeDialog(VWMessage.ERR_LOGIN_EXCEEDED);
            return Room_Status_Down;
        }
        String[] retValues=securityInfo;
        AdminWise.adminPanel.setWaitPointer();
        int loginRetVal =0;
        try
        {
            ///AdminWise.adminPanel.roomPanel.setRoomToolTip(null,null);
            if(securityInfo[2].equals("0") || securityInfo[0].equals(""))
            {
                retValues=displayLoginScreen(retValues,room.getName());
                if(retValues==null || retValues.length==0) return Room_Status_UnConnect;
                securityInfo[0]=retValues[0];
                securityInfo[1]=retValues[1];
                securityInfo[2]=retValues[2];
                //All Room option is not implemented.
                //So, When All room selected - Login dialog will come. Valli  24 Nov 2007
            }else if(silentLogin){
            	securityInfo[2] = "-1"; // by assigning securityInfo[2] to -1 it will not launch login dialog.
            }else if(securityInfo[2].equals("1") ){
            	retValues=displayLoginScreen(retValues,room.getName());
                if(retValues==null || retValues.length==0) return Room_Status_UnConnect;
                securityInfo[0]=retValues[0];
                securityInfo[1]=retValues[1];
                securityInfo[2]=retValues[2];
            }
            /*CV2019 Merges from SIDBI line****************************/
            String userName = Base64Coder.encode(securityInfo[0]);
    	    String password = Base64Coder.encode(securityInfo[1]);
    	    /*End of SIDBI line merges*********************************/
            loginRetVal = ViewWiseClient.login(room.getServer(), 
                    room.getName(), userName, password);
            silentLogin = false;
            if(loginRetVal<0)
            {
                if(loginRetVal==ViewWiseErrors.checkUserError)
                {
                    VWMessage.showMessage(null,ViewWiseClient.getErrorDescription
                        (ViewWiseErrors.checkUserError),VWMessage.DIALOG_TYPE);
                    securityInfo[2]="0";
                    return Room_Status_Down;
                }
                else if(loginRetVal==ViewWiseErrors.ServerNotFound)
                {
                    VWMessage.showMessage(null,ViewWiseClient.getErrorDescription(
                        ViewWiseErrors.ServerNotFound),VWMessage.DIALOG_TYPE);
                    return Room_Status_Down;
                }
                else if(loginRetVal==ViewWiseErrors.NoMoreLicenseSeats)
                {
                    VWMessage.showMessage(null,ViewWiseClient.getErrorDescription(
                    ViewWiseErrors.NoMoreLicenseSeats),VWMessage.DIALOG_TYPE);
                    return Room_Status_UnConnect;
                }
                else if(loginRetVal==ViewWiseErrors.NotAllowedConnections) 
                {
                    VWMessage.showNoticeDialog(AdminWise.connectorManager.getString("vwconnector.msg1")+
                        NewLineChar+AdminWise.connectorManager.getString("vwconnector.msg2")+" " + AdminWise.connectorManager.getString("Product.Name")+" "+ AdminWise.connectorManager.getString("vwconnector.msg3")+
                        NewLineChar+AdminWise.connectorManager.getString("vwconnector.msg4"));
                    return Room_Status_UnConnect;
                }
                else if(loginRetVal==ViewWiseErrors.accessDenied)
                {
                    VWMessage.showMessage(null,ViewWiseClient.getErrorDescription
                        (ViewWiseErrors.accessDenied),VWMessage.DIALOG_TYPE);
                    return Room_Status_UnConnect;
                }
                else if(loginRetVal==ViewWiseErrors.LoginCountExceeded) {
                    VWMessage.showNoticeDialog(ViewWiseClient.getErrorDescription
                    (ViewWiseErrors.LoginCountExceeded));
                    return Room_Status_UnConnect;
                    
                	/*	Enhancement No:66-ViewWise Client-Server Synchronization step on launch to confirm the client is up-to-date (e.g. updated jars).
                	 *	Created by: <Shanmugavalli.C>	Date: <04 Aug 2006>
                	 *	New error code added for enh #66 */  
                }else if(loginRetVal==ViewWiseErrors.JarFileMisMatchErr){
                	VWMessage.showMessage(null,ViewWiseClient.getErrorDescription
                            (ViewWiseErrors.JarFileMisMatchErr),VWMessage.DIALOG_TYPE);
                        return Room_Status_UnConnect;
	            }
                else if(loginRetVal==ViewWiseErrors.UserDoesNotExistINDB) {
	            	VWMessage.showMessage(null,ViewWiseClient.getErrorDescription
                            (ViewWiseErrors.UserDoesNotExistINDB),VWMessage.DIALOG_TYPE);
                        return Room_Status_UnConnect;
	            }else if(loginRetVal==ViewWiseErrors.invalidPassword) {
	            	VWMessage.showMessage(null,ViewWiseClient.getErrorDescription
                            (ViewWiseErrors.invalidPassword),VWMessage.DIALOG_TYPE);
                        return Room_Status_UnConnect;
	            }else if(loginRetVal==ViewWiseErrors.NoSubAdminRolesFound) {
	            	VWMessage.showMessage(null,ViewWiseClient.getErrorDescription
                            (ViewWiseErrors.NoSubAdminRolesFound),VWMessage.DIALOG_TYPE);
                        return Room_Status_UnConnect;
	            }
                else{
                    VWMessage.showMessage(null,VWMessage.ERR_ROOM_DOWN,VWMessage.DIALOG_TYPE);
                    return Room_Status_Down;
                }
            }
        }
        catch(Exception e){}
        finally
        {
            AdminWise.adminPanel.setDefaultPointer();
        }
        VWTemplateConnector.checkTemplateFiles();
        ///AdminWise.adminPanel.roomPanel.setRoomToolTip(securityInfo[0],ViewWiseClient.getClientHost());
        /*
        Document doc = new Document(62);
            System.out.println("Rcv: " + ViewWiseClient.getDocFolder
                (loginRetVal, doc,"c:\\tmp"));
         */
        
        AdminWise.listTable.put(room.getName(),new AdminClientRoom(room));      
        return loginRetVal;
    }
//------------------------------------------------------------------------------
    private String[] displayLoginScreen(String[] initValues,String roomName)
    {
        String [] retValues=null;
        VWLoginScreen login= new VWLoginScreen(AdminWise.adminFrame,"Login to "+roomName,initValues);
        login.requestFocus();
        if(!login.getCancelFlag()) retValues=login.getValues();
        login.dispose();
        login=null;
        return retValues;
    }    
//------------------------------------------------------------------------------
    public static Vector DoAction(String action,String param,int serverId)
    {   
        Vector ret=new Vector();
        ///System.out.println("Action ->"+action);
        ///System.out.println("Param ->"+param);
        ///System.out.println("ServerId ->"+serverId);
        ViewWiseClient.adminWise(serverId,action,param,ret);
        //System.out.println("Return Size ->"+ret.size());
        return ret;
    }
    //-----------------------------------------------------------
    public void disConnectRoom(int roomId)
    {
        ViewWiseClient.logout(roomId);
        AdminWise.adminPanel.roomPanel.setRoomToolTip(null,null);
    }
    //-----------------------------------------------------------
    public int silentLogin(String[] loginInfo)
    {

    	silentLogin = true;
        int retValue=0;
        AdminWise.adminPanel.roomPanel.setConnectEnable(false);
        try{
            VWRoom curRoom=AdminWise.adminPanel.roomPanel.setSelectedRoom(loginInfo[0]);
            if(curRoom==null) 
            {
                AdminWise.adminPanel.roomPanel.setConnectEnable(true);
                return 0;
            }
            securityInfo[0]=loginInfo[1];
            securityInfo[1]=loginInfo[2];
            securityInfo[2]="1";
            retValue=connectRoom(curRoom);
            silentLogin = false;
            if(retValue>0)
            {
                AdminWise.adminPanel.roomPanel.getSelectedRoom().setConnectStatus(Room_Status_Connect);
                AdminWise.adminPanel.roomPanel.getSelectedRoom().setId(retValue);
                AdminWise.adminPanel.roomPanel.connectActionUI(curRoom);
            }
            securityInfo[2]="0";
        }
        catch(Exception e){}; 
            AdminWise.adminPanel.roomPanel.setConnectEnable(true);
        return retValue;
    }
    //-----------------------------------------------------------
public static void setDocATData(int roomId,String nodeId,String eventId,String comment)
{
    ///ViewWiseClient.setDocATData(roomId,nodeId,eventId,comment);
}
    //-----------------------------------------------------------
public int logout()
{
    ///ViewWiseClient.shutdown();
    return 1;
}
    //-----------------------------------------------------------
public String getUserName(int roomId)
{
    Session session=ViewWiseClient.getSession(roomId);
    return session.user;
}
    //-----------------------------------------------------------
public String getClientIP()
{
    try
    {
        return java.net.InetAddress.getLocalHost().getHostAddress();
    }
     catch(java.net.UnknownHostException e)
     {}
    return "Local";
}
protected class IdleTimerAction implements java.awt.event.ActionListener
{
    public void actionPerformed(java.awt.event.ActionEvent e)
    {
        //int idleTime = roomIdleTimeOut;
        //lastActivateTime
        //Vector roomList = VWRoomConnector.getViewWiseRooms();
        
        //Hashtable roomTable =  VWRoomConnector.getVWRoom();
        Enumeration enumc = AdminWise.listTable.elements();
        //long now = System.currentTimeMillis();
        int roomId = 0;
        String roomName = "";
        int isIdle = -1;
        String server = "";
        //VWMessage.showMessage(null," listTable " + AdminWise.listTable.size());
        while (enumc.hasMoreElements())
        {
        	try{
        		AdminClientRoom vwConnectedRoom = (AdminClientRoom) enumc.nextElement();
        		VWRoom vwRoom = vwConnectedRoom.roomName;
            roomId = vwRoom.getId();
        	roomName = vwRoom.getName();
        	//VWMessage.showMessage(null," roomId /// roomName " + roomId + " /// " + roomName);
        	server = vwRoom.getServer();
        	isIdle = ViewWiseClient.isRoomIdle(roomId, roomName);
        	if(isIdle == VWEvent.IDLE_DISCONNECT){
        		VWEvent event = new VWEvent(server,roomName, VWEvent.IDLE_DISCONNECT,roomId);
        		AdminWise.gConnector.eventHandler(event);  
        		vwConnectedRoom.idleTimer.stop();
        		//AdminWise.listTable.remove(roomName);
        	}else if(isIdle == ViewWiseErrors.invalidSessionId){
        		VWEvent event = new VWEvent(server,roomName, ViewWiseErrors.invalidSessionId,roomId);
        		eventHandler(event);
        		vwConnectedRoom.idleTimer.stop();
        		//AdminWise.listTable.remove(roomName);
        	}else if(isIdle == ViewWiseErrors.destinationRoomServerInactive){
        		VWEvent event = new VWEvent(server,roomName, ViewWiseErrors.destinationRoomServerInactive,roomId);
        		eventHandler(event);
        		vwConnectedRoom.idleTimer.stop();
        		//AdminWise.listTable.remove(roomName);
        		
        	}else if(isIdle == ViewWiseErrors.DatabaseError){
				String msg =  AdminWise.connectorManager.getString("vwconnector.msg5");
				VWMessage.showMessage(AdminWise.adminFrame,msg,VWMessage.DIALOG_TYPE);				
        		//AdminWise.listTable.remove(roomName);
        	}
        	//VWMessage.showMessage(null,"Idle Information " + roomId + " - " + roomName + " - " + isIdle ,VWMessage.DIALOG_TYPE);
        	//VWMessage.showMessage(null,"Current Time in Millis - " + System.currentTimeMillis() ,VWMessage.DIALOG_TYPE);
        }catch(Exception ex){
        	
        }
        }
    }
 }

public class AdminClientRoom{
	public javax.swing.Timer idleTimer;
	private int idleTimeOut = 0;
	public VWRoom roomName = null;
	public AdminClientRoom(VWRoom roomName){
		this.roomName = roomName;
		//this.idleTimeOut = timeOut;
		//start();
		setIdleTimeout(1);
	}
    public void setIdleTimeout(int timeout)
    {
        if (timeout > 0)
        {
            if (idleTimer != null) idleTimer.stop();
            idleTimer = new javax.swing.Timer(timeout*500*60,
                    new IdleTimerAction());
            idleTimer.setRepeats(true);
            idleTimer.start();
        }
    }
    public void run(){
    	setIdleTimeout(1);
    }
}

	public void killOpenDRSdialogs(){
		try{
			VWStartIconProperties.active = false;
			VWTaskIconProperties.active = false;         
			VWEndIconProperties.active = false;         
		}catch(Exception e){
			//System.out.println("EXCEPTION in killOpenDRSdialogs:"+e.toString());
		}
	}

    //-----------------------------------------------------------
    public void eventHandler(VWEvent event) {
    	killOpenDRSdialogs();
        switch(event.getId()) {
            case VWEvent.ADMIN_DISCONNECT:
            	serverInactive = true;
            	endApp(event.getSessionId(), AdminWise.connectorManager.getString("vwconnector.msg1")+
                NewLineChar+AdminWise.connectorManager.getString("vwconnector.msg2")+" " + AdminWise.connectorManager.getString("Product.Name")+" "+ AdminWise.connectorManager.getString("vwconnector.msg3")+
                NewLineChar+AdminWise.connectorManager.getString("vwconnector.msg4"),event.getRoom());
            	serverInactive = false;
            	break;
            // Issue 485 - Change the alert message.
            case VWEvent.IDLE_DISCONNECT:
            	endApp(event.getSessionId(),AdminWise.connectorManager.getString("vwconnector.msg6")+ event.getServer()+"."+ event.getRoom() +AdminWise.connectorManager.getString("vwconnector.msg7"),event.getRoom());
            	break;
            case ViewWiseErrors.invalidSessionId:
            	endApp(event.getSessionId(), AdminWise.connectorManager.getString("vwconnector.msg6")+ event.getServer()+"."+ event.getRoom() +AdminWise.connectorManager.getString("vwconnector.msg7"),event.getRoom());
            	break;
            case ViewWiseErrors.destinationRoomServerInactive:
            	serverInactive = true;
            	endApp(event.getSessionId(),AdminWise.connectorManager.getString("vwconnector.msg6")+ event.getServer()+"."+ event.getRoom() +AdminWise.connectorManager.getString("vwconnector.msg8"),event.getRoom());
            	serverInactive = false;

              /*  endApp(event.getSessionId(), "This Room is Administratively Down."+
                NewLineChar+"Please contact your ViewWise Administrator"+
                NewLineChar+"before attempting to reconnect.");
              */
            // Fix End            
                break;
        }
    }
    //-----------------------------------------------------------
    private void endApp(int roomId, String msg,String roomName) {
// Fix Start Issue No : 481 , when abnormal disconnect adminwise show a connection details and not in disconnect status    	
    	final String tempMsg = msg;    	
    	final VWRoom selRoom = new VWRoom(roomId,roomName);    	
    	selRoom.setConnectStatus(Room_Status_Connect);
    	AdminWise.adminPanel.roomPanel.connectAction(selRoom);        	
    	AdminWise.listTable.remove(roomName);
    	 try {        	
        	AdminWise.listTable.remove(roomName);
        }catch(Exception ex){
        	
        }
        disConnectRoom(roomId); 
    	SwingUtilities.invokeLater(new Thread(){
    		public void run(){
    	        VWMessage.showMessage(AdminWise.adminFrame,tempMsg,VWMessage.DIALOG_TYPE);
    		}
    	});        
// Fix End        
    }
    //-----------------------------------------------------------
    public static boolean isServerMachine(){
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		return ViewWiseClient.isServerMachine(room.getServer());    		
		
	}
    //-----------------------------------------------------------
    public static boolean getHostedAdmin() {
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		return AdminWise.gConnector.ViewWiseClient.getHostedAdmin(room.getId());		
	}
    //-----------------------------------------------------------
    ///public static VWClient ViewWiseClient;
    public static VWAdmin ViewWiseClient;
    private static String[] securityInfo={"","","0"};
    public int  roomIdleTimeOut =0;
    static boolean needToLoad=true;
    boolean silentLogin = false;
    public static boolean serverInactive = false; 
    
//------------------------------------------------------------------------------ 
}