/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.4 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWConUsers;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;

import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import com.computhink.vws.server.Client;

import ViewWise.AdminWise.VWTable.*;
import ViewWise.AdminWise.VWUtil.VWUtil;
import com.computhink.vws.server.Client;
//--------------------------------------------------------------------------
public class VWConUserTable extends JTable implements VWConstant {
    protected VWConUserTableData m_data;
    public VWConUserTable(){
        super();
        m_data = new VWConUserTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        ///setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWConUserTableData.m_columns.length; k++) {
            TableCellRenderer renderer;
            
            ///DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
            ColoredTableCellRenderer textRenderer=new ColoredTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWConUserTableData.m_columns[k].m_alignment);
            renderer = textRenderer;
            VWConUserRowEditor editor=new VWConUserRowEditor(this);
            TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWConUserTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                    AdminWise.adminPanel.conUserPanel.setEnableMode(MODE_SELECT);
                else
                    AdminWise.adminPanel.conUserPanel.setEnableMode(MODE_UNSELECTED);
            }
        });
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        
        setRowHeight(TableRowHeight);
        
        setTableResizable();
    }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWConUserTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWConUserTable_LeftMouseClicked(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWConUserTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    void VWConUserTable_RightMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;
        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
         */
    }
    //--------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
    }
    //--------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
    }
    //--------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
    }
    //--------------------------------------------------------------------------
    public void addData(Vector list) {
        m_data.setData(list);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public String[][] getData() {
        return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public String[][] getReportData() {
        String[][] repData=m_data.getData();
        for(int i=0;i<repData.length;i++) {
            repData[i][2]=ClientType[VWUtil.to_Number(repData[i][2])];
            if(repData[i][5].equals("1"))
                repData[i][5]=VWConstant.YesNoData[0];
            else
                repData[i][5]=VWConstant.YesNoData[1];
        }
        return repData;
    }
    //--------------------------------------------------------------------------
    public boolean getIsCurClient(int row) {
        return m_data.getRowData(row)[6].equalsIgnoreCase("true");
    }
    //--------------------------------------------------------------------------
    public boolean getIsCurClient() {
        return getIsCurClient(getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public String[] getRowData(int rowNum) {
        return m_data.getRowData(rowNum);
    }
    //--------------------------------------------------------------------------
    public String getRowConClient(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return row[1];
    }
    //--------------------------------------------------------------------------
    public String getRowUserName(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return row[0];
    }
    //--------------------------------------------------------------------------
    public String getRowConUserType(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return row[2];
    }
    //--------------------------------------------------------------------------
    public int getRowClientId(int rowNum) {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[3]);
    }
    //Method added for MultiSelect Disconnect(27/11/2015)
    public Map getSelectRowClientIds(int[] selectedRows) {
    	Map row = null;
    	try {
    		row=m_data.getSelectedRowData(selectedRows);
    	} catch (Exception e) {
    	}
    	return row;
    }
    //--------------------------------------------------------------------------
    public void clearData() {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
}
class ConUserRowData {
    public String   m_UserName;
    public String   m_Client;
    public int   m_UserType;
    public boolean   m_UserAdmin;
    public boolean   m_CurClient;
    public int    m_ClientId;
    public String m_DateAndTime;
    public String m_UserTypeName;
    
    public ConUserRowData() {
        m_UserName="";
        m_Client="";
        m_UserType=0;
        m_UserAdmin=false;
        m_CurClient=false;
        m_ClientId=0;
        m_DateAndTime="";
        m_UserTypeName = "";
    }
    //--------------------------------------------------------------------------
    public ConUserRowData(Client client) {
    	try{
    		m_UserName=client.getUserName();
    		m_Client=client.getIpAddress();
    		m_UserType=client.getClientType();
    		m_UserAdmin=client.isAdmin();
    		m_CurClient=client.isCaller();
    		m_ClientId=client.getId();
    		m_DateAndTime = client.getConnectedDate();
    		m_UserTypeName = VWConstant.ClientType[m_UserType];
    	}catch (Exception e) {}
    }
}
//--------------------------------------------------------------------------
class ConUserColumnData {
    public String  m_title;
    float m_width;
    int m_alignment;
    
    public ConUserColumnData(String title, float width, int alignment) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
    }
}
//--------------------------------------------------------------------------
class VWConUserTableData extends AbstractTableModel {
    public static final ConUserColumnData m_columns[] = {
        new ConUserColumnData(VWConstant.ConUserColNames[0],0.4F,JLabel.LEFT ),
        new ConUserColumnData(VWConstant.ConUserColNames[1],0.3F, JLabel.LEFT),
        new ConUserColumnData(VWConstant.ConUserColNames[2],0.3F,JLabel.LEFT),
        
         /* Issue no - 672
     * Issue Description -  Add a column showing time and date connected be added to the connected user tab.
     * 						Add a column showing the session id of the connected user be added to the connected user tab.
     * Developer - Nebu Alex 
     */
        new ConUserColumnData(VWConstant.ConUserColNames[3],0.2F,JLabel.LEFT),
        new ConUserColumnData(VWConstant.ConUserColNames[4],0.2F,JLabel.LEFT)
        //End of fix 672

    };
    public static final int COL_USERNAME = 0;
    public static final int COL_Client = 1;
    public static final int COL_USERTYPE = 2;
    
    /* Issue no - 672
     * Issue Description -  Add a column showing time and date connected be added to the connected user tab.
     * 						Add a column showing the session id of the connected user be added to the connected user tab.
     * Developer - Nebu Alex 
     */
    public static final int COL_SESSIONID = 3;
    public static final int COL_DATEANDTIME = 4;
        //End of fix 672
    
    protected VWConUserTable m_parent;
    protected Vector m_vector;
    
    protected int m_sortCol = 0;
    protected boolean m_sortAsc = true;

    
    public VWConUserTableData(VWConUserTable parent) {
        m_parent = parent;
        m_vector = new Vector();
    }
    //--------------------------------------------------------------------------
    public void setData(Vector data) {
        m_vector.removeAllElements();
        if (data==null) return;
        int count =data.size();
        for(int i=0;i<count;i++) {
            m_vector.addElement(new ConUserRowData((Client)data.get(i)));
        }
    }
    //--------------------------------------------------------------------------
    public String[][] getData() {
        int count=getRowCount();
        String[][] data=new String[count][8];
        for(int i=0;i<count;i++) {
            ConUserRowData row=(ConUserRowData)m_vector.elementAt(i);
            data[i][0]=row.m_UserName;
            data[i][1]=row.m_Client;
            data[i][2]=String.valueOf(row.m_UserType);
            data[i][3]=String.valueOf(row.m_ClientId);
            data[i][4]=String.valueOf(row.m_DateAndTime);
            data[i][5]=String.valueOf(row.m_UserAdmin);
            data[i][6]=String.valueOf(row.m_CurClient);         
            data[i][7]=String.valueOf(row.m_UserTypeName);
        }
        return data;
    }
    
    //--------------------------------------------------------------------------
    //Method Added for MultiSelect Disconnect
    public Map getSelectedRowData(int[] selectedRows) {
    	Map rowIdMap=new HashMap(); 
    	Vector rowVector=new Vector();
    	String[] clientIds=new String[selectedRows.length];
    	rowVector.addAll(m_vector);
    	int rowIndex = 0;
    	for(int i=0;i<selectedRows.length;i++){
    		ConUserRowData row=(ConUserRowData)rowVector.elementAt(selectedRows[i]);
    		//Condition added to avoid disconnecting the current adminwise client
    		if(!row.m_CurClient)
    			rowIdMap.put(selectedRows[i], row.m_ClientId);
    	}
    	return rowIdMap;
    }

    //--------------------------------------------------------------------------
    public String[] getRowData(int rowNum) {
        String[] ConUserRowData=new String[8];
        ConUserRowData row=(ConUserRowData)m_vector.elementAt(rowNum);
        ConUserRowData[0]=row.m_UserName;
        ConUserRowData[1]=row.m_Client;
        ConUserRowData[2]=String.valueOf(row.m_UserType);
        ConUserRowData[3]=String.valueOf(row.m_ClientId);
        ConUserRowData[4]=String.valueOf(row.m_DateAndTime);
        
        ConUserRowData[5]=String.valueOf(row.m_UserAdmin);
        ConUserRowData[6]=String.valueOf(row.m_CurClient);
        ConUserRowData[7]=String.valueOf(row.m_UserTypeName);
     //   ConUserRowData[5]=String.valueOf(row.m_ClientId);
        return ConUserRowData;
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return m_vector==null ? 0 : m_vector.size();
    }
    //--------------------------------------------------------------------------
    public int getColumnCount() {
        return m_columns.length;
    }
    //--------------------------------------------------------------------------
    public String getColumnName(int column) {
    	String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
        return str;
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
        return true;
    }
    //--------------------------------------------------------------------------
    public Object getValueAt(int nRow, int nCol) {
        if (nRow < 0 || nRow>=getRowCount())
            return "";
        ConUserRowData row = (ConUserRowData)m_vector.elementAt(nRow);
        switch (nCol) {
            case COL_USERNAME:
                if(row.m_CurClient)
                    return new ColorData(row.m_UserName,ColorData.RED);
                else
                    return row.m_UserName;
            case COL_Client:
                if(row.m_CurClient)
                    return new ColorData(row.m_Client,ColorData.RED);
                else
                    return row.m_Client;
            case COL_USERTYPE:
                if(row.m_CurClient)
                {
                    return new ColorData(VWConstant.ClientType[row.m_UserType],ColorData.RED);
                }
                else
                {
                    String clientType=AdminWise.connectorManager.getString("conUserTable.clientType");
                    try
                    {
                        clientType=VWConstant.ClientType[row.m_UserType];
                    }
                    catch(Exception e)
                    {}
                    return clientType;
                }
      /*
        case COL_USERADMIN:
          if(row.m_UserAdmin.equals("1"))
              return new ColorData(VWConstant.YesNoData[0],ColorData.RED);
          else
              return VWConstant.YesNoData[1];
       */
                /* Issue no - 672
                 * Issue Description -  Add a column showing time and date connected be added to the connected user tab.
                 * 						Add a column showing the session id of the connected user be added to the connected user tab.
                 * Developer - Nebu Alex 
                 */
            case COL_SESSIONID:  
            	if(row.m_CurClient)
                    return new ColorData(String.valueOf(row.m_ClientId),ColorData.RED);
                else
                    return String.valueOf(row.m_ClientId);
            case COL_DATEANDTIME:  
            	if(row.m_CurClient)
                    return new ColorData(String.valueOf(row.m_DateAndTime),ColorData.RED);
                else
                    return String.valueOf(row.m_DateAndTime); 
            	// End of fix
        }
        return "";
    }
    //--------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
        if (nRow < 0 || nRow >= getRowCount())
            return;
        ConUserRowData row = (ConUserRowData)m_vector.elementAt(nRow);
        String svalue = value.toString();
        
        switch (nCol) {
            case COL_USERNAME:
                row.m_UserName = svalue;
                break;
            case COL_Client:
                row.m_Client = svalue;
                break;
                
            case COL_USERTYPE:
                row.m_UserType = Integer.parseInt(svalue);
                break;
                /* Issue no - 672
                 * Issue Description -  Add a column showing time and date connected be added to the connected user tab.
                 * 						Add a column showing the session id of the connected user be added to the connected user tab.
                 * Developer - Nebu Alex 
                 */
            case COL_SESSIONID:
            	row.m_ClientId = Integer.parseInt(svalue);
            	break;
            case COL_DATEANDTIME:
            	row.m_DateAndTime = svalue;
            // End of fix	

        }
    }
    //--------------------------------------------------------------------------
    public void clear() {
        m_vector.removeAllElements();
    }
    //--------------------------------------------------------------------------
    public void insert(ConUserRowData AdvanceSearchRowData) {
        m_vector.addElement(AdvanceSearchRowData);
    }
    //--------------------------------------------------------------------------
    public boolean remove(int row){
        if (row < 0 || row >= m_vector.size())
            return false;
        m_vector.remove(row);
        return true;
    }
    //--------------------------------------------------------------------------
    class ConUserComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public ConUserComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof ConUserRowData) || !(o2 instanceof ConUserRowData))
                return 0;
            ConUserRowData s1 = (ConUserRowData)o1;
            ConUserRowData s2 = (ConUserRowData)o2;
            int result = 0;
            double d1, d2;
            switch (m_sortCol) {
            	case COL_USERNAME:
                    result = s1.m_UserName.toLowerCase().compareTo(s2.m_UserName.toLowerCase());
                    break;
               case COL_Client:
                   result = s1.m_Client.compareTo(s2.m_Client);
                   break;
               case COL_USERTYPE:
                   result = String.valueOf(s1.m_UserTypeName).compareTo(String.valueOf(s2.m_UserTypeName));
                   break;
                 case COL_SESSIONID:
               	result = String.valueOf(s1.m_ClientId).compareTo(String.valueOf(s2.m_ClientId));
                   break;
                 case COL_DATEANDTIME:
               	result = s1.m_DateAndTime.toLowerCase().compareTo(s2.m_DateAndTime.toLowerCase());  
                   break;     
            }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof ConUserComparator) {
            	ConUserComparator compObj = (ConUserComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    } 
    
    class ColumnListener extends MouseAdapter {
        protected VWConUserTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWConUserTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
            
   /*          if(e.getModifiers()==e.BUTTON3_MASK)
                selectViewCol(e);
            else */if(e.getModifiers()==e.BUTTON1_MASK)
                sortCol(e);
        }
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {    	 
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new ConUserComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWConUserTableData.this));
            m_table.repaint();
        }
   }     
}