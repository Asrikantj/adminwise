package ViewWise.AdminWise.VWConUsers;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import  java.util.Vector;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWPrint;

public class VWConUserPanel extends JPanel implements  VWConstant
{
    public VWConUserPanel()
    {
    }
    
     public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWConUser.setLayout(null);
        add(VWConUser,BorderLayout.CENTER);
        VWConUser.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWConUser.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);
*/
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnDisconnect.setText(BTN_DISCONNECT_NAME);
        BtnDisconnect.setActionCommand(BTN_DISCONNECT_NAME);
        PanelCommand.add(BtnDisconnect);
//      BtnDisconnect.setBackground(java.awt.Color.white);
        BtnDisconnect.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnDisconnect.setIcon(VWImages.DisconnectIcon);
        
        top += hGap;
        BtnDisconnectAll.setText(BTN_DISCONNECTALL_NAME);
        BtnDisconnectAll.setActionCommand(BTN_DISCONNECTALL_NAME);
        PanelCommand.add(BtnDisconnectAll);
//      BtnDisconnectAll.setBackground(java.awt.Color.white);
        BtnDisconnectAll.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnDisconnectAll.setIcon(VWImages.DisconnectIcon);
        
        top += hGap + 12;
        BtnAllowCon.setText(BTN_ALLOWCON_NAME);
        BtnAllowCon.setActionCommand(BTN_ALLOWCON_NAME);
        BtnAllowCon.setIcon(VWImages.OkIcon);
        PanelCommand.add(BtnAllowCon);
//      BtnAllowCon.setBackground(java.awt.Color.white);
        String localLanguage=AdminWise.getLocaleLanguage();
        if(localLanguage.equals("nl_NL")){
        BtnAllowCon.setBounds(12,top-10,144 + 4, AdminWise.gBtnHeight+20);
        BtnAllowCon.setFocusable(false);
        }else
        	 BtnAllowCon.setBounds(12,top,144 + 4, AdminWise.gBtnHeight);	
        /*
        BtnUpdateUsers.setText(BTN_UPDATEUSERS_NAME);
        BtnUpdateUsers.setActionCommand(BTN_UPDATEUSERS_NAME);
        PanelCommand.add(BtnUpdateUsers);
        BtnUpdateUsers.setBackground(java.awt.Color.white);
        BtnUpdateUsers.setBounds(12,265,144, AdminWise.gBtnHeight);
        BtnUpdateUsers.setIcon(VWImages.ConUserIcon);
         */
        
        top += hGap + 12;
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        PanelCommand.add(BtnRefresh);
//      BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,top,144, AdminWise.gBtnHeight);
        
        top += hGap + 12;
        BtnSaveAs.setText(BTN_SAVEAS_NAME);
        BtnSaveAs.setActionCommand(BTN_SAVEAS_NAME);
        PanelCommand.add(BtnSaveAs);
//      BtnSaveAs.setBackground(java.awt.Color.white);
        BtnSaveAs.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSaveAs.setIcon(VWImages.SaveAsIcon);
        
        top += hGap;
        BtnPrint.setText(BTN_PRINT_NAME);
        BtnPrint.setActionCommand(BTN_PRINT_NAME);
        PanelCommand.add(BtnPrint);
//      BtnPrint.setBackground(java.awt.Color.white);
        BtnPrint.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnPrint.setIcon(VWImages.PrintIcon);
        
        PanelTable.setLayout(new BorderLayout());
        VWConUser.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWConUser.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.ConUserImage);
        SymAction lSymAction = new SymAction();
        BtnDisconnect.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        BtnSaveAs.addActionListener(lSymAction);
        BtnDisconnectAll.addActionListener(lSymAction);
        BtnAllowCon.addActionListener(lSymAction);
        BtnPrint.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
     }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnDisconnect)
                    BtnDisconnect_actionPerformed(event);
            else if (object == BtnRefresh)
                    BtnRefresh_actionPerformed(event);	
            else if (object == BtnDisconnectAll)
                    BtnDisconnectAll_actionPerformed(event);			
            else if (object == BtnSaveAs)
                    BtnSaveAs_actionPerformed(event);			
            else if (object == BtnAllowCon)
                    BtnAllowCon_actionPerformed(event);		
            else if (object == BtnPrint)
                    BtnPrint_actionPerformed(event);			
        }
    }
//------------------------------------------------------------------------------
    javax.swing.JPanel VWConUser = new javax.swing.JPanel();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_USERS_NAME, VWImages.ConUserImage);
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnDisconnect = new VWLinkedButton(1);
    VWLinkedButton BtnDisconnectAll = new VWLinkedButton(1);
    VWLinkedButton BtnRefresh = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    VWLinkedButton BtnAllowCon = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWConUserTable Table = new VWConUserTable();
//------------------------------------------------------------------------------        
class SymComponent extends java.awt.event.ComponentAdapter
{
    public void componentResized(java.awt.event.ComponentEvent event)
    {
        Object object = event.getSource();
        if (object == VWConUser)  VWConUser_componentResized(event);
    }
}
//------------------------------------------------------------------------------
void VWConUser_componentResized(java.awt.event.ComponentEvent event)
{
    if(event.getSource()==VWConUser)
    {
        Dimension mainDimension=this.getSize();
        Dimension panelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        panelDimension.width=mainDimension.width-commandDimension.width;
        PanelTable.setSize(panelDimension);
        PanelCommand.setSize(commandDimension);
        PanelTable.doLayout();
        AdminWise.adminPanel.MainTab.repaint();
    }
}
//------------------------------------------------------------------------------       
    public void loadTabData(VWRoom newRoom)
    {
        if(newRoom.getConnectStatus()!=Room_Status_Connect)
        {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        try{
            Table.addData(VWConUserConnector.getConUser());
        }
        catch(Exception e){};
        if(VWConUserConnector.getAllowConnections())
        {
        	String BTN_DISALLOWCON_NAME1=BTN_DISALLOWCON_NAME;
        	String str1=BTN_DISALLOWCON_NAME1.substring(0,18);
            String srt2=BTN_DISALLOWCON_NAME1.substring(18,BTN_DISALLOWCON_NAME1.length());
            String localLanguage=AdminWise.getLocaleLanguage();
            if(localLanguage.equals("nl_NL")){
            	BtnAllowCon.setText("<html><br>"+str1+"<br>"+srt2+"</html>");
            	BtnAllowCon.setFocusable(false);
            }else 
            	BtnAllowCon.setText(BTN_DISALLOWCON_NAME);
            BtnAllowCon.setIcon(VWImages.DelIcon);
        }
        else
        {
            BtnAllowCon.setText(BTN_ALLOWCON_NAME);
            BtnAllowCon.setIcon(VWImages.OkIcon);
        }
        setEnableMode(MODE_UNSELECTED);
    }
//------------------------------------------------------------------------------       
    public void unloadTabData()
    {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
//------------------------------------------------------------------------------       
    void BtnDisconnect_actionPerformed(java.awt.event.ActionEvent event)
    {
    	if(AdminWise.adminPanel.confirmDisconectUser)
    		if(VWMessage.showWarningDialog((java.awt.Component) this,
    				WRN_DISCONNECT_1+Table.getRowUserName(Table.getSelectedRow())+
    				WRN_DISCONNECT_2+AdminWise.adminPanel.roomPanel.getSelectedRoom().getName()+ "?"
    				)!=javax.swing.JOptionPane.YES_OPTION) return;
    	try{
    		//Code Modified for MultiSelect disconnect(27/11/2015)
    		// disConnectUser(Table.getSelectedRow());
    		AdminWise.adminPanel.setWaitPointer();
    		int[] selectedRows=Table.getSelectedRows();
    		Map rowIdMap= Table.getSelectRowClientIds(selectedRows);
    		try
    		{
    			Set setOfKeys = rowIdMap.keySet();
    			Iterator iterator = setOfKeys.iterator();
    			while (iterator.hasNext()) {
    				int key = ((Integer) iterator.next()).intValue();
    				int value = ((Integer)rowIdMap.get(key)).intValue();
    				//AdminWise.printToConsole("Key: "+ key+", Value: "+ value);
    				disConnectMultipleUser(key,value);
    				AdminWise.adminPanel.setWaitPointer();
    			}
    		}
    		catch(Exception e){
    			AdminWise.printToConsole("Inside catch of disconnect user:::"+e.getMessage());
    		}//End of MultiSelect disconnect code 
    		if(Table.getRowCount()>0)
    		{
    			Table.setRowSelectionInterval(0,0);
    			setEnableMode(MODE_SELECT);
    		}
    		else
    		{
    			setEnableMode(MODE_UNSELECTED);
    		}
    	}
    	catch(Exception e){}
    	finally{
    		Table.clearData();
    		Table.addData(VWConUserConnector.getConUser());
    		AdminWise.adminPanel.setDefaultPointer();}
    }
//------------------------------------------------------------------------------              
    void BtnDisconnectAll_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(AdminWise.adminPanel.confirmDisconectUser)
            if(VWMessage.showWarningDialog((java.awt.Component) this,
            WRN_DISCONNECT+AdminWise.adminPanel.roomPanel.getSelectedRoom().getName()+ "?"
            )!=javax.swing.JOptionPane.YES_OPTION) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            int i=0;
            while (i<Table.getRowCount())
            {
                if(!disConnectUser(i)) i++;
            }
            if(Table.getRowCount()>0)
            {
                Table.setRowSelectionInterval(0,0);
                setEnableMode(MODE_SELECT);
            }
            else
            {
                setEnableMode(MODE_UNSELECTED);
            }
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
 //----------------------------------------------------------------------------
    private boolean disConnectMultipleUser(int row,int clientId)
    {
    	try
    	{
    		AdminWise.adminPanel.setWaitPointer();
    		VWConUserConnector.disconnectUser(clientId);
    		Table.removeData(row);
    		AdminWise.adminPanel.setWaitPointer();    		
    		return true;
    	}
    	catch(Exception e){
    		AdminWise.printToConsole("Exception inside disConnectMultipleUser::::"+e.getMessage());
    	}
    	return false;
    }
//------------------------------------------------------------------------------
    private boolean disConnectUser(int row)
    {
        if(Table.getIsCurClient(row))  return false;
        try
        {
            int clientId=Table.getRowClientId(row);
            VWConUserConnector.disconnectUser(clientId);
            Table.removeData(row);
            return true;
        }
        catch(Exception e){};
        return false;
    }
//------------------------------------------------------------------------------       
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event)
    {   
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWSaveAsHtml.saveArrayInFile(Table.getReportData(),ConUserColNames,ConUserColWidths,
            null,(java.awt.Component)this,CONUSER_TITLE_NAME);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
//------------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event)
    {
        new VWPrint(Table);
    }
//------------------------------------------------------------------------------
void BtnAllowCon_actionPerformed(java.awt.event.ActionEvent event)
    {   
        if(BtnAllowCon.getText().equals(BTN_ALLOWCON_NAME))
        {
            VWConUserConnector.allowConnections();
            if(languageLocale.equals("nl_NL")){
            	String BTN_DISALLOWCON_NAME1=BTN_DISALLOWCON_NAME;
            	String str1=BTN_DISALLOWCON_NAME1.substring(0,18);
                String srt2=BTN_DISALLOWCON_NAME1.substring(18,BTN_DISALLOWCON_NAME1.length());
                String localLanguage=AdminWise.getLocaleLanguage();
                if(localLanguage.equals("nl_NL")){
                	BtnAllowCon.setText("<html><br>"+str1+"<br>"+srt2+"</html>");
                	BtnAllowCon.setFocusable(false);
                }else {
                	BtnAllowCon.setText(BTN_DISALLOWCON_NAME);
                }
            }
        }
        else
        {
            if(VWMessage.showWarningDialog((java.awt.Component) this,
           WRN_DISALLOW_1+"<"+AdminWise.adminPanel.roomPanel.getSelectedRoom().getName()+">"+NewLineChar+
            WRN_DISALLOW_2
            )!=javax.swing.JOptionPane.YES_OPTION) return;
            VWConUserConnector.disallowConnections();
            String BTN_DISALLOWCON_NAME1=BTN_DISALLOWCON_NAME;
        	String str1=BTN_DISALLOWCON_NAME1.substring(0,18);
            String srt2=BTN_DISALLOWCON_NAME1.substring(18,BTN_DISALLOWCON_NAME1.length());
            String localLanguage=AdminWise.getLocaleLanguage();
            if(localLanguage.equals("nl_NL")){
            	BtnAllowCon.setText("<html><br>"+str1+"<br>"+srt2+"</html>");
            	BtnAllowCon.setFocusable(false);
            }else 
            	BtnAllowCon.setText(BTN_DISALLOWCON_NAME);
        }
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        Table.clearData();
        Table.addData(VWConUserConnector.getConUser());
        if(VWConUserConnector.getAllowConnections()){
        	String BTN_DISALLOWCON_NAME1=BTN_DISALLOWCON_NAME;
        	String str1=BTN_DISALLOWCON_NAME1.substring(0,18);
            String srt2=BTN_DISALLOWCON_NAME1.substring(18,BTN_DISALLOWCON_NAME1.length());
            String localLanguage=AdminWise.getLocaleLanguage();
            if(localLanguage.equals("nl_NL")){
            	BtnAllowCon.setText("<html><br>"+str1+"<br>"+srt2+"</html>");
            	BtnAllowCon.setFocusable(false);
            }else 
            	BtnAllowCon.setText(BTN_DISALLOWCON_NAME);
        }
        else
            BtnAllowCon.setText(BTN_ALLOWCON_NAME);
        setEnableMode(MODE_UNSELECTED);
    }    
//------------------------------------------------------------------------------
    public void setEnableMode(int mode)
    {
        BtnDisconnect.setEnabled(true);
        BtnRefresh.setEnabled(true);
        BtnSaveAs.setEnabled(true);
        BtnPrint.setEnabled(true);
        BtnDisconnectAll.setEnabled(true);
        BtnAllowCon.setEnabled(true);
        switch (mode)
        {
            case MODE_UNSELECTED:
                BtnDisconnect.setEnabled(false);
                if(Table.getRowCount()==0)
                {
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                }
                else if(Table.getRowCount()==1)
                    BtnDisconnectAll.setEnabled(false);
                break;
            case MODE_SELECT:
                if(Table.getIsCurClient())
                    BtnDisconnect.setEnabled(false);
                if(Table.getRowCount()==1)
                    BtnDisconnectAll.setEnabled(false);
                break;
           case MODE_UNCONNECT:
                BtnDisconnect.setEnabled(false);
                BtnRefresh.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                BtnDisconnectAll.setEnabled(false);
                BtnAllowCon.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
//------------------------------------------------------------------------------
    private static int gCurRoom=0;
    private boolean UILoaded=false;
    String languageLocale=AdminWise.getLocaleLanguage();
}