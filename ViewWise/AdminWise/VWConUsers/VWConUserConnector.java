/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWConUserConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWConUsers;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;

//------------------------------------------------------------------------------
public class VWConUserConnector implements VWConstant
{
    public static Vector getConUser()
    {
        Vector clients = new Vector();
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            AdminWise.gConnector.ViewWiseClient.getConnectedClients(room.getId(),clients);
        return clients;
    }
//------------------------------------------------------------------------------
    public static void disconnectUser(int clientId)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            AdminWise.gConnector.ViewWiseClient.logoutClient(room.getId(),clientId);
    }
//------------------------------------------------------------------------------
    public static void allowConnections()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            AdminWise.gConnector.ViewWiseClient.setAllowConnection(room.getId(),true);
    }
//------------------------------------------------------------------------------
    public static void disallowConnections()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
        AdminWise.gConnector.ViewWiseClient.setAllowConnection(room.getId(),false);
    }
//------------------------------------------------------------------------------
    public static boolean getAllowConnections()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            return AdminWise.gConnector.ViewWiseClient.isConnectionEnabled(room.getId())==1;
        return false;
    }
//------------------------------------------------------------------------------
}