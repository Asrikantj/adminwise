/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * AdminWise<br>
 *
 * @version     $Revision: 1.6 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 */
package ViewWise.AdminWise;

import javax.swing.JApplet;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.vwc.VWCPreferences;
import com.computhink.vwc.VWCUtil;
import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.windows.WindowsLookAndFeel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import ViewWise.AdminWise.VWUtil.VWUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Frame;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWProperties;
import ViewWise.AdminWise.VWPanel.VWAdminPanel;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoomConnector;
import java.net.URL;
import java.net.MalformedURLException;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.resource.ResourceManager;

import java.util.Locale;
import java.util.prefs.Preferences;
//--------------------------------------------------------------------------
public class AdminWise extends JApplet implements VWConstant, Constants {
    static{
        try {
/*
Enhancement No / Purpose:  <85/Update the UI with JGoodies>
Created by: <Pandiya Raj.M>
Date: <03 Aug 2006>
*/        	
            String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
            String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
            //UIManager.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");
            UIManager.setLookAndFeel(plasticLookandFeel);
            
            String fontName = "Arial";
            if (VWCPreferences.getFontName().trim().length()> 0)
        	fontName = VWCPreferences.getFontName();
            System.out.println("Font Name : " + fontName + " : " + VWCPreferences.getFontSize());            
            setUIFont(new FontUIResource(fontName, Font.PLAIN, VWCPreferences.getFontSize()));
        }
        catch(java.lang.SecurityException se){
            VWMessage.showMessage(null,se.getMessage(),VWMessage.DIALOG_TYPE);
            permissionNotSet();
        }
        catch (Exception e) {System.err.println(e);}
    }
    private static AdminWise instance = null;
    public static ResourceManager connectorManager = null;
    //--------------------------------------------------------------------------
    public void init() {
        if(AdminWise.getCurrentMode()==APPLICATION_MODE) setVisible(false);
    	//Locale.setDefault(Locale.getAvailableLocales()[0]);
        Locale.setDefault(Locale.getDefault());
        if(Loaded)
            LoadedInit();
        else
            NewLoadInit();
    }
    //--------------------------------------------------------------------------
    public void LoadedInit() {
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(adminPanel,BorderLayout.CENTER);
        if(AdminWise.getCurrentMode()==APPLET_MODE)
        adminPanel.setVisible(true);
        gConnector=new VWConnector();        
    }
    //--------------------------------------------------------------------------
    public void NewLoadInit() {
        try{
            System.setProperty("awt.appletWarning","");
            String now=(new Date()).toString();
            getRootPane().putClientProperty("defeatSystemEventQueueCheck",Boolean.TRUE);
            new VWImages();
            VWUtil util=new VWUtil(this);
            ///VWMessage.showInitMsg(VWMessage.MSG_PROPERTIES_LOAD);
            String[] params=getParameters();
            VWProperties prop = new VWProperties(params);
            gConnector=new VWConnector();
            listTable = new Hashtable();
            ///VWMessage.showInitMsg(VWMessage.MSG_CHECKROOM_LOAD);
            adminPanel=new VWAdminPanel();
            if(AdminWise.getCurrentMode()==APPLET_MODE){
            getContentPane().setLayout(new BorderLayout());
            getContentPane().add(adminPanel,BorderLayout.CENTER);            
            adminPanel.setVisible(true);
            }
            try{this.setSize(util.gScreen_Size);} catch(Exception e){}
            VWMessage.setMessageOptions(this);
            SymComponent aSymComponent = new SymComponent();
            this.addComponentListener(aSymComponent);
            instance=this;
            Loaded=true;
        }
        catch(java.lang.SecurityException se){
            VWMessage.showMessage(null,se.getMessage(),VWMessage.DIALOG_TYPE);
            permissionNotSet();}
        catch (Exception e) {System.err.println(e);}
    }
    //--------------------------------------------------------------------------
    public static AdminWise getInstance() {
        return instance;
    }
    public static String getLocaleLanguage(){
    	localeLanguage=AdminWise.connectorManager.getLocale().toString();
    	return localeLanguage;
    }
    //--------------------------------------------------------------------------
    public static VWConnector gConnector=null;
    public static VWAdminPanel adminPanel=null;
    //--------------------------------------------------------------------------
    public String getAppletInfo(){
        return "Title: Administration \nAuthor: Computhink ,Inc., 2002 ";
    }
    //--------------------------------------------------------------------------
    public void start(){
        adminPanel.docTypePanel.spliter.setDividerLocation(0.6);
        adminPanel.docTypePanel.spliter.doLayout();
        adminPanel.docTypePanel.doLayout();
    }
    //--------------------------------------------------------------------------
    public void destroy() {
        WebCleanUp();
    }
    //--------------------------------------------------------------------------
    private void WebCleanUp() {
        VWRoomConnector.disConnectAllRooms();
        if(gConnector!=null)gConnector.logout();
    }
    //--------------------------------------------------------------------------
    private String getParameter(String key, String def) {
        if(getParameter(key)!=null)
            return getParameter(key);
        else
            return def;
    }
    //--------------------------------------------------------------------------
    private String[] getParameters() {
        String[] params=new String[Properties_Count];
        try{
            params[Property_VWVerbosity_Index]=getParameter("VWVerbosity","0");
            params[Property_VWFlushLog_Index]=getParameter("VWFlushLog","true");
            params[Property_VWTraceErrors_Index]=getParameter("VWTraceErrors","true");
            params[Property_VWStdOut_Index]=getParameter("VWStdOut","false");
            params[Property_VWFileSize_Index]=getParameter("VWFileSize","100000");
            params[Property_VWFileCount_Index]=getParameter("VWFileCount","5");
            params[Property_VWHome_Index]=getParameter("VWHome","");
        }
        catch(Exception e){
            params[Property_VWVerbosity_Index]="11";
            params[Property_VWFlushLog_Index]="True";
            params[Property_VWTraceErrors_Index]="True";
            params[Property_VWStdOut_Index]="False";
            params[Property_VWFileSize_Index]="100000";
            params[Property_VWFileCount_Index]="5";
            params[Property_VWHome_Index]="";
        }
        return params;
    }
    //--------------------------------------------------------------------------
    public String[][] getParameterInfo(){
        String[][] info = {
            {"VWID","Text","The user name. Default is Unknown ."},
            {"VWPwd","Text","The user password . Default '' ."},
            {"VWServer","Text","The server's address containts " + PRODUCT_NAME + " Service . Default is 127.0.0.1."},
            {"VWVerbosity","0,11","Log every think or not. Default is 0 ."},
            {"VWFlushLog","True,False","The Default is True ."},
            {"VWTraceErrors","True,False","Default is True"},
            {"VWStdOut","True,False","Display log in statndard output, Default is False."},
            {"VWFileSize","Number","The size of log file,Default is 100000 ."},
            {"VWFileCount","Number","The log file count,Default is 5 ."},
            {"VWHomeFolder","Text","The Home directory path,Default is C:\\Program Files\\Administration"}};
            return info;
    }
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == AdminWise.this)
                VWWeb_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWWeb_componentResized(java.awt.event.ComponentEvent event) {
        getContentPane().doLayout();
    }
    //--------------------------------------------------------------------------
    public static boolean initFile(){
    	    	
        try
        {       	
        	File filePath = new File(path);
            filePath.getParentFile().mkdirs();
            if(filePath.exists())
            {
                filePath.renameTo(new File(path+Util.getNow(1)));
            }
            FileOutputStream ufos = new FileOutputStream(path);
            osw = new OutputStreamWriter(ufos);
        }
        catch(IOException e)
        {
            String error=e.getLocalizedMessage();
            return false;

        }
        return true;
    }
    public static boolean closeFile()
    {
        try
        {
            osw.close();
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }
    
    public static boolean writeToFile(String data)
    {
        try
        {
            osw.write(data+Constants.NewLineChar);
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }
    static OutputStreamWriter osw = null;
    static String path = "c:\\vwtemp\\VWTemplate.txt";
    public static void main(String args[]) {
    	connectorManager = ResourceManager.getDefaultManager();
    	boolean VWTFile = false;
    	try{
    		if (args[3] != null && args[3].trim().length() > 0)
    			VWTFile = true;
    	}catch(Exception ex){
    		VWTFile = false;
    	}
    	adminWise_Mode=APPLET_MODE;
    	/*        args=new String[3];
    	 * 		
        args[0]="Feb28";
        args[1]="admin";
        args[2]="vw";        
        args[3]="c:\\test.vwt";
    	 */        
    	if(VWTFile){      		
    		adminWise_Mode=APPLICATION_MODE;
    		try{
    			initFile();
    			writeToFile("Login information  : Room name: " + args[0] + " User name: " + args[1]);
    		}catch(Exception ex){
    			System.out.println("Exception " + ex.getMessage());
    		}       		
    	}
    	//writeToFile("adminWise_Mode : " + adminWise_Mode);
    	showSplashScreen=false;
    	int sid = 0;
    	adminFrame = new AdminFrame(adminWise_Mode);
    	gConnector = new VWConnector();
    	try{
    		if(VWTFile) {
    			adminWise_Mode=APPLICATION_MODE;
    			if (args[3] == null || args[3].trim().length() == 0){
    				writeToFile("VWT file is missing - " + args[3]);
    				closeApp();
    			}           	
    			sid = gConnector.silentLogin(args);
    			//System.out.println("Session id : " + sid);
    			writeToFile("Info : Session id : " + sid);
    			if (sid < 0) {
    				writeToFile("Error : " + gConnector.ViewWiseClient.getErrorDescription(sid));
    			}
    			if (sid > 0){
    				writeToFile("Info : Login is successful.");
    				File vwtFile = new File(args[3].trim());
    				writeToFile("Info : VWT File Name " + vwtFile.getPath());	 
    				if (vwtFile.exists()) {
    					int result = AdminWise.adminPanel.templatePanel.applyVWT(args[3]);
    					writeToFile("Info : Apply the VWT file is " + ((result == 0)?" Success ."  : "Failed."));
    				}else
    					writeToFile("VWT file is not found.");
    			}
    		}else{
    			try{
    				gConnector.silentLogin(args);
    			}catch(Exception ex){

    			}
    		}        
    	}
    	catch(Exception e){ 
    		//VWMessage.showMessage(null,"Exception occurred while applying the template ");
    		writeToFile("Error : Exception occurred while applying the template : " + e.getMessage());
    		//e.printStackTrace();
    	}
    	finally{
    		if (adminWise_Mode == APPLICATION_MODE){
    			if (sid  > 0){
    				gConnector.disConnectRoom(sid);
    				writeToFile("Info : Logout is successful.");
    			}
    			closeApp();
    		}
    	}

    }
    
    static void closeApp(){
    	closeFile();    	
    	try{
    		java.lang.Runtime.getRuntime().exec("notepad.exe "+path);
    	}catch(Exception ex){
    		
    	}
    	System.exit(0);
    }
    //--------------------------------------------------------------------------
    public static void permissionNotSet() {
        VWMessage.showMessage(null,VWMessage.MSG_SECURITY_MISSING,VWMessage.DIALOG_TYPE);
        System.exit(0);
    }
    //--------------------------------------------------------------------------
    public void showHelpPage(int showType) {
    	AdminWise.printToConsole("showType >>>>>"+showType);
    	if (showType == 2) {
        	showHelpOnlinePage();        	
        } else {
        	String path = VWCUtil.getHome() + "\\help\\VWADMIN.chm";
	        if(path != null) 
	        {
	            try
	            {
	                //java.lang.Runtime.getRuntime().exec("Winhlp32.exe " + path);
	                java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + path);
	            }
	            catch(java.io.IOException ioe){VWMessage.showMessage(null,VWMessage.ERR_HELP_NOTFOUND,VWMessage.DIALOG_TYPE);}
	        }
        }
    }
    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
	//
	// sets the default font for all Swing components.
	// ex.
	// setUIFont (new javax.swing.plaf.FontUIResource
	// ("Serif",Font.ITALIC,12));
	//
	java.util.Enumeration keys = UIManager.getDefaults().keys();
	while (keys.hasMoreElements()) {
	    Object key = keys.nextElement();
	    Object value = UIManager.get(key);
	    
	    if (value instanceof javax.swing.plaf.FontUIResource){		
		UIManager.put(key, f);
	    }
	}
    }   
	public static void printToConsole(String msg){
		try
    	{
        	Preferences clientPref =  null;        	
        	boolean displayFlag = false;
        	try{
        		clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/administration");
        		displayFlag  = clientPref.getBoolean("debug",false);
        	}catch(Exception ex){
        		displayFlag = false;
        	}
        	
        	if (displayFlag){
        		String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
				msg = "AW::: " + now + " : " + msg + " \n";    			
				java.util.Properties props = System.getProperties(); 
		     	String filepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + "Administration.log";
		     	File log = new File(filepath);        
				String logFilePath = null;
				if (log.exists()) {
					if ((log.length() / (1024 * 1024)) >= 1) {						
						logFilePath = filepath.substring(0, (filepath.length() - 4));
						log.renameTo(new File(logFilePath +"_"+ Util.getNow(1) + ".log"));
					}
				}
		        FileOutputStream fos = new FileOutputStream(log, true);
		        fos.write(msg.getBytes());
		        fos.close();            
		        fos.close();
		        log = null;              
        	}
    	}catch (Exception e)
        {
        	System.out.println( "printToConsole");	
        }
		//System.out.println("VW:	" + msg);
	}
    //--------------------------------------------------------------------------
    public static int getCurrentMode() {
        return adminWise_Mode;
    }
    //--------------------------------------------------------------------------
    public void showHelpOnlinePage() {
    	try {
    		String path = "http://help.computhink.com/administration/";
        	Desktop.getDesktop().browse(new URL(path).toURI());
    	} catch(Exception e) {
    		AdminWise.printToConsole("Exception while showing help online page :"+e);
    	}
    }
    //--------------------------------------------------------------------------
    private static boolean Loaded=false;
    private static String curLookAndFeel=null;
    private static int adminWise_Mode=APPLET_MODE;
    private static boolean showSplashScreen=true;
    public static AdminFrame adminFrame=null;
    public static Hashtable listTable = null;
    public static int gBtnHeight = 24;
    public static int gBtnWidth = 80;
    public static int gPanelBtnWidth = 144;
    public static int gSmallBtnHeight = 21;
    public static String localeLanguage=null;
    public static Color getAWColor(){
    	return new Color(238, 242, 244);
    }
}
//--------------------------------------------------------------------------