/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWAdminPanel<br>
 *
 * @version     $Revision: 1.12 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWPanel;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.prefs.Preferences;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWViewerTabManager;
import ViewWise.AdminWise.VWArchive.VWArchivePanel;
import ViewWise.AdminWise.VWAuditTrail.VWATPanel;
import ViewWise.AdminWise.VWBackup.VWBackupPanel;
import ViewWise.AdminWise.VWRetention.VWRetentionPanel;
import ViewWise.AdminWise.VWConUsers.VWConUserPanel;
import ViewWise.AdminWise.VWDocLock.VWDocLockPanel;
import ViewWise.AdminWise.VWDocType.VWDocTypePanel;
import ViewWise.AdminWise.VWFormTemplate.VWFormTemplatePanel;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMask.VWMaskField;
import ViewWise.AdminWise.VWNodeSecurity.VWSecurityPanel;
import ViewWise.AdminWise.VWNotification.VWNotificationPanel;
import ViewWise.AdminWise.VWPrincipal.VWPrincipalPanel;
import ViewWise.AdminWise.VWRecycle.VWRecyclePanel;
import ViewWise.AdminWise.VWRedactions.VWRedactionsPanel;
import ViewWise.AdminWise.VWRestore.VWRestorePanel;
import ViewWise.AdminWise.VWRoom.VWRoomConnector;
import ViewWise.AdminWise.VWRoom.VWRoomPanel;
import ViewWise.AdminWise.VWRoute.VWRoutePanel;
import ViewWise.AdminWise.VWRoute.VWRouteReportPanel;
import ViewWise.AdminWise.VWSecureLink.VWSecureLinkPanel;
import ViewWise.AdminWise.VWStatistic.VWStatisticPanel;
import ViewWise.AdminWise.VWTemplate.VWTemplatePanel;
import ViewWise.AdminWise.VWUtil.VWUtil;

/**
 * A basic extension of the javax.swing.JApplet class
 */
public class VWAdminPanel extends JPanel implements VWConstant
{   
    public VWAdminPanel()
    {
        getViewWiseOptions();
        getBackupOptions();
        getATOptions();
        getRestoreOptions();
        MainTab =new JTabbedPane();
        routePanel = new VWRoutePanel();
        routeReportPanel=new VWRouteReportPanel();
        retentionPanel = new VWRetentionPanel();
        notificationPanel = new VWNotificationPanel();
        formTemplatePanel = new VWFormTemplatePanel();
        secureLinkPanel = new VWSecureLinkPanel();
        roomPanel = new VWRoomPanel();
        conUserPanel = new VWConUserPanel();
        docLockPanel = new VWDocLockPanel();
        redactionsPanel=new VWRedactionsPanel();
        recyclePanel = new VWRecyclePanel();
        docTypePanel = new VWDocTypePanel();
        templatePanel = new VWTemplatePanel();
        ///propertiesPanel = new VWPropertiesPanel();
        statisticPanel = new VWStatisticPanel();
        ///logViewerPanel = new VWLogViewerPanel();
        archivePanel = new VWArchivePanel();
        auditTrailPanel = new VWATPanel();
        backupPanel = new VWBackupPanel();
        restorePanel = new VWRestorePanel();
        principalPanel=new VWPrincipalPanel();
        securityPanel=new VWSecurityPanel();
        FindPanel = new JPanel(new CardLayout());
        JLabel1 = new javax.swing.JLabel();
        JLabel2 = new javax.swing.JLabel();

        MainTab.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        /**
         * CV10.1 Enhancement tabs grouping.
         */
        //Grouping 1 � Setup
        MainTab.addTab(TAB_DOCTYPES_NAME,VWImages.DocTypesIcon,docTypePanel,DOCTYPE_TOOLTIP);
        MainTab.addTab(TAB_ARCHIVE_NAME,VWImages.StorageIcon,archivePanel,ARCHIVE_TOOLTIP);
        MainTab.addTab(TAB_PRINCIPAL_NAME,VWImages.UserGroupIcon,principalPanel,PRINCIPAL_TOOLTIP);
        MainTab.addTab(TAB_RETENTION_NAME ,VWImages.RetentionIcon, retentionPanel,RETENTION_TOOLTIP);
        MainTab.addTab(TAB_NOTIFICATION_NAME ,VWImages.NotifyIcon, notificationPanel,NOTIFICATION_TOOLTIP);
        MainTab.addTab(TAB_ROUTE_NAME ,VWImages.RouteImage, routePanel,ROUTE_TOOLTIP);
        
        //(Grouping 2 � Monitoring)
        MainTab.addTab(TAB_AUDITTRAIL_NAME,VWImages.ATIcon,auditTrailPanel,AUDITTRAIL_TOOLTIP);
        MainTab.addTab(TAB_USERS_NAME,VWImages.ConUserIcon,conUserPanel,USERS_TOOLTIP);
        MainTab.addTab(TAB_DOCLOCK_NAME,VWImages.DocLockIcon,docLockPanel,DOCLOCK_TOOLTIP);
        MainTab.addTab(TAB_RECYCLE_NAME,VWImages.RecycleIcon,recyclePanel,RECYCLE_TOOLTIP);
        MainTab.addTab(TAB_STATISTIC_NAME,VWImages.StatisticIcon,statisticPanel,STATISTIC_TOOLTIP);  
        MainTab.addTab(TAB_REDACTIONS_NAME,VWImages.RedactionsIcon,redactionsPanel,REDACTIONS_TOOLTIP);
        MainTab.addTab(TAB_ROUTE_REPORT_NAME ,VWImages.RouteImage,  routeReportPanel,ROUTE_TOOLTIP);
        MainTab.addTab(TAB_FORM_TEMPLATE_NAME ,VWImages.FormTemplateImage, formTemplatePanel,FORM_TEMPLATE_TOOLTIP);
        MainTab.addTab(TAB_SECURE_LINK,VWImages.StatisticIcon,secureLinkPanel,SECURE_LINK_TOOLTIP);
        //(Group 3 -  Administration)      
        MainTab.addTab(TAB_TEMPLATE_NAME,VWImages.TemplateIcon,templatePanel,TEMPLATE_TOOLTIP);
        MainTab.addTab(TAB_BACKUP_NAME,VWImages.BackupIcon,backupPanel,BACKUP_TOOLTIP);
        MainTab.addTab(TAB_RESTORE_NAME,VWImages.RestoreIcon,restorePanel,RESTORE_TOOLTIP);
        
        ///MainTab.addTab(TAB_PROPERTIES_NAME,VWImages.PropertyIcon,propertiesPanel,PROPERTIES_TOOLTIP);
        ///MainTab.addTab(TAB_LOGVIEWER_NAME,VWImages.LogViewerIcon,logViewerPanel,LOGVIEWER_TOOLTIP);
       // MainTab.addTab(TAB_REDACTIONS_NAME,VWImages.RedactionsIcon,redactionsPanel,REDACTIONS_TOOLTIP);
     
        ///MainTab.addTab(TAB_SECURITY_NAME,VWImages.ConUserIcon,securityPanel,SECURITY_TOOLTIP);
       
       // MainTab.addTab(TAB_ROUTE_REPORT_NAME ,VWImages.RouteImage,  routeReportPanel,ROUTE_TOOLTIP);
       // MainTab.addTab(TAB_FORM_TEMPLATE_NAME ,VWImages.FormTemplateImage, formTemplatePanel,FORM_TEMPLATE_TOOLTIP);
       
        
        MainTab.setTabPlacement(SwingConstants.BOTTOM);
        SymChange lSymChange=new SymChange();
        MainTab.addChangeListener(lSymChange);
        SymMouse aSymMouse=new SymMouse();
        MainTab.addMouseListener(aSymMouse);
        VWRoomConnector.loadVWRooms();
        MainTab.setSelectedIndex(0);
        GridBagLayout gridbag=new GridBagLayout();
        GridBagConstraints c=new GridBagConstraints();
        setLayout(gridbag);
        setBounds(170,0,700,700);
        JLabel1.setText(LBL_ROOMS);
        c.fill=GridBagConstraints.BOTH;
        c.weightx=1.0;
        c.gridwidth=GridBagConstraints.REMAINDER;
        gridbag.setConstraints(JLabel1,c);
        ///add(JLabel1);
        c.gridwidth=1;
        c.gridheight = 2;
        c.weighty = 0.2;
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(roomPanel,c);
        add(roomPanel);
        /*
        JLabel2.setText("yyyy");
        c.weighty = 0.0;
        c.gridheight = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(JLabel2,c);
        add(JLabel2);
        */
        c.gridwidth = 1;
        c.gridheight = 2;
        c.weighty = 3.0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(MainTab,c);
        add(MainTab);
        new VWMaskField();
	}
//------------------------------------------------------------------------------
	/*
        public static JTabbedPane MainTab =new JTabbedPane();
        public static VWRoomPanel roomPanel = new VWRoomPanel();
        public static VWConUserPanel conUserPanel = new VWConUserPanel();
        public static VWDocLockPanel docLockPanel = new VWDocLockPanel();
        public static VWRecyclePanel recyclePanel = new VWRecyclePanel();
        public static VWDocTypePanel docTypePanel = new VWDocTypePanel();
        public static VWTemplatePanel templatePanel = new VWTemplatePanel();
        public static VWPropertiesPanel propertiesPanel = new VWPropertiesPanel();
        public static VWStatisticPanel statisticPanel = new VWStatisticPanel();
        public static VWLogViewerPanel logViewerPanel = new VWLogViewerPanel();
        public static VWArchivePanel archivePanel = new VWArchivePanel();
        public static VWATPanel auditTrailPanel = new VWATPanel();

	public static JPanel FindPanel = new JPanel(new CardLayout());
        javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
         **/
        public static JTabbedPane MainTab =null;
        public static VWConUserPanel conUserPanel = null;
        public static VWDocLockPanel docLockPanel = null;
        public static VWRecyclePanel recyclePanel = null;
        public static VWDocTypePanel docTypePanel = null;
        public static VWTemplatePanel templatePanel = null;
        ///public static VWPropertiesPanel propertiesPanel = null;
        public static VWStatisticPanel statisticPanel = null;
        ///public static VWLogViewerPanel logViewerPanel = null;
        public static VWArchivePanel archivePanel = null;
        public static VWATPanel auditTrailPanel = null;
        public static VWBackupPanel backupPanel = null;
        public static VWRestorePanel restorePanel = null;
        public static VWRedactionsPanel redactionsPanel = null;
        public static VWPrincipalPanel principalPanel=null;
        public static VWSecurityPanel securityPanel=null;
        public static JPanel FindPanel = null;
        public static VWRoomPanel roomPanel = null;
        public static VWRoutePanel routePanel = null;
        public static VWRouteReportPanel routeReportPanel = null;
        public static VWRetentionPanel retentionPanel = null;
        public static VWNotificationPanel notificationPanel = null;
        public static VWFormTemplatePanel formTemplatePanel = null;
        public static VWSecureLinkPanel secureLinkPanel = null;
        javax.swing.JLabel JLabel1 = null;
        javax.swing.JLabel JLabel2 = null;
//------------------------------------------------------------------------------
class SymChange implements javax.swing.event.ChangeListener
    {
        public void stateChanged(javax.swing.event.ChangeEvent event)
        {
            Object object = event.getSource();
            if (object == MainTab)
                    MainTab_stateChanged(event);
        }
}
//------------------------------------------------------------------------------         
    void MainTab_stateChanged(javax.swing.event.ChangeEvent event)
    {
        VWViewerTabManager.setActivTabData();
    }
//------------------------------------------------------------------------------ 
class SymMouse extends java.awt.event.MouseAdapter
{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if(object==MainTab)
            if(event.getModifiers()==event.BUTTON3_MASK){
                VWTree_RightMouseClicked(event);}
            else if(event.getModifiers()==event.BUTTON1_MASK){
                VWTree_LeftMouseClicked(event);}
    }
//------------------------------------------------------------------------------
    public void mouseEntered(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if(object==MainTab)
        {
            int index =MainTab.getSelectedIndex();
            if (index <=1) return;
        }
    }
//------------------------------------------------------------------------------    
    public void mouseExited(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if(object==MainTab)
        {
            int index =MainTab.getSelectedIndex();
            if (index <=1) return;
        }
    }
}
//------------------------------------------------------------------------------
void VWTree_RightMouseClicked(java.awt.event.MouseEvent event)
{
    if(event.getClickCount() == 1) rSingleClick(event);
}
//------------------------------------------------------------------------------
void VWTree_LeftMouseClicked(java.awt.event.MouseEvent event)
{    
    if ((event.getClickCount() == 2))   lDoubleClick(event);
    else if(event.getClickCount() == 1) lSingleClick(event);
}
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event)
{
  /*
    int index =MainTab.getSelectedIndex();
    if (index <=properyPos) return;
    VWMenu menu=new VWMenu(Viewer_TYPE);
    menu.show(MainTab,event.getX(),event.getY());
  */
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event)
{
}
//------------------------------------------------------------------------------
private void lSingleClick(java.awt.event.MouseEvent event)
{ 
}
//------------------------------------------------------------------------------
public void setWaitPointer()
{
    setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
}
//------------------------------------------------------------------------------
public void setDefaultPointer()
{
    setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
}
//------------------------------------------------------------------------------
public void saveViewWiseOptions(String[][] vwOptions)
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        int Options_No=options.length;
        for(int i=0;i<Options_No;i++)
        {
            prefs.put(vwOptions[i][0],vwOptions[i][1]);
        }
        setViewWiseOptions(vwOptions);
    }
//------------------------------------------------------------------------------
public void saveBackupOptions(String[][] vwOptions)
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        int Options_No=backupOptions.length;
        for(int i=0;i<Options_No;i++)
        {
            prefs.put(vwOptions[i][0],vwOptions[i][1]);
        }
        setBackupOptions(vwOptions);
    }

//------------------------------------------------------------------------------
public void saveRestoreOptions(String[][] vwOptions)
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        int Options_No=restoreOptions.length;
        for(int i=0;i<Options_No;i++)
        {
            prefs.put(vwOptions[i][0],vwOptions[i][1]);
        }
        setRestoreOptions(vwOptions);
    }
//------------------------------------------------------------------------------

public void saveATOptions(String[][] vwOptions)
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        int Options_No=ATOptions.length;
        for(int i=0;i<Options_No;i++)
            prefs.put(vwOptions[i][0],vwOptions[i][1]);
        setATOptions(vwOptions);
    }
//------------------------------------------------------------------------------
    public String[][] getViewWiseOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        String[][] vwOptions=options;
        int Options_No=options.length;
        for(int i=0;i<Options_No;i++)
        {
            vwOptions[i][1]=prefs.get(vwOptions[i][0],vwOptions[i][1]);
        }
      /*  if(AdminWise.gConnector.ViewWiseClient.isProxyEnabled())
        {
            vwOptions[Options_No-3][1]=YesNoData[0];
            vwOptions[Options_No-2][1]=AdminWise.gConnector.ViewWiseClient.getProxyHost();
            vwOptions[Options_No-1][1]=String.valueOf(AdminWise.gConnector.ViewWiseClient.getProxyPort());
        }
        else
        {
            vwOptions[Options_No-3][1]=YesNoData[1];
            vwOptions[Options_No-2][1]="";
            vwOptions[Options_No-1][1]="";
        }*/
        setViewWiseOptions(vwOptions);
        return vwOptions;
    }
//------------------------------------------------------------------------------
    public String[][] getBackupOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        String[][] vwOptions=backupOptions;
        int Options_No=backupOptions.length;
        for(int i=0;i<Options_No;i++)
        {
            vwOptions[i][1]=prefs.get(vwOptions[i][0],vwOptions[i][1]);
        }
        setBackupOptions(vwOptions);
        return vwOptions;
    }
//------------------------------------------------------------------------------
    public String[][] getRestoreOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        String[][] vwOptions=restoreOptions;
        int Options_No=restoreOptions.length;
        for(int i=0;i<Options_No;i++)
        {
            vwOptions[i][1]=prefs.get(vwOptions[i][0],vwOptions[i][1]);
        }
        setRestoreOptions(vwOptions);
        return vwOptions;
    }
//------------------------------------------------------------------------------
    public String[][] getATOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        String[][] vwOptions=ATOptions;
        int Options_No=ATOptions.length;
        for(int i=0;i<Options_No;i++)
        {
            vwOptions[i][1]=prefs.get(vwOptions[i][0],vwOptions[i][1]);
        }
        setATOptions(vwOptions);
        return vwOptions;
    }
//------------------------------------------------------------------------------
 private void setViewWiseOptions(String[][] vwOptions)
    {
	 	//CV10.1 commented for saveAppSetting,saveLastDSNName,confirmDeleteLogFile,setProxyEnable,setProxy
        confirmDelDocType=vwOptions[0][1].equals(YesNoData[0]);
        confirmUnusedIndex=vwOptions[1][1].equals(YesNoData[0]);
		//saveAppSetting =vwOptions[2][1].equals(YesNoData[0]);
        saveDialogPos=vwOptions[2][1].equals(YesNoData[0]);
        saveLastUserName=vwOptions[3][1].equals(YesNoData[0]);
        //saveLastDSNName=vwOptions[5][1].equals(YesNoData[0]);
        try
        {
            searchHits=VWUtil.to_Number(vwOptions[4][1]);
        }
        catch(Exception e){recycleList=1000;}
        try
        {
        	recycleList=VWUtil.to_Number(vwOptions[5][1]);
        	if(recycleList==0)
        		recycleList = 1000;
        }
        catch(Exception e){recycleList=1000;}
        confirmDelStorage=vwOptions[6][1].equals(YesNoData[0]);
        confirmDelIndex=vwOptions[7][1].equals(YesNoData[0]);
        confirmATDelFromDB=vwOptions[8][1].equals(YesNoData[0]);
        confirmDelRecycleDoc=vwOptions[9][1].equals(YesNoData[0]);
        confirmImportTemplateFiles=vwOptions[10][1].equals(YesNoData[0]);
        confirmDisconectUser=vwOptions[11][1].equals(YesNoData[0]);
        confirmUnlockDocument=vwOptions[12][1].equals(YesNoData[0]);
       // confirmDeleteLogFile=vwOptions[15][1].equals(YesNoData[0]);
        confirmRestoreDocuments=vwOptions[13][1].equals(YesNoData[0]);
        confirmEnableSIC=vwOptions[14][1].equals(YesNoData[0]);
       /* 
        if(vwOptions[18][1].equals(YesNoData[0]))
        {
            AdminWise.gConnector.ViewWiseClient.setProxyEnable(true);
            AdminWise.gConnector.ViewWiseClient.setProxy(vwOptions[19][1],
                VWUtil.to_Number(vwOptions[20][1]));
        }
        else
        {
            AdminWise.gConnector.ViewWiseClient.setProxyEnable(false);
        }*/
    }
//------------------------------------------------------------------------------
    private void setBackupOptions(String[][] vwOptions)
    {
        backupPath=vwOptions[0][1];
        backupType=vwOptions[1][1].equals(BackupType[0]);
        backupWithVR=vwOptions[2][1].equals(YesNoData[0]);
        backupWithAT=vwOptions[3][1].equals(YesNoData[0]);
        backupWithReferences=vwOptions[4][1].equals(YesNoData[0]);
        backupWithHTML=vwOptions[5][1].equals(YesNoData[0]);
        backupWithMiniViewer=vwOptions[6][1].equals(YesNoData[0]);
        backupWithUnencryptedPages=vwOptions[7][1].equals(YesNoData[0]);
        backupPageText=vwOptions[8][1].equals(YesNoData[0]);
    }
//------------------------------------------------------------------------------
    private void setRestoreOptions(String[][] vwOptions)
    {
        restoreLogPath=vwOptions[0][1];
        for(int i=0;i<RestoreType.length;i++)
        {
            if(vwOptions[1][1].equals(RestoreType[i]))
            {
                restoreType=i;
                break;
            }
        }
        restoreWithVR=vwOptions[2][1].equals(YesNoData[0]);
        restoreWithAT=vwOptions[3][1].equals(YesNoData[0]);
        restoreWithFolders=vwOptions[4][1].equals(YesNoData[0]);
        for(int i=0;i<RestoreSequenceSeed.length;i++)
        {
            if(vwOptions[5][1].equals(RestoreSequenceSeed[i]))
            {
                restoreSequenceSeed=i;
                break;
            }
        }
        restoreWriteInfoLog=vwOptions[6][1].equals(YesNoData[0]);
 // Added By Mallikarjuna to Restore Created and Modifed Dates 
        restoreCreatedDate=vwOptions[7][1].equals(YesNoData[0]);   
    }
//------------------------------------------------------------------------------
    /*  
    *  saveATOptions method changed to fix issue no 870. To generate multiple out put files max no. of lines 
    *  per file is got from user.*/
    private void setATOptions(String[][] vwOptions)
    {
        atArchivePath=vwOptions[0][1];
        try{
        	//maxNoOfLines = Integer.parseInt(vwOptions[1][1]);
        }catch(Exception ex){
        	maxNoOfLines = 10000;
        }
    }
//------------------------------------------------------------------------------
	public static boolean confirmDelDocType=true;
    public static boolean confirmUnusedIndex=true;
    public static boolean saveAppSetting=true;
    public static boolean saveDialogPos=true;
    public static boolean saveLastUserName=true;
    public static boolean saveLastDSNName=true;
    public static int     searchHits=1000;
    public static int     recycleList=1000;
    public static boolean confirmDelStorage=true;
    public static boolean confirmDelIndex=true;
    public static boolean confirmATDelFromDB=true;
    public static boolean confirmDelRecycleDoc=true;
    public static boolean confirmImportTemplateFiles=true;
    public static boolean confirmDisconectUser=true;
    public static boolean confirmUnlockDocument=true;
    public static boolean confirmDeleteLogFile=true;
    public static boolean confirmRestoreDocuments=true;
    public static boolean confirmEnableSIC=true;

    public static String  backupPath="";
    public static boolean backupType=false;
    public static boolean backupWithVR=true;
    public static boolean backupWithAT=true;
    public static boolean backupWithHTML=true;
    public static boolean backupWithReferences=true;
    public static boolean backupWithMiniViewer=false;
    public static boolean backupWithUnencryptedPages=false;
    public static boolean backupPageText=false;

    public static String restoreLogPath="";
    public static int restoreType=1;
    public static boolean restoreWithVR=true;
    public static boolean restoreWithAT=true;
    public static boolean restoreWithFolders=true;
    public static int restoreSequenceSeed=0;
    public static boolean restoreWriteInfoLog=false;
    public static boolean restoreReferences=true;
    public static boolean restoreCreatedDate=false;
    private static int toggleFlagValue;
    private static int tabStatus;
    public static String  atArchivePath="";
    public static int     maxNoOfLines=10000;
}