package ViewWise.AdminWise.VWPanel;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;

public class VWPicturePanel extends JPanel{

    javax.swing.JLabel pic = new javax.swing.JLabel();
    javax.swing.JLabel panelLabel = new javax.swing.JLabel();
    JLabel lineBorderLabel =  new JLabel("", JLabel.CENTER);
    String languageLocale=AdminWise.getLocaleLanguage();
    public VWPicturePanel(String panelName, ImageIcon icon) {
	// TODO Auto-generated constructor stub	
	setLayout(null);
	setBackground(AdminWise.getAWColor());
	pic.setBounds(4, 0, 32, 32);
	add(pic);
	if(languageLocale.equals("nl_NL")){
	panelLabel.setBounds(6, 36, 120+25, 20);
	}else{
		panelLabel.setBounds(6, 36, 120, 20);
	}
		
	add(panelLabel);
	pic.setIcon(icon);
	panelLabel.setText(panelName);
	lineBorderLabel.setBounds(8, 60, 130, 2);
	add(lineBorderLabel);
	lineBorderLabel.setBorder(BorderFactory.createLineBorder(new Color(53, 140, 190), 2));
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub
	JFrame frame = new JFrame();
	new VWImages();
	frame.add(new VWPicturePanel(AdminWise.connectorManager.getString("VWPicturePanel.lbl"), VWImages.DocTypeImage));
	frame.setVisible(true);
	frame.setSize(150, 92);
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

}
