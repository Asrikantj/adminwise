/**
 * @author apurba.m
 * This class is created for new Form Template Panel on Adminwise
 */

package ViewWise.AdminWise.VWFormTemplate;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.resource.ResourceManager;

import com.computhink.common.util.JTextFieldLimit;
import com.computhink.vwc.Session;

public class VWFormTemplatePanel extends JPanel implements VWConstant{
	private static final long serialVersionUID = 1L;
	String sourcePath="";
	public VWFormTemplatePanel() {
		setupUI();
	}

	public void setupUI() {
		try{
			int top = 116; int hGap = 28;
			if (UILoaded)
				return;
			int height = 24, width = 144, left = 12, heightGap = 30;
			int x=160, y=100, h=20, gap=10, x1=250;
			setLayout(new BorderLayout());
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			
			PanelFormTemplateSetting.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(0.5f)));
			BtnSave.setVisible(false);
			BtnCancel.setVisible(false);
			BtnSave.setFocusable(false);
			BtnCancel.setFocusable(false);

			VWFormTemplateSettings.setLayout(null);
			add(VWFormTemplateSettings, BorderLayout.CENTER);
			PanelCommand.setLayout(null);
			VWFormTemplateSettings.add(PanelCommand);
			PanelCommand.setBackground(AdminWise.getAWColor());
			PanelCommand.setBounds(0, 0, 168, 432);

			PanelCommand.add(picPanel);
			picPanel.setBounds(8,16,149,92);
			lblAvailableTemplate.setText(LBL_AVAILABLE_TEMPLATES);
			PanelCommand.add(lblAvailableTemplate);
			lblAvailableTemplate.setBackground(java.awt.Color.white);
			lblAvailableTemplate.setBounds(left, top, width, height);
			
			String lblHeadingString="<html><h4>Template Configuration</h4></html>";
			lblHeading.setText(lblHeadingString);
			PanelFormTemplateSetting.add(lblHeading);
			lblHeading.setBounds(242, 65, 160, 20);
			
			lineBorderLabel.setBounds(242, 90, 140, 2);
			PanelFormTemplateSetting.add(lineBorderLabel);
			lineBorderLabel.setBorder(BorderFactory.createLineBorder(new Color(53, 140, 190), 2));

			top += hGap;
			PanelCommand.add(cmbAvaialbleTemplate);
			cmbAvaialbleTemplate.setBackground(java.awt.Color.white);
			cmbAvaialbleTemplate.setBounds(left-1, top, width + 4, height - 2);

			top += hGap + 12;
			BtnNewTemplate.setText(BTN_NEW_TEMPLATE_NAME);
			BtnNewTemplate.setIcon(VWImages.addNewTemplateIcon); 
			BtnNewTemplate.setActionCommand(BTN_NEW_TEMPLATE_NAME);
			PanelCommand.add(BtnNewTemplate);
			BtnNewTemplate.setBounds(left, top, width, height);
			BtnNewTemplate.setVisible(true);

			top += hGap;
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
			PanelCommand.add(BtnUpdate);
			BtnUpdate.setBounds(left, top, width, height);
			BtnUpdate.setIcon(VWImages.UpdateIcon);

			top += hGap;
			BtnDelete.setText(BTN_DELETE_NAME);
			BtnDelete.setActionCommand(BTN_DELETE_NAME);
			PanelCommand.add(BtnDelete);
			BtnDelete.setBounds(left, top, width, height);
			BtnDelete.setIcon(VWImages.DelIcon);
			

			PanelTable.setLayout(new BorderLayout());
			VWFormTemplateSettings.add(PanelTable);
			PanelTable.setBounds(168, 0, 700,500);
			PanelTable.setBackground(java.awt.Color.white);
			PanelTable.setBorder(BorderFactory.createBevelBorder(10));
			
			PanelFormTemplateSetting.setLayout(null);
			PanelFormTemplateSetting.setBounds(0, 0, 700, 700);
			PanelTable.add(PanelFormTemplateSetting, BorderLayout.CENTER);
			PanelFormTemplateSetting.setBackground(Color.WHITE);
			
			String labelName= "<html>"+AdminWise.connectorManager.getString("FormTemplate.fromTemplateName")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font color='red'>(*)</font>"+"<b>&nbsp;&nbsp  :</html>";
			lblFormTemplateName.setText(labelName);
			lblFormTemplateName.setVisible(true);
			PanelFormTemplateSetting.add(lblFormTemplateName);
			lblFormTemplateName.setBounds(45, 144-2-10-1, 200, h);
			
			PanelFormTemplateSetting.add(txtFormTemplateName);
			txtFormTemplateName.setBackground(java.awt.Color.white);
			txtFormTemplateName.setBounds(240, 134, 300, h);
			txtFormTemplateName.setDocument(new JTextFieldLimit(50));
			txtFormTemplateName.setToolTipText(AdminWise.connectorManager.getString("VWFormTemplatePanel.txtFormTemplateName"));
			txtFormTemplateName.setEnabled(false);

			String labelFormName= "<html>"+AdminWise.connectorManager.getString("FormTemplate.blankpdf")+"&nbsp;&nbsp;&nbsp; <font color='red'>(*)</font>"+"<b>&nbsp;&nbsp  :</b></html>";
			lblSelectForm.setText(labelFormName);
			PanelFormTemplateSetting.add(lblSelectForm);
			lblSelectForm.setBounds(45, 220-2, 200, h);
			
			
			comboSelectForm.addItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
			PanelFormTemplateSetting.add(comboSelectForm);
			comboSelectForm.setBackground(java.awt.Color.white);
			comboSelectForm.setBounds(240, 220, 300, h);
			comboSelectForm.setEnabled(false);
			
			String labelSource="<html>"+AdminWise.connectorManager.getString("FormTemplate.sourceLocation")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='red'>(*)</font>"+"<b>&nbsp;&nbsp  :</b></html>";
			lblSourceLocation.setText(labelSource);
			PanelFormTemplateSetting.add(lblSourceLocation);
			lblSourceLocation.setBounds(45, 178-2-2, 200, h);
			lblUNC1.setText("(In UNC Format)");
			PanelFormTemplateSetting.add(lblUNC1);
			lblUNC1.setBounds(48, 188, 200, h);
			
			String labelDest="<html>"+AdminWise.connectorManager.getString("FormTemplate.destinationLocation")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='red'>(*)</font>"+"<b>&nbsp;&nbsp  :</b></html>";
			lblDestinationLocation.setText(labelDest);
			PanelFormTemplateSetting.add(lblDestinationLocation);
			lblDestinationLocation.setBounds(45, 254-2+10-2, 200, h);
			
			lblUNC.setText("(In UNC Format)");
			PanelFormTemplateSetting.add(lblUNC);
			lblUNC.setBounds(48, 264-2+10, 200, h);
			
			PanelFormTemplateSetting.add(txtSourceLocation);
			txtSourceLocation.setBackground(java.awt.Color.white);
			txtSourceLocation.setBounds(240, 178, 300, h);
			txtSourceLocation.setEnabled(false);
		
			
			
			PanelFormTemplateSetting.add(txtDestinationLocation);
			txtDestinationLocation.setBackground(java.awt.Color.white);
			txtDestinationLocation.setBounds(240, 254+10, 300, h);
			txtDestinationLocation.setEnabled(false);
			
			BtnSave.setText(SAVE_FORM_TEMPLATE);
			BtnSave.setActionCommand(SAVE_FORM_TEMPLATE);
			PanelFormTemplateSetting.add(BtnSave);
			BtnSave.setBounds(240-9, 274+25+5, 80, height);
			BtnSave.setIcon(VWImages.SaveIcon);
			
			BtnCancel.setText(CANCEL_FORM_TEMPLATE);
			BtnCancel.setActionCommand(CANCEL_FORM_TEMPLATE);
			PanelFormTemplateSetting.add(BtnCancel);
			BtnCancel.setBounds(300-9, 274+25+5, 80, height);
			BtnCancel.setIcon(VWImages.CloseIcon);

			setEnableMode(MODE_UNCONNECT);


			PanelFormTemplateSetting.setVisible(true);
			PanelFormTemplateSetting.setEnabled(true);

			SymComponent aSymComponent = new SymComponent();
			VWFormTemplateSettings.addComponentListener(aSymComponent);
			SysItem sysItem=new SysItem();
			
			txtSourceLocation.addFocusListener(sysItem);
			SymAction lSymAction = new SymAction();
			SymItem symItem = new SymItem();
			BtnNewTemplate.addActionListener(lSymAction);
			BtnUpdate.addActionListener(lSymAction);
			BtnDelete.addActionListener(lSymAction);
			BtnSave.addActionListener(lSymAction);
			BtnCancel.addActionListener(lSymAction);
			cmbAvaialbleTemplate.addItemListener(symItem);
			PanelFormTemplateSetting.setAutoscrolls(true);
			PanelFormTemplateSetting.setPreferredSize(new Dimension(900, 640));	
			repaint();
			doLayout();		
			UILoaded = true;
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while setting UI : "+ex.getMessage());
		}
	}

	public void fetchTemplateDetails (){
		AdminWise.printToConsole("Inside fetchTemplateDetails...........");
		
		Vector templateDetails= new Vector();
		String templateName= (String) cmbAvaialbleTemplate.getSelectedItem();
		if(!templateName.equalsIgnoreCase(AdminWise.connectorManager.getString("constant.LBL_AVAILABLE_TEMPLATES")) && !templateName.equalsIgnoreCase(AdminWise.connectorManager.getString("FormTemplate.refresh"))) {
			AdminWise.printToConsole("templateName Inside fetchTemplateDetails :::: "+templateName);
			int retVal= AdminWise.gConnector.ViewWiseClient.CVGetDTFormTemplateDetails(session.sessionId, templateName, templateDetails);
			AdminWise.printToConsole("templateDetails size inside fetchTemplateDetails after calling VWCLIENT :::: "+templateDetails.size());
			AdminWise.printToConsole("templateDetails inside fetchTemplateDetails after calling VWCLIENT :::: "+templateDetails);
			if(templateDetails.size() > 0){
				StringTokenizer st= new StringTokenizer(templateDetails.get(0).toString(), "\t");
				 templateId= Integer.parseInt(st.nextToken());
				String formTemplateName= st.nextToken();
				txtFormTemplateName.setText(formTemplateName);
				String formName= st.nextToken();
				AdminWise.printToConsole("formName after tokenization  :::: "+formName);
				AdminWise.printToConsole("comboSelectForm.getItemCount():::::"+comboSelectForm.getItemCount());
				//comboSelectForm.setSelectedItem(formName);
				AdminWise.printToConsole("comboSelectForm.getselectedItem:::"+comboSelectForm.getSelectedItem().toString());
				formTemplatePath= st.nextToken();
				AdminWise.printToConsole("formTemplatePath before settign ......."+formTemplatePath);
				fetchFormTemplateFiles(formTemplatePath);
				txtSourceLocation.setText(formTemplatePath);
				String destinationLocation= st.nextToken();
				txtDestinationLocation.setText(destinationLocation);
				AdminWise.printToConsole("formName::::::::::::::"+formName);
				comboSelectForm.setSelectedItem(formName);
			}
		} else if (templateName.equalsIgnoreCase(AdminWise.connectorManager.getString("FormTemplate.refresh"))) {
			cmbAvaialbleTemplate.setSelectedItem(AdminWise.connectorManager.getString("constant.LBL_AVAILABLE_TEMPLATES"));
			fetchAllFormTemplates();
		} else {
			txtFormTemplateName.setText("");
			comboSelectForm.setSelectedItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
			txtDestinationLocation.setText("");
			if(cmbAvaialbleTemplate.getSelectedItem().toString().equalsIgnoreCase(AdminWise.connectorManager.getString("constant.LBL_AVAILABLE_TEMPLATES"))){/*
				BtnDelete.setEnabled(false);
				BtnUpdate.setEnabled(false);
				BtnCancel.setVisible(false);
				BtnSave.setVisible(false);
				BtnNewTemplate.setEnabled(true);
				txtFormTemplateName.setText("");
				txtFormTemplateName.setEnabled(false);
				txtDestinationLocation.setText("");
				txtDestinationLocation.setEnabled(false);
				comboSelectForm.setSelectedItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
				comboSelectForm.setEnabled(false);
			*/}else{
				BtnDelete.setEnabled(true);
				BtnUpdate.setEnabled(false);
				
			}
		}
	}
	public boolean checkTemplatesExist(String serverPath){
		if(serverPath.contains("&#40;"))
			serverPath=serverPath.replace("&#40;","(");
		if(serverPath.contains("&#41;"))
			serverPath=serverPath.replace("&#41;",")");
		AdminWise.printToConsole("serverPath within ::: " + serverPath);
		File folder = new File(serverPath.trim());
		AdminWise.printToConsole("Folder::::" + folder);

		File[] listOfFiles = folder.listFiles();
		

		if(listOfFiles!=null&&listOfFiles.length>0){
			AdminWise.printToConsole("listOfFiles is true::::" + listOfFiles);
			return true;
		}else{
			AdminWise.printToConsole("listOfFiles is false::::");
			return false;
		}
	}

	public void fetchFormTemplateFiles(String serverPath){
		comboSelectForm.removeAllItems();
		if(serverPath.contains("&#40;"))
			serverPath=serverPath.replace("&#40;","(");
		if(serverPath.contains("&#41;"))
			serverPath=serverPath.replace("&#41;",")");
		AdminWise.printToConsole("serverPath within ::: " + serverPath);
		Vector templateNameListVector = new Vector<>();
		File folder = new File(serverPath.trim());
		AdminWise.printToConsole("Folder::::" + folder);

		File[] listOfFiles = folder.listFiles();
		AdminWise.printToConsole("listOfFiles::::" + listOfFiles);

		String ext = null; 
		if(listOfFiles!=null&&listOfFiles.length>0){
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				AdminWise.printToConsole("File ::::" + listOfFiles[i].getName());
				if (listOfFiles[i].getName().endsWith(".pdf") || listOfFiles[i].getName().endsWith(".PDF")) {
					templateNameListVector.add(listOfFiles[i].getName());
				}
			}
		}
		}

		AdminWise.printToConsole("templateNameListVector after adding file names :::: "+templateNameListVector);
		comboSelectForm.addItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
		for(int j=0; j<templateNameListVector.size(); j++){
			comboSelectForm.addItem(templateNameListVector.get(j));
		}
	}

	public void fetchAllFormTemplates(){
		Vector templateList= new Vector();
		int retVal = AdminWise.gConnector.ViewWiseClient.CVGetDTFormTemplate(session.sessionId, templateList);
		AdminWise.printToConsole("retVal inside fetchAllFormTemplates after calling VWCLIENT :::: "+retVal);
		AdminWise.printToConsole("templateList inside fetchAllFormTemplates after calling VWCLIENT :::: "+templateList);
		cmbAvaialbleTemplate.removeAllItems();
		cmbAvaialbleTemplate.addItem(LBL_AVAILABLE_TEMPLATES);
		if(templateList.size() > 0){
			for(int i=0; i<templateList.size(); i++) {
				StringTokenizer st= new StringTokenizer(templateList.get(i).toString(), "\t");
				 templateId= Integer.parseInt(st.nextToken());
				String templateName= st.nextToken();
				cmbAvaialbleTemplate.addItem(templateName);
			}
			cmbAvaialbleTemplate.addItem(AdminWise.connectorManager.getString("FormTemplate.refresh"));
		}
	}
	
	protected int testSrcFolder() {
		File file = new File(txtSourceLocation.getText());
		AdminWise.printToConsole("source path inside file ...."+file.getAbsolutePath());
		if(!file.isDirectory()){
			return -2;
		} else if(!file.canRead() || !file.canWrite()){
			return -3;
		}else if(!txtSourceLocation.getText().startsWith("\\\\") || txtSourceLocation.getText().contains(":")){
			return -4;			
		}else
		return 1;
	}

	
	

	protected int testFolder() {
		File file = new File(txtDestinationLocation.getText());
		File testfolder= new File(txtDestinationLocation.getText()+"\\Test Folder");
		if(txtDestinationLocation.getText().length() <= 0){
			return -1;
		} else if(!file.isDirectory()){
			return -2;
		} else if(!file.canRead() || !file.canWrite()){
			return -3;
		}else if(!txtDestinationLocation.getText().startsWith("\\\\") || txtDestinationLocation.getText().contains(":")){
			return -4;			
		}else {
			testfolder.mkdir();
			if(testfolder.exists()){
				if(testfolder.delete()){
					//JOptionPane.showMessageDialog(null, "Destination folder is accessable");
					return 1;
				}
			}
			return 1;
		}
	}


	public void setEnableMode(int mode) {
		AdminWise.printToConsole("Mode :: "+mode);	
		switch (mode) {
		case MODE_CONNECT:
			CURRENT_MODE = 0;
			cmbAvaialbleTemplate.setEnabled(true);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setVisible(true);
			BtnDelete.setEnabled(true);			
			lblAvailableTemplate.setVisible(true);
			cmbAvaialbleTemplate.setVisible(true);
			BtnNewTemplate.setEnabled(true);
			//fetchAllFormTemplates();
			//viewMode(false);
			break;

		case MODE_UNSELECTED:
			BtnNewTemplate.setEnabled(true);
			BtnUpdate.setEnabled(true);			
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(true);			
			BtnDelete.setVisible(true);
			lblAvailableTemplate.setVisible(true);
			cmbAvaialbleTemplate.setEnabled(true);
			cmbAvaialbleTemplate.setVisible(true);
			if(cmbAvaialbleTemplate.getSelectedItem().toString().equalsIgnoreCase(LBL_AVAILABLE_TEMPLATES)){
				BtnDelete.setEnabled(false);
				BtnUpdate.setEnabled(false);
			}else{
				BtnDelete.setEnabled(true);
				BtnUpdate.setEnabled(true);
			}
			//viewMode(false);
			break;

		case MODE_SELECT:
			BtnNewTemplate.setEnabled(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(true);
			BtnDelete.setVisible(true);
			//viewMode(false);
			lblAvailableTemplate.setVisible(true);
			cmbAvaialbleTemplate.setVisible(true);



			break;
			
		case MODE_DELETE:
			BtnNewTemplate.setEnabled(true);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(true);
			BtnDelete.setVisible(true);
			//viewMode(false);
			lblAvailableTemplate.setVisible(true);
			cmbAvaialbleTemplate.setVisible(true);
			break;

		case MODE_UNCONNECT:
			CURRENT_MODE = 0;
			cmbAvaialbleTemplate.removeAllItems();
			cmbAvaialbleTemplate.addItem(LBL_AVAILABLE_TEMPLATES);
			cmbAvaialbleTemplate.setVisible(true);

			BtnNewTemplate.setEnabled(false);
			BtnNewTemplate.setVisible(true);

			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(false);
			BtnDelete.setVisible(true);
			
			lblAvailableTemplate.setVisible(true);
			cmbAvaialbleTemplate.setEnabled(false);
			txtFormTemplateName.setText("");
			txtFormTemplateName.setEnabled(false);
			txtDestinationLocation.setText("");
			txtSourceLocation.setText("");
			txtSourceLocation.setEnabled(false);
			txtDestinationLocation.setEnabled(false);
			comboSelectForm.setEnabled(false);
			BtnSave.setEnabled(false);
			BtnSave.setVisible(false);
			BtnCancel.setEnabled(false);
			BtnCancel.setVisible(false);
			gCurRoom = 0;			
			break;

		case MODE_NEW:
			BtnNewTemplate.setEnabled(true);
			BtnNewTemplate.setText(BTN_SAVE_NAME);
			BtnNewTemplate.setIcon(VWImages.SaveIcon);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			BtnDelete.setEnabled(false);
			lblAvailableTemplate.setVisible(false);
			cmbAvaialbleTemplate.setVisible(false);
			//viewMode(true);			
			break;		
			
		case MODE_UPDATE:
			BtnNewTemplate.setEnabled(true);
			BtnNewTemplate.setText(BTN_SAVE_NAME);
			BtnNewTemplate.setIcon(VWImages.SaveIcon);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			BtnDelete.setEnabled(false);
			lblAvailableTemplate.setVisible(false);
			cmbAvaialbleTemplate.setVisible(false);
			//viewMode(true);			
			break;		
		}		
	}


	public void loadTabData(VWRoom newRoom)
	{
		try{
			if(newRoom.getConnectStatus()!=Room_Status_Connect)
			{
				setEnableMode(MODE_UNCONNECT);
				return;
			}
			if(newRoom.getId()==gCurRoom)return;
			gCurRoom=newRoom.getId();
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			fetchAllFormTemplates();
			UILoaded = true;
			setEnableMode(MODE_UNSELECTED);
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while loading tab data : "+ex.getMessage());
		}
	}

	public void unloadTabData() {
		try{
			repaint();
			gCurRoom = 0;
			setEnableMode(MODE_UNCONNECT);
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while unloading tab data: "+ex.getMessage());
		}
	}

	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnNewTemplate) {
				BtnAddNewTemplate_actionPerformed(event);
			}else if (object == BtnUpdate) {
				BtnUpdate_actionPerformed(event);
			} else if (object == BtnDelete) {
				BtnDelete_actionPerformed(event);
			} else if (object == BtnSave) {
				BtnSaveFormTemplate_actionPerformed(event);
			} else if (object == BtnCancel) {
				BtnCancelFormTemplate_actionPerformed(event);
			}
		}

		public void BtnAddNewTemplate_actionPerformed(ActionEvent event) {
			Vector serverPath= new Vector();
			txtFormTemplateName.setEnabled(true);
			txtFormTemplateName.setText("");
			comboSelectForm.setEnabled(true);
			comboSelectForm.removeAllItems();
			comboSelectForm.addItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
			comboSelectForm.setSelectedItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
			txtDestinationLocation.setEnabled(true);
			txtSourceLocation.setEnabled(true);
			txtSourceLocation.setEditable(true);
			txtDestinationLocation.setText("");
			txtSourceLocation.setText("");
			cmbAvaialbleTemplate.setEnabled(false);
			cmbAvaialbleTemplate.setSelectedItem(LBL_AVAILABLE_TEMPLATES);
			BtnUpdate.setEnabled(false);
			BtnDelete.setEnabled(false);	
			BtnSave.setVisible(true);
			BtnSave.setEnabled(true);
			BtnCancel.setVisible(true);
			BtnSave.setEnabled(true);
			BtnCancel.setEnabled(true);
			//fetchFormTemplateFiles(sourcePath);
			newClicked=true;
			updateClicked=false;
		}
		
		public void BtnUpdate_actionPerformed(ActionEvent event){
			Vector serverPath= new Vector();
			if(cmbAvaialbleTemplate.getSelectedItem().toString().equalsIgnoreCase(LBL_AVAILABLE_TEMPLATES)){
				JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.modifyvalidation"));
				BtnNewTemplate.setEnabled(true);
			} else { 
				txtFormTemplateName.setEnabled(true);
				comboSelectForm.setEnabled(true);
				comboSelectForm.setSelectedItem(comboSelectForm.getSelectedItem());
				txtDestinationLocation.setEnabled(true);
				txtSourceLocation.setEnabled(true);
				txtSourceLocation.setEditable(true);
				cmbAvaialbleTemplate.setEnabled(true);
				BtnNewTemplate.setEnabled(false);
				BtnUpdate.setEnabled(true);
				BtnDelete.setEnabled(false);	
				BtnSave.setVisible(true);
				BtnSave.setEnabled(true);
				BtnCancel.setVisible(true);
				BtnCancel.setEnabled(true);
				fetchTemplateDetails();
				updateClicked=true;
				newClicked=false;
			}
		}
		
		public void BtnDelete_actionPerformed(ActionEvent event){
			Vector result = new Vector();
			int ret=0;
			int retVal = VWMessage.showConfirmDialog(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWFormTemplatePanel.deletemsg"));
			if (retVal == 0) {
			if(cmbAvaialbleTemplate.getSelectedItem().toString().equalsIgnoreCase(LBL_AVAILABLE_TEMPLATES)){
				JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.deletevalidation"));
			} else {
				ret= AdminWise.gConnector.ViewWiseClient.CVDelDTFormTemplate(session.sessionId, cmbAvaialbleTemplate.getSelectedItem().toString(), result);
				AdminWise.printToConsole("ret after calling VWClient method inside BtnDelete_actionPerformed :::: "+ret);
			}
			if(ret == 1){
				JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.deletedSucess"));
				txtFormTemplateName.setText("");
				txtFormTemplateName.setEnabled(false);
				comboSelectForm.setSelectedItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
				comboSelectForm.setEnabled(false);
				txtDestinationLocation.setText("");
				txtSourceLocation.setText("");
				txtDestinationLocation.setEnabled(false);
				txtSourceLocation.setEnabled(false);
				cmbAvaialbleTemplate.setSelectedItem(LBL_AVAILABLE_TEMPLATES);
				fetchAllFormTemplates();
			}
			}
		}
		
		public void BtnSaveFormTemplate_actionPerformed(ActionEvent event) {
			//AdminWise.printToConsole("comboSelectForm.getSelectedIndex() :::: "+comboSelectForm.getSelectedIndex());
			String templateNameValue= txtFormTemplateName.getText();
			String formTemplatePath_save=txtSourceLocation.getText();
			String formNameValue= comboSelectForm.getSelectedItem().toString();
			int formFileNo= comboSelectForm.getSelectedIndex();
			String destinationValue= txtDestinationLocation.getText();
			comboSelectForm.setSelectedItem(comboSelectForm.getSelectedItem().toString());
			comboSelectForm.setSelectedIndex(formFileNo);
			//AdminWise.printToConsole("templateNameValue ............"+templateNameValue);
			//AdminWise.printToConsole("formTemplatePath ............"+formTemplatePath_save);
			//AdminWise.printToConsole("comboSelectForm.getselected item formNameValue ............"+formNameValue);
			//AdminWise.printToConsole("comboSelectForm.getselected index....."+comboSelectForm.getSelectedIndex());
			//AdminWise.printToConsole("comboSelectForm.getselected index....."+formFileNo);
			//AdminWise.printToConsole("destinationValue ............"+destinationValue);
			
			Vector result= new Vector();
			int retValue= 0;
			int saveRetValue= 0;
			int chkDestPath= testFolder();
			int chkSrcPath=testSrcFolder();
			AdminWise.printToConsole("chkSrcPath.........."+chkSrcPath);
			
		
			if(formTemplatePath_save.length()<=0){
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.sourcevalidation"));
			}
			else if(templateNameValue.length()<=0){
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.emptycheck"));
			}  else if(chkSrcPath==-2){
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.srcPathValid"));
			}else if(chkSrcPath==-3){
				JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.srcaccessable"));
			}else if(checkTemplatesExist(formTemplatePath_save)==false){
				AdminWise.printToConsole("checkTemplatesExist is false...");
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.nopdf"));
				return;
			}else if(chkDestPath == -1){
				JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.destPath"));
			} else if(chkDestPath == -2) {
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.destPathValid"));
			} else if(chkDestPath == -3) {
				JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.destnotaccessable"));				
			} else if(chkDestPath == -4||chkSrcPath==-4){
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.uncpathcheck"));
			} else if(formNameValue.equalsIgnoreCase(AdminWise.connectorManager.getString("FormTemplate.selectemplate"))){
				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.selectpdf"));
			}
					
			else {
				AdminWise.printToConsole("Inside else newClicked....."+newClicked);
				if(	newClicked==true) {
					newClicked=false;
					AdminWise.printToConsole("Inside if :::::::::");
					retValue= AdminWise.gConnector.ViewWiseClient.CVCheckForTemplateExist(session.sessionId, templateNameValue, result);
					AdminWise.printToConsole("retValue BtnSaveFormTemplate_actionPerformed ::::"+retValue);

					if(retValue == 0){
						
						saveRetValue=AdminWise.gConnector.ViewWiseClient.CVSetDTFormTemplate(session.sessionId,0,templateNameValue, formNameValue, formTemplatePath_save, destinationValue, result);
						if(saveRetValue > 0){
							JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.saved"));
							txtFormTemplateName.setText("");
							txtFormTemplateName.setEnabled(false);
							txtDestinationLocation.setText("");
							txtSourceLocation.setText("");
							txtDestinationLocation.setEnabled(false);
							txtSourceLocation.setEnabled(false);
							comboSelectForm.setSelectedItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
							comboSelectForm.setEnabled(false);
							cmbAvaialbleTemplate.setSelectedItem(LBL_AVAILABLE_TEMPLATES);
							BtnSave.setVisible(false);
							BtnCancel.setVisible(false);
							BtnNewTemplate.setEnabled(true);
							BtnUpdate.setEnabled(true);
							BtnDelete.setEnabled(true);
							cmbAvaialbleTemplate.setEnabled(true);
							fetchAllFormTemplates();
						} else if(saveRetValue == -1){
							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.saveError"));
						}
					} else if(retValue > 0){
						JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.dupicatTemplate"));
					} 
				} else if(updateClicked==true){
					AdminWise.printToConsole("Inside else :::::::::");
					updateClicked=false;
					String templateName=txtFormTemplateName.getText().toString();
					//fetchFormTemplateFiles(templateName);
					//AdminWise.printToConsole("templateName :::::::::"+templateName);
					//AdminWise.printToConsole("comboSelectForm.getSize() :::: " + comboSelectForm.getSize());
					//AdminWise.printToConsole("comboSelectForm.getSelectedItem().toString() :::::::::"+comboSelectForm.getSelectedItem().toString());
					//AdminWise.printToConsole("comboSelectForm.getselected index....."+comboSelectForm.getSelectedIndex());
					comboSelectForm.setSelectedItem(comboSelectForm.getSelectedItem().toString());
					String selectedFrom= comboSelectForm.getSelectedItem().toString();
					//AdminWise.printToConsole("selectedFrom.........."+selectedFrom);
					String destLocation=txtDestinationLocation.getText().toString();
					//AdminWise.printToConsole("destLocation.........."+destLocation);
					int selectedIndex=cmbAvaialbleTemplate.getSelectedIndex();
					//AdminWise.printToConsole("selectedIndex.........."+selectedIndex);
					String selectedTemplateName=cmbAvaialbleTemplate.getSelectedItem().toString();
					AdminWise.printToConsole("selectedTemplateName:::::::::::"+selectedTemplateName);
					saveRetValue=AdminWise.gConnector.ViewWiseClient.CVSetDTFormTemplate(session.sessionId,templateId,templateName, selectedFrom, formTemplatePath_save,destLocation,result);
					if(saveRetValue > 0){
					//int	ret1= AdminWise.gConnector.ViewWiseClient.CVDelDTFormTemplate(session.sessionId, cmbAvaialbleTemplate.getSelectedItem().toString(), result);
						//if(ret1 == 1)
						JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FormTemplate.savedSucess"));
						txtFormTemplateName.setText("");
						txtFormTemplateName.setEnabled(false);
						txtDestinationLocation.setText("");
						txtDestinationLocation.setEnabled(false);
						txtSourceLocation.setText("");
						txtSourceLocation.setEnabled(false);
						comboSelectForm.setSelectedItem(AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
						comboSelectForm.setEnabled(false);
						cmbAvaialbleTemplate.setSelectedItem(LBL_AVAILABLE_TEMPLATES);
						BtnSave.setVisible(false);
						BtnCancel.setVisible(false);
						BtnNewTemplate.setEnabled(true);
						BtnUpdate.setEnabled(true);
						BtnDelete.setEnabled(true);
						cmbAvaialbleTemplate.setEnabled(true);
						fetchAllFormTemplates();
					} else if(saveRetValue == -1){
						JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("FormTemplate.errorSave"));
					}
				}
			}
		}
		
		public void BtnCancelFormTemplate_actionPerformed(ActionEvent event) {
			if(newClicked==true){
				/*AdminWise.printToConsole("Inside newclicked ::::::::::");
				cmbAvaialbleTemplate.setSelectedItem(AdminWise.connectorManager.getString("constant.LBL_AVAILABLE_TEMPLATES"));
				BtnUpdate.setEnabled(false);
				BtnDelete.setEnabled(false);	*/
				txtFormTemplateName.setText("");
				txtFormTemplateName.setEnabled(false);
				txtDestinationLocation.setText("");
				txtDestinationLocation.setEnabled(false);
				txtSourceLocation.setText("");
				txtSourceLocation.setEnabled(false);
				comboSelectForm.setSelectedItem( AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
				comboSelectForm.setEnabled(false);
				BtnSave.setVisible(false);
				BtnCancel.setVisible(false);
				BtnNewTemplate.setEnabled(true);
				BtnUpdate.setEnabled(false);
				BtnDelete.setEnabled(false);		
				cmbAvaialbleTemplate.setEnabled(true);
				newClicked=false;
			}else if(updateClicked==true){
				AdminWise.printToConsole("Inside updateClicked ::::::::::");
				cmbAvaialbleTemplate.setSelectedItem(AdminWise.connectorManager.getString("constant.LBL_AVAILABLE_TEMPLATES"));
				txtSourceLocation.setText("");
				txtSourceLocation.setEnabled(false);
				BtnUpdate.setEnabled(false);
				BtnDelete.setEnabled(false);	
				updateClicked=false;
			}
			else{txtFormTemplateName.setText("");
			txtFormTemplateName.setEnabled(false);
			txtDestinationLocation.setText("");
			txtDestinationLocation.setEnabled(false);
			txtSourceLocation.setText("");
			txtSourceLocation.setEnabled(false);
			comboSelectForm.setSelectedItem( AdminWise.connectorManager.getString("FormTemplate.selectemplate"));
			comboSelectForm.setEnabled(false);
			BtnSave.setVisible(false);
			BtnCancel.setVisible(false);
			BtnNewTemplate.setEnabled(true);
			BtnUpdate.setEnabled(true);
			BtnDelete.setEnabled(true);		
			cmbAvaialbleTemplate.setEnabled(true);
			}
		}



	}
	
	class SymItem implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent event) {
			try{
				Object object = event.getSource();
				int state = event.getStateChange();
				if(state == ItemEvent.SELECTED){
					if(object == cmbAvaialbleTemplate){
						//AdminWise.printToConsole("Before fetemplateDetails....");
						fetchTemplateDetails();
						if(cmbAvaialbleTemplate.getSelectedItem().toString().equalsIgnoreCase(LBL_AVAILABLE_TEMPLATES)){
							if(updateClicked==true){
								updateClicked=false;
								BtnNewTemplate.setEnabled(true);;
								txtFormTemplateName.setEnabled(false);
								txtDestinationLocation.setEnabled(false);
								txtSourceLocation.setEnabled(false);
								comboSelectForm.setEnabled(false);
								BtnSave.setVisible(false);;
								BtnCancel.setVisible(false);
							}
							BtnUpdate.setEnabled(false);
							BtnDelete.setEnabled(false);	
							txtSourceLocation.setText("");
						}else{
							BtnUpdate.setEnabled(true);
							if(updateClicked==true){
								updateClicked=false;
								//cmbAvaialbleTemplate.setSelectedItem(LBL_AVAILABLE_TEMPLATES);
								BtnNewTemplate.setEnabled(true);
								txtFormTemplateName.setEnabled(false);
								txtDestinationLocation.setEnabled(false);
								txtSourceLocation.setEnabled(false);
								comboSelectForm.setEnabled(false);
								BtnSave.setVisible(false);;
								BtnCancel.setVisible(false);
								BtnDelete.setEnabled(true);
							}else{
								BtnDelete.setEnabled(true);	
							}
						}
					}
				}
			}catch(Exception e){
				AdminWise.printToConsole("Exception in item state changed :: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}

	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
			try {
				PanelFormTemplateSetting.repaint();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,e.toString());
			}
		}
	}

	class SymComponent extends java.awt.event.ComponentAdapter {
		public void componentResized(java.awt.event.ComponentEvent event) {
			Object object = event.getSource();
			if (object == VWFormTemplateSettings)
				VWNotification_componentResized(event);
		}
	}
	
	class SysItem extends java.awt.event.FocusAdapter {
		public void focusLost(java.awt.event.FocusEvent event) {
			AdminWise.printToConsole("Inside foucs lost......."+txtSourceLocation.getText());
			sourcePath=txtSourceLocation.getText();
			fetchFormTemplateFiles(sourcePath);
		}
	}
	
	void VWNotification_componentResized(java.awt.event.ComponentEvent event) {
		if (event.getSource() == VWFormTemplateSettings) {
			Dimension mainDimension = this.getSize();
			Dimension panelDimension = mainDimension;
			Dimension commandDimension = PanelCommand.getSize();
			commandDimension.height = mainDimension.height;
			panelDimension.width = mainDimension.width - commandDimension.width;
			PanelTable.setSize(panelDimension);
			PanelCommand.setSize(commandDimension);
			SPTable.setSize(panelDimension);
			SPTable.revalidate();
			PanelTable.doLayout();								
		}
	}

	//------------------------------------------------------------------------------------------------------------

	public int CURRENT_MODE = 0;
	private static int gCurRoom = 0;
	private boolean UILoaded = false;
	public Session session = null;
	int templateId=0;
	VWPicturePanel picPanel = new VWPicturePanel(TAB_FORM_TEMPLATE_NAME, VWImages.FormTemplateUIImage);
	JPanel VWFormTemplateSettings = new JPanel();
	JPanel PanelCommand = new JPanel();
	JPanel PanelFormTemplateSetting = new JPanel();
	JPanel PanelTable = new JPanel();

	VWLinkedButton BtnNewTemplate = new VWLinkedButton(1);
	VWLinkedButton BtnUpdate = new VWLinkedButton(1);
	VWLinkedButton BtnDelete = new VWLinkedButton(1);
	VWLinkedButton BtnSave = new VWLinkedButton(1);
	VWLinkedButton BtnCancel = new VWLinkedButton(1);
	
	JLabel lblModule = new JLabel();
	JLabel lblName = new JLabel();
	JLabel lblAvailableTemplate = new JLabel();
	JLabel lblFormTemplateName = new JLabel();
	JLabel lblSelectForm = new JLabel();
	JLabel lblSourceLocation = new JLabel();
	JLabel lblDestinationLocation = new JLabel();
	JLabel lblHeading = new JLabel();
	JLabel lblUNC = new JLabel();
	JLabel lblUNC1 = new JLabel();
	JTextField txtFormTemplateName = new JTextField();
	VWComboBox comboSelectForm = new  VWComboBox();
	JTextField txtDestinationLocation = new JTextField();
	JTextField txtSourceLocation = new JTextField();
	VWComboBox cmbAvaialbleTemplate = new VWComboBox();
	boolean updateClicked=false;
	boolean newClicked=false;
	String formTemplatePath=null;
	JLabel lineBorderLabel =  new JLabel("", JLabel.CENTER);
	JScrollPane SPTable = new JScrollPane();

}
