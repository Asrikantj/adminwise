/*
 * Created on Feb 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ViewWise.AdminWise.VWRoom;

import java.awt.CardLayout;
import java.awt.Frame;
import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

/**
 * @author pandiyaraj.m
 *
 * This Class created for display the dialog box for select the rooms and save the registration files.
 * 
 */
public class VWExportInfoDlg  extends javax.swing.JDialog implements  VWConstant, Constants {
	public VWExportInfoDlg(Frame parent,Object roomName)
	{
		super(parent,AdminWise.connectorManager.getString("ExportInfo.Create")+" "+ ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("ExportInfo.RegistrationFile"),true);
		getContentPane().setBackground(AdminWise.getAWColor());
		Vector rooms = new Vector();
		selRoom = (VWRoom)roomName;
		getContentPane().setLayout(new CardLayout(0,0));
		setSize(335,205);
		confirmPanel.setLayout(null);
		getContentPane().add("First",confirmPanel);
		confirmPanel.setBounds(0,0,324,192);
		lblCount.setText(AdminWise.connectorManager.getString("VWExportInfoDlg.FileLocation"));
		confirmPanel.add(lblCount);
		lblCount.setBounds(12,8,300,24);
		confirmPanel.add(txtPath);
		txtPath.setBounds(12,36,276,21);
		txtPath.setEditable(false);
		txtPath.setText("c:\\"+selRoom.getServer()+".vws");
		BtnBrowse.setText("...");
//		BtnBrowse.setBackground(java.awt.Color.white);
		confirmPanel.add(BtnBrowse);
		BtnBrowse.setBounds(288,36,29,21);
		
		confirmPanel.add(LstRooms);
		rooms = getRooms(roomName.toString());
		LstRooms.setBounds(12,70,295,68);
		LstRooms.setBackground(this.getBackground());
		LstRooms.loadData(rooms);
		LstRooms.selectAllItems();
		LstRooms.setVisible(true);	        
		
		BtnOk.setText(BTN_SAVE_NAME);
//		BtnOk.setBackground(java.awt.Color.white);
		confirmPanel.add(BtnOk);
		BtnOk.setBounds(140,150,82,23);
		BtnOk.setIcon(VWImages.SaveIcon);
		BtnCCancel.setText(BTN_CANCEL_NAME);
//		BtnCCancel.setBackground(java.awt.Color.white);
		BtnCCancel.setIcon(VWImages.CloseIcon);
		confirmPanel.add(BtnCCancel);
		BtnCCancel.setBounds(230,150,82,23);
		infoPanel.setLayout(null);
		getContentPane().add("Sec",infoPanel);
		BtnCancel.setText(BTN_CANCEL_NAME);
//		BtnCancel.setBackground(java.awt.Color.white);
		infoPanel.add(BtnCancel);
		BtnCancel.setBounds(120,96,91,23);
		BtnCancel.setIcon(VWImages.CloseIcon);
		
		SymAction lSymAction = new SymAction();
		BtnCancel.addActionListener(lSymAction);
		BtnOk.addActionListener(lSymAction);
		BtnCCancel.addActionListener(lSymAction);
		BtnBrowse.addActionListener(lSymAction);
		SymWindow aSymWindow = new SymWindow();
		addWindowListener(aSymWindow);
		getDlgOptions();
		setResizable(false);
	}
	//	------------------------------------------------------------------------------
	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
			Object object = event.getSource();
			if (object == VWExportInfoDlg.this)
				BtnCancel_actionPerformed(null);
		}
	}
	//	------------------------------------------------------------------------------
	//{{DECLARE_CONTROLS
	javax.swing.JPanel infoPanel = new javax.swing.JPanel();
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
	javax.swing.JLabel LblMain = new javax.swing.JLabel();
	javax.swing.JPanel confirmPanel = new javax.swing.JPanel();
	javax.swing.JLabel lblCount = new javax.swing.JLabel();
	javax.swing.JTextField txtPath = new javax.swing.JTextField();
	VWLinkedButton BtnBrowse = new VWLinkedButton(0,true);
	VWLinkedButton BtnOk = new VWLinkedButton(0,true);
	VWLinkedButton BtnCCancel = new VWLinkedButton(0,true);
	VWCheckList LstRooms = new VWCheckList();
	//	------------------------------------------------------------------------------
	class SymAction implements java.awt.event.ActionListener
	{
		public void actionPerformed(java.awt.event.ActionEvent event)
		{
			Object object = event.getSource();
			if (object == BtnCancel)
				BtnCancel_actionPerformed(event);
			else if (object == BtnOk)
				BtnOk_actionPerformed(event);
			else if (object == BtnCCancel)
				BtnCCancel_actionPerformed(event);
			else if (object == BtnBrowse)
				BtnBrowse_actionPerformed(event);
		}
	}
	//	------------------------------------------------------------------------------
	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
	{
		cancelAction=true;
	}
	//	------------------------------------------------------------------------------
	void BtnCCancel_actionPerformed(java.awt.event.ActionEvent event)
	{
		saveDlgOptions();
		setVisible(false);
	}
	//	------------------------------------------------------------------------------
	void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
	{
		List list=new LinkedList();
		if(selRoom==null) return ;
		String path=txtPath.getText();
		if(path!=null && !path.equals(""))
		{
			String roomInfo=selRoom.getServer()+"@"+selRoom.getServerHost()+":"+
			selRoom.getServerPort();
			List selectedRooms = LstRooms.getSelectedItems();
			String sRooms = "";
			for (int index=0;index<selectedRooms.size();index++){
				sRooms+=selectedRooms.get(index).toString()+";";
			}
			if (sRooms != null && sRooms.length() > 0)
				sRooms = "###"+sRooms;
			String str= AdminWise.gConnector.ViewWiseClient.encryptStr(roomInfo+sRooms);            
			VWUtil.writeStringToFile(str, path);   	    	
			setVisible(false);
		}
	}
	
	//	------------------------------------------------------------------------------
	void BtnBrowse_actionPerformed(java.awt.event.ActionEvent event)
	{
		JFileChooser chooser = new JFileChooser();
		VWFileFilter filter = new VWFileFilter();
		filter.addExtension("vws");
		filter.setDescription(PRODUCT_NAME + " Setting Files");		
		chooser.setFileFilter(filter);
		chooser.setCurrentDirectory(new File(txtPath.getText()));
		int returnVal = chooser.showSaveDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			String path=chooser.getSelectedFile().getPath();
			if(!path.toLowerCase().endsWith(".vws")) path+=".vws";
			txtPath.setText(path);//return path;
		};       	        
	}
	//	------------------------------------------------------------------------------
	private void saveDlgOptions()
	{
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		prefs.putInt("x1",this.getX());
		prefs.putInt("y1",this.getY());
		prefs.putInt("width1",this.getSize().width);
		prefs.putInt("height1",this.getSize().height);
	}
	//	------------------------------------------------------------------------------
	private void getDlgOptions()
	{
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		this.setLocation(prefs.getInt("x1",50),prefs.getInt("y1",50));
		///setSize(prefs.getInt("width1",335),prefs.getInt("height1",210));
	}
	//	------------------------------------------------------------------------------
	public void setMainText(String text)
	{
		LblMain.setText(text);
	}
	//	------------------------------------------------------------------------------
	public boolean cancelAction=false;
	public VWRoom selRoom = null;
	public Vector getRooms(String server){
		Vector rooms = new Vector();
		VWConnector.ViewWiseClient.getRooms(server,rooms);
		Collections.sort(rooms);
		return rooms;
	}
	public static void main(String []a){
		//	new VWExportInfoDlg(null).show();
	}
}
