package ViewWise.AdminWise.VWRoom;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;

import javax.swing.*;

import com.computhink.common.Constants;
import com.computhink.common.VWEvent;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.Session;

import ViewWise.AdminWise.VWRetention.VWRetentionPanel;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWOptions.VWOptionsDlg;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWViewerTabManager;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWImages.VWImages;

public class VWRoomPanel extends JPanel implements VWConstant {
    
	public VWRoomPanel() {
        setLayout(null);
        setSize(677,296);
        setBounds(16,12,538,32);
        setBackground(AdminWise.getAWColor());
        JLabel1.setText(LBL_ROOMS_NAME);
        add(JLabel1);
        JLabel1.setBounds(10,14,126,15);
        add(combo);
        combo.setBackground(java.awt.Color.white);
        if(languageLocale.equals("nl_NL")){
        	combo.setBounds(132+10,12,224, AdminWise.gSmallBtnHeight);
        }else
        	combo.setBounds(132,12,224, AdminWise.gSmallBtnHeight);
        BtnConnect.setText(BTN_CONNECT_NAME);
        BtnConnect.setToolTipText(BtnConnect.getText());
        BtnConnect.setIcon(VWImages.ConnectIcon);
        BtnConnect.setMnemonic('C');
        //BtnConnect.setBackground(java.awt.Color.white);
        add(BtnConnect);
        //int left = 366;
        int left = 348;
        
        BtnConnect.setBounds(left,10,108, AdminWise.gBtnHeight);
        BtnServers.setText(AdminWise.connectorManager.getString("Btn.Servers"));
        BtnServers.setToolTipText(AdminWise.connectorManager.getString("Tooltip.Servers"));
        BtnServers.setIcon(VWImages.AllRoomIcon);
        BtnServers.setMnemonic('S');
        //BtnServers.setBackground(java.awt.Color.white);
        add(BtnServers);
        
        left += 86;
        BtnServers.setBounds(left,10,94, AdminWise.gBtnHeight);
        
        BtnOptions.setText(BTN_OPTIONS_NAME);
        BtnOptions.setToolTipText(BTN_OPTIONS_NAME);
        BtnOptions.setIcon(VWImages.OptionsIcon);
        BtnOptions.setMnemonic('O');
        //BtnOptions.setBackground(java.awt.Color.white);
        add(BtnOptions);
        /*left = 558;
        BtnOptions.setBounds(left,10,148, AdminWise.gBtnHeight);*/
        left = 516;
        BtnOptions.setBounds(left,10,148, AdminWise.gBtnHeight);
        
        BtnHelp.setText(BTN_HELP_NAME);
        BtnHelp.setToolTipText(BTN_HELP_NAME);
        BtnHelp.setIcon(VWImages.HelpIcon);
        BtnHelp.setMnemonic('G');
        //BtnHelp.setBackground(java.awt.Color.white);
        add(BtnHelp);
        
        left += 90;
        BtnHelp.setBounds(left,10,88, AdminWise.gBtnHeight);
                
        BtnHelpOnline.setText(BTN_HELPONLINE_NAME);
        BtnHelpOnline.setToolTipText(BTN_HELPONLINE_NAME);
        BtnHelpOnline.setIcon(VWImages.HelpOnlineIcon);
        BtnHelpOnline.setMnemonic('H');
        //BtnHelp.setBackground(java.awt.Color.white);
        add(BtnHelpOnline);
        
        left += 88;
        BtnHelpOnline.setBounds(left,10,95, AdminWise.gBtnHeight);
        
        left += 95;
        lblContentverse = new JLabel(ResourceManager.getDefaultManager().getString("powered_by_contentverse"), SwingConstants.RIGHT);
		lblContentverse.setForeground(Color.GRAY);
		lblContentverse.setBounds(left, 10, 130, AdminWise.gBtnHeight);
		Font font = lblContentverse.getFont();
		lblContentverse.setFont(new Font(font.getName(), Font.PLAIN, 10));
		add(lblContentverse);
        
        
        SymAction lSymAction = new SymAction();
        BtnConnect.addActionListener(lSymAction);
        BtnOptions.addActionListener(lSymAction);
        BtnHelp.addActionListener(lSymAction);
        BtnHelpOnline.addActionListener(lSymAction);
        combo.addActionListener(lSymAction);
        BtnServers.addActionListener(lSymAction);
        SymItem lSymItem = new SymItem();
        combo.addItemListener(lSymItem);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        BtnConnect.addKeyListener(aSymKey);
        /*combo.addFocusListener(new FocusListener(){
            public void focusGained(FocusEvent e){} 
            public void focusLost(FocusEvent e)
            {
            	BtnConnect.requestFocus(true);
            }
        });*/
        
        addComponentListener(new SymComponent());
    	}	
	
	class SymComponent implements ComponentListener{
		public void componentMoved(ComponentEvent ce){}
		public void componentHidden(ComponentEvent ce){}
		public void componentResized(ComponentEvent ce){
			//int curLeft = (getWidth()-300); //165			
			int screenWidth = getWidth();
			AdminWise.printToConsole("screen width >>>> "+screenWidth);
			int curLeft = 0;
			int left = 366;
			if (screenWidth >= 950) {
				AdminWise.printToConsole("Screen size is above 950");
				BtnConnect.setBounds(left,10,108, AdminWise.gBtnHeight);
				
				left += 110;
				BtnServers.setBounds(left,10,94, AdminWise.gBtnHeight);
				
				curLeft = (getWidth()-400); //165
				AdminWise.printToConsole("curLeft >>>> "+curLeft);
				BtnOptions.setBounds(curLeft, 10,80, AdminWise.gBtnHeight);
				
		        curLeft = (curLeft +80);//90
	
		        BtnHelp.setBounds(curLeft, 10,88, AdminWise.gBtnHeight);
	
		        curLeft = (curLeft +88);//90
		        BtnHelpOnline.setBounds(curLeft,10,95, AdminWise.gBtnHeight);
		        
		        curLeft = (curLeft +95);//90
		        lblContentverse.setBounds(curLeft, 10, 125, AdminWise.gBtnHeight);
			} else {
				AdminWise.printToConsole("Screen size is below 950");
				left = 348;
				BtnConnect.setBounds(left,10,108, AdminWise.gBtnHeight);
				
				left += 86;
				BtnServers.setBounds(left,10,94, AdminWise.gBtnHeight);
				
				curLeft = (getWidth()-368); //165
				AdminWise.printToConsole("curLeft >>>> "+curLeft);
				BtnOptions.setBounds(curLeft, 10,80, AdminWise.gBtnHeight);
				
		        curLeft = (curLeft +73);//90
	
		        BtnHelp.setBounds(curLeft, 10,88, AdminWise.gBtnHeight);
	
		        curLeft = (curLeft +78);//90
		        BtnHelpOnline.setBounds(curLeft,10,95, AdminWise.gBtnHeight);
		        
		        curLeft = (curLeft +89);//90
		        lblContentverse.setBounds(curLeft, 10, 125, AdminWise.gBtnHeight);
			}
	        repaint();
		}
		public void componentShown(ComponentEvent ce){}
	}
	
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    VWComboBox combo = new VWComboBox();
    VWLinkedButton BtnConnect = new VWLinkedButton(0);
    VWLinkedButton BtnOptions = new VWLinkedButton(0);
    VWLinkedButton BtnHelp =new VWLinkedButton(0);
    VWLinkedButton BtnHelpOnline =new VWLinkedButton(0);
    VWLinkedButton BtnServers =new VWLinkedButton(0);
    JLabel lblContentverse = null;
    //--------------------------------------------------------------------------
    public VWComboBox getRoomCombo() {
        return combo;
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnConnect)
                BtnConnect_actionPerformed(event);
            else if (object == BtnOptions)
                BtnOptions_actionPerformed(event);
            else if (object == BtnHelp)
                BtnHelp_actionPerformed(event);
            else if (object == BtnServers)
                BtnServers_actionPerformed(event);
            else if (object == combo)
                SelRoom_actionPerformed(event);
            else if (object == BtnHelpOnline) {
            	BtnHelpOnline_actionPerformed(event);
            }
        }
    }
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyPressed(java.awt.event.KeyEvent event) {
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
            	BtnConnect_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    public void BtnConnect_actionPerformed(java.awt.event.ActionEvent event) {
        VWRoom selRoom = (VWRoom)combo.getSelectedItem();
        connectAction(selRoom);
    }
    //--------------------------------------------------------------------------
    public void BtnServers_actionPerformed(java.awt.event.ActionEvent event) {
        new VWNewRoomDlg(AdminWise.adminFrame,LBL_SERVERS_NAME);
    }
    //--------------------------------------------------------------------------
    public void SelRoom_actionPerformed(java.awt.event.ActionEvent event) {
    	VWRoom selRoom = (VWRoom)combo.getSelectedItem();
        if(selRoom==null) return;
        if(selRoom.getId()==0&& selRoom.getName().equals(LBL_REFRESH)) {
            VWRoomConnector.loadVWRooms();
            SelRoom_actionPerformed(null);
        }
        else if(selRoom.getId()==0 && selRoom.getName().equals(LBL_NOROOMS_FOUND)) {
            BtnConnect.setEnabled(false);
        }
        else {
            BtnConnect.setEnabled(true);
        }
    }
    //--------------------------------------------------------------------------
    public void connectAction(VWRoom selRoom) {
        if(selRoom !=null && selRoom.getConnectStatus()!=Room_Status_Connect) {
            if(VWRoomConnector.connectRoom(selRoom)) {
                BtnConnect.setText(BTN_DISCONNECT_NAME);
                BtnConnect.setToolTipText(BTN_DISCONNECT_NAME);
                BtnConnect.setIcon(VWImages.DisconnectIcon);
                if(VWDocTypeConnector.isDRSEnabled()==1){
                	/*int selectedTab=AdminWise.adminPanel.MainTab.getSelectedIndex();
                	AdminWise.adminPanel.MainTab.addTab(TAB_ROUTE_NAME ,VWImages.RouteImage, AdminWise.adminPanel.routePanel,ROUTE_TOOLTIP);
                	AdminWise.adminPanel.routePanel.setupUI();
                	AdminWise.adminPanel.MainTab.setSelectedIndex(selectedTab);
                	AdminWise.adminPanel.MainTab.setSelectedIndex(TAB_DOCTYPE_ID);
                	AdminWise.adminPanel.repaint();
                	AdminWise.adminPanel.MainTab.repaint();*/                	
                }else{
                	try{
                		int tabCount = AdminWise.adminPanel.MainTab.getTabCount();
                		if(tabCount==VWViewerTabManager.TOTAL_TAB_COUNT){
                			//AdminWise.adminPanel.MainTab.remove(tabCount-1);
                			//AdminWise.adminPanel.MainTab.removeTabAt(VWViewerTabManager.TAB_ROUTE_ID);
                		}
                	}catch(Exception ex){}
                }

                //Creating PurgeDocs and keepdoctype registry key with default values.
                Vector purgeDocsFlag = new Vector();
                int ret = VWConnector.ViewWiseClient.getPurgeDocs(selRoom.getId(), purgeDocsFlag);
                if(ret!=-1){
                	String purgeDocs = "false";
                	if(purgeDocsFlag!=null && purgeDocsFlag.size()>0){
                		purgeDocs = purgeDocsFlag.get(0).toString();
                	}
                	if(purgeDocs.equalsIgnoreCase("false")){
                		ret = VWConnector.ViewWiseClient.setPurgeDocs(selRoom.getId(), "false");
                		//VWRetentionPanel.purgeDocsFlag = false;
                	}
                }
                
                Vector keepDocTypeFlag = new Vector();
                int retVal = VWConnector.ViewWiseClient.getKeepDocType(selRoom.getId(), keepDocTypeFlag);
                if(retVal!=-1){
                	String keepdoctype = "true";
                	if(keepDocTypeFlag!=null && keepDocTypeFlag.size()>0){
                		keepdoctype = keepDocTypeFlag.get(0).toString();
                	}
                	if(keepdoctype.equalsIgnoreCase("true")){
                		retVal = VWConnector.ViewWiseClient.setKeepDocType(selRoom.getId(), "true");
                	}
                }
            	int selecteIndex=AdminWise.adminPanel.MainTab.getSelectedIndex();
	    	  	AdminWise.printToConsole("selectedIndex::::"+selecteIndex);
	    	  	AdminWise.printToConsole("AdminWise.adminPanel.MainTab.isEnabledAt(selecteIndex)33333"+AdminWise.adminPanel.MainTab.isEnabledAt(selecteIndex));
	    	  	if(AdminWise.adminPanel.MainTab.isEnabledAt(selecteIndex))
	    	  		VWViewerTabManager.loadDataForActivTab(selRoom);
	    	  	else
	    	  		VWViewerTabManager.unloadDataForActivTab();
            }
        }
        else if(selRoom !=null && selRoom.getConnectStatus()==Room_Status_Connect) {
        	int isIdle = VWConnector.ViewWiseClient.isRoomIdle(selRoom.getId(), selRoom.getName());
        	if(isIdle != VWEvent.IDLE_DISCONNECT && VWConnector.serverInactive == false){
        		if(AdminWise.adminPanel.docTypePanel.curMode == 9 || AdminWise.adminPanel.docTypePanel.curMode == 2){    		
        			int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("VWRoomPanel.msg_1"),AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
        			if(res != 0)
        				return;	
        		}
        		if(AdminWise.adminPanel.docTypePanel.curMode == 15){    		
    	    		int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("VWRoomPanel.msg_2"),AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
    	    		if(res != 0)
    	    			return;	
    	    	}
    	    	if(AdminWise.adminPanel.routePanel.CURRENT_MODE!=0 && AdminWise.adminPanel.routePanel.CURRENT_MODE!=2){    		
    	    		int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("VWRoomPanel.msg_3")+" "+ Constants.WORKFLOW_MODULE_NAME.toLowerCase() +"?",AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
    	    		if(res != 0)
    	    			return;	
    	    	}
    	    	if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE !=0 && AdminWise.adminPanel.retentionPanel.CURRENT_MODE != 2){    		
    	    		int res = JOptionPane.showConfirmDialog(null,AdminWise.connectorManager.getString("VWRoomPanel.msg_4"),AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.YES_NO_OPTION);    		
    	    		if(res != 0)
    	    			return;	
    	    	}
        	}       	
            VWRoomConnector.disConnectRoom(selRoom);
            if (selRoom.getName().equals(getSelectedRoom().getName())){            	
            	BtnConnect.setText(BTN_CONNECT_NAME);            
            	BtnConnect.setIcon(VWImages.ConnectIcon);
            	BtnConnect.setToolTipText(BTN_CONNECT_NAME);
            	VWViewerTabManager.unloadDataForActivTab();
            }
        }
    }
    //--------------------------------------------------------------------------
    public void connectActionUI(VWRoom selRoom) {
        if(selRoom !=null) {
            selRoom.setConnectStatus(Room_Status_Connect);
            BtnConnect.setText(BTN_DISCONNECT_NAME);
            BtnConnect.setToolTipText(BTN_DISCONNECT_NAME);
            BtnConnect.setIcon(VWImages.DisconnectIcon);
            //Added here since Adminwise from DTC is not having Route Tab
            if(VWDocTypeConnector.isDRSEnabled()==1){
            	/*int selectedTab=AdminWise.adminPanel.MainTab.getSelectedIndex();
            	AdminWise.adminPanel.MainTab.addTab(TAB_ROUTE_NAME ,VWImages.RouteImage, AdminWise.adminPanel.routePanel,ROUTE_TOOLTIP);
            	AdminWise.adminPanel.routePanel.setVisible(true);
            	AdminWise.adminPanel.routePanel.setupUI();
            	AdminWise.adminPanel.MainTab.setSelectedIndex(selectedTab);
            	AdminWise.adminPanel.MainTab.setSelectedIndex(TAB_DOCTYPE_ID);
            	AdminWise.adminPanel.repaint();
            	AdminWise.adminPanel.MainTab.repaint();*/
            }else{ 
            	try{
            		int tabCount = AdminWise.adminPanel.MainTab.getTabCount();   
            		if(tabCount==VWViewerTabManager.TOTAL_TAB_COUNT){
            			//AdminWise.adminPanel.MainTab.remove(tabCount-1);
            			//AdminWise.adminPanel.MainTab.removeTabAt(VWViewerTabManager.TAB_ROUTE_ID);
            		}
            	}catch(Exception ex){}
            }
            //CV10 Enhancement if condition added to load tab data based on enable/disable of tabs
        	int selecteIndex=AdminWise.adminPanel.MainTab.getSelectedIndex();
        	if(AdminWise.adminPanel.MainTab.isEnabledAt(selecteIndex))
        		VWViewerTabManager.loadDataForActivTab(selRoom);
        }
    }
    //--------------------------------------------------------------------------
    void BtnOptions_actionPerformed(java.awt.event.ActionEvent event) {
        new VWOptionsDlg(AdminWise.adminFrame);
    }
    //--------------------------------------------------------------------------
    void BtnHelp_actionPerformed(java.awt.event.ActionEvent event) {
        AdminWise.getInstance().showHelpPage(1);
    }
    //--------------------------------------------------------------------------
    void BtnHelpOnline_actionPerformed(java.awt.event.ActionEvent event) {
        AdminWise.getInstance().showHelpPage(2);
    }
    //--------------------------------------------------------------------------
    class SymItem implements java.awt.event.ItemListener {
        public void itemStateChanged(java.awt.event.ItemEvent event) {
            Object object = event.getSource();
            if (object == combo)
                Combo_itemStateChanged(event);
        }
    }
    //--------------------------------------------------------------------------
    void Combo_itemStateChanged(java.awt.event.ItemEvent event) {
    	if(!roomLoaded) return;
        VWRoom selRoom=(VWRoom)combo.getSelectedItem();
        if(selRoom!=null && selRoom.getId()>=0) {
            BtnConnect.setEnabled(true);
            if(selRoom.getConnectStatus()==Room_Status_Connect) {
                BtnConnect.setText(BTN_DISCONNECT_NAME);
                BtnConnect.setToolTipText(BTN_DISCONNECT_NAME);
                BtnConnect.setIcon(VWImages.DisconnectIcon);
                //Need to check routing tab when rooms are switched
                int selectedTab=AdminWise.adminPanel.MainTab.getSelectedIndex();                
               /* if(VWDocTypeConnector.isDRSEnabled()==1){                	
                	AdminWise.adminPanel.MainTab.addTab(TAB_ROUTE_NAME ,VWImages.RouteImage, AdminWise.adminPanel.routePanel,ROUTE_TOOLTIP);
                	AdminWise.adminPanel.MainTab.addTab(TAB_ROUTE_REPORT_NAME ,VWImages.RouteImage, AdminWise.adminPanel.routeReportPanel,ROUTE_TOOLTIP);
                	AdminWise.adminPanel.MainTab.addTab(TAB_FORM_TEMPLATE_NAME ,VWImages.FormTemplateImage, AdminWise.adminPanel.formTemplatePanel,FORM_TEMPLATE_TOOLTIP);

                	AdminWise.adminPanel.routePanel.setupUI();
                	AdminWise.adminPanel.MainTab.setSelectedIndex(selectedTab);
                	AdminWise.adminPanel.MainTab.setSelectedIndex(TAB_DOCTYPE_ID);
                	AdminWise.adminPanel.MainTab.repaint();
                	AdminWise.adminPanel.repaint();
                }else  {
                	
                	//AdminWise.adminPanel.routePanel.setVisible(false);                
                	try{
                		int tabCount = AdminWise.adminPanel.MainTab.getTabCount();   
                		if(tabCount==VWViewerTabManager.TOTAL_TAB_COUNT){
                			//AdminWise.adminPanel.MainTab.remove(tabCount-1);
                			AdminWise.adminPanel.MainTab.removeTabAt(VWViewerTabManager.TAB_ROUTE_ID);
                			AdminWise.adminPanel.MainTab.removeTabAt(VWViewerTabManager.TAB_ROUTEREPORT_ID);
                			AdminWise.adminPanel.MainTab.removeTabAt(VWViewerTabManager.TAB_FORM_TEMPLATE_ID);

                		}
                	}catch(Exception ex){}
                }*/
                VWViewerTabManager.loadDataForActivTab(selRoom);
                Session session=AdminWise.gConnector.ViewWiseClient.getSession(selRoom.getId());
                VWViewerTabManager.getTabsDisplayData(selRoom,session.user);
            }
            else {
                BtnConnect.setText(BTN_CONNECT_NAME);
                BtnConnect.setToolTipText(BTN_CONNECT_NAME);
                BtnConnect.setIcon(VWImages.ConnectIcon);
                VWViewerTabManager.unloadDataForActivTab();
            }
        }
        else if(selRoom!=null && selRoom.getId()==0) {
            BtnConnect.setEnabled(false);
            return;
        }
        else if(selRoom!=null && selRoom.getId()==-1) {
            VWNewRoomDlg newRoomDlg= new VWNewRoomDlg(AdminWise.adminFrame,LBL_CREATENEWROOM_NAME);
        }
    }
    //--------------------------------------------------------------------------
    public int getSelectedRoomId() {
        return getSelectedRoom().getId();
    }
    //--------------------------------------------------------------------------
    public VWRoom getSelectedRoom() {
        return (VWRoom)combo.getSelectedItem();
    }
    //--------------------------------------------------------------------------
    public VWRoom setSelectedRoom(String roomName) {
        roomLoaded=false;
        try {
            int count=combo.getItemCount();
            VWRoom room=null;
            for(int i=0;i<count;i++) {
                room=(VWRoom)combo.getItemAt(i);
                if(room.toString().equalsIgnoreCase(roomName) || room.getName().equalsIgnoreCase(roomName)) {
                    combo.setSelectedIndex(i);
                    roomLoaded=true;
                    return room;
                }
            }
        }
        catch (Exception e){};
        roomLoaded=true;
        return null;
    }
    //--------------------------------------------------------------------------
    public int getRoomCount() {
        return combo.getItemCount();
    }
    //--------------------------------------------------------------------------
    public void setConnectEnable(boolean enable) {
        if(enable)
            SelRoom_actionPerformed(null);
        else
            BtnConnect.setEnabled(enable);
    }
    //--------------------------------------------------------------------------
    public void setRoomToolTip(String userName,String clientIP) {
        if(userName==null) {
            combo.setToolTipText(null);
        }
        else {
            String toolTip="<HTML><P><B>User Name : </B>" + userName+"</P>";
            toolTip+="<P><B>Client Host : </B>"+clientIP+"</P>";
            toolTip+="</HTML>";
            combo.setToolTipText(toolTip);
        }
    }
    //--------------------------------------------------------------------------
    public boolean roomLoaded=false;
    public static boolean routeTabAdded=false;
    String languageLocale=AdminWise.getLocaleLanguage();
    public static void main(String[] args) {
        try {
	            String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
   	            String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
   	            //UIManager.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");    				
   	            UIManager.setLookAndFeel(windowsLookandFeel);
   	            
        	}catch(java.lang.Exception se){ }
    	
    	JFrame frame = new JFrame();
    	frame.add(new VWRoomPanel());
    	frame.setVisible(true);
    	frame.setSize(800,100);
	}
}
