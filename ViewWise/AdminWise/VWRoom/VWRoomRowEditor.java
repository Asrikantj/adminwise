/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWSimpleSearchRowEditor<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/

package ViewWise.AdminWise.VWRoom;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;

public class VWRoomRowEditor implements TableCellEditor, VWConstant{
    
    protected TableCellEditor editor,defaultEditor;
    protected VWRoomTable gTable=null;
    //--------------------------------------------------------------------------
    public VWRoomRowEditor(VWRoomTable table){
        JTextField text=new JTextField();
        text.setBorder(null);
        defaultEditor = new DefaultCellEditor(text);
        gTable=table;
    }
    //--------------------------------------------------------------------------
    public Component getTableCellEditorComponent(JTable table,Object value,
        boolean isSelected, int row, int column){
        if(row==0)
            editor=defaultEditor;
        if(row==1)
            editor=defaultEditor;
        if(row==2)
            editor=defaultEditor;
        return editor.getTableCellEditorComponent(table,
        value, isSelected, row, column);
    }
    //--------------------------------------------------------------------------
    public Object getCellEditorValue(){
        return editor.getCellEditorValue();
    }
    //--------------------------------------------------------------------------
    public boolean stopCellEditing(){
        return editor.stopCellEditing();
    }
    //--------------------------------------------------------------------------
    public void cancelCellEditing(){
        editor.cancelCellEditing();
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(EventObject anEvent){
        int column=-1,row=-1;
        if (anEvent instanceof MouseEvent) {
            Point point=((MouseEvent)anEvent).getPoint();
            column = gTable.columnAtPoint(point);
            row = gTable.rowAtPoint(point);
        }
        if(column==1 &&
            (VWNewRoomDlg.curMode== MODE_NEW || VWNewRoomDlg.curMode== MODE_UPDATE)) return true; 
        return false;
    }
    //--------------------------------------------------------------------------
    public void addCellEditorListener(CellEditorListener l){
        editor.addCellEditorListener(l);
    }
    //--------------------------------------------------------------------------
    public void removeCellEditorListener(CellEditorListener l){
        editor.removeCellEditorListener(l);
    }
    //--------------------------------------------------------------------------
    public boolean shouldSelectCell(EventObject anEvent){
        return editor.shouldSelectCell(anEvent);
    }
    //--------------------------------------------------------------------------
}