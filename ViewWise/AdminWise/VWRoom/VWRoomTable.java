/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRoom;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import com.computhink.common.resource.ResourceManager;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//------------------------------------------------------------------------------
public class VWRoomTable extends JTable implements VWConstant
{
    protected VWRoomTableData m_data;    
    public VWRoomTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWRoomTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWRoomTableData.m_columns.length; k++) {
        
        VWTableCellRenderer renderer =new VWTableCellRenderer();
        renderer.setHorizontalAlignment(VWRoomTableData.m_columns[k].m_alignment);
        VWRoomRowEditor editor=new VWRoomRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWRoomTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
	 setRowHeight(TableRowHeight);
	 
	 setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if (object instanceof JTable)
            if(event.getModifiers()==event.BUTTON3_MASK)
            VWRoomTable_RightMouseClicked(event);
            else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWRoomTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWRoomTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWRoomTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new RoomRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public String getRowDataType(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return row[3];
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class RoomRowData
{
    public String   m_PropertyName;
    public String   m_PropertyValue;
    public RoomRowData() {
    m_PropertyName="";
    m_PropertyValue="";
  }
  //----------------------------------------------------------------------------
  public RoomRowData(String[] data) 
  {
    int i=0;
    m_PropertyName=data[i++];
    m_PropertyValue=data[i];
  }
}
//------------------------------------------------------------------------------
class RoomColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public RoomColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWRoomTableData extends AbstractTableModel 
{
  public static final RoomColumnData m_columns[] = {
    new RoomColumnData( AdminWise.connectorManager.getString("VWRoomTable.RoomColumnData_1"),0.4F, JLabel.LEFT),
    new RoomColumnData( AdminWise.connectorManager.getString("VWRoomTable.RoomColumnData_2"),0.6F,JLabel.LEFT)};
  
    public static final int COL_PROPNAME = 0;
    public static final int COL_PROPVALUE = 1;

    protected VWRoomTable m_parent;
    protected Vector m_vector;

  public VWRoomTableData(VWRoomTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        RoomRowData row =new RoomRowData(data[i]); 
        m_vector.addElement(row);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][2];
    for(int i=0;i<count;i++)
    {
        RoomRowData row=(RoomRowData)m_vector.elementAt(i);
        data[i][0]=row.m_PropertyName;
        data[i][1]=row.m_PropertyValue;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] RoomRowData=new String[2];
    RoomRowData row=(RoomRowData)m_vector.elementAt(rowNum);
    RoomRowData[0]=row.m_PropertyName;
    RoomRowData[1]=row.m_PropertyValue;
    return RoomRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    RoomRowData row = (RoomRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_PROPNAME:    return row.m_PropertyName;
      case COL_PROPVALUE:   return row.m_PropertyValue;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    RoomRowData row = (RoomRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_PROPNAME:
        row.m_PropertyName = svalue; 
        break;
      case COL_PROPVALUE:
          row.m_PropertyValue = svalue;
            if(nRow==2)
                row.m_PropertyValue = String.valueOf(VWUtil.to_Number(svalue));
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(RoomRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
}