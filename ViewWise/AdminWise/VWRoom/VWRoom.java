/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWTreeItem<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRoom;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWRoom implements java.io.Serializable, VWConstant
{
    private int id=0;
    private String name="";
    private String server="";
    private String serverHost="";
    private int serverPort=0;
                
    private int connectStatus=Room_Status_UnConnect;

    public VWRoom(String roomInfo)
    {
        VWStringTokenizer tokenizer=new VWStringTokenizer(roomInfo,SepChar,false);
        this.id = VWUtil.to_Number(tokenizer.nextToken());
        setName(tokenizer.nextToken());
    }
    
    public VWRoom(int id,String name)
    {
        this.name=name;
        this.id=id;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }
    
    public String toString()
    {
        if(getName()==null || getName().equals(""))
            return getServer();
        return getServer()+"."+getName();
    }
    public int getConnectStatus()
    {
        return connectStatus;
    }

    public void setConnectStatus(int roomStatus)
    {
        this.connectStatus = roomStatus;
    }

    public java.lang.String getServerHost() {
        return serverHost;
    }
    
    public void setServerHost(java.lang.String serverHost) {
        this.serverHost = serverHost;
    }
    
    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }    
}