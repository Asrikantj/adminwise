/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWTreeConnector<br>
 *
 * @version     $Revision: 1.5 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWRoom;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;

import ViewWise.AdminWise.VWConnector.AdminClientRoom;
import ViewWise.AdminWise.VWPanel.VWAdminPanel;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.util.Hashtable;
import java.util.Enumeration;
import javax.swing.tree.DefaultMutableTreeNode;

import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWViewerTabManager;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import com.computhink.common.Room;
import com.computhink.common.ServerSchema;
import com.computhink.vwc.Session;

public class VWRoomConnector implements VWConstant {
    public static int loadVWRooms(){
        combo.removeAllItems();
        AdminWise.adminPanel.roomPanel.roomLoaded=false;
        int roomCount=0;
        List rooms=getViewWiseRooms();
        if((rooms==null || rooms.size()==0)) {
            VWRoom emptyRoom = new VWRoom(0,LBL_NOROOMS_FOUND);
            combo.addItem(emptyRoom);
            VWRoom refreshRoom = new VWRoom(0,LBL_REFRESH);
            combo.addItem(refreshRoom);
            return roomCount;
        }
        roomCount=rooms.size();
        for(int i =0 ;i<roomCount;i++) {
            VWRoom room= (VWRoom)rooms.get(i);
            combo.addItem(room);
            room=null;
        }
        /*
        VWRoom newRoom = new VWRoom(-1,"<New Room>");
        combo.addItem(newRoom);
         */
        AdminWise.adminPanel.roomPanel.roomLoaded=true;
        return roomCount;
    }
    //--------------------------------------------------------------------------
    public static Vector getViewWiseRooms() {
        Vector adminRooms=new Vector();
        Vector servers=new Vector();
        AdminWise.gConnector.ViewWiseClient.getServers(servers);
        if (servers==null || servers.size() == 0)   return null;
        for(int i=0;i<servers.size();i++) {
            String serverName=(String) servers.get(i);
            Vector rooms=new Vector();
            AdminWise.gConnector.ViewWiseClient.getRooms((String) servers.get(i), rooms);
            for(int j=0;j<rooms.size();j++) {
                String serverRoom=(String)rooms.get(j);
                VWRoom room=new VWRoom(0,serverRoom);
                room.setServer(serverName);
                adminRooms.add(room);
            }
        }
        return adminRooms;
    }
    //--------------------------------------------------------------------------
    public static boolean connectRoom(VWRoom room) {
        int retConnect=AdminWise.gConnector.connectRoom(room);
        if(retConnect<0) {
            if(retConnect==Room_Status_Down) {
                room.setConnectStatus(Room_Status_Down);
                ((VWRoom)combo.getSelectedItem()).setConnectStatus(Room_Status_Down);
                return false;
            }
            if(retConnect==Room_Status_UnConnect) {
                room.setConnectStatus(Room_Status_UnConnect);
                ((VWRoom)combo.getSelectedItem()).setConnectStatus(Room_Status_UnConnect);
                return false;
            }
        }
        ((VWRoom)combo.getSelectedItem()).setConnectStatus(Room_Status_Connect);
        ((VWRoom)combo.getSelectedItem()).setId(retConnect);
        VWViewerTabManager.loadDataForActivTab(room);
        //CV10 SubAdministrator enhacment to enable or disable tabs
        Session session=AdminWise.gConnector.ViewWiseClient.getSession(room.getId());
        VWViewerTabManager.getTabsDisplayData(room,session.user);
        int enableSecurityAdmin=AdminWise.gConnector.ViewWiseClient.enableSecurityAdmin(room.getId());
    	int securityAdmin=AdminWise.gConnector.ViewWiseClient.isSecurityAdmin(room.getId());
    	int checkSubAdmin=AdminWise.gConnector.ViewWiseClient.checkSubAdmin(room.getName(),room.getId(),session.user);
    	if((enableSecurityAdmin==1)&&(securityAdmin!=1)&&(checkSubAdmin==0)){
    		int toggleFlag=0;
    		Vector toggleVector=new Vector();
    		AdminWise.gConnector.ViewWiseClient.getToggleStatus(room.getId(),toggleVector);
    		if(toggleVector.size()>0&&toggleVector!=null)
    			toggleFlag=Integer.parseInt(toggleVector.get(0).toString());
    		if(toggleFlag==0){
    			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_BACKUP_ID, false);
    			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RESTORE_ID, false);
    			AdminWise.gConnector.ViewWiseClient.setTabStatus(session.sessionId,0);
    		}else{
    			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_BACKUP_ID, true);
    			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RESTORE_ID, true);
    			AdminWise.gConnector.ViewWiseClient.setTabStatus(session.sessionId,1);
    		}
    	}
        
        return true;
    }
    public static Hashtable getVWRoom(){
    	Hashtable listRooms = new Hashtable();
    	 VWRoom room = null;
    	 
    	if(combo != null && combo.getItemCount() > 0){
    		for (int i =0; i< combo.getItemCount();i++){
    			room = (VWRoom) combo.getItemAt(i);    			 
    			listRooms.put(room.getName(),room);
    		}
    	}
    	return listRooms;
    }
    //--------------------------------------------------------------------------
    public static void disConnectAllRooms() {
        int roomCount=0;
        List roomsList=getAllRoom();
        if(roomsList==null) return;
        roomCount=roomsList.size();
        for(int i=0;i<roomCount;i++) {
            disConnectRoom((VWRoom)roomsList.get(i));
        }
    }
    //--------------------------------------------------------------------------
    public static void disConnectRoom(VWRoom room) {
        if(((VWRoom)combo.getSelectedItem()).getId()==room.getId())
        {
            ((VWRoom)combo.getSelectedItem()).setConnectStatus(Room_Status_UnConnect);
            resetRoomData(room);
        }
        else
        {
            for(int i=0;i<combo.getItemCount();i++)
            {
                if(((VWRoom)combo.getItemAt(i)).getId()==room.getId())
                {
                    ((VWRoom)combo.getItemAt(i)).setConnectStatus(Room_Status_UnConnect);                                        
                    break;
                }
            }
        }
        
        AdminWise.gConnector.disConnectRoom(room.getId());
        try {
        	((AdminClientRoom)AdminWise.listTable.get(room.getName())).idleTimer.stop();
        	AdminWise.listTable.remove(room.getName());
        }catch(Exception ex){
        	
        }
    }
    //--------------------------------------------------------------------------
    private static List getAllRoom() {
        List rooms = new LinkedList();
        int count = combo.getItemCount();
        for(int i=0;i<count;i++) {
            VWRoom room=(VWRoom)combo.getItemAt(i);
            if(room.getId()>0) rooms.add(room);
        }
        return rooms;
    }
    //--------------------------------------------------------------------------
    private static void resetRoomData(VWRoom room) {
        AdminWise.adminPanel.docLockPanel.unloadTabData();
        AdminWise.adminPanel.conUserPanel.unloadTabData();
        ///AdminWise.adminPanel.propertiesPanel.unloadTabData();
        AdminWise.adminPanel.statisticPanel.unloadTabData();
        ///AdminWise.adminPanel.logViewerPanel.unloadTabData();
        AdminWise.adminPanel.archivePanel.unloadTabData();
        AdminWise.adminPanel.docTypePanel.unloadTabData();
        AdminWise.adminPanel.auditTrailPanel.unloadTabData();
    }
    //--------------------------------------------------------------------------
    public static VWComboBox combo=AdminWise.adminPanel.roomPanel.getRoomCombo();
}