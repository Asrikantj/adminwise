package ViewWise.AdminWise.VWRoom;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;
import  java.util.Vector;
import java.awt.Frame;
import java.util.prefs.*;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import javax.swing.border.EtchedBorder;
import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWMessage.VWMessage;

    //--------------------------------------------------------------------------
public class VWNewRoomDlg extends javax.swing.JDialog implements VWConstant, Constants {
    public VWNewRoomDlg(Frame parent,String title) {
        super(parent,title,true);
        ///getContentPane().setLayout(new BorderLayout());
        getContentPane().add(VWRoom,BorderLayout.CENTER);
        getContentPane().setBackground(AdminWise.getAWColor());
        setVisible(false);
        VWRoom.setLayout(null);
        VWRoom.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWRoom.add(PanelCommand);
        PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,12,152,132);
        PanelCommand.add(CmbServers);
        CmbServers.setBackground(java.awt.Color.white);
        //CmbServers.setBorder(etchedBorder);
        CmbServers.setBounds(12,160,144, AdminWise.gSmallBtnHeight);
        BtnNew.setText(BTN_NEW_NAME);
        BtnNew.setActionCommand(BTN_NEW_NAME);
        PanelCommand.add(BtnNew);
//        BtnNew.setBackground(java.awt.Color.white);
        BtnNew.setBounds(12,195,144, AdminWise.gBtnHeight);
        BtnNew.setHorizontalAlignment(SwingConstants.LEFT);
        BtnNew.setIcon(VWImages.AllRoomIcon);
        BtnUpdate.setText(BTN_UPDATE_NAME);
        BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
        BtnUpdate.setHorizontalAlignment(SwingConstants.LEFT);
        PanelCommand.add(BtnUpdate);
//        BtnUpdate.setBackground(java.awt.Color.white);
        BtnUpdate.setBounds(12,230,144, AdminWise.gBtnHeight);
        BtnUpdate.setIcon(VWImages.UpdateIcon);
        BtnDel.setText(BTN_DEL_NAME);
        BtnDel.setActionCommand(BTN_DEL_NAME);
        BtnDel.setHorizontalAlignment(SwingConstants.LEFT);
        BtnDel.setIcon(VWImages.DelIcon);
        PanelCommand.add(BtnDel);
//        BtnDel.setBackground(java.awt.Color.white);
        BtnDel.setBounds(12,265,144, AdminWise.gBtnHeight);
        BtnExport.setText(BTN_EXPORT_NAME);
        BtnExport.setActionCommand(BTN_EXPORT_NAME);
        BtnExport.setHorizontalAlignment(SwingConstants.LEFT);
        PanelCommand.add(BtnExport);
//        BtnExport.setBackground(java.awt.Color.white);
        BtnExport.setBounds(12,300,144, AdminWise.gBtnHeight);
        BtnExport.setIcon(VWImages.ExportIcon);
        BtnClose.setText(BTN_CLOSE_NAME);
        BtnClose.setActionCommand(BTN_CLOSE_NAME);
        BtnClose.setHorizontalAlignment(SwingConstants.LEFT);
        BtnClose.setIcon(VWImages.CloseIcon);
        PanelCommand.add(BtnClose);
//        BtnClose.setBackground(java.awt.Color.white);
        BtnClose.setBounds(12,335,144, AdminWise.gBtnHeight);
        
        PanelTable.setLayout(new BorderLayout());
        VWRoom.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        SymComponent aSymComponent = new SymComponent();
        VWRoom.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.NewRoomImage);
        ///Table.addData(RoomData);
        SymAction lSymAction = new SymAction();
        BtnNew.addActionListener(lSymAction);
        BtnClose.addActionListener(lSymAction);
        BtnDel.addActionListener(lSymAction);
        BtnExport.addActionListener(lSymAction);
        BtnUpdate.addActionListener(lSymAction);
        CmbServers.addActionListener(lSymAction);
        Table.addData(roomData);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        getDlgOptions();
        setResizable(false);
        loadRooms();
        setVisible(true);
    }
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(java.awt.event.WindowEvent event) {
            Object object = event.getSource();
            if (object == VWNewRoomDlg.this)
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnNew)
                BtnNew_actionPerformed(event);
            if (object == BtnUpdate)
                BtnUpdate_actionPerformed(event);
            else if (object == BtnClose)
                BtnClose_actionPerformed(event);
            else if (object == BtnDel)
                BtnDel_actionPerformed(event);
            else if (object == BtnExport)
                BtnExport_actionPerformed(event);
            else if (object == CmbServers)
                CmbServers_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWRoom = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnNew = new VWLinkedButton(0,true);
    VWLinkedButton BtnUpdate = new VWLinkedButton(0,true);
    VWLinkedButton BtnDel = new VWLinkedButton(0,true);
    VWLinkedButton BtnExport = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWRoomTable Table = new VWRoomTable();
    VWComboBox CmbServers = new VWComboBox();
    EtchedBorder etchedBorder = new EtchedBorder
    (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWRoom)
                VWRoom_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWRoom_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWRoom) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    void BtnNew_actionPerformed(java.awt.event.ActionEvent event) {
        if(BtnNew.getText().equals(BTN_NEW_NAME))
        {
            curMode=MODE_NEW;
            String[][] roomData={{"Server Name",""},{"Server Host",""},{"Communication Port","0"}};
            Table.addData(roomData);
            setEnableMode(MODE_NEW);
        }
        else
        {
            stopGridEdit();
            if(saveViewWiseServer())
                setEnableMode(MODE_SELECT);
        }
    }
    //--------------------------------------------------------------------------
    void BtnUpdate_actionPerformed(java.awt.event.ActionEvent event) {
        if(BtnUpdate.getText().equals(BTN_UPDATE_NAME))
        {
            curMode=MODE_UPDATE;
            setEnableMode(MODE_UPDATE);
        }
        else
        {
            stopGridEdit();
            curMode=-1;
            setEnableMode(MODE_SELECT);
            loadRoomData();
        }
    }
    //--------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event) {
        saveOptions();
        for(int i=0;i<newRoomsList.size();i++)
        {
            Vector rooms=new Vector();
            VWRoom server=(VWRoom)newRoomsList.get(i);
            AdminWise.gConnector.ViewWiseClient.registerServer(server.getServer(), 
                server.getServerHost(), server.getServerPort());
            AdminWise.gConnector.ViewWiseClient.getRooms(server.getServer(), rooms);
            for(int j=0;j<rooms.size();j++)
            {
                VWRoom newRoom= new VWRoom(0,(String)rooms.get(j));
                newRoom.setServer(server.getServer());
                AdminWise.adminPanel.roomPanel.getRoomCombo().addItem(newRoom);
            }
        }
        setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnDel_actionPerformed(java.awt.event.ActionEvent event) {
        
        VWRoom selRoom=(VWRoom) CmbServers.getSelectedItem();
        if(selRoom==null) return ;
        if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    "Delete server (" + selRoom.getServer() +")?")!=
                    javax.swing.JOptionPane.YES_OPTION) return;
        AdminWise.gConnector.ViewWiseClient.removeVWS(selRoom.getServer());
        newRoomsList.remove(selRoom.getServer());
        CmbServers.removeItem(selRoom);
        setEnableMode(MODE_SELECT);
    }
    //--------------------------------------------------------------------------
    void BtnExport_actionPerformed(java.awt.event.ActionEvent event) {
 /*      List list=new LinkedList();
        VWRoom selRoom=(VWRoom) CmbServers.getSelectedItem();
        if(selRoom==null) return ;
        String path=loadSaveDialog();
        if(path!=null && !path.equals(""))
        {
            String roomInfo=selRoom.getServer()+"@"+selRoom.getServerHost()+":"+
                selRoom.getServerPort();
            String str=AdminWise.gConnector.ViewWiseClient.encryptStr(roomInfo);
            VWUtil.writeStringToFile(str, path);
        }
 */
    	// Enhancement Show/Hide Room
        Object selRoom= CmbServers.getSelectedItem();
    	VWExportInfoDlg export = new VWExportInfoDlg(AdminWise.adminFrame,selRoom);
    	export.setVisible(true);
    	//Enhancement End
    }
    //--------------------------------------------------------------------------
    void CmbServers_actionPerformed(java.awt.event.ActionEvent event) {
        if(!loaddocRooms) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            loadRoomData();
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    private void loadRoomData() {
        stopGridEdit();
        VWRoom selRoom=(VWRoom) CmbServers.getSelectedItem();
        if(selRoom==null) return;
        if(CmbServers.getSelectedIndex()==0) {
            Table.clearData();
            setEnableMode(MODE_UNSELECTED);
        }
        else if (CmbServers.getSelectedIndex()>0) {
            String[][] selRoomData=VWConstant.roomData;
            selRoomData[0][1]=selRoom.getServer();
            selRoomData[1][1]=selRoom.getServerHost();
            selRoomData[2][1]=String.valueOf(selRoom.getServerPort());
            Table.addData(selRoomData);
            setEnableMode(MODE_SELECT);
        }
    }
    //--------------------------------------------------------------------------
    private void saveOptions() {
        if(!AdminWise.adminPanel.saveDialogPos) return;
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
        prefs.putInt("width", this.getSize().width);
        prefs.putInt("height", this.getSize().height);
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage( getClass() );
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        ///setSize(prefs.getInt("width",600),prefs.getInt("height",400));
        setSize(600,400);
    }
    //--------------------------------------------------------------------------
    private boolean saveViewWiseServer() {
        String[][] roomData= Table.getData();
        if(roomData[0][1].trim().equals(""))
        {
            VWMessage.showMessage(this,VWMessage.ERR_SERVER_NONAME,VWMessage.DIALOG_TYPE);
            return false;
        }
        else if(roomData[1][1].trim().equals(""))
        {
            VWMessage.showMessage(this,VWMessage.ERR_SERVER_NOHOST,VWMessage.DIALOG_TYPE);
            return false;
        }
        VWRoom selServer=null;
        if(curMode==MODE_UPDATE) 
        {
                selServer=(VWRoom)CmbServers.getSelectedItem();
        }
        else
        {
            selServer=new VWRoom(0,null);
            selServer.setServer(roomData[0][1].trim());
            selServer.setServerHost(roomData[1][1]);
            selServer.setServerPort(VWUtil.to_Number(roomData[2][1]));
        }
        for(int i=0;i<CmbServers.getItemCount();i++)
        {
            VWRoom selRoom=(VWRoom) CmbServers.getItemAt(i);
            if(selServer.getServer().equals(((VWRoom)CmbServers.getItemAt(i)).getServer())) continue;
            if(selRoom.getServer().equals(roomData[0][1].trim()))
            {
                VWMessage.showMessage(this,VWMessage.ERR_SERVER_ALREADYEXIST,VWMessage.DIALOG_TYPE);
                return false;
            }
        }
        if(curMode==MODE_UPDATE)
        {
            VWRoom selRoom=(VWRoom) CmbServers.getSelectedItem();
            if(selRoom==null) return false; 
            AdminWise.gConnector.ViewWiseClient.removeVWS(selRoom.getServer());
            CmbServers.removeItem(selRoom);
        }
        VWRoom room=new VWRoom(0,null);
        room.setServer(roomData[0][1].trim());
        room.setServerHost(roomData[1][1]);
        room.setServerPort(VWUtil.to_Number(roomData[2][1]));

        AdminWise.gConnector.ViewWiseClient.addVWS(room.getServer(),
            room.getServerHost(), room.getServerPort());
        CmbServers.addItem(room);
        if(curMode==MODE_NEW)
        {
            newRoomsList.add(room);
        }
        CmbServers.setSelectedItem(room);
        return true;
    }
    //--------------------------------------------------------------------------
    private void loadRooms() {
        loaddocRooms=false;
        CmbServers.removeAllItems();
        VWRoom emptyRoom=new VWRoom(0, null);
        emptyRoom.setServer("<" + ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("constant.LBL_SERVERS_NAME")+">");
        CmbServers.addItem(emptyRoom);        
        loadViewWiseServers();
        loaddocRooms=true;
        loadRoomData();
    }
    //--------------------------------------------------------------------------
    private void stopGridEdit() {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
    }
    //--------------------------------------------------------------------------
    private void setEnableMode(int mode) {
        setEnableMode(mode,false);
    }
    //--------------------------------------------------------------------------
    private void setEnableMode(int mode,boolean force) {
        ///if(!force && curMode==mode) return;
        curMode=mode;
        BtnNew.setVisible(true);
        BtnUpdate.setVisible(true);
        BtnDel.setVisible(true);
        BtnExport.setVisible(true);
        BtnClose.setVisible(true);
        CmbServers.setVisible(true);
        
        BtnNew.setEnabled(true);
        BtnUpdate.setEnabled(true);
        BtnDel.setEnabled(true);
        BtnExport.setEnabled(true);
        BtnClose.setEnabled(true);
        
        switch (mode) {
            case MODE_SELECT:
                BtnNew.setText(BTN_NEW_NAME);
                BtnNew.setIcon(VWImages.AllRoomIcon);
                BtnUpdate.setText(BTN_UPDATE_NAME);
                BtnUpdate.setIcon(VWImages.UpdateIcon);
                if(Table.getRowCount()>0)
                    Table.setRowSelectionInterval(0,0);
                break;
            case MODE_UNSELECTED:
                BtnNew.setText(BTN_NEW_NAME);
                BtnNew.setIcon(VWImages.AllRoomIcon);
                BtnUpdate.setText(BTN_UPDATE_NAME);
                BtnUpdate.setIcon(VWImages.UpdateIcon);
                BtnUpdate.setEnabled(false);
                BtnDel.setEnabled(false);
                BtnExport.setEnabled(false);
                break;
            case MODE_UPDATE:
                BtnNew.setText(BTN_SAVE_NAME);
                BtnNew.setIcon(VWImages.SaveIcon);
                BtnUpdate.setText(BTN_CANCEL_NAME);
                BtnUpdate.setIcon(VWImages.CloseIcon);
                BtnDel.setVisible(false);
                BtnExport.setVisible(false);
                BtnClose.setVisible(false);
                CmbServers.setVisible(false);
                Table.requestFocus();
                Table.setRowSelectionInterval(0,1);
                break;
            case MODE_NEW:
                BtnNew.setText(BTN_SAVE_NAME);
                BtnNew.setIcon(VWImages.SaveIcon);
                BtnUpdate.setText(BTN_CANCEL_NAME);
                BtnUpdate.setIcon(VWImages.CloseIcon);
                BtnDel.setVisible(false);
                BtnExport.setVisible(false);
                BtnClose.setVisible(false);
                CmbServers.setVisible(false);
                Table.requestFocus();
                Table.setRowSelectionInterval(0,1);
                break;
        }
    }
    //--------------------------------------------------------------------------
    private String loadSaveDialog() {
        JFileChooser chooser = new JFileChooser();
        VWFileFilter filter = new VWFileFilter();
        filter.addExtension("vws");
        filter.setDescription(PRODUCT_NAME + " Setting Files");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String path=chooser.getSelectedFile().getPath();
            if(!path.toLowerCase().endsWith(".vws")) path+=".vws";
            return path;
        };
        return "";
    }
    //--------------------------------------------------------------------------
    public void loadViewWiseServers() {
        int selIndex=0;
        String selServer=AdminWise.adminPanel.roomPanel.getSelectedRoom().getServer();
        Vector adminRooms=new Vector();
        Vector servers=new Vector();
        AdminWise.gConnector.ViewWiseClient.getVWSs(servers);
        if (servers==null || servers.size() == 0)   return ;
        for(int i=0;i<servers.size();i++) {
            VWStringTokenizer tokens=new VWStringTokenizer((String) servers.get(i),SepChar,false);
            String serverName=tokens.nextToken();
            String serverHost=tokens.nextToken();
            int serverPort=VWUtil.to_Number(tokens.nextToken());
            VWRoom room=new VWRoom(0,null);
            room.setServer(serverName);
            room.setServerHost(serverHost);
            room.setServerPort(serverPort);
            if(selServer.equals(serverName)) selIndex=i+1;
            CmbServers.addItem(room);
        }
        CmbServers.setSelectedIndex(selIndex);
    }
    private static int gCurRoom=0;
    private boolean loaddocRooms=true;
    public static int curMode=-1;
    private List newRoomsList=new LinkedList();
}