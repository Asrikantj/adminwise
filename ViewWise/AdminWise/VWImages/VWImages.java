/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWImages<br>
 *
 * @version     $Revision: 1.10 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWImages;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.util.StringTokenizer;
import ViewWise.AdminWise.VWMessage.VWMessage;
public class VWImages
{
//------------------------------------------------------------------------------    
    public final static int COPY_ICON=100;
	public VWImages()
    {
        try{
        FilterIcon = new ImageIcon(getClass().getResource("images/Filter.gif"));
        FindIcon = new ImageIcon(getClass().getResource("images/FindIcon.gif"));
        AdvancedFindImage = new ImageIcon(getClass().getResource("images/AdvancedFindImage.png"));
        TemplateImage = new ImageIcon(getClass().getResource("images/TemplateImage.png"));
        FindImage = new ImageIcon(getClass().getResource("images/FindImage.png"));
        DocTypeImage = new ImageIcon(getClass().getResource("images/DocTypeImage.png"));
        SecuriyImage = new ImageIcon(getClass().getResource("images/SecurityImage.png"));
        DocTypesIcon = new ImageIcon(getClass().getResource("images/DocTypes.png"));   
        ConUserImage = new ImageIcon(getClass().getResource("images/ConUserImage.png"));   
        DocLockImage = new ImageIcon(getClass().getResource("images/DocLockImage.png"));   
        ArchiveImage = new ImageIcon(getClass().getResource("images/ArchiveImage.png"));   
        StatisticImage= new ImageIcon(getClass().getResource("images/StatisticImage.png")); 
        PropertiesImage= new ImageIcon(getClass().getResource("images/PropertiesImage.gif")); 
        ATImage= new ImageIcon(getClass().getResource("images/AudittrailImage.png"));
        LogViewerImage= new ImageIcon(getClass().getResource("images/LogViewerImage.gif"));
        OptionsImage= new ImageIcon(getClass().getResource("images/OptionsImage.gif"));
        AMOptionsImage = new ImageIcon(getClass().getResource("images/AutoMailOptionsImage.gif"));
        NewRoomImage=new ImageIcon(getClass().getResource("images/NewRoomImage.gif"));
        UserEmailIdImage = new ImageIcon(getClass().getResource("images/UserEmailIdImage.gif")); 
        UpdateRouteImage = new ImageIcon(getClass().getResource("images/update-route.gif"));
        BrowseRouteImage = new ImageIcon(getClass().getResource("images/browse-route.gif"));
        
        CabinetCollapsedIcon = new ImageIcon(getClass().getResource("images/tree/CabinetCollapsedIcon.png"));        
        CabinetExpandedIcon = new ImageIcon(getClass().getResource("images/tree/CabinetExpandedIcon.png"));        
        DrawerCollapsedIcon = new ImageIcon(getClass().getResource("images/tree/DrawerCollapsedIcon.png"));        
        DrawerExpandedIcon = new ImageIcon(getClass().getResource("images/tree/DrawerExpandedIcon.png"));        
        FolderCollapsedIcon = new ImageIcon(getClass().getResource("images/tree/FoldercollapsedIcon.png"));        
        FolderExpandedIcon = new ImageIcon(getClass().getResource("images/tree/FolderExpandedIcon.png"));        
        AllRoomIcon = new ImageIcon(getClass().getResource("images/tree/AllRoomIcon.png"));
        RoomUnConnectIcon = new ImageIcon(getClass().getResource("images/tree/RoomUnConnectIcon.png"));   
        RoomDownIcon = new ImageIcon(getClass().getResource("images/tree/RoomDownIcon.png"));   
        RoomConnectIcon = new ImageIcon(getClass().getResource("images/tree/RoomConnectIcon.png"));   
        NewIcon= new ImageIcon(getClass().getResource("images/NewIcon.gif"));
        NewDocTypeIcon = new ImageIcon(getClass().getResource("images/NewDocType.gif"));
        AddIcon= new ImageIcon(getClass().getResource("images/AddIcon.gif"));
        DelIcon= new ImageIcon(getClass().getResource("images/DelIcon.png"));
        CustomReportIcon=new ImageIcon(getClass().getResource("images/CutomReport.png"));
        ClearIcon= new ImageIcon(getClass().getResource("images/ClearIcon.gif"));
        HelpIcon= new ImageIcon(getClass().getResource("images/Help.gif"));
        HelpOnlineIcon = new ImageIcon(getClass().getResource("images/AW_OnlineHelp.png"));
        ConnectIcon= new ImageIcon(getClass().getResource("images/Connect.gif"));
        DisconnectIcon= new ImageIcon(getClass().getResource("images/Disconnect.gif"));
        OptionsIcon= new ImageIcon(getClass().getResource("images/Options.gif"));

        UpIcon= new ImageIcon(getClass().getResource("images/UpIcon.gif"));
        DownIcon= new ImageIcon(getClass().getResource("images/DownIcon.gif"));
        StorageIcon= new ImageIcon(getClass().getResource("images/StorageIcon.png"));
        ConUserIcon= new ImageIcon(getClass().getResource("images/ConUserIcon.png"));
        StatisticIcon=new ImageIcon(getClass().getResource("images/StatisticIcon.png"));
        DocLockIcon= new ImageIcon(getClass().getResource("images/DocLockIcon.png"));
        RecycleImage= new ImageIcon(getClass().getResource("images/RecycleImage.png"));
        ATIcon= new ImageIcon(getClass().getResource("images/AuditTrailIcon.png"));
        CheckOutIcon=new ImageIcon(getClass().getResource("images/tree/AllRoomIcon.gif"));
        CheckInIcon=new ImageIcon(getClass().getResource("images/tree/AllRoomIcon.gif"));
        SessionIcon=new ImageIcon(getClass().getResource("images/tree/AllRoomIcon.gif"));
        
        RadioIcon1=new ImageIcon(getClass().getResource("images/Radio/rbs.gif")); 
        RadioIcon2=new ImageIcon(getClass().getResource("images/Radio/rbp.gif")); 
        RadioIcon3=new ImageIcon(getClass().getResource("images/Radio/rbr.gif")); 
        RadioIcon4=new ImageIcon(getClass().getResource("images/Radio/rbrs.gif")); 
        RadioIcon5=new ImageIcon(getClass().getResource("images/Radio/rb.gif")); 
        AdminWiseIcon= new ImageIcon(getClass().getResource("images/AdminWiseIcon.png"));  
        RefreshIcon=new ImageIcon(getClass().getResource("images/RefreshIcon.gif"));  
        SelectAllIcon= new ImageIcon(getClass().getResource("images/SelectAllIcon.gif"));  
        DeselectAllIcon=new ImageIcon(getClass().getResource("images/DeselectAllIcon.gif"));  
        TemplateIcon=new ImageIcon(getClass().getResource("images/TemplateIcon.png"));  
        DSNIcon=new ImageIcon(getClass().getResource("images/DSNIcon.gif"));  
        SecurityReportIcon=new ImageIcon(getClass().getResource("images/SecurityReport.png"));
        SubAdminIcon=new ImageIcon(getClass().getResource("images/SubAdmin.png"));
        BackupImage=new ImageIcon(getClass().getResource("images/BackupImage.png"));  
        RestoreImage=new ImageIcon(getClass().getResource("images/RestoreImage.png"));  
        BackupIcon=new ImageIcon(getClass().getResource("images/BackupIcon.png"));  
        RestoreIcon=new ImageIcon(getClass().getResource("images/RestoreIcon.png"));  
        RedactionsImage=new ImageIcon(getClass().getResource("images/RedactionsImage.png"));
        RedactionsIcon=new ImageIcon(getClass().getResource("images/RedactionsIcon.png"));
        //CloseIcon=new ImageIcon(getClass().getResource("images/Close.gif"));
        CloseIcon=new ImageIcon(getClass().getResource("images/cancel.png"));
        SaveIcon=new ImageIcon(getClass().getResource("images/save.png"));
        UpdateIcon=new ImageIcon(getClass().getResource("images/update-route.gif"));
        ExportIcon=new ImageIcon(getClass().getResource("images/Export.gif"));
        PrintIcon=new ImageIcon(getClass().getResource("images/Print.gif"));
        SaveAsIcon=new ImageIcon(getClass().getResource("images/SaveAs.gif"));
        CopyIcon=new ImageIcon(getClass().getResource("images/Copy.gif"));
        OkIcon=new ImageIcon(getClass().getResource("images/ok.png"));
        ReplaceIcon=new ImageIcon(getClass().getResource("images/Replace.gif"));
        LoadIcon=new ImageIcon(getClass().getResource("images/Load.gif"));
        SizeIcon=new ImageIcon(getClass().getResource("images/Size.gif"));
        MailIcon=new ImageIcon(getClass().getResource("images/Mail.gif"));
        VerRevIcon=new ImageIcon(getClass().getResource("images/VerRev.gif"));
        RetentionIcon=new ImageIcon(getClass().getResource("images/DocRetentionIcon.png"));
        ExecuteIcon=new ImageIcon(getClass().getResource("images/Execute.gif"));
        RecycleIcon =new ImageIcon(getClass().getResource("images/RecycleIcon.png"));

        MoveFirstIcon=new ImageIcon(getClass().getResource("images/MoveFirst.gif"));
        MoveLastIcon=new ImageIcon(getClass().getResource("images/MoveLast.gif"));
        MoveNextIcon=new ImageIcon(getClass().getResource("images/MoveNext.gif"));
        MovePreviousIcon=new ImageIcon(getClass().getResource("images/MovePrevious.gif"));

        
//      DRS related changes
        DocRouteImage = new ImageIcon(getClass().getResource("images/DocRoutingImage.png"));
        DocRetentionImage = new ImageIcon(getClass().getResource("images/DocRetentionImage.png"));
        
        RouteImage = new ImageIcon(getClass().getResource("images/DocRoutingIcon.png"));
        FormTemplateImage=new ImageIcon(getClass().getResource("images/FormTemplate.png"));
        FormTemplateUIImage=new ImageIcon(getClass().getResource("images/FormTemplateUI.png"));
        NewRouteIcon = new ImageIcon(getClass().getResource("images/NewRoute.gif"));
        AssignRouteIcon = new ImageIcon(getClass().getResource("images/AssignRoute.gif"));
        DocumentInRouteIcon = new ImageIcon(getClass().getResource("images/DocumentInRoute.gif"));
        
        MoveLeftIcon =  new ImageIcon(getClass().getResource("images/MoveLeft.png"));
        MoveRightIcon =  new ImageIcon(getClass().getResource("images/MoveRight.png"));
        MoveUpIcon =  new ImageIcon(getClass().getResource("images/MoveUp.gif"));
        MoveLeftAllIcon =  new ImageIcon(getClass().getResource("images/MoveLeftAll.png"));
        MoveRightAllIcon =  new ImageIcon(getClass().getResource("images/MoveRightAll.png"));
        MoveDownIcon =  new ImageIcon(getClass().getResource("images/MoveDown.gif"));
        
        RouteConfigIcon =  new ImageIcon(getClass().getResource("images/RouteConfig.gif"));
        
        ApproverAddIcon =  new ImageIcon(getClass().getResource("images/Approver+.gif"));
        ApproverRemoveIcon =  new ImageIcon(getClass().getResource("images/Approver-.gif"));
        SendMailIcon =  new ImageIcon(getClass().getResource("images/SendMail.gif"));
        DontSendMailIcon =  new ImageIcon(getClass().getResource("images/DontSendMail.gif"));
        UserIcon =  new ImageIcon(getClass().getResource("images/UserIcon.gif"));
        UserGroupIcon =  new ImageIcon(getClass().getResource("images/SecurityIcon.png"));
        ApproverActionYesIcon =  new ImageIcon(getClass().getResource("images/ActionYes.gif"));
        ApproverActionNoIcon =  new ImageIcon(getClass().getResource("images/ActionNo.gif"));
        ApproverMoveDownIcon =  new ImageIcon(getClass().getResource("images/ApproverDown.gif"));
        ApproverMoveUpIcon =  new ImageIcon(getClass().getResource("images/ApproverUp.gif"));
        
        AcceptIcon =  new ImageIcon(getClass().getResource("images/Accept.gif"));
        RejectIcon =  new ImageIcon(getClass().getResource("images/Reject.gif"));
        EndRouteIcon =  new ImageIcon(getClass().getResource("images/EndRoute.gif"));
        SummaryIcon =  new ImageIcon(getClass().getResource("images/Summary.gif"));
        UsrIcon =  new ImageIcon(getClass().getResource("images/UsrIcon.gif"));        
        RouteSuspendIcon =  new ImageIcon(getClass().getResource("images/suspend.gif"));
        RouteStartButtonIcon =  new ImageIcon(getClass().getResource("images/startbutton.gif"));
        RouteEndButtonIcon =  new ImageIcon(getClass().getResource("images/endbutton.gif"));
        RouteTaskButtonIcon =  new ImageIcon(getClass().getResource("images/taskbutton.gif"));
        RouteSuspendButtonIcon =  new ImageIcon(getClass().getResource("images/suspendbutton.gif"));        
        StartIcon =  new ImageIcon(getClass().getResource("images/start.gif"));
        TaskIcon =  new ImageIcon(getClass().getResource("images/task.gif"));
        StopIcon =  new ImageIcon(getClass().getResource("images/end.gif"));
        StartImage = StartIcon.getImage();
        TaskImage = TaskIcon.getImage(); 
        StopImage = StopIcon.getImage();
        
		/****CV2019 merges from SIDBI line***/
        documentVwrIcon = new ImageIcon(getClass().getResource("images/MetaDataReport.png"));
        /************************************/
        docMetaDataIcon =  new ImageIcon(getClass().getResource("images/MetaDataReport.png"));        
        docRouteSummaryIcon =  new ImageIcon(getClass().getResource("images/SummaryReport.png"));
        includePagesIcon = new ImageIcon(getClass().getResource("images/IncludePages.png"));  
        BrowseIcon = new ImageIcon(getClass().getResource("images/browse.gif"));
        NotifyImage = new ImageIcon(getClass().getResource("images/NotifyImage.png"));
        NotifyIcon = new ImageIcon(getClass().getResource("images/NotifyIcon.png"));
        
        addRow = new ImageIcon(getClass().getResource("images/Approver+.gif"));
        removeRow = new ImageIcon(getClass().getResource("images/Approver-.gif"));

	DBLookupIcon = new ImageIcon(getClass().getResource("images/DbLookup.gif"));
	plusIcon = new ImageIcon(getClass().getResource("images/plus.gif"));
	minusIcon = new ImageIcon(getClass().getResource("images/minus.gif"));
	addNewTemplateIcon = new ImageIcon(getClass().getResource("images/plus.gif"));
	reset=new ImageIcon(getClass().getResource("images/reset.png"));
	findIndexWithRightArrow = new ImageIcon(getClass().getResource("images/find-arrow-right.gif"));
	upIcon= new ImageIcon(getClass().getResource("images/up.gif"));
    downIcon= new ImageIcon(getClass().getResource("images/down.gif"));
    PreviewInRouteIcon = new ImageIcon(getClass().getResource("images/DocumentInRoute.gif"));
    }
        catch (Exception e){
            System.out.println("Error while loading Images "+e.toString());
            e.printStackTrace();
        }
    }
    //Load image when you need it
    public ImageIcon getIcon(int iconID)
    {
        switch (iconID)
        {
            case (COPY_ICON):
            if(CopyIcon==null)
                try
                {
                    CopyIcon=new ImageIcon(getClass().getResource("images/Copy.gif"));
                }
                catch(Exception e){System.out.println(
                    "Error while loading [Copy.gif] ->"+e.toString());}
            return CopyIcon;
        }
        return null;
    }
	//DRS related changes
    public static ImageIcon MoveLeftIcon = null;
    public static ImageIcon MoveRightIcon = null;
    public static ImageIcon MoveUpIcon = null;
    public static ImageIcon MoveLeftAllIcon = null;
    public static ImageIcon MoveRightAllIcon = null;
    public static ImageIcon MoveDownIcon = null;
    

    public static ImageIcon FindIcon = null;
    public static ImageIcon DocListIcon = null;
    public static ImageIcon OptionsIcon = null;
    public static ImageIcon PropertyImage = null;
    public static ImageIcon TemplateImage = null;
    public static ImageIcon AdvancedFindImage = null;
    public static ImageIcon SearchCommentImage = null;
    public static ImageIcon CabinetCollapsedIcon = null;
    public static ImageIcon CabinetExpandedIcon = null;
    public static ImageIcon DrawerCollapsedIcon = null;
    public static ImageIcon DrawerExpandedIcon = null;
    public static ImageIcon FolderCollapsedIcon = null;
    public static ImageIcon FolderExpandedIcon = null;
    public static ImageIcon CheckOutIcon = null;
    public static ImageIcon CheckInIcon = null;
    public static ImageIcon RoomUnConnectIcon = null;
    public static ImageIcon RoomDownIcon = null;
    public static ImageIcon RoomConnectIcon = null;
    public static ImageIcon AllRoomIcon = null;
    public static ImageIcon SecuriyImage = null;
    public static ImageIcon SessionIcon=null;
    public static ImageIcon DocTypesIcon=null;
    public static ImageIcon ConUserImage=null;
    public static ImageIcon DocLockImage=null;
    public static ImageIcon ArchiveImage=null;
    public static ImageIcon StatisticImage=null;
    public static ImageIcon PropertiesImage=null;
    public static ImageIcon ATImage=null;
    public static ImageIcon AMOptionsImage=null;
    public static ImageIcon LogViewerImage=null;
    public static ImageIcon DocTypeImage=null;
    public static ImageIcon OptionsImage=null;
    public static ImageIcon FindImage=null;
    //DRS related changes
    public static ImageIcon DocRouteImage = null;
    public static ImageIcon RouteImage = null;
    public static ImageIcon FormTemplateImage = null;
    public static ImageIcon FormTemplateUIImage=null;
    public static ImageIcon NewRouteIcon = null;
    public static ImageIcon AssignRouteIcon = null;
    public static ImageIcon DocumentInRouteIcon = null;
    
    public static ImageIcon DocRetentionImage = null;
    
    public static ImageIcon NewDocTypeIcon=null;
    public static ImageIcon NewIcon=null;
    public static ImageIcon AddIcon=null;
    public static ImageIcon DelIcon=null;
    public static ImageIcon CustomReportIcon=null;
    public static ImageIcon ClearIcon=null;
    public static ImageIcon UpIcon=null;
    public static ImageIcon DownIcon=null;
    public static ImageIcon StorageIcon=null;
    public static ImageIcon ConUserIcon=null;
    public static ImageIcon DocLockIcon=null;
    public static ImageIcon RecycleImage=null;
    public static ImageIcon RedactionsImage=null;
    public static ImageIcon ATIcon=null;
    public static ImageIcon StatisticIcon=null;
    public static ImageIcon AdminWiseIcon=null;
    public static ImageIcon NewRoomImage=null;
    public static ImageIcon RefreshIcon=null;
    public static ImageIcon SelectAllIcon=null;
    public static ImageIcon DeselectAllIcon=null;
    public static ImageIcon TemplateIcon=null;
    public static ImageIcon DSNIcon=null;
    public static ImageIcon SecurityReportIcon=null;
    public static ImageIcon SubAdminIcon=null;
    public static ImageIcon BackupImage=null;
    public static ImageIcon RestoreImage=null;
    public static ImageIcon BackupIcon=null;
    public static ImageIcon RestoreIcon=null;
    public static ImageIcon RedactionsIcon=null;
    public static ImageIcon RecycleIcon=null;
    
    public static ImageIcon RadioIcon1=null;
    public static ImageIcon RadioIcon2=null;
    public static ImageIcon RadioIcon3=null;
    public static ImageIcon RadioIcon4=null;
    public static ImageIcon RadioIcon5=null;
    public static ImageIcon HelpIcon=null;
    public static ImageIcon HelpOnlineIcon=null;
    public static ImageIcon ConnectIcon=null;
    public static ImageIcon DisconnectIcon=null;
    public static ImageIcon FilterIcon=null;
    public static ImageIcon CloseIcon=null;
    public static ImageIcon ReportCloseIcon=null;
    public static ImageIcon SaveIcon=null;
    public static ImageIcon UpdateIcon=null;
    public static ImageIcon ExportIcon=null;
    public static ImageIcon PrintIcon=null;
    public static ImageIcon SaveAsIcon=null;
    public static ImageIcon CopyIcon=null;
    public static ImageIcon OkIcon=null;
    public static ImageIcon ReplaceIcon=null;
    public static ImageIcon LoadIcon=null;
    public static ImageIcon SizeIcon=null;
    public static ImageIcon MailIcon=null;
    public static ImageIcon VerRevIcon=null;
    public static ImageIcon RetentionIcon=null;
    public static ImageIcon ExecuteIcon=null;
    //DRS related changes
    public static ImageIcon UserEmailIdImage = null;
    public static ImageIcon UpdateRouteImage = null;
    public static ImageIcon BrowseRouteImage = null;
    
    public static ImageIcon MoveNextIcon=null;
    public static ImageIcon MoveLastIcon=null;
    public static ImageIcon MoveFirstIcon=null;
    public static ImageIcon MovePreviousIcon=null;
    
    public static ImageIcon RouteConfigIcon =null;
    
    public static ImageIcon ApproverAddIcon =null;
    public static ImageIcon ApproverRemoveIcon =null;
    public static ImageIcon SendMailIcon =null;
    public static ImageIcon DontSendMailIcon =null;

    public static ImageIcon UserIcon=null;
    public static ImageIcon UserGroupIcon=null;
    public static ImageIcon ApproverActionYesIcon=null;
    public static ImageIcon ApproverActionNoIcon=null;
    public static ImageIcon ApproverMoveDownIcon=null;
    public static ImageIcon ApproverMoveUpIcon=null;
    public static ImageIcon AcceptIcon= null;
    public static ImageIcon RejectIcon= null;
    public static ImageIcon EndRouteIcon= null;
    public static ImageIcon SummaryIcon= null;
    public static ImageIcon UsrIcon= null;
    
    public static ImageIcon RouteSuspendIcon= null;    
    public static ImageIcon RouteStartButtonIcon= null;
    public static ImageIcon RouteEndButtonIcon= null;
    public static ImageIcon RouteTaskButtonIcon= null;
    public static ImageIcon RouteSuspendButtonIcon= null;

    public static ImageIcon StartIcon= null;
    public static ImageIcon TaskIcon= null;
    public static ImageIcon StopIcon= null;
    public static Image StartImage= null;
    public static Image TaskImage= null;
    public static Image StopImage = null;
    
    public static ImageIcon DBLookupIcon= null;
    public static ImageIcon plusIcon= null;
    public static ImageIcon minusIcon= null;
    
    public static ImageIcon documentVwrIcon =null;
    public static ImageIcon docMetaDataIcon =null;
    public static ImageIcon docRouteSummaryIcon =null;
    public static ImageIcon includePagesIcon =null;
    public static ImageIcon BrowseIcon =null;
    public static ImageIcon NotifyImage = null;
    public static ImageIcon NotifyIcon = null;
    
	public static ImageIcon addRow = null;
	public static ImageIcon removeRow = null;
	public static ImageIcon addNewTemplateIcon = null;
	public static ImageIcon reset=null;
	public static ImageIcon findIndexWithRightArrow = null;
	public static ImageIcon PreviewInRouteIcon = null;
	public static ImageIcon upIcon = null;
	public static ImageIcon downIcon = null;

//------------------------------------------------------------------------------
}