/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWDSN;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.awt.Dimension;
import javax.swing.JDialog;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.border.TitledBorder;
import java.awt.Insets;
import javax.swing.BoxLayout;
import javax.swing.border.TitledBorder;
import ViewWise.AdminWise.VWTree.VWTree;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import ViewWise.AdminWise.VWTree.VWTreeNode;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.util.prefs.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.border.LineBorder;

import com.computhink.common.DBLookup;

import java.sql.SQLException;

public class VWDNSDlg extends javax.swing.JDialog implements  VWConstant
{
	public VWDNSDlg(Frame parent)
	{
            super(parent,true);
            ///parent.setIconImage(VWImages.DSNIcon.getImage());
            getContentPane().setBackground(AdminWise.getAWColor());
            setTitle(LBL_DBLINK_NAME);
            getContentPane().setLayout(null);
            setVisible(false);
            JPanel1.setLayout(null);
            getContentPane().add(JPanel1);
            JPanel1.setBorder(titledBorder1);
            JPanel1.setBounds(4,4,341,142);
            JLabel1.setText(LBL_DSNNAME_NAME);
            JPanel1.add(JLabel1);
            JLabel1.setBounds(8,26,74,14);
            JLabel2.setText(LBL_USER_NAME);
            JPanel1.add(JLabel2);
            JLabel2.setBounds(8,52,74,19);
            JLabel3.setText(LBL_PASSWORD);
            JPanel1.add(JLabel3);
            JLabel3.setBounds(8,80,74,24);
            JPanel1.add(TxtDSNName);
            TxtDSNName.setBounds(88,25,247,20);
            JPanel1.add(TxtUserName);
            TxtUserName.setBounds(88,53,247,19);
            
            JLabel6.setText(LBL_DSNStatus);
            JPanel1.add(JLabel6);
            JLabel6.setBounds(8,110,82, AdminWise.gBtnHeight);
            
            JPanel1.add(CmbDSNStatus);
            CmbDSNStatus.setBounds(88, 110, 82, AdminWise.gBtnHeight);
            CmbDSNStatus.addItem("Disabled");	
            CmbDSNStatus.addItem("Enabled");	
            CmbDSNStatus.setSelectedIndex(1);
            
            BtnConnect.setText(BTN_CONNECT_NAME);
            JPanel1.add(BtnConnect);
            BtnConnect.setBounds(250,110,82, AdminWise.gBtnHeight);
//            BtnConnect.setBackground(java.awt.Color.white);
            BtnConnect.setIcon(VWImages.ConnectIcon);
            
            JPanel1.add(TxtPassword);
            TxtPassword.setBounds(88,80,246,19);
            JPanel2.setLayout(null);
            JPanel2.setBorder(titledBorder2); 
            getContentPane().add(JPanel2);
            JPanel2.setBounds(4,150,341,150);
            JLabel4.setText(LBL_TABLE_NAME);
            JPanel2.add(JLabel4);
            JLabel4.setBounds(8,20,74,24);
            JPanel2.add(CmbTable);
            CmbTable.setBounds(8,44,156, AdminWise.gBtnHeight);
            JLabel5.setText(LBL_FIELD_NAME);
            JPanel2.add(JLabel5);
            JLabel5.setBounds(178,20,74,24);
            JPanel2.add(CmbField);
            CmbField.setBounds(178,44,156, AdminWise.gBtnHeight);
            JPanel2.add(JSPSQL);
            JSPSQL.setBounds(8,74,326,64);
            TxtSQL.setBackground(java.awt.Color.white);
            TxtSQL.setAutoscrolls(true);
            TxtSQL.setWrapStyleWord(true);
            ///TxtSQL.setBorder(LineBorder.createGrayLineBorder());
            BtnSave.setText(BTN_SAVE_NAME);
//          BtnSave.setBackground(java.awt.Color.white);
            getContentPane().add(BtnSave);
            BtnSave.setBounds(8,312,82, AdminWise.gBtnHeight);
            BtnSave.setIcon(VWImages.SaveIcon);
          
            BtnExecute.setText(BTN_EXECUTE_NAME);
//            BtnExecute.setBackground(java.awt.Color.white);
            getContentPane().add(BtnExecute);
            BtnExecute.setBounds(138,312,82, AdminWise.gBtnHeight);
            BtnExecute.setIcon(VWImages.ExecuteIcon);
            BtnCancel.setText(BTN_CANCEL_NAME);
//            BtnCancel.setBackground(java.awt.Color.white);
            getContentPane().add(BtnCancel);
            BtnCancel.setBounds(258,312,82, AdminWise.gBtnHeight);
            BtnCancel.setIcon(VWImages.CloseIcon);
            setResizable(false);
            parent.setIconImage(VWImages.StorageIcon.getImage());
            SymKey aSymKey = new SymKey();
            TxtDSNName.addKeyListener(aSymKey);
            TxtUserName.addKeyListener(aSymKey);
            TxtPassword.addKeyListener(aSymKey);
            TxtSQL.addKeyListener(aSymKey);
            SymAction aSymAction=new SymAction();
            BtnSave.addActionListener(aSymAction);
            BtnConnect.addActionListener(aSymAction);
            BtnExecute.addActionListener(aSymAction);
            BtnCancel.addActionListener(aSymAction);
            CmbTable.addActionListener(aSymAction);
            CmbField.addActionListener(aSymAction);
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            getDlgOptions();
            BtnConnect.setEnabled(!TxtDSNName.getText().trim().equals(""));
            BtnExecute.setEnabled(false);
            CmbTable.setEnabled(false);
            CmbField.setEnabled(false);
            //TxtSQL.setEnabled(false);
            JLabel4.setEnabled(false);
            JLabel5.setEnabled(false);
            setVisible(true);
	}
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDNSDlg.this)
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(BtnConnect.isEnabled() && event.getKeyText(event.getKeyChar()).equals("Enter"))
                    BtnConnect_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
//------------------------------------------------------------------------------
        public void keyReleased(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(object==TxtDSNName)
            {
                String dsnName=TxtDSNName.getText();
                ///dsnName+=event.getKeyChar();
                if(dsnName.trim().equals(""))
                {
                    BtnConnect.setEnabled(false);
                }
                else
                {
                    BtnConnect.setEnabled(true);
                }
            }
            else if(object==TxtSQL)
            {
                String sqlString=TxtSQL.getText().trim();
                if(sqlString.equals(""))
                {
                    BtnExecute.setEnabled(false);
                }
                else
                {
                    BtnExecute.setEnabled(true);
                }
            }
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnConnect)
                        BtnConnect_actionPerformed(event);
                else if (object == BtnExecute)
                        BtnExecute_actionPerformed(event);			
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
                else if (object == CmbTable)
                        CmbTable_actionPerformed(event);
                else if (object == CmbField)
                        CmbField_actionPerformed(event);
                else if (object == BtnSave)
                	 BtnSave_actionPerformed(event);                
        }
    }
//------------------------------------------------------------------------------
    //{{DECLARE_CONTROLS
    javax.swing.JPanel storagePanel = new javax.swing.JPanel();
    VWComboBox CmbTable = new VWComboBox();
    VWComboBox CmbField = new VWComboBox();
    VWComboBox CmbDSNStatus = new VWComboBox();
    VWLinkedButton BtnConnect = new VWLinkedButton(0,true);
    VWLinkedButton BtnExecute = new VWLinkedButton(0,true);
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    TitledBorder titledBorder1 = new TitledBorder(LBL_DSNINFO_NAME);
    TitledBorder titledBorder2 = new TitledBorder(LBL_SQLSTATEMENT_NAME);
    javax.swing.JPanel JPanel1 = new javax.swing.JPanel();
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel4 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel5 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel6 = new javax.swing.JLabel();
    javax.swing.JTextField TxtDSNName = new javax.swing.JTextField();
    javax.swing.JTextField TxtUserName = new javax.swing.JTextField();
    javax.swing.JPasswordField TxtPassword = new javax.swing.JPasswordField();
    javax.swing.JPanel JPanel2 = new javax.swing.JPanel();
    javax.swing.JTextArea TxtSQL = new javax.swing.JTextArea();
    javax.swing.JScrollPane JSPSQL = new javax.swing.JScrollPane(TxtSQL);
//  ------------------------------------------------------------------------------
    void BtnExecute_actionPerformed(java.awt.event.ActionEvent event)
    {
        String sqlStatement=TxtSQL.getText().trim();
        getExecuteSql(sqlStatement);
        if(result !=null && result.size()>0)
        {
            VWDSNConnector.closeConnection();
            saveDlgOptions();
            this.setVisible(false);
        }
    }
//------------------------------------------------------------------------------
    public boolean validateDSNData()
    {
        String dsnName=TxtDSNName.getText().trim();
        if(dsnName.equals(""))
        {
            VWMessage.showMessage(this,VWMessage.ERR_DSN_NONAME);
            TxtDSNName.requestFocus();
            return false;
        }
        return true;
    }    
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        VWDSNConnector.closeConnection();
        saveDlgOptions();
        result=null;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
    	AdminWise.adminPanel.setWaitPointer();
    	DBLookup dbLookup = new DBLookup(0);
    	try{
    		dbLookup.setDsnName(TxtDSNName.getText().trim());
    		if(CmbTable.isEnabled())
    			dbLookup.setExternalTable(CmbTable.getSelectedItem().toString());
    		dbLookup.setUserName((TxtUserName.getText().trim().equals("")?"-":TxtUserName.getText().trim()));
    		String pwrd = new String(TxtPassword.getPassword());
    		dbLookup.setPassWord(pwrd.equals("")?"-":pwrd);
    		dbLookup.setConfigQuery((TxtSQL.getText().equals("")?"-":TxtSQL.getText()));
    		dbLookup.setDsnStatus(CmbDSNStatus.getSelectedIndex());
    		dbLookup.setDsnType(1);
    		//Don't want to save the table name as in the DBLookup.
    		dbLookup.setExternalTable("-");
    		dbLookup.setIndexId(AdminWise.adminPanel.docTypePanel.getSelIndexId());
    		dbLookup.setDocTypeId(AdminWise.adminPanel.docTypePanel.getSelDTId());
    		

    		setSelectionDbDsnInfo(dbLookup);
    		AdminWise.adminPanel.setDefaultPointer();
    	}catch(Exception ex){}
    }
    
    void setSelectionDbDsnInfo(DBLookup dbDSNInfo){
    	ViewWise.AdminWise.VWDocType.VWIndexRec selIndex=AdminWise.adminPanel.docTypePanel.IndicesTable.getRowData(
                AdminWise.adminPanel.docTypePanel.IndicesTable.getSelectedRow());
    	selIndex.setDbDSNInfo(dbDSNInfo);
    }

    void BtnConnect_actionPerformed(java.awt.event.ActionEvent event)
    {
        AdminWise.adminPanel.setWaitPointer();
        if(connectToDSN())
        {
            CmbTable.setEnabled(true);
            CmbField.setEnabled(true);
            //TxtSQL.setEnabled(true);
            JLabel4.setEnabled(true);
            JLabel5.setEnabled(true);
            loadTables();
        }
        AdminWise.adminPanel.setDefaultPointer();
    }
//------------------------------------------------------------------------------
    void CmbTable_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(!lodedTable || CmbTable.getSelectedIndex()<0) return;
        String tableName=CmbTable.getSelectedItem().toString();
        if(tableName!=null && !tableName.equals(""))
        loadFields(tableName);
        createSQLStatement();
    }
//------------------------------------------------------------------------------
    void CmbField_actionPerformed(java.awt.event.ActionEvent event)
    {
        createSQLStatement();
    }
//------------------------------------------------------------------------------
    private void loadTables()
    {
        lodedTable=false;
        CmbTable.removeAllItems();
        List tables=null;
        try
        {
            tables=VWDSNConnector.getDSNTables();
        }
        catch (SQLException e)
        {
            VWMessage.showMessage(this,e.getLocalizedMessage());
            System.out.println(e.getLocalizedMessage());  
            return;
        }
        if(tables==null || tables.size()==0) return ;
        int count = tables.size();
        for(int i=0;i<count;i++)
        {
            CmbTable.addItem((String)tables.get(i));
        }
        lodedTable=true;
        CmbTable.setSelectedIndex(-1);
    }
//------------------------------------------------------------------------------
    private void loadFields(String tableName)
    {
        List tables=null;
        CmbField.removeAllItems();
        try{
            tables=VWDSNConnector.getDSNTableFields(tableName);
        }
        catch (SQLException e)
        {
            VWMessage.showMessage(this,e.getLocalizedMessage());
            System.out.println(e.getLocalizedMessage());  
            return;
        }
        if(tables==null || tables.size()==0) return ;
        int count = tables.size();
        for(int i=0;i<count;i++)
        {
            CmbField.addItem((String)tables.get(i));
        }
    }
//------------------------------------------------------------------------------    
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
        if(AdminWise.adminPanel.saveLastDSNName)
            prefs.put( "DSN Name",TxtDSNName.getText().trim());
        else
            prefs.put( "DSN Name","");
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        setSize(357,378);
        //TxtDSNName.setText(prefs.get("DSN Name",""));
        
        //get db values for dsn settings.
        int docTypeId = AdminWise.adminPanel.docTypePanel.getSelDTId();
        int indexId = AdminWise.adminPanel.docTypePanel.getSelIndexId();
        
        Vector<DBLookup> dsnInfo = new Vector<DBLookup>();
        dsnInfo = VWDSNConnector.getDSNInfo(docTypeId, indexId);
        
        if(dsnInfo.size()>0){
        	DBLookup dbLookup = dsnInfo.get(0);
        	TxtDSNName.setText(dbLookup.getDsnName());
        	TxtUserName.setText(dbLookup.getUserName().equals("-")?"":dbLookup.getUserName());
        	TxtPassword.setText(dbLookup.getPassWord().equals("-")?"":dbLookup.getPassWord());
	        TxtSQL.setText(dbLookup.getConfigQuery());
	        System.out.println("dsn status : "+dbLookup.getDsnStatus());
	        
	        CmbDSNStatus.setSelectedIndex(dbLookup.getDsnStatus());
        }
        if(!TxtDSNName.getText().equals(""))
            BtnConnect.setEnabled(true);
    }
//------------------------------------------------------------------------------
    private boolean connectToDSN()
    {
        try
        {
            VWDSNConnector.getDSNConnection(TxtDSNName.getText().trim(),
                TxtUserName.getText().trim(),
                /*
            	 * JPasswordField.getText() is reaplced with JPasswordfield.getPassword()
            	 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
            	 */
               new String(TxtPassword.getPassword()));
        }
        catch(java.lang.ClassNotFoundException e1)
        {
            VWMessage.showMessage(this,e1.getLocalizedMessage());
            System.out.println(e1.getLocalizedMessage()); 
            return false;
        }
        catch (SQLException e2)
        {
            VWMessage.showMessage(this,e2.getLocalizedMessage());
           System.out.println(e2.getLocalizedMessage()); 
           return false;
        }
        return true;
    }
//------------------------------------------------------------------------------
    private void createSQLStatement()
    {
        String sqlStatement="";
        if(CmbTable.getSelectedIndex()<0 || CmbField.getSelectedIndex()<0) return;
        String tableName=CmbTable.getSelectedItem().toString();
        String fieldName=CmbField.getSelectedItem().toString();
        if (tableName.indexOf(' ')>0) tableName=DoubleQuotation+tableName+DoubleQuotation;
        if (fieldName.indexOf(' ')>0) fieldName=DoubleQuotation+fieldName+DoubleQuotation;
        sqlStatement="Select " + fieldName + " From " + tableName;
        TxtSQL.setText(sqlStatement);
        BtnExecute.setEnabled(true);
    }
//------------------------------------------------------------------------------
    private void getExecuteSql(String sqlStatement)
    {
        AdminWise.adminPanel.setWaitPointer();
        try{
            result=VWDSNConnector.getExecuteSql(sqlStatement);
        }
        catch (SQLException e)
        {
            VWMessage.showMessage(this,e.getLocalizedMessage());
            System.out.println(e.getLocalizedMessage());
            AdminWise.adminPanel.setDefaultPointer();
            result=null;
            AdminWise.adminPanel.setDefaultPointer();
            return;
        }
        if(result==null || result.size()==0)
        {
            VWMessage.showMessage(this,VWMessage.ERR_DSN_NORESULT);
            AdminWise.adminPanel.setDefaultPointer();
            return;
        }
        AdminWise.adminPanel.setDefaultPointer();
    }
//------------------------------------------------------------------------------
    public List getValues()
    {
        return result;
    }
//------------------------------------------------------------------------------
    List result=new LinkedList();
    private boolean lodedTable=false;
} 