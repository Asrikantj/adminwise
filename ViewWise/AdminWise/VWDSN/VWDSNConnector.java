/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWPropertiesConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWDSN;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.lang.ClassNotFoundException;

import com.computhink.common.DBLookup;
//--------------------------------------------------------------------------
public class VWDSNConnector {
    public static void getDSNConnection(String dsnName,String userName,String password)
    throws SQLException,ClassNotFoundException {
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        
        String url = "jdbc:odbc:" + dsnName;
        if(!userName.equals("")){
            con = DriverManager.getConnection(url,userName,password);
            dsnUserName = userName;
        }
        else
            con = DriverManager.getConnection(url);
    }
    //--------------------------------------------------------------------------
    public static List getExecuteSql(String sqlStatement) throws SQLException {
        if(con==null || con.isClosed()) return null;
        List result = new LinkedList();
        if(sqlStatement.equals("")) return result;
        PreparedStatement call=con.prepareStatement(sqlStatement);
        ResultSet rs = call.executeQuery();
        while (rs.next()) {
            String sCol = rs.getString(1);
            if (sCol!=null) {
                result.add(sCol);
            }
        }
        rs.close();
        rs=null;
        con.clearWarnings();
        return result;
    }
    //--------------------------------------------------------------------------
    public static void closeConnection() {
        try {
            if(con!=null && !con.isClosed()) con.close();;
            con=null;
        }
        catch (java.sql.SQLException e) {
        }
    }
    //--------------------------------------------------------------------------
    public static List getDSNTables() throws SQLException {    	
        if(con==null || con.isClosed()) return null;
        List tables=new LinkedList();
        ResultSet rs=null;
        DatabaseMetaData dsnMetaData=con.getMetaData();
        String[] types = {"TABLE"};
        if (dsnMetaData.getDatabaseProductName().toLowerCase().indexOf("oracle") != -1){
        	rs = dsnMetaData.getTables(null,dsnUserName,null,types);	
        }else{
        	rs = dsnMetaData.getTables(null,null,null,types);
        }
                
        while (rs.next()) {
            String sCol = rs.getString("TABLE_NAME");
            if (tables.indexOf(sCol) == -1){
            	tables.add(sCol);
            }
/*            if (sCol!=null) {
                sCol=sCol.trim();
                if(tables.size()==0) {
                    tables.add(sCol);
                }
                else {
                    boolean inserted=false;
                    String strTmp=sCol.toLowerCase();
                    for(int i=0;i<tables.size();i++)
                        if(strTmp.compareTo(((String)tables.get(i)).toLowerCase()) < 0) {
                            tables.add(i,sCol);
                            inserted=true;
                            break;
                        }
                    if(!inserted) tables.add(sCol);
                }
            }*/
        }
        rs.close();
        rs=null;
        con.clearWarnings();
        return tables;
    }
    //--------------------------------------------------------------------------
    public static List getDSNTableFields(String tableName)throws SQLException {
        if(con==null || con.isClosed()) return null;
        List tableFields=new LinkedList();
        DatabaseMetaData dsnMetaData=con.getMetaData();
        ResultSet rs = dsnMetaData.getColumns(null,null,tableName,null);
        while (rs.next()) {
            String sCol = rs.getString("COLUMN_NAME");
            if (tableFields.indexOf(sCol) == -1){
            	tableFields.add(sCol);
            }
/*            if (sCol!=null) {
                sCol=sCol.trim();
                if(tableFields.size()==0) {
                    tableFields.add(sCol);
                }
                else {
                    boolean inserted=false;
                    String strTmp=sCol.toLowerCase();
                    for(int i=0;i<tableFields.size();i++)
                        if(strTmp.compareTo(((String)tableFields.get(i)).toLowerCase()) < 0) {
                            tableFields.add(i,sCol);
                            inserted=true;
                            break;
                        }
                    if(!inserted) tableFields.add(sCol);
                }
            }*/
        }
        rs.close();
        rs=null;
        con.clearWarnings();
        return tableFields;
    }
    public static void setDSNInfo(DBLookup dbLookup){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	AdminWise.gConnector.ViewWiseClient.setDBLookup(room.getId(), dbLookup);    	
    }
    public static Vector<DBLookup> getDSNInfo(int docTypeId, int indexId){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Vector<DBLookup> dsnInfo = new Vector<DBLookup>();
    	AdminWise.gConnector.ViewWiseClient.getDSNInfo(room.getId(), docTypeId, indexId, VWConstant.selListDSNInfo, dsnInfo);
    	return dsnInfo;
    }
    
    //--------------------------------------------------------------------------
    private static Connection con=null;
    private static String dsnUserName = "";
}
