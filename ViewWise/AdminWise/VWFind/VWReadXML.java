package ViewWise.AdminWise.VWFind;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWUtil;
import com.computhink.common.Index;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;

public class VWReadXML implements VWConstant
{

	//public static String searchDocName = "abc";
	private static String docType = "";
	private static ArrayList<String> docInfo = new ArrayList<String>();
	private static String dirName="", dirPath="", pages = "";

	private static Vector<Index> docTypeIndices = new Vector<Index>();
	private static Vector<String> dateInfo = new Vector<String>();
	private static Hashtable indKeyReqTable = new Hashtable();

	public static void findBackupData(String searchStr, String path, String cFromDate, String cToDate, 
			String mFromDate, String mToDate, ArrayList<String> backUpDocInfo){
		InputStream inputStream = null;
		Reader reader = null;
		InputSource is = null;
		try {
			if(path == null || path.trim().equals(""))
				return;
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			File docFolder = new File(path);
			File[] subDir = docFolder.listFiles(); // subDir = list of Document [TestServer.VW7][1523]

			docInfo.clear();
			VWCUtil.DateVector.removeAllElements();
			String action = "FindInBackupDocs";

			if(searchStr.trim().equals("") && cFromDate.trim().equals("") && cToDate.trim().equals("") 
					&& mFromDate.trim().equals("") && mToDate.trim().equals("")){
				for(int i=0; i<subDir.length; i++){
					dirName = subDir[i].getName();
					dirPath = subDir[i].getPath();
					File[] subFolders = subDir[i].listFiles(); //subFolders = Pages folder and Document.vwd
					if(subFolders != null && checkForPages(subFolders))
						pages = "Yes";
					else
						pages = "No";

					setDocInfo();
				}
			}else{
				MyHandler handler = new MyHandler(action, searchStr, cFromDate, cToDate, mFromDate, mToDate);

				for(int i=0; i<subDir.length; i++){
					dirName = subDir[i].getName();
					dirPath = subDir[i].getPath();
					File[] subFolders = subDir[i].listFiles(); //subFolders = Pages folder and Document.vwd

					if(subFolders != null && checkForPages(subFolders))
						pages = "Yes";
					else
						pages = "No";

					if(subFolders != null && subFolders.length > 0){		
						for(int j=0; j<subFolders.length; j++){
							if(subFolders[j].getName().equalsIgnoreCase(BackupFileName)){
								inputStream= new FileInputStream(subFolders[j]);
								reader = new InputStreamReader(inputStream);
								is = new InputSource(reader);
								is.setEncoding("UTF-8");
								//System.out.println("calling parse");
								saxParser.parse(is, handler);			
							}
						}
					}
				}
			}
			backUpDocInfo.addAll(docInfo);
		} catch (Exception e) {
			System.out.println("exception : "+e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				if(inputStream != null){
					inputStream.close();
					inputStream = null;
				}
				if(reader != null){
					reader.close();
					reader = null;
				}
				if(is != null){
					is = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String getDocProperties(String dirPath, Vector<String> cmDate, Vector<Index> docTypeIndicesInfo){
		InputStream inputStream = null;
		Reader reader = null;
		InputSource is = null;
		try{
			if(dirPath == null || dirPath.trim().equals(""))
				return "";
			dateInfo.clear();
			docTypeIndices.clear();
			dirPath = dirPath  + File.separator + BackupFileName;
			String action = "getDocProperties";
			MyHandler handler = new MyHandler(action);
			inputStream= new FileInputStream(dirPath);
			reader = new InputStreamReader(inputStream);
			is = new InputSource(reader);
			is.setEncoding("UTF-8");
			try{
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				//System.out.println("calling parse");
				saxParser.parse(is, handler);
			}
			catch(Exception ex){
			}
			cmDate.addAll(dateInfo);
			docTypeIndicesInfo.addAll(docTypeIndices);
			return docType;

		}catch(Exception e){
			System.out.println("Error while getting Document Properties : "+e.getMessage());
			e.printStackTrace();
			return null;
		}finally{
			try {
				if(inputStream != null){
					inputStream.close();
					inputStream = null;
				}
				if(reader != null){
					reader.close();
					reader = null;
				}
				if(is != null){
					is = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean checkForPages(File[] subFolders) {
		boolean isContainsPages = false;
		try{
			for(int i=0; i<subFolders.length; i++){
				if(subFolders[i].getName().equalsIgnoreCase(BackupPageFolderName)){
					File pageFolder = new File(subFolders[i].getPath());
					if(pageFolder.list() != null && pageFolder.list().length > 0)
						isContainsPages = true;
					else
						isContainsPages = false;
				}else if(subFolders[i].getName().equalsIgnoreCase(BackupFileName)){
					VWCUtil.parseXML(subFolders[i].getPath());
				}
			}
		}catch(Exception e){
			System.out.println("Exception : "+e.getMessage());
			e.printStackTrace();
		}
		return isContainsPages;

	}

	public static void setDocInfo(){
		String str = dirName+VWConstant.SepChar+dirPath+VWConstant.SepChar+pages;
		if(!docInfo.contains(str))
			docInfo.add(str);		
	}

	public static void addIndices(int indexId, String indexName, String indexValue) {
		Index index = new Index(indexId);
		index.setName(indexName);
		index.setValue(indexValue);

		String str = (String) indKeyReqTable.get(indexId);
		AdminWise.printToConsole("str >>>>"+str);
		/**Not null condition added for the issue -  Restore Tab:Select any document in the Restore list.Double click on the document or select the properties. The indices are not displaying for the document type**/
		if (str != null) {
			StringTokenizer st = new StringTokenizer(str, ",");
			if(st.hasMoreTokens()){
				index.setKey(st.nextToken().equalsIgnoreCase("true")?true:false);
				index.setRequired(st.nextToken().equalsIgnoreCase("true")?true:false);
			}
		}
		docTypeIndices.add(index);
	}

	public static void setDocTypeName(String value) {
		docType = value;		
	}

	public static void setIndKeyReqValue(int indexId, String DTkey, String required) {
		String str = DTkey+","+required;
		indKeyReqTable.put(indexId, str);
	}

	public static void main( String[] args ){
		String path = "C:\\Documents and Settings\\vijaykumar.s\\Desktop\\Backup Doc\\New Folder (2)\\Document\\";
		String searchStr = ""; //"doc";
		String cFromDate = "07/28/2011"; //"2011-08-27";
		String cToDate = "07/28/2011"; //"2011-08-29";
		String mFromDate = ""; //"2011-07-27";
		String mToDate = ""; //"2011-07-29";
		ArrayList<String> backUpDocInfo = new ArrayList<String>();

		findBackupData(searchStr, path, cFromDate, cToDate, mFromDate, mToDate, backUpDocInfo);

		System.out.println("DocIds Size : "+docInfo.size());
		System.out.println("back Up DocIds Size : "+backUpDocInfo.size());
		for(int j=0; j<backUpDocInfo.size(); j++){
			System.out.println("Doc Id "+(j+1)+" = "+backUpDocInfo.get(j));
		}

		/*Vector<Index> ind = new Vector<Index>();
		Vector<String> dateInfo = new Vector<String>();
		String docTypeName = getDocProperties(path, dateInfo, ind);
		//System.out.println("Main() ::::: docTypeName : "+docTypeName);

		System.out.println("************ date Info size : "+dateInfo.size());
		for(int i=0; i<dateInfo.size(); i++)
			System.out.println(dateInfo.get(i));

		System.out.println("ind size() : "+ind.size());
		for(int i=0; i<ind.size(); i++){
			Index index = (Index) ind.get(i);
			System.out.println("name:"+index.getName());
			System.out.println("value:"+index.getValue());
			System.out.println("key:"+index.isKey());
			System.out.println("req:"+index.isRequired());
		}*/
	}

	public static void setDateInfo(String value) {
		dateInfo.add(value);		
	}
}


class MyHandler extends DefaultHandler{

	boolean isDocument = false, isDocType = false;
	boolean isIndices = false, isName = false, isValue = false;
	boolean isCreationDate = false, isModifiedDate = false;

	boolean flag = true;

	int actionId = -1;
	String searchStr = "", docId = "";
	String cFromDate = "", cToDate = "", mFromDate = "", mToDate = "";

	public MyHandler(String action) {
		for(int i=0; i<actionString.length; i++){
			if(actionString[i].equalsIgnoreCase(action)){
				actionId = i;
				break;
			}
		}
	}

	public MyHandler(String action, String searchStr, String cFromDate, String cToDate, 
			String mFromDate, String mToDate){
		this.searchStr = searchStr;
		this.cFromDate = cFromDate;
		this.cToDate = cToDate;
		this.mFromDate = mFromDate;
		this.mToDate = mToDate;

		for(int i=0; i<actionString.length; i++){
			if(actionString[i].equalsIgnoreCase(action)){
				actionId = i;
				break;
			}			
		}

	}

	int indexId = 0;
	String indexName = "", indexValue = "";
	String required = "", DTkey = "";
	boolean isRequired = false, isDTKey = false;
	StringBuffer bufValue = new StringBuffer();

	public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
		//		System.out.println("Start Element :" + qName);
		try{
			if (qName.equalsIgnoreCase("VWDocument")) {
				isDocument = true;				
			}else if (qName.equalsIgnoreCase("Creation-Date")) {
				isCreationDate = true;						
			}else if (qName.equalsIgnoreCase("Modified-Date")) {
				isModifiedDate = true;						
			}else if (qName.equalsIgnoreCase("Indices")) {
				switch(actionId){
				case FIND_IN_BACKUP_DOCS:
					if(searchStr != null && !searchStr.trim().equals("")){
						isIndices = true;						
					}
					break;
				case GET_DOC_PROPERTIES:
					isIndices = true;
					break;
				}
			}else if(qName.equalsIgnoreCase("Name")){
				isName = true;				
			}else if (qName.equalsIgnoreCase("Value")) {
				isValue = true;						
			}else if (qName.equalsIgnoreCase("DocType")) {
				isDocType = true;						
			}else if (qName.equalsIgnoreCase("Required")) {
				isRequired = true;
			}else if (qName.equalsIgnoreCase("DTKey")) {
				isDTKey = true;						
			}

			//System.out.println("Attributes Length : "+attributes.getLength());
			if(isDocument){
				if(attributes != null && attributes.getLength()>0)
					docId = attributes.getValue("id");		
				isDocument = false;
			}
			if(isRequired){
				if(attributes != null && attributes.getLength()>0)
					indexId = Integer.parseInt(attributes.getValue("id"));				
			}
			if(isName){
				if(attributes != null && attributes.getLength()>0)
					indexId = Integer.parseInt(attributes.getValue("id"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		AdminWise.printToConsole("End Element :" + qName);
		if(qName.equalsIgnoreCase("Indices")){
			if(isIndices){
				isIndices = false;
				flag = false;   // set flag=false if isIndices is true (isIndices will be true once the search string is found)
			}
		}else if(qName.equalsIgnoreCase("Value")){
			AdminWise.printToConsole("actionId :" + actionId);
			switch(actionId){
			case FIND_IN_BACKUP_DOCS:
				if((isIndices && isValue && bufValue.length() > 0)){
					String val = VWUtil.fixXMLEntityString(bufValue.toString());
					if (val.toLowerCase().contains(searchStr.toLowerCase())) {
						isIndices = false;	//set isIndices flag to false once the search string is found						
					}
					bufValue = new StringBuffer();
				}
				isValue = false;
				break;

			case GET_DOC_PROPERTIES:
				if(isIndices){
					//System.out.println("String buffer :: "+buf);
					indexValue = VWUtil.fixXMLEntityString(bufValue.toString());
					AdminWise.printToConsole("indexValue :" + indexValue);
					VWReadXML.addIndices(indexId, indexName, indexValue);
					isValue = false;
					indexId = 0;
					indexName = "";
					indexValue = "";
					bufValue = new StringBuffer();
				}
				break;
			}
		}else if (qName.equalsIgnoreCase("Creation-Date")) {
			isCreationDate = false;						
		}else if (qName.equalsIgnoreCase("Modified-Date")) {
			isModifiedDate = false;						
		}else if(qName.equalsIgnoreCase("VWDocument")){
			switch(actionId){
			case FIND_IN_BACKUP_DOCS:
				if(flag){
					VWReadXML.setDocInfo();
				}else{
					flag = true;
				}
				break;

			case GET_DOC_PROPERTIES:
				break;
			}
		}else if (qName.equalsIgnoreCase("DocType")) {
			isDocType = false;						
		}else if (qName.equalsIgnoreCase("Name")) {
			if(isDocType && isName){
				String docTypeName = VWUtil.fixXMLEntityString(bufValue.toString());
				VWReadXML.setDocTypeName(docTypeName);
				isDocType = false;
			}else if(isIndices && isName){
				indexName = VWUtil.fixXMLEntityString(bufValue.toString());
			}
			bufValue = new StringBuffer();
			isName = false;						
		}else if (qName.equalsIgnoreCase("Required")) {
			isRequired = false;						
		}else if (qName.equalsIgnoreCase("DTKey")) {
			switch(actionId){
			case FIND_IN_BACKUP_DOCS:
				isDTKey = false;
				break;

			case GET_DOC_PROPERTIES:
				isDTKey = false;
				VWReadXML.setIndKeyReqValue(indexId, DTkey, required);
				indexId = 0;
				DTkey = "";
				required = "";
				break;
			}
		}
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		//System.out.println("********************* Start Characters *********************");
		String value = new String(ch, start, length);
		switch(actionId){
		case FIND_IN_BACKUP_DOCS:
			try{
				findInBackupDocs(value);
			}catch(Exception e){
				System.out.println("Exception while finding the backup documents "+e.getMessage());
				e.printStackTrace();
			}
			break;

		case GET_DOC_PROPERTIES:
			try{
				getDocProperties(value);

			}catch(Exception e){
				System.out.println("Exception while Getting the Document Properties : "+e.getMessage());
				e.printStackTrace();
			}
			break;
		}
	}

	private void getDocProperties(String value) {
		try{
			if(isCreationDate){
				VWReadXML.setDateInfo(value);
			}
			if(isModifiedDate){
				VWReadXML.setDateInfo(value);
			}
			if(isDocType){
				if(isName){
					bufValue.append(value);
					//VWReadXML.setDocTypeName(value);
					//isDocType = false;
				}				
			}
			if(isRequired){				
				required = value;
			}
			if(isDTKey){				
				DTkey = value;
			}
			if(isIndices){
				//System.out.println("***********getDocProperties()*********** ::: "+value);
				if(isName){
					bufValue.append(value);   // To read the complete Index Name
					//indexName = value;
					//isName = false;
				}
				if(isValue){
					bufValue.append(value);		// To read the complete Index Value
					//indexValue = value;
					//isValue = false;
				}
			}
		}catch(Exception e){
		}
	}

	private void findInBackupDocs(String value) {
		try{
			if(value != null && !value.trim().equals("")){
				if(flag){
					if(isCreationDate && !cFromDate.trim().equals("") && !cToDate.trim().equals("")){
						StringTokenizer st = new StringTokenizer(value, " ");  //value = "2011-07-27 17:53:46"
						boolean ret = false;
						if(st.hasMoreTokens()){
							value = st.nextToken();
							ret = checkDate(value, cFromDate, cToDate);
						}
						if(!ret)
							flag = false;
					}
					else if(isModifiedDate && !mFromDate.trim().equals("") && !mToDate.trim().equals("")){
						StringTokenizer st = new StringTokenizer(value, " ");
						boolean ret = false;
						if(st.hasMoreTokens()){
							value = st.nextToken();
							ret = checkDate(value, mFromDate, mToDate);
						}
						if(!ret)
							flag = false;						
					}
					else if((isIndices && isValue)){
						bufValue.append(value);
						/*if (value.toLowerCase().contains(searchStr.toLowerCase())) {
							isIndices = false;	//set isIndices flag to false once the search string is found
							//flag = false;
						}	*/				
					}
				}
			}
		}catch(Exception e){
		}
	}

	private boolean checkDate(String documentDate, String fromDate, String toDate) {
		boolean ret = false;
		try{
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(documentDate);

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date fDate = sdf.parse(fromDate);
			Date tDate = sdf.parse(toDate);
			Date docDate = sdf.parse(sdf.format(date));

			if(tDate.before(fDate)){
				ret = false;
			}else if(docDate.after(fDate) && docDate.before(tDate)){
				ret = true;
			}else if(docDate.equals(fDate) || docDate.equals(tDate)){
				ret = true;
			}else{
				ret = false;
			}			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}	
		return ret;
	}

	String[] actionString = {"FindInBackupDocs", "getDocProperties"};
	public static final int FIND_IN_BACKUP_DOCS = 0;
	public static final int GET_DOC_PROPERTIES = 1;
}
