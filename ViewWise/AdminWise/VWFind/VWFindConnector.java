/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWTableConnector<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWFind;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import ViewWise.AdminWise.VWArchive.VWArchiveConnector;
import ViewWise.AdminWise.VWBackup.VWBackupConnector;
import ViewWise.AdminWise.VWUtil.VWDataStorage;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import com.computhink.common.Search;
import com.computhink.common.DocType;
import com.computhink.common.Creator;
import com.computhink.common.Document;

public class VWFindConnector implements VWConstant
{    
    static int chunkSize=100;
    public static Vector getUsers(int roomId, boolean withFixedFields)
    {
        Vector users=new Vector();
        AdminWise.gConnector.ViewWiseClient.getCreators(roomId, users);
        if(users == null || users.size()==0) return null;
        ///return addFixedFieldToUsers(users,withFixedFields);
        return users;
    }
//--------------------------------------------------------------
    /*
    private static Vector addFixedFieldToUsers(Vector users,boolean withFixedFields)
    {
        if(withFixedFields)
            for(int i=0;i<Conds[AUTHOR_CONDS_FIXFIELDS].length;i++)
                users.add(i,new Creator(0,CondsArr[Conds[AUTHOR_CONDS_FIXFIELDS][i]].getName()));
        return users;
    }
     */
//--------------------------------------------------------------
    public static int createSearch(Search searchInfo,int roomId)
    {
        AdminWise.gConnector.ViewWiseClient.createSearch(roomId, searchInfo);
        return searchInfo.getId();
    }
//--------------------------------------------------------------
    public static Vector getSearchLists()
    {   
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector srchs=new Vector();
        int ret=AdminWise.gConnector.ViewWiseClient.getSearchs(room.getId(),srchs);
        return srchs;
    }
//--------------------------------------------------------------
    public static Search getSearchInfo(int searchId,int roomId)
    {
        Search srch=new Search(searchId,"");
        AdminWise.gConnector.ViewWiseClient.getSearch(roomId,srch);
        return srch;
    }
//--------------------------------------------------------------
    public static DocType getSearchDocTypeIndices(int docTypeId,int roomId)
    {
        DocType docType=new DocType(docTypeId);
        AdminWise.gConnector.ViewWiseClient.getDocTypeIndices(roomId,docType);
        return docType;
    }
//--------------------------------------------------------------
    public static void delSearch(int searchId)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        Search srch=new Search(searchId,"");
        AdminWise.gConnector.ViewWiseClient.delSearch(room.getId(), srch);
    }
//------------------------------------------------------------------------------ 
/*
    public static int saveHSearch(String searchName,int temp)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null && room.getConnectStatus()!=Room_Status_Connect) return 0;       
        
        return getSearchId(0,searchName,temp,0);
    }
*/
//------------------------------------------------------------------------------ 
    public static Vector getDocTypes(int roomId)
    {
        Vector types=new Vector();
        AdminWise.gConnector.ViewWiseClient.getDocTypes(roomId,types);
        return types;
    }
//------------------------------------------------------------------------------ 
public void callFind(int roomId, Search srch, boolean append)
{
    new searchAction(roomId, srch, append);
}
//------------------------------------------------------------------------------ 
public int find(int roomId, Search srch, boolean append)
{
    VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
    int resultCount = 0;
    AdminWise.adminPanel.setWaitPointer();
    VWMessage.showBrowserMsgWithAction(VWMessage.MSG_FIND_RUN);
    try{
        serverResultCount=0;
        boolean onlyDocs = true;
        resultCount=AdminWise.gConnector.ViewWiseClient.find(roomId,srch, onlyDocs);
        //resultCount=AdminWise.gConnector.ViewWiseClient.find(roomId,srch);
        if(resultCount>0) 
        {
            srch.setLastDocId(0);            
            serverResultCount=resultCount;
        }
    }catch(Exception e){
        System.out.println(e.getMessage());
        VWMessage.showMessage(null,VWMessage.ERR_FIND);
    }
    finally{
        AdminWise.adminPanel.setDefaultPointer();
        if(AdminWise.getCurrentMode()==APPLET_MODE)
            VWMessage.showMessage(null,"" + resultCount+" "+AdminWise.connectorManager.getString("FindConnector.msg_1"));
    }
    return resultCount;
    }
//------------------------------------------------------------------------------ 
    public int getFindResult(int roomId, Search srch, boolean append)
    {
        int resultCount = 0;
        try{
            Vector docs=new Vector();
            AdminWise.gConnector.ViewWiseClient.getFindResult(roomId,srch,docs,0);
            if(docs!=null && docs.size()>0)
            {
                resultCount=docs.size();
                srch.setLastDocId(((Document)docs.get(docs.size()-1)).getId());
            }
            if(VWFindDlg.getCallerID()==CALLER_ARCHIVE)
                VWArchiveConnector.loadDocumentFromFind(docs,append);
            else if(VWFindDlg.getCallerID()==CALLER_BACKUP)
                VWBackupConnector.loadDocumentFromFind(docs,append);
        }catch(Exception e){
            System.out.println(e.getMessage());
            VWMessage.showMessage(null,VWMessage.ERR_FIND);}
        return resultCount;
    }
//------------------------------------------------------------------------------ 
    public void setChunkSize(int chunkSize)
    {
        /*if(VWFindDlg.getCallerID()==CALLER_BACKUP)
            this.chunkSize=0;
        else*/ if(chunkSize>0) 
            this.chunkSize=chunkSize;
        else
            this.chunkSize=100;
    }
//------------------------------------------------------------------------------ 
    private class searchAction extends Thread 
    {
        Search srch;
        boolean append;
        int roomId;
        searchAction(int roomId, Search srch, boolean append)
        {
            this.srch=srch;
            this.append=append;
            this.roomId=roomId;
            setDaemon(true);
            setPriority(java.lang.Thread.MIN_PRIORITY);
            start();
        }
        public void run()
        {
            int curResultCount=0;
            searchCancel=false;
            try
            {
                 if(find(roomId,srch,append)>0)
                 {
                    srch.setRowNo(chunkSize);
                    if(!searchCancel)
                    {
                        curResultCount=getFindResult(roomId,srch,append);
                        resultCount+=curResultCount;
                    }
                    while(!searchCancel && curResultCount>0 && resultCount<serverResultCount)
                    {
                        curResultCount=getFindResult(roomId,srch,true);
                        resultCount+=curResultCount;
                    }
                 }
            }
            catch(Exception e){}
            finally{
                ViewWise.AdminWise.VWFind.VWFindPanel.setFindMode(true);
                searchCancel=false;
                resultCount=0;
            }
        }
    }
    public static int resultCount=0;
    public static int serverResultCount=0;
    public static boolean searchCancel=false;
}