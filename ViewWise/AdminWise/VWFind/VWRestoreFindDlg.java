package ViewWise.AdminWise.VWFind;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JFrame;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;

public class VWRestoreFindDlg extends javax.swing.JDialog implements VWConstant
{
	public VWRestoreFindDlg(Frame parent){
		super(parent,LBL_FIND_BACKUP_TITLE, true);
		getContentPane().setBackground(AdminWise.getAWColor());
		getContentPane().setLayout(new BorderLayout());
		setVisible(false);
		getContentPane().add(JSPRestoreFindPanel);
        setSize(350,260);
        setLocationRelativeTo(null);
        this.setResizable(false);
        restoreFindPanel.setBackground(AdminWise.getAWColor());
	}
	
	public static void main(String a[]){
		VWRestoreFindDlg dlg = new VWRestoreFindDlg(AdminWise.adminFrame);
		/*
		 * JDialog.show() is replaced with JDialog.setVisible() 
		 * as JDialog.show() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
		dlg.setVisible(true);
	}
	
	public VWRestoreFindPanel restoreFindPanel=new VWRestoreFindPanel();
    javax.swing.JScrollPane JSPRestoreFindPanel = new javax.swing.JScrollPane(restoreFindPanel);
}