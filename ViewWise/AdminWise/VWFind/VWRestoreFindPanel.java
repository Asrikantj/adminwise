package ViewWise.AdminWise.VWFind;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ViewWise.AdminWise.VWMessage.VWMessageConstant;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWFind.VWFindPanel.SymMouse;
import ViewWise.AdminWise.VWImages.VWImages;

import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;


public class VWRestoreFindPanel extends JPanel implements  VWConstant, VWMessageConstant
{
	public VWRestoreFindPanel() {
		int top = 10; int hGap = 20;

		BtnFind = new JButton();
		BtnClose = new JButton();
		BtnLocation = new JButton();
		setLayout(new BorderLayout());

		VWFind.setLayout(null);
		add(VWFind,BorderLayout.CENTER);
		VWFind.setBounds(0,0,350,290);
		PanelCommand.setLayout(null);
		VWFind.add(PanelCommand);
		//PanelCommand.setBackground(java.awt.Color.white);
		PanelCommand.setBounds(0,0,350,290);

		LblIndex.setText(LBL_INDEX_TEXT);
		PanelCommand.add(LblIndex);
		LblIndex.setBounds(8,top,80,19);
		LblIndex.setForeground(java.awt.Color.blue);

		top += hGap;

		TxtField.setText("");
		PanelCommand.add(TxtField);
		TxtField.setBackground(java.awt.Color.white);
		TxtField.setBounds(8, top, 322, 19);

		top += hGap;

		LblLocation.setText(LBL_BACKUP_LOCATION);
		PanelCommand.add(LblLocation);
		LblLocation.setBounds(8,top,144,19);
		LblLocation.setForeground(java.awt.Color.blue);

		top += hGap;		

		TxtLocation.setText(AdminWise.adminPanel.backupPath);
		TxtLocation.setEditable(false);
		PanelCommand.add(TxtLocation);
		TxtLocation.setBackground(java.awt.Color.white);
		TxtLocation.setBounds(8, top, 295, 19);

		//BtnLocation.setText("...");
		BtnLocation.setBounds(305, top, 25, 19);
		BtnLocation.setBorderPainted(true);
		BtnLocation.setIcon(VWImages.BrowseIcon);
		BtnLocation.setToolTipText(AdminWise.connectorManager.getString("general.browse"));
		PanelCommand.add(BtnLocation);
		
		top += hGap;

		LblDocCreated.setText(LBL_DOCCREATE);
		LblDocCreated.setForeground(java.awt.Color.blue);
		PanelCommand.add(LblDocCreated);
		LblDocCreated.setBounds(8,top,155,30);

		top += hGap+5;

		LblDocCFrom.setText(AdminWise.connectorManager.getString("RestoreFindPanel.from"));
		PanelCommand.add(LblDocCFrom);
		LblDocCFrom.setBounds(8,top,33,19);
		LblDocCFrom.setVisible(true);

		TxtCFrom.setBounds(43,top,120, AdminWise.gSmallBtnHeight);
		TxtCFrom.setDateFormat("MM/dd/yyyy");
		TxtCFrom.setEditable(false);
		TxtCFrom.setVisible(true);
		PanelCommand.add(TxtCFrom);

		LblDocCTo.setText(AdminWise.connectorManager.getString("RestoreFindPanel.to"));
		LblDocCTo.setVisible(true);
		PanelCommand.add(LblDocCTo);
		LblDocCTo.setBounds(190,top,49,19);

		TxtCTo.setBounds(213,top,120, AdminWise.gSmallBtnHeight);
		TxtCTo.setDateFormat("MM/dd/yyyy");
		TxtCTo.setEditable(false);
		TxtCTo.setVisible(true);
		PanelCommand.add(TxtCTo);


		top += hGap;


		LblDocModified.setText(LBL_DOCMOD);
		LblDocModified.setForeground(java.awt.Color.blue);
		PanelCommand.add(LblDocModified);
		LblDocModified.setBounds(8,top,155,30);

		top += hGap+5;

		LblDocMFrom.setText(AdminWise.connectorManager.getString("RestoreFindPanel.from"));
		PanelCommand.add(LblDocMFrom);
		LblDocMFrom.setBounds(8,top,33,19);
		LblDocMFrom.setVisible(true);

		TxtMFrom.setBounds(43, top, 120, AdminWise.gSmallBtnHeight);
		TxtMFrom.setDateFormat("MM/dd/yyyy");
		TxtMFrom.setEditable(false);
		TxtMFrom.setVisible(true);
		PanelCommand.add(TxtMFrom);

		LblDocMTo.setText(AdminWise.connectorManager.getString("RestoreFindPanel.to"));
		LblDocMTo.setVisible(true);
		PanelCommand.add(LblDocMTo);
		LblDocMTo.setBounds(190,top,49,19);

		TxtMTo.setBounds(213,top,120, AdminWise.gSmallBtnHeight);
		TxtMTo.setDateFormat("MM/dd/yyyy");
		TxtMTo.setEditable(false);
		TxtMTo.setVisible(true);
		PanelCommand.add(TxtMTo);


		top += hGap+10;



		BtnFind.setText(BTN_FINDNOW_NAME);
		BtnFind.setActionCommand(BTN_FINDNOW_NAME);
		BtnFind.setBounds(160,top, 80, AdminWise.gBtnHeight);
		BtnFind.setBorderPainted(true);
		PanelCommand.add(BtnFind);

		BtnClose.setText(BTN_CLOSE_NAME);
		BtnClose.setActionCommand(BTN_CLOSE_NAME);
		BtnClose.setBounds(250,top,80, AdminWise.gBtnHeight);
		//BtnClose.setIcon(VWImages.CloseIcon);
		BtnClose.setBorderPainted(true);
		PanelCommand.add(BtnClose);

		SymAction lSymAction = new SymAction();
		BtnLocation.addActionListener(lSymAction);
		BtnFind.addActionListener(lSymAction);
		BtnClose.addActionListener(lSymAction);
		
		SymMouse aSymMouse = new SymMouse();
        LblIndex.addMouseListener(aSymMouse);
		LblLocation.addMouseListener(aSymMouse);
		LblDocCreated.addMouseListener(aSymMouse);
		LblDocModified.addMouseListener(aSymMouse);
	}
	
	 class SymMouse extends java.awt.event.MouseAdapter
	    {
	        public void mouseExited(java.awt.event.MouseEvent event)
	        {
	            Object object = event.getSource();
	            if (object instanceof JLabel)
	                    Lbl_mouseExited(event);
	        }
	        public void mouseEntered(java.awt.event.MouseEvent event)
	        {
	            Object object = event.getSource();
	            if (object instanceof JLabel)
	                    Lbl_mouseEntered(event);
	        }	        
	    }
//	------------------------------------------------------------------------------
	    void Lbl_mouseEntered(java.awt.event.MouseEvent event)
	    {
	        ((JLabel)event.getSource()).setForeground(java.awt.Color.red);
	    }
//	------------------------------------------------------------------------------
	    void Lbl_mouseExited(java.awt.event.MouseEvent event)
	    {
	        ((JLabel)event.getSource()).setForeground(java.awt.Color.blue);
	    }
//	------------------------------------------------------------------------------

	class SymAction implements java.awt.event.ActionListener
	{
		public void actionPerformed(java.awt.event.ActionEvent event)
		{
			Object object = event.getSource();
			if (object == BtnLocation)
				BtnLocation_actionPerformed(event);
			if (object == BtnFind)
				BtnFind_actionPerformed(event);
			if (object == BtnClose)
				BtnClose_actionPerformed(event);			
		}

		
	}
	
	void BtnLocation_actionPerformed(ActionEvent event) {
		String path = loadOpenDialog(this);
		TxtLocation.setText(path);
	}
	void BtnFind_actionPerformed(ActionEvent event) {
		
		if(!validateFindData())
			return;
		
		String cFromDate = ((String) TxtCFrom.getSelectedItem() == null ? "" : (String) TxtCFrom.getSelectedItem());
		String cToDate = ((String) TxtCTo.getSelectedItem() == null ? "" : (String) TxtCTo.getSelectedItem());
		String mFromDate = ((String) TxtMFrom.getSelectedItem() == null ? "" : (String) TxtMFrom.getSelectedItem());
		String mToDate = ((String) TxtMTo.getSelectedItem() == null ? "" : (String) TxtMTo.getSelectedItem());
		
		ArrayList<String> backUpDocInfo = new ArrayList<String>();
		VWReadXML.findBackupData(TxtField.getText(), TxtLocation.getText(), cFromDate, cToDate, mFromDate, mToDate, backUpDocInfo);
		AdminWise.adminPanel.restorePanel.clearTable();
		AdminWise.adminPanel.restorePanel.insertData(backUpDocInfo);
		AdminWise.adminPanel.restorePanel.restoreFindDlg.setVisible(false);
		
	}
	private boolean validateFindData() {
		String cFromDate = (String)(TxtCFrom.getSelectedItem() == null ? "" : TxtCFrom.getSelectedItem());
		String cToDate = (String)(TxtCTo.getSelectedItem() == null ? "" : TxtCTo.getSelectedItem());
		String mFromDate = (String)(TxtMFrom.getSelectedItem() == null ? "" : TxtMFrom.getSelectedItem());
		String mToDate = (String)(TxtMTo.getSelectedItem() == null ? "" : TxtMTo.getSelectedItem());
		
		if(TxtLocation == null || TxtLocation.getText().trim().equals("")){
			VWMessage.showMessage(this, ViewWise.AdminWise.VWMessage.VWMessageConstant.MSG_SELECT_LOCATION);
			return false;
		}
		if((!cFromDate.trim().equals("") && cToDate.trim().equals("")) ||
				(cFromDate.trim().equals("") && !cToDate.trim().equals(""))){
			VWMessage.showMessage(this, VWMessageConstant.MSG_SELECT_From_To);
			return false;
		}
		if((!mFromDate.trim().equals("") && mToDate.trim().equals("")) ||
				(mFromDate.trim().equals("") && !mToDate.trim().equals(""))){
			VWMessage.showMessage(this, VWMessageConstant.MSG_SELECT_From_To);
			return false;
		}
		
		return true;
	}
	void BtnClose_actionPerformed(ActionEvent event) {
		AdminWise.adminPanel.restorePanel.restoreFindDlg.setVisible(false);		
	}
	public static String loadOpenDialog(Component parent) {
		JFileChooser chooser = new JFileChooser();
		if(!lastPath.equals(""))
			chooser.setSelectedFile(new File(lastPath));
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setCurrentDirectory(new File(AdminWise.adminPanel.backupPath));
		int returnVal = chooser.showOpenDialog(parent);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			String path=chooser.getSelectedFile().getPath();
			if(!path.equals("")) lastPath=path;
			return path;
		}
		return "";
	}


	JButton BtnFind = new JButton();
	JButton BtnClose = new JButton();	
	JButton BtnLocation = new JButton();

	javax.swing.JPanel VWFind = new javax.swing.JPanel();
	javax.swing.JPanel PanelCommand = new javax.swing.JPanel();

	JLabel LblIndex = new JLabel();
	JTextField TxtField = new JTextField();

	JLabel LblLocation = new JLabel();
	JTextField TxtLocation = new JTextField();

	JLabel LblDocCreated = new JLabel();
	JLabel LblDocCFrom = new JLabel();

	JLabel LblDocCTo = new JLabel();
	JLabel LblDocModified = new JLabel();
	JLabel LblDocMFrom = new JLabel();
	JLabel LblDocMTo = new JLabel();

	VWDateComboBox TxtCFrom = new VWDateComboBox();
	VWDateComboBox TxtCTo = new VWDateComboBox();
	VWDateComboBox TxtMFrom = new VWDateComboBox();
	VWDateComboBox TxtMTo = new VWDateComboBox();


	private static String lastPath="";

}