/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWFind;

import java.util.List;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.awt.Dimension;
import javax.swing.JDialog;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.border.TitledBorder;
import java.awt.Insets;
import javax.swing.BoxLayout;
import ViewWise.AdminWise.VWTree.VWTree;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import ViewWise.AdminWise.VWTree.VWTreeNode;
import ViewWise.AdminWise.VWConstant;
import java.util.prefs.*;
import javax.swing.tree.TreePath;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWImages.VWImages;

public class VWDlgLocation extends javax.swing.JDialog implements  VWConstant
{
	public boolean reportCabinetSearch = false;
	public VWDlgLocation(Frame parent,String caption)
	{
		super(parent,caption,true);
		try {
            ///parent.setIconImage(VWImages.FindIcon.getImage());
            getContentPane().setBackground(AdminWise.getAWColor());
            setSize(360,430);
            getContentPane().setLayout(null);
            treePanel.setBounds(6,6,342,342);
            treePanel.setBorder(titledBorder1);            
            getContentPane().add(treePanel);
            treePanel.setBackground(AdminWise.getAWColor());
            BtnOk.setText(BTN_OK_NAME);
            BtnOk.setActionCommand(BTN_OK_NAME);
            //BtnOk.setBackground(java.awt.Color.white);
            getContentPane().add(BtnOk);
            BtnOk.setIcon(VWImages.OkIcon);
            BtnRefresh.setText(BTN_REFRESH_NAME);
            BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
            //BtnRefresh.setBackground(java.awt.Color.white);
            BtnRefresh.setIcon(VWImages.RefreshIcon);
            getContentPane().add(BtnRefresh);
            BtnClose.setText(BTN_CLOSE_NAME);
            BtnClose.setActionCommand(BTN_CLOSE_NAME);
//            BtnClose.setBackground(java.awt.Color.white);
            BtnClose.setIcon(VWImages.CloseIcon);
            getContentPane().add(BtnClose);
            if(languageLocale.equals("nl_NL")){
            BtnOk.setBounds(48,360,72+15, AdminWise.gBtnHeight);
            BtnRefresh.setBounds(140,360,72+15, AdminWise.gBtnHeight);
            BtnClose.setBounds(232,360,72+15, AdminWise.gBtnHeight);
            }else{
            	  BtnOk.setBounds(48,360,72, AdminWise.gBtnHeight);
                  BtnRefresh.setBounds(140,360,72, AdminWise.gBtnHeight);
                  BtnClose.setBounds(232,360,72, AdminWise.gBtnHeight);
            }
            SymAction lSymAction = new SymAction();
            BtnRefresh.addActionListener(lSymAction);
            BtnOk.addActionListener(lSymAction);
            BtnClose.addActionListener(lSymAction);
            setResizable(false);
            loadDlgData();
            SymKey aSymKey =new SymKey();
            tree.addKeyListener(aSymKey);
            BtnOk.addKeyListener(aSymKey);
            BtnClose.addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            getDlgOptions();
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in VWDlgLocation load :"+e.getMessage());
		}
	}
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgLocation.this)
                BtnClose_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnClose_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnOk)
                        BtnOk_actionPerformed(event);			
                else if (object == BtnRefresh)
                        BtnRefresh_actionPerformed(event);			
                else if (object == BtnClose)
                        BtnClose_actionPerformed(event);			
        }
    }
//------------------------------------------------------------------------------
    //{{DECLARE_CONTROLS
    public VWTree tree = new VWTree();
    javax.swing.JScrollPane treePanel = new javax.swing.JScrollPane(tree);
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    TitledBorder titledBorder1 = new TitledBorder(LBL_LOCATION_NAME);
    VWLinkedButton BtnOk =  new VWLinkedButton(0,true);
    VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    private boolean isValidFolder()
    {
    	String nodePath = tree.getSelectedNodeStrPath();
    	int firstIndex = nodePath.indexOf("\\");
    	if(firstIndex == -1){
    		javax.swing.JOptionPane.showMessageDialog(this,AdminWise.connectorManager.getString("dlgSaveLocation.msg1")+" ","",javax.swing.JOptionPane.OK_OPTION);
    		return false;
    	}else{ 
    		StringTokenizer st = new StringTokenizer(nodePath, "\\");
    		if(st.countTokens() >= 4){
    			return true;
    		}else{
    			javax.swing.JOptionPane.showMessageDialog(this,AdminWise.connectorManager.getString("dlgSaveLocation.msg1")+" ","",javax.swing.JOptionPane.OK_OPTION);
    			return false;
    		}
    	}
    }
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {   
    	AdminWise.printToConsole("reportCabinetSearch in BtnOk_actionPerformed :"+reportCabinetSearch);
    	/**CV10.2 - Added for Workflow report cabinet selection issue****/
    	if (!reportCabinetSearch) {
	    	if(isValidFolder()){
	    		AdminWise.printToConsole("inside valid folder...");
	    		saveDlgOptions();
	    		cancelFlag=false;
	    		setVisible(false);
	    	}
    	} else {
    		saveDlgOptions();
    		cancelFlag=false;
    		setVisible(false);
    	}
    	
    }
//------------------------------------------------------------------------------
    public void loadDlgData()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect /*&& gCurRoom!=room.getId()*/)
        {
            gCurRoom=room.getId();
            treeLoaded=false;
            VWTreeConnector.setTree(tree);
            tree.loadTreeData();
            /*CV10.2 - This method has added for single tree node selection*******/
            AdminWise.printToConsole("reportCabinetSearch in loadDlgData :"+reportCabinetSearch);
            if (reportCabinetSearch) {
            	tree.setSingleNodeSelection(true);
            }
        }
}
//------------------------------------------------------------------------------     
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        VWTreeConnector.setTree(tree);
        tree.loadTreeData();
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x",this.getX());
            prefs.putInt( "y",this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
    
    /**CV10.2 - Added for Workflow report cabinet selection issue****/
    public void setCabinetSearch() {
    	reportCabinetSearch = true;
    	AdminWise.printToConsole("reportCabinetSearch in setCabinetSearch:"+reportCabinetSearch);
    }
//------------------------------------------------------------------------------
    String languageLocale=AdminWise.getLocaleLanguage();
    private boolean treeLoaded=false;
    private static int gCurRoom=0;
    public boolean cancelFlag=true;
}