/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWadminPanel<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWFind;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ViewWise.AdminWise.VWTree.VWTree;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Panel;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import ViewWise.AdminWise.VWConstant;
import java.awt.Dimension;
import java.awt.Frame;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;
import java.awt.Insets;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import java.util.prefs.*;

public class VWFindDlg extends javax.swing.JDialog implements VWConstant
{   
    public VWFindDlg(Frame parent,int callerID)
    {
        super(parent,LBL_FIND_TITLE,true);
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.FindIcon.getImage());
        getContentPane().setLayout(new BorderLayout());
        setVisible(false);
        getContentPane().add(JSPFindPanel);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        setSize(650,534);
        this.setResizable(false);
        this.callerID=callerID;
        findPanel.setCaller(callerID);
        loadTabData();
        getDlgOptions();
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object instanceof VWFindDlg)
                Dlg_WindowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dlg_WindowClosing(java.awt.event.WindowEvent event)
    {
        saveDlgOptions();
    }
//------------------------------------------------------------------------------
    public void addNotify()
    {
        Dimension size = getSize();
        super.addNotify();
        if (frameSizeAdjusted)
                return;
        frameSizeAdjusted = true;
        Insets insets = getInsets();
        ///setSize(insets.left + insets.right + size.width, insets.top + insets.bottom + size.height);
    }
//------------------------------------------------------------------------------
    public void loadTabData()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect && gCurRoom!=room.getId())
        {
            findPanel.loadData(room.getId(),room.getName());
        }
    }
//------------------------------------------------------------------------------
    public void setVisible(boolean b)
    {
        if(b)
        {
            loadTabData();
            findPanel.setFindMode(!VWFindConnector.searchCancel);
        }
      /*
        else
            findPanel.unloadTabData();
       **/
        super.setVisible(b);
    }
//------------------------------------------------------------------------------
    public void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
        /*
        prefs.putInt("width", this.getSize().width);
        prefs.putInt("height", this.getSize().height);
         */
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        ///setSize(prefs.getInt("width",657),prefs.getInt("height",564));
    }
//------------------------------------------------------------------------------
public void setAppendEnabled(boolean appendEnabled)
{
    findPanel.setAppendEnabled(appendEnabled);
}
//------------------------------------------------------------------------------
    public static int getCallerID()
    {
        return callerID;
    }
//------------------------------------------------------------------------------
    boolean frameSizeAdjusted = false;
    private static int gCurRoom=0;
    private static int callerID =0;//0 archive , 1 backup
    public VWFindPanel findPanel=new VWFindPanel();
    javax.swing.JScrollPane JSPFindPanel = new javax.swing.JScrollPane(findPanel);
//------------------------------------------------------------------------------
}