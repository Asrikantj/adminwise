package ViewWise.AdminWise.VWFind;

import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import java.util.Vector;
import javax.swing.JLabel;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import java.awt.Rectangle;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWMessage.VWMessage;
import java.util.List;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import com.computhink.common.Search;
import com.computhink.common.SearchCond;
import com.computhink.common.Creator;
import com.computhink.common.DocType;

public class VWFindPanel extends JPanel implements  VWConstant
{
	public VWFindPanel()
	{
			//Desc :ReInitialised static objects.		
			BtnFind = new VWLinkedButton(0);
			BtnClose = new VWLinkedButton(0);
			ChkInHitList = new javax.swing.JCheckBox();
			ChkAppend = new javax.swing.JCheckBox();
			
			setLayout(new BorderLayout());
            VWFind.setLayout(null);
            add(VWFind,BorderLayout.CENTER);
            VWFind.setBounds(0,0,575,433);
            PanelCommand.setLayout(null);
            VWFind.add(PanelCommand);
            PanelCommand.setBackground(java.awt.Color.white);
            PanelCommand.setBounds(0,0,168,432);
            Pic.setIconTextGap(0);
            Pic.setAutoscrolls(true);
            PanelCommand.add(Pic);
            Pic.setBounds(8,12,152,132);
            PanelCommand.add(CmbSavedSearch);
            CmbSavedSearch.setBackground(java.awt.Color.white);
            CmbSavedSearch.setBorder(etchedBorder); 
            CmbSavedSearch.setBounds(12,144,144, AdminWise.gSmallBtnHeight);
            TxtSearchName.setVisible(false);
            PanelCommand.add(TxtSearchName);
            TxtSearchName.setBackground(java.awt.Color.white);
            TxtSearchName.setBorder(etchedBorder); 
            TxtSearchName.setBounds(12,144,144, AdminWise.gSmallBtnHeight);
            BtnSaveSearch.setText(BTN_SAVE_NAME);
            BtnSaveSearch.setActionCommand(BTN_SAVE_NAME);
            PanelCommand.add(BtnSaveSearch);
//            BtnSaveSearch.setBackground(java.awt.Color.white);
            //BtnSaveSearch.setBorder(etchedBorder); 
            BtnSaveSearch.setEnabled(false);
            BtnSaveSearch.setBounds(12,166,72, AdminWise.gBtnHeight);
            BtnSaveSearch.setIcon(VWImages.SaveIcon);
            BtnDelSearch.setText(BTN_DEL_NAME);
            BtnDelSearch.setActionCommand(BTN_DEL_NAME);
            PanelCommand.add(BtnDelSearch);
//            BtnDelSearch.setBackground(java.awt.Color.white);
            //BtnDelSearch.setBorder(etchedBorder); 
            BtnDelSearch.setEnabled(false);
            BtnDelSearch.setBounds(84,166,72, AdminWise.gBtnHeight);
            BtnDelSearch.setIcon(VWImages.DelIcon);
            BtnFind.setText(BTN_FINDNOW_NAME);
            BtnFind.setActionCommand(BTN_FINDNOW_NAME);
            PanelCommand.add(BtnFind);
//            BtnFind.setBackground(java.awt.Color.white);
            BtnFind.setBounds(12,200,144, AdminWise.gBtnHeight);
            BtnFind.setIcon(VWImages.FindIcon);
            ///BtnSave.setText(BTN_SAVESEARCH_NAME);
            ///BtnSave.setActionCommand(BTN_SAVESEARCH_NAME);
            /*
            PanelCommand.add(BtnSave);
            BtnSave.setBackground(java.awt.Color.white);
            BtnSave.setBounds(12,230,144, AdminWise.gBtnHeight);
            BtnOption.setText(BTN_OPTIONS_NAME);
            BtnOption.setActionCommand(BTN_OPTIONS_NAME);
            PanelCommand.add(BtnOption);
            BtnOption.setBackground(java.awt.Color.white);
            BtnOption.setBounds(12,265,144, AdminWise.gBtnHeight);
            */
            BtnReset.setText(BTN_RESET_NAME);
            BtnReset.setActionCommand(BTN_RESET_NAME);
            PanelCommand.add(BtnReset);
//            BtnReset.setBackground(java.awt.Color.white);
            BtnReset.setBounds(12,235,144, AdminWise.gBtnHeight);
            BtnReset.setIcon(VWImages.ClearIcon);
            BtnClose.setText(BTN_CLOSE_NAME);
            BtnClose.setActionCommand(BTN_CLOSE_NAME);
            PanelCommand.add(BtnClose);
//            BtnClose.setBackground(java.awt.Color.white);
            BtnClose.setBounds(12,270,144, AdminWise.gBtnHeight);
            BtnClose.setIcon(VWImages.CloseIcon);
            JPanel2.setLayout(null);
            JPanel2.setBounds(2,4,453,466);
            JLabel1.setText(LBL_INDEXTEXT);
            JPanel2.add(JLabel1);
            JLabel1.setBounds(8,16,144,19);
            JPanel2.add(TxtField);
            TxtField.setBounds(160,12,279,21);
            JLabel2.setText(LBL_CONTAINTSTEXT);
            JPanel2.add(JLabel2);
            JLabel2.setBounds(8,50,144,26);
            JPanel2.add(TxtContaints);
            TxtContaints.setBounds(160,48,279,21);
            LblLocation.setText(LBL_LOCATION +" >>");
            LblLocation.setForeground(java.awt.Color.blue);
            JPanel2.add(LblLocation);
            LblLocation.setBounds(8,84,155,30);
            JPanel2.add(TxtLocation);
            TxtLocation.setBounds(160,100,279, AdminWise.gSmallBtnHeight);
            TxtLocation.setEditable(false);
            TxtLocation.setVisible(false);
            JPanel2.add(BtnLocation);
            BtnLocation.setBounds(439,100,28, AdminWise.gSmallBtnHeight);
//            BtnLocation.setBackground(java.awt.Color.white);
            BtnLocation.setVisible(false);
            BtnLocation.setText("...");
            LblLocationInfo.setBounds(160,84,279,30);
            /*
            LblLocationInfo.setText(AdminWise.adminPanel.roomPanel.getSelectedRoom().getName());
             */
            JPanel2.add(LblLocationInfo);
            JPanel2.add(LstDocType);
            LstDocType.setBounds(64,150,375,64);
            LstDocType.setVisible(false);
            LblDocType.setText(LBL_DOCTYPES+" >>");
            LblDocType.setForeground(java.awt.Color.blue);
            JPanel2.add(LblDocType);
            LblDocType.setBounds(8,120,155,30);
            LblDocTypeInfo.setBounds(160,120,279,30);
            LblDocTypeInfo.setText(LBL_NODOCTYPE_COND);
            JPanel2.add(LblDocTypeInfo);
            JPanel2.add(BtnDTRefresh);
//            BtnDTRefresh.setBackground(java.awt.Color.white);
            BtnDTRefresh.setBounds(440,150,22, AdminWise.gSmallBtnHeight);
            BtnDTRefresh.setVisible(false);
            BtnDTRefresh.setToolTipText(BTN_REFRESH_NAME);
            BtnDTRefresh.setActionCommand(BTN_REFRESH_NAME);
            BtnDocTypeIndices.setToolTipText(LBL_DOCTYPEINDICES);
            BtnDocTypeIndices.setActionCommand(BTN_DOCTYPEINDICES_NAME);
            JPanel2.add(BtnDocTypeIndices);            
            BtnDocTypeIndices.setBounds(440,171,22, AdminWise.gSmallBtnHeight);
//            BtnDocTypeIndices.setBackground(java.awt.Color.white);
            BtnDocTypeIndices.setVisible(false);
            JPanel2.add(BtnDeSelectDT);
//            BtnDeSelectDT.setBackground(java.awt.Color.white);
            BtnDeSelectDT.setToolTipText(AdminWise.connectorManager.getString("FindPanel.BtnDeSelectDTToolTip"));
            BtnDeSelectDT.setBounds(440,192,22, AdminWise.gSmallBtnHeight);
            BtnDeSelectDT.setVisible(false);
            JPanel2.add(LstAuthors);
            LstAuthors.setBounds(64,184,375,66);
            LstAuthors.setVisible(false);
            LblAuthor.setText(LBL_AUTHORS + " >>");
            LblAuthor.setForeground(java.awt.Color.blue);
            JPanel2.add(LblAuthor);
            LblAuthor.setBounds(8,156,155,30);
            LblAuthorInfo.setBounds(160,156,279,30);
            LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
            JPanel2.add(LblAuthorInfo);
            JPanel2.add(BtnAuthorRefresh);
            BtnAuthorRefresh.setToolTipText(BTN_REFRESH_NAME);
            BtnAuthorRefresh.setBounds(440,184,22, AdminWise.gSmallBtnHeight);
//            BtnAuthorRefresh.setBackground(java.awt.Color.white);
            BtnAuthorRefresh.setVisible(false);
            JPanel2.add(BtnAuthorSelectAll);
//            BtnAuthorSelectAll.setBackground(java.awt.Color.white);
            BtnAuthorSelectAll.setBounds(440,206,22, AdminWise.gSmallBtnHeight);
            BtnAuthorSelectAll.setVisible(false);
            BtnAuthorSelectAll.setToolTipText(LBL_SELECTALL);
            JPanel2.add(BtnAuthorDeSelect);
//            BtnAuthorDeSelect.setBackground(java.awt.Color.white);
            BtnAuthorDeSelect.setToolTipText(LBL_DESELECTALL);
            BtnAuthorDeSelect.setBounds(440,228,22, AdminWise.gSmallBtnHeight);
            BtnAuthorDeSelect.setVisible(false);
            LblDocCreate.setText(LBL_DOCCREATE + " >>");
            LblDocCreate.setForeground(java.awt.Color.blue);
            JPanel2.add(LblDocCreate);
            LblDocCreate.setBounds(8,192,155,30);
            LblDocCInfo.setBounds(160,192,279,30);
            LblDocCInfo.setText(LBL_NODATE_COND);
            JPanel2.add(LblDocCInfo);
            LblDocCFrom.setText(AdminWise.connectorManager.getString("FindPanel.from"));
            JPanel2.add(LblDocCFrom);
            LblDocCFrom.setBounds(160,214,49,19);
            LblDocCFrom.setVisible(false);
            JPanel2.add(TxtCFrom);
            TxtCFrom.setBounds(216,220,222, AdminWise.gSmallBtnHeight);
            TxtCFrom.setDateFormat("MM/dd/yyyy");
            TxtCFrom.setEditable(false);
            TxtCFrom.setVisible(false);
            JPanel2.add(TxtCTo);
            TxtCTo.setBounds(216,250,222, AdminWise.gSmallBtnHeight);
            TxtCTo.setDateFormat("MM/dd/yyyy");
            TxtCTo.setEditable(false);
            TxtCTo.setVisible(false);
            LblDocCTo.setText(AdminWise.connectorManager.getString("FindPanel.to"));
            LblDocCTo.setVisible(false);
            JPanel2.add(LblDocCTo);
            LblDocCTo.setBounds(160,244,49,19);
            LblDocMod.setText(LBL_DOCMOD + " >>");
            LblDocMod.setForeground(java.awt.Color.blue);
            JPanel2.add(LblDocMod);
            LblDocMod.setBounds(8,230,155,30);
            LblDocMInfo.setBounds(160,230,279,30);
            LblDocMInfo.setText(LBL_NODATE_COND);
            JPanel2.add(LblDocMInfo);
            LblDocMFrom.setText(AdminWise.connectorManager.getString("FindPanel.from"));
            JPanel2.add(LblDocMFrom);
            LblDocMFrom.setBounds(160,250,49,19);
            LblDocMFrom.setVisible(false);
            JPanel2.add(TxtMTo);
            TxtMTo.setBounds(216,286,222, AdminWise.gSmallBtnHeight);
            TxtMTo.setDateFormat("MM/dd/yyyy");
            TxtMTo.setEditable(false);
            TxtMTo.setVisible(false);
            JPanel2.add(TxtMFrom);
            TxtMFrom.setBounds(216,256,222, AdminWise.gSmallBtnHeight);
            TxtMFrom.setDateFormat("MM/dd/yyyy");
            TxtMFrom.setEditable(false);
            TxtMFrom.setVisible(false);
            LblDocMTo.setText(AdminWise.connectorManager.getString("FindPanel.to"));
            JPanel2.add(LblDocMTo);
            LblDocMTo.setBounds(160,280,49,19);
            LblDocMTo.setVisible(false);
            ChkAppend.setText(CHK_APPENDHITLIST_NAME);
            ChkAppend.setActionCommand(CHK_APPENDHITLIST_NAME);
	    PanelCommand.add(ChkAppend);
            ChkAppend.setBackground(java.awt.Color.white);
            ChkAppend.setBorder(etchedBorder); 
            ChkAppend.setBounds(12,345,144, AdminWise.gSmallBtnHeight);
            ChkVersions.setText(LBL_VERSIONS);
            ChkVersions.setActionCommand(LBL_VERSIONS);
	    PanelCommand.add(ChkVersions);
            ChkVersions.setBackground(java.awt.Color.white);
            ChkVersions.setBorder(etchedBorder); 
            ChkVersions.setBounds(12,370,144, AdminWise.gSmallBtnHeight);
            ChkComment.setText(LBL_COMMENT);
            ChkComment.setActionCommand(LBL_COMMENT);
	    PanelCommand.add(ChkComment);
            ChkComment.setBackground(java.awt.Color.white);
            ChkComment.setBorder(etchedBorder); 
            ChkComment.setBounds(12,395,144, AdminWise.gSmallBtnHeight);

            ChkInHitList.setText(BTN_FINDINLIST_NAME);
            ChkInHitList.setActionCommand(BTN_FINDINLIST_NAME);
	    PanelCommand.add(ChkInHitList);
            ChkInHitList.setBackground(java.awt.Color.white);
            ChkInHitList.setBorder(etchedBorder); 
            ChkInHitList.setBounds(12,420,144, AdminWise.gSmallBtnHeight);
            ChkInHitList.setEnabled(false);
            
            ChkChunkSize.setVisible(false);
            ChkChunkSize.setText(LBL_CHUNKSIZE);
            ChkChunkSize.setActionCommand(LBL_CHUNKSIZE);
	    PanelCommand.add(ChkChunkSize);
            ChkChunkSize.setBackground(java.awt.Color.white);
            ChkChunkSize.setBorder(etchedBorder); 
            ChkChunkSize.setBounds(12,460,100, AdminWise.gSmallBtnHeight);
            ChkChunkSize.setSelected(true);
            PanelCommand.add(TxtChunkSize);
            TxtChunkSize.setBackground(java.awt.Color.white);
            TxtChunkSize.setBorder(etchedBorder); 
            TxtChunkSize.setBounds(112,460,44, AdminWise.gSmallBtnHeight);
            TxtChunkSize.setText("100");
            TxtChunkSize.setColumns(4);
            TxtChunkSize.setVisible(false);

            PanelTable.setLayout(new BorderLayout());
            VWFind.add(PanelTable);
            PanelTable.setBounds(168,0,433,429);
            SPTable.setOpaque(true);
            PanelTable.add(SPTable,BorderLayout.CENTER);
            SPTable.getViewport().add(JPanel2);
            SymComponent aSymComponent = new SymComponent();
            VWFind.addComponentListener(aSymComponent);
            Pic.setIcon(VWImages.FindImage);
            SymAction lSymAction = new SymAction();
            BtnDTRefresh.addActionListener(lSymAction);
            BtnReset.addActionListener(lSymAction);
            ///BtnSave.addActionListener(lSymAction);
            BtnSaveSearch.addActionListener(lSymAction);
            CmbSavedSearch.addActionListener(lSymAction);
            BtnDocTypeIndices.addActionListener(lSymAction);
            BtnClose.addActionListener(lSymAction);
            BtnDelSearch.addActionListener(lSymAction);
            BtnDeSelectDT.addActionListener(lSymAction);
            BtnAuthorDeSelect.addActionListener(lSymAction);
            BtnAuthorSelectAll.addActionListener(lSymAction);
            BtnAuthorRefresh.addActionListener(lSymAction);
            ChkChunkSize.addActionListener(lSymAction);
            BtnFind.addActionListener(lSymAction);
            BtnLocation.addActionListener(lSymAction);
            SymMouse aSymMouse = new SymMouse();
            LblLocation.addMouseListener(aSymMouse);
            LblDocType.addMouseListener(aSymMouse);
            LblAuthor.addMouseListener(aSymMouse);
            LblDocCreate.addMouseListener(aSymMouse);
            LblDocMod.addMouseListener(aSymMouse);
	}
	
	 /**CV10.2 - Workflow rport enhancement changes*****/
	 public VWFindPanel(VWDTIndicesDlg docTypeIndicesDlg, int sessionID, JLabel LblDocType, VWCheckList LstAuthors) {
		this.docTypeIndicesDlg = docTypeIndicesDlg;
		this.gCurRoom = sessionID;
		this.LblDocType = LblDocType;
		this.LstAuthors = LstAuthors;
	 }
	 /** end of CV10.2 code merges*****/
//------------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter
    {
        public void mouseExited(java.awt.event.MouseEvent event)
        {
            Object object = event.getSource();
            if (object instanceof JLabel)
                    Lbl_mouseExited(event);
        }
        public void mouseEntered(java.awt.event.MouseEvent event)
        {
            Object object = event.getSource();
            if (object instanceof JLabel)
                    Lbl_mouseEntered(event);
        }
        public void mouseClicked(java.awt.event.MouseEvent event)
        {
            Object object = event.getSource();
            if (object == LblLocation)
                    LblLocation_mouseClicked(event);
            else if (object == LblDocType)
                LblDocType_mouseClicked(event);
            else if (object == LblAuthor)
                LblAuthor_mouseClicked(event);
            else if (object == LblDocCreate)
                LblDocCreate_mouseClicked(event);
            else if (object == LblDocMod)
                LblDocMod_mouseClicked(event);
        }
    }
//------------------------------------------------------------------------------
    void Lbl_mouseEntered(java.awt.event.MouseEvent event)
    {
        ((JLabel)event.getSource()).setForeground(java.awt.Color.red);
    }
//------------------------------------------------------------------------------
    void Lbl_mouseExited(java.awt.event.MouseEvent event)
    {
        ((JLabel)event.getSource()).setForeground(java.awt.Color.blue);
    }
//------------------------------------------------------------------------------
    void LblLocation_mouseClicked(java.awt.event.MouseEvent event)
    {
        if(LblLocation.getText().endsWith(">>"))
        {
            LblLocation.setText(LBL_LOCATION + " <<");
            TxtLocation.setVisible(true);
            TxtLocation.setText(LblLocationInfo.getText());
            BtnLocation.setVisible(true);
            LblLocationInfo.setVisible(false);
        }
        else
        {
            LblLocation.setText(LBL_LOCATION + " >>");
            TxtLocation.setVisible(false);
            BtnLocation.setVisible(false);
            LblLocationInfo.setText(TxtLocation.getText());
            LblLocationInfo.setVisible(true);
        }
    }
    void LblDocType_mouseClicked(java.awt.event.MouseEvent event)
    {
        if(LblDocType.getText().endsWith(">>"))
        {
            LblDocType.setText(LBL_DOCTYPES + " <<");
            LstDocType.setVisible(true);
            BtnDocTypeIndices.setVisible(true);
            BtnDTRefresh.setVisible(true);
            BtnDeSelectDT.setVisible(true);
            LblDocTypeInfo.setVisible(false);
            Rectangle rec=LblAuthor.getBounds();
            rec.y=rec.y + 70;
            LblAuthor.setBounds(rec);
            rec=LstAuthors.getBounds();
            rec.y=rec.y + 70;
            LstAuthors.setBounds(rec);
            rec=BtnAuthorRefresh.getBounds();
            rec.y=rec.y + 70;
            BtnAuthorRefresh.setBounds(rec);
            rec=BtnAuthorDeSelect.getBounds();
            rec.y=rec.y + 70;
            BtnAuthorDeSelect.setBounds(rec);
            rec=BtnAuthorSelectAll.getBounds();
            rec.y=rec.y + 70;
            BtnAuthorSelectAll.setBounds(rec);
            rec=LblDocCreate.getBounds();
            rec.y=rec.y + 70;
            LblDocCreate.setBounds(rec);
            rec=LblDocCFrom.getBounds();
            rec.y=rec.y + 70;
            LblDocCFrom.setBounds(rec);
            rec=TxtCFrom.getBounds();
            rec.y=rec.y + 70;
            TxtCFrom.setBounds(rec);
            rec=LblDocCTo.getBounds();
            rec.y=rec.y + 70;
            LblDocCTo.setBounds(rec);
            rec=TxtCTo.getBounds();
            rec.y=rec.y + 70;
            TxtCTo.setBounds(rec);
            rec=LblDocMod.getBounds();
            rec.y=rec.y + 70;
            LblDocMod.setBounds(rec);
            rec=LblDocMFrom.getBounds();
            rec.y=rec.y + 70;
            LblDocMFrom.setBounds(rec);
            rec=TxtMFrom.getBounds();
            rec.y=rec.y + 70;
            TxtMFrom.setBounds(rec);
            rec=LblDocMTo.getBounds();
            rec.y=rec.y + 70;
            LblDocMTo.setBounds(rec);
            rec=TxtMTo.getBounds();
            rec.y=rec.y + 70;
            TxtMTo.setBounds(rec);
            rec=LblAuthorInfo.getBounds();
            rec.y=rec.y + 70;
            LblAuthorInfo.setBounds(rec);
            rec=LblDocCInfo.getBounds();
            rec.y=rec.y + 70;
            LblDocCInfo.setBounds(rec);
            rec=LblDocMInfo.getBounds();
            rec.y=rec.y + 70;
            LblDocMInfo.setBounds(rec);
            /*
            rec=AdminWise.adminPanel.archivePanel.findDlg.getBounds();
            rec.height =rec.height+70;
            AdminWise.adminPanel.archivePanel.findDlg.setBounds(rec);
             */
            if(LstDocType.getItemsCount()==0)
                loadDocTypeFields();
        }
        else
        {
            LblDocType.setText(LBL_DOCTYPES + " >>");
            LstDocType.setVisible(false);
            BtnDocTypeIndices.setVisible(false);
            BtnDTRefresh.setVisible(false);
            BtnDeSelectDT.setVisible(false);
            LblDocTypeInfo.setVisible(true);
            LblDocTypeInfo.setText(getInfoText(LBL_DOCUMENTTYPE,LstDocType.getselectedItemsCount()));
            Rectangle rec=LblAuthor.getBounds();
            rec.y=rec.y - 70;
            LblAuthor.setBounds(rec);
            rec=LstAuthors.getBounds();
            rec.y=rec.y - 70;
            LstAuthors.setBounds(rec);
            rec=BtnAuthorRefresh.getBounds();
            rec.y=rec.y - 70;
            BtnAuthorRefresh.setBounds(rec);
            rec=BtnAuthorDeSelect.getBounds();
            rec.y=rec.y - 70;
            BtnAuthorDeSelect.setBounds(rec);
            rec=BtnAuthorSelectAll.getBounds();
            rec.y=rec.y - 70;
            BtnAuthorSelectAll.setBounds(rec);
            rec=LblDocCreate.getBounds();
            rec.y=rec.y - 70;
            LblDocCreate.setBounds(rec);
            rec=LblDocCFrom.getBounds();
            rec.y=rec.y - 70;
            LblDocCFrom.setBounds(rec);
            rec=TxtCFrom.getBounds();
            rec.y=rec.y - 70;
            TxtCFrom.setBounds(rec);
            rec=LblDocCTo.getBounds();
            rec.y=rec.y - 70;
            LblDocCTo.setBounds(rec);
            rec=TxtCTo.getBounds();
            rec.y=rec.y - 70;
            TxtCTo.setBounds(rec);
            rec=LblDocMod.getBounds();
            rec.y=rec.y - 70;
            LblDocMod.setBounds(rec);
            rec=LblDocMFrom.getBounds();
            rec.y=rec.y - 70;
            LblDocMFrom.setBounds(rec);
            rec=TxtMFrom.getBounds();
            rec.y=rec.y - 70;
            TxtMFrom.setBounds(rec);
            rec=LblDocMTo.getBounds();
            rec.y=rec.y - 70;
            LblDocMTo.setBounds(rec);   
            rec=TxtMTo.getBounds();
            rec.y=rec.y - 70;
            TxtMTo.setBounds(rec);
            rec=LblAuthorInfo.getBounds();
            rec.y=rec.y - 70;
            LblAuthorInfo.setBounds(rec);
            rec=LblDocCInfo.getBounds();
            rec.y=rec.y - 70;
            LblDocCInfo.setBounds(rec);                    
            rec=LblDocMInfo.getBounds();
            rec.y=rec.y - 70;
            LblDocMInfo.setBounds(rec);
            /*
            rec=AdminWise.adminPanel.archivePanel.findDlg.getBounds();
            rec.height =rec.height-70;
            AdminWise.adminPanel.archivePanel.findDlg.setBounds(rec);
             */
        }
    }
    void LblAuthor_mouseClicked(java.awt.event.MouseEvent event)
    {
        if(LblAuthor.getText().endsWith(">>"))
        {
            LblAuthor.setText(LBL_AUTHORS + " <<");                    
            LstAuthors.setVisible(true);
            BtnAuthorDeSelect.setVisible(true);
            BtnAuthorSelectAll.setVisible(true);
            BtnAuthorRefresh.setVisible(true);
            LblAuthorInfo.setVisible(false);
            Rectangle rec=LblDocCreate.getBounds();
            rec.y=rec.y + 65;
            LblDocCreate.setBounds(rec);
            rec=LblDocCFrom.getBounds();
            rec.y=rec.y + 65;
            LblDocCFrom.setBounds(rec);
            rec=TxtCFrom.getBounds();
            rec.y=rec.y + 65;
            TxtCFrom.setBounds(rec);
            rec=LblDocCTo.getBounds();
            rec.y=rec.y + 65;
            LblDocCTo.setBounds(rec);
            rec=TxtCTo.getBounds();
            rec.y=rec.y + 65;
            TxtCTo.setBounds(rec);
            rec=LblDocMod.getBounds();
            rec.y=rec.y + 65;
            LblDocMod.setBounds(rec);
            rec=LblDocMFrom.getBounds();
            rec.y=rec.y + 65;
            LblDocMFrom.setBounds(rec);
            rec=TxtMFrom.getBounds();
            rec.y=rec.y + 65;
            TxtMFrom.setBounds(rec);
            rec=LblDocMTo.getBounds();
            rec.y=rec.y + 65;
            LblDocMTo.setBounds(rec);
            rec=TxtMTo.getBounds();
            rec.y=rec.y + 65;
            TxtMTo.setBounds(rec);
            rec=LblDocCInfo.getBounds();
            rec.y=rec.y + 65;
            LblDocCInfo.setBounds(rec);
            rec=LblDocMInfo.getBounds();
            rec.y=rec.y + 65;
            LblDocMInfo.setBounds(rec);
            rec=getBounds();
            rec.height =rec.height+65;
            setBounds(rec);
            if(LstAuthors.getItemsCount()==0)
                loadAuthorFields();
        }
        else
        {
            LblAuthor.setText(LBL_AUTHORS + " >>");
            LstAuthors.setVisible(false);
            LblAuthorInfo.setVisible(true);
            BtnAuthorDeSelect.setVisible(false);
            BtnAuthorSelectAll.setVisible(false);
            BtnAuthorRefresh.setVisible(false);
            LblAuthorInfo.setText(getInfoText(LBL_CREATOR,LstAuthors.getselectedItemsCount()));
            Rectangle rec=LblDocCreate.getBounds();
            rec.y=rec.y - 65;
            LblDocCreate.setBounds(rec);
            rec=LblDocCFrom.getBounds();
            rec.y=rec.y - 65;
            LblDocCFrom.setBounds(rec);
            rec=TxtCFrom.getBounds();
            rec.y=rec.y - 65;
            TxtCFrom.setBounds(rec);
            rec=LblDocCTo.getBounds();
            rec.y=rec.y - 65;
            LblDocCTo.setBounds(rec);
            rec=TxtCTo.getBounds();
            rec.y=rec.y - 65;
            TxtCTo.setBounds(rec);
            rec=LblDocMod.getBounds();
            rec.y=rec.y - 65;
            LblDocMod.setBounds(rec);
            rec=LblDocMFrom.getBounds();
            rec.y=rec.y - 65;
            LblDocMFrom.setBounds(rec);
            rec=TxtMFrom.getBounds();
            rec.y=rec.y - 65;
            TxtMFrom.setBounds(rec);
            rec=LblDocMTo.getBounds();
            rec.y=rec.y - 65;
            LblDocMTo.setBounds(rec);   
            rec=TxtMTo.getBounds();
            rec.y=rec.y - 65;
            TxtMTo.setBounds(rec);
            rec=LblDocCInfo.getBounds();
            rec.y=rec.y - 65;
            LblDocCInfo.setBounds(rec);
            rec=LblDocMInfo.getBounds();
            rec.y=rec.y - 65;
            LblDocMInfo.setBounds(rec);
            rec=getBounds();
            rec.height =rec.height-65;
            setBounds(rec);
        }
    }
    void LblDocCreate_mouseClicked(java.awt.event.MouseEvent event)
    {
        if(LblDocCreate.getText().endsWith(">>"))
        {
            LblDocCreate.setText(LBL_DOCCREATE + " <<");
            LblDocCTo.setVisible(true);
            LblDocCFrom.setVisible(true);
            TxtCTo.setVisible(true);
            TxtCFrom.setVisible(true);
            LblDocCInfo.setVisible(false);
            Rectangle rec=LblDocMod.getBounds();
            rec.y=rec.y + 40;
            LblDocMod.setBounds(rec);
            rec=LblDocMFrom.getBounds();
            rec.y=rec.y + 40;
            LblDocMFrom.setBounds(rec);
            rec=TxtMFrom.getBounds();
            rec.y=rec.y + 40;
            TxtMFrom.setBounds(rec);
            rec=LblDocMTo.getBounds();
            rec.y=rec.y + 40;
            LblDocMTo.setBounds(rec);
            rec=TxtMTo.getBounds();
            rec.y=rec.y + 40;
            TxtMTo.setBounds(rec);
            rec=LblDocMInfo.getBounds();
            rec.y=rec.y + 40;
            LblDocMInfo.setBounds(rec);
            rec=getBounds();
            rec.height =rec.height+40;
            setBounds(rec);
        }
        else
        {
            LblDocCreate.setText(LBL_DOCCREATE + " >>");
            LblDocCTo.setVisible(false);
            LblDocCFrom.setVisible(false);
            TxtCTo.setVisible(false);
            TxtCFrom.setVisible(false);
            LblDocCInfo.setVisible(true);
            LblDocCInfo.setText(getInfoText((String)TxtCFrom.getText(),
            (String)TxtCTo.getText()));
            Rectangle rec=LblDocMod.getBounds();
            rec.y=rec.y - 40;
            LblDocMod.setBounds(rec);
            rec=LblDocMFrom.getBounds();
            rec.y=rec.y - 40;
            LblDocMFrom.setBounds(rec);
            rec=TxtMFrom.getBounds();
            rec.y=rec.y - 40;
            TxtMFrom.setBounds(rec);
            rec=LblDocMTo.getBounds();
            rec.y=rec.y - 40;
            LblDocMTo.setBounds(rec);   
            rec=TxtMTo.getBounds();
            rec.y=rec.y - 40;
            TxtMTo.setBounds(rec);
            rec=LblDocMInfo.getBounds();
            rec.y=rec.y - 40;
            LblDocMInfo.setBounds(rec);          
            rec=getBounds();
            rec.height =rec.height-40;
            setBounds(rec);
        }
    }
    void LblDocMod_mouseClicked(java.awt.event.MouseEvent event)
    {
        if(LblDocMod.getText().endsWith(">>"))
        {
            LblDocMod.setText(LBL_DOCMOD + " <<");
            LblDocMTo.setVisible(true);
            LblDocMFrom.setVisible(true);
            TxtMTo.setVisible(true);
            TxtMFrom.setVisible(true);
            LblDocMInfo.setVisible(false);
            Rectangle rec=getBounds();
            rec.height =rec.height+40;
            setBounds(rec);
        }
        else
        {
            LblDocMod.setText(LBL_DOCMOD + " >>");
            LblDocMTo.setVisible(false);
            LblDocMFrom.setVisible(false);
            TxtMTo.setVisible(false);
            TxtMFrom.setVisible(false);
            LblDocMInfo.setVisible(true);
            LblDocMInfo.setText(getInfoText((String)TxtMFrom.getText(),
            (String)TxtMTo.getText()));
            Rectangle rec=getBounds();
            rec.height =rec.height-40;
            setBounds(rec);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
	{
            public void actionPerformed(java.awt.event.ActionEvent event)
            {
                Object object = event.getSource();
                if (object == BtnFind)
                    BtnFind_actionPerformed(event);
                else if (object == BtnSave)
                    BtnSave_actionPerformed(event);
                else if (object == BtnReset)
                    BtnReset_actionPerformed(event);
                else if (object == CmbSavedSearch)
                    CmbSavedSearch_actionPerformed(event);
                else if (object == BtnDocTypeIndices)
                    BtnDocTypeIndices_actionPerformed(event);
                else if (object == BtnSaveSearch)
                    BtnSaveSearch_actionPerformed(event);
                else if (object == BtnClose)
                    BtnClose_actionPerformed(event);
                else if (object == BtnDelSearch)
                    BtnDelSearch_actionPerformed(event);
                else if (object == BtnDeSelectDT)
                    BtnDeSelectDT_actionPerformed(event);
                else if (object == BtnAuthorRefresh)
                    BtnAuthorRefresh_actionPerformed(event);
                else if (object == BtnAuthorDeSelect)
                    BtnAuthorDeSelect_actionPerformed(event);
                else if (object == BtnAuthorSelectAll)
                    BtnAuthorSelectAll_actionPerformed(event);
                else if (object == BtnLocation)
                    BtnLocation_actionPerformed(event);
                else if (object == BtnDTRefresh)
                    BtnDTRefresh_actionPerformed(event);
            }
	}
//------------------------------------------------------------------------------
    javax.swing.JPanel VWFind = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    JLabel Pic = new JLabel();
    static VWLinkedButton BtnFind = new VWLinkedButton(0);
    VWLinkedButton BtnSave = new VWLinkedButton(0);
    VWLinkedButton BtnOption = new VWLinkedButton(0);
    VWLinkedButton BtnReset = new VWLinkedButton(0);
    VWLinkedButton BtnHelp= new VWLinkedButton(0);
    static VWLinkedButton BtnClose= new VWLinkedButton(0);
    VWLinkedButton BtnDTRefresh= new VWLinkedButton(VWImages.RefreshIcon);
    VWLinkedButton BtnDeSelectDT= new VWLinkedButton(VWImages.DeselectAllIcon);
    VWLinkedButton BtnDocTypeIndices = new VWLinkedButton(VWImages.DocTypesIcon);
    VWLinkedButton BtnAuthorRefresh= new VWLinkedButton(VWImages.RefreshIcon);
    VWLinkedButton BtnAuthorDeSelect= new VWLinkedButton(VWImages.DeselectAllIcon);
    VWLinkedButton BtnAuthorSelectAll= new VWLinkedButton(VWImages.SelectAllIcon);
    
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    javax.swing.JPanel JPanel2 = new javax.swing.JPanel();
    JLabel JLabel1 = new JLabel();
    javax.swing.JTextField TxtField = new javax.swing.JTextField();
    javax.swing.JTextField TxtChunkSize = new javax.swing.JTextField();
    
    JLabel JLabel2 = new JLabel();
    javax.swing.JTextField TxtContaints = new javax.swing.JTextField();
    javax.swing.JCheckBox ChkVersions = new javax.swing.JCheckBox();
    javax.swing.JCheckBox ChkComment = new javax.swing.JCheckBox();
    static javax.swing.JCheckBox ChkInHitList = new javax.swing.JCheckBox();
    javax.swing.JCheckBox ChkChunkSize = new javax.swing.JCheckBox();
    JLabel LblLocation = new JLabel();
    javax.swing.JTextField TxtLocation = new javax.swing.JTextField();
    VWLinkedButton BtnLocation = new VWLinkedButton(0);
    VWCheckList LstDocType = new VWCheckList();
    JLabel LblDocType = new JLabel();
    VWCheckList LstAuthors = new VWCheckList();
    JLabel LblAuthor = new JLabel();
    JLabel LblDocCreate = new JLabel();
    JLabel LblDocCFrom = new JLabel();
    VWDateComboBox TxtMFrom=new VWDateComboBox();
    VWDateComboBox TxtMTo=new VWDateComboBox();
    JLabel LblDocCTo = new JLabel();
    JLabel LblDocMod = new JLabel();
    JLabel LblDocMFrom = new JLabel();
    VWDateComboBox TxtCFrom=new VWDateComboBox();
    VWDateComboBox TxtCTo=new VWDateComboBox();
    JLabel LblDocMTo = new JLabel();
    JLabel LblLocationInfo = new JLabel();
    JLabel LblDocTypeInfo = new JLabel();
    JLabel LblAuthorInfo = new JLabel();
    JLabel LblDocCInfo = new JLabel();
    JLabel LblDocMInfo = new JLabel();
    VWComboBox CmbSavedSearch = new VWComboBox();
    VWLinkedButton BtnSaveSearch = new VWLinkedButton(0);
    VWLinkedButton BtnDelSearch = new VWLinkedButton(0);
    javax.swing.JTextField TxtSearchName = new javax.swing.JTextField();
    static javax.swing.JCheckBox ChkAppend = new javax.swing.JCheckBox();
    javax.swing.border.EtchedBorder etchedBorder = 
	    new javax.swing.border.EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
//------------------------------------------------------------------------------        
class SymComponent extends java.awt.event.ComponentAdapter
{
        public void componentResized(java.awt.event.ComponentEvent event)
        {
            Object object = event.getSource();
            if (object == VWFind)
                VWFind_componentResized(event);
        }
}
//------------------------------------------------------------------------------
void VWFind_componentResized(java.awt.event.ComponentEvent event)
{
    if(event.getSource()==VWFind)
    {
        Dimension mainDimension=this.getSize();
        Dimension panelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        panelDimension.width=mainDimension.width-commandDimension.width;
        PanelTable.setSize(panelDimension);
        PanelCommand.setSize(commandDimension);
        PanelTable.doLayout();
    }
}
//------------------------------------------------------------------------------       
    public void loadTabData(VWRoom newRoom)
    {
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        VWFindConnector.searchCancel=false;
    }
//--------------------------------------------------------------   
    public void unloadTabData()
    {
        gLoadingSavedSearch=false;
        VWFindConnector.searchCancel=true;
        gCurRoom=0;
        docTypeIndicesDlg = null;
        dlgLocation = null;
        nodeId=0;
        nodePath="";
        curSearchId=0;
        searchDTIndices=null;
        caller=-1;
        search=new Search(0,"");
    }
//--------------------------------------------------------------
    void BtnDTRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        List selectedDT=LstDocType.getSelectedItemIds();
        loadDocTypeFields();
        LstDocType.setCheckItems(selectedDT);
    }  
//--------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
        
    }
//-------------------------------------------------------------- 
    void BtnReset_actionPerformed(java.awt.event.ActionEvent event)
    { 
        clearAction(true);
			/*Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
			Created by: <Pandiya Raj>
			Date: <24 May 2006>
			*/        
        // Clear the document type indices table when clicks on Reset button
        if(docTypeIndicesDlg != null)
        docTypeIndicesDlg.Table.clearData();
        //
        LstDocType.deSelectAllItems();
        LblDocTypeInfo.setText(LBL_NODOCTYPE_COND);
        LstAuthors.deSelectAllItems();
        LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
        TxtField.setText("");
        TxtContaints.setText("");
			/*Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
			Created by: <Pandiya Raj>
			Date: <24 May 2006>
			*/        
        ChkInHitList.setSelected(false);
        ChkAppend.setSelected(false);
        ChkVersions.setSelected(false);
        ChkComment.setSelected(false);
        TxtCFrom.clear();
        TxtCTo.clear();
        LblDocCInfo.setText(LBL_NODATE_COND);
        TxtMFrom.clear();
        TxtMTo.clear();
        LblDocMInfo.setText(LBL_NODATE_COND);
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        TxtLocation.setText(room.getName());
        LblLocationInfo.setText(room.getName());
        nodeId=0;
        TxtField.requestFocus();
    }
//--------------------------------------------------------------
private void loadDocTypeFields()
{
    LstDocType.removeItems();
    Vector data=VWFindConnector.getDocTypes(gCurRoom);
    if (data==null)
    {
       data.add(new DocType(0,LBL_NODATAFOUND));
    }
    LstDocType.loadData(data);
}
//--------------------------------------------------------------
private void loadAuthorFields()
{
    LstAuthors.removeItems();
    Vector data=VWFindConnector.getUsers(gCurRoom,true);
    if (data==null || data.size()==0)
    {
        data=new Vector();
        data.add(new Creator(0,LBL_NOUSERFOUND));
    }
    LstAuthors.loadData(data);
}
//--------------------------------------------------------------
public void loadData(int roomId,String roomName)
{
    if(gCurRoom!=roomId)
    {
        gCurRoom=roomId;
        loadSaveSearchFields();
        loadDocTypeFields();
        loadAuthorFields();
        TxtLocation.setText(roomName);
        LblLocationInfo.setText(roomName);
	nodeId=0;
        enableSearchInContaints=VWDocTypeConnector.getIsSearchICEnable();
        TxtContaints.setEnabled(enableSearchInContaints);
    }
}
//--------------------------------------------------------------
void BtnDocTypeIndices_actionPerformed(java.awt.event.ActionEvent event)
{
    if(docTypeIndicesDlg != null)
        {
            ///docTypeIndicesDlg.setCaller(caller);
            docTypeIndicesDlg.loadTabData(gCurRoom);
			/*Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
			Created by: <Pandiya Raj>
			Date: <24 May 2006>
			*/
            // set the data into the document indices
            Vector vect = docTypeIndicesDlg.Table.getData();
            docTypeIndicesDlg.Table.clearData();
            List selectedDT=LstDocType.getSelectedItems();
			/*Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
			Created by: <Pandiya Raj>
			Date: <24 May 2006>
			*/
            Vector resultVector = new Vector();
            StringBuffer selectedDocType = new StringBuffer();
            if(selectedDT.size()>0){
            for(int index=0;index<selectedDT.size();index++){
            	selectedDocType.append("\t"+selectedDT.get(index)+"\t");
            }
            for(int index=0;index<vect.size();index++){           	
            	String docType=((SearchCond)vect.get(index)).getDocType();
                if(selectedDocType.indexOf("\t"+docType+"\t") != -1){
                	resultVector.add(vect.get(index));
                }    
            }
            }
            deleteUnselectedDTIndices(selectedDT);
            docTypeIndicesDlg.loadDocTypeData(selectedDT);
            docTypeIndicesDlg.Table.insertData(searchDTIndices);
			/*Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
			Created by: <Pandiya Raj>
			Date: <24 May 2006>
			*/
			// To retain the condition to document type indices table.
            docTypeIndicesDlg.Table.insertData(resultVector);           
            docTypeIndicesDlg.setVisible(true);
        }
        else
        {
            docTypeIndicesDlg = new VWDTIndicesDlg(AdminWise.adminFrame);
            ///docTypeIndicesDlg.setCaller(caller);
            docTypeIndicesDlg.loadTabData(gCurRoom);
            docTypeIndicesDlg.Table.clearData();
            List selectedDT=LstDocType.getSelectedItems();
            deleteUnselectedDTIndices(selectedDT);
            docTypeIndicesDlg.loadDocTypeData(selectedDT);
            docTypeIndicesDlg.Table.insertData(searchDTIndices);
            docTypeIndicesDlg.setVisible(true);
        }
        if(docTypeIndicesDlg.cancelFlag) return;
        LstDocType.setCheckItems(docTypeIndicesDlg.LstDocType.getSelectedItems());
        searchDTIndices=docTypeIndicesDlg.Table.getData();
}
//--------------------------------------------------------------
    void CmbSavedSearch_actionPerformed(java.awt.event.ActionEvent event)
	{
    	/*Save button should be enabled   C.Shanmugavalli   2 March 2007*/
    	if (gLoadingSavedSearch) return ;
	        int sel=getSelectedSearch();
	        BtnDelSearch.setEnabled(true);
	        BtnSaveSearch.setEnabled(true);
	        if(sel==CmbSavedSearch.getItemCount()-1)
	        {
	            newSearchAction();
	            return;
	        }
	        if (sel==0) 
	        {
		        BtnSaveSearch.setEnabled(false);
	            BtnDelSearch.setEnabled(false);
	            return;
	        }
	        Search rec=(Search)CmbSavedSearch.getItemAt(sel);
	        loadSearchInfo(rec.getId());
	        BtnDelSearch.setEnabled(true);
	        BtnSaveSearch.setEnabled(true);
	        CmbSavedSearch.setToolTipText(rec.getName());
	}
//-------------------------------------------------------------- 
    private int getSelectedSearch()
    {
        return CmbSavedSearch.getSelectedIndex();
    }
//--------------------------------------------------------------    
    private void loadSearchInfo(int searchId)
    {
        if(curSearchId==searchId) return;
        curSearchId=searchId;
        clearAction(false);
        Search searchInfo=VWFindConnector.getSearchInfo(searchId,gCurRoom);
        if(searchInfo ==null) return;
        nodeId=searchInfo.getNodeId();
        TxtField.setText(searchInfo.getIndex());
        TxtContaints.setText(searchInfo.getContents());
        ChkVersions.setSelected(searchInfo.isPreviousVersions());
        ChkComment.setSelected(searchInfo.isSearchInComment());
        TxtCFrom.setText(searchInfo.getCreationDate1());
        TxtCTo.setText(searchInfo.getCreationDate2());
        TxtMFrom.setText(searchInfo.getModifiedDate1());
        TxtMTo.setText(searchInfo.getModifiedDate2());
        String nodePath=searchInfo.getTreePath();
        nodePath=nodePath.equals(".")?"":nodePath;
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(nodePath!=null && !nodePath.equals("") && !nodePath.equals(".") && nodePath.length()>0)
            nodePath=room.getName()+"\\"+nodePath;
        else
            nodePath=room.getName();
        
        if(LblLocationInfo.isVisible())
            LblLocationInfo.setText(nodePath);
        else
            TxtLocation.setText(nodePath);
        LblAuthorInfo.setText(LBL_NOAUTHOR_COND);
        if(searchInfo.getSearchAuthorCount()>0)
        {
            if(LstAuthors.getItemsCount()==0) loadAuthorFields();
            LstAuthors.setCheckItems(searchInfo.getCreators());
            LblAuthorInfo.setText(getInfoText(LBL_CREATOR,LstAuthors.getselectedItemsCount()));
        }
        else
        {
            LstAuthors.deSelectAllItems();
        }
        LblDocTypeInfo.setText(LBL_NODOCTYPE_COND);
        if(searchInfo.getSearchDTCount()>0)
        {
            if(LstDocType.getItemsCount()==0) loadDocTypeFields();
            LstDocType.setCheckItems(searchInfo.getDocTypes());
            LblDocTypeInfo.setText(getInfoText(LBL_DOCUMENTTYPE,LstDocType.getselectedItemsCount()));
        }
        else
        {
            LstDocType.deSelectAllItems();
        }
        if(searchInfo.getSearchDTCondCount()>0)
            searchDTIndices=searchInfo.getConds();
        
        LblDocCInfo.setText(getInfoText((String)TxtCFrom.getSelectedItem(),
            (String)TxtCTo.getSelectedItem()));
        LblDocMInfo.setText(getInfoText((String)TxtMFrom.getSelectedItem(),
            (String)TxtMTo.getSelectedItem()));
    }
//--------------------------------------------------------------
    void BtnDeSelectDT_actionPerformed(java.awt.event.ActionEvent event)
    {
        LstDocType.deSelectAllItems();
    }
//--------------------------------------------------------------
    void BtnAuthorRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        List selectedAuthors=LstAuthors.getSelectedItemIds();
        loadAuthorFields();
        LstAuthors.setCheckItems(selectedAuthors);
    }
//--------------------------------------------------------------
    void BtnAuthorDeSelect_actionPerformed(java.awt.event.ActionEvent event)
    {
        LstAuthors.deSelectAllItems();
    }
//--------------------------------------------------------------
    void BtnAuthorSelectAll_actionPerformed(java.awt.event.ActionEvent event)
    {
       LstAuthors.setCheckAllItems(0); 
    }
//--------------------------------------------------------------
    void BtnLocation_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(dlgLocation != null)
        {
            dlgLocation.loadDlgData();
            if(nodeId!=dlgLocation.tree.getSelectedNodeId())
                dlgLocation.tree.setSelectionPath(nodeId,true);
            dlgLocation.setVisible(true);
        }
        else
        {
            dlgLocation = new VWDlgLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("FindPanel.dlgLocation"));
            dlgLocation.tree.setSelectionPath(nodeId,true);
            dlgLocation.setVisible(true);
        }
        if(dlgLocation.cancelFlag) return;
        
        TxtLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
	nodeId=dlgLocation.tree.getSelectedNodeId();
    }
//--------------------------------------------------------------
    void BtnDelSearch_actionPerformed(java.awt.event.ActionEvent event)
    {
	if(BtnDelSearch.getText().equals(BTN_CANCEL_NAME))
            clearAction(true);
        else
            delSearchAction();
    }
//--------------------------------------------------------------
     private void delSearchAction()
    {
        int searchId=((Search)CmbSavedSearch.getModel().getSelectedItem()).getId();
        if (searchId==0) return;
        gLoadingSavedSearch=true;
        try{
            VWFindConnector.delSearch(searchId);
            CmbSavedSearch.removeItemAt(CmbSavedSearch.getSelectedIndex());
            BtnReset_actionPerformed(null);
            gLoadingSavedSearch=false;
        }
        catch(Exception e){gLoadingSavedSearch=false;}
    }
//--------------------------------------------------------------
void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
{
    if(VWFindDlg.getCallerID()==CALLER_ARCHIVE)
    {
        AdminWise.adminPanel.archivePanel.findDlg.saveDlgOptions();
        AdminWise.adminPanel.archivePanel.findDlg.setVisible(false);
        AdminWise.adminPanel.archivePanel.closeFindDlg();
    }
    else if(VWFindDlg.getCallerID()==CALLER_BACKUP)
    {
        AdminWise.adminPanel.backupPanel.findDlg.saveDlgOptions();
        AdminWise.adminPanel.backupPanel.findDlg.setVisible(false);
        AdminWise.adminPanel.backupPanel.closeFindDlg();
    }
}
//--------------------------------------------------------------
void BtnSaveSearch_actionPerformed(java.awt.event.ActionEvent event)
{
    //String searchName=TxtSearchName.getText().trim();
	/* When saved search is saved again should not add new item in combo box 
	*	C.Shanmugavalli 		28 Feb 2007	 */
    if (TxtSearchName.isVisible() && TxtSearchName.getText().trim().length() == 0){
		JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("FindPanel.msg_1"),"Find ",JOptionPane.OK_OPTION);
		TxtSearchName.requestFocus(true);
		TxtSearchName.repaint();
		return;
	}
    //if(checkIsNewSearchValid(searchName)>0) return;
    int selectedSearchId = 0;
    if(CmbSavedSearch.isVisible()) {
		//searchName=TxtSearchName.getText().trim();
		/* No need to check for same name. Since in DTC it allows to create search with same name. Valli   27 Feb 2007*/
		//if(checkIsNewSearchValid(searchName)>0) return;
		search=(Search)CmbSavedSearch.getSelectedItem();
		selectedSearchId = search.getId();
		
	}
	else {
		search=new Search(0,TxtSearchName.getText());
		search.setTemp(false);
		search.setNodeId(nodeId);
	}
    int searchId=SaveSearchAction(search);
	if(selectedSearchId==searchId)return;
	if(searchId==0) return;
	int newIndex=CmbSavedSearch.getItemCount()-1;
	CmbSavedSearch.insertItemAt(search,newIndex);
	CmbSavedSearch.setSelectedIndex(CmbSavedSearch.getItemCount()-2);
	BtnDelSearch.setEnabled(true);
	BtnDelSearch.setText(BTN_DEL_NAME);
	TxtSearchName.setVisible(false);
	TxtSearchName.setText("");
	CmbSavedSearch.setVisible(true);
	BtnSaveSearch.setEnabled(true);
    if(AdminWise.getCurrentMode()==APPLET_MODE)
        VWMessage.showMessage(this,VWMessage.MSG_SEARCH_SAVESUSCC);
}
//--------------------------------------------------------------
private int checkIsNewSearchValid(String searchName)
{
    if(searchName.equals("")) 
    {
        VWMessage.showMessage(this,VWMessage.ERR_SEARCH_EMPTYNAME);
        return SEARCH_EMPTYNAME_ERROR;
    }
    for(int i=1;i<CmbSavedSearch.getItemCount()-1;i++)
    {
        if(searchName.equalsIgnoreCase(((Search)CmbSavedSearch.getItemAt(i)).getName()))
        {
            VWMessage.showMessage(this,VWMessage.ERR_SEARCHNAME_ALREADYEXIST);
            return SEARCH_ALREADYEXIST_ERROR;
        }
    }
/*
    int checjCount=0;
    for(int i=0;i<LstDocType.getItemsCount();i++)
    {
        if(LstDocType.getItemIsCheck(i)) checjCount++;
    }
    if (checjCount==0) 
    {    
     VWMessage.showMessage(this,VWMessage.ERR_EMPTY_SEARCHWITHOUTCONDS);
     return EMPTY_SEARCHWITHOUTCONDS_ERROR;
    }
 */
    return NO_ERROR;
}
//--------------------------------------------------------------
private int SaveSearchAction(Search search)
{
    search.setIndex((TxtField.getText()==null?"":TxtField.getText()));
    search.setContents((TxtContaints.getText()==null?"":TxtContaints.getText()));
    search.setPreviousVersions(ChkVersions.isSelected());
    search.setSearchInComment(ChkComment.isSelected());
    
    search.setCreationDate1((TxtCFrom.getText()==null?"":TxtCFrom.getText()));
    search.setCreationDate2((TxtCTo.getText()==null?"":TxtCTo.getText()));
    
    search.setModifiedDate1((TxtMFrom.getText()==null?"":TxtMFrom.getText()));
    search.setModifiedDate2((TxtMTo.getText()==null?"":TxtMTo.getText()));
    
    int count =LstDocType.getItemsCount();
    Vector searcDocTypes=new Vector();
    System.out.println("searcDocTypes Size ->"+searcDocTypes.size());
    for (int i=0;i<count;i++)
    {
        System.out.println("getItemIsCheck ->"+LstDocType.getItemIsCheck(i));
        if(LstDocType.getItemIsCheck(i))
            searcDocTypes.add((DocType)LstDocType.getItem(i));
    }
    System.out.println("searcDocTypes Size ->"+searcDocTypes.size());
    search.setDocTypes(searcDocTypes);
    count =LstAuthors.getItemsCount();
    Vector searchAuthors=new Vector();
    for (int i=0;i<count;i++)
    {
        String tmpStr="";
        if(LstAuthors.getItemIsCheck(i))
            searchAuthors.add((Creator)LstAuthors.getItem(i));
    }
    search.setCreators(searchAuthors);
    ///String[][] searchConds=docTypeIndicesDlg.Table.getData();
    deleteUnselectedDTIndices(LstDocType.getSelectedItemIds());
    if(searchDTIndices!=null)   search.setConds(searchDTIndices);

    System.out.println("searcDocTypes xxx->"+searcDocTypes.size());
    for(int i=0;i<searcDocTypes.size();i++)
        System.out.println("searcDocTypes ->"+((DocType)searcDocTypes.get(i)).getId());
    search.setFindResultWith(1);// In AdminWise Find result with is always "With exact pharase" So it is 1
    VWFindConnector.createSearch(search,gCurRoom);
    
    if (search.getId()==0)
    {
        VWMessage.showMessage(this,VWMessage.ERR_SEARCH_NOTSAVE);
        return 0;
    }
    return search.getId();
    }
//--------------------------------------------------------------
private void newSearchAction()
{
    BtnDelSearch.setEnabled(true);
    BtnDelSearch.setText(BTN_CANCEL_NAME);
    CmbSavedSearch.setVisible(false);
    TxtSearchName.setVisible(true);
    BtnSaveSearch.setEnabled(true);
    TxtSearchName.setText("");
}
//--------------------------------------------------------------
void BtnFind_actionPerformed(java.awt.event.ActionEvent event)
{
    if(BtnFind.getText()==BTN_STOP_NAME)
    {
        VWFindConnector.searchCancel=true;
    }
    else
    {
    		//Desc :ReInitialised search object, for better search results.
    		search = new Search(0,"");    		
            search.setTemp(true);
            search.setNodeId(nodeId);
             search.setId(0);
            int searchId=SaveSearchAction(search);
            if (searchId==0) return;
            ///search.setRowNo(VWUtil.to_Number(TxtChunkSize.getText()));
            search.setRowNo(AdminWise.adminPanel.searchHits);
            search.setHitListId(ChkInHitList.isSelected()?1:0);
            BtnFind.setText(BTN_STOP_NAME);
            BtnClose.setEnabled(false);
            BtnFind.setIcon(VWImages.DisconnectIcon);
            VWFindConnector findCon=new VWFindConnector();
            findCon.setChunkSize(VWUtil.to_Number(TxtChunkSize.getText()));
            findCon.callFind(gCurRoom,search,ChkAppend.isSelected());
            if(VWFindDlg.getCallerID()==CALLER_ARCHIVE)
                AdminWise.adminPanel.archivePanel.findDlg.saveDlgOptions();
            else if(VWFindDlg.getCallerID()==CALLER_BACKUP)
                AdminWise.adminPanel.backupPanel.findDlg.saveDlgOptions();
    }
}
//--------------------------------------------------------------
   private void clearAction(boolean withCmbSearch)
   {
        LstDocType.deSelectAllItems();
		/*Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
		Created by: <Pandiya Raj>
		Date: <24 May 2006>
		*/
        // Clear the document type indices table when clicks on Clear button
        if(docTypeIndicesDlg != null)
        docTypeIndicesDlg.Table.clearData();
        searchDTIndices=null;
		TxtSearchName.setText("");
        if (CmbSavedSearch.getItemCount()>0 && withCmbSearch)
        {
            CmbSavedSearch.setSelectedIndex(0);
            curSearchId=0;
        }
        BtnSaveSearch.setEnabled(false);
        BtnDelSearch.setEnabled(false);
        BtnDelSearch.setText(BTN_DEL_NAME);
        CmbSavedSearch.setVisible(true);
        TxtSearchName.setVisible(false);
   }
//--------------------------------------------------------------
private void loadSaveSearchFields()
{
    try{
        gLoadingSavedSearch=true;
        CmbSavedSearch.removeAllItems();
        CmbSavedSearch.addItem(SEARCH_CAPTION);
        List data=VWFindConnector.getSearchLists();
        if (data!=null)
            for (int i=0;i<data.size();i++)
                CmbSavedSearch.addItem(data.get(i));
        CmbSavedSearch.addItem(NEW_NAME);
    }
    catch (Exception e){}
    finally
    {
        gLoadingSavedSearch=false;   
    }
    CmbSavedSearch_actionPerformed(null);
}
//--------------------------------------------------------------
private String getInfoText(String info,int count)
{
    if(count==0) return "No " + info + " selected";
    if(count==1) return "1 " + info + " selected";
    return "" + count + " " + info + "s" + " selected";
}
//--------------------------------------------------------------
private String getInfoText(String frameDate,String toDate)
{
    if(frameDate==null || frameDate.equals("")) frameDate=null;
    if(toDate==null || toDate.equals("")) toDate=null;
    if(frameDate==null && toDate ==null)
        return LBL_NODATE_COND;
    if(frameDate!=null && toDate ==null)
        return "From  [" + frameDate + "]";
    if(frameDate==null && toDate!=null)
        return "To  [" + toDate + "]";
     return "From  [" + frameDate + "]  To  [" + toDate + "]";
}
//--------------------------------------------------------------
public void setFindPath(int searchNodeId,String searchNodePath)
{
    nodeId=searchNodeId;
    nodePath=searchNodePath;
    if(searchNodeId==0)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        searchNodePath=room.getName();
    }
    if(LblLocationInfo.isVisible())
        LblLocationInfo.setText(searchNodePath);
    else
        TxtLocation.setText(searchNodePath);
}
//--------------------------------------------------------------
private void deleteUnselectedDTIndices(List selectedDT)
{
    if(selectedDT==null || selectedDT.size()==0 || searchDTIndices==null)
    {
        searchDTIndices=null;
        return;
    }
    int tableCount=searchDTIndices.size();
    int listCount=selectedDT.size();
    Vector newSearchDTIndices=new Vector();
    for(int i=0;i<tableCount;i++)
    {
        
        int docTypeId=((SearchCond)searchDTIndices.get(i)).getDocTypeId();
        if(selectedDT.contains(String.valueOf(docTypeId)))
            newSearchDTIndices.add(searchDTIndices.get(i));
    }
    searchDTIndices=newSearchDTIndices;
}
//--------------------------------------------------------------
public void setAppendEnabled(boolean appendEnabled)
{
    ChkAppend.setEnabled(appendEnabled);
}
//--------------------------------------------------------------
public void setCaller(int caller)
{
    this.caller=caller;
}
//--------------------------------------------------------------
public static void setFindMode(boolean moreResult)
{
    ChkAppend.setEnabled(moreResult);
    ChkInHitList.setEnabled(moreResult);
    BtnFind.setText(BTN_FINDNOW_NAME);
    BtnFind.setIcon(VWImages.FindIcon);
    BtnClose.setEnabled(moreResult);
}
//--------------------------------------------------------------
private boolean gLoadingSavedSearch=false;
private int gCurRoom=0;
public VWDTIndicesDlg docTypeIndicesDlg = null;
public VWDlgLocation dlgLocation = null;
private int nodeId=0;
private String nodePath="";
private int curSearchId=0;
private Vector searchDTIndices=null;
private boolean enableSearchInContaints=false;
private int caller=-1;
private Search search=new Search(0,"");
//--------------------------------------------------------------
}