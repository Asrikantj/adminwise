/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWadminPanel<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWFind;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ViewWise.AdminWise.VWTree.VWTree;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Panel;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import ViewWise.AdminWise.VWConstant;
import java.awt.Dimension;
import java.awt.Frame;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;
import java.awt.Insets;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import java.util.prefs.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWTable.VWAdvanceSearchTable;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import ViewWise.AdminWise.VWUtil.VWUtil;
import com.computhink.common.DocType;
import ViewWise.AdminWise.VWUtil.*;

public class VWDTIndicesDlg extends javax.swing.JDialog implements VWConstant
{   
    public VWDTIndicesDlg(Frame parent/*,int caller*/)
    {
        super(parent,LBL_DOCTYPEINDICES_NAME_1,true);
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.FindIcon.getImage());
        getContentPane().setLayout(new BorderLayout());
        setVisible(false);

        dtIndicesPanel.setLayout(new BorderLayout());
        VWAdvanceSearch.setLayout(null);
        this.setResizable(false);
        dtIndicesPanel.add(VWAdvanceSearch,BorderLayout.CENTER);
        VWAdvanceSearch.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWAdvanceSearch.add(PanelCommand);
        ///PanelCommand.setBackground(new java.awt.Color(0,128,192));
        PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,12,152,132);///(12,12,144,132)
        BtnOk.setText(BTN_OK_NAME);
        BtnOk.setActionCommand(BTN_OK_NAME);
        PanelCommand.add(BtnOk);
//        BtnOk.setBackground(java.awt.Color.white);
        ///BtnOk.setBorder(etchedBorder); 
        BtnOk.setBounds(12,215,144, AdminWise.gBtnHeight);
        BtnOk.setIcon(VWImages.OkIcon);
        BtnClose.setText(BTN_CLOSE_NAME);
        BtnClose.setActionCommand(BTN_CLOSE_NAME);
        PanelCommand.add(BtnClose);
//        BtnClose.setBackground(java.awt.Color.white);
        BtnClose.setBounds(12,320,144, AdminWise.gBtnHeight);
        BtnClose.setIcon(VWImages.CloseIcon);
        BtnClose.setIcon(VWImages.CloseIcon);
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        PanelCommand.add(BtnRefresh);
//        BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,250,144, AdminWise.gBtnHeight);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        BtnClear.setText(BTN_CLEAR_NAME);
        BtnClear.setActionCommand(BTN_CLEAR_NAME);
        PanelCommand.add(BtnClear);
//        BtnClear.setBackground(java.awt.Color.white);
        ///BtnClear.setBorder(etchedBorder); 
        BtnClear.setBounds(12,285,144, AdminWise.gBtnHeight);
        BtnClear.setIcon(VWImages.ClearIcon);
        BtnClear.setIcon(VWImages.ClearIcon);
        /*
        BtnSimpleSearch.setText(BTN_SIMPLE_NAME);
        BtnSimpleSearch.setActionCommand(BTN_SIMPLE_NAME);
        PanelCommand.add(BtnSimpleSearch);
        BtnSimpleSearch.setBackground(java.awt.Color.white);
        ///BtnSimpleSearch.setBorder(etchedBorder); 
        BtnSimpleSearch.setBounds(12,308,144,24);
         **/
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        PanelTable.setLayout(gridbag);
        VWAdvanceSearch.add(PanelTable);
        PanelTable.setBounds(170,0,460,700);
        JLabel1.setText(LBL_DOCTYPE_NAME);
        JLabel1.setLabelFor(LstDocType);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        /*
        Issue No / Purpose:  <0427200601 - In Adminwise Search Panel,  resolved the issues related to Document Type Indices>
		Created by: <Pandiya Raj.M>
		Date: <24 May 2006>
		*/        
        // To align the Table view in the Document Type Indices change the gridbagconstraint
        c.weighty = 0.0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(JLabel1,c);
        PanelTable.add(JLabel1);
        c.gridwidth = 1;
        c.gridheight = 2;
        c.weighty = 1.0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(LstDocType,c);
        ///LstDocType.setSource(caller);
        PanelTable.add(LstDocType);
        JLabel2.setText(LBL_DOCTYPEINDICES_NAME);
        JLabel2.setLabelFor(SPTable);
        c.weighty = 0.0;
        // To align the Table view in the Document Type Indices change the gridbagconstraint
        c.gridheight = 2;
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(JLabel2,c);
        PanelTable.add(JLabel2);
        c.gridwidth = 1;
        c.gridheight = 2;
        // To align the Table view in the Document Type Indices change the gridbagconstraint
        c.weighty = 2.0;        
        c.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(SPTable,c);
        PanelTable.add(SPTable);

        SymComponent aSymComponent = new SymComponent();
        VWAdvanceSearch.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.FindImage);

        SymAction lSymAction = new SymAction();
        BtnOk.addActionListener(lSymAction);
        BtnClose.addActionListener(lSymAction);
        BtnClear.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        Table.getParent().setBackground(java.awt.Color.white);
        getContentPane().add(dtIndicesPanel);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        LstDocType.addVWEventListener(new VWCheckListListener() {
            public void VWItemChecked(VWItemCheckEvent event)
            {
                addSearchDocTypeIndices(event.getObject());
            }
            public void VWItemUnchecked(VWItemCheckEvent event)
            {
                delSearchDocTypeIndices(event.getObject());
            }
        });
        getDlgOptions();
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object instanceof VWFindDlg)
                Dlg_WindowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dlg_WindowClosing(java.awt.event.WindowEvent event)
    {
        saveDlgOptions();
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnOk)
                BtnOk_actionPerformed(event);
            else if (object == BtnClose)
                BtnClose_actionPerformed(event);
            else if (object == BtnClear)
                BtnClear_actionPerformed(event);
            else if (object == BtnRefresh)
                BtnRefresh_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    public void addNotify()
    {
        Dimension size = getSize();
        super.addNotify();
        if (frameSizeAdjusted) return;
        frameSizeAdjusted = true;
        Insets insets = getInsets();
        setSize(insets.left + insets.right + size.width, insets.top + insets.bottom + size.height);
    }
//------------------------------------------------------------------------------
    public void setVisible(boolean b)
    {
        if(b)
        {
            doLayout();
            PanelTable.doLayout();
        }
        super.setVisible(b);
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
        prefs.putInt("width", this.getSize().width);
        prefs.putInt("height", this.getSize().height);
    }
//------------------------------------------------------------------------------
    public void loadTabData(int newRoomid)
    {
        if (gCurRoom!=newRoomid)
        {
            gCurRoom=newRoomid;
            loadDocTypeFields(true);
        }
    }
//------------------------------------------------------------------------------
    public void loadDocTypeData(List list) {
        loadDocTypeFields(false);
        LstDocType.deSelectAllItems();
        if(list==null) return;
        int count=list.size();
        for(int i=0;i<count;i++) {
            boolean found=false;
            Object item=list.get(i);
            int docTypeId=0;
            String docTypeName="";
            if(item instanceof String) {
                docTypeId=VWUtil.to_Number((String)list.get(i));
            }
            else if(item instanceof DocType) {
                docTypeId=((DocType)list.get(i)).getId();
                docTypeName=((DocType)list.get(i)).getName();
            }
            found=false;
            for(int j=0;j<Table.getRowCount();j++) {
                if(Table.getRowTypeId(j)==docTypeId) {
                    found=true;
                    break;
                }
            }
            if(!found) {
                addSearchDocTypeIndices(docTypeId,docTypeName);
                LstDocType.setItemCheck(0,true,docTypeId);
            }
            else {
                LstDocType.setItemCheck(0,true,docTypeId);
            }
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        setSize(prefs.getInt("width",630),prefs.getInt("height",564));
    }
//------------------------------------------------------------------------------
    JPanel dtIndicesPanel=new JPanel();
    javax.swing.JPanel VWAdvanceSearch = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnOk = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
    VWLinkedButton BtnClear = new VWLinkedButton(0,true);
    VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
    public VWCheckList LstDocType = new VWCheckList();
    javax.swing.JCheckBox ChkAppend = new javax.swing.JCheckBox();
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    public VWAdvanceSearchTable Table = new VWAdvanceSearchTable();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane(Table);
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.border.EtchedBorder etchedBorder = 
        new javax.swing.border.EtchedBorder
        (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
//------------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter
    {
        public void componentResized(java.awt.event.ComponentEvent event)
        {
            Object object = event.getSource();
            if (object == VWAdvanceSearch)
                VWAdvanceSearch_componentResized(event);
        }
    }
//------------------------------------------------------------------------------
    void VWAdvanceSearch_componentResized(java.awt.event.ComponentEvent event)
    {
        if(event.getSource()==VWAdvanceSearch)
        {
           Dimension mainDimension=this.getSize();
           Dimension PanelDimension=mainDimension;
           Dimension commandDimension=PanelCommand.getSize();
           commandDimension.height=mainDimension.height;
           PanelDimension.width=mainDimension.width-commandDimension.width;
           PanelDimension.height=mainDimension.height;
           PanelCommand.setSize(commandDimension);
           PanelTable.setSize(PanelDimension);
           getParent().repaint();
        }
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        Table.clearData();
        loadDocTypeFields(true);
    }
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        cancelFlag=false;
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        cancelFlag=true;
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnClear_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        clearAction();
    }
//------------------------------------------------------------------------------
   private void clearAction()
   {
        ///LstDocType.deSelectAllItems();
        Table.clearUserData();    
   }
//------------------------------------------------------------------------------
   public void clearRoomData()
   {
        LstDocType.removeItems();
        Table.clearUserData();
   }
//------------------------------------------------------------------------------
public void loadDocTypeFields(boolean force)
{
    if(!force && LstDocType.getItemsCount()>0) return;
    LstDocType.removeItems();
    Vector data=VWFindConnector.getDocTypes(gCurRoom);
    if (data==null) data.add(new DocType(0,LBL_NODATAFOUND));
    LstDocType.loadData(data);
}
//------------------------------------------------------------------------------
private void setGridData(int docTypeId)
{
    Vector data =new Vector();
    Table.insertData(data);
}
//------------------------------------------------------------------------------
public void addSearchDocTypeIndices(Object docTypeRec)
{
    if(docTypeRec==null) return;
    DocType record=(DocType)docTypeRec;
    addSearchDocTypeIndices(record.getId(),record.getName());
}
//------------------------------------------------------------------------------
public void addSearchDocTypeIndices(int docTypeId,String docTypeName)
{
    if(docTypeId==0) return;
    DocType docType=VWFindConnector.getSearchDocTypeIndices(docTypeId,gCurRoom);
    docType.setName(docTypeName);
    if(docType!=null) Table.insertData(docType);
}
//------------------------------------------------------------------------------
public void delSearchDocTypeIndices(Object docTypeRec)
{
    if(docTypeRec==null) return;
    delSearchDocTypeIndices(((DocType)docTypeRec).getId());
}
//------------------------------------------------------------------------------
public void delSearchDocTypeIndices(int docTypeId)
{
    if(docTypeId==0) return;
    Table.delData(docTypeId);
}
//------------------------------------------------------------------------------
    public static int gCurRoom=0;
    private static boolean gLoadingSavedSearch=false;
    public boolean cancelFlag=true;
    boolean frameSizeAdjusted = false;
//------------------------------------------------------------------------------
}