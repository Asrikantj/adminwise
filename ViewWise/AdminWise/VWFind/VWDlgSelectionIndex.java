/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWFind;

import java.awt.*;
import javax.swing.*;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWConstant;

public class VWDlgSelectionIndex extends javax.swing.JDialog implements VWConstant
{
	public VWDlgSelectionIndex(Frame parent,int indexId) 
	{
            super(parent);
            ///parent.setIconImage(VWImages.AddIcon.getImage());
            getContentPane().setBackground(AdminWise.getAWColor());
            setTitle(LBL_FIND_NAME);
            setModal(true);
            getContentPane().setLayout(null);
            setSize(283,438);
            setVisible(false);
            JLabel1.setText(LBL_SELECTIONINDEX_NAME);
            getContentPane().add(JLabel1);
            JLabel1.setBounds(6,4,194, AdminWise.gBtnHeight);
            getContentPane().add(LstSeValues);
            LstSeValues.setBounds(4,28,276,366);
            BtnOk.setText(BTN_OK_NAME);
            getContentPane().add(BtnOk);
            BtnOk.setBounds(6,410,82,20);
//            BtnOk.setBackground(java.awt.Color.white);
            BtnCancel.setText(BTN_CANCEL_NAME);
            getContentPane().add(BtnCancel);
            BtnCancel.setBounds(194,410,82,20);
//            BtnCancel.setBackground(java.awt.Color.white);
            LstSeValues.loadData(loadSelectionValue(indexId));
            SymAction lSymAction = new SymAction();
            BtnOk.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            SymKey aSymKey = new SymKey();
            addKeyListener(aSymKey);
            LstSeValues.addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            this.addWindowListener(aSymWindow);
            setResizable(false);
            getDlgOptions();
            setVisible(true);
            //}}
	}
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosed(java.awt.event.WindowEvent event)
        {
                Object object = event.getSource();
                if (object == VWDlgSelectionIndex.this)
                        Dialog_windowClosed(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosed(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnOk)
                        BtnOk_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
	public void setVisible(boolean b)
	{
		if (b) 
                {
                    Dimension d =VWUtil.getScreenSize();
                    setLocation(d.width/4,d.height/4);
                }
		super.setVisible(b);
	}
//------------------------------------------------------------------------------
	public void addNotify()
	{
		// Record the size of the window prior to calling parents addNotify.
		Dimension size = getSize();

		super.addNotify();

		if (frameSizeAdjusted)
			return;
		frameSizeAdjusted = true;

		// Adjust size of frame according to the insets
		Insets insets = getInsets();
		setSize(insets.left + insets.right + size.width, insets.top + insets.bottom + size.height);
	}
//------------------------------------------------------------------------------
	boolean frameSizeAdjusted = false;
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	VWCheckList LstSeValues = new VWCheckList();
	VWLinkedButton BtnOk = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    public List getValues()
    {
        List selIndices=new LinkedList();
        int count=LstSeValues.getItemsCount();
        for(int i=0;i<count;i++)
            if(LstSeValues.getItemIsCheck(i))
                selIndices.add(LstSeValues.getItem(i));
        return selIndices;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private Vector loadSelectionValue(int indexId)
    {
        Vector selValues=VWDocTypeConnector.getSelectionValues(indexId);
        if(selValues==null || selValues.size()==0) return null;
        return selValues;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
}