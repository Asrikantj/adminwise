package ViewWise.AdminWise.VWDocType;

import java.awt.event.ActionEvent;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTriggerIndexTable;

import com.computhink.common.Index;

public class VWTriggerIndexDialog extends JDialog{
	
	private static final long serialVersionUID = -2867345153655551491L;
	VWExternalDBLookup dbLookupDlg;
    VWTriggerIndexTable triggerIndexTable;
    
    JPanel triggerPanel;
    JScrollPane SPtriggerIndex;
    JButton btnOk; 
    JButton btnCancel;
    Hashtable triggerTableData = new Hashtable();
	
    public VWTriggerIndexDialog(VWExternalDBLookup dbLookupDlg){
    	super();
    	try{
    		this.dbLookupDlg = dbLookupDlg;
    		//setSize(250, 150);
    		setBounds(380, 350, 300, 205);

    		setTitle(AdminWise.connectorManager.getString("TriggerIndexDlg.title"));
    		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    		getContentPane().setBackground(AdminWise.getAWColor());  
    		getContentPane().setLayout(null);

    		triggerPanel = new JPanel();
    		triggerPanel.setLayout(null);
    		/*triggerPanel.setMinimumSize(new Dimension(200,150));
    		triggerPanel.setPreferredSize(new Dimension(250,150));
    		*/
    		triggerPanel.setBounds(10, 10, 280, 160);
    		triggerPanel.setBackground(AdminWise.getAWColor());
    		//triggerPanel.setBorder(new LineBorder(Color.red));

    		triggerIndexTable = new VWTriggerIndexTable(this);
    		SPtriggerIndex = new JScrollPane(triggerIndexTable);
    		SPtriggerIndex.setBounds(0, 0, 275, 123);
    		triggerPanel.add(SPtriggerIndex);

    		btnOk = new JButton(AdminWise.connectorManager.getString("general.ok"));
    		btnOk.setBounds(55, 135, 80, 20);
    		triggerPanel.add(btnOk);

    		btnCancel = new JButton(AdminWise.connectorManager.getString("general.cancel"));
    		btnCancel.setBounds(145, 135, 80, 20);
    		triggerPanel.add(btnCancel);

    		getContentPane().add(triggerPanel);

    		SymAction symAction = new SymAction();
    		btnOk.addActionListener(symAction);
    		btnCancel.addActionListener(symAction);

    		initTriggerData();
    		System.out.println("1");

    		setResizable(false);
    		System.out.println("2");
    		setModal(true);
    		System.out.println("3");
    		setVisible(true);
    		System.out.println("--------------------- VWTriggerIndexDialog() End---------------------");
    	}catch(Exception e){
    		System.out.println("Exception e : "+e.getMessage());
    	}
    }

    private void initTriggerData() {
    	try{
    		System.out.println("initTriggerData()");
    		Vector data = new Vector();
    		Vector mappedIndices = dbLookupDlg.getMappedIndices();

    		if(mappedIndices == null || mappedIndices.size() <= 0)
    			return;

    		for(int i=0; i<mappedIndices.size(); i++){
    			String tempStr = "";
    			Index idx = (Index) mappedIndices.get(i);
    			if(idx.isTriggerEnabled()){
    				//tempStr = idx.getName() + ",";
    				data.add(idx);
    			}				
    		}
    		triggerIndexTable.addData(data);
    		System.out.println("initTriggerData() End");
    	}catch(Exception e){
    		System.out.println("Exception in in initTriggerData() :: "+e.getMessage());
    	}
    }

	class SymAction implements java.awt.event.ActionListener{

		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			
			if(obj == btnOk){
				btnOk_actionPerformed(e);
			}
			else if(obj == btnCancel){
				btnCancel_actionPerformed(e);
			}
			
		}
		
	}
	public void btnOk_actionPerformed(ActionEvent e) {
		try{

			Vector triggerData = triggerIndexTable.getData();			
			System.out.println("triggerData ::: "+triggerData);

			if(triggerData == null || triggerData.size() <= 0)
				return;
			
			for(int i=0; i<triggerData.size(); i++){
				int tIndexId;
				String tIndexValue = "";
				

				Index tIdx = (Index)triggerData.get(i);
				tIndexId = tIdx.getId();
				tIndexValue = tIdx.getValue();
				
				if(tIndexValue == null || tIndexValue.trim().length() == 0){
					JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("TriggerIndexDlg.msg_1"));
					return;
				}

				if (tIdx.getTriggerDataType() == 1){
					double result = Double.NaN;
					try{
						result = Double.parseDouble(tIndexValue);
					}catch(NumberFormatException nfex){
						JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("TriggerIndexDlg.msg_2")+tIdx.getName()+">");
						return;
					}
				}
				triggerTableData.put(tIndexId, tIndexValue);				
			}
			closeDialog();
		}catch(Exception ex){
			System.out.println("Exception while getting Trigger Data : "+ex.getMessage());
		}
	}

	public void btnCancel_actionPerformed(ActionEvent e) {
		closeDialog();		
	}

	private void closeDialog() {
		setVisible(false);
		dbLookupDlg.requestFocus();
		dbLookupDlg.toFront();
		dispose();
	}

	public Hashtable getTriggerTableData() {
		return triggerTableData;		
	}
	
	public static void main(String args){
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
			JFrame frame = new JFrame();
			VWTriggerIndexDialog dlg = new VWTriggerIndexDialog(null);
			dlg.setVisible(true);
		}catch(Exception e){
			
		}
	}
}