/*
                      Copyright (c) 1997-2001
                         Computhink Software
 *
                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.4 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWDocType;

import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import java.awt.*; 


import ViewWise.AdminWise.VWTable.*;

import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;

import ViewWise.AdminWise.AdminWise;
//------------------------------------------------------------------------------
public class VWDTIndexTable extends JTable implements VWConstant
{
    protected VWDTIndexTableData m_data;    
    private int indexType=-1;    
    public VWDTIndexTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWDTIndexTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k=0;k<VWDTIndexTableData.m_columns.length;k++) {
            TableCellRenderer renderer;
            ColoredTableCellRenderer textRenderer=new ColoredTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWDTIndexTableData.m_columns[k].m_alignment);
            renderer = textRenderer;
            VWDTIndexRowEditor editor=new VWDTIndexRowEditor(this);
            TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWDTIndexTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        this.
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	 addMouseListener(aSymMouse);
	 setRowHeight(TableRowHeight);
	 
	 setTableResizable();
	 
	 KeyboardFocusManager.getCurrentKeyboardFocusManager()  
	 .addKeyEventDispatcher(new KeyEventDispatcher(){  
		 public boolean dispatchKeyEvent(KeyEvent ke){  
			 //AdminWise.printToConsole("VWDTIndexTable : KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() :: "+KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner());
			 if(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof VWDTIndexTable) {
				 if(ke.getID()==KeyEvent.KEY_PRESSED)  
				 {  
					 int key = ke.getKeyCode();  
					 ke.consume();  
				 }  
			 }
			 return false;
		 }}); 
 }
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        java.awt.Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);
        VWIndexRec row = (VWIndexRec) m_data.m_vector.elementAt(rowIndex);
        if(row.getId() > 0)
            tip = " " + row.getId();
        else
        	tip = "-";
        return tip;
    }
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWDTIndexTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWDTIndexTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWDTIndexTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWDTIndexTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(VWIndexRec[] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public VWIndexRec[] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(VWIndexRec[] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(data[i]);
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
 public void insertRowData(VWIndexRec index)
  {
        m_data.insert(index);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(List data)
  {
    for(int i=0;i<data.size();i++)
    {
        m_data.insert((VWIndexRec)data.get(i));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public VWIndexRec getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public VWIndexRec getRowData()
  {
        return getRowData(getSelectedRow());
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public int getIndexType()
  {
      return indexType;
  }
//------------------------------------------------------------------------------
  public void setIndexType(int type)
  {
      indexType=type;
  }
//------------------------------------------------------------------------------
  public void setRowUp(int row)
  {
      m_data.setRowUp(row);
      this.setRowSelectionInterval(row-1,row-1);
  }
//------------------------------------------------------------------------------
  public void setRowDown(int row)
  {
      m_data.setRowDown(row);
      this.setRowSelectionInterval(row+1,row+1);
  }
//------------------------------------------------------------------------------
  public void resetKey(int row)
  {
      if(m_data.resetKey(row))
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------  
}
class DTIndexColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public DTIndexColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWDTIndexTableData extends AbstractTableModel 
{
  public static final DTIndexColumnData m_columns[] = {
    new DTIndexColumnData( AdminWise.connectorManager.getString("DTIndexTable.DTIndexColumnData_0"),0.4F,JLabel.LEFT ),
    new DTIndexColumnData( AdminWise.connectorManager.getString("DTIndexTable.DTIndexColumnData_1"),0.1F, JLabel.LEFT),
    new DTIndexColumnData( AdminWise.connectorManager.getString("DTIndexTable.DTIndexColumnData_2"),0.05F, JLabel.LEFT),
    new DTIndexColumnData( AdminWise.connectorManager.getString("DTIndexTable.DTIndexColumnData_3"),0.45F, JLabel.LEFT),
  };
  public static final int COL_NAME = 0;
  public static final int COL_TYPE = 1;
  public static final int COL_REQUIRED = 2;
  public static final int COL_DESC = 3;
  protected VWDTIndexTable m_parent;
  protected Vector m_vector;
  
  public VWDTIndexTableData(VWDTIndexTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
        m_vector.addElement((VWIndexRec)data.get(i));
  }
//------------------------------------------------------------------------------
public void setData(VWIndexRec[] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
        m_vector.addElement(data[i]);
  }
//------------------------------------------------------------------------------
public VWIndexRec[] getData() {
    int count=getRowCount();
    VWIndexRec[] data=new VWIndexRec[count];
    for(int i=0;i<count;i++)
        data[i]=(VWIndexRec)m_vector.elementAt(i);
    return data;
  }
//------------------------------------------------------------------------------
  public VWIndexRec getRowData(int rowNum) {
        if(rowNum<0) return null;
        return (VWIndexRec)m_vector.elementAt(rowNum);
  }
//--------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//--------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//--------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//--------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//--------------------------------------------------------------
  public java.lang.Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    VWIndexRec row = (VWIndexRec)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_NAME:
          if(row.getId()<0)
                return new ColorData(row.getName(),ColorData.GREEN);
          else if(row.getIndexUsed()>1)
                return new ColorData(row.getName(),ColorData.RED);
          return row.getName();
      case COL_TYPE: return row.getTextType();
      case COL_REQUIRED: 
          if(row.getKey()) 
              return VWConstant.VWIndexFieldType[0];
          else
              return row.getRequired()?VWConstant.VWIndexFieldType[1]:
                    VWConstant.VWIndexFieldType[2];
      case COL_DESC: return row.getDescription();
    }
    return "";
  }
//--------------------------------------------------------------
  /*Method modified to make the reset key field enable for update key field mode
	 * Release :Build5-001
	 *Modified By Madhavan
	 */
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    VWIndexRec row = (VWIndexRec)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_NAME:
        row.setName(svalue); 
        break;
      case COL_TYPE:
          int curType = row.getType();
          row.setTextType(svalue);
          if(row.getType()!= curType)
          {
              row.resetSubInfo();
              AdminWise.adminPanel.docTypePanel.loadIndexProperties(row,true);
          }
        break;
      case COL_REQUIRED:
	// 	VWMessage.showMessage(null,"Inside COL_Required",VWMessage.DIALOG_TYPE);
      	//Condition is if dttype is used excecute first block
      	// Fix for Issue 585 && 586
       boolean dtUsed= AdminWise.adminPanel.docTypePanel.getSelDTIndexUsed(nRow);
      	if(dtUsed){
      	//	VWMessage.showMessage(null,"DTUsed",VWMessage.DIALOG_TYPE);
        row.setRequired(svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[0])||
        svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[1]));
	  }else{
      	//	VWMessage.showMessage(null,"DTNotUsed",VWMessage.DIALOG_TYPE);
        row.setKey(svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[0]));
	    row.setRequired(svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[0])||
    	        svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[1]));
       if(row.getKey())AdminWise.adminPanel.docTypePanel.IndicesTable.resetKey(nRow);
      	} 
      	
      	if(dtUsed && VWDocTypePanel.curMode==VWConstant.MODE_UPDATE_KEY ){
      	 row.setKey(svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[0]));
      	 row.setRequired(svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[0])||
      	        svalue.equalsIgnoreCase(VWConstant.VWIndexFieldType[1]));
         if(row.getKey())AdminWise.adminPanel.docTypePanel.IndicesTable.resetKey(nRow);
         
        } 
        break;
      case COL_DESC:
        row.setDescription(svalue); 
        break;
    }
  }
//--------------------------------------------------------------
  public void clear()
    {
        m_vector.removeAllElements();
    }
//--------------------------------------------------------------
    public void insert(VWIndexRec IndexRowData) 
    {
        m_vector.addElement(IndexRowData);
    }
//--------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
    public void setRowUp(int rowNum) {
        if(rowNum==0) return;
        if(rowNum>m_vector.size()-1) return;
        VWIndexRec preRow=(VWIndexRec)m_vector.elementAt(rowNum -1);
        VWIndexRec curRow=(VWIndexRec)m_vector.elementAt(rowNum);
        m_vector.setElementAt(curRow,rowNum -1);
        m_vector.setElementAt(preRow,rowNum);
  }
//------------------------------------------------------------------------------
    public void setRowDown(int rowNum) {
        if(rowNum==m_vector.size()-1) return;
        VWIndexRec nextRow=(VWIndexRec)m_vector.elementAt(rowNum +1);
        VWIndexRec curRow=(VWIndexRec)m_vector.elementAt(rowNum);
        m_vector.setElementAt(curRow,rowNum +1);
        m_vector.setElementAt(nextRow,rowNum);
  }
//------------------------------------------------------------------------------
  public boolean resetKey(int row)
  {
      if(m_vector.size()==1) return false;
        for(int i=0;i<m_vector.size();i++)
        {
            VWIndexRec index = (VWIndexRec)m_vector.elementAt(i);
            if(index.getKey() && i!=row)
            {
                index.setKey(false);
                break;
            }
        }
      return true;
  }
//------------------------------------------------------------------------------
}