/*
 * Created on Jul 6, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ViewWise.AdminWise.VWDocType;

import java.awt.Frame;
import java.util.prefs.Preferences;

import javax.swing.border.TitledBorder;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTree.VWTree;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import java.util.StringTokenizer;


/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VWDlgSaveLocation extends javax.swing.JDialog implements  VWConstant{
	
	public VWDlgSaveLocation(Frame parent,String caption)
	{
            super(parent,caption,true);
            getContentPane().setBackground(AdminWise.getAWColor());
            ///parent.setIconImage(VWImages.FindIcon.getImage());
            setSize(360,430);
            getContentPane().setLayout(null);
            treePanel.setBounds(6,6,342,342);
            treePanel.setBorder(titledBorder1);
            getContentPane().add(treePanel);
            treePanel.setBackground(AdminWise.getAWColor());
            BtnOk.setText(BTN_OK_NAME);
            BtnOk.setActionCommand(BTN_OK_NAME);
//            BtnOk.setBackground(java.awt.Color.white);
            getContentPane().add(BtnOk);
            BtnOk.setIcon(VWImages.OkIcon);
            
            BtnRefresh.setText(BTN_REFRESH_NAME);
            BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
//            BtnRefresh.setBackground(java.awt.Color.white);
            BtnRefresh.setIcon(VWImages.RefreshIcon);
            getContentPane().add(BtnRefresh);
            
            BtnClose.setText(BTN_CLOSE_NAME);
            BtnClose.setActionCommand(BTN_CLOSE_NAME);
//            BtnClose.setBackground(java.awt.Color.white);
            BtnClose.setIcon(VWImages.CloseIcon);
            getContentPane().add(BtnClose);
            
            BtnRefresh.setBounds(8,360,82, AdminWise.gBtnHeight);
            BtnOk.setBounds(197,360,72, AdminWise.gBtnHeight);            
            BtnClose.setBounds(272,360,72, AdminWise.gBtnHeight);
            
            SymAction lSymAction = new SymAction();
            BtnRefresh.addActionListener(lSymAction);
            BtnOk.addActionListener(lSymAction);
            BtnClose.addActionListener(lSymAction);
            setResizable(false);
            loadDlgData();
            SymKey aSymKey =new SymKey();
            tree.addKeyListener(aSymKey);
            BtnOk.addKeyListener(aSymKey);
            BtnClose.addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            getDlgOptions();
	}
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgSaveLocation.this)
                BtnClose_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnClose_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnOk)
                        BtnOk_actionPerformed(event);			
                else if (object == BtnRefresh)
                        BtnRefresh_actionPerformed(event);			
                else if (object == BtnClose)
                        BtnClose_actionPerformed(event);			
        }
    }
//------------------------------------------------------------------------------
    //{{DECLARE_CONTROLS
    public VWTree tree = new VWTree();
    javax.swing.JScrollPane treePanel = new javax.swing.JScrollPane(tree);
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    TitledBorder titledBorder1 = new TitledBorder(LBL_LOCATION_NAME);
    VWLinkedButton BtnOk =  new VWLinkedButton(0,true);
    VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {   
    	
    	if(isValidFolder()){
    	        saveDlgOptions();
    	        cancelFlag=false;
    	        setVisible(false);
    	        dispose();
    	}
    }
//------------------------------------------------------------------------------
    private boolean isValidFolder()
    {
    	String nodePath = tree.getSelectedNodeStrPath();
    	int firstIndex = nodePath.indexOf("\\");
    	if(firstIndex == -1){
    		javax.swing.JOptionPane.showMessageDialog(this,AdminWise.connectorManager.getString("dlgSaveLocation.msg1")+" ","",javax.swing.JOptionPane.OK_OPTION);
    		return false;
    	}else{ 
    		StringTokenizer st = new StringTokenizer(nodePath, "\\");
    		if(st.countTokens() >= 4){
    			return true;
    		}else{
    			javax.swing.JOptionPane.showMessageDialog(this,AdminWise.connectorManager.getString("dlgSaveLocation.msg1")+" ","",javax.swing.JOptionPane.OK_OPTION);
    			return false;
    		}
    	}
    }
    
//------------------------------------------------------------------------------
    
    public void loadDlgData()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect /*&& gCurRoom!=room.getId()*/)
        {
            gCurRoom=room.getId();
            treeLoaded=false;
            VWTreeConnector.setTree(tree);
            tree.loadTreeData();
        }
}
//------------------------------------------------------------------------------     
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        VWTreeConnector.setTree(tree);
        tree.loadTreeData();
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x",this.getX());
            prefs.putInt( "y",this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
    private boolean treeLoaded=false;
    private static int gCurRoom=0;
    public boolean cancelFlag=true;
    public static void main(String[] args) {
	Frame frame = new Frame(); 
	VWDlgSaveLocation location = new VWDlgSaveLocation(frame, "Test");
	location.setVisible(true);
    }
}
