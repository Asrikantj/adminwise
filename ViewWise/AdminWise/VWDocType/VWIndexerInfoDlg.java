package ViewWise.AdminWise.VWDocType;

import javax.swing.JPanel;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import java.util.prefs.*;
import java.awt.Component;
///import com.cct.util.SafeThread;
import java.util.List;
import java.util.LinkedList;
//------------------------------------------------------------------------------
public class VWIndexerInfoDlg extends javax.swing.JDialog implements VWConstant
{
    public VWIndexerInfoDlg(Frame parent,List docIdList)
    {
            super(parent,LBL_IndexerInfo,false);
            getContentPane().setBackground(AdminWise.getAWColor());
            ///parent.setIconImage(VWImages.DocTypesIcon.getImage());
            //getContentPane().setLayout(null);
            setSize(335,220);
            infoPanel.setLayout(null);
            getContentPane().add(infoPanel);
            BtnCancel.setText(BTN_CANCEL_NAME);
//            BtnCancel.setBackground(java.awt.Color.white);
            infoPanel.add(BtnCancel);
            BtnCancel.setBounds(120,144,91,23);
            infoPanel.add(PrgrsMain);
            PrgrsMain.setBounds(12,108,303,19);
            infoPanel.add(PrgrsDoc);
            PrgrsDoc.setBounds(12,48,303,19);
            infoPanel.add(LblMain);
            LblMain.setBounds(12,84,303,15);
            infoPanel.add(LblDoc);
            LblDoc.setBounds(12,24,303,15);
            PrgrsMain.setMaximum(docIdList.size());
            PrgrsMain.setMinimum(0);
            PrgrsDoc.setMaximum(100);
            PrgrsDoc.setMinimum(0);
            LblMain.setText(AdminWise.connectorManager.getString("IndexerInfoDlg.LblMain")+" "+docIdList.size()+"]");
            LblDoc.setText(LBL_ReadDocInfo);
            SymAction lSymAction = new SymAction();
            BtnCancel.addActionListener(lSymAction);
            getDlgOptions();
            setResizable(false);            
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            setVisible(true);
            new IndexerThread(docIdList);
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWIndexerInfoDlg.this)
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
	//{{DECLARE_CONTROLS
        javax.swing.JPanel infoPanel = new javax.swing.JPanel();
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
	javax.swing.JProgressBar PrgrsMain = new javax.swing.JProgressBar();
	javax.swing.JProgressBar PrgrsDoc = new javax.swing.JProgressBar();
	javax.swing.JLabel LblMain = new javax.swing.JLabel();
	javax.swing.JLabel LblDoc = new javax.swing.JLabel();
	//}}
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelAction=true;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x2", this.getX());
        prefs.putInt("y2", this.getY());
        prefs.putInt("width2", this.getSize().width);
        prefs.putInt("height2", this.getSize().height);
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x2",50),prefs.getInt("y2",50));
        setSize(prefs.getInt("width2",335),prefs.getInt("height2",210));
    }
//------------------------------------------------------------------------------
    public void setMainText(String text)
    {
        LblMain.setText(text);
    }
//------------------------------------------------------------------------------
    public void setDocText(String text)
    {
        LblDoc.setText(text);
    }
//------------------------------------------------------------------------------
 private class IndexerThread extends Thread/*SafeThread*/
    {
        IndexerThread(List docIdList)
        {
            this.docIdList=docIdList;
            setDaemon(true);
            ///setPriority(java.lang.Thread.MAX_PRIORITY);
            start();
        }
        public void run()
        {
            try{
                AdminWise.adminPanel.setWaitPointer();
                int count=docIdList.size();
                String nodeId="";
                for(int i=0;i<count;i++)
                {
                    nodeId=(String)docIdList.get(i);
                    setMainText(AdminWise.connectorManager.getString("IndexerInfoDlg.setMailTxt_0")+(i+1)+AdminWise.connectorManager.getString("IndexerInfoDlg.setMailTxt_1")+count+"]");
                    if(cancelAction) 
                        break;
                    try{
                        VWDocTypeConnector.addDocumentToIndexer(nodeId,true);
                    }
                        catch(Exception e){}
                    PrgrsMain.setValue(i+1);
                }
            }
            catch(Exception e){}
            finally{
                AdminWise.adminPanel.setDefaultPointer();            
                ///stop();
                saveDlgOptions();
                setVisible(false);
            }
        }
        List docIdList=new LinkedList();
 }
//------------------------------------------------------------------------------
    public boolean cancelAction=false;
}