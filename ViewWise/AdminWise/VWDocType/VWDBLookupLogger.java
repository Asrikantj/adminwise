package ViewWise.AdminWise.VWDocType;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.Util;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;

public class VWDBLookupLogger{

    /**
     * Default Serial Version ID
     */
    private static final long serialVersionUID = 1L;

    private JDialog loggerDialog = new JDialog(); 
    Vector LogMsg = new Vector();
    Vector mappedIndices = new Vector();
    DocType docType = null;
    String triggerValue = "";
    private boolean isDBError = false;
    public VWDBLookupLogger(JDialog parent, String title, Vector logMessage, DocType docType,  Vector mappedIndices)
    {
	loggerDialog =  new JDialog(parent,title,true);
	loggerDialog.getContentPane().setBackground(AdminWise.getAWColor());	
	loggerDialog.setTitle(title);
	loggerDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	loggerDialog.setModal(true);
	loggerDialog.getContentPane().setLayout(null);	        
	this.docType = docType;
	this.LogMsg = logMessage;
	this.mappedIndices = mappedIndices;
	initComponents();	
	clearMessage();	
	printMessage();
	if (docType != null){
	    getData();
	}
	loggerDialog.setResizable(false);
	loggerDialog.setVisible(true);	
    }
    void getData(){
	Vector outputLog = new Vector();
	VWExternalLookupConnector.getExternalData(docType, outputLog);
	isDBError = false;
	for (int i = 0; i < outputLog.size(); i++){
	    writeLog(outputLog.get(i).toString());
	}
/*	if (isDBError){
	    return;
	}*/
	Vector indices = docType.getIndices();	
	writeLog(Util.getNow(2) + " : Total Indices count " + indices.size());
	writeLog(Util.getNow(2) + " : External DB lookup data");
	writeLog(Util.getNow(2) + " : ------------------------------------------------------------------------------------------" );
	writeLog(Util.getNow(2) + " : Index Name\t\t- Value from external DB" );
	writeLog(Util.getNow(2) + " : ------------------------------------------------------------------------------------------" );
	int len = 35;
	for (int i = 0; i < indices.size(); i++){
	    Index idx = (Index)indices.get(i);
	    Index mappedIdx = null;
	    try{
		for (int j= 0; j < mappedIndices.size(); j++){
		    mappedIdx = (Index) mappedIndices.get(j);
		    if (mappedIdx.getId() == idx.getId()){
			break;
		    }		
		}		
	    }catch(Exception ex){
		
	    }	    
	    int nameLength = idx.getName().trim().length();
	    if (/*idx.getType() != Index.IDX_SELECTION &&*/ 
	    		idx.getType() != Index.IDX_SEQUENCE && 
	    		idx.getType() != Index.IDX_BOOLEAN &&
	    		(!idx.isReadOnly() ||(idx.isReadOnly() && idx.getType() == Index.IDX_SELECTION )) && (mappedIdx != null && mappedIdx.getExternalColumn().trim().length() > 0)){
	    	String printIndexName = idx.getName().trim();
	    	if(nameLength<len){
	    		int appendLength = len-nameLength;
	    		for(int l = 0; l<appendLength; l++){
	    			printIndexName = printIndexName+" ";
	    		}
	    	}
	    	if (idx.getType() == Index.IDX_DATE && idx.getInfo().trim().endsWith("1")) continue;
	    	writeLog(Util.getNow(2) + " : " + printIndexName+" "+" "+ "\t- " + idx.getDefaultValue().trim());
	    }
	}
	writeLog(Util.getNow(2) + " :  ------------------------------------------------------------------------------------------" );	
    }
    private void initComponents() 
    {
	new  VWImages();
	btCopy = new JButton(AdminWise.connectorManager.getString("general.copy"));
	btSave = new JButton(AdminWise.connectorManager.getString("general.save"));
	btClose = new JButton(AdminWise.connectorManager.getString("general.close"));

	tpDBLookupLogger = new MyJTextPane();

	//lDBLookupLogger = new JLabel("DB Lookup Logger:");
	spDBLookup = new JScrollPane(tpDBLookupLogger);
	spDBLookup.setWheelScrollingEnabled(true);	
	loggerDialog.getContentPane().add(btCopy);
	btCopy.setBounds(378, 290, 73, 26);
	btCopy.setEnabled(true);
	btCopy.setIcon(VWImages.CopyIcon);

	loggerDialog.getContentPane().add(btClose);
	btClose.setBounds(455, 290, 73, 26);
	btClose.setIcon(VWImages.CloseIcon);

	spDBLookup.setBorder(new TitledBorder(AdminWise.connectorManager.getString("dbLookup.title")));
	//tpDBLookupLogger.setBackground(new Color(255, 255, 204))
	spDBLookup.getViewport().setBackground(Color.white); ;
	//tpDBLookupLogger.setForeground(Color.blue);
	tpDBLookupLogger.setEditable(false);
	tpDBLookupLogger.setDocument(new DefaultStyledDocument());
	tpDBLookupLogger.setFont(new Font("Arial", Font.PLAIN, 11));	
	//tpDBMessages.setFont(new Font("Tahoma", 1, 12));
	tpDBLookupLogger.setAutoscrolls(true);	
	loggerDialog.getContentPane().add(spDBLookup);
	spDBLookup.setBounds(12, 10, 520, 270);
	spDBLookup.setViewportView(tpDBLookupLogger);
	spDBLookup.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	loggerDialog.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent evt) {
		closeDialog();
	    }
	});
	btClose.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		closeDialog();
	    }
	});
	btCopy.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		copyDBLogger();
	    }
	});
	btSave.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		saveLogger();
	    }
	});    

	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	loggerDialog.setSize(new Dimension(550, 355));
	loggerDialog.setLocation((screenSize.width-550)/2,(screenSize.height-380)/2);
    }
    void addMessage(String msg){
	tpDBLookupLogger.setDocument(new DefaultStyledDocument());
	tpDBLookupLogger.setText(tpDBLookupLogger.getText() + (tpDBLookupLogger.getText().trim().length() == 0?"":"\n") + msg);
	tpDBLookupLogger.repaint();

    }
    void printMessage(){
	for (int i = 0; i < LogMsg.size(); i++){
	    String msg = LogMsg.get(i).toString();
	   /* addMessage(msg);*/
	    writeLog(msg);
	}
    }
    void writeLog(String msg)
    {
	SimpleAttributeSet sas = new SimpleAttributeSet();	
	Document doc = tpDBLookupLogger.getDocument();
	try
	{
	    msg += "\n";
	    if (msg.toLowerCase().indexOf("exception") >= 0 || 
		    msg.toLowerCase().indexOf("error") >= 0 ||
		    msg.toLowerCase().indexOf("sqlexception") >= 0 ){		
	        StyleConstants.setForeground(sas, Color.red);
	        isDBError = true;
	    }else{
	        StyleConstants.setForeground(sas, Color.blue);
	    }
	    doc.insertString(doc.getLength(), msg ,sas);	    
	    tpDBLookupLogger.repaint();
	    tpDBLookupLogger.setCaretPosition(tpDBLookupLogger.getDocument().getLength());
	    //tpDBLookupLogger.setCaretPosition(tpDBLookupLogger.getDocument().getLength());
	}
	catch (Exception e){}
    }

    void clearMessage(){	
	tpDBLookupLogger.setText("");
	tpDBLookupLogger.repaint();

    }
    private void closeDialog() 
    {
	loggerDialog.setVisible(false);
	loggerDialog.dispose();
    }

    private void copyDBLogger() 
    {
	tpDBLookupLogger.selectAll();
	tpDBLookupLogger.copy();
    }

    private void saveLogger() 
    {
    }

    //private JLabel lDBLookupLogger;
    private JScrollPane spDBLookup;
    private JButton btCopy;
    private JButton btSave;
    private JButton btClose;
    private MyJTextPane tpDBLookupLogger;
// Override with the getScrollableTracksViewportWidth method horizontal scroll bar is available and default wrap
// functionality blocked    
    class MyJTextPane extends JTextPane {
	MyJTextPane(){
	    super();
	}
	public boolean getScrollableTracksViewportWidth() {
	    // TODO Auto-generated method stub    
	    return false;
	}
    }
    public static void main(String[] args) {
	try{
		String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
		UIManager.setLookAndFeel(plasticLookandFeel);
	}catch(Exception ex){}

	JDialog frame = new JDialog();
	Vector data = new Vector();
	data.add("Error Test Message   ");
	data.add("Normal Test Message");
	data.add("Exception Test Message");
	data.add("Test Message");
	//new VWDBLookupLogger(frame, " Error Test" , data, null, "", new Vector());
    }
}
