/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWRecord<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWDocType;

import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.util.List;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWUtil;
import com.computhink.common.DocType;

public class VWDocTypeRec
{
    public VWDocTypeRec(String recordString)
    {
        this(recordString,0);
    }
    public VWDocTypeRec(String recordString,int type)
    {
        if(type==0)
        {
            VWStringTokenizer tokens = new VWStringTokenizer(recordString,
                VWConstant.SepChar,false);
            id = VWUtil.to_Number(tokens.nextToken());
            name=tokens.nextToken();
            used=!tokens.nextToken().trim().equals("0");
            try{
                enableSIC=tokens.nextToken().trim().equals("1");
            }catch(Exception e){enableSIC=false;}
            try{
                autoMail_To=tokens.nextToken().trim();
            }catch(Exception e){autoMail_To="";}
            try{
                autoMail_Cc=tokens.nextToken().trim();
            }catch(Exception e){autoMail_Cc="";}
            try{
                autoMail_Subject=tokens.nextToken().trim();
            }catch(Exception e){autoMail_Subject="";}
        }
        else if(type==1)
        {
            VWStringTokenizer tokens = new VWStringTokenizer(recordString,
                VWConstant.SepChar,false);
            id = VWUtil.to_Number(tokens.nextToken());
            name=tokens.nextToken();
            keyName=tokens.nextToken();
            numOfIndices=tokens.nextToken();
            try{
                enableSIC=tokens.nextToken().trim().equals("1");
            }catch(Exception e){enableSIC=false;}
            try{
                autoMail_To=tokens.nextToken().trim();
            }catch(Exception e){autoMail_To="";}
            try{
                autoMail_Cc=tokens.nextToken().trim();
            }catch(Exception e){autoMail_Cc="";}
            try{
                autoMail_Subject=tokens.nextToken().trim();
            }catch(Exception e){autoMail_Subject="";}
        }
}
//------------------------------------------------------------------------------
    public VWDocTypeRec(DocType docType)
    {
        id=docType.getId();
        name=docType.getName();
        used=docType.getUsed();
        autoMail_To=docType.getTo();
        autoMail_Cc=docType.getCc();
        autoMail_Subject=docType.getSubject();
        enableSIC=docType.getEnableTextSearch();
    }
//------------------------------------------------------------------------------
    public VWDocTypeRec(int recId,String recName,boolean recUsed,String recAutoMail_To,
    String recAutoMail_Cc,String recAutoMail_Subject,boolean recEnableSIC)
    {
        id=recId;
        name=recName;
        used=recUsed;
        autoMail_To=recAutoMail_To;
        autoMail_Cc=recAutoMail_Cc;
        autoMail_Subject=recAutoMail_Subject;
        enableSIC=recEnableSIC;
    }
//------------------------------------------------------------------------------
    public VWDocTypeRec(int recId,String recName,boolean recEnableSIC)
    {
        this(recId,recName,false,"","","",recEnableSIC);
    }
//------------------------------------------------------------------------------
    public VWDocTypeRec(String recName,String recAutoMail_To,String recAutoMail_Cc,
        String recAutoMail_Subject,boolean recEnableSIC,String recIndexCount,
        String recKeyName)
    {
        name=recName;
        autoMail_To=recAutoMail_To;
        autoMail_Cc=recAutoMail_Cc;
        autoMail_Subject=recAutoMail_Subject;
        enableSIC=recEnableSIC;
        numOfIndices=recIndexCount;
        keyName=recKeyName;
    }
//------------------------------------------------------------------------------
    public int getId()
    {
        return id;
    }
//------------------------------------------------------------------------------
    public void setId(int recId)
    {
        id=recId;
    }
//------------------------------------------------------------------------------
    public String getName()
    {
        return name;
    }
//------------------------------------------------------------------------------
    public void setName(String recName)
    {
        name=recName;
    }
//------------------------------------------------------------------------------
    public String getKeyName()
    {
        return keyName;
    }
//------------------------------------------------------------------------------
    public void setKeyName(String recKeyName)
    {
        keyName=recKeyName;
    }
//------------------------------------------------------------------------------
    public String getNumOfInices()
    {
        return numOfIndices;
    }
//------------------------------------------------------------------------------
    public void setNumOfInices(String recNOIName)
    {
        numOfIndices=recNOIName;
    }

//------------------------------------------------------------------------------
    public String getAutoMail_To()
    {
        return autoMail_To;
    }
//------------------------------------------------------------------------------
    public void setAutoMail_To(String recAutoMail_To)
    {
        autoMail_To=recAutoMail_To;
    }
//------------------------------------------------------------------------------
    public String getAutoMail_Cc()
    {
        return autoMail_Cc;
    }
//------------------------------------------------------------------------------
    public void setAutoMail_Cc(String recAutoMail_Cc)
    {
        autoMail_Cc=recAutoMail_Cc;
    }
//------------------------------------------------------------------------------
    public String getAutoMail_Subject()
    {
        return autoMail_Subject;
    }
//------------------------------------------------------------------------------
    public void setAutoMail_Subject(String recAutoMail_Subject)
    {
        autoMail_Subject=recAutoMail_Subject;
    }
//------------------------------------------------------------------------------
    public boolean getUsed()
    {
        return used;
    }
//------------------------------------------------------------------------------
    public void setUsed(boolean recUsed)
    {
        used=recUsed;
    }
//------------------------------------------------------------------------------
    public boolean getEnableSIC()
    {
        return enableSIC;
    }
//------------------------------------------------------------------------------
    public void setEnableSIC(boolean recEnableSIC)
    {
        enableSIC=recEnableSIC;
    }
//------------------------------------------------------------------------------
    public List getDocTypeIndices()
    {
        return indicesList;
    }
//------------------------------------------------------------------------------
    public void setDocTypeIndices(List indices)
    {
        indicesList=indices;
    }
//------------------------------------------------------------------------------
    public String toString()
    {
        return this.name;
    }
//------------------------------------------------------------------------------    
    private int id=0;
    private String name="";
    private String autoMail_To="";
    private String autoMail_Cc="";
    private String autoMail_Subject="";
    private boolean used=false;
    private boolean enableSIC=false;
    private String numOfIndices="0";
    private String keyName="";
    public String VREnable="0";
    public String VRInitVersion="0";
    public String VRInitRevision="0";
    public String VRPagesChange="0";
    public String VRIncPagesChange="0";
    public String VRPageChange="0";
    public String VRIncPageChange="0";
    public String VRPropertiesChange="0";
    public String VRIncPropertiesChange="0";
    
    private List indicesList =new LinkedList();
}