
/*
		A basic implementation of the JDialog class.
*/
/*
 * Created on Jun 13, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

package ViewWise.AdminWise.VWDocType;

/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;
import javax.swing.InputVerifier;
import javax.swing.JComponent;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWRecord;

import com.computhink.common.DocType;


public class VWDlgRetentionSettings extends javax.swing.JDialog implements
    VWConstant
{
    public VWDlgRetentionSettings(Frame parent,int docTypeId)
    {
            super(parent);
            getContentPane().setBackground(AdminWise.getAWColor());
            vwDocTypeRec = null;
            //JOptionPane.showMessageDialog(null,"docTypeId: "+docTypeId,"",JOptionPane.OK_OPTION);
            vwDocTypeRec = VWDocTypeConnector.getDocTypeInfo(docTypeId);
            setTitle(LBL_RETENTIONSETTING_NAME+" for \""+vwDocTypeRec.getName()+"\"");
            setModal(true);
            getContentPane().setLayout(null);
            setSize(430,345);
            setVisible(false);
            JPanel1.setLayout(null);
            JPanel1.setBorder(titledBorder1);
            getContentPane().add(JPanel1);
            JPanel1.setBounds(4,38,422,260);
            JLabel1.setText(LBL_DOCEXPAFT);
            JPanel1.add(JLabel1);
            JLabel1.setBounds(26,20,120,20);

            JPanel1.add(TxtDays);
            TxtDays.setBounds(152,20,70,20);
            TxtDays.setName(AdminWise.connectorManager.getString("dlgretentionsetting.name"));
            TxtDays.setInputVerifier(new JTextVerifier());
            TxtDays.addFocusListener(txtListener);


            JLabel2.setText(LBL_DAYS);
            JPanel1.add(JLabel2);
            JLabel2.setBounds(240,20,88,20);

            JLabel3.setText(LBL_DOCEXPSTARTS);
            JPanel1.add(JLabel3);
            JLabel3.setBounds(26,55,354,20);

            JPanel1.add(CmbExpStarts);
            CmbExpStarts.setBounds(26,80,310,20);

            JLabel4.setText(LBL_DISPOSALACTIONS);
            JPanel1.add(JLabel4);
            JLabel4.setBounds(26,115,380,28);

            JPanel1.add(CmbDisposalActions);
            CmbDisposalActions.setBounds(26,145,380,20);

            JLabel5.setText(LBL_MOVETO);
            JPanel1.add(JLabel5);
            JLabel5.setBounds(26,180,100,28);
            JLabel5.setVisible(false);

            JPanel1.add(TxtMovePath);
            TxtMovePath.setBounds(26,205,310,20);
            TxtMovePath.setVisible(false);

            JPanel1.add(BtnLocation);
            BtnLocation.setBounds(345,205,28, AdminWise.gBtnHeight);
//            BtnLocation.setBackground(java.awt.Color.white);
            BtnLocation.setVisible(false);
            BtnLocation.setEnabled(true);
            BtnLocation.setText("...");

            JPanel1.add(BtnBrowse);
            BtnBrowse.setBounds(345,205,60, AdminWise.gBtnHeight);
//            BtnBrowse.setBackground(java.awt.Color.white);
            BtnBrowse.setVisible(false);
            BtnBrowse.setEnabled(true);
            BtnBrowse.setText(AdminWise.connectorManager.getString("general.browse"));


            ChkEnable.setText(LBL_RETENTION_ENABLE);
            ChkEnable.setActionCommand(LBL_RETENTION_ENABLE);
            getContentPane().add(ChkEnable);
            ChkEnable.setBounds(4,8,392, AdminWise.gBtnHeight);
            BtnSave.setText(BTN_SAVE_NAME);
            getContentPane().add(BtnSave);
            BtnSave.setBounds(281,310,72, AdminWise.gBtnHeight);
//            BtnSave.setBackground(java.awt.Color.white);
            BtnSave.setIcon(VWImages.OkIcon);

            BtnCancel.setText(BTN_CANCEL_NAME);
            getContentPane().add(BtnCancel);
            BtnCancel.setBounds(353,310,72, AdminWise.gBtnHeight);
//            BtnCancel.setBackground(java.awt.Color.white);
            BtnCancel.setIcon(VWImages.CloseIcon);

            BtnRefresh.setText(BTN_REFRESH_NAME);
            getContentPane().add(BtnRefresh);
            BtnRefresh.setBounds(6,310,72, AdminWise.gBtnHeight);
//            BtnRefresh.setBackground(java.awt.Color.white);
            BtnRefresh.setIcon(VWImages.RefreshIcon);

            SymAction lSymAction = new SymAction();
            BtnSave.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            BtnRefresh.addActionListener(lSymAction);
            BtnLocation.addActionListener(lSymAction);
            ChkEnable.addActionListener(lSymAction);
            CmbDisposalActions.addActionListener(lSymAction);
            BtnBrowse.addActionListener(lSymAction);
            TxtDays.setInputVerifier(jTextVerifier);
            TxtDays.addKeyListener(jTextVerifier);
            TxtDays.setToolTipText(AdminWise.connectorManager.getString("dlgretentionsetting.tooltip1"));

            SymKey aSymKey = new SymKey();
            addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            setResizable(false);
            loadRetentionSettings(docTypeId,false);

            getDlgOptions();
            setVisible(true);
    }
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
                Object object = event.getSource();
                if (object == VWDlgRetentionSettings.this)
                        Dialog_windowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnSave_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnSave)
                        BtnSave_actionPerformed(event);
                else if (object == BtnCancel)
                    BtnCancel_actionPerformed(event);
                else if (object == ChkEnable)
                    ChkEnable_actionPerformed(event);
                else if (object == ChkEnable)
                    ChkEnable_actionPerformed(event);
                else if (object == CmbExpStarts)
                	CmbExpStarts_actionPerformed(event);
                else if (object == CmbDisposalActions)
                	CmbDisposalActions_actionPerformed(event);
                else if (object == BtnBrowse)
                    BtnBrowse_actionPerformed(event);
                else if (object == BtnLocation)
                    BtnLocation_actionPerformed(event);
                else if (object == BtnRefresh)
                	BtnRefresh_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
	public void addNotify()
	{
            Dimension size = getSize();
            super.addNotify();
            if (frameSizeAdjusted) return;
            frameSizeAdjusted = true;
            Insets insets = getInsets();
            setSize(insets.left+insets.right+size.width, insets.top+
                insets.bottom+size.height);
	}
//------------------------------------------------------------------------------
		boolean frameSizeAdjusted = false;
        javax.swing.JPanel JPanel1 = new javax.swing.JPanel();
        javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
        javax.swing.JTextField TxtDays = new javax.swing.JTextField();
        javax.swing.JTextField TxtMovePath = new javax.swing.JTextField();

        javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
        javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
        javax.swing.JLabel JLabel4 = new javax.swing.JLabel();
        javax.swing.JLabel JLabel5 = new javax.swing.JLabel();

        VWComboBox CmbExpStarts = new VWComboBox();
        VWComboBox CmbDisposalActions = new VWComboBox();

        javax.swing.JCheckBox ChkEnable = new javax.swing.JCheckBox();
        VWLinkedButton BtnSave = new VWLinkedButton(0,true);
        VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
        VWLinkedButton BtnLocation = new VWLinkedButton(0,true);
        VWLinkedButton BtnBrowse = new VWLinkedButton(0,true);
        VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);

        TitledBorder titledBorder1 = new TitledBorder(LBL_SETTING);
        TextFieldListener txtListener = new TextFieldListener();
        //Max input is 9 digit
        JTextVerifier jTextVerifier = new JTextVerifier(9);
//------------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(saveRetentionSettings()){
        	saveDlgOptions();
    	    this.setVisible(false);
    	}
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        loadRetentionSettings(curDocTypeId,true);
    }
//------------------------------------------------------------------------------
    void ChkEnable_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkEnable.isSelected())
        {
            JPanel1.setEnabled(true);
            JLabel1.setEnabled(true);
            JLabel2.setEnabled(true);
            TxtDays.setEnabled(true);
            TxtDays.setText("");
            JLabel3.setEnabled(true);
            JLabel4.setEnabled(true);
            JLabel5.setEnabled(true);
            TxtMovePath.setEnabled(true);
            CmbExpStarts.setEnabled(true);
            CmbDisposalActions.setEnabled(true);
            CmbExpStarts.removeAllItems();
    		CmbDisposalActions.removeAllItems();
    		ARSExpFields = getDateDocTypeFields(curDocTypeId);
    		CmbExpStarts.addItems(ARSExpFields);
            CmbDisposalActions.addItems(ARSActions);

        }
        else
        {
            JPanel1.setEnabled(false);
            JLabel1.setEnabled(false);
            JLabel2.setEnabled(false);
            TxtDays.setText("");
            TxtDays.setEnabled(false);
            JLabel3.setEnabled(false);
            JLabel4.setEnabled(false);
            JLabel5.setEnabled(false);
            TxtMovePath.setVisible(false);

            CmbExpStarts.removeAllItems();
    		CmbDisposalActions.removeAllItems();
    		ARSExpFields = getDateDocTypeFields(curDocTypeId);
    		CmbExpStarts.addItems(ARSExpFields);
            CmbDisposalActions.addItems(ARSActions);
            CmbExpStarts.setEnabled(false);
            CmbDisposalActions.setEnabled(false);

            BtnLocation.setEnabled(false);
            BtnLocation.setVisible(false);
            BtnBrowse.setEnabled(false);
            BtnBrowse.setVisible(false);

        }
    }
    /*
     * Expiration period drop down
     */
    void CmbExpStarts_actionPerformed(java.awt.event.ActionEvent event){

    }
    /*
     * Disposal method drow down actions
     */
    void CmbDisposalActions_actionPerformed(java.awt.event.ActionEvent event){

    	VWRecord selObject=(VWRecord)CmbDisposalActions.getSelectedItem();

        if(selObject != null && selObject.getId() == 1 ){
        	BtnLocation.setVisible(true);
        	BtnLocation.setEnabled(true);
	       	BtnBrowse.setVisible(false);
        	BtnBrowse.setEnabled(false);
        	JLabel5.setVisible(true);
        	JLabel5.setText(LBL_MOVETO);
        	TxtMovePath.setVisible(true);
        	TxtMovePath.setText("");
        	TxtMovePath.setEnabled(false);
        }else if(selObject != null && selObject.getId() == 2 ){
        	BtnLocation.setVisible(false);
        	BtnLocation.setEnabled(false);
        	BtnBrowse.setVisible(true);
        	BtnBrowse.setEnabled(true);
        	JLabel5.setVisible(true);
        	JLabel5.setText(LBL_SAVEIN);
        	TxtMovePath.setVisible(true);
        	TxtMovePath.setText("");
        	TxtMovePath.setEnabled(false);
        }else if(selObject != null && selObject.getId() == 5 ){
        	BtnLocation.setVisible(false);
        	BtnLocation.setEnabled(false);
        	BtnBrowse.setVisible(true);
        	BtnBrowse.setEnabled(true);
        	JLabel5.setVisible(true);
        	JLabel5.setText(LBL_MOVETO);
        	TxtMovePath.setVisible(true);
        	TxtMovePath.setText("");
        	TxtMovePath.setEnabled(false);
        }else{
        	BtnLocation.setVisible(false);
        	BtnLocation.setEnabled(false);
        	BtnBrowse.setVisible(false);
        	BtnBrowse.setEnabled(false);
        	TxtMovePath.setVisible(false);
        	TxtMovePath.setEnabled(false);
        	JLabel5.setVisible(false);
        }
    }
/*
 * To get the tree structure
 */
    void BtnLocation_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(dlgLocation != null)
        {
            dlgLocation.loadDlgData();
            if(nodeId!=dlgLocation.tree.getSelectedNodeId())
                dlgLocation.tree.setSelectionPath(nodeId,true);
            	dlgLocation.setVisible(true);
        }
        else
        {
            dlgLocation = new VWDlgSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("dlgretentionsetting.dlgLocation"));
            dlgLocation.tree.setSelectionPath(nodeId,true);
            dlgLocation.setVisible(true);
        }
        if(dlgLocation.cancelFlag) return;

        TxtMovePath.setText(dlgLocation.tree.getSelectedNodeStrPath());
        nodeId=dlgLocation.tree.getSelectedNodeId();
    }
//------------------------------------------------------------------------------
 void BtnBrowse_actionPerformed(java.awt.event.ActionEvent event)
    {
		loadSelectFolderDialog();
 		//loadSaveDialog();
	}
//------------------------------------------------------------------------------
    public boolean saveRetentionSettings()
    {
        try{
        	AdminWise.adminPanel.setWaitPointer();
        	DocType docType=new DocType(curDocTypeId);
        	String retentionPeriod = TxtDays.getText();
        	int enable = ChkEnable.isSelected()?1:0;

        	if(retentionPeriod.trim().length() > 0)
	  		{
	  			try{
	  				int retPeriod = Integer.parseInt(retentionPeriod);
	  				if(retPeriod < 0 || retentionPeriod.equals("-0")){
	  					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("dlgretentionsetting.jmsg1"),"Retention Setting",JOptionPane.OK_OPTION);
		  				TxtDays.repaint();
		  				TxtDays.requestFocus(true);
		  				return false;
	  				}
	  			}catch(NumberFormatException ne){
	  				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("dlgretentionsetting.jmsg2"),"Retention Setting",JOptionPane.OK_OPTION);
	  				TxtDays.repaint();
	  				TxtDays.setText("");
	  				TxtDays.requestFocus(true);
	  				return false;
	  			}
             }else if(enable == 1){
  				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("dlgretentionsetting.jmsg3"),"Retention Setting",JOptionPane.OK_OPTION);
  				TxtDays.repaint();
  				TxtDays.requestFocus(true);
  				return false;
             }
            docType.setARSEnable((ChkEnable.isSelected()?"1":"0"));
            if(enable == 1){
    			String expStartsID = Integer.toString(((VWRecord)CmbExpStarts.getSelectedItem()).getId());
    			String DisposalActionID = Integer.toString(((VWRecord)CmbDisposalActions.getSelectedItem()).getId());
    			String now = (new Date()).toString();
	            docType.setAVRRetentionPeriod(retentionPeriod);
				docType.setARSRetentionPeriodStartsID(expStartsID);
				docType.setARSDisposalActionID(DisposalActionID);
				if(Integer.parseInt(DisposalActionID) == 1 || Integer.parseInt(DisposalActionID) == 2 ||Integer.parseInt(DisposalActionID) == 5 )
				{
					String backupPath=TxtMovePath.getText();
					if(backupPath==null||backupPath.equals("") ){
						JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("dlgretentionsetting.jmsg4"),"Retention Setting",JOptionPane.OK_OPTION);
						TxtMovePath.repaint();
						TxtMovePath.requestFocus(true);
		  				return false;
	
					}
					if(Integer.parseInt(DisposalActionID) == 1)
						docType.setARSMoveDocLocation(nodeId+":"+backupPath);
					else
						docType.setARSMoveDocLocation(backupPath);
				}
				else
					docType.setARSMoveDocLocation("");
				docType.setTag("0");
	            if(docType.getARSEnable().equals("1") && !alreadyEnabled)
	                VWMessage.showInfoDialog((java.awt.Component) this, VWMessage.MSG_RETENTION_SETTING_CHANGED);
	            docType.setTag("1");
				docType.setARSPolicyCreatedDate(now);
            }
            VWDocTypeConnector.setDocTypeRetention(docType);
            
        }catch(Exception e){}
            finally{AdminWise.adminPanel.setDefaultPointer();}
        return true;
   }
//------------------------------------------------------------------------------
  public void loadRetentionSettings(int docTypeId,boolean force)
    {

  		//if(!force && curDocTypeId==docTypeId) return;
  		vwDocTypeRec = VWDocTypeConnector.getDocTypeInfo(docTypeId);
  		setTitle(LBL_RETENTIONSETTING_NAME+" for \""+vwDocTypeRec.getName()+"\"");
        if(!force && curDocTypeId==docTypeId) return;
        String str="0";
        curDocTypeId=docTypeId;
        //JOptionPane.showMessageDialog(null,"VWR docTypeId "+docTypeId+":","",JOptionPane.OK_OPTION);
        DocType docType=null;
        docType = VWDocTypeConnector.getDocTypeRetention(curDocTypeId);
        if(docType==null || docType.getARSEnable().equals("0"))
        {
            ChkEnable.setSelected(false);
            ChkEnable_actionPerformed(null);
            alreadyEnabled=false;
            return;
        }
        ARSExpFields = null;
        ARSExpFields = getDateDocTypeFields(docTypeId);
        CmbExpStarts.removeAllItems();
		CmbDisposalActions.removeAllItems();
		CmbExpStarts.addItems(ARSExpFields);
        CmbDisposalActions.addItems(ARSActions);

        alreadyEnabled=true;
        ChkEnable.setSelected(true);
        ChkEnable_actionPerformed(null);
        alreadyEnabled=true;

        TxtDays.setText((docType.getARSRetentionPeriod()));

        int DisposalActionsIndex = Integer.parseInt(docType.getARSDisposalActionID());
		CmbDisposalActions.setSelectedIndex(DisposalActionsIndex);
		String indexID = docType.getARSRetentionPeriodStartsID();
		//JOptionPane.showMessageDialog(null,"indexID "+indexID+":","",JOptionPane.OK_OPTION);
		if(indexID == "-1")
		{
			CmbExpStarts.setSelectedIndex(0);
			}
		else if	(indexID == "-2")
		{
			CmbExpStarts.setSelectedIndex(1);
		}else {

			int cbmIndexID = getCmbIndexID(Integer.parseInt(indexID));
			CmbExpStarts.setSelectedIndex(cbmIndexID);
		}
		if(DisposalActionsIndex == 1 || (DisposalActionsIndex == 2) || (DisposalActionsIndex == 5) ){
			String temp = docType.getARSMoveDocLocation();
			if(DisposalActionsIndex == 1 ){
				if(temp!=null && !temp.equals("") && temp.indexOf(":")!=-1)
					temp = temp.substring(temp.indexOf(":")+1, temp.length());
			}
			TxtMovePath.setText(temp);
			TxtMovePath.setVisible(true);
		}
		if(DisposalActionsIndex == 1 ||DisposalActionsIndex == 5){
			JLabel5.setText(LBL_MOVETO);
		}
		else if(DisposalActionsIndex == 2){
			JLabel5.setText(LBL_SAVEIN);
		}

	    return;
	}
//------------------------------------------------------------------------------
  public void loadSelectFolderDialog()
{
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    String backupPath=TxtMovePath.getText();
    chooser.setCurrentDirectory(new File(backupPath));
    int returnVal = chooser.showSaveDialog(/*(Component)this*/null);
    if(returnVal == JFileChooser.APPROVE_OPTION)
    {
        backupPath=chooser.getSelectedFile().getPath();
        chooser.setVisible(false);
        chooser=null;
    }
    TxtMovePath.setText(backupPath);
}
  public void loadSaveDialog()
  {
      JFileChooser chooser = new JFileChooser();
      VWFileFilter filter = new VWFileFilter();
      filter.addExtension("html");
      //filter.addExtension("xml");
      filter.setDescription("Html Files");
      chooser.setCurrentDirectory(new File("c:\\"));
      chooser.setFileFilter(filter);
      int returnVal = chooser.showSaveDialog(null);
      String path = TxtMovePath.getText();
      if(returnVal == JFileChooser.APPROVE_OPTION)
      {
          path=chooser.getSelectedFile().getPath();
          if(!path.toUpperCase().endsWith(".HTML")) path+=".html";
          TxtMovePath.setText(path);
          //return path;
      };
      TxtMovePath.setText("");
      //return "";
  	}

    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt("x", this.getX());
            prefs.putInt("y", this.getY());
        }
        else
        {
            prefs.putInt("x",50);
            prefs.putInt("y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
    /*
     * To get list of date type index fields of particular doctype id
     */
    private VWRecord[] getDateDocTypeFields(int docTypeId){

    	List indices = null;
		indices = VWDocTypeConnector.getDTIndices(docTypeId);
		int count = 0;
		for (int j = 0; j<indices.size(); j++){
			VWIndexRec indexRec = (VWIndexRec) indices.get(j);
			if(indexRec.getType() == 2){
				count++;
			}
		}
    	VWRecord[] ARSExpFieldList = new VWRecord[count+2];
    	ARSExpFieldList[0] = new VWRecord(-1,AdminWise.connectorManager.getString("dlgretentionsetting.ARSExpFieldList0"));
    	ARSExpFieldList[1] = new VWRecord(-2,AdminWise.connectorManager.getString("dlgretentionsetting.ARSExpFieldList1"));
    	int cnt = 1;
		for (int j = 0; j<indices.size(); j++){
			VWIndexRec indexRec = (VWIndexRec) indices.get(j);
			if(indexRec.getType() == 2){
				ARSExpFieldList[cnt+1] = new VWRecord(indexRec.getId(),indexRec.getName());
				cnt++;
			}
		}
		//JOptionPane.showMessageDialog(null,"length "+ARSExpFieldList.length+":","",JOptionPane.OK_OPTION);
	   	return ARSExpFieldList;
    }

    private int getCmbIndexID(int indexID){

    	int i = 0;
    	int cnt = CmbExpStarts.getItemCount();
    	for(i=0; i<cnt; i++){
    		if(indexID == ((VWRecord)CmbExpStarts.getItemAt(i)).getId() )
    			break;
    		//((VWRecord)CmbExpStarts.getItemAt(i)).getName();
    	}
    	return i;
    }

    class TextFieldListener implements FocusListener{
	  	public void focusGained(FocusEvent e){

        }
	public void focusLost(FocusEvent e){
		/*
			javax.swing.JTextField source = (javax.swing.JTextField)e.getSource();

	  	    if(source.getName().equals("RetentionPeriod")){
	  	    	String sDays = source.getText();

		  		if(sDays.trim().length() > 0)
		  		{
		  			try{
		  				Integer.parseInt(sDays);

		  			}catch(NumberFormatException ne){
		  				JOptionPane.showMessageDialog(null,"Retention period should be in numeric","Retention Setting",JOptionPane.OK_OPTION);
		  				source.repaint();
		  				source.requestFocus(true);
		  				//source.nextFocus();
		  				//source.requestFocus(true);
		  				//source.setFocusable(true);
		  			}
	             }else{
	             	//JOptionPane.showMessageDialog(null,"Retention period should be in numeric","Retention Setting",JOptionPane.OK_OPTION);
	             	//source.requestFocus();
	             }
		  	 }
		  	 */
     }

    }

    public class JTextVerifier extends InputVerifier implements KeyListener {
    	private int nMaxLength=10;

    	/** Creates new JTextVerifier */
    	public JTextVerifier() {
    		super();
    	}
    	public JTextVerifier(int nLength) {
    		super();
    		nMaxLength=nLength;
    	}
    	public boolean verify(JComponent input) {
    		javax.swing.JTextField tf = (javax.swing.JTextField) input;
    		String str = tf.getText();
    		if (str.length() >= nMaxLength)
    			return false;
    		else
    			return true;
    	}
    	public void keyPressed(java.awt.event.KeyEvent ke) {
    	}
    	public void keyReleased(java.awt.event.KeyEvent ke) {
    	}
    	public void keyTyped(java.awt.event.KeyEvent ke) {
    		System.out.println("KeyTyped");
    		javax.swing.JTextField jt = (javax.swing.JTextField) ke.getSource();
    		if (jt.getSelectedText() == null && !jt.getInputVerifier().verify(jt))
    			jt.setText(jt.getText().substring(0,jt.getText().length()-1));
    	}
    }



    private int curDocTypeId=0;
    private boolean alreadyEnabled=false;
    public VWDlgSaveLocation dlgLocation = null;
    private int nodeId=0;
    private String nodePath="";
    VWRecord[] ARSExpFields = null;
    VWDocTypeRec vwDocTypeRec = null;
}
