/*
                      Copyright (c) 1997-2001
                     Computhink Software
 
                      All rights reserved.
 */
/**
 * VWDocTypeConnector<br>
 *
 * @version     $Revision: 1.5 $
 * @author      <a href="mailto:fadish@Bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWDocType;

import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import ViewWise.AdminWise.VWArchive.VWArchiveConnector;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;

import com.computhink.common.DBLookup;
import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.Document;
import com.computhink.vws.server.VWSPreferences;

public class VWDocTypeConnector implements VWConstant {
	public static DBLookup dbLookup;
	public static int docTypeId;
    public DBLookup getDbLookup() {
		return dbLookup;
	}
	public static void setDbLookup(DBLookup dsnLookup) {
		dbLookup = dsnLookup;
	}
	public static Vector getDocTypes() {
    	Vector docTypes=new Vector();
        try
        {
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector types=new Vector();
        

            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getDocTypes(room.getId(), types);
            int count=types.size();
            for(int i=0;i<count;i++)
                docTypes.add(new VWDocTypeRec((DocType)types.get(i)));
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return docTypes;
    }
    //-----------------------------------------------------------
    public static VWIndexRec[] getIndices(boolean withFixedFields) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector indicesList=new Vector();
        VWIndexRec[] indices=null;
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getIndices(room.getId(),0, indicesList);
            if(indicesList==null || indicesList.size()==0) return null;
            List recList=new LinkedList();
            int count=indicesList.size();
            int begin=0;
            /*
            if(withFixedFields)
            {
                begin=FixedFields.length;
                indices=new VWIndexRec[count+begin];
                for(int i=0;i<FixedFields.length;i++) recList.add(FixedFields[i]);
            }
            else
            {
             */
            indices=new VWIndexRec[count];
                /*
            }
                 */
            for(int i=0;i<count;i++) recList.add(new VWIndexRec((String)indicesList.get(i),true));
            recList=sortList(recList);
            for(int i=0;i<recList.size();i++) indices[i]=(VWIndexRec)recList.get(i);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return indices;
    }
    
    /**@author apurba.m
     * CV10.1 Enhancement: Added for calling new VWIndexRec constructor for avoiding the fetching of
     * selection values
     * @param withFixedFields
     * @return
     */
    public static VWIndexRec[] getIndicesForCustomReport(boolean withFixedFields) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector indicesList=new Vector();
        VWIndexRec[] indices=null;
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getIndices(room.getId(),0, indicesList);
            if(indicesList==null || indicesList.size()==0) return null;
            List recList=new LinkedList();
            int count=indicesList.size();
            int begin=0;
            indices=new VWIndexRec[count];
            for(int i=0;i<count;i++) {
            	/**@author apurba.m
            	 * Extra parameter is added for avoiding fetching of selection values
            	 */
            	recList.add(new VWIndexRec((String)indicesList.get(i),true,"customReportGeneration"));
            }
            recList=sortList(recList);
            for(int i=0;i<recList.size();i++) indices[i]=(VWIndexRec)recList.get(i);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return indices;
    }
    //-----------------------------------------------------------
    public static VWIndexRec getIndex(int indexId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector indicesList=new Vector();
        AdminWise.gConnector.ViewWiseClient.getIndices(room.getId(),indexId, indicesList);
        if(indicesList==null || indicesList.size()==0) return null;
        VWIndexRec indexRec=null;
        indexRec=new VWIndexRec((String)indicesList.get(0),true);
        return indexRec;
    }
    //-----------------------------------------------------------
    public static List getDTIndices(int docTypeId) {
    	return getDTIndices(docTypeId, "");
    }
    
    public static List getDTIndices(int docTypeId, String dontPopulateSelValues) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        DocType dt=new DocType(docTypeId);
        List indices=new LinkedList();
        try
        {
            AdminWise.adminPanel.setWaitPointer();    
            AdminWise.gConnector.ViewWiseClient.getDocTypeIndices(room.getId(), dt, dontPopulateSelValues);
            if(dt.getIndices()==null || dt.getIndices().size()==0) {
            	return null;
            }
            int count=dt.getIndices().size();
            for(int i =0;i<count;i++){
            	indices.add(new VWIndexRec((Index)dt.getIndices().get(i)));
            }
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return indices;
    }
    //-----------------------------------------------------------
    public static List displayAddIndexDlg(VWIndexRec[] indices) {
        VWDlgAddIndex addIndexDlg = new VWDlgAddIndex(AdminWise.adminFrame,indices);
        addIndexDlg.requestFocus();
        if(addIndexDlg.getCancelFlag()) return null;
        List retValues=addIndexDlg.getValues();
        addIndexDlg.dispose();
        return retValues;
    }
    //-----------------------------------------------------------
    public static Vector displaySelectionIndexDlg() {
        VWDlgSelectionIndex selectionIndexDlg = new VWDlgSelectionIndex(AdminWise.adminFrame);
        selectionIndexDlg.requestFocus();
        if(selectionIndexDlg.getCancelFlag()) return null;
        Vector retValues=selectionIndexDlg.getValuesList();
	    /*
	     * Issue # 599-1 	: Box character appearing in between the selection index value  	 
	     * Created By	: M.Premananth
	     * Date			: 11-Jul-2006
	     */
        String sTemp = "";
        for(int i=0;i<retValues.size();i++){
        	sTemp = (String)retValues.get(i);
        	StringTokenizer st = new StringTokenizer(sTemp,"\t");
        	if(st.countTokens() > 1){
        		while(st.hasMoreTokens())
        			retValues.add(st.nextToken());
        		retValues.remove(i);
        	}
        }
        AdminWise.adminPanel.docTypePanel.setSelectionIndexValues(retValues);
        String defaultValue=AdminWise.adminPanel.docTypePanel.getSelectionIndexDefaultValues().trim();
	    /*
	     * Issue # 599-1: Default Selection value not retained, after altering the values in Selection Index Values window  	 
	     * Created By	: M.Premananth
	     * Date			: 11-Jul-2006
	     */
         boolean bFlag = true;
        for(int i=0;i<retValues.size();i++){
	        if(retValues.get(i).toString().trim().equalsIgnoreCase(defaultValue)){
	        	bFlag = false;
	        	break;
	        }
        }
        if(bFlag)
        	AdminWise.adminPanel.docTypePanel.setSelectionIndexDefaultValues("");
        selectionIndexDlg.dispose();
        return retValues;
    }
    //-----------------------------------------------------------
    // calling validate sequence index to include Unique to room in Unique property
    public static int validateSeqIndex(int indexId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
       
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Index index = new Index(indexId);
       
        return AdminWise.gConnector.ViewWiseClient.getValidateSeqIndex(room.getId(),indexId);
    }
    //-----------------------------------------------------------
    public static Vector getSelectionValues() {
        return getSelectionValues(AdminWise.adminPanel.docTypePanel.getSelIndexId());
    }
    //-----------------------------------------------------------
    public static Vector getSelectionValues(int indexId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Index index = new Index(indexId);
        AdminWise.gConnector.ViewWiseClient.populateSelectionIndex(room.getId(), index);
        return index.getDef();
    }
    //-----------------------------------------------------------
    public static int updateIndex(VWIndexRec newIndex) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        int newId=AdminWise.gConnector.ViewWiseClient.updateIndex(room.getId(),
        newIndex.getIndex());
        if(newId>0 && newIndex.getType()==Index.IDX_SELECTION){
        	int docTypeId = AdminWise.adminPanel.docTypePanel.getSelDTId();
        	try{
        		if(newIndex.getDbDSNInfo()!=null){
        			/**
        			 * Issue: Saving DSN Info with doctypeId and IndexId with -1. 
        			 * So creating DT and Index and then inserting DSNInfo. 
        			 */
    				if(AdminWise.adminPanel.docTypePanel.curMode==MODE_NEW && docTypeId < 0){
    					docTypeId = getDocTypeId();
    				}
    				else if(AdminWise.adminPanel.docTypePanel.curMode==MODE_NEWINDEX){
    					//On creation of index field from New Index field Btn
    					docTypeId = 0;
    				}
        			updateSelectionDSNInfo(docTypeId, newId, newIndex.getDbDSNInfo());
        		}
        	}catch(Exception ex){
        	}
        	updateIndexSelectionValues(newId,newIndex.getIndexSelectionValues());

        }
        return newId;
    }
	//-----------------------------------------------------------
    public static int updateDTIndex(int docTypeId,VWIndexRec newIndex,int indexOreder) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Index index =new Index(newIndex.getId());
        try
        {
            AdminWise.adminPanel.setWaitPointer(); 
            index.setDTIndexId(newIndex.getDTIndexId());
            index.setMask(newIndex.getMask());
            index.setDef(newIndex.getDefault());
            index.setKey(newIndex.getKey());
            index.setRequired(newIndex.getRequired());
            index.setOrder(indexOreder);
            index.setInfo(newIndex.getInfo());
            index.setDescription(newIndex.getDescription());
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return AdminWise.gConnector.ViewWiseClient.updateDTIndex(room.getId(),
        docTypeId,index);
    }
    //-----------------------------------------------------------
    public static int updateDocType(VWDocTypeRec docTypeRec) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        DocType docType=new DocType(docTypeRec.getId());
        docType.setName(docTypeRec.getName());
        docType.setEnableTextSearch(docTypeRec.getEnableSIC());
        return AdminWise.gConnector.ViewWiseClient.updateDocType(room.getId(), docType);
    }
    //-----------------------------------------------------------
    public static int removeDocType(VWDocTypeRec docTypeRec) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        return AdminWise.gConnector.ViewWiseClient.deleteDocType(room.getId(),
        new DocType(docTypeRec.getId()));
    }
    //-----------------------------------------------------------
    public static int removeIndex(VWIndexRec indexRec) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        if(indexRec==null) return 0;
        AdminWise.gConnector.ViewWiseClient.deleteIndex(room.getId(),
        new Index(indexRec.getId()));
        return 1;
    }
    //-----------------------------------------------------------
    public static int removeDocTypeIndex(VWIndexRec indexRec) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        return AdminWise.gConnector.ViewWiseClient.deleteDTIndex(room.getId(),
        new Index(indexRec.getDTIndexId()));
    }
    //-----------------------------------------------------------
    public static void updateSelectionDSNInfo(int docTypeId, int indexId,DBLookup dbDSNInfo) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        try
        {
        	System.out.println("updateSelectionDSNInfo");
            AdminWise.adminPanel.setWaitPointer();
            dbDSNInfo.setIndexId(indexId);
            dbDSNInfo.setDocTypeId(docTypeId);
            AdminWise.gConnector.ViewWiseClient.setDBLookup(room.getId(), dbDSNInfo);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //-----------------------------------------------------------
    /**
     * Not in use.
     * We are passing the selection values as a vector not as a String.
     * Need to comment for Selction list optimization.
     *  
     * ViewWiseClient.updateIndexSelection method by string selection values are used from DTC 
     */
    public static void updateIndexSelectionValues(int indexId,String strValues) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        try
        {
            AdminWise.adminPanel.setWaitPointer(); 
            if(strValues==null || strValues.trim().equals("")) return;
            AdminWise.gConnector.ViewWiseClient.updateIndexSelection(room.getId(),
            new Index(indexId), strValues);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    public static void updateIndexSelectionValues(int indexId, Vector selValues) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        try
        {
            AdminWise.adminPanel.setWaitPointer(); 
            if(selValues==null) return;
            AdminWise.gConnector.ViewWiseClient.updateIndexSelection(room.getId(),
            new Index(indexId), selValues);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    
    //-----------------------------------------------------------
    public static boolean checkIndexNameIsExists(VWIndexRec indexRec) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return true;
        Index index=new Index(indexRec.getId());
        index.setName(indexRec.getName());
        index.setType(indexRec.getType());
        index.setMask(indexRec.getMask());
        index.setSystem(indexRec.getSVid());
        index.setInfo(indexRec.getInfo());
/*        System.out.println(index.getName()+";"+
            String.valueOf(index.getType())+";"+
            index.getMask()+";"+
            String.valueOf(index.getSystem())+";"+
            String.valueOf(1));*/
        int indexId= AdminWise.gConnector.ViewWiseClient.isIndexFound(room.getId(),index,1); 
        if (indexId==indexRec.getId() || indexId==0) return false;
        return true;
    }
    //-----------------------------------------------------------
    public static VWDocTypeRec getDocTypeInfo(int docTypeid) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        DocType docType=new DocType(docTypeid);
        Vector types=new Vector();
        AdminWise.gConnector.ViewWiseClient.
        getDocType(room.getId(), docType, types);
        if(types==null || types.size()==0) return null;
        VWDocTypeRec docTypes=new VWDocTypeRec((DocType)types.get(0));
        return docTypes;
    }
    //-----------------------------------------------------------
    public static DocType getDocTypeVerRev(int docTypeid) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        DocType docType=new DocType(docTypeid);
        AdminWise.gConnector.ViewWiseClient.getDTVersionSettings(room.getId(),docType);
        return docType;
    }
    //-----------------------------------------------------------
    public static void setDocTypeVerRev(DocType docType) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.setDTVersionSettings(room.getId(),docType);
    }


	 /*
	  * Purpose:  To get Retention details of Document Type
	  * Created by: Shanmugavalli.C
	  * Date: June 14 2006
	  */

     public static DocType getDocTypeRetention(int docTypeid) {
         VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
         ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
         DocType docType=new DocType(docTypeid);
         AdminWise.gConnector.ViewWiseClient.getDTRetentionSettings(room.getId(),docType);
         return docType;
     }

	 /*
	  * Purpose:  To set Retention details of Document Type
	  * Created by: Shanmugavalli.C
	  * Date: June 14 2006
	  */
    public static void setDocTypeRetention(DocType docType) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.setDTRetentionSettings(room.getId(),docType);
    }
    /* Purpose:  Added for ARS service Master enable and disable
     * Created By: C.Shanmugavalli		Date:	20 Sep 2006
     */

    public static int isARSEnabled() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return -1;
        return AdminWise.gConnector.ViewWiseClient.isARSEnabled(room.getId());
    }
    public static int isDRSEnabled() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return -1;
        return AdminWise.gConnector.ViewWiseClient.isDRSEnabled(room.getId());
    }
    //-----------------------------------------------------------
    public static boolean getIsSearchICEnable() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return false;
        return (AdminWise.gConnector.ViewWiseClient.isTextSearchEnabled(room.getId())==1);
    }
    //-----------------------------------------------------------
    public static void updateDocTypeAutoMail(VWDocTypeRec docTypeRec) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        DocType docType=new DocType(docTypeRec.getId());
        docType.setTo(docTypeRec.getAutoMail_To());
        docType.setCc(docTypeRec.getAutoMail_Cc());
        docType.setSubject(docTypeRec.getAutoMail_Subject());
        AdminWise.gConnector.ViewWiseClient.setDocTypeAutoMail(room.getId(),docType);
    }
    //-----------------------------------------------------------
    private static List sortList(List sList) {
        List newList=new LinkedList();
        int count = sList.size();
        String str="",strTmp="";
        for(int i=0;i<count;i++) {
            str=((VWIndexRec)sList.get(i)).getName();
            if (str!=null) {
                str=str.trim();
                if(newList.size()==0) {
                    newList.add(sList.get(i));
                }
                else {
                    boolean inserted=false;
                    strTmp=str.toLowerCase();
                    for(int j=0;j<newList.size();j++)
                        if(strTmp.compareTo((((VWIndexRec)newList.get(j)).
                            getName()).toLowerCase()) < 0) {
                            newList.add(j,sList.get(i));
                            inserted=true;
                            break;
                        }
                    if(!inserted) newList.add(sList.get(i));
                }
            }
        }
        return newList;
    }
    //-----------------------------------------------------------
    public static List getDTDocumentsNotInIndexer(int docTypeId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        List nodeIdList=AdminWise.gConnector.DoAction("GetDTDocsNotIndexed",
        Integer.toString(docTypeId),room.getId());
        if(nodeIdList==null || nodeIdList.size()==0) return null;
        return nodeIdList;
    }
    //-----------------------------------------------------------
    public static void addDocumentToIndexer(String nodeId,boolean withStatus) {
        int result=0;
        if(withStatus)
            AdminWise.adminPanel.docTypePanel.indexerInfoDlg.PrgrsDoc.setValue(20);
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(withStatus) {
            AdminWise.adminPanel.docTypePanel.indexerInfoDlg.setDocText
                (AdminWise.connectorManager.getString("docTypeConnector.indexerInfoDlgTxt_0"));
            AdminWise.adminPanel.docTypePanel.indexerInfoDlg.PrgrsDoc.setValue(30);
        }
        result=AdminWise.gConnector.ViewWiseClient.addIndexerDoc(room.getId(), 
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
Pass the third argument as '0', to index all the pages in the documents.
*/
		
            new Document(VWUtil.to_Number(nodeId)),0);
        if(withStatus) {
            AdminWise.adminPanel.docTypePanel.indexerInfoDlg.PrgrsDoc.setValue(80);
            if(result==0)
                AdminWise.adminPanel.docTypePanel.indexerInfoDlg.setDocText(AdminWise.connectorManager.getString("docTypeConnector.indexerInfoDlgTxt_1"));
            else
                AdminWise.adminPanel.docTypePanel.indexerInfoDlg.setDocText
                    (AdminWise.connectorManager.getString("docTypeConnector.indexerInfoDlgTxt_2"));
            AdminWise.adminPanel.docTypePanel.indexerInfoDlg.PrgrsDoc.setValue(100);
        }
    }
    //-----------------------------------------------------------

	 /*
	  * Purpose:  To get Disposal actions
	  * Created by: Shanmugavalli.C
	  * Date: June 14 2006
	  */
    public static Vector getDisposalActs() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector disposalActs = new Vector();
        Vector docTypes=new Vector();
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getDisposalActions(room.getId(), disposalActs);
            int count=disposalActs.size();
            for(int i=0;i<count;i++)
                docTypes.add(new VWDocTypeRec((DocType)disposalActs.get(i)));
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return disposalActs;
    }
	
    public static int getDocTypeId() {
		return VWDocTypeConnector.docTypeId;
	}
	public static void setDocTypeId(int docTypeId) {
		VWDocTypeConnector.docTypeId = docTypeId;
	}
	/* Method checkKeyIndex is added to do the procedure call 
	 * If the index value is empty it returns the index name 
	 * otherwise returns zero.
	 * Implemented for the document key field change. 
	 * Release :Build5-001
	 * Added By Madhavan
	 */
	public static int checkKeyIndex(VWDocTypeRec docTypeRec, Vector indiceslist, int count,Vector result) {
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        DocType docType=new DocType(docTypeRec.getId());
        docType.setName(docTypeRec.getName());
        docType.setEnableTextSearch(docTypeRec.getEnableSIC());
        return AdminWise.gConnector.ViewWiseClient.checkKeyIndex(room.getId(),docType,indiceslist,count,result);
	}
	/*
	 * updateDocTypeKeyField to set the current key field.
	 * Implemented for the document key field change.
	 * Release :Build5-001
	 * Added By Madhavan
	 */
	public static int updateDocTypeKeyField(int oldKeyIndexId, int newkeyIndexId, VWDocTypeRec docTypeRec) {
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        DocType docType=new DocType(docTypeRec.getId());
        docType.setName(docTypeRec.getName());
        docType.setEnableTextSearch(docTypeRec.getEnableSIC());
		return AdminWise.gConnector.ViewWiseClient.updateDocTypeKeyField(room.getId(),oldKeyIndexId,newkeyIndexId,docType);
	}
}