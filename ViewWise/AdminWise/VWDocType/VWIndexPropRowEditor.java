/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWSimpleSearchRowEditor<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/

package ViewWise.AdminWise.VWDocType;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWMask.VWMaskField;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import com.computhink.common.Index;
import java.util.Vector;

public class VWIndexPropRowEditor implements TableCellEditor,VWConstant{
    
    protected TableCellEditor editor,defaultEditor;
    private VWIndexPropTable gTable=null;
    private VWMaskField gMaskEdit=null;
    //-----------------------------------------------------------
    public VWIndexPropRowEditor(VWIndexPropTable table){
        JTextField text=new JTextField();
        text.setBorder(null);
        defaultEditor = new DefaultCellEditor(text);
        gTable=table;
    }
    //-----------------------------------------------------------
    public Component getTableCellEditorComponent(JTable table,
    Object value,boolean isSelected,int row,int column) {
        gMaskEdit=null;
        String displayValue=(String)value;
        int type = gTable.getIndexType();
        //AdminWise.printToConsole("type:"+type+":displayValue:"+displayValue);
        boolean used= false;/*AdminWise.adminPanel.docTypePanel.getSelIndexUsed();*/
        if(type==Index.IDX_TEXT) //text type
        {
            if(row==0 && !used)
                editor=defaultEditor; // mask field
            else if(row==1)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
            else if(row==2 && !used)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
            else if(row==3) {
                editor=defaultEditor;
                String mask=gTable.getValues()[0].trim();
                if(!mask.equals(""))
                    editor= new DefaultCellEditor(new VWMaskField(mask,VWMaskField.TEXT_TYPE,true));// defaultVal field
            }
        }else if(type==Index.IDX_BOOLEAN) //Boolean type
        {
            if(row==0)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
            else if(row==1)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Default value field
        }else if(type==Index.IDX_DATE) {
            if(row==0 && !used)
                editor=new DefaultCellEditor(new VWComboBox(DateMasks)); // mask field
            else if(row==1)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
            else if(row==2)
                editor=new DefaultCellEditor(new VWComboBox(DateValueTypes)); // DateValueTypes
            else if(row==3) {
                VWDateComboBox dateCombo=new VWDateComboBox();
                ///dateCombo.setEditable(true);
                dateCombo.setDateFormat(gTable.getValues()[0]);
                editor=new DefaultCellEditor(dateCombo);// defaultVal field
            }
        }else if(type==Index.IDX_SEQUENCE) {
            if(row==0 && !used)
                editor=defaultEditor; // Prefix field
            ///editor= new DefaultCellEditor(new VWMaskField(VWMaskField.SEQUENCE_TYPE,10,true));
            else if(row==1 && !used)
                editor= new DefaultCellEditor(new VWMaskField(VWMaskField.USNUMBER_TYPE,1,true));// digitals field
            else if(row==2 && !used)
                editor=defaultEditor;// suffix field
            ///editor= new DefaultCellEditor(new VWMaskField(VWMaskField.SEQUENCE_TYPE,10,true));
            else if(row==3)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
            else if(row==4 && !used){
            	boolean indexUsed= AdminWise.adminPanel.docTypePanel.getSelIndexUsed();
            	if (VWDocTypePanel.checkcoditon == true&&indexUsed) 
            		editor=new DefaultCellEditor(new VWComboBox(YesNoData));//Unique to room is add in combo
            	else 
            		editor=new DefaultCellEditor(new VWComboBox(SeqUniqData));
            }
             //editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
            else if(row==5)// defaultVal field
            {
                String strLen=gTable.getValues()[1].trim().equals("")?"0":gTable.getValues()[1].trim();
                int noOfDigit=0;
                try{
                    noOfDigit=VWUtil.to_Number(strLen);
                }
                catch(Exception e){};
                editor= new DefaultCellEditor(new VWMaskField(VWMaskField.USNUMBER_TYPE,noOfDigit,true));
            }
        }else if(type==Index.IDX_SELECTION) {
            if(row==1)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
            else if(row==2 && !used)
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
            else if(row==4) {
                editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // MultiSelect
            }
            else if(row==3)
            {
                VWIndexRec selIndexRec=AdminWise.adminPanel.docTypePanel.IndicesTable.getRowData();
                Vector arrValues = selIndexRec.getIndexSelectionValues();
                String strValues = selIndexRec.getIndexSelectionValuesStr();
                String selValuesArray[] = strValues.split("\\|");
                if(selValuesArray.length == arrValues.size()){
                	if (arrValues.get(0).toString().trim().length() > 0)
                		arrValues.insertElementAt(" ", 0);
                }
                if(arrValues==null || arrValues.size()==0){
                	arrValues = VWDocTypeConnector.getSelectionValues();
                }
                editor=new DefaultCellEditor(new VWComboBox(arrValues));// Default field
            }
        }else if(type==Index.IDX_NUMBER) {
            if(row==0 && !used)
                editor=new DefaultCellEditor(new VWComboBox(VWIndexNumericMask));// Mask field
            String subType=AdminWise.adminPanel.docTypePanel.PropTable.getRowData(0)[1];
            if(subType.equalsIgnoreCase(VWIndexNumericMask[0])) {
                if(row==1)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
                else if(row==2 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
                else if(row==3) {
                    gMaskEdit=new VWMaskField(AdminWise.adminPanel.docTypePanel.
                    getSelIndexNumberMask(0),VWMaskField.NUMBER_TYPE,true);
                    editor= new DefaultCellEditor(gMaskEdit);// Default Value
                    displayValue=gMaskEdit.getActualValue(value);
                    gMaskEdit.setText(displayValue);
                }
            }else if(subType.equalsIgnoreCase(VWIndexNumericMask[1])) {
                if(row==1 && !used) {
                    editor= new DefaultCellEditor(new VWMaskField(VWMaskField.USNUMBER_TYPE,1,true));// Desimal Places
                }
                else if(row==2 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData));
                else if(row==3 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData));
                else if(row==4)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
                else if(row==5 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
                else if(row==6) {
               		gMaskEdit=new VWMaskField(AdminWise.adminPanel.docTypePanel.
               		getSelIndexNumberMask(1),VWMaskField.NUMBER_TYPE,true);
                    editor= new DefaultCellEditor(gMaskEdit);// Default Value
                    displayValue=gMaskEdit.getActualValue(value);
                    gMaskEdit.setText(displayValue);
                }
            }else if(subType.equalsIgnoreCase(VWIndexNumericMask[2])) {
                if(row==1 && !used)
                    editor= new DefaultCellEditor(new VWMaskField(VWMaskField.USNUMBER_TYPE,1,true));// Desimal Places
                else if(row==2 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(VWMaskField.getCurrencySymbols()));
                else if(row==3 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData));
                else if(row==4 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData));
                else if(row==5)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
                else if(row==6 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
                else if(row==7) {
                	String indexMask = AdminWise.adminPanel.docTypePanel.getSelIndexNumberMask(2);//0.00
                	/*if(indexMask.indexOf(".")!=-1){
                		int dotIndex = indexMask.indexOf(".");
                		String mask = indexMask.substring(dotIndex+1);
                		if(mask.indexOf(".")!=-1){
                	      gMaskEdit=new VWMaskField(mask,VWMaskField.NUMBER_TYPE,true);
                		}else{
                			gMaskEdit=new VWMaskField(AdminWise.adminPanel.docTypePanel.getSelIndexNumberMask(2),VWMaskField.NUMBER_TYPE,true);
                		}
                	}else*/{
                		gMaskEdit=new VWMaskField(AdminWise.adminPanel.docTypePanel.getSelIndexNumberMask(2),VWMaskField.NUMBER_TYPE,true);
                	}
                    gMaskEdit.setCurrencySymbol(gTable.getValues()[2]);
                    editor= new DefaultCellEditor(gMaskEdit);// Default Value
                    displayValue=gMaskEdit.getActualValue(value);
                }
            }else if(subType.equalsIgnoreCase(VWIndexNumericMask[3])) {
                if(row==1 && !used)
                    editor= new DefaultCellEditor(new VWMaskField(VWMaskField.USNUMBER_TYPE,1,true));// Desimal Places
                else if(row==2)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
                else if(row==3 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
                else if(row==4) {
                    gMaskEdit=new VWMaskField(AdminWise.adminPanel.docTypePanel.
                    getSelIndexNumberMask(3),VWMaskField.NUMBER_TYPE,true);
                    editor= new DefaultCellEditor(gMaskEdit);// Default Value
                    displayValue=gMaskEdit.getActualValue(value);
                }
            }else if(subType.equalsIgnoreCase(VWIndexNumericMask[4])) {
                if(row==1 && !used)
                    editor= new DefaultCellEditor(new VWMaskField(VWMaskField.USNUMBER_TYPE,1,true));// Desimal Places
                else if(row==2)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Read Only
                else if(row==3 && !used)
                    editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Unique
                else if(row==4) {
                    gMaskEdit=new VWMaskField(AdminWise.adminPanel.docTypePanel.
                    getSelIndexNumberMask(4),VWMaskField.NUMBER_TYPE,true);
                    editor= new DefaultCellEditor(gMaskEdit);// Default Value
                    displayValue=gMaskEdit.getActualValue(value);
                }
            }
        }
        return editor.getTableCellEditorComponent(table,value,isSelected,row,column);
    }
    //-----------------------------------------------------------
    public Object getCellEditorValue(){
        if(gMaskEdit !=null) {
            try {
                gMaskEdit.commitEdit();
            }
            catch(java.text.ParseException e){}
            String retValue=gMaskEdit.getFormatedValue();
            return retValue;
        }
        return editor.getCellEditorValue();
    }
    //-----------------------------------------------------------
    public boolean stopCellEditing(){
        return editor.stopCellEditing();
    }
    //-----------------------------------------------------------
    public void cancelCellEditing() {
        editor.cancelCellEditing();
    }
    //-----------------------------------------------------------
    public boolean isCellEditable(EventObject anEvent){
        int mode=AdminWise.adminPanel.docTypePanel.getCurMode();
        if(mode!=MODE_NEW && mode!=MODE_UPDATE && mode!=MODE_NEWINDEX) return false;
        int column=0,row=0,type=0;
        if (anEvent instanceof MouseEvent) {
            Point point=((MouseEvent)anEvent).getPoint();
            column = gTable.columnAtPoint(point);
            row = gTable.rowAtPoint(point);
            type = gTable.getIndexType();
        }
        if(column==0) return false;
        if(AdminWise.adminPanel.docTypePanel.getIsFixedIndex()) return false;
        boolean dtUsed= AdminWise.adminPanel.docTypePanel.getSelDTIndexUsed();
        boolean used= AdminWise.adminPanel.docTypePanel.getSelIndexUsed();
        int dtIndexId=AdminWise.adminPanel.docTypePanel.getSelIndexDTId();
        if(type==Index.IDX_TEXT) {
            if(row==0 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // mask field
            else if(row==1) return true; // Read Only
            else if(row==2 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // Unique
            else if(row==3) return true; // defaultVal field
        }
        else if(type==Index.IDX_BOOLEAN) {
            if(row==0) return true; // Read Only
            else if(row==1) return true; // Default Value
        }
        else if(type==Index.IDX_DATE) {
            if(row==0 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // mask field
            else if(row==1) return true; // Read Only
            else if(row==2) return true; // Value Types
            else if(row==3 ) {
                String valueType=AdminWise.adminPanel.docTypePanel.PropTable.getRowData(2)[1];
                if(valueType.equalsIgnoreCase(DateValueTypes[1]) || valueType.equalsIgnoreCase(DateValueTypes[2]) )
                /*&& AdminWise.adminPanel.docTypePanel.getSelIndexSVid()==0*/
                    return true;// defaultVal field
                else
                    return false;
            }
        }
        else if(type==Index.IDX_NUMBER) {
            String subType=AdminWise.adminPanel.docTypePanel.PropTable.getRowData(0)[1];
            if(row==0 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;
            else if(subType.equalsIgnoreCase(VWIndexNumericMask[0])) {
                if(row==1) return true; // Read Only
                else if(row==2 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // Unique
                else if(row==3) return true;// Default Value
            }
            else if(subType.equalsIgnoreCase(VWIndexNumericMask[1])) {
                if(row==1 && /*!used &&*/ (!dtUsed ||dtIndexId==0)) return true;// Desimal Places
                else if(row==2 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;
                else if(row==3 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;
                else if(row==4) return true; // Read Only
                else if(row==5 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // Unique
                else if(row==6) return true;// Default Value
            }
            else if(subType.equalsIgnoreCase(VWIndexNumericMask[2])) {
                if(row==1 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;// Desimal Places
                else if(row==2 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;
                else if(row==3 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;
                else if(row==4 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;
                else if(row==5) return true; // Read Only
                else if(row==6 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // Unique
                else if(row==7) return true;// Default Value
            }
            else if(subType.equalsIgnoreCase(VWIndexNumericMask[3])) {
                if(row==1 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;// Desimal Places
                else if(row==2) return true; // Read Only
                else if(row==3 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // Unique
                else if(row==4) return true;// Default Value
            }
            else if(subType.equalsIgnoreCase(VWIndexNumericMask[4])) {
                if(row==1 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true;// Desimal Places
                else if(row==2) return true; // Read Only
                else if(row==3 /*&& !used*/ && (!dtUsed ||dtIndexId==0)) return true; // Unique
                else if(row==4) return true;// Default Value
            }
        }
        else if(type==Index.IDX_SEQUENCE) {
            if(row==0 /*&& !used*/ && ((!dtUsed ||dtIndexId==0)&&!VWDocTypePanel.uniqueSeqSetFlag)) return true; // Preffix field
            else if(row==1 /*&& !used*/ && ((!dtUsed ||dtIndexId==0)&&!VWDocTypePanel.uniqueSeqSetFlag)) return true;// digitals field
            else if(row==2 /*&& !used*/ && ((!dtUsed ||dtIndexId==0)&&!VWDocTypePanel.uniqueSeqSetFlag)) return true;// suffix field
            else if(row==3&&!VWDocTypePanel.uniqueSeqSetFlag) return true; // Read Only
            else if(row==4 && ((!dtUsed ||dtIndexId==0)&&!VWDocTypePanel.uniqueSeqSetFlag))return true; // Unique
            else if(row==5 && (!dtUsed&&!VWDocTypePanel.uniqueSeqSetFlag)) return true;// defaultVal field
        }
        else if(type==Index.IDX_SELECTION) {
            if(row==0) {
                VWDocTypeConnector.displaySelectionIndexDlg();
                return false; // Values field
            }
            else if(row==1) return true; // Read Only
            else if(row==2 && /*!used &&*/ (!dtUsed ||dtIndexId==0)) return true; // Unique
            else if(row==4){  //MultiSelect
            	AdminWise.printToConsole("MultiSelect row VWIndexPropRowEditor.isCellEditable ::::"+row);
            	return true;  
            }
            else if(row==3) // Default field
            {
                String values=AdminWise.adminPanel.docTypePanel.PropTable.getRowData(0)[1];
                if(values != null && !values.equals(""))    return true;
            }
        }
        return false ;
    }
    //-----------------------------------------------------------
    public void addCellEditorListener(CellEditorListener l){
        editor.addCellEditorListener(l);
    }
    //-----------------------------------------------------------
    public void removeCellEditorListener(CellEditorListener l){
        editor.removeCellEditorListener(l);
    }
    //-----------------------------------------------------------
    public boolean shouldSelectCell(EventObject anEvent){
        return editor.shouldSelectCell(anEvent);
    }
    //-----------------------------------------------------------
}