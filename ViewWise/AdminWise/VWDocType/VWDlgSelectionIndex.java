
/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWDocType;

import java.awt.*;
import javax.swing.*;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.TreeSet;

import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.AdminWise;
import java.util.prefs.*;
import ViewWise.AdminWise.VWDSN.VWDNSDlg;
import javax.swing.JDialog;

import com.computhink.common.DBLookup;

import ViewWise.AdminWise.VWImages.VWImages;
import java.util.Vector;

public class VWDlgSelectionIndex extends JDialog implements VWConstant
{
	public VWDlgSelectionIndex(Frame parent) 
	{
		super(parent,true);
                ///parent.setIconImage(VWImages.AddIcon.getImage());
		getContentPane().setBackground(AdminWise.getAWColor());
		setTitle(LBL_SELECTIONINDEX_NAME);
		getContentPane().setLayout(null);
		setVisible(false);
		JLabel1.setText(LBL_SELECTIONINDEXVALUES_NAME);
		getContentPane().add(JLabel1);
		JLabel1.setBounds(6,4,194, AdminWise.gBtnHeight);
		getContentPane().add(JSPSQL);
		JSPSQL.setBounds(4,28,276,366);
                
        BtnClear.setText(BTN_CLEAR_NAME);
		getContentPane().add(BtnClear);
		BtnClear.setBounds(220,8,60,20);
//      BtnClear.setBackground(java.awt.Color.white);
                
        BtnSort.setText(BTN_SORT_NAME);
		getContentPane().add(BtnSort);
		BtnSort.setBounds(170,8,50,20);
//      BtnSort.setBackground(java.awt.Color.white);
                
		BtnAdd.setText(BTN_OK_NAME);
		getContentPane().add(BtnAdd);
		BtnAdd.setBounds(6,410,82,20);
//      BtnAdd.setBackground(java.awt.Color.white);
        BtnAdd.setIcon(VWImages.OkIcon);
                
		BtnDSN.setText(BTN_DSN_NAME);
		getContentPane().add(BtnDSN);
		BtnDSN.setBounds(100,410,82,20);
//                BtnDSN.setBackground(java.awt.Color.white);
		BtnCancel.setText(BTN_CANCEL_NAME);
		getContentPane().add(BtnCancel);
		BtnCancel.setBounds(194,410,82,20);
//		BtnCancel.setBackground(java.awt.Color.white);
		BtnCancel.setIcon(VWImages.CloseIcon);

		///TxtValues.setBorder(etchedBorder); 
		TxtValues.setLineWrap(false);
		TxtValues.setWrapStyleWord(false);
		SymAction lSymAction = new SymAction();
		BtnAdd.addActionListener(lSymAction);
		BtnDSN.addActionListener(lSymAction);
		BtnCancel.addActionListener(lSymAction);
		BtnSort.addActionListener(lSymAction);
		BtnClear.addActionListener(lSymAction);
		SymKey aSymKey = new SymKey();
		addKeyListener(aSymKey);
		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		setResizable(false);
		loadValues();
		getDlgOptions();
		setVisible(true);
		//}}
	}
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosed(java.awt.event.WindowEvent event)
        {
                Object object = event.getSource();
                if (object == VWDlgSelectionIndex.this)
                        Dialog_windowClosed(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosed(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnAdd_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnAdd)
                        BtnAdd_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
                else if (object == BtnDSN)
                        BtnDSN_actionPerformed(event);
                else if (object == BtnSort)
                        BtnSort_actionPerformed(event);
                else if (object == BtnClear)
                        BtnClear_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
	public void setVisible(boolean b)
	{
		if (b) 
                {
                    Dimension d =VWUtil.getScreenSize();
                    setLocation(d.width/4,d.height/4);
                }
		super.setVisible(b);
	}
//------------------------------------------------------------------------------
	public void addNotify()
	{
            Dimension size=getSize();
            super.addNotify();
            if (frameSizeAdjusted)  return;
            frameSizeAdjusted=true;
            Insets insets=getInsets();
	}
//------------------------------------------------------------------------------
	boolean frameSizeAdjusted = false;
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	javax.swing.JTextArea TxtValues = new javax.swing.JTextArea();
        javax.swing.JScrollPane JSPSQL = new javax.swing.JScrollPane(TxtValues);
	VWLinkedButton BtnAdd = new VWLinkedButton(0,true);
	VWLinkedButton BtnDSN = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
        VWLinkedButton BtnSort = new VWLinkedButton(0,true);
        VWLinkedButton BtnClear = new VWLinkedButton(0,true);
        javax.swing.border.EtchedBorder etchedBorder = new javax.swing.border.EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
//------------------------------------------------------------------------------
    void BtnAdd_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnDSN_actionPerformed(java.awt.event.ActionEvent event)
    {
    		VWDNSDlg dsnDlg = new VWDNSDlg((Frame)this.getParent());
            List dsnResult=dsnDlg.getValues();
            dsnDlg.dispose();
            dsnDlg=null;
            this.requestFocus();
            if(dsnResult!= null)
            {
                AdminWise.adminPanel.setWaitPointer();
                appendDataFromList(dsnResult);
                AdminWise.adminPanel.setDefaultPointer();
            }	
    }
    void setSelectionDbDsnInfo(DBLookup dbDSNInfo){
    	VWIndexRec selIndex=AdminWise.adminPanel.docTypePanel.IndicesTable.getRowData(
                AdminWise.adminPanel.docTypePanel.IndicesTable.getSelectedRow());
    	selIndex.setDbDSNInfo(dbDSNInfo);
    }
//------------------------------------------------------------------------------
    void BtnSort_actionPerformed(java.awt.event.ActionEvent event)
    {
	    /*
	     * Issue # 673 	: Open Adminwise->connect to room->click on new document type->select new index field.  
	     *                Then for field type choose selection.  For the index values properties choose values.  
	     *                Then click on DSN.  Choose the table name.  
	     *                Once you click on execute it takes a long time to process  	 
	     * Created By	: M.Premananth
	     * Date			: 14-Jul-2006
	     */
    	/*
        List valuesList=getValuesList();
        List sortedValuesList=new LinkedList();
        int count=valuesList.size();
        if(count==0) return;
        String value="";
        for(int i=0;i<count;i++)
        {
            value=(String)valuesList.get(i);
            value=value.toLowerCase().trim();
            if (value==null || value.equals("")) continue;
            if(sortedValuesList.size()==0)
            {
                sortedValuesList.add(valuesList.get(i));
            }
            else
            {
                boolean inserted=false;
                for(int j=0;j<sortedValuesList.size();j++)
                    if(value.compareTo(((String)sortedValuesList.get(j)).toLowerCase()) < 0)
                    {
                        sortedValuesList.add(j,valuesList.get(i));
                        inserted=true;
                        break;
                    }
                if(!inserted) sortedValuesList.add(valuesList.get(i));
            }  
        }*/
        StringBuffer values = new StringBuffer();
    	List valuesList = getValuesList();   	
        int count = valuesList.size();
        if(count==0) return;
    	Collections.sort(valuesList);
        count = valuesList.size();
        for(int i=0;i<count-1;i++){
        	/*Commented below line and added the if else criteria to avoid issue in removing the values greater than 255 while 
        	clicking the sort button Date:- 14/9/2015 CV83B2 */
        	//values.append(valuesList.get(i));/*+NewLineChar*/
        	if ((valuesList.get(i).toString().length()>=254) ){
        		 values.append(valuesList.get(i)+"\n");/*+NewLineChar*/
        	}else{
        		 values.append(valuesList.get(i));/*+NewLineChar*/
        	}
           
        }
        values.append(valuesList.get(count-1));
        TxtValues.setText(values.toString());
    }
//------------------------------------------------------------------------------
    void BtnClear_actionPerformed(java.awt.event.ActionEvent event)
    {
        TxtValues.setText("");
    }
//------------------------------------------------------------------------------
    public String getValues()
    {
        String line="";
        int lineStart=0,lineEnd=0;
        String valueStr="";
        int lineCount=TxtValues.getLineCount();
        for (int i=0;i<lineCount;i++)
        {
            try
            {
                lineStart=TxtValues.getLineStartOffset(i);
                lineEnd=TxtValues.getLineEndOffset(i);
                line=TxtValues.getText(lineStart, lineEnd-lineStart);
                if(line.equals(""))
                {
                    if(line.length()>255)    line=line.substring(0,255);
                    valueStr+=line+"|";
                }
            }
            catch(javax.swing.text.BadLocationException e){}
        }
        if(valueStr.equals("")) return valueStr;
        return (valueStr.substring(0,valueStr.length()-1));
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private void loadValues()
    {
    	int itemsCount = 0;
        VWIndexRec selIndex=AdminWise.adminPanel.docTypePanel.IndicesTable.getRowData(
            AdminWise.adminPanel.docTypePanel.IndicesTable.getSelectedRow());
       Vector strValues = selIndex.selectionValues;
       
        ///Vector strValues = VWDocTypeConnector.getSelectionValues();
        if(strValues==null || strValues.equals("")) return;
        if(strValues.size()>0)        	
        	itemsCount = strValues.size();
        for(int i=0;i<strValues.size();i++)
            TxtValues.append(((String)strValues.get(i)).trim() + NewLineChar);
        JLabel1.setText(LBL_SELECTIONINDEXVALUES_NAME + 
            " ["+String.valueOf(itemsCount)+"]");
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        setSize(292,477);
    }
//------------------------------------------------------------------------------
    public Vector getValuesList()
    {
        Vector valuesList=new Vector();
        String strValues=TxtValues.getText().trim();
        if(strValues.equals("")) return valuesList;
        int lineCount=TxtValues.getLineCount();
        String line="";
        int lineStart=0,lineEnd=0;
        for (int i=0;i<lineCount;i++)
        {
            try
            {
                lineStart=TxtValues.getLineStartOffset(i);
                lineEnd=TxtValues.getLineEndOffset(i);
                line=TxtValues.getText(lineStart, lineEnd-lineStart);
                if(!line.equals(""))
                {
                	//Value modified to 255 to 254 in CV83B2 Sort issue
                    if(line.length()>254) line=line.substring(0,254);
                    valuesList.add(line);
                }
            }
            catch(javax.swing.text.BadLocationException e){}
        }
        return valuesList;
    }
//------------------------------------------------------------------------------
    private void appendDataFromList(List dsnResult)
    {
	    /*
	     * Issue # 673 	: Open Adminwise->connect to room->click on new document type->select new index field.  
	     *                Then for field type choose selection.  For the index values properties choose values.  
	     *                Then click on DSN.  Choose the table name.  
	     *                Once you click on execute it takes a long time to process  	 
	     * Created By	: M.Premananth
	     * Date			: 14-Jul-2006
	     */
        List valuesList = getValuesList();
        StringBuffer values	= new StringBuffer();
        String item 	= "";
        TreeSet set 	= new TreeSet();
        Iterator itr = valuesList.iterator();
        while(itr.hasNext())
        {
        	item = ((String)itr.next()).trim();
        	if(set.add(item))
        		values.append(item + NewLineChar);
        }
        itr = dsnResult.iterator();
        while(itr.hasNext())
        {
        	item = ((String)itr.next()).trim();
        	if(set.add(item))
        		values.append(item + NewLineChar);
        }
        TxtValues.setText(values.toString());
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
}