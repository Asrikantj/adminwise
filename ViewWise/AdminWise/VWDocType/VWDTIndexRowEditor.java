/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWSimpleSearchRowEditor<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWDocType;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;

public class VWDTIndexRowEditor implements TableCellEditor, VWConstant{
  
  protected TableCellEditor editor,defaultEditor;
  protected VWDTIndexTable gTable=null;
 //------------------------------------------------------------------------------
  public VWDTIndexRowEditor(VWDTIndexTable table){
    JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;
  }
//------------------------------------------------------------------------------
  public Component getTableCellEditorComponent(JTable table,
      Object value, boolean isSelected, int row, int column){    
    
    if (column ==1)
    {
        editor=new DefaultCellEditor(new VWComboBox(VWIndexTypes));
    }
    else if (column ==2)
    {
        /*
        if(AdminWise.adminPanel.docTypePanel.getSelDTIndexUsed(row))
            editor=new DefaultCellEditor(new VWComboBox(VWConstant.VWIndexFieldRequired));
        else
         */
            editor=new DefaultCellEditor(new VWComboBox(VWConstant.VWIndexFieldType));
    }
    else
    {
        editor=defaultEditor;
    }
    return editor.getTableCellEditorComponent(table,value,isSelected,row,column);
  }
//------------------------------------------------------------------------------
  public Object getCellEditorValue(){
        return editor.getCellEditorValue();
  }
//------------------------------------------------------------------------------
  public boolean stopCellEditing(){
    return editor.stopCellEditing();
  }
//------------------------------------------------------------------------------
  public void cancelCellEditing(){
    editor.cancelCellEditing();
  }
//------------------------------------------------------------------------------
	/*Method modified to make the key field editable for update key field mode
	 * Release :Build5-001
	 *Modified By Madhavan
	 */
  public boolean isCellEditable(EventObject anEvent){
    int mode=AdminWise.adminPanel.docTypePanel.getCurMode();
    if(mode!=MODE_NEW && mode!=MODE_UPDATE&& mode!=MODE_UPDATE_KEY && mode!=MODE_NEWINDEX) return false;
    int column=0,row=0;
    VWIndexRec selIndex=null;
    if (anEvent instanceof MouseEvent)
    {
	Point point=((MouseEvent)anEvent).getPoint();
        column = gTable.columnAtPoint(point);
        row = gTable.rowAtPoint(point);
        selIndex = gTable.getRowData(row);
    }
    else
    {
        row = gTable.getSelectedRow();
        selIndex = gTable.getRowData(row);
    }
    if(AdminWise.adminPanel.docTypePanel.getIsFixedIndex(row)) return false;
    boolean dtUsed= AdminWise.adminPanel.docTypePanel.getSelDTIndexUsed(row);
    boolean indexUsed= AdminWise.adminPanel.docTypePanel.getSelIndexUsed(row);
  
    if((column==0 && !indexUsed)&&( mode!=MODE_UPDATE_KEY))
        return true;
    else if ((column==1 && !indexUsed && (!dtUsed || selIndex.getDTIndexId()==0))&&(mode!=MODE_UPDATE_KEY))
        return true;
    else if (column==2 && !selIndex.getKey() && mode!=MODE_NEWINDEX)
        return true;
    else if (column==2  && mode==MODE_UPDATE_KEY)
    	return true;
    else if (column==3)
        return true;
    return false;
  }
//------------------------------------------------------------------------------
  public void addCellEditorListener(CellEditorListener l){
    editor.addCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public void removeCellEditorListener(CellEditorListener l){
    editor.removeCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public boolean shouldSelectCell(EventObject anEvent){
    return editor.shouldSelectCell(anEvent);
  }
//------------------------------------------------------------------------------
}
  