/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWDocType;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.ListSelectionModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import ViewWise.AdminWise.VWTable.*;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMask.VWMaskField;
import com.computhink.common.Index;

//------------------------------------------------------------------------------
public class VWIndexPropTable extends JTable implements VWConstant
{
    protected VWIndexPropTableData m_data;    
    private int indexType=-1;    
    public VWIndexPropTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWIndexPropTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWIndexPropTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        ColoredTableCellRenderer textRenderer=new ColoredTableCellRenderer();
        ///DefaultTableCellRenderer textRenderer =new DefaultTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWIndexPropTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWIndexPropRowEditor editor=new VWIndexPropRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWIndexPropTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                {
                }       
                else
                {
                }
            }
        });
        setRowHeight(TableRowHeight);
        
        setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if (object instanceof JTable)
            if(event.getModifiers()==event.BUTTON3_MASK)
            VWIndexPropTable_RightMouseClicked(event);
            else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWIndexPropTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWIndexPropTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWIndexPropTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public String[] getValues()
  {
        return m_data.getValues();
  }
  //------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new IndexPropRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowIndexPropValue(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[1]);
  }
//------------------------------------------------------------------------------
  public void updateRowData(int rowNum,String[] data)
  {
        m_data.updateRowData(rowNum,data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void updateRowValue(int rowNum,String value)
  {
        m_data.updateRowValue(rowNum,value);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public int getIndexType()
  {
      return indexType;
  }
//------------------------------------------------------------------------------
  public void setIndexType(int type)
  {
      indexType=type;
  }
//------------------------------------------------------------------------------
  public void ResetDefaultValue(int row)
  {
      m_data.ResetLastRow(row);
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class IndexPropRowData
{
  public String   m_PropName;
  public String   m_PropValue;
  
  public IndexPropRowData() {
    m_PropName = "";
    m_PropValue = "";
  }
//------------------------------------------------------------------------------
  public IndexPropRowData(String IndexPropName,String IndexPropPath) 
  {
    m_PropName = IndexPropName;
    m_PropValue = IndexPropPath;
  }
//------------------------------------------------------------------------------
  public IndexPropRowData(String str)
  {
    VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
    try
    {
        m_PropName = tokens.nextToken();
    }
    catch(Exception e){m_PropName="<Unknown name>";}
    try
    {
        m_PropValue = tokens.nextToken();
    }
    catch(Exception e){m_PropValue="<Unknown value>";}
  }
//------------------------------------------------------------------------------
  public IndexPropRowData(String[] data) 
  {
    int i=0;
    m_PropName = data[i++];
    m_PropValue = data[i++];
  }
}
//------------------------------------------------------------------------------
class IndexPropColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public IndexPropColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWIndexPropTableData extends AbstractTableModel 
{
  public static final IndexPropColumnData m_columns[] = {
    new IndexPropColumnData(VWConstant.IndexPropColumnNames[0],0.3F,JLabel.LEFT ),
    new IndexPropColumnData(VWConstant.IndexPropColumnNames[1],0.7F, JLabel.LEFT),
  };
  public static final int COL_PROPNAME = 0;
  public static final int COL_PROPVALUE = 1;

  protected VWIndexPropTable m_parent;
  protected Vector m_vector;

  public VWIndexPropTableData(VWIndexPropTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(new IndexPropRowData((String)data.get(i)));
    }
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        IndexPropRowData row =new IndexPropRowData(data[i]); 
        m_vector.addElement(row);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][2];
    for(int i=0;i<count;i++)
    {
        IndexPropRowData row=(IndexPropRowData)m_vector.elementAt(i);
        data[i][0]=row.m_PropName;
        data[i][1]=row.m_PropValue;
    }
    return data;
  }
//------------------------------------------------------------------------------
public String[] getValues() {
    int count=getRowCount();
    String[] data=new String[count];
    for(int i=0;i<count;i++)
    {
        IndexPropRowData row=(IndexPropRowData)m_vector.elementAt(i);
        data[i]=row.m_PropValue;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] IndexPropRowData=new String[2];
    IndexPropRowData row=(IndexPropRowData)m_vector.elementAt(rowNum);
    IndexPropRowData[0]=row.m_PropName;
    IndexPropRowData[1]=row.m_PropValue;
    return IndexPropRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    IndexPropRowData row = (IndexPropRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_PROPNAME: return row.m_PropName;
      case COL_PROPVALUE: return row.m_PropValue;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    IndexPropRowData row = (IndexPropRowData)m_vector.elementAt(nRow);
    /**
	 * Code added - null check added for the issue no. PVW 6.10167 - on click of
	 * none in the calendar it is hanging the table. Added By: Vijaypriya.B.K on 30 Dec 2008
	 */
    String svalue = "";
    if (value != null) {
		svalue = value.toString();
	}
    switch (nCol) {
      case COL_PROPNAME:
        row.m_PropName=svalue;
        break;
      case COL_PROPVALUE:
        if(nRow==0 && AdminWise.adminPanel.docTypePanel.PropTable.getIndexType()==Index.IDX_TEXT)
        {
            svalue=VWUtil.removeCharFromString(svalue,"~");
        }
        if(nRow==2 && AdminWise.adminPanel.docTypePanel.PropTable.getIndexType()==Index.IDX_DATE)
        {
            if( svalue.equalsIgnoreCase(VWConstant.DateValueTypes[0]) ||
                svalue.equalsIgnoreCase(VWConstant.DateValueTypes[2]))
            updateRowValue(3,"");
        }
        if(nRow==0 && AdminWise.adminPanel.docTypePanel.PropTable.getIndexType()==Index.IDX_NUMBER)
        {
              for(int i=0;i<VWConstant.VWIndexNumericMask.length;i++)
              {
                  if(svalue.equalsIgnoreCase(VWConstant.VWIndexNumericMask[i]))
                  {
                        String[][] data = AdminWise.adminPanel.docTypePanel.PropTable.getData();
                        int newDataLength=VWConstant.IndexNumberFields[i].length;
                        String[][] newData=new String[newDataLength][2];
                        for(int j=0;j<newDataLength;j++)
                        {
                            newData[j][0]=VWConstant.IndexNumberFields[i][j];
                            newData[j][1]="";                            
                            for(int k=1;k<data.length-1;k++)
                            {
                                if(newData[j][0].equalsIgnoreCase(data[k][0]))
                                {
                                    newData[j][1]=data[k][1];
                                    break;
                                }
                            }
                            if(newData[j][1].equals("") && 
                            (newData[j][0].equalsIgnoreCase(VWConstant.IndexNumberFields[1][2]) || 
                             newData[j][0].equalsIgnoreCase(VWConstant.IndexNumberFields[1][3])||
                             newData[j][0].equalsIgnoreCase(VWConstant.IndexNumberFields[2][3])||
                             newData[j][0].equalsIgnoreCase(VWConstant.IndexNumberFields[2][4])))
                                newData[j][1]=VWConstant.YesNoData[1];
                             if(newData[j][1].equals("") &&
                              newData[j][0].equalsIgnoreCase(VWConstant.IndexNumberFields[2][2]))
                              newData[j][1]=(String)VWMaskField.getCurrencySymbols().get(0);
                        }
                        newData[0][1]=svalue;
                        AdminWise.adminPanel.docTypePanel.PropTable.addData(newData);
                  }
              }
        }
        if(!AdminWise.adminPanel.docTypePanel.PropTable.getRowData(nRow)[1].equalsIgnoreCase(svalue))
            AdminWise.adminPanel.docTypePanel.PropTable.ResetDefaultValue(nRow);
         row.m_PropValue=svalue;
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void ResetLastRow(int curRow)
    {
    	/**
    	 * CV10.1 Enhancement: Condition added for adding multiselect property in Selection Index
    	 */
    		int index=m_vector.size()-3;
    		AdminWise.printToConsole("index ResetLastRow ::::"+index);
    		if(curRow>=index) return;
    		IndexPropRowData row = (IndexPropRowData)m_vector.elementAt(index+2);
    		//Condition added to fix the issue in resetting the multiselect when readonly property is change
    		if(m_parent.getIndexType() != 4){
    		row.m_PropValue="";
    		}
    	
    }
//------------------------------------------------------------------------------
    public void insert(IndexPropRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
    m_vector.remove(row);
    return true;
  }
//------------------------------------------------------------------------------
    public void updateRowData(int rowNum,String[] data) 
    {
        IndexPropRowData row=(IndexPropRowData)m_vector.elementAt(rowNum);
        row.m_PropName=data[0];
        row.m_PropValue=data[1];
  }
//------------------------------------------------------------------------------
    public void updateRowValue(int rowNum,String value) 
    {
        IndexPropRowData row=(IndexPropRowData)m_vector.elementAt(rowNum);
        row.m_PropValue=value;
  }
//------------------------------------------------------------------------------
}