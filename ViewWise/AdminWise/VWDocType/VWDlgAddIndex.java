/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWDocType;

import java.awt.*;
import javax.swing.*;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.*;
import ViewWise.AdminWise.VWConstant;

public class VWDlgAddIndex extends javax.swing.JDialog implements VWConstant
{
	public VWDlgAddIndex(Frame parent,VWIndexRec[] exceptIndices) 
	{
            super(parent);
            ///parent.setIconImage(VWImages.AddIcon.getImage());
            getContentPane().setBackground(AdminWise.getAWColor());
            setTitle(LBL_ADDINDEX_NAME);
            setModal(true);
            getContentPane().setLayout(null);
            setSize(283,438);
            setVisible(false);
            JLabel1.setText(LBL_LISTINDEX_NAME);
            getContentPane().add(JLabel1);
            JLabel1.setBounds(6,4,194, AdminWise.gBtnHeight);
            getContentPane().add(LstIndices);
            LstIndices.setBounds(4,28,276,366);
            BtnAdd.setText(BTN_ADD_NAME);
            getContentPane().add(BtnAdd);
            String localeLanguage=AdminWise.connectorManager.getLocale().toString();
            if(localeLanguage.equals("nl_NL")){
            	 BtnAdd.setBounds(100,410,72+16, AdminWise.gBtnHeight);
            }else
            BtnAdd.setBounds(136,410,72, AdminWise.gBtnHeight);
//            BtnAdd.setBackground(java.awt.Color.white);
            BtnAdd.setIcon(VWImages.OkIcon);
            BtnDelete.setText(BTN_DEL_NAME);
            BtnDelete.setIcon(VWImages.DelIcon);
            getContentPane().add(BtnDelete);
            if(localeLanguage.equals("nl_NL")){
            BtnDelete.setBounds(4,410,72+8+15, AdminWise.gBtnHeight);
            }else
             BtnDelete.setBounds(4,410,72, AdminWise.gBtnHeight);
//            BtnDelete.setBackground(java.awt.Color.white);
            BtnDelete.setEnabled(false);
            BtnCancel.setText(BTN_CANCEL_NAME);
            getContentPane().add(BtnCancel);
            if(localeLanguage.equals("nl_NL")){
            BtnCancel.setBounds(192,410,72+16, AdminWise.gBtnHeight);
            }else{
            BtnCancel.setBounds(208,410,72, AdminWise.gBtnHeight);
            }
//            BtnCancel.setBackground(java.awt.Color.white);
            BtnCancel.setIcon(VWImages.CloseIcon);
            lExceptIndices=exceptIndices;
            try{
                loadedList=false;
                LstIndices.loadData(loadIndices(lExceptIndices));
                loadedList=true;
            }
            catch(Exception e){loadedList=true;}
            SymAction lSymAction = new SymAction();
            BtnAdd.addActionListener(lSymAction);
            BtnDelete.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            SymKey aSymKey = new SymKey();
            addKeyListener(aSymKey);
            LstIndices.addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            this.addWindowListener(aSymWindow);
            setResizable(false);
            LstIndices.getList().getSelectionModel().addListSelectionListener(
                new VWListSelectionListener());
            LstIndices.addVWEventListener(new VWCheckListListener() {
            public void VWItemChecked(VWItemCheckEvent event)
            {
                if(!loadedList) return;
                if(event.getItemIndex()==0)
                    LstIndices.selectAllItems();
                else
                if(!LstIndices.getItemIsCheck(LstIndices.getList().getSelectedIndex()))
                    LstIndices.setItemCheck(0,false);
            }
            public void VWItemUnchecked(VWItemCheckEvent event)
            {
                if(!loadedList) return;
                if(event.getItemIndex()==0)
                    LstIndices.deSelectAllItems();
                if(!LstIndices.getItemIsCheck(LstIndices.getList().getSelectedIndex()))
                    LstIndices.setItemCheck(0,false);
            }
        });
        
            getDlgOptions();
            setVisible(true);    
}
//------------------------------------------------------------------------------     
    class VWListSelectionListener implements javax.swing.event.ListSelectionListener
    {
        public void valueChanged(javax.swing.event.ListSelectionEvent e)
        {
            if(!loadedList) return;
            VWIndexRec selItem= (VWIndexRec)LstIndices.getList().getSelectedValue();
            if(selItem==null || selItem.getIndexUsed()!=0 || selItem.getId()<=0)
                BtnDelete.setEnabled(false);
            else
                BtnDelete.setEnabled(true);
        }         
    }
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgAddIndex.this)
                Dialog_windowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnAdd_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnAdd)
                BtnAdd_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
            else if (object == BtnDelete)
                BtnDelete_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
	public void setVisible(boolean b)
	{
            if (b) 
            {
                Dimension d =VWUtil.getScreenSize();
                setLocation(d.width/4,d.height/4);
            }
            super.setVisible(b);
	}
//------------------------------------------------------------------------------
	public void addNotify()
	{
            // Record the size of the window prior to calling parents addNotify.
            Dimension size = getSize();

            super.addNotify();

            if (frameSizeAdjusted)
                    return;
            frameSizeAdjusted = true;

            // Adjust size of frame according to the insets
            Insets insets = getInsets();
            setSize(insets.left + insets.right + size.width, insets.top + insets.bottom + size.height);
	}
//------------------------------------------------------------------------------
	boolean frameSizeAdjusted = false;
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	VWCheckList LstIndices = new VWCheckList();
	VWLinkedButton BtnAdd = new VWLinkedButton(0,true);
	VWLinkedButton BtnDelete = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnAdd_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnDelete_actionPerformed(java.awt.event.ActionEvent event)
    {
        VWIndexRec selItem= (VWIndexRec)LstIndices.getList().getSelectedValue();
        if(selItem==null) return;
        if(AdminWise.adminPanel.confirmDelIndex)
        if(VWMessage.showConfirmDialog((java.awt.Component) this,
            MSG_DELETEINDEX + " (" + selItem.getName() +")?"
            )!=javax.swing.JOptionPane.YES_OPTION) return;
        if(VWDocTypeConnector.removeIndex(selItem)>0)
        {
            try{
                loadedList=false;
                LstIndices.loadData(loadIndices(lExceptIndices));
                loadedList=true;
            }
            catch(Exception e){loadedList=true;}
        }
         
    }
//------------------------------------------------------------------------------
    public List getValues()
    {
        List selIndices=new LinkedList();
        int count=LstIndices.getItemsCount();
        for(int i=1;i<count;i++)
            if(LstIndices.getItemIsCheck(i))
                selIndices.add(LstIndices.getItem(i));
        return selIndices;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private Vector loadIndices(VWIndexRec[] exceptIndices)
    {
        Vector OtherIndices=new Vector();
        VWIndexRec[] indices=VWDocTypeConnector.getIndices(false);
        if(indices==null || indices.length==0) return null;
        int count=indices.length;
        if(exceptIndices!=null && exceptIndices.length>0)
        {
            for(int i=0;i<count;i++)
            {
                if(!isIndexAlreadyIn(exceptIndices,indices[i])) 
                    OtherIndices.add(indices[i]);
            } 
        }   
        else
        {
            for(int i=0;i<count;i++)
            { 
               OtherIndices.add(indices[i]);
            }   
        }
        OtherIndices.add(0,new VWIndexRec(0,"<All Indices>"));
        return OtherIndices;
    }
//------------------------------------------------------------------------------
    private boolean isIndexAlreadyIn(VWIndexRec[] exceptIndices,VWIndexRec index)
    {
        int count=exceptIndices.length;
        for(int j=0;j<count;j++)
        {
            if(exceptIndices[j].getId()==index.getId()) return true;
        }
        return false;
}
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
VWIndexRec[] lExceptIndices=null;
boolean loadedList=true;
}