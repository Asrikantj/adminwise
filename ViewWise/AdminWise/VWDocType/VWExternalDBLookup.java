package ViewWise.AdminWise.VWDocType;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWTable.VWIndexMappingTable;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

import java.util.Hashtable;
import java.util.Vector;
import java.util.prefs.*;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;

import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.computhink.common.Constants;
import com.computhink.common.DBLookup;
import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;

public class VWExternalDBLookup extends javax.swing.JDialog implements VWConstant, Constants
{
    /**
     * Default serialVersionId. 
     */
    //private static final long serialVersionUID = 1L;

    public VWExternalDBLookup(Frame dialog, String title, VWDocTypeRec selDocType)
    {
	super(dialog);
	setTitle(AdminWise.connectorManager.getString("externalDBLookup.title")+" "+selDocType.getName());
	setModal(true);	
	initComponents();	
	//PR Comment
	loadData(selDocType);
	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	setEnableMode(false);	
	setResizable(false);
	setVisible(true);
    }

    public void initComponents(){
    getContentPane().setBackground(AdminWise.getAWColor());  
 	getContentPane().setLayout(null);
	setSize(544,564);
	 String languageLocale=AdminWise.getLocaleLanguage();
	TitledBorder connectionBorder = new TitledBorder(lbl_Border_Connection);
	connectionBorder.setTitleColor(Color.BLUE);
	PnlConnection.setBorder(connectionBorder);
	PnlConnection.setBackground(AdminWise.getAWColor());  
	PnlConnection.setLayout(null);
	getContentPane().add(PnlConnection);
	PnlConnection.setBounds(10, 6, 518, 211);
	int left = 15, width = 408, top = 19;
	JLabel1.setText(lbl_DSNName);
	PnlConnection.add(JLabel1);
	JLabel1.setBounds(left, top, 78, AdminWise.gBtnHeight);
	
	PnlConnection.add(TxtDSNName);
	if(languageLocale.equals("nl_NL")){
		TxtDSNName.setBounds(95+10, top, width-10, 20);
	}else
	TxtDSNName.setBounds(95, top, width, 20);
	TxtDSNName.setName(lbl_DSNName);
	TxtDSNName.getDocument().addDocumentListener(new DocListener());
	
	top += 31;
	JLabel2.setText(lbl_UserName);
	PnlConnection.add(JLabel2);
	if(languageLocale.equals("nl_NL")){
		JLabel2.setBounds(left, top, 78+12, AdminWise.gBtnHeight);
	}else
		JLabel2.setBounds(left, top, 78, AdminWise.gBtnHeight);
	
	PnlConnection.add(TxtUserName);
	if(languageLocale.equals("nl_NL")){
		TxtUserName.setBounds(95+10, top, width-10, 20);	
	}else
	TxtUserName.setBounds(95, top, width, 20);
	TxtUserName.setName(lbl_UserName);
	TxtUserName.getDocument().addDocumentListener(new DocListener());

	top += 31;
	JLabel3.setText(lbl_Password);
	PnlConnection.add(JLabel3);
	JLabel3.setBounds(left, top, 78, AdminWise.gBtnHeight);

	PnlConnection.add(TxtPassword);
	if(languageLocale.equals("nl_NL")){
		TxtPassword.setBounds(95+10, top, width-10, 20);
	}else
	TxtPassword.setBounds(95, top, width, 20);
	TxtPassword.setName(lbl_Password);
	TxtPassword.getDocument().addDocumentListener(new DocListener());
	
	top += 31;
	JLabel4.setText(lbl_Mapping);
	PnlConnection.add(JLabel4);
	JLabel4.setBounds(left, top, 78, 28);

	PnlConnection.add(TxtMapTableName);
	if(languageLocale.equals("nl_NL")){
		TxtMapTableName.setBounds(95+10, top, width-10, 20);	
	}else
	TxtMapTableName.setBounds(95, top, width, 20);
	TxtMapTableName.setName(lbl_Mapping);
	TxtMapTableName.getDocument().addDocumentListener(new DocListener());

	top += 31;
	lblDSNQuery.setText(lbl_Query);
	PnlConnection.add(lblDSNQuery);
	lblDSNQuery.setBounds(left, top, 78, 28);

	PnlConnection.add(txtDSNQuery);
	if(languageLocale.equals("nl_NL")){
		txtDSNQuery.setBounds(95+10, top, width-10, 20);	
	}else
	txtDSNQuery.setBounds(95, top, width, 20);
	txtDSNQuery.setName(lbl_Query);
	txtDSNQuery.getDocument().addDocumentListener(new DocListener());
	
	
/*	cmbDSNStatus.setText(CHK_DSN_STATUS);	
	getContentPane().add(cmbDSNStatus);
	cmbDSNStatus.setBounds(12,136,105,25);
	cmbDSNStatus.setActionCommand(CHK_DSN_STATUS);
	//BtnConnect.setIcon(VWImages.ConnectIcon);
	PnlConnection.add(cmbDSNStatus);
	cmbDSNStatus.setSelected(true);
	cmbDSNStatus.setRolloverEnabled(false);
	cmbDSNStatus.setHorizontalTextPosition(JCheckBox.LEFT);
	cmbDSNStatus.setIconTextGap(28);
*/
	top += 31;
	
	lblDSNStatus.setText(DSN_STATUS);
	getContentPane().add(lblDSNStatus);
	lblDSNStatus.setBounds(left, top, 78, 25);	
	PnlConnection.add(lblDSNStatus);
	
	
	cmbDSNStatus.addItem(AdminWise.connectorManager.getString("externalDBLookup.DSN_STATUS_Disabled"));	
	cmbDSNStatus.addItem(AdminWise.connectorManager.getString("externalDBLookup.DSN_STATUS_Enable"));	
	getContentPane().add(cmbDSNStatus);
	if(languageLocale.equals("nl_NL")){
		cmbDSNStatus.setBounds(95+10, top, 70-10, 21);
	}else
	cmbDSNStatus.setBounds(95, top, 70, 21);
	//cmbDSNStatus.setActionCommand(DSN_STATUS);
	//BtnConnect.setIcon(VWImages.ConnectIcon);
	PnlConnection.add(cmbDSNStatus);
	cmbDSNStatus.setSelectedIndex(1);
	
	BtnConnect.setText(BTN_CONNECT_NAME);
	BtnConnect.setActionCommand(BTN_CONNECT_NAME);
	getContentPane().add(BtnConnect);
	 if(languageLocale.equals("nl_NL")){
	BtnConnect.setBounds(420-15, top, 83+15, 25);//(260, top, 73, 25);
	 }else
		 BtnConnect.setBounds(420, top, 83, 25);
	BtnConnect.setIcon(VWImages.ConnectIcon);
	PnlConnection.add(BtnConnect);

	// This is for Configuration frame.	
	TitledBorder configBorder = new TitledBorder(lbl_Border_Config);
	configBorder.setTitleColor(Color.BLUE);
	PnlConfig.setBorder(configBorder);
	PnlConfig.setBackground(AdminWise.getAWColor());
	PnlConfig.setLayout(null);
	getContentPane().add(PnlConfig);
	PnlConfig.setBounds(10, 220, 518, 270);

	/*JLabel5.setText(lbl_Trigger);
	PnlConfig.add(JLabel5);
	JLabel5.setBounds(left, 19, 75, 28);

	PnlConfig.add(CmbTriggerName);	
	CmbTriggerName.setBounds(95, 24, width, 20);
	
	JLabel6.setText(lbl_TriggerType);
	PnlConfig.add(JLabel6);
	JLabel6.setBounds(left, 54, 72, 28);

	PnlConfig.add(CmbTriggerType);
	CmbTriggerType.setBounds(95, 56, width, AdminWise.gSmallBtnHeight);
*/

	BtnVerify.setText(BTN_VERIFY_NAME);
	BtnVerify.setActionCommand(BTN_VERIFY_NAME);
	getContentPane().add(BtnVerify);
	 if(languageLocale.equals("nl_NL")){
		 BtnVerify.setBounds(12,498,73+15,25);	
	 }else
		 BtnVerify.setBounds(12,498,73,25);	
	 
	BtnVerify.setIcon(VWImages.OkIcon);
	getContentPane().add(BtnVerify);

	BtnGenerateQuery.setText(BTN_GENERATE_NAME);
	BtnGenerateQuery.setActionCommand(BTN_GENERATE_NAME);
	 if(languageLocale.equals("nl_NL")){
		 BtnGenerateQuery.setBounds(280-45, 498, 93+15, 25);
	 }else
		 BtnGenerateQuery.setBounds(280, 498, 93, 25);
	getContentPane().add(BtnGenerateQuery);

	BtnSave.setText(BTN_SAVE_NAME);
	BtnSave.setActionCommand(BTN_SAVE_NAME);
	getContentPane().add(BtnSave);
	 if(languageLocale.equals("nl_NL")){
		 BtnSave.setBounds(376-30,498,73+15,25);
	 }else
		 BtnSave.setBounds(376,498,73,25);
	BtnSave.setIcon(VWImages.SaveIcon);
	getContentPane().add(BtnSave);

	BtnClose.setText(BTN_CLOSE_NAME);
	BtnClose.setActionCommand(BTN_CLOSE_NAME);
	getContentPane().add(BtnClose);
	 if(languageLocale.equals("nl_NL")){
	BtnClose.setBounds(452-15, 498, 73+15, 25);
	 }else
		 BtnClose.setBounds(452, 498, 73, 25); 
		 
	BtnClose.setIcon(VWImages.CloseIcon);
	getContentPane().add(BtnClose);



	table = new VWIndexMappingTable(this);	 
	scrollPane = new JScrollPane(table);
	scrollPane.getViewport().setBackground(AdminWise.getAWColor());
	scrollPane.setBounds(left, 20, WIDTH, HEIGHT);
	PnlConfig.add(scrollPane);
	table.addPropertyChangeListener(new PropertyChangeListener(){
	    public void propertyChange(PropertyChangeEvent evt){		
		if (evt.getPropertyName().indexOf("tableCellEditor") != -1){
			isTablePropchanged = true;
		    BtnSave.setEnabled(true);
		    BtnVerify.setEnabled(false);
		}
	    }
	});
	
	txtareaConfigQuery.setText("");
	txtareaConfigQuery.getDocument().addDocumentListener(new DocListener());
	SPConfigQuery = new JScrollPane(txtareaConfigQuery);
	SPConfigQuery.setBounds(left, 157, WIDTH, 100);
	PnlConfig.add(SPConfigQuery);
	
	

	//{{REGISTER_LISTENERS
	SymAction lSymAction = new SymAction();
	BtnClose.addActionListener(lSymAction);
	BtnSave.addActionListener(lSymAction);
	BtnReset.addActionListener(lSymAction);
	BtnConnect.addActionListener(lSymAction);
	BtnVerify.addActionListener(lSymAction);
	BtnGenerateQuery.addActionListener(lSymAction);
	//cmbDSNStatus.addActionListener(lSymAction);
	SymKey aSymKey = new SymKey();

	addKeyListener(aSymKey);

	TxtDSNName.addKeyListener(aSymKey);
	TxtMapTableName.addKeyListener(aSymKey);
	TxtUserName.addKeyListener(aSymKey);
	TxtPassword.addKeyListener(aSymKey);
	BtnSave.addKeyListener(aSymKey);
	BtnReset.addKeyListener(aSymKey);
	BtnConnect.addKeyListener(aSymKey);	
	
	ItemAction action = new ItemAction();
	cmbDSNStatus.addItemListener(action);
	/*CmbTriggerName.addItemListener(action);
	CmbTriggerType.addItemListener(action);*/
	
	TxtMapTableName.addFocusListener(new FocusAdapter(){
	    public void focusLost(FocusEvent e) {
		    createDSNQuery();
	    }
	});
    }

    public void loadData(VWDocTypeRec selDocTypeRec){
	this.doctypeRecord = selDocTypeRec;
	docTypeId = selDocTypeRec.getId();
	
	loadDocTypeIndices(docTypeId);	
	table.addData(indices);
	loadDSNInfo(docTypeId);
	loadIndexMapping(docTypeId);
	if (TxtDSNName.getText().trim().length() > 0 ) BtnVerify.setEnabled(true);
	getDlgOptions();
    }

    public void loadDocTypeIndices(int docTypeId){
	indices = new Vector();
	indices.removeAllElements();
	indices.addAll(VWDocTypeConnector.getDTIndices(docTypeId));
	//CmbTriggerName.removeAllItems();
	for (int count = 0; count < indices.size(); count++) {
	    VWIndexRec idx = (VWIndexRec)indices.get(count);
	    if (/*idx.getType() != Index.IDX_SELECTION &&*/ idx.getType() != Index.IDX_SEQUENCE && idx.getType() != Index.IDX_BOOLEAN && (!idx.getReadOnly() ||(idx.getReadOnly() && idx.getType() == Index.IDX_SELECTION ))){
		if (idx.getType() == Index.IDX_DATE && idx.getInfo().trim().endsWith("1")) continue;
		//CmbTriggerName.addItem(indices.get(count));
	    }
	}

	String dataType[] = {AdminWise.connectorManager.getString("externalDBLookup.dataType_0"), AdminWise.connectorManager.getString("externalDBLookup.dataType_1"),
			AdminWise.connectorManager.getString("externalDBLookup.dataType_2"), AdminWise.connectorManager.getString("externalDBLookup.dataType_3")};
	//CmbTriggerType.removeAllItems();
	/*for(int count=0;count<dataType.length; count++){
	    CmbTriggerType.addItem(dataType[count]);	
	}*/

    }

    public void loadDSNInfo(int docTypeId){
	//"VWSQLDSN	sa	sa	[Sheet1$]"
	Vector dsnDetails = new Vector();
	VWExternalLookupConnector.loadDSNInfo(docTypeId, dsnDetails);
	if (dsnDetails != null && dsnDetails.size() > 0){
	    try{
		DBLookup data = (DBLookup) dsnDetails.get(0);
		setTextValue(TxtDSNName, data.getDsnName());		
		setTextValue(TxtMapTableName, data.getExternalTable());
		setTextValue(TxtUserName, data.getUserName());
		setTextValue(TxtPassword, data.getPassWord());//TxtPassword.setText(tokens.nextToken().trim());
		cmbDSNStatus.setSelectedIndex(data.getDsnStatus());
		txtareaConfigQuery.setText(data.getConfigQuery() == null ? "" : data.getConfigQuery());
		txtareaConfigQuery.setCaretPosition(0);
		
		if (txtDSNQuery.getText().trim().length() == 0 && TxtMapTableName.getText().trim().length() > 0){
		    createDSNQuery();
		}
	    }catch(Exception ex){

	    }

	}
    }

    public void loadIndexMapping(int docTypeId) {
	try{
	DocType docType = new DocType(docTypeId);
	docType.setName(doctypeRecord.getName());
	VWExternalLookupConnector.loadIndexMapping(docType);	
	table.insertData(docType);
	/*int count = CmbTriggerName.getItemCount();
	for (int i = 0; i < count; i++){
	    VWIndexRec idx = (VWIndexRec) CmbTriggerName.getItemAt(i);
	    if (idx.getId() == docType.getTriggerIndexId()){
		CmbTriggerName.setSelectedItem(idx);
		break;
	    }	    
	}
	CmbTriggerType.setSelectedIndex(docType.getTriggerIndexDataType());*/
	}catch(Exception Ex){
	    System.out.println("Errrr " + Ex.getMessage());
	    //Ex.printStackTrace();
	}
    }

    public void createDSNQuery(){
	if (txtDSNQuery.getText().trim().length() > 0 ) return;
	String query = "select * from " + TxtMapTableName.getText() + " where 1 = 0";
	txtDSNQuery.setText(query);
    }
    public void setTextValue(JTextField field , String value){
	if (value == null) value = "";
	if (value != null ) value = value.trim();
	field.setText(value);
    }

//  ------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
	public void keyTyped(java.awt.event.KeyEvent event)
	{
	    Object object = event.getSource();
	    if(event.getKeyText(event.getKeyChar()).equals("Enter"))
	    {
		BtnSave.requestFocus();
		BtnSave_actionPerformed(null);
	    }
	    else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
	    {
		BtnReset.requestFocus();
		BtnReset_actionPerformed(null);
	    }
	}
    }
    public class DocListener implements DocumentListener
    {
        private JComponent component;

        public DocListener()
        {            
        }
        public void changedUpdate(DocumentEvent e)
        {
           //component.setEnabled(true);
            BtnSave.setEnabled(true);
            BtnVerify.setEnabled(false);
           
        } 
        public void insertUpdate(DocumentEvent e)
        {
            BtnSave.setEnabled(true);
            BtnVerify.setEnabled(false);

        } 
        public void removeUpdate(DocumentEvent e)
        {
            BtnSave.setEnabled(true);
            BtnVerify.setEnabled(false);

        }
    }
    
//  ------------------------------------------------------------------------------
    class ItemAction implements java.awt.event.ItemListener
    {	
	public void itemStateChanged(ItemEvent e){
	    BtnSave.setEnabled(true);
	    BtnVerify.setEnabled(false);
	}
    }
    class SymAction implements java.awt.event.ActionListener
    {
	public void actionPerformed(java.awt.event.ActionEvent event)
	{
		Object object = event.getSource();
		if (object == BtnSave)
			BtnSave_actionPerformed(event);
		else if (object == BtnReset)
			BtnReset_actionPerformed(event);		
		else if (object == BtnConnect)
			BtnTest_actionPerformed(event);
		else if (object == BtnVerify)
			BtnVerify_actionPerformed(event);
		else if (object == BtnClose)
			BtnClose_actionPerformed(event);
		else if (object == BtnGenerateQuery)
			BtnGenerateQuery_actionPerformed(event);
	}
    }

//  ------------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
    	if(table.isEditing()){
    		table.getCellEditor().stopCellEditing();
    	}

    	if (TxtDSNName.getText().trim().length() == 0) {
    		JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_0"),ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("externalDBLookup.msg_1"), JOptionPane.OK_OPTION);
    		return;
    	}
    	if (TxtMapTableName.getText().trim().length() == 0) {
    		JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_2"), ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ AdminWise.connectorManager.getString("externalDBLookup.msg_1"), JOptionPane.OK_OPTION);
    		return;
    	}
    	Vector mappedIndices = table.getData();
    	/*int triggerIndexId = ((VWIndexRec)CmbTriggerName.getSelectedItem()).getId();
		int triggerIndexDataType = CmbTriggerType.getSelectedIndex();*/

    	int triggerCount = 0;
    	for (int cnt = 0; cnt < mappedIndices.size(); cnt++){
    		Index idx = (Index) mappedIndices.get(cnt);
    		if (idx.isTriggerEnabled()){
    			triggerCount++;
    			if (idx.getExternalColumn().trim().equals("")){
    				JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_3") + idx.getName() + ">", ResourceManager.getDefaultManager().getString("CVProduct.Name") + " "+AdminWise.connectorManager.getString("externalDBLookup.msg_1"), JOptionPane.OK_OPTION);
    				return;
    			}
    		}
    	}
    	if(triggerCount == 0){
    		JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_4"), ResourceManager.getDefaultManager().getString("CVProduct.Name") + " "+AdminWise.connectorManager.getString("externalDBLookup.msg_1"), JOptionPane.OK_OPTION);
    		return;
    	}

    	String query = txtareaConfigQuery.getText();
    	if(query == null || query.trim().length() == 0){
    		query = generateQuery();
    		if(query != null && query.trim().length() != 0){
    			txtareaConfigQuery.setText(query);
    			txtareaConfigQuery.setCaretPosition(0);
    		}		
    	}else if(isTablePropchanged){
    		int option = JOptionPane.showConfirmDialog(this, Msg_TProp_Changed, ResourceManager.getDefaultManager().getString("CVProduct.Name") + " "+AdminWise.connectorManager.getString("externalDBLookup.msg_1"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    		if(option == JOptionPane.YES_OPTION){
    			query = generateQuery();
    			if(query != null && query.trim().length() != 0){
    				txtareaConfigQuery.setText(query);
    				txtareaConfigQuery.setCaretPosition(0);
    			}			
    		}	    
    	}

    	if(query.contains("\n"))
    		query = query.replace("\n", " ");

    	DocType docType = new DocType(docTypeId);
    	docType.setName(doctypeRecord.getName());
    	docType.setIndices(mappedIndices);
    	//docType.setTriggerIndexId(triggerIndexId);	

    	DBLookup lookupInfo = new DBLookup(0);
    	lookupInfo.setDocTypeId(docTypeId);
    	lookupInfo.setDsnName(TxtDSNName.getText().trim());
    	lookupInfo.setUserName(TxtUserName.getText().trim());
    	/*
    	 * JPasswordField.getText() is reaplced with JPasswordfield.getPassword()
    	 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
    	 */
    	lookupInfo.setPassWord(new String (TxtPassword.getPassword()));

    	lookupInfo.setDsnStatus(cmbDSNStatus.getSelectedIndex());
    	lookupInfo.setExternalTable(TxtMapTableName.getText().trim());
    	/*lookupInfo.setTriggerIndexId(triggerIndexId);
	lookupInfo.setTriggerIndexDataType(triggerIndexDataType);*/
    	lookupInfo.setDocType(docType);
    	lookupInfo.setConfigQuery(query);

    	VWExternalLookupConnector.setDBLookup(lookupInfo);	
    	//VWExternalLookupConnector.saveIndexMapping(docType, triggerIndexDataType);
    	cancelFlag=false;
    	isTablePropchanged = false;
    	BtnSave.setEnabled(false);
    	BtnVerify.setEnabled(true);
    }

    public void BtnGenerateQuery_actionPerformed(ActionEvent event) {
    	try{
    		String query = generateQuery();
    		if(query != null && query.trim().length() != 0){
    			isTablePropchanged = false;
    			txtareaConfigQuery.setText(query);
    			txtareaConfigQuery.setCaretPosition(0);
    		}
    	}catch(Exception e){
    		
    	}

    }
    
    public String generateQuery(){
    	String query = "";
		Vector indices = table.getData();
		String selectStr = "", whereStr = "", cond = " and ";
		boolean errFlag = false;
		int triggerCount = 0;
		
    	try{
    		if(indices != null && indices.size() > 0){
    			for(int i=0; i<indices.size(); i++){
    				Index idx = (Index) indices.get(i);
    				String extCol = idx.getExternalColumn();
    				
    				if (idx.isTriggerEnabled())
    				{
    					triggerCount++;
    					if (extCol == null || extCol.trim().length() == 0){
    						errFlag = true;
    						JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_3")+ idx.getName() + ">", ResourceManager.getDefaultManager().getString("CVProduct.Name") + " External DB Lookup", JOptionPane.OK_OPTION);
    						break;
    					}
    				}
    				
    				if(extCol != null && extCol.trim().length() > 0){
    					selectStr = selectStr + idx.getId()+ "," + extCol + ",";

    					if(idx.isTriggerEnabled()){
    						int triggerDataType = idx.getTriggerDataType();

    						if(triggerDataType != NUMERIC_DATA_TYPE)
    							whereStr = whereStr + "LTRIM(RTRIM(" + extCol + ")) = '@"+ extCol +"'"+ cond;
    						else
    							whereStr = whereStr + "LTRIM(RTRIM(" + extCol + ")) = @"+ extCol + cond;
    					}
    				}
    			}
    			
    			if(selectStr != null && selectStr.endsWith(","))
    				selectStr = selectStr.substring(0, selectStr.length()-1);
    			
    			if(whereStr != null && whereStr.endsWith(cond))
    				whereStr = whereStr.substring(0, (whereStr.length()-cond.length()));
    			
    			if(!errFlag)
    				query = "Select "+ selectStr +" \nFrom "+ TxtMapTableName.getText() +" \nWhere "+whereStr;
    		}
    		if(triggerCount == 0){
	    		JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_4"), ResourceManager.getDefaultManager().getString("CVProduct.Name") + " External DB Lookup", JOptionPane.OK_OPTION);
	    		return "";
	    	}
    	}catch(Exception e){
    		AdminWise.printToConsole("Exception while Generating Query "+e.getMessage());
    		e.printStackTrace();
    	}
    	return query;
    }

	public Vector getMappedIndices(){
	Vector data = new Vector();
	data = table.getData();
	if (data != null)
	    return data;
	else
	    return new Vector();
    }
//  ------------------------------------------------------------------------------
    void BtnReset_actionPerformed(java.awt.event.ActionEvent event)
    {
	cancelFlag=true;
	setVisible(false);
    }
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
    {
	cancelFlag=true;
	saveDlgOptions();
	setVisible(false);
    }
    
    void BtnVerify_actionPerformed(java.awt.event.ActionEvent event)
    {
    	try{
    		Vector LogMessage = new Vector();
    		LogMessage.add(Util.getNow(2) + " : DSN Name\t- " + TxtDSNName.getText());
    		LogMessage.add(Util.getNow(2) + " : User Name\t- " + TxtUserName.getText());
    		LogMessage.add(Util.getNow(2) + " : External Table\t- " + TxtMapTableName.getText());
    		LogMessage.add(Util.getNow(2) + " : Document Type\t- " + doctypeRecord.getName());
    		LogMessage.add("");
    		
    		DocType docType = new DocType(docTypeId);
    		docType.setName(doctypeRecord.getName());
    		triggerIndexDialog = new VWTriggerIndexDialog(this);

    		Hashtable triggerTableData = triggerIndexDialog.getTriggerTableData();
    		Vector indices = new Vector();
    		Vector mappedIndices = getMappedIndices();

    		if(mappedIndices != null && mappedIndices.size() > 0){
    			for(int i=0; i<mappedIndices.size(); i++){
    				Index idx = (Index) mappedIndices.get(i);
    				if(idx.isTriggerEnabled()){
    					String triggerInput = "";
    					if(triggerTableData != null && triggerTableData.size() > 0){
    						triggerInput = (String) triggerTableData.get(idx.getId());
    					}
    					idx.setTriggerValue(triggerInput);
    					LogMessage.add(Util.getNow(2) + " : Read data for trigger Index "+idx.getName()+" is <" + triggerInput+">");
    				}
    				indices.add(idx);
    			}
    			docType.setIndices(indices);
    		}
    		if(triggerTableData != null && triggerTableData.size() > 0)
    			new VWDBLookupLogger(this, ResourceManager.getDefaultManager().getString("CVProduct.Name") + " DB lookup logger", LogMessage, docType, mappedIndices);    		    		
    	}catch(Exception ex){
    		AdminWise.printToConsole("Exception in verify process " + ex.getMessage());
    	}
    }
   
    String addSpace(String val, int len){
    	String result  = val; 
    	for (int i= val.length(); i < len; i++){
    		result += " ";
    	}
    	return result;
    }

//  ------------------------------------------------------------------------------
    void BtnTest_actionPerformed(java.awt.event.ActionEvent event)
    {
		if (TxtDSNName.getText().trim().length() == 0) {
		    JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_5"), ResourceManager.getDefaultManager().getString("CVProduct.Name") + " External DB Lookup", JOptionPane.OK_OPTION);
		    return;
		}
		if (TxtMapTableName.getText().trim().length() == 0) {
		    JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("externalDBLookup.msg_6"), ResourceManager.getDefaultManager().getString("CVProduct.Name") + " External DB Lookup", JOptionPane.OK_OPTION);
		    return;
		}
		Vector columnList = new Vector();
		AdminWise.printToConsole("Before calling getExternalTableColumns method....");
		VWExternalLookupConnector.getExternalTableColumns(TxtDSNName.getText().trim(), 
		TxtUserName.getText().trim(), 
		/*
    	 * JPasswordField.getText() is reaplced with JPasswordfield.getPassword()
    	 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
    	 */
		new String(TxtPassword.getPassword()), 
		TxtMapTableName.getText().trim(), 
		txtDSNQuery.getText().trim(),
		columnList);
		AdminWise.printToConsole("columnList afterconnecting with DB....."+columnList);
		if (columnList != null && columnList.size() > 0){
			/**CV10.2 - added for DBLookup customer issue******/
			AdminWise.printToConsole("columnList.get(0)...."+columnList.get(0));
		    if (columnList.get(0).toString().equalsIgnoreCase("error")){
		    	JOptionPane.showMessageDialog(this, columnList.get(1).toString(), ResourceManager.getDefaultManager().getString("CVProduct.Name") + " External DB Lookup", JOptionPane.OK_OPTION);
		    }else{
		    	columnList.add(0, "");
				table.setExternalColumnList(columnList);
				if (columnList != null && columnList.size() > 0){
				    isConnected = true;
				    setEnableMode(true);
				}
		    }
		    /***End of CV10.2 merges*************************/
		}
    }    

//  ------------------------------------------------------------------------------

    public void setEnableMode(boolean flag){
	/*CmbTriggerName.setEnabled(flag);
	CmbTriggerType.setEnabled(flag);*/
	table.setEnabled(flag);
	BtnSave.setEnabled(flag);
	txtareaConfigQuery.setEnabled(flag);
	SPConfigQuery.setEnabled(flag);
	BtnGenerateQuery.setEnabled(flag);
    }
    public String[] getValues()
    {
	String[] values=new String[3];
	values[0]=TxtUserName.getText();
	/*
	 * JPasswordField.getText() is reaplced with JPasswordfield.getPassword()
	 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
	 */
	values[1]=new String(TxtPassword.getPassword());
	return values;
    }
//  ------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
	return cancelFlag;
    }
//  ------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
	Preferences prefs = Preferences.userNodeForPackage(getClass());
	if(AdminWise.adminPanel.saveDialogPos)
	{
	    prefs.putInt( "x", this.getX());
	    prefs.putInt( "y", this.getY());
	}

    }
//  ------------------------------------------------------------------------------
    private void getDlgOptions()
    {
	Preferences prefs = Preferences.userNodeForPackage(getClass());
	//PR...
	Dimension d =VWUtil.getScreenSize();
	int x=prefs.getInt( "x",d.width/4);
	int y=prefs.getInt( "y",d.height/3);
	setLocation(x,y);
    }
//  ------------------------------------------------------------------------------    
    public void setSaveEnabled(boolean flag) {
    	isTablePropchanged = true;
		BtnSave.setEnabled(flag);
		BtnVerify.setEnabled(!flag);		
	}

//  ------------------------------------------------------------------------------
    //{{DECLARE_CONTROLS
    private String lbl_Trigger = AdminWise.connectorManager.getString("externalDBLookup.lbl_Trigger");
    private String lbl_TriggerType = AdminWise.connectorManager.getString("externalDBLookup.lbl_TriggerType");
    private String BTN_CONNECT_NAME =AdminWise.connectorManager.getString("externalDBLookup.BTN_CONNECT_NAME");
    private String BTN_VERIFY_NAME = AdminWise.connectorManager.getString("externalDBLookup.BTN_VERIFY_NAME");
    private String DSN_STATUS = AdminWise.connectorManager.getString("externalDBLookup.DSN_STATUS");
    javax.swing.JPanel PnlConnection = new javax.swing.JPanel();
    javax.swing.JPanel PnlConfig = new javax.swing.JPanel();
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel4 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel5 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel6 = new javax.swing.JLabel();
    javax.swing.JLabel lblDSNStatus = new javax.swing.JLabel();
    javax.swing.JLabel lblDSNQuery = new javax.swing.JLabel();
    
    javax.swing.JComboBox cmbDSNStatus = new javax.swing.JComboBox();
    javax.swing.JTextField TxtDSNName = new javax.swing.JTextField(50);
    javax.swing.JTextField TxtMapTableName = new javax.swing.JTextField(50);
    javax.swing.JTextField TxtUserName = new javax.swing.JTextField(50);
    javax.swing.JPasswordField TxtPassword = new javax.swing.JPasswordField(50);
    javax.swing.JTextField txtDSNQuery = new javax.swing.JTextField(50);    
    
    JTextArea txtareaConfigQuery = new JTextArea(4, 40);
    JScrollPane SPConfigQuery; 

    /**
     * Following fields are Commented for DbLookup enhancement - Allowing multiple tigger indices and Data types
     */
    /*public VWComboBox CmbTriggerName = new VWComboBox();
    public VWComboBox CmbTriggerType = new VWComboBox();
*/
    VWLinkedButton BtnConSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnReset = new VWLinkedButton(0,true);
    VWLinkedButton BtnConnect = new VWLinkedButton(0,true);
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
    VWLinkedButton BtnVerify = new VWLinkedButton(0,true);
    VWLinkedButton BtnReport = new VWLinkedButton(0,true);
    VWLinkedButton BtnGenerateQuery = new VWLinkedButton(0, true);
    
//  ------------------------------------------------------------------------------
    boolean cancelFlag=true;
    private String lbl_Border_Connection = AdminWise.connectorManager.getString("externalDBLookup.lbl_Border_Connection");
    private String lbl_Border_Config =AdminWise.connectorManager.getString("externalDBLookup.lbl_Border_Config");
    private String lbl_DSNName = AdminWise.connectorManager.getString("externalDBLookup.lbl_DSNName");
    private String lbl_Mapping = AdminWise.connectorManager.getString("externalDBLookup.lbl_Mapping");
    private String lbl_UserName =AdminWise.connectorManager.getString("externalDBLookup.lbl_UserName");
    private String lbl_Password =AdminWise.connectorManager.getString("externalDBLookup.lbl_Password");
    private String lbl_Query =AdminWise.connectorManager.getString("externalDBLookup.lbl_Query");


    private VWIndexMappingTable table;
    private JScrollPane scrollPane;

    public static int TOP=130;
    public static int LEFT=6;
    public static int WIDTH=488;
    public static int HEIGHT=125;
    public VWDocTypeRec doctypeRecord = null;
    Vector indices = new Vector();
    Vector IndicesList = new Vector();
    JComboBox cmbTableField[];
    int TABLE_INDEXFIELD_COMBO_COUNT = 0;
    int TABLE_FIELD_COMBO_COUNT = 0;
    int docTypeId = 0;
    boolean isConnected = false; 
    boolean isTablePropchanged = false;
    
    VWTriggerIndexDialog triggerIndexDialog;    
    
    public static void main(String[] args) {
    	try{
    		String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
    		UIManager.setLookAndFeel(plasticLookandFeel);
    		JFrame frame = new JFrame();
    		VWDocTypeRec docType = new VWDocTypeRec("1	patient	0	1");
    		VWExternalDBLookup lookup =new VWExternalDBLookup(frame, "Test", docType);	    
	    //lookup.loadData(docType);
    		frame.setVisible(true);
    	}catch(Exception ex){
    		System.out.println("ERRRRR " + ex.getMessage());
    		ex.printStackTrace();
    	}
    }
}