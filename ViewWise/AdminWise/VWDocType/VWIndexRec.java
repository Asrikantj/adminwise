/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWRecord<br>
 *
 * @version     $Revision: 1.5 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWDocType;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;

import java.util.Vector;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.common.DBLookup;
import com.computhink.common.Index;
import ViewWise.AdminWise.VWMask.VWMaskField;

public class VWIndexRec implements VWConstant
{
	public VWIndexRec(String recordString,boolean fromIndexField)
	{
		 this(recordString, fromIndexField, true);
	}
    public VWIndexRec(String recordString,boolean fromIndexField, boolean loadSubInfo)
    {
        String str="";
        VWStringTokenizer tokens=new VWStringTokenizer(recordString,SepChar,false);
        if(fromIndexField)
        {
            id = VWUtil.to_Number(tokens.nextToken());
            name=tokens.nextToken().trim();
            type=VWUtil.to_Number(tokens.nextToken());
            str=tokens.nextToken();
            mask=str.trim().equals(".")?"":str;
            str=tokens.nextToken().trim();
            defaultValue=str.equals(".")?"":str;
            tokens.nextToken();
            sCounter=VWUtil.to_Number(tokens.nextToken());
            used=VWUtil.to_Number(tokens.nextToken());
            svid=VWUtil.to_Number(tokens.nextToken());
            info=tokens.nextToken().trim();
            if(!info.equals("") && !info.equals("."))
            {
                try{
                    readOnly=info.substring(0,1).equals("1");
                  if (info.substring(1,2).equals("2") ) 
                    	uniqueSeq="2";
                 else
                    unique=info.substring(1,2).equals("1");
                  /*
                   * Enhancement:- CV10.1 MultiSelect property for seleciton index
                   */
                  if(type==Index.IDX_SELECTION){
                	  multiSelect= info.substring(2, 3).equals("1");
                  } else {
                	  subNumberType=info.substring(2,3);  
                  }                    
                    if(info.length()>3)
                        valueType=VWUtil.to_Number(info.substring(3,4));
                }
                catch(Exception e){};
            }
            try{
                str=tokens.nextToken().trim();
                description=str.equals(".")?"":str;
            }
            catch(Exception e)
            {};            
        }
        else
        {
            dtIndexId=VWUtil.to_Number(tokens.nextToken());
            id = VWUtil.to_Number(tokens.nextToken());
            name=tokens.nextToken();
            type=VWUtil.to_Number(tokens.nextToken());
            str=tokens.nextToken();
            mask=str.trim().equals(".")?"":str;
            str=tokens.nextToken();
            defaultValue=str.trim().equals(".")?"":str;
            required=tokens.nextToken().equals("1");
            sCounter=VWUtil.to_Number(tokens.nextToken());
            key=tokens.nextToken().equals("1");
            tokens.nextToken();
            used=VWUtil.to_Number(tokens.nextToken());
            svid=VWUtil.to_Number(tokens.nextToken());
            info=tokens.nextToken().trim();
            if(!info.equals("") && !info.equals("."))
            {
                try{
                    readOnly=info.substring(0,1).equals("1");
                    if (info.substring(1,2).equals("2") ) 
                    	uniqueSeq="2";
                    else
                    unique=info.substring(1,2).equals("1");
                    /*
                     * Enhancement:- CV10.1 MultiSelect property for seleciton index
                     */
                    if(type==Index.IDX_SELECTION){
                      	multiSelect= info.substring(2, 3).equals("1");
                      } else {
                    	  subNumberType=info.substring(2,3);
                      }
                    if(info.length()>3)
                        valueType=VWUtil.to_Number(info.substring(3,4));
                }
                catch(Exception e1){}
            }
            try{
                str=tokens.nextToken().trim();
                description=str.equals(".")?"":str;
            }
            catch(Exception e){};
        }
        /**
         * Code optimization, not required to call loadSubInfo for Route which will call selection list. 
         */
        if(loadSubInfo)
        	loadSubInfo();
    }
    public VWIndexRec(Index index){
    	this(index, true);
    }
    public VWIndexRec(Index index, boolean loadSubInfo)
    {
    	try {
	        id = index.getId();
	        dtIndexId=index.getDTIndexId();
	        name=index.getName().trim();
	        AdminWise.printToConsole("type -------------:"+index.getType());
	        type=index.getType();     
	        AdminWise.printToConsole("Mask -------------:"+index.getMask());
	        mask=index.getMask().trim().equals(".")?"":index.getMask();
	        AdminWise.printToConsole("defaultValue -------------:"+index.getDefaultValue());
	        defaultValue=index.getDefaultValue().equals(".")?"":index.getDefaultValue();
	        sCounter=index.getCounter();
	        used=index.getUsed();
	        svid=index.getSystem();
	        required=index.isRequired();
	        key=index.isKey();
	        AdminWise.printToConsole("info -------------:"+index.getInfo());
	        info=index.getInfo().trim();
	        selectionValues=index.getDef();	        
	        if(!info.equals("") && !info.equals("."))
	        {
	            try{
	                readOnly=info.substring(0,1).equals("1");
	                if (info.substring(1,2).equals("2") ) 
	                	uniqueSeq="2";
	                else
	                unique=info.substring(1,2).equals("1");
	                /*
	                 * Enhancement:- CV10.1 MultiSelect property for seleciton index
	                 */
	                if(type==Index.IDX_SELECTION){
	                	multiSelect= info.substring(2, 3).equals("1");
	                } else {
	                	subNumberType=info.substring(2,3);
	                }                
	                if(info.length()>3){
	                    valueType=VWUtil.to_Number(info.substring(3,4));
	                }
	            }
	            catch(Exception e){};
	        }
	            try{
	                description=index.getDescription().trim();
	                triggerEnabled = index.isTriggerEnabled();
	                triggerDataType = index.getTriggerDataType();
	            }
	            catch(Exception e)
	            {};
	            /**
	             * Code optimization, not required to call loadSubInfo for Route which will call selection list. 
	             */
	            if(loadSubInfo)
	            	loadSubInfo();
	            AdminWise.printToConsole("End of the VWIndexRec object creation.....");
    	} catch (Exception e) {
    		AdminWise.printToConsole("Exception while creating VWIndexRec object for indices.... "+e);
    	}
    }
//-----------------------------------------------------------
    public VWIndexRec(int recId,int recDTIndexId,String recName,int recType,
        String recMask,String recDefault,int recSCounter,int recSVid,
        boolean recKey,boolean recRequired,String recDescription,
        boolean recUnique,boolean recReadOnly , boolean recMultiSelect)
    {
        id=recId;
        dtIndexId=recDTIndexId;
        name=recName.trim();
        type=recType;
        mask=recMask.trim().equals(".")?"":recMask;
        defaultValue=recDefault.trim().equals(".")?"":recDefault;
        sCounter=recSCounter;
        svid=recSVid;
        key=recKey;
        required=recRequired;
        description=recDescription;
        unique=recUnique;
        readOnly=recReadOnly;
        multiSelect= recMultiSelect;
        loadSubInfo();
    }
//-----------------------------------------------------------
    public VWIndexRec(int recId,int recDTIndexId,String recName,int recType,
        String recMask,String recDefault,int recSCounter,int recSVid,
        boolean recKey,boolean recRequired,String recDescription,String recInfo)
    {
        id=recId;
        dtIndexId=recDTIndexId;
        name=recName.trim();
        type=recType;
        if(recMask==null || recMask.trim().equals(""))
            mask="";
        else
            mask=recMask;
        if(recDefault==null || recDefault.trim().equals(""))
            defaultValue="";
        else
            defaultValue=recDefault;
        sCounter=recSCounter;
        svid=recSVid;
        key=recKey;
        required=recRequired;
        description=recDescription;
        info=recInfo;
    }
    
    /**@author apurba.m
     * CV10.1 Enhancement: Added for getting indices without selection values for generating custom
     * report of Recycle, AT and Retention Panels
     * @param recordString
     * @param fromIndexField
     * @param validateVariable
     */
    public VWIndexRec(String recordString,boolean fromIndexField, String validateVariable)
	{
		 this(recordString, fromIndexField, true, validateVariable);
	}
    public VWIndexRec(String recordString,boolean fromIndexField, boolean loadSubInfo, String validateVariable)
    {
        String str="";
        VWStringTokenizer tokens=new VWStringTokenizer(recordString,SepChar,false);
        if(fromIndexField)
        {
            id = VWUtil.to_Number(tokens.nextToken());
            name=tokens.nextToken().trim();
            type=VWUtil.to_Number(tokens.nextToken());
            str=tokens.nextToken();
            mask=str.trim().equals(".")?"":str;
            str=tokens.nextToken().trim();
            defaultValue=str.equals(".")?"":str;
            tokens.nextToken();
            sCounter=VWUtil.to_Number(tokens.nextToken());
            used=VWUtil.to_Number(tokens.nextToken());
            svid=VWUtil.to_Number(tokens.nextToken());
            info=tokens.nextToken().trim();
            if(!info.equals("") && !info.equals("."))
            {
                try{
                    readOnly=info.substring(0,1).equals("1");
                  if (info.substring(1,2).equals("2") ) 
                    	uniqueSeq="2";
                 else
                    unique=info.substring(1,2).equals("1");
                    subNumberType=info.substring(2,3);
                    if(info.length()>3)
                        valueType=VWUtil.to_Number(info.substring(3,4));
                }
                catch(Exception e){};
            }
            try{
                str=tokens.nextToken().trim();
                description=str.equals(".")?"":str;
            }
            catch(Exception e)
            {};            
        }
        else
        {
            dtIndexId=VWUtil.to_Number(tokens.nextToken());
            id = VWUtil.to_Number(tokens.nextToken());
            name=tokens.nextToken();
            type=VWUtil.to_Number(tokens.nextToken());
            str=tokens.nextToken();
            mask=str.trim().equals(".")?"":str;
            str=tokens.nextToken();
            defaultValue=str.trim().equals(".")?"":str;
            required=tokens.nextToken().equals("1");
            sCounter=VWUtil.to_Number(tokens.nextToken());
            key=tokens.nextToken().equals("1");
            tokens.nextToken();
            used=VWUtil.to_Number(tokens.nextToken());
            svid=VWUtil.to_Number(tokens.nextToken());
            info=tokens.nextToken().trim();
            if(!info.equals("") && !info.equals("."))
            {
                try{
                    readOnly=info.substring(0,1).equals("1");
                    if (info.substring(1,2).equals("2") ) 
                    	uniqueSeq="2";
                    else
                    unique=info.substring(1,2).equals("1");
                    subNumberType=info.substring(2,3);
                    if(info.length()>3)
                        valueType=VWUtil.to_Number(info.substring(3,4));
                }
                catch(Exception e1){}
            }
            try{
                str=tokens.nextToken().trim();
                description=str.equals(".")?"":str;
            }
            catch(Exception e){};
        }
    }
//-----------------------------------------------------------
    public VWIndexRec(int recId,String recName)
    {
        id=recId;
        name=recName;
    }
//-----------------------------------------------------------
    public int getId()
    {
        return id;
    }
//-----------------------------------------------------------
    public void setId(int recId)
    {
        id=recId;
    }
//-----------------------------------------------------------
    public String getName()
    {
        return name;
    }
//-----------------------------------------------------------
    public void setName(String recName)
    {
        name=recName.trim();
    }
//-----------------------------------------------------------
    public int getType()
    {
        return type;
    }
//-----------------------------------------------------------
    public void setType(int recType)
    {
        type=recType;
    }
//-----------------------------------------------------------
    public String getTextType()
    {
        return VWConstant.VWIndexTypes[type];
    }
//-----------------------------------------------------------
    public void setTextType(String recTextType)
    {
        for(int i=0;i<VWConstant.VWIndexTypes.length;i++)
        {
            if(VWConstant.VWIndexTypes[i].equalsIgnoreCase(recTextType))
            {
                type=i;
                break;
            }
        }
    }
//-----------------------------------------------------------
    public String getMask()
    {
        return mask;
    }
//-----------------------------------------------------------
    public void setMask(String recMask)
    {
        mask=recMask;
    }
//-----------------------------------------------------------
    public String getDefault()
    {
        return defaultValue;
    }
//-----------------------------------------------------------
    public void setDefault(String recDefault)
    {
        defaultValue=recDefault;
    }
//-----------------------------------------------------------
    public String getDescription()
    {
        return description;
    }
//-----------------------------------------------------------
    public void setDescription(String recDescription)
    {
        description=recDescription;
    }
//-----------------------------------------------------------
    public String getInfo()
    {
        return info;
    }
//-----------------------------------------------------------
    public void setInfo(String recInfo)
    {
        info=recInfo;
    }
//-----------------------------------------------------------
    public int getSVid()
    {
        return svid;
    }
//-----------------------------------------------------------
    public void setSVid(int recSVid)
    {
        svid=recSVid;
    }
//-----------------------------------------------------------  
    public int getSCounter()
    {
        return sCounter;
    }
//-----------------------------------------------------------
    public void setSCounter(int recSCounter)
    {
        sCounter=recSCounter;
    }
//-----------------------------------------------------------
    public boolean getKey()
    {
        return key;
    }
//-----------------------------------------------------------
    public void setKey(boolean recKey)
    {
        key=recKey;
    }
//-----------------------------------------------------------
    public boolean getRequired()
    {
        return required;
    }
//-----------------------------------------------------------
    public void setRequired(boolean recRequired)
    {
        required=recRequired;
    }
//-----------------------------------------------------------
    public boolean getReadOnly()
    {
        return readOnly;
    }
//-----------------------------------------------------------
    public void setReadOnly(boolean recReadOnly)
    {
        readOnly=recReadOnly;
    }
//-----------------------------------------------------------
    public boolean getUnique()
    {
        return unique;
    }
//-----------------------------------------------------------
    public void setUnique(boolean recUnique)
    {
        unique=recUnique;
    }
//-----------------------------------------------------------
    public int getDTIndexId()
    {
        return dtIndexId;
    }
//-----------------------------------------------------------
    public void setIndexUsed(int recUsed)
    {
        used=recUsed;
    }
//-----------------------------------------------------------
    public int getIndexUsed()
    {
        return used;
    }
//-----------------------------------------------------------
    public void addIndexUsed()
    {
        used++;
    }
//-----------------------------------------------------------
    public void setDTIndexId(int recDTIndexId)
    {
        dtIndexId=recDTIndexId;
    }
    //-----------------------------------------------------------
    public Vector getIndexSelectionValues()
    {
    	if(type==Index.IDX_SELECTION){
    		try{
    			if(subInfo!=null){
    				if(!subInfo[0].equals("")  && !subInfo[0].equals("...")){
    					String[] values = subInfo[0].split("\\|");
    					for(int i=0;i<values.length;i++){
    						String temp = values[i];
    						if(temp!=null){
    							selectionValues.add(temp);
    						}
    					}
    				}
    			}
    		}catch(Exception ex){
    			//System.out.println("ex in getIndexSelectionValues is : "+ex.getMessage());
    		}
    		return selectionValues;
    	}else
        	return null;
    }
    //-----------------------------------------------------------
    public String getIndexSelectionValuesStr()
    {
    	//This method is optimized by using StringBuffer object and remove the duplicate in the selection list 
    	// For GCNA client we faced issue on the update selection list 
    	String strValues="";
    	StringBuffer buffer = new StringBuffer();
    	if(type==Index.IDX_SELECTION)
    	{
    		if(subInfo!=null){
    			if(!subInfo[0].equals("")  && !subInfo[0].equals("...")){
    				for(int i=0;i<subInfo.length;i++){
    					String temp = subInfo[i];
    					temp = temp.replaceAll("\\|", "");
    		            selectionValues.add(temp);
    				}
    				return subInfo[0];
    			}
    		}
    		if(selectionValues==null || selectionValues.size()==0) return "";
    		int count = selectionValues.size();
    		String sTemp = "";
    		// fix for 508                   
    		Vector filter = new Vector();
    		if (selectionValues.size() > 0){
    			try{
    				for (int i = 0 ; i < count; i++){
    					//System.out.println("Select Values " + i + " : " + selectionValues.get(i) + " ::: " +selectionValues.size());
    					if (i >= (selectionValues.size())) break;
    					String temp = selectionValues.get(i).toString().trim().replaceAll("\n", "").toLowerCase();
    					if (filter.contains(temp)) {
    						//System.out.println("Item " + i + " : " + selectionValues.get(i).toString()+ " ::: " + temp);
    						Object tmpObj = selectionValues.get(i);
    						selectionValues.remove(tmpObj);
    						//System.out.println("Count ::: " + selectionValues.size());
    						continue;
    					}
    					filter.add(temp);        	    
    				}
    			}catch(Exception ex){        	    
    				ex.printStackTrace();
    			}
    			filter = null;
    			count=selectionValues.size();
    			buffer.append(selectionValues.get(0).toString().trim().replaceAll("\n", ""));        	
    			for(int i=1;i<count;i++)
    			{
    				// Removes space in between values in selection index
    				/*
    				 * Issue #599 	: Add values to an existing selection IF or Create a new selection IF, 
    				 * and add values to it. (Example method: AW -> DT tab -> Update -> Selection IF -> 
    				 * Values -> Enter values -> OK -> Save). Choose one of these fields as the Default Value.	 
    				 * Created By	: M.Premananth
    				 * Date			: 6-July-2006
    				 */        	    
    				sTemp = ((String)selectionValues.get(i)).trim().replaceAll("\n", "");
    				if (sTemp.length() > 0){        	
    					buffer.append("|"+ sTemp);
    				}
    			}
    		}
    	}
    	buffer.trimToSize();
    	strValues = buffer.toString();
    	while(strValues.endsWith("|")){
    		strValues = strValues.substring(0,strValues.lastIndexOf("|"));
    	}
	//End of fix
	return strValues;
    }
//-----------------------------------------------------------
    public void setIndexSelectionValues(Vector recSelectionValues)
    {
        if(type==Index.IDX_SELECTION)
        if(subInfo==null) subInfo=new String[3];
        subInfo[0]=recSelectionValues==null || recSelectionValues.size()==0?"":"...";
        selectionValues=new Vector();
        for(int i=0;i<recSelectionValues.size();i++)
            selectionValues.add(recSelectionValues.get(i));
    }
//-----------------------------------------------------------
    public void setIndexSelectionValuesStr(String recSelectionValues)
    {
        if(type==Index.IDX_SELECTION)
        if(subInfo==null) subInfo=new String[3];
        subInfo[0]=recSelectionValues;
    }
//-----------------------------------------------------------
    public String[] getSubInfo()
    {
        return subInfo;
    }
//-----------------------------------------------------------
    public void resetSubInfo()
    {
        subInfo=null;
    }
    
	public boolean isTriggerEnabled() {
		return triggerEnabled;
	}
	
	public void setTriggerEnabled(boolean triggerEnabled) {
		this.triggerEnabled = triggerEnabled;
	}
	
	public int getTriggerDataType() {
		return triggerDataType;
	}
	public void setTriggerDataType(int triggerDataType) {
		this.triggerDataType = triggerDataType;
	}
	public String getTriggerValue() {
		return triggerValue;
	}
	public void setTriggerValue(String triggerValue) {
		this.triggerValue = triggerValue;
	}
//-----------------------------------------------------------
    public void setSubInfo(String[] recSubInfo)
    {
        if(recSubInfo==null || recSubInfo.length==0) return;
        subInfo=recSubInfo;
        if(type==Index.IDX_TEXT)
        {
            svid=0;
            mask=subInfo[0].trim();
            readOnly=subInfo[1].equals(YesNoData[0]);
            unique=subInfo[2].equals(YesNoData[0]);
            defaultValue=subInfo[3];
        }
        else if(type==Index.IDX_BOOLEAN)
        {
            svid=0;
            readOnly=subInfo[0].equals(YesNoData[0]);
            defaultValue=subInfo[1];
        }
        else if(type==Index.IDX_DATE)
        {
            if(svid!=1) svid=0;
            mask=subInfo[0].trim();
            readOnly=subInfo[1].equals(YesNoData[0]);
            for(int vi=0;vi<DateValueTypes.length;vi++)
                if(DateValueTypes[vi].equals(subInfo[2]))   valueType=vi; 
            defaultValue=subInfo[3];
            if(valueType==0) svid=1;
        }
        else if(type==Index.IDX_SEQUENCE)
        {
            svid=0;
            subInfo[0].replace('~','_');
            mask=subInfo[0];
            if(subInfo[1].trim().equals("")) subInfo[1]="0";
            int count=VWUtil.to_Number(subInfo[1]);
            for(int i=0;i<count;i++) mask+="~";
            subInfo[2].replace('~','_');
            mask+=subInfo[2];
            readOnly=subInfo[3].equals(YesNoData[0]);
            if (subInfo[4].equals(SeqUniqData[2])) 
            	uniqueSeq=SeqUniqData[2];
            else 
                 unique=subInfo[4].equals(YesNoData[0]);
            
            
            if(subInfo[5].trim().equals("")) subInfo[5]="0";
            defaultValue=subInfo[5].trim();
            try
            {
                sCounter=VWUtil.to_Number(defaultValue);
            }
            catch(Exception e)
            {
                sCounter=VWUtil.to_Number(defaultValue.substring(subInfo[0].length(),
                subInfo[5].length()-subInfo[2].length()));
            }
        }
        else if(type==Index.IDX_SELECTION)
        {
            if(svid!=2) svid=0;
            readOnly=subInfo[1].equals(YesNoData[0]);
            unique=subInfo[2].equals(YesNoData[0]);
            mask="";
            /*
             * Issue #599 	: Add values to an existing selection IF or Create a new selection IF, 
             * and add values to it. (Example method: AW -> DT tab -> Update -> Selection IF -> 
             * Values -> Enter values -> OK -> Save). Choose one of these fields as the Default Value.	 
             * Created By	: M.Premananth
             * Date			: 6-July-2006
             */
            if(subInfo[3]!= null)
            	defaultValue=subInfo[3].trim();
            multiSelect=subInfo[4].equals(YesNoData[0]);
        }
        else if(type==Index.IDX_NUMBER)
        {
            svid=0;
            if(subInfo[0].equals(VWIndexNumericMask[0]))
            {
                mask="0";
                subNumberType="0";
                readOnly=subInfo[1].equals(YesNoData[0]);
                unique=subInfo[2].equals(YesNoData[0]);
                defaultValue=subInfo[3];
            }
            else if(subInfo[0].equals(VWIndexNumericMask[1]))
            {
                mask="0";
                subNumberType="1";
                if(subInfo[1].trim().equals("")) subInfo[1]="0";
                int count=VWUtil.to_Number(subInfo[1]);
                if(count>0)
                {
                    mask+=".";
                    for(int i=0;i<count;i++)
                        mask+="0";
                }
                if(subInfo[3].equals(YesNoData[0]))
                    mask="#,##" + mask;
                if(subInfo[2].equals(YesNoData[0]))
                    mask="(" + mask + ")";
                readOnly=subInfo[4].equals(YesNoData[0]);
                unique=subInfo[5].equals(YesNoData[0]);
                defaultValue=subInfo[6];
            }
            else if(subInfo[0].equals(VWIndexNumericMask[2]))
            {
                mask="0";
                subNumberType="2";
                if(subInfo[1].trim().equals("")) subInfo[1]="0";
                int count=VWUtil.to_Number(subInfo[1]);
                if(count>0)
                {
                    mask+=".";
                    for(int i=0;i<count;i++)
                        mask+="0";
                }
                if(subInfo[4].equals(YesNoData[0]))
                    mask="#,##" + mask;
                mask=subInfo[2].trim()+mask;
                if(subInfo[3].equals(YesNoData[0]))
                    mask="(" + mask + ")";
                readOnly=subInfo[5].equals(YesNoData[0]);
                unique=subInfo[6].equals(YesNoData[0]);
                defaultValue=subInfo[7];
            }
           else if(subInfo[0].equals(VWIndexNumericMask[3]))
            {
                mask="0";
                subNumberType="3";
                if(subInfo[1].trim().equals("")) subInfo[1]="0";
                int count=VWUtil.to_Number(subInfo[1]);
                if(count>0)
                {
                    mask+=".";
                    for(int i=0;i<count;i++)
                        mask+="0";
                }
                mask+="%";
                readOnly=subInfo[2].equals(YesNoData[0]);
                unique=subInfo[3].equals(YesNoData[0]);
                defaultValue=subInfo[4];
            }
            else if(subInfo[0].equals(VWIndexNumericMask[4]))
            {
                mask="0";
                subNumberType="4";
                if(subInfo[1].trim().equals("")) subInfo[1]="0";
                int count=VWUtil.to_Number(subInfo[1]);
                if(count>0)
                {
                    mask+=".";
                    for(int i=0;i<count;i++)
                        mask+="0";
                }
                mask+="E+00";
                readOnly=subInfo[2].equals(YesNoData[0]);
                unique=subInfo[3].equals(YesNoData[0]);
                defaultValue=subInfo[4];
            }  
        }
        setInfoData();
    }
//-----------------------------------------------------------
    private void setInfoData()
    {
        if(readOnly)
            info="1";
        else
            info="0";
        if (uniqueSeq.equals(SeqUniqData[2]))
          	info+="2"; 
        else if(unique||uniqueSeq.equals(SeqUniqData[0]))
            info+="1";
        else
            info+="0";
        if(type==Index.IDX_SELECTION) {
        	if(multiSelect){
            	info+="1";
        	}
            else {
            	info+="0";	
            }
        } else {
           info+=subNumberType;
        }
        info+=String.valueOf(valueType);
    }
//-----------------------------------------------------------
    private void loadSubInfo()
    {
    	AdminWise.printToConsole("Index.IDX_TEXT >>>> "+Index.IDX_TEXT);
    	AdminWise.printToConsole("type >>>> "+type);
        if(type==Index.IDX_TEXT)
        {
            subInfo=new String[4];
            subInfo[0]=mask;
            subInfo[1]=readOnly?YesNoData[0]:YesNoData[1];
            subInfo[2]=unique?YesNoData[0]:YesNoData[1];
            subInfo[3]=defaultValue;
            AdminWise.printToConsole("After loading subinfo >>>> "+subInfo);
        }
        else if(type==Index.IDX_BOOLEAN)
        {
            subInfo=new String[2];
            subInfo[0]=readOnly?YesNoData[0]:YesNoData[1];
            subInfo[1]=defaultValue;
        }
        else if(type==Index.IDX_DATE)
        {
            subInfo=new String[4];
            subInfo[0]=mask;
            subInfo[1]=readOnly?YesNoData[0]:YesNoData[1];            
            subInfo[2]=DateValueTypes[valueType];
            subInfo[3]=defaultValue;
        }
        else if(type==Index.IDX_SEQUENCE)
        {
            subInfo=new String[6];
            int begin=mask.indexOf("~");
            subInfo[0]=mask.substring(0,begin);
            int noOfDigit=VWUtil.getCharCount(mask,"~");
            subInfo[1]=Integer.toString(noOfDigit);
            subInfo[2]=mask.substring(begin+noOfDigit,mask.length());
            subInfo[3]=readOnly?YesNoData[0]:YesNoData[1];
            if (uniqueSeq.equals("2")) {
            	subInfo[4]=SeqUniqData[2];
            }
            	
            else {
            	subInfo[4]=unique?YesNoData[0]:YesNoData[1];
            }
            subInfo[5]=defaultValue;
        }
        else if(type==Index.IDX_SELECTION)
        {
        	AdminWise.printToConsole("type loadSubInfo ::::"+type);
            subInfo=new String[5];
        	//subInfo=new String[4];
            subInfo[0]="...";///VWDocTypeConnector.getSelectionValuesString(id);
            if ( selectionValues == null || (selectionValues != null && selectionValues.size() == 0))
        	selectionValues= VWDocTypeConnector.getSelectionValues(id);
            
            subInfo[1]=readOnly?YesNoData[0]:YesNoData[1];
            subInfo[2]=unique?YesNoData[0]:YesNoData[1];
            subInfo[3]=defaultValue;
            subInfo[4]=multiSelect?YesNoData[0]:YesNoData[1];
        }
        else if(type==Index.IDX_NUMBER)
        {
            if(subNumberType.equals("0"))
            {
               subInfo=new String[4];
               subInfo[0]= VWIndexNumericMask[0];
               subInfo[1]=readOnly?YesNoData[0]:YesNoData[1];
               subInfo[2]=unique?YesNoData[0]:YesNoData[1];
               subInfo[3]=defaultValue;
            }
            else if(subNumberType.equals("3"))
            {
                subInfo=new String[5];
                subInfo[0]=(VWIndexNumericMask[3]);
                subInfo[1]=Integer.toString((VWUtil.getCharCount(mask,"0")-1));
                subInfo[2]=readOnly?YesNoData[0]:YesNoData[1];
                subInfo[3]=unique?YesNoData[0]:YesNoData[1];
                subInfo[4]=defaultValue;
            }
            else if(subNumberType.equals("4"))
            {
                subInfo=new String[5];
                subInfo[0]=(VWIndexNumericMask[4]);
                subInfo[1]=Integer.toString((VWUtil.getCharCount(mask,"0")-3));
                subInfo[2]=readOnly?YesNoData[0]:YesNoData[1];
                subInfo[3]=unique?YesNoData[0]:YesNoData[1];
                subInfo[4]=defaultValue;
            }
            else if(subNumberType.equals("2"))
            {   
                subInfo=new String[8];
                subInfo[0]=VWIndexNumericMask[2];
                subInfo[1]=Integer.toString(VWUtil.getCharCount(mask,"0")-1);
                subInfo[2]=VWUtil.getCurrencySymbol(retainCurrency(mask),VWMaskField.getCurrencySymbols());
                subInfo[3]=mask.indexOf("(")>=0?YesNoData[0]:YesNoData[1];
                subInfo[4]=mask.indexOf("#,##")>=0?YesNoData[0]:YesNoData[1];
                subInfo[5]=readOnly?YesNoData[0]:YesNoData[1];
                subInfo[6]=unique?YesNoData[0]:YesNoData[1];
                subInfo[7]=defaultValue;
            }  
            else
            {
                subInfo=new String[8];
                subInfo[0]=VWIndexNumericMask[1];
                subInfo[1]=Integer.toString((VWUtil.getCharCount(mask,"0")-1));
                subInfo[2]=mask.indexOf("(")>=0?YesNoData[0]:YesNoData[1];
                subInfo[3]=mask.indexOf("#,##")>=0?YesNoData[0]:YesNoData[1];
                subInfo[4]=readOnly?YesNoData[0]:YesNoData[1];
                subInfo[5]=unique?YesNoData[0]:YesNoData[1];
                subInfo[6]=defaultValue;
            }
            AdminWise.printToConsole("End of the loadsubinfo method.....");
        }
    }
    public String retainCurrency(String currency) {
  	  if (currency.startsWith("("))
  		  currency = currency.substring(1, currency.length());
  	  if (currency.endsWith(")"))
  		  currency = currency.substring(0, currency.length() - 1);
  	  currency = currency.replace("0.0", "");
  	  currency = currency.replace("0", "");
  	  currency = currency.replaceAll("#,##", "");
  	  return currency;
    }
//-----------------------------------------------------------
    public String toString()
    {
        return this.name;
    }
//-----------------------------------------------------------
    public Index getIndex()
    {
        Index index = new Index(getId()); 
        index.setName(getName());
        index.setType(getType());
        index.setMask(getMask());
        index.setDef(getDefault());
        index.setRequired(getRequired()); 
        index.setCounter(getSCounter());
        index.setSystem(getSVid());
        index.setInfo(getInfo());
        index.setDescription(getDescription());
        return index;
    }
//-----------------------------------------------------------
    private int id=0;
    private int dtIndexId=0;
    private String name="";
    private int type=0;
    private String mask="";
    private String defaultValue="";
    private int sCounter=0;
    private int svid=0;
    private boolean key=false;
    private boolean required=false;
    private String description="";    
    private String info="";
    private String[] subInfo=null;
    private int used=0;
    private boolean unique=false;
    private String uniqueSeq="";
    private boolean readOnly=false;
    private boolean multiSelect=false;
    private String subNumberType="0";
    private int valueType=0;
    public Vector selectionValues=new Vector();
    public DBLookup dbDSNInfo;
    /**
     * Following fields are added for DbLookup enhancement - Allowing multiple tigger indices and Data types
     */
    private boolean triggerEnabled;
    private int triggerDataType;
    private String triggerValue;
	public DBLookup getDbDSNInfo() {
		return dbDSNInfo;
	}
	public void setDbDSNInfo(DBLookup dbDSNInfo) {
		this.dbDSNInfo = dbDSNInfo;
	}
	
//-----------------------------------------------------------
}
