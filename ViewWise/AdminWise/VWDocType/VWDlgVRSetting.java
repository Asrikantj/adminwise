
/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWDocType;

import java.awt.*;
import javax.swing.*;
import java.util.List;
import java.util.LinkedList;
import java.util.prefs.*;
import javax.swing.border.TitledBorder;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import com.computhink.common.DocType;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;

public class VWDlgVRSetting extends javax.swing.JDialog implements 
    VWConstant
{
    public VWDlgVRSetting(Frame parent,int docTypeId)
    {
            super(parent);
            getContentPane().setBackground(AdminWise.getAWColor());
            setTitle(LBL_VRSETTING_NAME);
            setModal(true);
            getContentPane().setLayout(null);
            String languageLocale=AdminWise.getLocaleLanguage();
            if(languageLocale.equals("nl_NL")){
            	 setSize(400+60,400);
            }else
            	 setSize(400,400);
            	
           
            setVisible(false);
            JPanel1.setLayout(null);
            JPanel1.setBackground(AdminWise.getAWColor());
            JPanel1.setBorder(titledBorder1);
            getContentPane().add(JPanel1);
            if(languageLocale.equals("nl_NL")){
            	JPanel1.setBounds(4+7,38,392+47,314);
            }else
            JPanel1.setBounds(4,38,392,314);
            JLabel1.setText(LBL_INITVER);
            JPanel1.add(JLabel1);
            if(languageLocale.equals("nl_NL")){
            	 JLabel1.setBounds(26,14+10,86+30,20);
            }else
            JLabel1.setBounds(26,14,86,20);
            installSpinner(TxtInitVersion);
            JPanel1.add(TxtInitVersion);
            if(languageLocale.equals("nl_NL")){
            	TxtInitVersion.setBounds(152+5,14+10,45,20);
            }else
            	TxtInitVersion.setBounds(152,14,45,20);
            JLabel2.setText(LBL_INITREV);
            JPanel1.add(JLabel2);
            if(languageLocale.equals("nl_NL")){
            JLabel2.setBounds(240-20,14+10,88+30,20);
            }else
            JLabel2.setBounds(240,14,88,20);
            installSpinner(TxtInitRevision);
            JPanel1.add(TxtInitRevision);
            if(languageLocale.equals("nl_NL")){
            TxtInitRevision.setBounds(334+10+7,14+10,45,20);
            }else
             TxtInitRevision.setBounds(334,14,45,20);
            ChkOpt3.setText(LBL_OPTION_1);            
            JPanel1.add(ChkOpt3);
            if(languageLocale.equals("nl_NL")){
            	ChkOpt3.setBounds(18,58,354+60,28);
            }else
            	ChkOpt3.setBounds(18,58,354,28);	
            javax.swing.ButtonGroup group1 = new javax.swing.ButtonGroup(); 
            ChkOpt4.setText(LBL_INCVER);
            JPanel1.add(ChkOpt4);
            if(languageLocale.equals("nl_NL")){
            	ChkOpt4.setBounds(26,96,124+6,20);
            }else
            	ChkOpt4.setBounds(26,96,124,20);
            ChkOpt4.setSelected(true);
            installSpinner(TxtOpt1IncVer);
            JPanel1.add(TxtOpt1IncVer);
            if(languageLocale.equals("nl_NL")){
            TxtOpt1IncVer.setBounds(152+5,96,45,20);
            }else
            	TxtOpt1IncVer.setBounds(152,96,45,20);  	
            installSpinner(TxtOpt1IncRev);
            JPanel1.add(TxtOpt1IncRev);
            if(languageLocale.equals("nl_NL")){
            TxtOpt1IncRev.setBounds(334+10+7,96,45,20);
            }else
            	   TxtOpt1IncRev.setBounds(334,96,45,20);
            group1.add(ChkOpt4);
            ChkOpt5.setText(LBL_INCREV);
            JPanel1.add(ChkOpt5);
            if(languageLocale.equals("nl_NL")){
            	ChkOpt5.setBounds(208+5,96,124+4+6,20);
            }else
            	ChkOpt5.setBounds(208,96,124,20);
            group1.add(ChkOpt5); 
            ChkOpt6.setText(LBL_OPTION_2);
            JPanel1.add(ChkOpt6);
            if(languageLocale.equals("nl_NL")){
            ChkOpt6.setBounds(18,146,354+15,28);
            }else
            ChkOpt6.setBounds(18,146,354,28);
            javax.swing.ButtonGroup group2 = new javax.swing.ButtonGroup(); 
            ChkOpt7.setText(LBL_INCVER);
            JPanel1.add(ChkOpt7);
            if(languageLocale.equals("nl_NL")){
            ChkOpt7.setBounds(26,184,124+7,20);
            }else
            	ChkOpt7.setBounds(26,184,124,20);
            ChkOpt7.setSelected(true);
            JPanel1.add(TxtOpt2IncVer);
            installSpinner(TxtOpt2IncVer);
            if(languageLocale.equals("nl_NL")){
            	TxtOpt2IncVer.setBounds(152+5,184,45,20);
            }else
            	TxtOpt2IncVer.setBounds(152,184,45,20);
            group2.add(ChkOpt7); 
            ChkOpt8.setText(LBL_INCREV);
            JPanel1.add(ChkOpt8);
            if(languageLocale.equals("nl_NL")){
            ChkOpt8.setBounds(208+5,184,124+7,20);
            }else
            	  ChkOpt8.setBounds(208,184,124,20);
            installSpinner(TxtOpt2IncRev);
            JPanel1.add(TxtOpt2IncRev);
            if(languageLocale.equals("nl_NL")){
            TxtOpt2IncRev.setBounds(334+10+7,184,45,20);
            }else
            TxtOpt2IncRev.setBounds(334,184,45,20);
            group2.add(ChkOpt8); 
            ChkOpt9.setText(LBL_OPTION_3);
            JPanel1.add(ChkOpt9);
            if(languageLocale.equals("nl_NL")){
            ChkOpt9.setBounds(18,236,354+15,28);
            }else
            	 ChkOpt9.setBounds(18,236,354,28);
            javax.swing.ButtonGroup group3 = new javax.swing.ButtonGroup(); 
            ChkOpt10.setText(LBL_INCVER);
            JPanel1.add(ChkOpt10);
            if(languageLocale.equals("nl_NL")){
            ChkOpt10.setBounds(26,274,124+7,20);
            }else
            	 ChkOpt10.setBounds(26,274,124,20);
            ChkOpt10.setSelected(true);
            group3.add(ChkOpt10); 
            installSpinner(TxtOpt3IncVer);
            JPanel1.add(TxtOpt3IncVer);
            if(languageLocale.equals("nl_NL")){
            TxtOpt3IncVer.setBounds(152+5,274,45,20);
            }else
                TxtOpt3IncVer.setBounds(152,274,45,20);
            ChkOpt11.setText(LBL_INCREV);
            JPanel1.add(ChkOpt11);
            if(languageLocale.equals("nl_NL")){
            ChkOpt11.setBounds(208+5,274,124+7,20);
            }else
            ChkOpt11.setBounds(208,274,124,20);
            installSpinner(TxtOpt3IncRev);
            JPanel1.add(TxtOpt3IncRev);
            group3.add(ChkOpt11); 
            if(languageLocale.equals("nl_NL")){
            TxtOpt3IncRev.setBounds(334+10+7,274,45,20);
            }else
                TxtOpt3IncRev.setBounds(334,274,45,20);
            
            ChkEnable.setText(LBL_VERREV_ENABLE);
            ChkEnable.setActionCommand(LBL_VERREV_ENABLE);
            getContentPane().add(ChkEnable);
            ChkEnable.setBackground(AdminWise.getAWColor());
            ChkOpt3.setBackground(AdminWise.getAWColor());
            ChkOpt4.setBackground(AdminWise.getAWColor());
            ChkOpt5.setBackground(AdminWise.getAWColor());
            ChkOpt6.setBackground(AdminWise.getAWColor());
            ChkOpt7.setBackground(AdminWise.getAWColor());
            ChkOpt8.setBackground(AdminWise.getAWColor());
            ChkOpt9.setBackground(AdminWise.getAWColor());
            ChkOpt10.setBackground(AdminWise.getAWColor());
            ChkOpt11.setBackground(AdminWise.getAWColor());
            if(languageLocale.equals("nl_NL")){
            	ChkEnable.setBounds(4+15,8,392, AdminWise.gBtnHeight);
            }else
            	ChkEnable.setBounds(4,8,392, AdminWise.gBtnHeight);
            BtnOk.setText(BTN_OK_NAME);
            getContentPane().add(BtnOk);
            ///BtnOk.setBounds(35,366,72, AdminWise.gBtnHeight);
            if(languageLocale.equals("nl_NL")){
            BtnOk.setBounds(250+15,366,72+18, AdminWise.gBtnHeight);
            }else
            	BtnOk.setBounds(252,366,72, AdminWise.gBtnHeight);
//            BtnOk.setBackground(java.awt.Color.white);
            BtnOk.setIcon(VWImages.OkIcon);
            BtnRefresh.setText(BTN_REFRESH_NAME);
            getContentPane().add(BtnRefresh);
            if(languageLocale.equals("nl_NL")){
            	BtnRefresh.setBounds(5+13,366,82+18, AdminWise.gBtnHeight);
            }else
            	BtnRefresh.setBounds(5,366,82, AdminWise.gBtnHeight);
//            BtnRefresh.setBackground(java.awt.Color.white);
            BtnRefresh.setIcon(VWImages.RefreshIcon);
            BtnCancel.setText(BTN_CANCEL_NAME);
            getContentPane().add(BtnCancel);
            ///BtnCancel.setBounds(281,366,84,24);
            if(languageLocale.equals("nl_NL")){
            BtnCancel.setBounds(324+18+15,366,72+18, AdminWise.gBtnHeight);
            }else
            BtnCancel.setBounds(324,366,72, AdminWise.gBtnHeight);
//            BtnCancel.setBackground(java.awt.Color.white);
            BtnCancel.setIcon(VWImages.CloseIcon);
            SymAction lSymAction = new SymAction();
            BtnOk.addActionListener(lSymAction);
            BtnRefresh.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            ChkEnable.addActionListener(lSymAction);
            ChkOpt3.addActionListener(lSymAction);
            ChkOpt4.addActionListener(lSymAction);
            ChkOpt5.addActionListener(lSymAction);
            ChkOpt6.addActionListener(lSymAction);
            ChkOpt7.addActionListener(lSymAction);
            ChkOpt8.addActionListener(lSymAction);
            ChkOpt9.addActionListener(lSymAction);
            ChkOpt10.addActionListener(lSymAction);
            ChkOpt11.addActionListener(lSymAction);
            SymKey aSymKey = new SymKey();
            addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            setResizable(false);
            loadSettings(docTypeId,false);
            getDlgOptions();
            setVisible(true);
    }
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
                Object object = event.getSource();
                if (object == VWDlgVRSetting.this)
                        Dialog_windowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnOk)
                        BtnOk_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
                else if (object == BtnRefresh)
                        BtnRefresh_actionPerformed(event);
                else if (object == ChkEnable)
                        ChkEnable_actionPerformed(event);
                else if (object == ChkOpt3)
                        ChkOpt3_actionPerformed(event);
                else if (object == ChkOpt4)
                        ChkOpt4_actionPerformed(event);
                else if (object == ChkOpt5)
                        ChkOpt5_actionPerformed(event);
                else if (object == ChkOpt6)
                        ChkOpt6_actionPerformed(event);
                else if (object == ChkOpt7)
                        ChkOpt7_actionPerformed(event);
                else if (object == ChkOpt8)
                        ChkOpt8_actionPerformed(event);
                else if (object == ChkOpt9)
                        ChkOpt9_actionPerformed(event);
                else if (object == ChkOpt10)
                        ChkOpt10_actionPerformed(event);
                else if (object == ChkOpt11)
                        ChkOpt11_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------     
	public void addNotify()
	{
            Dimension size = getSize();
            super.addNotify();
            if (frameSizeAdjusted) return;
            frameSizeAdjusted = true;
            Insets insets = getInsets();
            setSize(insets.left+insets.right+size.width, insets.top+ 
                insets.bottom+size.height);
	}
//------------------------------------------------------------------------------
	boolean frameSizeAdjusted = false;
        javax.swing.JPanel JPanel1 = new javax.swing.JPanel();
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
        JSpinner TxtInitVersion = new JSpinner(new SpinnerNumberModel(0,0,999,1));
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
        JSpinner TxtInitRevision = new JSpinner(new SpinnerNumberModel(0,0,999,1));
        javax.swing.JCheckBox ChkOpt3 = new javax.swing.JCheckBox();
        javax.swing.JRadioButton ChkOpt4 = new javax.swing.JRadioButton();
        javax.swing.JRadioButton ChkOpt5 = new javax.swing.JRadioButton();
        JSpinner TxtOpt1IncVer = new JSpinner(new SpinnerNumberModel(0,0,999,1));
        JSpinner TxtOpt1IncRev = new JSpinner(new SpinnerNumberModel(0,0,999,1));
        javax.swing.JCheckBox ChkOpt6 = new javax.swing.JCheckBox();
        javax.swing.JRadioButton ChkOpt7 = new javax.swing.JRadioButton();
        javax.swing.JRadioButton ChkOpt8 = new javax.swing.JRadioButton();
        JSpinner TxtOpt2IncVer = new JSpinner(new SpinnerNumberModel(0,0,999,1));
        JSpinner TxtOpt2IncRev = new JSpinner(new SpinnerNumberModel(0,0,999,1));
	javax.swing.JCheckBox ChkOpt9 = new javax.swing.JCheckBox();
        javax.swing.JRadioButton ChkOpt10 = new javax.swing.JRadioButton();
        javax.swing.JRadioButton ChkOpt11 = new javax.swing.JRadioButton();
        JSpinner TxtOpt3IncVer = new JSpinner(new SpinnerNumberModel(0,0,999,1));
        JSpinner TxtOpt3IncRev = new JSpinner(new SpinnerNumberModel(0,0,999,1));
	javax.swing.JCheckBox ChkEnable = new javax.swing.JCheckBox();
	VWLinkedButton BtnOk = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
        VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
        TitledBorder titledBorder1 = new TitledBorder(LBL_SETTING);
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveVRSettings();
        saveDlgOptions();
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        loadSettings(curDocTypeId,true);
    }
//------------------------------------------------------------------------------
    void ChkEnable_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkEnable.isSelected())
        {
            JPanel1.setEnabled(true);
            JLabel1.setEnabled(true);
            TxtInitVersion.setEnabled(true);
            JLabel2.setEnabled(true);
            TxtInitRevision.setEnabled(true);
            ChkOpt3.setEnabled(true);
            ChkOpt6.setEnabled(true);
            ChkOpt9.setEnabled(true);
            ChkOpt3.setSelected(true);
            ChkOpt6.setSelected(true);
            ChkOpt9.setSelected(true);
            ChkOpt3_actionPerformed(null);            
            ChkOpt6_actionPerformed(null);
            ChkOpt9_actionPerformed(null);
            ChkOpt4.setSelected(true);
            ChkOpt8.setSelected(true);
            ChkOpt11.setSelected(true);
            TxtOpt1IncVer.setValue(new Integer(1));
            TxtOpt2IncRev.setValue(new Integer(1));
            TxtOpt3IncRev.setValue(new Integer(1));
            TxtInitVersion.setValue(new Integer(1));
            TxtInitRevision.setValue(new Integer(0));
            ChkOpt4_actionPerformed(null);
            ChkOpt8_actionPerformed(null);            
            ChkOpt11_actionPerformed(null);
            TxtInitVersion.requestFocus();
        }
        else
        {
            JPanel1.setEnabled(false);
            JLabel1.setEnabled(false);
            TxtInitVersion.setEnabled(false);
            JLabel2.setEnabled(false);
            TxtInitRevision.setEnabled(false);
            ChkOpt3.setSelected(false);
            ChkOpt3.setEnabled(false);
            ChkOpt6.setSelected(false);
            ChkOpt6.setEnabled(false);
            ChkOpt9.setSelected(false);
            ChkOpt9.setEnabled(false);
            TxtInitVersion.setValue(new Integer(0));
            TxtInitRevision.setValue(new Integer(0));
            ChkOpt3_actionPerformed(null);
            ChkOpt6_actionPerformed(null);
            ChkOpt9_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    void ChkOpt3_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt3.isSelected())
        {
            ChkOpt4.setEnabled(true);
            ChkOpt5.setEnabled(true);
            ChkOpt4.setSelected(true);
        }
        else
        {
            ChkOpt4.setEnabled(false);
            ChkOpt5.setEnabled(false);
            ChkOpt4.setSelected(false);
            ChkOpt5.setSelected(false);
            ChkOpt4.setSelected(false);
            ChkOpt5.setSelected(false);
        }
        ChkOpt4_actionPerformed(null);
        ChkOpt5_actionPerformed(null);
    }
//------------------------------------------------------------------------------
void ChkOpt4_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt4.isSelected() && ChkOpt3.isSelected())
        {
            ChkOpt5.setSelected(false);
            TxtOpt1IncRev.setValue(new Integer(0));
            TxtOpt1IncRev.setEnabled(false);
            TxtOpt1IncVer.setEnabled(true);
            TxtOpt1IncVer.requestFocus();
        }
        else
        {
            TxtOpt1IncVer.setValue(new Integer(0));
            TxtOpt1IncVer.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
void ChkOpt5_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt5.isSelected() && ChkOpt3.isSelected())
        {
            ChkOpt4.setSelected(false);
            TxtOpt1IncVer.setValue(new Integer(0));
            TxtOpt1IncVer.setEnabled(false);
            TxtOpt1IncRev.setEnabled(true);
            TxtOpt1IncRev.requestFocus();
        }
        else
        {
            TxtOpt1IncRev.setValue(new Integer(0));
            TxtOpt1IncRev.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
    void ChkOpt6_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt6.isSelected())
        {
            ChkOpt7.setEnabled(true);
            ChkOpt8.setEnabled(true);
            ChkOpt7.setSelected(true);
        }
        else
        {
            ChkOpt7.setEnabled(false);
            ChkOpt8.setEnabled(false);
            ChkOpt7.setSelected(false);
            ChkOpt8.setSelected(false);
            ChkOpt7.setSelected(false);
            ChkOpt8.setSelected(false);
        }
        ChkOpt7_actionPerformed(null);
        ChkOpt8_actionPerformed(null);
    }
//------------------------------------------------------------------------------
void ChkOpt7_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt7.isSelected() && ChkOpt6.isSelected())
        {
            ChkOpt8.setSelected(false);
            TxtOpt2IncRev.setValue(new Integer(0));
            TxtOpt2IncRev.setEnabled(false);
            TxtOpt2IncVer.setEnabled(true);
            TxtOpt2IncVer.requestFocus();
        }
        else
        {
            TxtOpt2IncVer.setValue(new Integer(0));
            TxtOpt2IncVer.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
void ChkOpt8_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt8.isSelected() && ChkOpt6.isSelected())
        {
            ChkOpt7.setSelected(false);
            TxtOpt2IncVer.setValue(new Integer(0));
            TxtOpt2IncVer.setEnabled(false);
            TxtOpt2IncRev.setEnabled(true);
            TxtOpt2IncRev.requestFocus();
        }
        else
        {
            TxtOpt2IncRev.setValue(new Integer(0));
            TxtOpt2IncRev.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
    void ChkOpt9_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt9.isSelected())
        {
            ChkOpt10.setEnabled(true);
            ChkOpt11.setEnabled(true);
            ChkOpt10.setSelected(true);
        }
        else
        {
            ChkOpt10.setEnabled(false);
            ChkOpt11.setEnabled(false);
            ChkOpt10.setSelected(false);
            ChkOpt11.setSelected(false);
            ChkOpt10.setSelected(false);
            ChkOpt11.setSelected(false);
        }
        ChkOpt10_actionPerformed(null);
        ChkOpt11_actionPerformed(null);
    }
//------------------------------------------------------------------------------
void ChkOpt10_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt10.isSelected() && ChkOpt9.isSelected())
        {
            ChkOpt11.setSelected(false);
            TxtOpt3IncRev.setValue(new Integer(0));
            TxtOpt3IncRev.setEnabled(false);
            TxtOpt3IncVer.setEnabled(true);
            TxtOpt3IncVer.requestFocus();
        }
        else
        {
            TxtOpt3IncVer.setValue(new Integer(0));
            TxtOpt3IncVer.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
void ChkOpt11_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(ChkOpt11.isSelected() && ChkOpt9.isSelected())
        {
            ChkOpt10.setSelected(false);
            TxtOpt3IncVer.setValue(new Integer(0));
            TxtOpt3IncVer.setEnabled(false);
            TxtOpt3IncRev.setEnabled(true);
            TxtOpt3IncRev.requestFocus();
        }
        else
        {
            TxtOpt3IncRev.setValue(new Integer(0));
            TxtOpt3IncRev.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
    public void saveVRSettings()
    {                
        try{
                AdminWise.adminPanel.setWaitPointer();
            String param="";
            String opt1="0",opt2="0",opt3="0",optValue1="0",optValue2="0",optValue3="0";
            if(ChkOpt3.isSelected() && ChkOpt4.isSelected())
            {
                opt1="1";
                optValue1=TxtOpt1IncVer.getValue().toString();
            }
            else if(ChkOpt3.isSelected() && ChkOpt5.isSelected())
            {
                opt1="2";
                optValue1=TxtOpt1IncRev.getValue().toString();
            }
            if(ChkOpt6.isSelected() && ChkOpt7.isSelected())
            {
                opt2="1";
                optValue2=TxtOpt2IncVer.getValue().toString();
            }
            else if(ChkOpt6.isSelected() && ChkOpt8.isSelected())
            {
                opt2="2";
                optValue2=TxtOpt2IncRev.getValue().toString();
            }
            if(ChkOpt9.isSelected() && ChkOpt10.isSelected())
            {
                opt3="1";
                optValue3=TxtOpt3IncVer.getValue().toString();
            }
            else if(ChkOpt9.isSelected() && ChkOpt11.isSelected())
            {
                opt3="2";
                optValue3=TxtOpt3IncRev.getValue().toString();
            }
            String initVersion=TxtInitVersion.getValue().toString();
            String initRevision=TxtInitRevision.getValue().toString();
            DocType docType=new DocType(curDocTypeId);
            docType.setVREnable((ChkEnable.isSelected()?"1":"0")); 
            docType.setTag("0");
            if(docType.getVREnable().equals("1") && !alreadyEnabled)
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                VWMessage.MSG_ATSETTING_CHANGED)==javax.swing.JOptionPane.YES_OPTION)
            docType.setTag("1");
            docType.setVRInitVersion((initVersion.equals("")?"0":initVersion));
            docType.setVRInitRevision((initRevision.equals("")?"0":initRevision));
            docType.setVRPagesChange(opt1);
            docType.setVRIncPagesChange((optValue1.equals("0")?"1":optValue1));
            docType.setVRPageChange(opt2);
            docType.setVRIncPageChange((optValue2.equals("0")?"1":optValue2));
            docType.setVRPropertiesChange(opt3);
            docType.setVRIncPropertiesChange((optValue3.equals("0")?"1":optValue3));
            VWDocTypeConnector.setDocTypeVerRev(docType);
        }
        catch(Exception e){}
            finally{AdminWise.adminPanel.setDefaultPointer();}
    }
//------------------------------------------------------------------------------
    public void loadSettings(int docTypeId,boolean force)
    {
        if(!force && curDocTypeId==docTypeId) return;
        String str="0";
        curDocTypeId=docTypeId;
        DocType docType=VWDocTypeConnector.getDocTypeVerRev(curDocTypeId);
        if(docType==null || docType.getVREnable().equals("0"))
        {
            ChkEnable.setSelected(false);
            ChkEnable_actionPerformed(null);
            alreadyEnabled=false;
            return;
        }
        alreadyEnabled=true;
        ChkEnable.setSelected(true);
        ChkEnable_actionPerformed(null);
        TxtInitVersion.setValue(new Integer(docType.getVRInitVersion()));
        TxtInitRevision.setValue(new Integer(docType.getVRInitRevision()));
        if(docType.getVRPagesChange().equals("0"))
        {
            ChkOpt3.setSelected(false);
            ChkOpt3_actionPerformed(null);
        }
        else if(docType.getVRPagesChange().equals("1"))
        {
            ChkOpt3.setSelected(true);
            ChkOpt3_actionPerformed(null);
            ChkOpt4.setSelected(true);
            ChkOpt4_actionPerformed(null);
            TxtOpt1IncVer.setValue(new Integer(docType.getVRIncPagesChange()));
        }
        else if(docType.getVRPagesChange().equals("2"))
        {
            ChkOpt3.setSelected(true);
            ChkOpt3_actionPerformed(null);
            ChkOpt5.setSelected(true);
            ChkOpt5_actionPerformed(null);
            TxtOpt1IncRev.setValue(new Integer(docType.getVRIncPagesChange()));
        }
        if(docType.getVRPageChange().equals("0"))
        {
            ChkOpt6.setSelected(false);
            ChkOpt6_actionPerformed(null);
        }
        else if(docType.getVRPageChange().equals("1"))
        {
            ChkOpt6.setSelected(true);
            ChkOpt6_actionPerformed(null);
            ChkOpt7.setSelected(true);
            ChkOpt7_actionPerformed(null);
            TxtOpt2IncVer.setValue(new Integer(docType.getVRIncPageChange()));
        }
        else if(docType.getVRPageChange().equals("2"))
        {
            ChkOpt6.setSelected(true);
            ChkOpt6_actionPerformed(null);
            ChkOpt8.setSelected(true);
            ChkOpt8_actionPerformed(null);
            TxtOpt2IncRev.setValue(new Integer(docType.getVRIncPageChange()));
        }
        if(docType.getVRPropertiesChange().equals("0"))
        {
            ChkOpt9.setSelected(false);
            ChkOpt9_actionPerformed(null);
        }
        else if(docType.getVRPropertiesChange().equals("1"))
        {
            ChkOpt9.setSelected(true);
            ChkOpt9_actionPerformed(null);
            ChkOpt10.setSelected(true);
            ChkOpt10_actionPerformed(null);
            TxtOpt3IncVer.setValue(new Integer(docType.getVRIncPropertiesChange()));
        }
        else if(docType.getVRPropertiesChange().equals("2"))
        {
            ChkOpt9.setSelected(true);
            ChkOpt9_actionPerformed(null);
            ChkOpt11.setSelected(true);
            ChkOpt11_actionPerformed(null);
            TxtOpt3IncRev.setValue(new Integer(docType.getVRIncPropertiesChange()));
        }
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt("x", this.getX());
            prefs.putInt("y", this.getY());
        }
        else
        {
            prefs.putInt("x",50);
            prefs.putInt("y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
    private void installSpinner(JSpinner spn)
    {
        JFormattedTextField ftf=((JSpinner.DefaultEditor)spn.getEditor()).getTextField();
        ftf.setEditable(false); 
        ftf.setColumns(3);
    }
//------------------------------------------------------------------------------
    private int curDocTypeId=0;
    private boolean alreadyEnabled=false;
}
