package ViewWise.AdminWise.VWDocType;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar.Separator;
import javax.swing.border.EtchedBorder;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWAutoMail.VWAutoMailDlg;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.VWDlgAssignRouteSetting;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.common.Constants;
import com.computhink.common.Index;
import com.computhink.common.util.JTextFieldLimit;
import com.computhink.resource.ResourceManager;

public class VWDocTypePanel extends JPanel implements VWConstant {
    public VWDocTypePanel() {
        setupUI();
    }
    //--------------------------------------------------------------------------
    public void setupUI() {
	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWDocType.setLayout(null);
        add(VWDocType,BorderLayout.CENTER);
        VWDocType.setBounds(0,0,605,446);
        PanelCommand.setLayout(null);
        VWDocType.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());        
        PanelCommand.setBounds(0,0,168,445);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);
*/      
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        PanelCommand.add(CmbDocType);
        CmbDocType.setBackground(java.awt.Color.white);
        //CmbDocType.setBorder(etchedBorder);
        CmbDocType.setBounds(12,top,144, AdminWise.gSmallBtnHeight);
        TxtDocTypeName.setVisible(false);
        PanelCommand.add(TxtDocTypeName);
        TxtDocTypeName.setBackground(java.awt.Color.white);
        TxtDocTypeName.setBorder(etchedBorder);
        TxtDocTypeName.setBounds(12,top,144, AdminWise.gSmallBtnHeight);
        TxtDocTypeName.setDocument(new JTextFieldLimit(255));
        TxtDocTypeName.setToolTipText(AdminWise.connectorManager.getString("docTypePanel.docTypeToolTip"));
        top += hGap + 6;
        BtnNew.setText(BTN_NEWDT_NAME);
        BtnNew.setActionCommand(BTN_NEWDT_NAME);
        PanelCommand.add(BtnNew);
//        BtnNew.setBackground(java.awt.Color.white);
        BtnNew.setBounds(12,top,144 + 4, AdminWise.gBtnHeight);
        BtnNew.setIcon(VWImages.NewDocTypeIcon);
        BtnUpdate.setText(BTN_UPDATE_NAME);
        BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
        PanelCommand.add(BtnUpdate);
//        BtnUpdate.setBackground(java.awt.Color.white);
        top += hGap;
        BtnUpdate.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnUpdate.setIcon(VWImages.UpdateIcon);
        BtnUpdateKey.setText(BTN_UPDATE_KEY_NAME);
        BtnUpdateKey.setActionCommand(BTN_UPDATE_KEY_NAME);
        BtnUpdateKey.setIcon(VWImages.UpdateIcon);
        PanelCommand.add(BtnUpdateKey);
        top += hGap;
        BtnUpdateKey.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnDelete.setText(BTN_DEL_NAME);
        BtnDelete.setActionCommand(BTN_DEL_NAME);
        BtnDelete.setIcon(VWImages.DelIcon);
        PanelCommand.add(BtnDelete);
//        BtnDelete.setBackground(java.awt.Color.white);
        top += hGap;
        BtnDelete.setBounds(12,top,144, AdminWise.gBtnHeight);

        BtnVerRev.setText(BTN_VERREV_NAME);
        BtnVerRev.setActionCommand(BTN_VERREV_NAME);
        PanelCommand.add(BtnVerRev);
//        BtnVerRev.setBackground(java.awt.Color.white);
        top += hGap + 12;
        BtnVerRev.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnVerRev.setIcon(VWImages.VerRevIcon);

        /**
         * Added for Lint to External Data - Vijaypriya.B.K
         */
        BtnDBLookup.setText(BTN_DBLOOKUP_NAME);
        PanelCommand.add(BtnDBLookup);
        // BtnNewIndex.setBackground(java.awt.Color.white);
        top += hGap;
        BtnDBLookup.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnDBLookup.setIcon(VWImages.DBLookupIcon);

        
        
        BtnAssignRoute.setText(BTN_ASSIGNROUTE);
        BtnAssignRoute.setActionCommand(BTN_ASSIGNROUTE);
        PanelCommand.add(BtnAssignRoute);
//        BtnAssignRoute.setBackground(java.awt.Color.white);
        top += hGap;
        BtnAssignRoute.setBounds(12,top,144,AdminWise.gBtnHeight);//12,475,144,AdminWise.gBtnHeight
        BtnAssignRoute.setIcon(VWImages.AssignRouteIcon);
        
                
        
        BtnAutoMail.setText(BTN_AUTOMAIL_NAME);
        BtnAutoMail.setActionCommand(BTN_AUTOMAIL_NAME);
        PanelCommand.add(BtnAutoMail);
//        BtnAutoMail.setBackground(java.awt.Color.white);
        top += hGap;
        BtnAutoMail.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnAutoMail.setIcon(VWImages.MailIcon);
                
        BtnNewIndex.setText(BTN_NEWINDEX_NAME);
        BtnNewIndex.setActionCommand(BTN_REFRESH_NAME);
        PanelCommand.add(BtnNewIndex);
//        BtnNewIndex.setBackground(java.awt.Color.white);
        top += hGap + 12;
        BtnNewIndex.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnNewIndex.setIcon(VWImages.NewIcon);

        ChkEnableSIC.setText("<html><br>"+"  "+AdminWise.connectorManager.getString("docTypePanel.ChkEnableSIC")+"</html>");
        ChkEnableSIC.setToolTipText(CHK_SIC_NAME);
        PanelCommand.add(ChkEnableSIC);
        ChkEnableSIC.setBackground(AdminWise.getAWColor());
        top += hGap;
        ChkEnableSIC.setBounds(20,top,144, 48);//12,530,144,AdminWise.gBtnHeight)

        /**
         * Retention commented as new enhancement created for 
         * applying retention poliy as a seperate tag
         */
//      Valli code for Retention
        /* Purpose:  Position changed for ARS service Master enable and disable
        * Created By: C.Shanmugavalli		Date:	20 Sep 2006
        */
        /*BtnRetention.setText(BTN_RETENTION_NAME);
        BtnRetention.setActionCommand(BTN_RETENTION_NAME);
        PanelCommand.add(BtnRetention);
//        BtnRetention.setBackground(java.awt.Color.white);
        BtnRetention.setBounds(12, 440, 144, AdminWise.gBtnHeight);
        BtnRetention.setIcon(VWImages.RetentionIcon);*/
        
 
        

        
        
        
        PanelTable.setBounds(170,0,700,700);
        VWDocType.add(PanelTable);
        PanelTable.setLayout(new BorderLayout());
        PanelTable.add(spliter,BorderLayout.CENTER);
        PanelList.setLayout(new BorderLayout());
        PanelList.add(JLabel1,BorderLayout.NORTH);
        PanelList.add(SPIndicesTable,BorderLayout.CENTER);
        PanelList.add(toolBar,BorderLayout.EAST);
        PanelList.setBackground(Color.white);
        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        toolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        toolBar.setSize(16,16);
        toolBar.add(BtnIndexNew);
        BtnIndexNew.setToolTipText(TOOLTIP_INDEXNEW);
        BtnIndexNew.setBackground(toolBar.getBackground());
        Separator[] separators=new Separator[6];
        for(int i=0;i<6;i++) {
            separators[i]=new Separator();
            separators[i].setOrientation(javax.swing.SwingConstants.VERTICAL);
        }
        toolBar.add(separators[0]);
        BtnIndexAdd.setToolTipText(TOOLTIP_INDEXADD);
        BtnIndexAdd.setBackground(toolBar.getBackground());
        toolBar.add(BtnIndexAdd);
        toolBar.add(separators[1]);
        BtnIndexDel.setToolTipText(TOOLTIP_INDEXDEL);
        BtnIndexDel.setBackground(toolBar.getBackground());
        toolBar.add(BtnIndexDel);
        toolBar.add(separators[2]);
        BtnIndexClear.setToolTipText(TOOLTIP_INDEXCLEAR);
        BtnIndexClear.setBackground(toolBar.getBackground());
        toolBar.add(BtnIndexClear);
        toolBar.add(separators[3]);
        BtnIndexUp.setBackground(toolBar.getBackground());
        BtnIndexUp.setToolTipText(TOOLTIP_INDEXUP);
        toolBar.add(BtnIndexUp);
        toolBar.add(separators[4]);
        BtnIndexDown.setBackground(toolBar.getBackground());
        BtnIndexDown.setToolTipText(TOOLTIP_INDEXDOWN);
        toolBar.add(BtnIndexDown);
        toolBar.add(separators[5]);
        JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
        JLabel1.setBackground(Color.WHITE);
        JLabel1.setForeground(new Color(7, 38, 58));
        JLabel2.setText(LBL_INDEXPROP_NAME);
        JLabel2.setBackground(Color.WHITE);
        JLabel2.setForeground(new Color(7, 38, 58));
        JLabel2.setLabelFor(SPPropTable);
        PanelProp.setLayout(new BorderLayout());
        PanelProp.add(JLabel2,BorderLayout.NORTH);
        PanelProp.add(SPPropTable,BorderLayout.CENTER);
        PanelProp.setBackground(Color.white);
        spliter.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        spliter.setTopComponent(PanelList);
        spliter.setBottomComponent(PanelProp);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.DocTypeImage);
        SymAction lSymAction = new SymAction();
        BtnNew.addActionListener(lSymAction);
        BtnUpdate.addActionListener(lSymAction);
        BtnUpdateKey.addActionListener(lSymAction);
        BtnDelete.addActionListener(lSymAction);
        BtnNewIndex.addActionListener(lSymAction);
        BtnDBLookup.addActionListener(lSymAction);
        BtnIndexNew.addActionListener(lSymAction);
        BtnIndexAdd.addActionListener(lSymAction);
        BtnIndexDel.addActionListener(lSymAction);
        BtnIndexClear.addActionListener(lSymAction);
        BtnIndexUp.addActionListener(lSymAction);
        BtnIndexDown.addActionListener(lSymAction);
        CmbDocType.addActionListener(lSymAction);
        BtnVerRev.addActionListener(lSymAction);
        //Valli code for Retention
        BtnRetention.addActionListener(lSymAction);
        BtnAutoMail.addActionListener(lSymAction);
        BtnAssignRoute.addActionListener(lSymAction);
        
        IndicesTable.getParent().setBackground(java.awt.Color.white);
        PropTable.getParent().setBackground(java.awt.Color.white);
        spliter.setDividerLocation(0.5);
        spliter.doLayout();
        IndicesTable.getSelectionModel().addListSelectionListener
        (new VWListSelectionListener());
        resizePanel();
        UILoaded=true;
    }
    //--------------------------------------------------------------------------
    class VWListSelectionListener implements javax.swing.event.ListSelectionListener {
        public void valueChanged(javax.swing.event.ListSelectionEvent e) {
            loadIndexProperties((VWIndexRec) IndicesTable.getRowData
            (IndicesTable.getSelectedRow()),false);
            if(curMode==MODE_NEW || curMode==MODE_UPDATE||curMode==MODE_UPDATE_KEY)
                setIndexEnableMode(MODE_SELECTINDEX);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnNew)
                BtnNew_actionPerformed(event);
            else if (object == BtnUpdate)
                BtnUpdate_actionPerformed(event);
            else if (object == BtnDelete)
                BtnDelete_actionPerformed(event);
            else if(object == BtnUpdateKey)
            	BtnUpdateKey_actionPerformed(event);
            else if (object == BtnNewIndex)
                BtnNewIndex_actionPerformed(event);
            else if (object == BtnDBLookup)
            	BtnDBLookup_actionPerformed(event);
            else if (object == BtnVerRev)
                BtnVerRev_actionPerformed(event);
            //Valli code for Retention
            else if (object == BtnRetention)
            	BtnRetention_actionPerformed(event);
            else if (object == BtnAutoMail)
                BtnAutoMail_actionPerformed(event);
            else if (object == BtnIndexNew)
                BtnIndexNew_actionPerformed(event);
            else if (object == BtnIndexAdd)
                BtnIndexAdd_actionPerformed(event);
            else if (object == BtnIndexDel)
                BtnIndexDel_actionPerformed(event);
            else if (object == BtnIndexClear)
                BtnIndexClear_actionPerformed(event);
            else if (object == BtnIndexUp)
                BtnIndexUp_actionPerformed(event);
            else if (object == BtnIndexDown)
                BtnIndexDown_actionPerformed(event);
            else if (object == CmbDocType)
                CmbDocType_actionPerformed(event);
            else if (object == BtnAssignRoute)
            	BtnAssignRoute_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    public javax.swing.JSplitPane spliter = new javax.swing.JSplitPane();
    javax.swing.JPanel VWDocType = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    //VWPanel PanelCommand = new VWPanel(VWImages.bgIcon);
    VWPicturePanel picPanel = new VWPicturePanel(TAB_DOCTYPES_NAME, VWImages.DocTypeImage);
    javax.swing.JPanel PanelList = new javax.swing.JPanel();
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JPanel PanelProp = new javax.swing.JPanel();
    javax.swing.JToolBar toolBar = new javax.swing.JToolBar();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnNew = new VWLinkedButton(1);
    VWLinkedButton BtnUpdate = new VWLinkedButton(1);
    VWLinkedButton BtnUpdateKey = new VWLinkedButton(1);
    VWLinkedButton BtnDelete = new VWLinkedButton(1);
    VWLinkedButton BtnVerRev = new VWLinkedButton(1);
    VWLinkedButton BtnRetention = new VWLinkedButton(1);
    VWLinkedButton BtnAutoMail = new VWLinkedButton(1);
    VWLinkedButton BtnNewIndex = new VWLinkedButton(1);
    VWLinkedButton BtnDBLookup = new VWLinkedButton(1);
    VWLinkedButton BtnIndexNew = new VWLinkedButton(VWImages.NewIcon,false);
    VWLinkedButton BtnIndexAdd = new VWLinkedButton(VWImages.AddIcon,false);
    VWLinkedButton BtnIndexDel = new VWLinkedButton(VWImages.DelIcon,false);
    VWLinkedButton BtnIndexClear = new VWLinkedButton(VWImages.ClearIcon,false);
    VWLinkedButton BtnIndexDown = new VWLinkedButton(VWImages.DownIcon,false);
    VWLinkedButton BtnIndexUp = new VWLinkedButton(VWImages.UpIcon,false);
    
    VWLinkedButton BtnAssignRoute = new VWLinkedButton(1);
    
    public static VWComboBox CmbDocType = new VWComboBox();
    public static VWDTIndexTable IndicesTable = new VWDTIndexTable();
    public static VWIndexPropTable PropTable = new VWIndexPropTable();
    javax.swing.JScrollPane SPIndicesTable = new javax.swing.JScrollPane(IndicesTable);
    javax.swing.JScrollPane SPPropTable = new javax.swing.JScrollPane(PropTable);
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JTextField TxtDocTypeName = new javax.swing.JTextField();
    EtchedBorder etchedBorder = new EtchedBorder
    (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
    javax.swing.JCheckBox ChkEnableSIC = new javax.swing.JCheckBox();
    //}}
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object instanceof VWDocTypePanel)
                VWDocType_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWDocType_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource() instanceof VWDocTypePanel) {
            resizePanel();
        }
    }
    private void resizePanel()
    {
        Dimension mainDimension=this.getSize();
        Dimension PanelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        PanelDimension.width=mainDimension.width-commandDimension.width;
        PanelDimension.height=mainDimension.height;
        PanelCommand.setSize(commandDimension);
        PanelTable.setSize(PanelDimension);
        AdminWise.adminPanel.MainTab.repaint();
        spliter.setDividerLocation(0.5);
        spliter.doLayout();
        doLayout();
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        stopGridEdit();
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            IndicesTable.clearData();
            PropTable.clearData();
            CmbDocType.removeAllItems();
            setEnableMode(MODE_UNCONNECT);
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        IndicesTable.clearData();
        PropTable.clearData();
        loadDocTypes();
        enableSearchInContaints=VWDocTypeConnector.getIsSearchICEnable();
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        stopGridEdit();
        IndicesTable.clearData();
        PropTable.clearData();
        CmbDocType.removeAllItems();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnNew_actionPerformed(java.awt.event.ActionEvent event) {
        if(BtnNew.getText().equals(BTN_NEWDT_NAME)) {
            stopGridEdit();
            JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
            IndicesTable.clearData();
            PropTable.clearData();
            setEnableMode(MODE_NEW);
        }
        else if(BtnNew.getText().equals(BTN_SAVE_NAME)) {
            try{
                AdminWise.adminPanel.setWaitPointer();
                stopGridEdit();
                //JOptionPane.showMessageDialog(null,"Hi1 "+IndicesTable.getRowCount(),"",JOptionPane.OK_OPTION);
                if(IndicesTable.getRowCount()==0) {
                    if(curMode==MODE_NEWINDEX) {
                        VWMessage.showMessage(this,VWMessage.ERR_NOINDICES);
                        return;
                    }
                    else {
                        VWMessage.showMessage(this,VWMessage.ERR_DOCTYPE_NOINDICES);
                        return;
                    }
                }
                //JOptionPane.showMessageDialog(null,"Hi2 "+curIndexRec,"",JOptionPane.OK_OPTION);
                if(curIndexRec !=null) curIndexRec.setSubInfo(PropTable.getValues());
                if(!validateDocTypeData()) return;
                //JOptionPane.showMessageDialog(null,"Hi3 curMode "+curMode,"",JOptionPane.OK_OPTION);
                if(curMode==MODE_NEW)
                    updateDocType(0);
                else if(curMode==MODE_UPDATE||curMode==MODE_UPDATE_KEY) {
                    if(deletedIndices !=null && deletedIndices.size()>0)
                        deleteDocTypeIndices();
                    updateDocType(((VWDocTypeRec)CmbDocType.getSelectedItem()).getId());
                    deletedIndices=new LinkedList();
                    if(enableSearchInContaints && !isDocTypeESIC && ChkEnableSIC.isSelected()
                    && AdminWise.adminPanel.confirmEnableSIC &&
                    VWMessage.showConfirmDialog((java.awt.Component) this,
                    		AdminWise.connectorManager.getString("docTypePanel.msg_1")+((VWDocTypeRec)
                    CmbDocType.getSelectedItem()).getName()+
                    AdminWise.connectorManager.getString("docTypePanel.msg_2"))==javax.swing.JOptionPane.YES_OPTION) {
                        List docIdList=VWDocTypeConnector.getDTDocumentsNotInIndexer
                        (((VWDocTypeRec)CmbDocType.getSelectedItem()).getId());
                        if(docIdList!=null && docIdList.size()>0) {
                            indexerInfoDlg=new VWIndexerInfoDlg(
                                AdminWise.adminFrame,docIdList);
                        }
                    }
                }
                else if(curMode==MODE_NEWINDEX) {
                    saveIndices();
                    IndicesTable.clearData();
                }
                //JOptionPane.showMessageDialog(null,"Hi4 "+((VWDocTypeRec)CmbDocType.getSelectedItem()).getId(),"",JOptionPane.OK_OPTION);
                VWDocTypeRec selDocType = (VWDocTypeRec)CmbDocType.getSelectedItem();
                loadDocTypeData();
                /*
                if(selDocType.getId()>0)
                {
                    setEnableMode(MODE_SELECT);
                }
                else
                {
                    setEnableMode(MODE_UNSELECTED);
                }
                 */
            }
            catch(Exception e){}
            finally{AdminWise.adminPanel.setDefaultPointer();}
        }
    }
    //--------------------------------------------------------------------------
    void BtnUpdate_actionPerformed(java.awt.event.ActionEvent event) {
        if(BtnUpdate.getText().equals(BTN_UPDATE_NAME)) {
            stopGridEdit();
            int docTypeId=((VWDocTypeRec)CmbDocType.getSelectedItem()).getId();
            VWDocTypeRec docTypeRec=VWDocTypeConnector.getDocTypeInfo(docTypeId);
            isDocTypeESIC=docTypeRec.getEnableSIC();
            VWDocTypeRec dDocTypeRec=(VWDocTypeRec)CmbDocType.getSelectedItem();
            if(dDocTypeRec !=null) {
                dDocTypeRec.setName(docTypeRec.getName());
                dDocTypeRec.setUsed(docTypeRec.getUsed());
            }
            setEnableMode(MODE_UPDATE);
            deletedIndices =new LinkedList();
        }
        else if(BtnUpdate.getText().equals(BTN_CANCEL_NAME)) {
            stopGridEdit();
            CmbDocType.setSelectedIndex(CmbDocType.getSelectedIndex());
            setIndexEnableMode(MODE_DISABLETOOLBAR);
            if(((VWDocTypeRec)CmbDocType.getSelectedItem()).getId()==-1){
                setEnableMode(MODE_UNSELECTED);
                JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
            }else{
                setEnableMode(MODE_SELECT);
                VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
                	JLabel1.setText(AdminWise.connectorManager.getString("docTypePanel.JLabel_1")+selDocType.getName()+" - "+selDocType.getId());
                }
            deletedIndices =new LinkedList();
            //JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
        }
    }
    //--------------------------------------------------------------------------
    /*Method added for the updatekey button added to document key field changes
     *Release :Build5-001
     *Modified By Madhavan.
     */
    void BtnUpdateKey_actionPerformed(java.awt.event.ActionEvent event) {
    	if(BtnUpdateKey.getText().equals(BTN_UPDATE_KEY_NAME)) {
    		stopGridEdit();
    		int count=0;
    		try{
    			int docTypeId=((VWDocTypeRec)CmbDocType.getSelectedItem()).getId();
    			VWDocTypeRec docTypeRec=VWDocTypeConnector.getDocTypeInfo(docTypeId);
    			isDocTypeESIC=docTypeRec.getEnableSIC();
    			VWDocTypeRec dDocTypeRec=(VWDocTypeRec)CmbDocType.getSelectedItem();
    			VWIndexRec[] indices=IndicesTable.getData();

    			if(indices != null && indices.length > 0){
    				count=indices.length;
    				for(int i=0;i<count;i++) {
    					if(indices[i].getKey()){
    						oldKeyIndexId = indices[i].getId();
    					}
    				}        		
    			}
    			if(dDocTypeRec !=null) {
    				dDocTypeRec.setName(docTypeRec.getName());
    				dDocTypeRec.setUsed(docTypeRec.getUsed());
    			}
    			setEnableMode(MODE_UPDATE_KEY);
    			deletedIndices =new LinkedList();
    		}
    		catch(Exception ex){}
    	}

    }
    //--------------------------------------------------------------------------
    void BtnDelete_actionPerformed(java.awt.event.ActionEvent event) {
        VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
        if(selDocType.getUsed()) {
            BtnDelete.setEnabled(false);
            return;
        }
        
        /**Added for CV2020 issue fix - Create a New document type in administration and create a document in DTC.Now delete the doc type from administration and refresh it.Again doc type is found in the list**/
        int docTypeId=selDocType.getId();
		VWDocTypeRec docTypeRec=VWDocTypeConnector.getDocTypeInfo(docTypeId);
		AdminWise.printToConsole("document type information :"+docTypeRec);
        if (docTypeRec != null && docTypeRec.getUsed()) {
        	AdminWise.printToConsole("is doc type in use? : "+docTypeRec.getUsed());
        	VWMessage.showNoticeDialog((java.awt.Component) this, VWMessage.ERR_DOCINUSE);
        	BtnDelete.setEnabled(false);
        	return;
        }
        /************************************************************************************/
        
        if(AdminWise.adminPanel.confirmDelDocType)
            if(VWMessage.showConfirmDialog((java.awt.Component) this,
            		AdminWise.connectorManager.getString("docTypePanel.mag_3")+ selDocType.getName() +")?")!=
            javax.swing.JOptionPane.YES_OPTION) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            deleteDocType(selDocType);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    private boolean deleteDocType(VWDocTypeRec selDocType) {
        stopGridEdit();
        if(selDocType==null) return false;
        if(selDocType.getUsed())
            return false;
        VWDocTypeConnector.removeDocType(selDocType);
        VWIndexRec[] indices=IndicesTable.getData();
        for(int i=0;i<indices.length;i++) {
            if(indices[i].getIndexUsed()<=1 && AdminWise.adminPanel.confirmUnusedIndex) {
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                MSG_UNUSEDINDEX_1+" (" + indices[i].getName() +") " +MSG_UNUSEDINDEX_2 
                )==javax.swing.JOptionPane.YES_OPTION)
                    VWDocTypeConnector.removeIndex(indices[i]);
            }
        }
        CmbDocType.removeItemAt(CmbDocType.getSelectedIndex());
        clearDocTypeDetails();
        CmbDocType.setSelectedIndex(0);
        setEnableMode(MODE_UNSELECTED);
        return true;
    }
    //--------------------------------------------------------------------------
    private int deleteDocTypeIndices() {
        if(deletedIndices==null || deletedIndices.size()==0) return 0;
        for(int i=0;i<deletedIndices.size();i++) {
            VWIndexRec index=(VWIndexRec)deletedIndices.get(i);
            VWDocTypeConnector.removeDocTypeIndex(index);
            if(index.getIndexUsed()<=1) {
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                MSG_UNUSEDINDEX_1 + " [ " + index.getName() +" ]"+MSG_UNUSEDINDEX_3
                )==javax.swing.JOptionPane.YES_OPTION)
                    VWDocTypeConnector.removeIndex(index);
            }
        }
        return 0;
    }
    //--------------------------------------------------------------------------
    void BtnNewIndex_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        setEnableMode(MODE_NEWINDEX);
        BtnIndexNew_actionPerformed(null);
    }
//  --------------------------------------------------------------------------
    void BtnDBLookup_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        //setEnableMode(MODE_NEWINDEX);
        BtnLookupDB_actionPerformed(null);
    }
    //--------------------------------------------------------------------------
    void BtnVerRev_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        if(settingDlg==null) {
            settingDlg = new VWDlgVRSetting(AdminWise.adminFrame,getSelDTId());
        }
        else {
            settingDlg.loadSettings(getSelDTId(),true);
            settingDlg.setVisible(true);
        }
    }
    //--------------------------------------------------------------------------
    //Valli Method added for Retention
    void BtnRetention_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        if(retentionSettingDlg==null) {
        	retentionSettingDlg = new VWDlgRetentionSettings(AdminWise.adminFrame,getSelDTId());
        }
        else {
        	retentionSettingDlg.loadRetentionSettings(getSelDTId(),true);
        	retentionSettingDlg.setVisible(true);
        }
    }
    
    void BtnAssignRoute_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        //Desc   :Checks weather available and assigned route list is having routes. If not there message is displayed. If available 
        //		  Dialog box is displayed.
        //Author :Nishad Nambiar
        //Date   :28-Nov-2007
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int gCurRoom = room.getId();
        Vector routeList = new Vector();
        AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, 0,routeList);
        Vector assignedRouteList = new Vector();
        AdminWise.gConnector.ViewWiseClient.getAssignedRoutesOfDocType(gCurRoom, getSelDTId(),assignedRouteList);
        if(routeList.size()==0 && assignedRouteList.size()==0){
        	JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("docTypePanel.msg_4")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("docTypePanel.msg_5"));
        }
        else{        
	        if(assignRouteDlg ==null) {
	        	assignRouteDlg = new VWDlgAssignRouteSetting(AdminWise.adminFrame,getSelDTId(),routeList);
	        	assignRouteDlg.setVisible(true);
	        }
	        else {
	        	assignRouteDlg.setVecRouteInfo(routeList);
	        	assignRouteDlg.setVisible(true);
	        	assignRouteDlg.loadData(getSelDTId());
	        }
        }
    }
    //--------------------------------------------------------------------------
    void BtnAutoMail_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
        if(selDocType==null) return;
        String[] autoMailData=new String[3];
        autoMailData[0]=selDocType.getAutoMail_To();
        autoMailData[1]=selDocType.getAutoMail_Cc();
        autoMailData[2]=selDocType.getAutoMail_Subject();
        if(autoMailDlg==null) {
            autoMailDlg = new VWAutoMailDlg(AdminWise.adminFrame);
            autoMailDlg.setData(autoMailData);
            autoMailDlg.setVisible(true);
        }
        else {
            autoMailDlg.setData(autoMailData);
            autoMailDlg.setVisible(true);
        }
        if(autoMailDlg.getCancelFlag()) return;
        String[] newData=autoMailDlg.getValues();
        selDocType.setAutoMail_To(newData[0]);
        selDocType.setAutoMail_Cc(newData[1]);
        selDocType.setAutoMail_Subject(newData[2]);
        VWDocTypeConnector.updateDocTypeAutoMail(selDocType);
    }
    //--------------------------------------------------------------------------
    void BtnIndexNew_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        VWIndexRec newIndexRec=new VWIndexRec(0,0,"New Index " +
        (IndicesTable.getRowCount()+1),0,"","",0,0,false,false,"",false,false,false);
        IndicesTable.insertRowData(newIndexRec);
//        VWIndexRec newIndexRec=new VWIndexRec(0,0,"New Index " +
//              (IndicesTable.getRowCount()+1),0,"","",0,0,false,false,"",false,false);
//              IndicesTable.insertRowData(newIndexRec);
        int newRowIndex=IndicesTable.getRowCount()-1;
        IndicesTable.setRowSelectionInterval(newRowIndex,newRowIndex);
        IndicesTable.editCellAt(newRowIndex,0);
        IndicesTable.requestFocus();
        setIndexEnableMode(MODE_SELECTINDEX);
    }
    //--------------------------------------------------------------------------
    void BtnLookupDB_actionPerformed(java.awt.event.ActionEvent event) {
	VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
	externalDBLookup = new VWExternalDBLookup(AdminWise.adminFrame, AdminWise.connectorManager.getString("docTypePanel.externalDBLookup")+selDocType.getName(), selDocType);
	//externalDBLookup.setTitle("External Database Lookup For - "+selDocType.getName());
    }
    //--------------------------------------------------------------------------
    void BtnIndexAdd_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        List selIndices=VWDocTypeConnector.displayAddIndexDlg(IndicesTable.getData());
        if(selIndices!=null && selIndices.size()>0) {
            for(int i=0;i<selIndices.size();i++) {
                VWIndexRec index=(VWIndexRec)selIndices.get(i);
                index.addIndexUsed();
                if(deletedIndices.contains(index))
                    deletedIndices.remove(index);
            }
            IndicesTable.insertData(selIndices);
        }
        if(IndicesTable.getSelectedRow()<0 && IndicesTable.getRowCount()>0) {
            IndicesTable.setRowSelectionInterval(0,0);
            setIndexEnableMode(MODE_SELECTINDEX);
        }
    }
    //--------------------------------------------------------------------------
    void BtnIndexDel_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        int selRow=IndicesTable.getSelectedRow();
        VWIndexRec delIndex=IndicesTable.getRowData(selRow);
        if(curMode==MODE_UPDATE && !deletedIndices.contains(delIndex) &&
        delIndex.getDTIndexId()>0)
            deletedIndices.add(delIndex);
        IndicesTable.removeData(selRow);
        PropTable.clearData();
        curIndexRec=null;
        if(IndicesTable.getRowCount()>0)
            IndicesTable.setRowSelectionInterval(0,0);
        else
            setIndexEnableMode(MODE_UNSELECTINDEX);
    }
    //--------------------------------------------------------------------------
    void BtnIndexClear_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        if(curMode==MODE_UPDATE || curMode==MODE_NEW || curMode==MODE_NEWINDEX) {
            if(VWMessage.showConfirmDialog((java.awt.Component)this,
            MSG_CLEARINDICES)!=javax.swing.JOptionPane.YES_OPTION) return;
            int i=0;
            while(i<IndicesTable.getRowCount()) {
                VWIndexRec delIndex=IndicesTable.getRowData(i);
                if(delIndex != null) {
                    if(!getSelDTIndexUsed() && delIndex.getDTIndexId()>0) {
                        deletedIndices.add(delIndex);
                        IndicesTable.removeData(i);
                    }
                    else if(delIndex.getDTIndexId()==0) {
                        IndicesTable.removeData(i);
                    }
                    else {
                        i++;
                    }
                }
                else {
                    i++;
                }
            }
            if(IndicesTable.getRowCount()>0) {
                IndicesTable.setRowSelectionInterval(0,0);
            }
            else {
                PropTable.clearData();
                setIndexEnableMode(MODE_UNSELECTINDEX);
            }
        }
    }
    //--------------------------------------------------------------------------
    void BtnIndexUp_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        forceSaveInfo=true;
        IndicesTable.setRowUp(IndicesTable.getSelectedRow());
        forceSaveInfo=false;
        
    }
    //--------------------------------------------------------------------------
    void BtnIndexDown_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        forceSaveInfo=true;
        IndicesTable.setRowDown(IndicesTable.getSelectedRow());
        forceSaveInfo=false;
    }
    //--------------------------------------------------------------------------
    void CmbDocType_actionPerformed(java.awt.event.ActionEvent event) {
        if(!docTypeLoaded) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            loadDocTypeData();
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    private void loadDocTypeData() {
        stopGridEdit();
        VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
        if(selDocType==null) return;
        //JOptionPane.showMessageDialog(null,"DoctypeID "+selDocType.getId(),"",JOptionPane.OK_OPTION);
        if(selDocType.getId()==-1) {
            clearDocTypeDetails();
            setEnableMode(MODE_UNSELECTED);
            JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
        }
        else if (selDocType.getId()==-2) {
            loadDocTypes();
            clearDocTypeDetails();
            setEnableMode(MODE_UNSELECTED);
            JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
        }
        else {
            List indices=VWDocTypeConnector.getDTIndices(selDocType.getId());
            if(indices==null || indices.size()==0) {
                setEnableMode(MODE_UNSELECTED);
                return;
            }
            ChkEnableSIC.setSelected(selDocType.getEnableSIC());
            IndicesTable.addData(indices);
            /*
            String displayStr=selDocType.getDisplayFields();
            for(int i=0;i<displayStr.length();i++)
            {
                int fieldIndex=VWUtil.to_Number(displayStr.substring(i,i+1));
                if(fieldIndex>0)
                    IndicesTable.insertRowData(FixedFields[i]);
            }
             */
            if(IndicesTable.getRowCount()>0) {
                IndicesTable.setRowSelectionInterval(0,0);
                setEnableMode(MODE_SELECT);
            }
            JLabel1.setText(AdminWise.connectorManager.getString("docTypePanel.JLabel_1")+selDocType.getName()+" - "+selDocType.getId());
        }
    }
    //--------------------------------------------------------------------------
    private void setEnableMode(int mode) {
        setEnableMode(mode,false);
    }
    //--------------------------------------------------------------------------
    private void setEnableMode(int mode,boolean force) {
        ///if(!force && curMode==mode) return;
        curMode=mode;
        BtnNew.setVisible(true);
        BtnUpdate.setVisible(true);
        BtnUpdateKey.setVisible(true);
        BtnDelete.setVisible(true);
        BtnNewIndex.setVisible(true);
        BtnVerRev.setVisible(true);
        //Valli code for Retention
        //BtnRetention.setVisible(true);
        BtnAutoMail.setVisible(true);
        BtnNew.setEnabled(true);
        BtnUpdate.setEnabled(true);
        BtnUpdateKey.setEnabled(true);
        BtnVerRev.setEnabled(true);
        BtnAssignRoute.setEnabled(true);
        /**
         * Enhancement for Linking document type with external table
         */
        BtnDBLookup.setVisible(true);
        BtnDBLookup.setEnabled(true);
        
        /* Purpose:  Added for ARS service Master enable and disable
        * Created By: C.Shanmugavalli		Date:	20 Sep 2006
        */
        
        if(VWDocTypeConnector.isARSEnabled()==1){
        	BtnRetention.setVisible(true);
        	BtnRetention.setEnabled(true);
        }else{
        	BtnRetention.setEnabled(false);
        	//BtnRetention.setVisible(false);
        }
        if(VWDocTypeConnector.isDRSEnabled()==1){
        	BtnAssignRoute.setVisible(true);
        	BtnAssignRoute.setEnabled(true);
        }else{
        	BtnAssignRoute.setEnabled(false);
        	//BtnAssignRoute.setVisible(false);
        }
        
        BtnAutoMail.setEnabled(true);
        VWDocTypeRec selDocType=(VWDocTypeRec)CmbDocType.getSelectedItem();
        if(selDocType!=null && selDocType.getUsed())
            BtnDelete.setEnabled(false);
        else
            BtnDelete.setEnabled(true);
        BtnNewIndex.setEnabled(true);
        CmbDocType.setEnabled(true);
        TxtDocTypeName.setVisible(false);
        CmbDocType.setVisible(true);
        //ChkEnableSIC.setVisible(enableSearchInContaints);
        ChkEnableSIC.setVisible(true);
        ChkEnableSIC.setEnabled(false);
        //JLabel1.setText(LBL_DOCTYPEINDICES_NAME_1);
        switch (mode) {
            case MODE_SELECT:
                BtnNew.setText(BTN_NEWDT_NAME);
                BtnNew.setIcon(VWImages.NewDocTypeIcon);
                BtnUpdate.setText(BTN_UPDATE_NAME);
                BtnUpdate.setIcon(VWImages.UpdateIcon);
                BtnUpdateKey.setText(BTN_UPDATE_KEY_NAME);
                BtnUpdateKey.setIcon(VWImages.UpdateIcon);
                setIndexEnableMode(MODE_DISABLETOOLBAR);
                if(IndicesTable.getRowCount()>0)
                    IndicesTable.setRowSelectionInterval(0,0);
                break;
            case MODE_UNSELECTED:
                BtnNew.setText(BTN_NEWDT_NAME);
                BtnNew.setIcon(VWImages.NewDocTypeIcon);
                BtnUpdate.setText(BTN_UPDATE_NAME);
                BtnUpdate.setIcon(VWImages.UpdateIcon);
                BtnUpdate.setEnabled(false);
                BtnUpdateKey.setText(BTN_UPDATE_KEY_NAME);
                BtnUpdateKey.setIcon(VWImages.UpdateIcon);
                BtnUpdateKey.setEnabled(false);
                BtnVerRev.setEnabled(false);
               //Valli code for Retention
                BtnRetention.setEnabled(false);
                BtnAssignRoute.setEnabled(false);

                BtnAutoMail.setEnabled(false);
                BtnDelete.setEnabled(false);
                BtnIndexNew.setEnabled(false);
                BtnIndexAdd.setEnabled(false);
                BtnIndexDel.setEnabled(false);
                BtnIndexClear.setEnabled(false);
                BtnIndexUp.setEnabled(false);
                BtnIndexDown.setEnabled(false);
                ChkEnableSIC.setEnabled(false);
                BtnAssignRoute.setEnabled(false);
                BtnDBLookup.setEnabled(false);
                break;
            case MODE_UPDATE:
                BtnNew.setText(BTN_SAVE_NAME);
                BtnNew.setIcon(VWImages.SaveIcon);
                BtnUpdate.setText(BTN_CANCEL_NAME);
                BtnUpdate.setIcon(VWImages.CloseIcon);
                BtnUpdateKey.setVisible(false);
                BtnDelete.setVisible(false);
                BtnNewIndex.setVisible(false);
                BtnVerRev.setVisible(false);
              //Valli code for Retention
                BtnRetention.setVisible(false);
                BtnAssignRoute.setVisible(false);
                BtnDBLookup.setVisible(false);
                BtnAutoMail.setVisible(false);
                TxtDocTypeName.setVisible(true);
                CmbDocType.setVisible(false);
                ChkEnableSIC.setVisible(true);
                ChkEnableSIC.setEnabled(true);
                setIndexEnableMode(MODE_SELECTINDEX);
                TxtDocTypeName.setText(CmbDocType.getSelectedItem().toString());
                BtnAssignRoute.setEnabled(false);
                TxtDocTypeName.requestFocus();
                break;
            case MODE_UPDATE_KEY:
            	  BtnNew.setText(BTN_SAVE_NAME);
                  BtnNew.setIcon(VWImages.SaveIcon);
                  BtnUpdate.setText(BTN_CANCEL_NAME);
                  BtnUpdate.setIcon(VWImages.CloseIcon);
                  BtnUpdateKey.setVisible(false);
                  BtnDelete.setVisible(false);
                  BtnNewIndex.setVisible(false);
                  BtnVerRev.setVisible(false);
                  BtnRetention.setVisible(false);
                  BtnAssignRoute.setVisible(false);
                  BtnDBLookup.setVisible(false);
                  BtnAutoMail.setVisible(false);
                  TxtDocTypeName.setVisible(true);
                  CmbDocType.setVisible(false);
                  ChkEnableSIC.setVisible(true);
                  ChkEnableSIC.setEnabled(false);
                  TxtDocTypeName.setText(CmbDocType.getSelectedItem().toString());
                  setIndexEnableMode(MODE_DISABLETOOLBAR);
                  BtnAssignRoute.setEnabled(false);
                  TxtDocTypeName.requestFocus();
                  break;
            case MODE_NEW:
                BtnNew.setText(BTN_SAVE_NAME);
                BtnNew.setIcon(VWImages.SaveIcon);
                BtnUpdate.setText(BTN_CANCEL_NAME);
                BtnUpdate.setIcon(VWImages.CloseIcon);
                BtnDelete.setVisible(false);
                BtnNewIndex.setVisible(false);
                BtnVerRev.setVisible(false);
                BtnUpdateKey.setVisible(false);
               //Valli code for Retention
                BtnRetention.setVisible(false);
                BtnAssignRoute.setVisible(false);
                BtnDBLookup.setVisible(false);
                BtnAutoMail.setVisible(false);
                TxtDocTypeName.setText("");
                TxtDocTypeName.setVisible(true);
                //This is used for viewing the dsn info for a new document type
                CmbDocType.setSelectedItem(0);
                CmbDocType.setVisible(false);
                ChkEnableSIC.setEnabled(true);
                setIndexEnableMode(MODE_UNSELECTINDEX);
                ChkEnableSIC.setVisible(true);
                ChkEnableSIC.setSelected(false);
                BtnAssignRoute.setEnabled(false);
                TxtDocTypeName.requestFocus();
                curIndexRec=null;
                break;
            case MODE_NEWINDEX:
                BtnNew.setText(BTN_SAVE_NAME);
                BtnNew.setIcon(VWImages.SaveIcon);
                BtnUpdate.setText(BTN_CANCEL_NAME);
                BtnUpdate.setIcon(VWImages.CloseIcon);
                BtnUpdateKey.setVisible(false);
                BtnDelete.setVisible(false);
                BtnNewIndex.setVisible(false);
                BtnVerRev.setVisible(false);
                //Valli code for Retention
                BtnRetention.setVisible(false);
                BtnAssignRoute.setVisible(false);
                BtnDBLookup.setVisible(false);
                BtnAutoMail.setVisible(false);
                TxtDocTypeName.setText("");
                TxtDocTypeName.setVisible(false);
                //This is used for viewing the dsn info for a new document type
                CmbDocType.setSelectedItem(0);
                CmbDocType.setVisible(false);
                ChkEnableSIC.setVisible(false);
                IndicesTable.clearData();
                BtnAssignRoute.setEnabled(false);
                setIndexEnableMode(MODE_NEWINDEX);
                JLabel1.setText(LBL_INDICESLIST_NAME);
                curIndexRec=null;
                break;
            case MODE_UNCONNECT:
                BtnNew.setText(BTN_NEWDT_NAME);
                BtnNew.setIcon(VWImages.NewDocTypeIcon);
                BtnUpdate.setText(BTN_UPDATE_NAME);
                BtnUpdate.setIcon(VWImages.UpdateIcon);
                BtnUpdateKey.setText(BTN_UPDATE_KEY_NAME);
                BtnUpdateKey.setIcon(VWImages.UpdateIcon);
                BtnNew.setEnabled(false);
                BtnUpdate.setEnabled(false);
                BtnVerRev.setEnabled(false);
                BtnUpdateKey.setEnabled(false);
              //Valli code for Retention
                BtnRetention.setEnabled(false);
                BtnAssignRoute.setEnabled(false);
                BtnAutoMail.setEnabled(false);
                BtnDelete.setEnabled(false);
                BtnNewIndex.setEnabled(false);
                CmbDocType.setEnabled(false);
                BtnIndexNew.setEnabled(false);
                BtnIndexAdd.setEnabled(false);
                BtnIndexDel.setEnabled(false);
                BtnIndexClear.setEnabled(false);
                BtnIndexUp.setEnabled(false);
                BtnIndexDown.setEnabled(false);
                ChkEnableSIC.setVisible(true);
                ChkEnableSIC.setEnabled(false);
                BtnAssignRoute.setEnabled(false);
                BtnDBLookup.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
    //--------------------------------------------------------------------------
    private void setIndexEnableMode(int mode) {
        BtnIndexNew.setEnabled(true);
        BtnIndexAdd.setEnabled(true);
        BtnIndexDel.setEnabled(true);
        BtnIndexClear.setEnabled(true);
        BtnIndexUp.setEnabled(true);
        BtnIndexDown.setEnabled(true);
        switch (mode) {
            case MODE_UNSELECTINDEX:
                BtnIndexDel.setEnabled(false);
                if(IndicesTable.getRowCount()==0)
                    BtnIndexClear.setEnabled(false);
                BtnIndexUp.setEnabled(false);
                BtnIndexDown.setEnabled(false);
                if(curMode==MODE_NEWINDEX)
                    BtnIndexAdd.setEnabled(false);
                break;
            case MODE_SELECTINDEX:
                if(IndicesTable.getRowCount()==1 ||
                IndicesTable.getSelectedRow()==0||
                curMode==MODE_NEWINDEX)
                    BtnIndexUp.setEnabled(false);
                if(IndicesTable.getRowCount()==1 ||
                IndicesTable.getSelectedRow()==IndicesTable.getRowCount()-1||
                curMode==MODE_NEWINDEX)
                    BtnIndexDown.setEnabled(false);
                if(curMode==MODE_UPDATE_KEY)
                {
                	BtnIndexNew.setEnabled(false);
                    BtnIndexAdd.setEnabled(false);
                    BtnIndexDel.setEnabled(false);
                    BtnIndexClear.setEnabled(false);
                    BtnIndexUp.setEnabled(false);
                    BtnIndexDown.setEnabled(false);
                }
	                VWDocTypeRec selDocType=(VWDocTypeRec)CmbDocType.getSelectedItem();
	                VWIndexRec selIndex=IndicesTable.getRowData();
	                if(selDocType !=null && selIndex != null &&
	                selDocType.getUsed() && selIndex.getDTIndexId()>0)
	                    BtnIndexDel.setEnabled(false);
                break;
            case MODE_DISABLETOOLBAR:
                BtnIndexNew.setEnabled(false);
                BtnIndexAdd.setEnabled(false);
                BtnIndexDel.setEnabled(false);
                BtnIndexClear.setEnabled(false);
                BtnIndexUp.setEnabled(false);
                BtnIndexDown.setEnabled(false);
                break;
            case MODE_NEWINDEX:
                BtnIndexUp.setEnabled(false);
                BtnIndexDown.setEnabled(false);
                BtnIndexAdd.setEnabled(false);
                break;
        }
    }
    //--------------------------------------------------------------------------
    private void loadDocTypes() {
        docTypeLoaded=false;
        CmbDocType.removeAllItems();
        CmbDocType.addItem(new VWDocTypeRec(-1,LBL_DOCTYPE_NAME_1,false));
        Vector data=VWDocTypeConnector.getDocTypes();
        if (data!=null)
            for (int i=0;i<data.size();i++)
                CmbDocType.addItem(data.get(i));
        CmbDocType.addItem(new VWDocTypeRec(-2,LBL_REFRESH_NAME,false));
        docTypeLoaded=true;
    }
    //--------------------------------------------------------------------------
    public void clearDocTypeDetails() {
        stopGridEdit();
        curIndexRec=null;
        IndicesTable.clearData();
        PropTable.clearData();
        ChkEnableSIC.setSelected(false);
    }
    //--------------------------------------------------------------------------
    public void loadIndexProperties(VWIndexRec indexRec,boolean typeChange) {
    	 /*Commented stopGridEdit method to highlight the row selection when user
         * clicks key field in update key mode
         * Modified by Madhavan
         */
    	if(curMode!=MODE_UPDATE_KEY)
    		stopGridEdit();
        if(indexRec==null) return;
        if(curIndexRec !=null && (forceSaveInfo || indexRec!=curIndexRec))
            curIndexRec.setSubInfo(PropTable.getValues());
        forceSaveInfo=false;
        curIndexRec=indexRec;
        String[] indexSubData=null;
        if(!typeChange)  indexSubData=indexRec.getSubInfo();
        PropTable.setIndexType(indexRec.getType());
        if(indexRec.getType()==Index.IDX_TEXT) {
            if(indexSubData==null || indexSubData.length==0)
                indexSubData=DefaultTextIndex;
            String[][] data={{IndexTextFields[0],indexSubData[0]},
            {IndexTextFields[1],indexSubData[1]},
            {IndexTextFields[2],indexSubData[2]},
            {IndexTextFields[3],indexSubData[3]}};
            PropTable.addData(data);
        }
        else if(indexRec.getType()==Index.IDX_BOOLEAN) {
            if(indexSubData==null || indexSubData.length==0)
                indexSubData=DefaultBooleanIndex;
            String[][] data={{IndexBoolFields[0],indexSubData[0]},
            {IndexBoolFields[1],indexSubData[1]}};
            PropTable.addData(data);
        }
        else if(indexRec.getType()==Index.IDX_DATE) {
            if(indexSubData==null || indexSubData.length==0)
                indexSubData=DefaultDateIndex;
            String[][] data={{IndexDateFields[0],indexSubData[0]},
            {IndexDateFields[1],indexSubData[1]},
            {IndexDateFields[2],indexSubData[2]},
            {IndexDateFields[3],indexSubData[3]}};
            PropTable.addData(data);
        }
        else if(indexRec.getType()==Index.IDX_SEQUENCE) {
        	/**
        	 * Code added to perform the procedure call validate sequence index 
        	 */
        	
        	int uniqueFlag = VWDocTypeConnector.validateSeqIndex(indexRec.getId());
        	if(indexSubData==null || indexSubData.length==0) {
        		if (uniqueFlag == 0 || uniqueFlag == 4){
        			indexSubData=DefaultSequenceIndex;
        			checkcoditon=true;
        		}
        		else{
        			indexSubData=DefaultUniqueSequenceIndex;
        			checkcoditon=false;
        		}
        	}
        	else{
        		if (uniqueFlag == 0 || uniqueFlag == 4)
        			checkcoditon=true;

        		else
        			checkcoditon=false;

        	}

        	if(indexSubData[4].equals("Unique to Room")){
        		if(getSelIndexUsed())
        			uniqueSeqSetFlag=true;
        		else
        			uniqueSeqSetFlag=false;
        	}else{
        		uniqueSeqSetFlag=false;
        	}//End of modified code for unique to room sequence changes


        	String[][] data={{IndexSequenceFields[0],indexSubData[0]},
        			{IndexSequenceFields[1],indexSubData[1]},
        			{IndexSequenceFields[2],indexSubData[2]},
        			{IndexSequenceFields[3],indexSubData[3]},
        			{IndexSequenceFields[4],indexSubData[4]},
        			{IndexSequenceFields[5],indexSubData[5]}};
        	PropTable.addData(data);
        }
        else if(indexRec.getType()==Index.IDX_SELECTION) {
            if(indexSubData==null || indexSubData.length==0) {
                indexSubData=DefaultSelectionIndex;
                indexSubData[0]="...";///VWDocTypeConnector.getSelectionValuesString();
                indexRec.selectionValues=VWDocTypeConnector.getSelectionValues();
                if(!typeChange) indexSubData[3]=indexRec.getDefault();
            }
            String[][] data={{IndexSelectionFields[0],indexSubData[0]},
            /**
             * "Read Only is not required" for "Selection" data type .
             * Modified By	: Vijaypriya.B.K
             * Date			: 27-11-2008
             * "Read Only is not required" for "Selection" data type. But old doc types will have values for this property. So we can't
             * Remove the row. Code is reverted
             */
            {IndexSelectionFields[1],indexSubData[1]},
            {IndexSelectionFields[2],indexSubData[2]},
            {IndexSelectionFields[3],indexSubData[3]},
            {IndexSelectionFields[4],indexSubData[4]}};
            PropTable.addData(data);
        }
        else if(indexRec.getType()==Index.IDX_NUMBER) {
            if(indexSubData==null || indexSubData.length==0)
                indexSubData=DefaultNumberIndex;
            if(indexSubData[0].equals(VWIndexNumericMask[0])) {
                String[][] data={{IndexNumberFields[0][0],indexSubData[0]},
                {IndexNumberFields[0][1],indexSubData[1]},
                {IndexNumberFields[0][2],indexSubData[2]},
                {IndexNumberFields[0][3],indexSubData[3]}};
                PropTable.addData(data);
            }
            else if(indexSubData[0].equals(VWIndexNumericMask[1])) {
                String[][] data={{IndexNumberFields[0][0],indexSubData[0]},
                {IndexNumberFields[1][1],indexSubData[1]},
                {IndexNumberFields[1][2],indexSubData[2]},
                {IndexNumberFields[1][3],indexSubData[3]},
                {IndexNumberFields[1][4],indexSubData[4]},
                {IndexNumberFields[1][5],indexSubData[5]},
                {IndexNumberFields[1][6],indexSubData[6]}};
                PropTable.addData(data);
            }
            else if(indexSubData[0].equals(VWIndexNumericMask[2])) {
                String[][] data={{IndexNumberFields[0][0],indexSubData[0]},
                {IndexNumberFields[2][1],indexSubData[1]},
                {IndexNumberFields[2][2],indexSubData[2]},
                {IndexNumberFields[2][3],indexSubData[3]},
                {IndexNumberFields[2][4],indexSubData[4]},
                {IndexNumberFields[2][5],indexSubData[5]},
                {IndexNumberFields[2][6],indexSubData[6]},
                {IndexNumberFields[2][7],indexSubData[7]}};
                PropTable.addData(data);
            }
            else if(indexSubData[0].equals(VWIndexNumericMask[3])) {
                String[][] data={{IndexNumberFields[0][0],indexSubData[0]},
                {IndexNumberFields[3][1],indexSubData[1]},
                {IndexNumberFields[3][2],indexSubData[2]},
                {IndexNumberFields[3][3],indexSubData[3]},
                {IndexNumberFields[3][4],indexSubData[4]}};
                PropTable.addData(data);
            }
            else if(indexSubData[0].equals(VWIndexNumericMask[4])) {
                String[][] data={{IndexNumberFields[0][0],indexSubData[0]},
                {IndexNumberFields[4][1],indexSubData[1]},
                {IndexNumberFields[4][2],indexSubData[2]},
                {IndexNumberFields[4][3],indexSubData[3]},
                {IndexNumberFields[4][4],indexSubData[4]}};
                PropTable.addData(data);
            }
        }
    }
    //--------------------------------------------------------------------------
    public boolean getSelIndexUsed() {
        return getSelIndexUsed(IndicesTable.getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public int getSelIndexDTId() {
        int row=IndicesTable.getSelectedRow();
        
        VWIndexRec selIndex=IndicesTable.getRowData(row);
        if(selIndex==null) return 0;
        return selIndex.getDTIndexId();
    }
    //--------------------------------------------------------------------------
    public boolean getSelIndexUsed(int row) {
        VWIndexRec selIndex=IndicesTable.getRowData(row);
        if(selIndex==null) return false;
        if(selIndex.getId()==0) return false;
        if(selIndex.getIndexUsed()==1) return false;
        return true;
    }
    //--------------------------------------------------------------------------
    public boolean getSelDTIndexUsed() {
        return getSelDTIndexUsed(IndicesTable.getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public boolean getSelDTIndexUsed(int row) {
        /*
        VWIndexRec selIndex=IndicesTable.getRowData(row);
        if(selIndex==null) return false;
        if(selIndex.getId()==0) return false;
        if(selIndex.getDTIndexId()==0) return false;
         */
        if(curMode==MODE_NEW) return false;
        VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
        if(!selDocType.getUsed()) return false;
        return true;
    }
    //--------------------------------------------------------------------------
    public boolean getIsFixedIndex() {
        return getIsFixedIndex(IndicesTable.getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public boolean getIsFixedIndex(int row) {
        VWIndexRec selIndex=IndicesTable.getRowData(row);
        if(selIndex!=null && selIndex.getId()<0) return true;
        return false;
    }
    //--------------------------------------------------------------------------
    public int getSelIndexSVid() {
        int row=IndicesTable.getSelectedRow();
        VWIndexRec selIndex=IndicesTable.getRowData(row);
        if(selIndex !=null)
            return selIndex.getSVid();
        return 0;
    }
    //--------------------------------------------------------------------------
    public int getSelIndexId() {
        int row=IndicesTable.getSelectedRow();
        VWIndexRec selIndex=IndicesTable.getRowData(row);
        return selIndex.getId();
    }
    //--------------------------------------------------------------------------
    public int getSelDTId() {
        VWDocTypeRec selDocType=(VWDocTypeRec) CmbDocType.getSelectedItem();
        if(selDocType==null) return 0;
        return selDocType.getId();
    }
    //--------------------------------------------------------------------------
    public String getSelectionIndexValues() {
        if(PropTable.getIndexType()!=Index.IDX_SELECTION) return "";
        String[][] data = PropTable.getData();
        return data[0][1];
    }
    //--------------------------------------------------------------------------
    public void setSelectionIndexValues(Vector newValue) {
        VWIndexRec selIndex=IndicesTable.getRowData(IndicesTable.getSelectedRow());
        selIndex.selectionValues=newValue;
        ///PropTable.updateRowValue(0,newValue);
        PropTable.updateRowValue(0,"...");
    }
    //--------------------------------------------------------------------------
    public void setSelectionIndexDefaultValues(String newValue) {
        PropTable.updateRowValue(3,newValue);
    }
    //--------------------------------------------------------------------------
    public String getSelectionIndexDefaultValues() {
        if(PropTable.getIndexType()!=Index.IDX_SELECTION) return "";
        String[][] data = PropTable.getData();
        return data[3][1];
    }
    //--------------------------------------------------------------------------
    public void updateDocTypeIndices(int docTypeId) {
        VWIndexRec[] indices=IndicesTable.getData();
        int count=indices.length;
        /**
		 * Issue: Saving DSN Info with doctypeId and IndexId with -1. 
		 * So creating DT and Index and then inserting DSNInfo. 
		 */
        VWDocTypeConnector.setDocTypeId(docTypeId);
        
        for(int i=0;i<count;i++) {
            boolean flagUpdateIndex = false;
            if(indices[i].getId()==0) /// add new index
            {
                int newId=VWDocTypeConnector.updateIndex(indices[i]);
                if(newId > 0) indices[i].setId(newId);
                flagUpdateIndex = true;
            }
            /*for update index name when used in one or less doc type*/
            else if(indices[i].getIndexUsed()<=1) /// update index
            {
                VWDocTypeConnector.updateIndex(indices[i]);
                flagUpdateIndex = true;
            }
            if(indices[i].getId()> 0) /// update doctype indices
            {
                int indexDTid=VWDocTypeConnector.updateDTIndex(docTypeId,indices[i],i);
                //VWDocTypeConnector.updateSelectionDSNInfo(docTypeId, indices[i].getId(), indices[i].getDbDSNInfo());
                if(indices[i].getDTIndexId()==0) indices[i].setDTIndexId(indexDTid);
            }
            
            if(indices[i].getId()>0 && indices[i].getType()==Index.IDX_SELECTION && !flagUpdateIndex) /// update doctype indices
            {
                VWDocTypeConnector.updateIndex(indices[i]);
            }            
        }
    }
    //--------------------------------------------------------------------------
    public void saveIndices() {
        VWIndexRec[] indices=IndicesTable.getData();
        int count=indices.length;
        for(int i=0;i<count;i++) {
            int newId=VWDocTypeConnector.updateIndex(indices[i]);
            if(newId > 0) indices[i].setId(newId);
        }
    }
    //--------------------------------------------------------------------------
    /*Method modified for UPDATE_KEY mode to make two procedure calls and to validate the
     * index names which is have index value as empty before changing the document key filed
     * if the index value is empty then it will alert with the index name of the key field
     * *Release :Build5-001
     *Modified By Madhavan.
     */
    public void updateDocType(int docTypeId) {
    	try{
    		VWDocTypeRec docTypeRec=new VWDocTypeRec(docTypeId,TxtDocTypeName.getText(),
    				ChkEnableSIC.isSelected());
    		//JOptionPane.showMessageDialog(null,"DocTypeName "+TxtDocTypeName.getText(),"",JOptionPane.OK_OPTION);
    		Vector indiceslist=new Vector();
    		VWIndexRec[] indices=IndicesTable.getData();
    		int count=0;
    		int newkeyIndexId=0;
    		if(indices != null && indices.length > 0){
    			for(int i=0;i<indices.length;i++) {
    				if(indices[i].getKey()||indices[i].getRequired()){
    					if(indices[i].getKey()){
    						newkeyIndexId=indices[i].getId();
    						indiceslist.add(0, newkeyIndexId);
    					}
    					else{
    						indiceslist.add(indices[i].getId());
    					}
    					count++;
    				}
    			}
    		}
    		String resultString="";
    		String messageString="";
    		int indexCount=0;
    		Vector result=new Vector();
    		/*
    		 * CV10.1 If condition commented :-
    		 * If document index field is "Not Required" and some documents are created with empty value, 
    		 * then from Administration change it to "Required" field but not displaying any validation message like 
    		 * "Cannot store empty value when index is required".
    		 * Date :- 9/6/2017.
    		 * Modified By :- Madhavan
    		 */
    		
    		/*if(curMode==MODE_UPDATE_KEY)
    		{*/
    			VWDocTypeConnector.checkKeyIndex(docTypeRec,indiceslist,count,result);

    			if(result != null && result.size() > 0 && !result.elementAt(0).equals("0")){   			
    				resultString=resultString+result.elementAt(0);
    				StringTokenizer strtokenizer = new StringTokenizer(resultString,",");
    				while(strtokenizer.hasMoreTokens()){
    					if(indexCount<=3){
    						messageString=messageString+strtokenizer.nextToken()+",";
    						indexCount++;
    					}     	
    					if(indexCount==3){
    						messageString=messageString+"\n";
    						indexCount=0;
    					}
    				}
    				messageString=messageString.trim();
    				messageString=messageString.substring(0, messageString.length()-1);
    				VWMessage.showMessage(this,"["+messageString+"]"+VWMessage.ERR_EMPTY_INDEX_VALUE);	

    				return;

    			}
    		//}

    		int newId=VWDocTypeConnector.updateDocType(docTypeRec);
    		if(newId==0) return;
    		updateDocTypeIndices(newId);
    		if(docTypeRec.getId()==0) {
    			docTypeRec.setId(newId);
    			CmbDocType.insertItemAt(docTypeRec,CmbDocType.getItemCount()-1);
    			CmbDocType.setSelectedIndex(CmbDocType.getItemCount()-2);
    		}
    		else {
    			((VWDocTypeRec)CmbDocType.getSelectedItem()).setName(docTypeRec.getName());
    			((VWDocTypeRec)CmbDocType.getSelectedItem()).setEnableSIC
    			(docTypeRec.getEnableSIC());
    		}
    		if(curMode==MODE_UPDATE_KEY){
    			VWDocTypeConnector.updateDocTypeKeyField(oldKeyIndexId,newkeyIndexId,docTypeRec);
    		}
    	}
    	catch(Exception e){}
    }
    //--------------------------------------------------------------------------
    public boolean validateDocTypeData() {
        if(curMode !=MODE_NEWINDEX) {
            String docTypeName=TxtDocTypeName.getText().trim();
            if(docTypeName.equals("")) {
                VWMessage.showMessage(this,VWMessage.ERR_DOCTYPE_NONAME);
                TxtDocTypeName.requestFocus();
                return false;
            }
            if(curMode==MODE_NEW || (curMode==MODE_UPDATE && 
            !CmbDocType.getSelectedItem().toString().equalsIgnoreCase(docTypeName)))
                for(int i=0;i<CmbDocType.getItemCount();i++) {
                    if(docTypeName.equalsIgnoreCase(CmbDocType.getItemAt(i).toString())) {
                        VWMessage.showMessage(this,VWMessage.ERR_DOCTYPENAME_ALREADYEXIST);
                        TxtDocTypeName.requestFocus();
                        return false;
                    }
                }
        }
        VWIndexRec[] indices=IndicesTable.getData();
        int count=indices.length;
        boolean key=false;
        for(int i=0;i<count;i++) {
            if(!key)    key=indices[i].getKey();
            if(indices[i].getName().trim().equals("")) {
                VWMessage.showMessage(this,VWMessage.ERR_INDEX_NONAME);
                IndicesTable.setRowSelectionInterval(i,i);
                return false;
            }
            for(int k=0;k<count;k++)
                if( k!=i && indices[i].getName().trim().equalsIgnoreCase(indices[k].getName().trim())) {
                    VWMessage.showMessage(this,VWMessage.ERR_INDEXNAME_ALREADYEXIST);
                    IndicesTable.setRowSelectionInterval(k,k);
                    return false;
                }
            if(checkIndexName(indices[i])) {
                VWMessage.showMessage(this,VWMessage.ERR_INDEXNAME_ALREADYEXIST);
                IndicesTable.setRowSelectionInterval(i,i);
                return false;
            }
            /*
            if(curMode !=MODE_NEWINDEX)
            {
                if((indices[i].getKey() &&
                    indices[i].getDefault().trim().equals("") &&
                    indices[i].getSVid()!=1)
                    ///||(indices[i].getReadOnly() && indices[i].getDefault().trim().equals("")))
                {
                    VWMessage.showMessage(this,VWMessage.ERR_INDEXDEFAULT_NOTFOUND);
                    IndicesTable.setRowSelectionInterval(i,i);
                    return false;
                }
            }
             */
            if(indices[i].getType()==Index.IDX_DATE) {
                if(indices[i].getMask().equals("")) {
                    VWMessage.showMessage(this,VWMessage.ERR_DATEMASK_NOTFOUND);
                    IndicesTable.setRowSelectionInterval(i,i);
                    PropTable.setRowSelectionInterval(0,0);
                    return false;
                }
            }
              /* Purpose: Additinal issue - 662-01 Can't create document in DTC 
             * if document type is created like key field with Read only yes, doc type is Date, Value type is fixed but Default value is empty
             * if document type is created like key field with Read only yes, doc type is not Date but Default value is empty 
             * Created By : C.Shanmugavalli Date 26 Sep 2006
             * 
             * Condition check added for the following conditions
             * Read Only is Yes, Data type is Key/ Required / Not Required, Default value is Empty, Data type Date/ Text
             * for issue number 6.10091
             * Modified By : Vijaypriya Dated 26 Nov 2008
              */
            
            if(indices[i].getReadOnly()&& indices[i].getType()==Index.IDX_DATE && (indices[i].getKey() || indices[i].getRequired() || !indices[i].getRequired()) &&
            	( indices[i].getDefault()== null || indices[i].getDefault().equals(" ")|| indices[i].getDefault().trim().equals(""))&& 
				(indices[i].getInfo().equals("1001") || indices[i].getInfo().equals("1002")))
            {
	            	VWMessage.showMessage(this,VWMessage.ERR_DEFAULT_NOTFOUND);
	                IndicesTable.setRowSelectionInterval(i,i);
	                PropTable.setRowSelectionInterval(0,0);
	                return false;
            }
            /**
             * This code is added for alerting the message (ERR_DEFAULT_NOTFOUND) if the following condition satisfies,
             * Indices with Data type as Date, Field type as Required/Not Required/Key, Read Only as No, Value type as Fixed Date.
             * Added By: Vijaypriya.B.K Dated 26 Nov 2008 and Modified on 28 Dec 2008 (added Field type Required and Key) 
             */
            
            if(!(indices[i].getReadOnly()) && indices[i].getType()==Index.IDX_DATE && (!(indices[i].getRequired()) || indices[i].getRequired() || indices[i].getKey()) &&
            ( indices[i].getDefault()== null || indices[i].getDefault().equals(" ")|| indices[i].getDefault().trim().equals(""))&& 
    				indices[i].getInfo().equals("0001"))
                {
                    VWMessage.showMessage(this,VWMessage.ERR_DEFAULT_FIXED_DATE);
                    IndicesTable.setRowSelectionInterval(i,i);
                    PropTable.setRowSelectionInterval(0,0);
                    return false;
                }
            
            if(indices[i].getReadOnly()&& indices[i].getType()!=Index.IDX_DATE && (indices[i].getKey() || indices[i].getRequired()) &&
                ( indices[i].getDefault()== null || indices[i].getDefault().equals(" ")|| indices[i].getDefault().trim().equals("")))
    				
                {
	            	/**
	                 * If condition is added for stop alerting the message (ERR_DEFAULT_NOTFOUND) only for Selection type,
	                 * When field type as Required, Read Only yes, Default Value is Empty.
	                 * Added By: Srikanth A Dated 07 Jul 2017 as per Rajesh change request mail sent on 06 Jul 2017 
	                 */
            		if(indices[i].getType()!=Index.IDX_SELECTION) {
	                    VWMessage.showMessage(this,VWMessage.ERR_DEFAULT_NOTFOUND);
	                    IndicesTable.setRowSelectionInterval(i,i);
	                    PropTable.setRowSelectionInterval(0,0);
	                    return false;
            		}
                }
            if(indices[i].getType()==Index.IDX_SELECTION) {
                Vector selectionValues=indices[i].getIndexSelectionValues();
                if(selectionValues==null || selectionValues.size()==0) {
                    VWMessage.showMessage(this,VWMessage.ERR_SELECTIONVALUES_NOTFOUND);
                    IndicesTable.setRowSelectionInterval(i,i);
                    PropTable.setRowSelectionInterval(0,0);
                    return false;
                }
                /*
                String defaultValue=indices[i].getDefault().trim();
                if(!defaultValue.equals("") && selectionValues.indexOf(defaultValue)<0)
                {
                   VWMessage.showMessage(this,VWMessage.ERR_BOOLDEFAULT_NOTFOUND);
                   IndicesTable.setRowSelectionInterval(i,i);
                   PropTable.setRowSelectionInterval(3,3);
                   return false;
                }
                 */
            }
            if(indices[i].getType()==Index.IDX_SEQUENCE) {
                if(indices[i].getMask().indexOf("~")<0) {
                    VWMessage.showMessage(this,VWMessage.ERR_SEQUENCEDIGIT_NOTFOUND);
                    IndicesTable.setRowSelectionInterval(i,i);
                    PropTable.setRowSelectionInterval(1,1);
                    return false;
                }
            }
            if(indices[i].getType()==Index.IDX_BOOLEAN) {
                if(indices[i].getDefault().equals("")) {
                    VWMessage.showMessage(this,VWMessage.ERR_BOOLDEFAULT_NOTFOUND);
                    IndicesTable.setRowSelectionInterval(i,i);
                    PropTable.setRowSelectionInterval(i,i);
                    return false;
                }
            }
            /*
            if(indices[i].getType()==NumberType)
            {
                if(indices[i].getSubInfo() != null &&
                indices[i].getSubInfo()[0].equals(VWIndexNumericMask[1]) &&
                ((indices[i].getSubInfo()[1].equals("") ||
                    indices[i].getSubInfo()[1].equals("0")) &&
                    !indices[i].getSubInfo()[0].equals(VWIndexNumericMask[3])))
                {
                    VWMessage.showMessage(this,VWMessage.ERR_NUMBER_NODECIMALPLACES);
                    IndicesTable.setRowSelectionInterval(i,i);
                    PropTable.setRowSelectionInterval(1,1);
                    return false;
                }
            }
             */
        }
        if(!key && curMode !=MODE_NEWINDEX) {
            VWMessage.showMessage(this,VWMessage.ERR_DOCTYPEKEY_NOTFOUND);
            IndicesTable.setRowSelectionInterval(0,0);
            return false;
        }
        return true;
    }
    //--------------------------------------------------------------------------
    private boolean checkIndexName(VWIndexRec newIndex) {
        return VWDocTypeConnector.checkIndexNameIsExists(newIndex);
    }
    //--------------------------------------------------------------------------
    public int getCurMode() {
        return curMode;
    }
    //--------------------------------------------------------------------------
    public String getSelIndexNumberMask(int subType) {
        String[] subInfo=PropTable.getValues();
        String mask="";
        if(subType==0) {
            mask="0";
        }
        else if(subType==1) {
            mask="0";
            if(subInfo[1].trim().equals("")) subInfo[1]="0";
            int count=VWUtil.to_Number(subInfo[1]);
            if(count > 0) {
                mask+=".";
                for(int i=0;i<count;i++)
                    mask+="0";
            }
            if(subInfo[3].equals(YesNoData[0]))
                mask="#,##" + mask;
            if(subInfo[2].equals(YesNoData[0]))
                mask="(" + mask + ")";
        }
        else if(subType==2) {
            mask="0";
            if(subInfo[1].trim().equals("")) subInfo[1]="0";
            int count=VWUtil.to_Number(subInfo[1]);
            if(count > 0) {
                mask+=".";
                for(int i=0;i<count;i++)
                    mask+="0";
            }
            if(subInfo[4].equals(YesNoData[0]))
                mask="#,##" + mask;
            mask=subInfo[2].trim()+mask;
            if(subInfo[3].equals(YesNoData[0]))
                mask="(" + mask + ")";
        }
        else if(subType==3) {
            mask="0";
            if(subInfo[1].trim().equals("")) subInfo[1]="0";
            int count=VWUtil.to_Number(subInfo[1]);
            if(count > 0) {
                mask+=".";
                for(int i=0;i<count;i++)
                    mask+="0";
            }
            mask+="%";
        }
        else if(subType==4) {
            mask="0";
            if(subInfo[1].trim().equals("")) subInfo[1]="0";
            int count=VWUtil.to_Number(subInfo[1]);
            if(count > 0) {
                mask+=".";
                for(int i=0;i<count;i++)
                    mask+="0";
            }
            mask+="E+00";
        }
        return mask;
    }
    //--------------------------------------------------------------------------
    private void stopGridEdit() {
        try{
            IndicesTable.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        try{
            PropTable.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
    }
    //--------------------------------------------------------------------------
    public VWIndexRec[] getAMDTIndices() {
        int count=IndicesTable.getRowCount();
        VWIndexRec[] retData=new VWIndexRec[count+1];
        VWIndexRec[] tableData = IndicesTable.getData();
        for(int i=0;i<count;i++)
            retData[i+1]=tableData[i];
        retData[0]=new VWIndexRec(0,"");
        return retData;
    }
    //--------------------------------------------------------------------------
    private VWIndexRec curIndexRec=null;
    private static int gCurRoom=0;
    public static int curMode=-1;
    private List deletedIndices=new LinkedList();
    private boolean docTypeLoaded=false;
    VWDlgVRSetting settingDlg = null;
    VWDlgRetentionSettings retentionSettingDlg = null;
    VWDlgAssignRouteSetting assignRouteDlg = null;
    VWExternalDBLookup externalDBLookup = null;
    public VWAutoMailDlg autoMailDlg=null;
    private boolean forceSaveInfo=false;
    private boolean enableSearchInContaints=false;
    public VWIndexerInfoDlg indexerInfoDlg=null;
    private boolean isDocTypeESIC=false;
    private boolean UILoaded=false;
    public static boolean uniqueSeqSetFlag=false;
    public static boolean checkcoditon=false;
    int oldKeyIndexId=0;
    //--------------------------------------------------------------------------
}