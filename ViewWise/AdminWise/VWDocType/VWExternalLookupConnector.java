package ViewWise.AdminWise.VWDocType;

import java.util.Vector;

import com.computhink.common.DBLookup;
import com.computhink.common.DocType;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;


public class VWExternalLookupConnector {
    public static void getExternalTableColumns(String dsnName, String userName, String passWord, String externalTable, String query, Vector columnList){
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        ///if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        AdminWise.gConnector.ViewWiseClient.getExternalLookupColumns(room.getId(), dsnName, userName, passWord, externalTable, query, columnList);        
    }
    
    public static int setDBLookup(DBLookup lookupInfo/*int docTypeId, String dsnName, String userName, String passWord, String externalTable*/){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	//int ret = AdminWise.gConnector.ViewWiseClient.setDSNInfo(room.getId(), docTypeId, dsnName, userName, passWord, externalTable);
	int ret = AdminWise.gConnector.ViewWiseClient.setDBLookup(room.getId(), lookupInfo);
	return ret;
    }
    
    public static int loadDSNInfo(int docTypeId, Vector dsnDetails){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	int ret = AdminWise.gConnector.ViewWiseClient.getDSNInfo(room.getId(), docTypeId, dsnDetails);
	return ret;
    }
/*    
    public static int saveIndexMapping(DocType docType, int triggerIndexDataType){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	int ret = AdminWise.gConnector.ViewWiseClient.setIndexMapping(room.getId(), docType, triggerIndexDataType);
	return ret;
    }*/
    
    public static int loadIndexMapping(DocType docType){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	int ret = AdminWise.gConnector.ViewWiseClient.getIndexMapping(room.getId(), docType);
	return ret;
    }    

    public static int getExternalData(DocType docType, Vector outputLog){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	int ret = AdminWise.gConnector.ViewWiseClient.getExternalData(room.getId(), docType, outputLog);
	return ret;
    }    
    
}
