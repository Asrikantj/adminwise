/*
 * VWClientListener.java
 *
 * Created on March 11, 2004, 12:28 PM
 */

package ViewWise.AdminWise.VWUtil;
/**
 *
 * @author  Administrator
 */
public interface VWCheckListListener extends java.util.EventListener {
        public void VWItemChecked(VWItemCheckEvent event);
        public void VWItemUnchecked(VWItemCheckEvent event);
    }
