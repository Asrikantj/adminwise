/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWUtil<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWUtil;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.JApplet;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWMessage.VWMessage;

import com.computhink.vwc.VWCUtil;

public class VWUtil implements VWConstant {
    private static String home = "";
    
    static {
        findHome();
    }
    public static String getHome() {
        return home;
    }
    private static void findHome() {
        String  urlPath = "";
        String  indexerHome = "";
        //dot (.) is replaced with '/' because getHome is not giving any value. Valli 	5 Nov 2206
        String res = "ViewWise/AdminWise/VWImages/images/AdminWiseIcon.gif";
        String jarres = "/lib/AdminWise.jar!";
        
        int startIndex = 0;
        int endIndex = 0;
        try {
            URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){urlPath="";}
        if (!urlPath.equalsIgnoreCase("")) {
            if (urlPath.startsWith("file")) {
                //file:/c:/program%20files/viewwise%20indexer/lib/viewwiseindexer.jar!/image/indexer.gif
                startIndex = 6;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else {
                ///c:/program%20files/viewwise%20indexer/Classes/indexer.gif
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            indexerHome = urlPath.substring(startIndex, endIndex-1);
            indexerHome = indexerHome.replace('/', '\\');
        }
        home = fixURLPath(indexerHome);
    }
    //-----------------------------------------------------------
    public VWUtil(JApplet applet) {
        try{
            this.applet=applet;
            DetectBrowserType();
            gAppURL=applet.getCodeBase();
            gDocURL=applet.getDocumentBase();
        }
        catch (Exception e){}
    }
    //-----------------------------------------------------------
    public static int DetectBrowserType() {
        String appletContext = applet.getAppletContext().toString();
        if(appletContext.startsWith("sun.applet.AppletViewer"))
            gBrowser_Type = 0;
        else
            if(appletContext.startsWith("netscape.applet."))
                gBrowser_Type = 2;
            else
                if(appletContext.startsWith("com.ms.applet."))
                    gBrowser_Type = 1;
                else
                    if(appletContext.startsWith("sunw.hotjava.tags.TagAppletPanel"))
                        gBrowser_Type = 3;
                    else
                        if(appletContext.startsWith("sun.plugin.navig.win32.AppletPlugin"))
                            gBrowser_Type = 4;
                        else
                            if(appletContext.startsWith("sun.plugin.ocx.ActiveXApplet"))
                                gBrowser_Type = 0;
        return gBrowser_Type;
    }
    //-----------------------------------------------------------
    public static String GetFixedString(int i, int len) {
        String str;
        for(str = String.valueOf(i); str.length() < len; str = str + " ");
        return str;
    }
    //-----------------------------------------------------------
    public static String GetFixedString(String str, int len) {
        if(str==null) str="";
        for(; str.length() < len; str = str + " ");
        return str;
    }
    //-----------------------------------------------------------
    public static Dimension getScreenSize() {
        Toolkit theKit = applet.getToolkit();
        gScreen_Size = theKit.getScreenSize();
        if(gBrowser_Type == 2) {
            gScreen_Size.width = (gScreen_Size.width * 96) / 100;
            gScreen_Size.height = (gScreen_Size.height * 78) / 100;
        }
        return gScreen_Size;
    }
    //-----------------------------------------------------------
    public static URL getCodeBase() {
        return gAppURL;
    }
    //-----------------------------------------------------------
    public static URL getDocBase() {
        return gDocURL;
    }
    //-----------------------------------------------------------
    public static Toolkit getToolkit() {
        return applet.getToolkit();
    }
    //-----------------------------------------------------------
    public static String[][] listTo2DArrOfString(List list) {
        if(list==null || list.size()==0) return null;
        int listCount=list.size();
        VWStringTokenizer token=new VWStringTokenizer((String)list.get(0),
        VWConstant.SepChar,false);
        int recordCount=token.countTokens();
        token=null;
        String[][] data=new String[listCount][recordCount];
        for(int i=0;i<listCount;i++) {
            VWStringTokenizer tokens=new VWStringTokenizer((String)list.get(i),
            VWConstant.SepChar,false);
            for(int j=0;j<recordCount;j++)
                data[i][j]=tokens.nextToken();
        }
        return data;
    }
    //-----------------------------------------------------------
    public static String[] listTo1DArrOfString(List list) {
        if(list==null || list.size()==0) return null;
        int listCount=list.size();
        String[] data=new String[listCount];
        for(int i=0;i<listCount;i++)
            data[i]=(String)list.get(i);
        return data;
    }
    //-----------------------------------------------------------
    public static JApplet getApplet() {
        return applet;
    }
    //-----------------------------------------------------------
    public static int getCharCount(String str,String chr) {
        int charCount=0;
        int count=str.length();
        for(int i=0;i<count;i++) {
            if(str.substring(i,i+1).equalsIgnoreCase(chr)) charCount++;
        }
        return charCount;
    }
    /*
    //-----------------------------------------------------------
    public static String isArrayContaintsString(String str,String[] strArr) {
        int count=strArr.length;
        for(int i=0;i<count;i++) {
            if(str.indexOf(strArr[i])>=0) {
                return i;
            }
        }
        return "";
    }
     */
    //-----------------------------------------------------------
    public static String getCurrencySymbol(String str,Vector list) {
        int count=list.size();
        for(int i=0;i<count;i++) {
        	if(str.equalsIgnoreCase((String)list.get(i))) {//if(str.indexOf((String)list.get(i))>=0) {        		
                return (String)list.get(i);
            }
        }
        return "";
    }
    //-----------------------------------------------------------
    public static String[] convertStringToArr(String strValues,String sep,boolean addeSpace) {
        if(strValues==null || strValues.equals("")) return null;
        List valuesList= new LinkedList();
        VWStringTokenizer tokens = new VWStringTokenizer(strValues,sep,false);
        while (tokens.hasMoreTokens()) {
            String str=tokens.nextToken().trim();
            if(!str.equals("")) valuesList.add(str);
        }
        int count = valuesList.size();
        String[] retArr=new String[addeSpace?count+1:count];
        for(int i=0;i<count;i++)
            retArr[i]=(String)valuesList.get(i);
        if(addeSpace)retArr[count]="";
        return retArr;
    }
    //-----------------------------------------------------------
    public static String removeCharFromString(String str,String chr) {
        String retStr="";
        if(str==null || str.equals("")) return "";
        int count=str.length();
        int index=0;
        int chrLen=chr.length();
        while (index<count)
        {
            if(index+chrLen>count)
            {
                return retStr+=str.substring(index,count);
            }
            String tmpStr=str.substring(index,index+chrLen);
            if(!tmpStr.equalsIgnoreCase(chr))
            {
                retStr+=str.substring(index,index+1);
                index++;
            }
            else
            {
                index=index+chrLen;
            }
        }   
        return retStr;
    }
    //-----------------------------------------------------------
    /*
    public static String fixNodePath(String nodePath)
    {
        if(nodePath== null || nodePath.equals("") || nodePath.equals(".")|| nodePath.equals("\\")) return "";
        String m_Path="";
        VWStringTokenizer pathTokens=new VWStringTokenizer(nodePath,"\\",false);
        String tmp="";
        while (pathTokens.hasMoreTokens())
        {
            tmp=pathTokens.nextToken();
            m_Path=tmp + "\\" + m_Path;
        }
        return m_Path;
    }
     */
    //-----------------------------------------------------------
    public static boolean writeListToFile(Vector list,String path,Component parent) {
        String errDesc="";
        boolean retVal=false;
        if(!(retVal=VWCUtil.writeListToFile(list,path,errDesc)))
        {
        	//Added for issue fix 184
        	if(VWCUtil.ErrorDesc != null && VWCUtil.ErrorDesc.length() > 0 && AdminWise.adminPanel.restoreWriteInfoLog){
        		VWMessage.showMessage(parent,VWCUtil.ErrorDesc);
        	}
        }
        return retVal;
    }
    //-----------------------------------------------------------
    public static boolean writeStringToFile(String str,String path) {
        try {
            File filePath=new File(path);
            File folderPath=filePath.getParentFile();
            if(!folderPath.canWrite())
            	path= VWProperties.getCachDirPath()+File.separator+filePath.getName();
            FileOutputStream ufos = new FileOutputStream(path);
            ///OutputStreamWriter osw = new OutputStreamWriter(ufos,"UTF-16");
            OutputStreamWriter osw = new OutputStreamWriter(ufos);
            ////System.out.println(AdminWise.gConnector.ViewWiseClient.decryptStr(str));
            osw.write(str);
            osw.close();
            /*
            
            RandomAccessFile raf = new RandomAccessFile(filePath,"r");
            str = raf.readLine();
            System.out.println(str);
            System.out.println(AdminWise.gConnector.ViewWiseClient.decryptStr(str));
            raf.close();
            */
        }
        catch(IOException e) {
            return false;
        }
        return true;
    }
    //-----------------------------------------------------------
    public static String writeListToTmpFile(Component parent,Vector list,String fileName) {
        String path=VWProperties.getCachDirPath()+File.separator+fileName;
        writeListToFile(list,path,parent);
        return path;
    }
    //-----------------------------------------------------------
    public static int to_Number(String str) {
        try {
            str=str.trim();
            return Integer.parseInt(str);
        }
        catch(Exception e){return 0;}
    }
    //-----------------------------------------------------------
    public static String fixURLPath(String urlPath) {
        String sFixPath="";
        if(urlPath==null || urlPath.equals("")) return sFixPath;
        int count=urlPath.length();
        for(int i=0;i<count;i++) {
            if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("0")) {
                sFixPath+=" ";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equalsIgnoreCase("B")) {
                sFixPath+="+";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equalsIgnoreCase("F")) {
                sFixPath+="/";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("3") &&
            urlPath.substring(i+2,i+3).equalsIgnoreCase("F")) {
                sFixPath+="?";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("5")) {
                sFixPath+="%";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("3")) {
                sFixPath+="#";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("6")) {
                sFixPath+="&";
                i=i+2;
            }
            else {
                sFixPath+=urlPath.substring(i,i+1);
            }
        }
        return sFixPath;
    }
    //-----------------------------------------------------------
    public static List sortList(List sList) {
        List newList=new LinkedList();
        int count = sList.size();
        String str="",strTmp="";
        for(int i=0;i<count;i++) {
            str=(String)sList.get(i);
            if (str!=null) {
                str=str.trim();
                if(newList.size()==0) {
                    newList.add(str);
                }
                else {
                    boolean inserted=false;
                    strTmp=str.toLowerCase();
                    for(int j=0;j<newList.size();j++)
                        if(strTmp.compareTo(((String)newList.get(j)).toLowerCase()) < 0) {
                            newList.add(j,str);
                            inserted=true;
                            break;
                        }
                    if(!inserted) newList.add(str);
                }
            }
        }
        return newList;
    }
    //-----------------------------------------------------------
    public static String fixXMLString(String str) {
        String fixedStr="";
        if(str==null || str.trim().equals("")) return fixedStr;
        int charCount=str.length();
        for(int i=0;i<charCount;i++) {
            String tmpChar=str.substring(i,i+1);
            if(tmpChar.equals("<"))         fixedStr=fixedStr+"&lt;";
            else if(tmpChar.equals(">"))    fixedStr=fixedStr+"&gt;";
            else if(tmpChar.equals("\""))   fixedStr=fixedStr+"&quot;";
            else if(tmpChar.equals("'"))    fixedStr=fixedStr+"&apos;";
            else if(tmpChar.equals("&"))    fixedStr=fixedStr+"&amp;";
            //fixedStr=fixedStr+"_";
            else
                fixedStr=fixedStr+tmpChar;
        }
        return fixedStr;
    }
    //-----------------------------------------------------------
    static URL fileToURL(File file) {
        String path = file.getAbsolutePath();
        String fSep = System.getProperty("file.separator");
        if (fSep != null && fSep.length() == 1)
            path = path.replace(fSep.charAt(0), '/');
        if (path.length() > 0 && path.charAt(0) != '/')
            path = '/' + path;
        try {
            return new URL("file", null, path);
        }
        catch (java.net.MalformedURLException e) {
            throw new Error("unexpected MalformedURLException");
        }
    }
    //-----------------------------------------------------------
    public static String getFixedValue(String str,String defaultVal) {
        if(str==null || str.trim().equals("") || str.trim().equals("."))
            return defaultVal;
        return str;
    }
    //-----------------------------------------------------------
    public static Vector toVector(List list) {
        Vector vector=new Vector();
        vector.addAll(list);
        return vector;
    }
    //-----------------------------------------------------------
    public static String getFormatedDate(String dateStr, String toMask) {
        if(toMask==null || toMask.equals("") || dateStr==null || dateStr=="")
            return dateStr;
        Date date=new Date();
        SimpleDateFormat dateFormat = null;
        try
        {
        // It will give an exception when casting to DateFormat.SHORT. So change the parse date format to "MM/dd/yyyy"
            //date = DateFormat.getDateInstance(DateFormat.SHORT).parse(dateStr);
/*
Issue No / Purpose:  <03/03/0427200601/Adminwise Search Panel>
Created by: <Pandiya Raj.M>
Date: <1 Jun 2006>
*/
// If date format is "yyyyMMdd", convert into "MM/dd/yyyy"            
        	if (dateStr.length() == 8){
        		dateStr = (dateStr.substring(4,6)+"/"+dateStr.substring(6,8)+"/"+dateStr.substring(0,4));
        	}
        	dateFormat= new SimpleDateFormat("MM/dd/yyyy");
    		date= dateFormat.parse(dateStr);        	
        }
        catch (java.text.ParseException e){}
        Format formatter;
        formatter = new SimpleDateFormat(toMask,Locale.ENGLISH);
        String s = formatter.format(date);
        return s;
    }
    //-----------------------------------------------------------
    public static String getFormatedDate(Date date,String mask) {
        if(mask==null || mask.equals("") || date==null)
            return "";
        Format formatter;
        formatter = new SimpleDateFormat(mask,Locale.ENGLISH);
        String s = formatter.format(date);
        return s;
    }
    //-----------------------------------------------------------
    static String replaceWord(String original, String find, String replacement) {
    int i = original.indexOf(find);
    if (i < 0) {
        return original;  // return original if 'find' is not in it.
    }
  
    String partBefore = original.substring(0, i);
    String partAfter  = original.substring(i + find.length());
  
    return partBefore + replacement + partAfter;
}
    public static String rawDate()
    {
       // Time format: 27 Oct 1998 15:27:39 PST
       // Get Greg's time of day
       Calendar dt = new GregorianCalendar();
       StringBuffer s = new StringBuffer();

       String[] ids = TimeZone.getAvailableIDs(dt.get(dt.ZONE_OFFSET));
       if (ids.length == 0)
          return "Unknown Date - no time zone names available";

       // If in daylight savings time change the middle letter of the
       // time zone to 'D'.
       if (dt.get(dt.DST_OFFSET) > 0)
       {
          StringBuffer b = new StringBuffer(ids[0]);
          b.setCharAt(1, 'D');
          ids[0] = b.toString();
       }
       //s.append(dayName[dt.get(dt.DAY_OF_WEEK)-1]).append(' ');
       s.append(dt.get(dt.YEAR)).append(formatNumber(dt.get(dt.MONTH) + 1, 2)).append(formatNumber(dt.get(dt.DATE), 2));
       s.append(formatNumber(dt.get(dt.HOUR_OF_DAY),2)).append(formatNumber(dt.get(dt.MINUTE),2)).append(formatNumber(dt.get(dt.SECOND),2));
       //s.append(' ').append(ids[0]);

       return s.toString();
    }
    public static String formatNumber(int number, int maxDigits)
    {
        String numberString = Integer.toString(number);
        while (numberString.length() < maxDigits)
            numberString = "0" + numberString;
        return numberString;
    }
//  -----------------------------------------------------------
    //This method checks the value passed is number or not.
    public static boolean isNumeric(String value, boolean maxLimit){
		boolean isValid = false;
		int numVal =0;
		try{
			numVal = Integer.parseInt(value);	
			if(maxLimit){
				if(numVal>0 && numVal<1000)
					isValid = true;
				else
					isValid = false;
			}else{
				if(numVal>0)
					isValid = true;
				else 
					isValid = false;
			}
		}catch(NumberFormatException nfe){
			isValid = false;
			return isValid;			
		}		
		return isValid;
	}    
//-----------------------------------------------------------
    public static Dimension gScreen_Size = null;
    public static int gBrowser_Type = 0;
    private static URL gAppURL=null;
    private static URL gDocURL=null;
    private static JApplet applet=null;    
}