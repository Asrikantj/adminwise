/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWUtil<br>
 *
 * @version     $Revision: 1.10 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWUtil;

import java.awt.Component;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JFileChooser;

import com.computhink.common.Constants;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;

public class VWSaveAsHtml implements VWConstant, Constants
{
	 public static boolean saveArrayInFile(String[][] data,String[] caption,
		        String[] colWidth,String path,Component parent,String title)
	{
		 		return saveArrayInFile(data,caption,colWidth,path,parent,title, false);
	}
     public static boolean saveArrayInFile(String[][] data,String[] caption,
        String[] colWidth,String path,Component parent,String title, boolean  multipleFiles)
    {
        if(path==null || path.equals(""))
        	path=loadSaveDialog(parent);
        if(path==null || path.equals("")) return false;
        if(path.toLowerCase().endsWith("html")){
        	if(multipleFiles){
        		int dataLimit = 0;
        		try{
        			dataLimit = Integer.parseInt(AdminWise.adminPanel.getATOptions()[1][1]);
            		return saveArrayInHtmlFile(data,caption,colWidth,path,parent,title, dataLimit);
        		}catch(Exception e){
        			dataLimit = 10000;
        			return saveArrayInHtmlFile(data,caption,colWidth,path,parent,title, dataLimit);
        		}
        	}else
        		return saveArrayInHtmlFile(data,caption,colWidth,path,parent,title);
        }
        else if(path.toLowerCase().endsWith("xml"))
            return saveArrayInXMLFile(data,caption,path,parent,title);
        return false;
     }
//------------------------------------------------------------------------------   
    private static boolean saveArrayInHtmlFile(String[][] data,String[] caption,
        String[] colWidth,String path,Component parent,String title)
    {
        Vector htmlContaints=new Vector();
        htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
        htmlContaints.add("<html>");
        htmlContaints.add("<head>");
        htmlContaints.add("</head>");
        htmlContaints.add("<style>");
        htmlContaints.add("TH {");
        		htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
        htmlContaints.add("}");
        htmlContaints.add("TD {");
        htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
        		htmlContaints.add("}");
        htmlContaints.add("</style>");
        htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
        htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
        htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
        htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
        ///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
        htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
        
        htmlContaints.add("<tr>");
        for(int i=0;i<caption.length;i++)
            htmlContaints.add("<td width='" + colWidth[i] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[i] + "</B></td>");
        htmlContaints.add("</tr>");
        for(int i=0;i<data.length;i++)
        {
            htmlContaints.add("<tr>");
            for(int j=0;j<caption.length;j++)
                htmlContaints.add("<td width='" + colWidth[j] + "%' >" + 
                data[i][j] + "</td>");
            htmlContaints.add("</tr>");
        }
        htmlContaints.add("</table>");
        htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.recordsFound")+" "+data.length+"</font></span></p>");
        htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
        htmlContaints.add("</body>");
        htmlContaints.add("</html>");
        return VWUtil.writeListToFile(htmlContaints,path,parent);
    }
    /* Purpose/Issue No 870: Need to generate multiple output file if the records are more than 10,000
    *  Or the records per file limit the user enters in Setting screen.
    *  And css is included in html itself instead of specifing for each <td> */
    public static boolean saveArrayInHtmlFile(String[][] data,String[] caption,
            String[] colWidth,String path,Component parent,String title, int dataLimit)
        {
    	if(data.length < dataLimit)
    		return saveArrayInHtmlFile(data,caption,colWidth,path,parent,title);
    	if(path==null || path.equals(""))     		return false;
        String tempPath = ""; 
        if(path.endsWith(".html")){
        		path = path.substring(0,path.length()-5);
        }else tempPath = path;
    	int i = 0;
    	int fromIndex = 1;
    	int fileCount = data.length/dataLimit;
    	if(dataLimit*fileCount < data.length)
    		fileCount = fileCount+1;
    	boolean flag = false;
   			for (int count = 1;count <=fileCount ; count++)
    		{
	            Vector htmlContaints=new Vector();
	            htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
	            htmlContaints.add("<html>");
	            htmlContaints.add("<head>");
	            htmlContaints.add("</head>");
	            htmlContaints.add("<style>");
	            htmlContaints.add("TH {");
        			htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
        		htmlContaints.add("}");
	            htmlContaints.add("TD {");
	            htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
	            		htmlContaints.add("}");
	            htmlContaints.add("</style>");
	            htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
	            htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
	            htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
	            htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
	            ///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
	            htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
	            
	            htmlContaints.add("<tr>");
	            for(int k=0;k<caption.length;k++)
	                htmlContaints.add("<td width='" + colWidth[k] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[k] + "</B></td>");
	            htmlContaints.add("</tr>");
	            //for(int i=0;i<dataLimit;i++)
	            int temp = dataLimit*count;
	            if(dataLimit*(fileCount-1) < data.length && count == fileCount)
	            	temp = data.length;
	            for(i=fromIndex-1; i<temp; i++)
	            {
	                htmlContaints.add("<tr>");
	                for(int j=0;j<caption.length;j++)
	                    htmlContaints.add("<td width='" + colWidth[j] + "%' >" + 
	                    data[i][j] + "</td>");
	                htmlContaints.add("</tr>");
	            }
	            htmlContaints.add("</table>");
            	htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.recordsFound")+" "+(fromIndex)+AdminWise.connectorManager.getString("VWSaveAsHtml.recordsFound")+" "+temp+"</font></span></p>");
	            htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
	            htmlContaints.add("</body>");
	            htmlContaints.add("</html>");
	            fromIndex = count*dataLimit+1;
        		tempPath = path+"_"+VWUtil.rawDate()+".html";
	            flag = VWUtil.writeListToFile(htmlContaints,tempPath,parent);
    		}
   			return flag;
        }
    
    /**@author apurba.m
	 * CV 10.1 Enhancement: Created for generating html report with custom indices for RecycleDocuments Panel
	 */
	public static boolean saveArrayInFileForRd(String[][] data,List customData,Vector newCustomIndexList,String[] caption,
			String[] colWidth,String path,Component parent,String title)
	{
		return saveArrayInFileForRd(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, false);
	}
	public static boolean saveArrayInFileForRd(String[][] data,List customData,Vector newCustomIndexList, String[] caption,
			String[] colWidth,String path,Component parent,String title, boolean  multipleFiles)
	{
		AdminWise.printToConsole("saveArrayInFileForRdAndAt 1!!!!");
		if(path==null || path.equals("")) return false;
		if(path.toLowerCase().endsWith("html")){
			if(multipleFiles){
				int dataLimit = 0;
				try{
					dataLimit = Integer.parseInt(AdminWise.adminPanel.getATOptions()[1][1]);
					return saveArrayInHtmlFileForRd(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, dataLimit);
				}catch(Exception e){
					dataLimit = 5000;
					return saveArrayInHtmlFileForRd(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, dataLimit);
				}
			}else
				return saveArrayInHtmlFileForRd(data,customData,newCustomIndexList,caption,colWidth,path,parent,title);
		}
		else if(path.toLowerCase().endsWith("xml"))
			return saveArrayInXMLFile(data,caption,path,parent,title);
		return false;
	}
	//------------------------------------------------------------------------------   
	private static boolean saveArrayInHtmlFileForRd(String[][] data,List customData, Vector newCustomList, String[] caption,
			String[] colWidth,String path,Component parent,String title)
	{
		AdminWise.printToConsole("saveArrayInHtmlFileForRd 1!!!!");
		Vector htmlContaints=new Vector();
		htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContaints.add("<html>");
		htmlContaints.add("<head>");
		htmlContaints.add("</head>");
		htmlContaints.add("<style>");
		htmlContaints.add("TH {");
		htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
		htmlContaints.add("}");
		htmlContaints.add("TD {");
		htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
		htmlContaints.add("}");
		htmlContaints.add("</style>");
		htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
		htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
		htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
		htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
		///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
		htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");

		htmlContaints.add("<tr>");
		for(int i=0;i<caption.length;i++)
			htmlContaints.add("<td width='" + colWidth[i] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[i] + "</B></td>");
		//htmlContaints.add("</tr>");

		/**
		 * For New Custom Index
		 */
		int columnWidth=10;
		AdminWise.printToConsole("caption.length saveArrayInHtmlFileForRecycled 1 :::: "+caption.length);
		AdminWise.printToConsole("newCustomList.size() saveArrayInHtmlFileForRecycled 1 :::: "+newCustomList.size());
		int totalLength= caption.length+newCustomList.size();
		AdminWise.printToConsole("totalLength ::::"+totalLength);
		AdminWise.printToConsole("data.length saveArrayInHtmlFileForRecycled 1::::"+data.length);
		try{
			String customDataStr[][]= new String [data.length][totalLength];
			String tokens= "";

			for(int i=0;i<newCustomList.size();i++){
				htmlContaints.add("<td width='" + columnWidth + "%' align='center' BGCOLOR='#cccc99'><B>" + newCustomList.get(i).toString() + "</B></td>");
			}
			htmlContaints.add("</tr>");

			AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 1 !!!!");

			for(int n=0; n< data.length; n++){
				StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
				tokens= tokens+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken();
				for(int i=0; i<newCustomList.size(); i++){
					if(st.hasMoreTokens()){
						customDataStr[n][i]= st.nextToken();
					}
				}
			}
			AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 2 !!!!");
			for(int l=0; l< data.length; l++){
				int m=5;
				for(int n=0; n<newCustomList.size(); n++){
					data[l][m]= customDataStr[l][n];
					m++;
				}
			}

			AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 3 !!!!");

			for (int i = 0; i < data.length; i++) {
				htmlContaints.add("<tr>");
				for (int j = 0; j < totalLength; j++) {
					if (data[i][j] == null) {
						htmlContaints.add("<td width='" + columnWidth + "%' >" + "-" + "</td>");
					} else {
						htmlContaints.add("<td width='" + columnWidth + "%' >" + data[i][j] + "</td>");
					}
				}
				htmlContaints.add("</tr>");
			}

			AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 4 !!!!");

		} catch(Exception ex){
			AdminWise.printToConsole("EXCEPTION while generating Recycled report 1 ::::"+ex.getMessage());
			return false;
		}
		htmlContaints.add("</table>");
		htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.totalRecordsFound")+" "+data.length+"</font></span></p>");
		htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
		htmlContaints.add("</body>");
		htmlContaints.add("</html>");
		AdminWise.printToConsole("Local path ::::"+path);
		return VWUtil.writeListToFile(htmlContaints,path,parent);
	}
	/* Purpose/Issue No 870: Need to generate multiple output file if the records are more than 10,000
	 *  Or the records per file limit the user enters in Setting screen.
	 *  And css is included in html itself instead of specifing for each <td> */
	public static boolean saveArrayInHtmlFileForRd(String[][] data, List customData, Vector newCustomList, String[] caption,
			String[] colWidth,String path,Component parent,String title, int dataLimit)
	{
		AdminWise.printToConsole("saveArrayInHtmlFileForRd 2!!!!");
		if(data.length < dataLimit)
			return saveArrayInHtmlFileForRd(data,customData,newCustomList,caption,colWidth,path,parent,title);
		if(path==null || path.equals(""))     		return false;
		String tempPath = ""; 
		if(path.endsWith(".html")){
			path = path.substring(0,path.length()-5);
			AdminWise.printToConsole("path Inside saveArrayInHtmlFileForRd 2 :::: "+path);
		}else {
			tempPath = path;
			AdminWise.printToConsole("tempPath Inside saveArrayInHtmlFileForRd 2 ::::  "+tempPath);
		}
		int i = 0;
		int fromIndex = 1;
		int fileCount = data.length/dataLimit;
		AdminWise.printToConsole("fileCount 1 Inside saveArrayInHtmlFileForRd 2 ::::  "+fileCount);
		if(dataLimit*fileCount < data.length){
			AdminWise.printToConsole("dataLimit*fileCount Inside saveArrayInHtmlFileForRd 2 ::::  "+dataLimit*fileCount);
			fileCount = fileCount+1;
			AdminWise.printToConsole("fileCount 2 Inside saveArrayInHtmlFileForRd 2 ::::  "+fileCount);
		}
		boolean flag = false;
		for (int count = 1;count <=fileCount ; count++)
		{
			AdminWise.printToConsole("fileCount Inside For Loop Inside saveArrayInHtmlFileForRd 2 ::::  "+fileCount);
			Vector htmlContaints=new Vector();
			htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
			htmlContaints.add("<html>");
			htmlContaints.add("<head>");
			htmlContaints.add("</head>");
			htmlContaints.add("<style>");
			htmlContaints.add("TH {");
			htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
			htmlContaints.add("}");
			htmlContaints.add("TD {");
			htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
			htmlContaints.add("}");
			htmlContaints.add("</style>");
			htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
			htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
			htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
			htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
			///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
			htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");

			htmlContaints.add("<tr>");
			for(int k=0;k<caption.length;k++)
				htmlContaints.add("<td width='" + colWidth[k] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[k] + "</B></td>");
			// htmlContaints.add("</tr>");
			//for(int i=0;i<dataLimit;i++)

			int columnWidth=10;
			AdminWise.printToConsole("caption.length saveArrayInHtmlFileForRecycled 2 :::: "+caption.length);
			AdminWise.printToConsole("newCustomList.size() saveArrayInHtmlFileForRecycled 2 :::: "+newCustomList.size());
			int totalLength= caption.length+newCustomList.size();
			AdminWise.printToConsole("totalLength ::::"+totalLength);
			AdminWise.printToConsole("data.length saveArrayInHtmlFileForRecycled 2::::"+data.length);

			try{
				String customDataStr[][]= new String [data.length][totalLength];
				String tokens= "";

				for(int k=0;k<newCustomList.size();k++){
					htmlContaints.add("<td width='" + columnWidth + "%' align='center' BGCOLOR='#cccc99'><B>" + newCustomList.get(k).toString() + "</B></td>");
				}
				htmlContaints.add("</tr>");
				AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 5 !!!!");

				for(int n=0; n< data.length; n++){
					StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
					tokens= tokens+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken();
					for(int l=0; l<newCustomList.size(); l++){
						if(st.hasMoreTokens()){
							customDataStr[n][l]= st.nextToken();
						}
					}
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 6 !!!!");
				for(int l=0; l< data.length; l++){
					int m=5;
					for(int n=0; n<newCustomList.size(); n++){
						data[l][m]= customDataStr[l][n];
						m++;
					}
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 7 !!!!");
				int temp = dataLimit*count;
				AdminWise.printToConsole("temp Inside saveArrayInHtmlFileForRecycled 2 ::::  "+temp);
				if(dataLimit*(fileCount-1) < data.length && count == fileCount){
					temp = data.length;
					AdminWise.printToConsole("temp 2 Inside saveArrayInHtmlFileForRecycled 2 ::::  "+temp);
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 8 !!!!");
				for(i=fromIndex-1; i<temp; i++)
				{
					htmlContaints.add("<tr>");
					for (int j = 0; j < totalLength; j++) {
						if (data[i][j] == null) {
							htmlContaints.add("<td width='" + columnWidth + "%' >" + "-" + "</td>");
						} else {
							htmlContaints.add("<td width='" + columnWidth + "%' >" + data[i][j] + "</td>");
						}
					}
					htmlContaints.add("</tr>");
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 9 !!!!");
				htmlContaints.add("</table>");
				htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.recordsFound")+" "+(fromIndex)+"\t"+AdminWise.connectorManager.getString("VWSaveAsHtml.to")+" "+temp+"</font></span></p>");
				htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
				htmlContaints.add("</body>");
				htmlContaints.add("</html>");
				fromIndex = count*dataLimit+1;
				AdminWise.printToConsole("fromIndex Inside saveArrayInHtmlFileForRecycled 2 ::::  "+fromIndex);
				tempPath = path+"_"+VWUtil.rawDate()+".html";
				AdminWise.printToConsole("tempPath 2 Inside saveArrayInHtmlFileForRecycled 2 ::::  "+tempPath);
				flag = VWUtil.writeListToFile(htmlContaints,tempPath,parent);
				AdminWise.printToConsole("flag Inside saveArrayInHtmlFileForRecycled 2 ::::  "+flag);
				AdminWise.printToConsole("saveArrayInHtmlFileForRecycled BreakPoint 10 !!!!");
			}catch (Exception ex){
				AdminWise.printToConsole("EXCEPTION while generating Recycle report 2 :::: "+ex.getMessage());
				return false;
			}
		}
		return flag;
	}

	/**@author apurba.m
	 * CV 10.1 Enhancement: Created for Generating HTML Report with Custom Indices For Audit Trail Panel
	 */
	public static boolean saveArrayInFileForAT(String[][] data,Vector customData,Vector newCustomIndexList,String[] caption,
			String[] colWidth,String path,Component parent,String title)
	{
		AdminWise.adminPanel.setWaitPointer();
		return saveArrayInFileForAT(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, false);
	}
	public static boolean saveArrayInFileForAT(String[][] data,Vector customData,Vector newCustomIndexList, String[] caption,
			String[] colWidth,String path,Component parent,String title, boolean  multipleFiles)
	{
		AdminWise.printToConsole("saveArrayInFileForAT 1!!!!");
		AdminWise.adminPanel.setWaitPointer();
		if(path==null || path.equals("")) return false;
		if(path.toLowerCase().endsWith("html")){
			if(multipleFiles){
				int dataLimit = 0;
				try{
					dataLimit = Integer.parseInt(AdminWise.adminPanel.getATOptions()[1][1]);
					return saveArrayInHtmlFileForAT(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, dataLimit);
				}catch(Exception e){
					AdminWise.printToConsole("Catch Block saveArrayInFileForAT 1 ::::"+e.getMessage());
					dataLimit = 5000;
					return saveArrayInHtmlFileForAT(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, dataLimit);
				}
			}else
				return saveArrayInHtmlFileForAT(data,customData,newCustomIndexList,caption,colWidth,path,parent,title);
		}
		else if(path.toLowerCase().endsWith("xml"))
			return saveArrayInXMLFile(data,caption,path,parent,title);
		return false;
	}
	//------------------------------------------------------------------------------   
	private static boolean saveArrayInHtmlFileForAT(String[][] data,Vector customData, Vector newCustomList, String[] caption,
			String[] colWidth,String path,Component parent,String title)
	{
		AdminWise.adminPanel.setWaitPointer();
		AdminWise.printToConsole("saveArrayInHtmlFileForAT 1!!!!");
		Vector htmlContaints=new Vector();
		htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContaints.add("<html>");
		htmlContaints.add("<head>");
		htmlContaints.add("</head>");
		htmlContaints.add("<style>");
		htmlContaints.add("TH {");
		htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
		htmlContaints.add("}");
		htmlContaints.add("TD {");
		htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
		htmlContaints.add("}");
		htmlContaints.add("</style>");
		htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
		htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
		htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
		htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
		///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
		htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");

		htmlContaints.add("<tr>");
		for(int i=0;i<caption.length;i++)
			htmlContaints.add("<td width='" + colWidth[i] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[i] + "</B></td>");
		//htmlContaints.add("</tr>");

		/**
		 * For New Custom Index
		 */
		int columnWidth=20;
		int totalLength= caption.length+newCustomList.size();
		AdminWise.printToConsole("totalLength saveArrayInHtmlFileForAT 1::::"+totalLength);
//		AdminWise.printToConsole("data.length saveArrayInHtmlFileForAT 1::::"+data.length);
		String customDataStr[][]= new String [customData.size()][totalLength];
		try {
			
			String tokens= "";
			Vector allValuesVect= new Vector();
			AdminWise.printToConsole("customData.size For saveArrayInHtmlFileForAT 1 ::::"+customData.size());

			for(int i=0;i<newCustomList.size();i++){
				htmlContaints.add("<td width='" + columnWidth + "%' align='center' BGCOLOR='#cccc99'><B>" + newCustomList.get(i).toString() + "</B></td>");
			}
			htmlContaints.add("</tr>");
			AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 1 !!!!");
			
			if(newCustomList.size() >0 && newCustomList != null){
				for(int n=0; n<customData.size(); n++){
					StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
					String firstThreeValues= st.nextToken()+"\t"+st.nextToken()+"\t"+st.nextToken();
					if(st.hasMoreTokens()){
						String objectName= st.nextToken();
						customDataStr[n][0]= objectName;
						String userName= st.nextToken();
						customDataStr[n][4]= userName;
						String clientHost= st.nextToken();
						customDataStr[n][5]= clientHost;
						String createDate= st.nextToken();
						customDataStr[n][3]= createDate;
						int type= VWUtil.to_Number(st.nextToken());
						String location= st.nextToken();
						customDataStr[n][6]= location;
						String description= st.nextToken();
						customDataStr[n][7]= description;
						String value= st.nextToken();
						String objectType= st.nextToken();
						customDataStr[n][1]= objectType;
						String actionName=st.nextToken();
						String event= VWConstant.ATStrActions[type];
						customDataStr[n][2]= event;
						if(st.hasMoreTokens()){
							for(int l=8; l<totalLength; l++){
								customDataStr[n][l]=st.nextToken();
							}
						}
					}
				}
			} else {
				for(int n=0; n<customData.size(); n++){
					StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
					String firstThreeValues= st.nextToken()+"\t"+st.nextToken()+"\t"+st.nextToken();
					if(st.hasMoreTokens()){
						String objectName= st.nextToken();
						customDataStr[n][0]= objectName;
						String userName= st.nextToken();
						customDataStr[n][4]= userName;
						String clientHost= st.nextToken();
						customDataStr[n][5]= clientHost;
						String createDate= st.nextToken();
						customDataStr[n][3]= createDate;
						int type= VWUtil.to_Number(st.nextToken());
						String location= st.nextToken();
						customDataStr[n][6]= location;
						String description= st.nextToken();
						customDataStr[n][7]= description;
						String value= st.nextToken();
						String objectType= st.nextToken();
						customDataStr[n][1]= objectType;
						String actionName=st.nextToken();
						String event= VWConstant.ATStrActions[type];
						customDataStr[n][2]= event;
//						if(st.hasMoreTokens()){
//							for(int l=8; l<totalLength; l++){
//								customDataStr[n][l]=st.nextToken();
//							}
//						}
					}
//					AdminWise.printToConsole("Token generated at :::: "+n);
				}
			}
            
//			if(newCustomList.size()>0){
//				for(int n=0; n< data.length; n++){
//					StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
//					tokens= tokens+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken();
////					AdminWise.printToConsole("Token Generated Break Point less than 5000 ::::"+n);
//					for(int l=0; l<newCustomList.size(); l++){
//						if(st.hasMoreTokens()){
//							customDataStr[n][l]= st.nextToken();
//						}
//					}
//				}
//			}
//			AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 2 !!!!");
//			for(int l=0; l< data.length; l++){
//				if(newCustomList.size()>0){
//					int m=8;
//					for(int n=0; n<newCustomList.size(); n++){
//						data[l][m]= customDataStr[l][n];
//						m++;
//					}
//				}
//			}
			AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 3 !!!!");
			for (int i = 0; i < customDataStr.length; i++) {
				htmlContaints.add("<tr>");
				for (int j = 0; j < totalLength; j++) {
					if (customDataStr[i][j] == null || customDataStr[i][j] == "~" || customDataStr[i][j].equals("~")) {
						htmlContaints.add("<td width='" + columnWidth + "%' >" + "-" + "</td>");
					} else {
						htmlContaints.add("<td width='" + columnWidth + "%' >" + customDataStr[i][j] + "</td>");
					}
				}
				htmlContaints.add("</tr>");
			}
			AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 4 !!!!");
		} catch(Exception ex){
			AdminWise.printToConsole("EXCEPTION occured during Audit Trail Report Generation 1 ::::  "+ex.getMessage());
//			for(int m=0; m<totalLength; m++){
//				AdminWise.printToConsole("customDataStr[14]["+m+"] :::: "+customDataStr[14][m]);
//				AdminWise.printToConsole("customDataStr[15]["+m+"] :::: "+customDataStr[15][m]);
//				AdminWise.printToConsole("customData.get(14)"+customData.get(14).toString());
//				AdminWise.printToConsole("customData.get(15)"+customData.get(15).toString());
//			}
			return false;
		}

		htmlContaints.add("</table>");
		htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.totalRecordsFound")+" "+data.length+"</font></span></p>");
		htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
		htmlContaints.add("</body>");
		htmlContaints.add("</html>");
		return VWUtil.writeListToFile(htmlContaints,path,parent);
	}
	/* Purpose/Issue No 870: Need to generate multiple output file if the records are more than 10,000
	 *  Or the records per file limit the user enters in Setting screen.
	 *  And css is included in html itself instead of specifing for each <td> */
	public static boolean saveArrayInHtmlFileForAT(String[][] data, Vector customData, Vector newCustomList, String[] caption,
			String[] colWidth,String path,Component parent,String title, int dataLimit)
	{
		AdminWise.printToConsole("saveArrayInHtmlFileForAT 2!!!!");
		AdminWise.printToConsole("\n");
		if(data.length < dataLimit)
			return saveArrayInHtmlFileForAT(data,customData,newCustomList,caption,colWidth,path,parent,title);
		if(path==null || path.equals(""))     		return false;
		String tempPath = ""; 
		if(path.endsWith(".html")){
			path = path.substring(0,path.length()-5);
			AdminWise.printToConsole("path Inside saveArrayInHtmlFileForAT 2 :::: "+path);
		}else {
			tempPath = path;
			AdminWise.printToConsole("tempPath Inside saveArrayInHtmlFileForAT 2 ::::  "+tempPath);
		}

		int i = 0;
		int fromIndex = 1;
		int fileCount = data.length/dataLimit;
		AdminWise.printToConsole("fileCount 1 Inside saveArrayInHtmlFileForAT 2 ::::  "+fileCount);
		if(dataLimit*fileCount < data.length){
			AdminWise.printToConsole("dataLimit*fileCount Inside saveArrayInHtmlFileForAT 2 ::::  "+dataLimit*fileCount);
			fileCount = fileCount+1;
			AdminWise.printToConsole("fileCount 2 Inside saveArrayInHtmlFileForAT 2 ::::  "+fileCount);
		}
		boolean flag = false;
		for (int count = 1;count <=fileCount ; count++)
		{
			AdminWise.printToConsole("fileCount Inside For Loop Inside saveArrayInHtmlFileForAT 2 ::::  "+fileCount);
			Vector htmlContaints=new Vector();
			htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
			htmlContaints.add("<html>");
			htmlContaints.add("<head>");
			htmlContaints.add("</head>");
			htmlContaints.add("<style>");
			htmlContaints.add("TH {");
			htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
			htmlContaints.add("}");
			htmlContaints.add("TD {");
			htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
			htmlContaints.add("}");
			htmlContaints.add("</style>");
			htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
			htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
			htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
			htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
			///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
			htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");

			htmlContaints.add("<tr>");
			for(int k=0;k<caption.length;k++)
				htmlContaints.add("<td width='" + colWidth[k] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[k] + "</B></td>");
			// htmlContaints.add("</tr>");
			//for(int i=0;i<dataLimit;i++)
			int columnWidth=20;
			int totalLength= caption.length+newCustomList.size();
			AdminWise.printToConsole("totalLength saveArrayInHtmlFileForAT 2::::"+totalLength);
//			AdminWise.printToConsole("data.length saveArrayInHtmlFileForAT 2::::"+data.length);
			String customDataStr[][]= new String [customData.size()][totalLength];

			try {
				
				String tokens= "";
				AdminWise.printToConsole("customData.size saveArrayInHtmlFileForAT 2 :::: "+customData.size());

				for(int k=0;k<newCustomList.size();k++){
					htmlContaints.add("<td width='" + columnWidth + "%' align='center' BGCOLOR='#cccc99'><B>" + newCustomList.get(k).toString() + "</B></td>");
				}
				htmlContaints.add("</tr>");
				AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 5 !!!!");
				
				if(newCustomList.size() >0 && newCustomList != null){
					for(int n=0; n<customData.size(); n++){
						StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
						String firstThreeValues= st.nextToken()+"\t"+st.nextToken()+"\t"+st.nextToken();
						if(st.hasMoreTokens()){
							String objectName= st.nextToken();
							customDataStr[n][0]= objectName;
							String userName= st.nextToken();
							customDataStr[n][4]= userName;
							String clientHost= st.nextToken();
							customDataStr[n][5]= clientHost;
							String createDate= st.nextToken();
							customDataStr[n][3]= createDate;
							int type= VWUtil.to_Number(st.nextToken());
							String location= st.nextToken();
							customDataStr[n][6]= location;
							String description= st.nextToken();
							customDataStr[n][7]= description;
							String value= st.nextToken();
							String objectType= st.nextToken();
							customDataStr[n][1]= objectType;
							String actionName=st.nextToken();
							String event= VWConstant.ATStrActions[type];
							customDataStr[n][2]= event;
							if(st.hasMoreTokens()){
								for(int l=8; l<totalLength; l++){
									customDataStr[n][l]=st.nextToken();
								}
							}
						}
//						AdminWise.printToConsole("Token generated at with custom columns :::: "+n);
					}
				} else {
					for(int n=0; n<customData.size(); n++){
						StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
						String firstThreeValues= st.nextToken()+"\t"+st.nextToken()+"\t"+st.nextToken();
						if(st.hasMoreTokens()){
							String objectName= st.nextToken();
							customDataStr[n][0]= objectName;
							String userName= st.nextToken();
							customDataStr[n][4]= userName;
							String clientHost= st.nextToken();
							customDataStr[n][5]= clientHost;
							String createDate= st.nextToken();
							customDataStr[n][3]= createDate;
							int type= VWUtil.to_Number(st.nextToken());
							String location= st.nextToken();
							customDataStr[n][6]= location;
							String description= st.nextToken();
							customDataStr[n][7]= description;
							String value= st.nextToken();
							String objectType= st.nextToken();
							customDataStr[n][1]= objectType;
							String actionName=st.nextToken();
							String event= VWConstant.ATStrActions[type];
							customDataStr[n][2]= event;
//							if(st.hasMoreTokens()){
//								for(int l=8; l<totalLength; l++){
//									customDataStr[n][l]=st.nextToken();
//								}
//							}
						}
//						AdminWise.printToConsole("Token generated at :::: "+n);
					}
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 7 !!!!");
				
				int temp = dataLimit*count;
				AdminWise.printToConsole("temp Inside saveArrayInHtmlFileForAT 2 ::::  "+temp);
				if(dataLimit*(fileCount-1) < customDataStr.length && count == fileCount) {
					temp = customDataStr.length;
					AdminWise.printToConsole("temp 2 Inside saveArrayInHtmlFileForAT 2 ::::  "+temp);
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 8 !!!!");
				for(i=fromIndex-1; i<temp; i++)
				{
					htmlContaints.add("<tr>");
					for (int j = 0; j < totalLength; j++) {
						if (customDataStr[i][j] == null || customDataStr[i][j] == "~"  || customDataStr[i][j].equals("~")) {
							htmlContaints.add("<td width='" + columnWidth + "%' >" + "-" + "</td>");
						} else {
							htmlContaints.add("<td width='" + columnWidth + "%' >" + customDataStr[i][j] + "</td>");
						}
					}
					htmlContaints.add("</tr>");
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 9 !!!!");
				htmlContaints.add("</table>");
				htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.recordsFound")+" "+(fromIndex)+"\t"+AdminWise.connectorManager.getString("VWSaveAsHtml.to")+" "+temp+"</font></span></p>");
				htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
				htmlContaints.add("</body>");
				htmlContaints.add("</html>");
				fromIndex = count*dataLimit+1;
				AdminWise.printToConsole("fromIndex Inside saveArrayInHtmlFileForAT 2 ::::  "+fromIndex);
				tempPath = path+"_"+VWUtil.rawDate()+".html";
				AdminWise.printToConsole("tempPath 2 Inside saveArrayInHtmlFileForAT 2 ::::  "+tempPath);
				flag = VWUtil.writeListToFile(htmlContaints,tempPath,parent);
				AdminWise.printToConsole("flag Inside saveArrayInHtmlFileForAT 2 ::::  "+flag);
				AdminWise.printToConsole("saveArrayInHtmlFileForAT BreakPoint 10 !!!!");
			}catch (Exception ex){
				AdminWise.printToConsole("EXCEPTION occured during Audit Trail Report Generation 2 ::::  "+ex.getMessage());
				return false;
			}
		}
		return flag;
	}
	/**@author apurba.m
	 * CV 10.1 Enhancement: Created for Generating HTML Report with Custom Indices For Retention Panel
	 */
	public static boolean saveArrayInFileForRetention(String[][] data,Vector customData,Vector newCustomIndexList,String[] caption,
			String[] colWidth,String path,Component parent,String title)
	{
		AdminWise.adminPanel.setWaitPointer();
		return saveArrayInFileForRetention(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, false);
	}
	public static boolean saveArrayInFileForRetention(String[][] data,Vector customData,Vector newCustomIndexList, String[] caption,
			String[] colWidth,String path,Component parent,String title, boolean  multipleFiles)
	{
		AdminWise.printToConsole("saveArrayInFileForRetention 1!!!!");
		AdminWise.adminPanel.setWaitPointer();
		if(path==null || path.equals("")) return false;
		if(path.toLowerCase().endsWith("html")){
			if(multipleFiles){
				int dataLimit = 0;
				try{
					dataLimit = Integer.parseInt(AdminWise.adminPanel.getATOptions()[1][1]);
					return saveArrayInHtmlFileForRetention(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, dataLimit);
				}catch(Exception e){
					AdminWise.printToConsole("Catch Block saveArrayInFileForRetention 1 ::::"+e.getMessage());
					dataLimit = 5000;
					return saveArrayInHtmlFileForRetention(data,customData,newCustomIndexList,caption,colWidth,path,parent,title, dataLimit);
				}
			}else
				return saveArrayInHtmlFileForRetention(data,customData,newCustomIndexList,caption,colWidth,path,parent,title);
		}
		else if(path.toLowerCase().endsWith("xml"))
			return saveArrayInXMLFile(data,caption,path,parent,title);
		return false;
	}
	//------------------------------------------------------------------------------   
	private static boolean saveArrayInHtmlFileForRetention(String[][] data,Vector customData, Vector newCustomList, String[] caption,
			String[] colWidth,String path,Component parent,String title)
	{
		AdminWise.adminPanel.setWaitPointer();
		AdminWise.printToConsole("saveArrayInHtmlFileForRetention 1!!!!");
		Vector htmlContaints=new Vector();
		htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContaints.add("<html>");
		htmlContaints.add("<head>");
		htmlContaints.add("</head>");
		htmlContaints.add("<style>");
		htmlContaints.add("TH {");
		htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
		htmlContaints.add("}");
		htmlContaints.add("TD {");
		htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
		htmlContaints.add("}");
		htmlContaints.add("</style>");
		htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
		htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
		htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
		htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
		///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
		htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");

		htmlContaints.add("<tr>");
		for(int i=0;i<caption.length;i++)
			htmlContaints.add("<td width='" + colWidth[i] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[i] + "</B></td>");
		//htmlContaints.add("</tr>");

		/**
		 * For New Custom Index
		 */
		int columnWidth=20;
		int totalLength= caption.length+newCustomList.size();
		AdminWise.printToConsole("totalLength saveArrayInHtmlFileForRetention 1::::"+totalLength);
		AdminWise.printToConsole("data.length saveArrayInHtmlFileForRetention 1::::"+data.length);

		try {
			String customDataStr[][]= new String [data.length][totalLength];
			String tokens= "";
			AdminWise.printToConsole("customData.size saveArrayInHtmlFileForRetention 1 ::::"+customData.size());

			for(int i=0;i<newCustomList.size();i++){
				htmlContaints.add("<td width='" + columnWidth + "%' align='center' BGCOLOR='#cccc99'><B>" + newCustomList.get(i).toString() + "</B></td>");
			}
			htmlContaints.add("</tr>");
			AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 1 !!!!");
			
			for(int n=0; n< data.length; n++){
				StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
				tokens= tokens+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken();
				for(int i=0; i<newCustomList.size(); i++){
					if(st.hasMoreTokens()){
						customDataStr[n][i]= st.nextToken();
					}
				}
			}
			AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 2 !!!!");
			for(int l=0; l< data.length; l++){
				int m=5;
				for(int n=0; n<newCustomList.size(); n++){
					data[l][m]= customDataStr[l][n];
					m++;
				}
			}
			AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 3 !!!!");
			for (int i = 0; i < data.length; i++) {
				htmlContaints.add("<tr>");
				for (int j = 0; j < totalLength; j++) {
					if (data[i][j] == null) {
						htmlContaints.add("<td width='" + columnWidth + "%' >" + "-" + "</td>");
					} else {
						htmlContaints.add("<td width='" + columnWidth + "%' >" + data[i][j] + "</td>");
					}
				}
				htmlContaints.add("</tr>");
			}
			AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 4 !!!!");
		}catch (Exception ex){
			AdminWise.printToConsole("EXCEPTION occured during Retention Report Generation 1 ::::  "+ex.getMessage());
			return false;
		}
		htmlContaints.add("</table>");
		htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.totalRecordsFound")+" "+data.length+"</font></span></p>");
		htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
		htmlContaints.add("</body>");
		htmlContaints.add("</html>");
		return VWUtil.writeListToFile(htmlContaints,path,parent);
	}
	/* Purpose/Issue No 870: Need to generate multiple output file if the records are more than 10,000
	 *  Or the records per file limit the user enters in Setting screen.
	 *  And css is included in html itself instead of specifing for each <td> */
	public static boolean saveArrayInHtmlFileForRetention(String[][] data, Vector customData, Vector newCustomList, String[] caption,
			String[] colWidth,String path,Component parent,String title, int dataLimit)
	{
		AdminWise.printToConsole("saveArrayInHtmlFileForRetention 2!!!!");
		if(data.length < dataLimit)
			return saveArrayInHtmlFileForRetention(data,customData,newCustomList,caption,colWidth,path,parent,title);
		if(path==null || path.equals(""))     		return false;
		String tempPath = ""; 
		if(path.endsWith(".html")){
			path = path.substring(0,path.length()-5);
		}else tempPath = path;
		int i = 0;
		int fromIndex = 1;
		int fileCount = data.length/dataLimit;
		if(dataLimit*fileCount < data.length)
			fileCount = fileCount+1;
		boolean flag = false;
		for (int count = 1;count <=fileCount ; count++)
		{
			Vector htmlContaints=new Vector();
			htmlContaints.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
			htmlContaints.add("<html>");
			htmlContaints.add("<head>");
			htmlContaints.add("</head>");
			htmlContaints.add("<style>");
			htmlContaints.add("TH {");
			htmlContaints.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699';");
			htmlContaints.add("}");
			htmlContaints.add("TD {");
			htmlContaints.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
			htmlContaints.add("}");
			htmlContaints.add("</style>");
			htmlContaints.add("<title>"+ PRODUCT_NAME +" Admin (" + title +")</title>");
			htmlContaints.add("<body BGCOLOR='#F7F7E7'>");
			htmlContaints.add("<p align='center'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("Administration.Title")+"</font></span></p>");
			htmlContaints.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
			///htmlContaints.add("<table border='1' cellspacing='1' width='100%' id='AutoNumber1' style='border-collapse: collapse' cellpadding=2>");
			htmlContaints.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");

			htmlContaints.add("<tr>");
			for(int k=0;k<caption.length;k++)
				htmlContaints.add("<td width='" + colWidth[k] + "%' align='center' BGCOLOR='#cccc99'><B>" + caption[k] + "</B></td>");
			// htmlContaints.add("</tr>");

			/**
			 * For New Custom Index
			 */
			int columnWidth=20;
			int totalLength= caption.length+newCustomList.size();
			AdminWise.printToConsole("totalLength saveArrayInHtmlFileForRetention 2::::"+totalLength);
			AdminWise.printToConsole("data.length saveArrayInHtmlFileForRetention 2::::"+data.length);

			try {
				String customDataStr[][]= new String [data.length][totalLength];
				String tokens= "";
				AdminWise.printToConsole("customData.size saveArrayInHtmlFileForRetention 2 ::::"+customData.size());

				for(int k=0;k<newCustomList.size();k++){
					htmlContaints.add("<td width='" + columnWidth + "%' align='center' BGCOLOR='#cccc99'><B>" + newCustomList.get(k).toString() + "</B></td>");
				}
				htmlContaints.add("</tr>");
				AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 5 !!!!");

				for(int n=0; n< data.length; n++){
					StringTokenizer st= new StringTokenizer(customData.get(n).toString(), "\t");
					tokens= tokens+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken()+st.nextToken();
					for(int l=0; l<newCustomList.size(); l++){
						if(st.hasMoreTokens()){
							customDataStr[n][l]= st.nextToken();
						}
					}
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 6 !!!!");
				for(int l=0; l< data.length; l++){
					int m=5;
					for(int n=0; n<newCustomList.size(); n++){
						data[l][m]= customDataStr[l][n];
						m++;
					}
				}

				int temp = dataLimit*count;
				if(dataLimit*(fileCount-1) < data.length && count == fileCount)
					temp = data.length;
				for(i=fromIndex-1; i<temp; i++)
				{
					htmlContaints.add("<tr>");
					for (int j = 0; j < totalLength; j++) {
						if (data[i][j] == null) {
							htmlContaints.add("<td width='" + columnWidth + "%' >" + "-" + "</td>");
						} else {
							htmlContaints.add("<td width='" + columnWidth + "%' >" + data[i][j] + "</td>");
						}
					}
					htmlContaints.add("</tr>");
				}
				AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 7 !!!!");
				htmlContaints.add("</table>");
				htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.recordsFound")+" "+(fromIndex)+"\t"+AdminWise.connectorManager.getString("VWSaveAsHtml.to")+" "+temp+"</font></span></p>");
				htmlContaints.add("<p align='left'><span lang='en-us'><font size='2'>"+AdminWise.connectorManager.getString("VWSaveAsHtml.date")+" "+(new Date()).toString()+"</font></span></p>");
				htmlContaints.add("</body>");
				htmlContaints.add("</html>");
				fromIndex = count*dataLimit+1;
				tempPath = path+"_"+VWUtil.rawDate()+".html";
				flag = VWUtil.writeListToFile(htmlContaints,tempPath,parent);
				AdminWise.printToConsole("saveArrayInHtmlFileForRetention BreakPoint 8 !!!!");
			}catch (Exception ex){
				AdminWise.printToConsole("EXCEPTION occured during Retention Report Generation 2 ::::  "+ex.getMessage());
				return false;
			}
		}
		return flag;
	}
	private static boolean saveArrayInXMLFile(String[][] data,String[] caption,
			String path,Component parent,String title)
	{
		AdminWise.printToConsole("saveArrayInXMLFile 1!!!!");
		Vector XMLContaints=new Vector();
		XMLContaints.add("<?xml version='1.0'?>");
		XMLContaints.add("<"+VWUtil.fixXMLString(title).replace(' ','_')+">");
		XMLContaints.add("<RecordCount>"+data.length+"</RecordCount>");
		for(int i=0;i<data.length;i++)
		{
			XMLContaints.add("<Record"+i+">");
			for(int j=0;j<caption.length;j++)
				XMLContaints.add("<"+VWUtil.fixXMLString(caption[j]).replace(' ','_')+">" + 
						VWUtil.fixXMLString(data[i][j])+"</"+VWUtil.fixXMLString(caption[j]).replace(' ','_')+">");
			XMLContaints.add("</Record"+i+">");
		}
		XMLContaints.add("</"+VWUtil.fixXMLString(title).replace(' ','_')+">");
		return VWUtil.writeListToFile(XMLContaints,path,parent);
	}
	//------------------------------------------------------------------------------ 
	public static String loadSaveDialog(Component parent)
	{
		AdminWise.adminPanel.setWaitPointer();
		JFileChooser chooser = new JFileChooser();
		VWFileFilter filter = new VWFileFilter();
		filter.addExtension("html");
		filter.addExtension("xml");
		filter.setDescription("Html | XML Files");
		chooser.setCurrentDirectory(new File("c:\\"));
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(parent);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			String path=chooser.getSelectedFile().getPath();
			if(!path.toUpperCase().endsWith(".HTML") && !path.toUpperCase().endsWith(".XML")) path+=".html";
			return path;
		};
		return "";
	}
	//------------------------------------------------------------------------------
	/*
    String selFile="";
        SwingUtilities.invokeLater(new Runnable() {
         public void run() {
        JFileChooser chooser = new JFileChooser();
        VWFileFilter filter = new VWFileFilter();
        filter.addExtension("html");
        filter.addExtension("xml");
        filter.setDescription("Html | XML Files");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showSaveDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            String path=chooser.getSelectedFile().getPath();
            if(!path.toUpperCase().endsWith(".HTML") && !path.toUpperCase().endsWith(".XML")) path+=".html";
            ///selFile=path;
            ///return path;
        }}});
        ///return "";
        return selFile;
        */
}
 