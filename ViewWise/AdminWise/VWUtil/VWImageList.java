/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWImageList<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWUtil;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;

//------------------------------------------------------------------------------
public class VWImageList extends JList {
    
    public VWImageList()
    {
	this(0);
    }
//------------------------------------------------------------------------------
    public VWImageList(int selectionType)
    {
	setLayout(new BorderLayout());
        setBackground(java.awt.Color.white);
        if(selectionType==0)
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        else
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setCellRenderer(new ImageListRenderer());
    }
//------------------------------------------------------------------------------
    public VWImageList(int selectionType,Object[] listData) 
    {
        this(selectionType);
        loadData(listData);
    }
//------------------------------------------------------------------------------ 
    public void loadData(List listData)
    {
        if(listData==null || listData.size()==0) return;
        int count =listData.size();
        Object[] listItems=new Object[count];
        for(int i=0;i<count;i++)    listItems[i]=listData.get(i);
        loadData(listItems);
    }
//------------------------------------------------------------------------------  
    public void loadData(Object[] listData)
    {
        setListData(listData);
    }
//------------------------------------------------------------------------------
    public int getItemsCount()
    {
        return getModel().getSize();
    }
//------------------------------------------------------------------------------
    public Vector getItems()
    {
        int count=getModel().getSize();
        Vector dataList=new Vector();
        for(int i=0;i<count;i++)
            dataList.add(getModel().getElementAt(i));
        return dataList;
    }
}
//------------------------------------------------------------------------------
class ImageListRenderer extends javax.swing.DefaultListCellRenderer
{
     public Component getListCellRendererComponent( 
         JList list,Object value,int index, 
         boolean isSelected,boolean cellHasFocus) 
     { 
         Component retValue = super.getListCellRendererComponent( 
            list, value, index, isSelected, cellHasFocus 
        ); 
        setIcon(VWImages.ConUserIcon);
        /*
         if(value instanceof AclEntry)
        {
            if(((VWRecord)value).getSecName().equals("1"))
                setIcon(VWImages.ConUserIcon);
            else
                setIcon(VWImages.ConUserIcon);
        }
        */
        return retValue; 
     }
}
//------------------------------------------------------------------------------