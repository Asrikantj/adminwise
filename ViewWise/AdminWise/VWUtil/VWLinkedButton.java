/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWLinkedButton<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWUtil;

import javax.swing.JButton;
import java.awt.Cursor;
import javax.swing.border.EtchedBorder;
import javax.swing.SwingConstants;
public class VWLinkedButton extends JButton
{
    public VWLinkedButton(int alignment)
    {
        setBorderPainted(false);
        setFocusPainted(true);
        setContentAreaFilled(false); 
        //setBackground(new java.awt.Color(204,204,204));
        //setForeground(java.awt.Color.blue);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        //this.setMargin(new java.awt.Insets(2,0,0,2));
        normalBorder=getBorder();
        //setBorder(etchedBorder);
        if(alignment==1)
        {
            setHorizontalAlignment(SwingConstants.LEFT);
        }
        else
        {
            setHorizontalAlignment(SwingConstants.CENTER);
        }
    }
    public VWLinkedButton(int alignment, boolean dialog)
    {
    	if(dialog){
    		setBorderPainted(true);
    		setFocusPainted(true);
    		setContentAreaFilled(true);
    	}else{
    		setBorderPainted(false);
    		setFocusPainted(true);
    		setContentAreaFilled(false);
    	}
        //setBackground(new java.awt.Color(204,204,204));
        //setForeground(java.awt.Color.blue);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        //this.setMargin(new java.awt.Insets(2,0,0,2));
        normalBorder=getBorder();
        //setBorder(etchedBorder);
        if(alignment==1)
        {
            setHorizontalAlignment(SwingConstants.LEFT);
        }
        else
        {
            setHorizontalAlignment(SwingConstants.CENTER);
        }
    }
    /*
    public VWLinkedButton()
    {
        new VWLinkedButton(1);
    }
     */
    public VWLinkedButton(javax.swing.Icon icon)
    {
        this(icon,true);
    }
    public VWLinkedButton(javax.swing.Icon icon,boolean dynamic)
    {
            super(icon);
           // setFocusPainted(false);
           // setBackground(new java.awt.Color(204,204,204));
            //setForeground(java.awt.Color.blue);
            
            SymMouse aSymMouse = new SymMouse();
            addMouseListener(aSymMouse);
            setRequestFocusEnabled(false);
           // normalBorder=getBorder();
            if(dynamic)
            {
                this.setMargin(new java.awt.Insets(2,0,0,2));
                normalBorder=getBorder();
                //setBorder(etchedBorder);
            }
            else
            {
                this.setMargin(new java.awt.Insets(0,0,0,0));
                //normalBorder=null;
            }                 
    }
	class SymMouse extends java.awt.event.MouseAdapter
	{
            public void mouseExited(java.awt.event.MouseEvent event)
            {
                Object object = event.getSource();
                if (object == VWLinkedButton.this)
                        JButton_mouseExited(event);
            }
            public void mouseEntered(java.awt.event.MouseEvent event)
            {
                Object object = event.getSource();
                if (object == VWLinkedButton.this)
                        JButton_mouseEntered(event);
            }
	}

	void JButton_mouseEntered(java.awt.event.MouseEvent event)
	{
            if(isEnabled())
            {
                //if(normalBorder!=null) setBorder(normalBorder);
                setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
	}
	void JButton_mouseExited(java.awt.event.MouseEvent event)
	{
            //if(normalBorder!=null) setBorder(etchedBorder);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
        private javax.swing.border.EtchedBorder etchedBorder = new javax.swing.border.EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
        private javax.swing.border.Border normalBorder = null;
}
