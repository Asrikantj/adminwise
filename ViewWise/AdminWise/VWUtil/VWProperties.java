package ViewWise.AdminWise.VWUtil;

import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.Properties;
import java.io.File;

public class VWProperties implements VWConstant
{ 
    public VWProperties(String[] params)
    {
        homeDir=System.getProperty("java.io.tmpdir");
    }
//------------------------------------------------------------------------------
   public static void loadVWClientPropreties()
    {
        System.setProperties(System.getProperties());
        System.setProperty("javaplugin.jre.params","-Djava.library.path="+getLibraryPath());
    }
//------------------------------------------------------------------------------   
   public static String getCachDirPath()
   {
       return homeDir+Temp_Folder_Name;
   }
//------------------------------------------------------------------------------
   public static String getLibraryPath()
   {
       return homeDir+System_Folder_Name;
   }
//------------------------------------------------------------------------------
   public static String getRefPath()
   {
       return homeDir+Ref_Folder_Name;
   }
//------------------------------------------------------------------------------
   public static String getHelpPath()
   {
       return homeDir+Help_Folder_Name;
   }
//------------------------------------------------------------------------------

   private static void getSystemProperties()
   {
       Properties sysProp=System.getProperties();
       java.util.Enumeration enumProperties=sysProp.propertyNames();
       String str="";
       while(enumProperties.hasMoreElements())
       {
           str=(String)enumProperties.nextElement();
           System.setProperty(str,sysProp.getProperty(str));
       }
   }
//------------------------------------------------------------------------------
   private static String homeDir="";
}