package ViewWise.AdminWise.VWUtil;

import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import  javax.swing.JList;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWAuditTrail.VWDlgATSetting;

public class VWComboBox extends JComboBox
{
    public VWComboBox()
    {
        super();
        setRenderer(new VWComboBoxRenderer());
    }
    public VWComboBox(Object[] items)
    {
        super(items);
        setRenderer(new VWComboBoxRenderer());
        for(int i=0;i<items.length;i++)
        {
            cmbToolTips.add(items[i].toString().trim());
        }
    }
    public VWComboBox(List items)
    {
        this(VWUtil.listTo1DArrOfString(items));
    }
    public void removeItemAt(int anIndex)
    {
        super.removeItemAt(anIndex);
        cmbToolTips.remove(anIndex);
    }
    public void removeAllItems()
    {
        super.removeAllItems();
        cmbToolTips=new LinkedList();
    }
    public void insertItemAt(Object anObject,int index)
    {
        super.insertItemAt(anObject,index);
        cmbToolTips.add(index,anObject.toString().toString().trim());
    }
    public void addItem(Object anObject)
    {
        super.addItem(anObject);
        cmbToolTips.add(anObject.toString().trim());
    }
    public void addItems(Object[] anObject)
    {
        if(anObject==null) return;
        int count=anObject.length;
        for(int i=0;i<count;i++)
        {
            if(anObject[i]!=null)
            {
                super.addItem(anObject[i]);
                cmbToolTips.add(anObject[i].toString().trim());
            }
        }
    }
    
    /* Purpose:  Added for ARS service Master enable and disable
    * Created By: C.Shanmugavalli		Date:	22 Sep 2006
    */
    public void addItemsForARS(Object[] anObject, int arsEnable)
    {
    	//Retrieved the contents of [VWDlgATSetting.auditTrialSettings] to the local variable vAuditTrialSettings. When adding the 
    	//anObject Object array contents to Combo, it is checked weather it exists in the local Vector vAuditTrialSettings or not.
    	//Author :Nishad Nambiar
    	Vector vAuditTrialSettings = new Vector();
    	vAuditTrialSettings = VWDlgATSetting.auditTrialSettings;
        if(anObject==null) return;
        int count=anObject.length;
        for(int i=0;i<count;i++)
        {
            if(anObject[i]!=null)
            {
            		boolean isSelectedSetting = vAuditTrialSettings.contains(anObject[i].toString().trim());
            		if(isSelectedSetting){
            			super.addItem(anObject[i]);
            			cmbToolTips.add(anObject[i].toString().trim());
            		}
            }
        }
        
    }
    
    //Desc   :This function loads 'All Events' in Combo Box
    //Author :Nishad Nambiar
    //Date   :13-Mar-2007
    public void addAllEventsForARS(Object[] anObject, int arsEnable)
    {
    	Vector vAuditTrialAllEvents = new Vector();
    	vAuditTrialAllEvents.add(VWConstant.allEvents);
    	//vAuditTrialAllEvents = VWDlgATSetting.auditTrialAllEvents;
        if(anObject==null) return;
        int count=anObject.length;
        for(int i=0;i<vAuditTrialAllEvents.size();i++)
        {
            if(anObject[i]!=null)
            {
           		boolean isSelectedSetting = vAuditTrialAllEvents.contains(anObject[i].toString().trim());
       			super.addItem(anObject[i]);
       			cmbToolTips.add(anObject[i].toString().trim());
            }
        }
    }
    
    public void addAllItemsForARS(Object[] anObject, int arsEnable)
    {
        if(anObject==null) return;
        int count=anObject.length;
        for(int i=0;i<count;i++)
        {
            if(anObject[i]!=null)
            {
            	if(arsEnable == 0 && (anObject[i].toString().trim().equalsIgnoreCase(AdminWise.connectorManager.getString("VWComboBox.arsEnable"))||
            			anObject[i].toString().trim().equalsIgnoreCase(AdminWise.connectorManager.getString("VWComboBox.anObject_1"))||
            			anObject[i].toString().trim().equalsIgnoreCase(AdminWise.connectorManager.getString("VWComboBox.anObject_2"))) )
            		continue;
            	else{
            		super.addItem(anObject[i]);
            		cmbToolTips.add(anObject[i].toString().trim());
            	}
            }
        }
    }
    public void addItems(List objectList)
    {
        if(objectList==null) return;
        int count=objectList.size();
        for(int i=0;i<count;i++)
        {
            super.addItem(objectList.get(i));
            cmbToolTips.add((objectList.get(i)).toString());
        }
    }
  class VWComboBoxRenderer extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent( JList list, 
           Object value, int index, boolean isSelected, boolean cellHasFocus) {
      if (isSelected) {
        setBackground(list.getSelectionBackground());
        setForeground(list.getSelectionForeground());      
        if (-1 < index) {
          list.setToolTipText((String)cmbToolTips.get(index));
        }
      } else {
        setBackground(list.getBackground());
        setForeground(list.getForeground());
      } 
      setFont(list.getFont());
      setText((value == null) ? "" : value.toString());     
      return this;
    }  
  }
  private List cmbToolTips=new LinkedList();

}
