package ViewWise.AdminWise.VWUtil;

import java.io.*;
import java.util.Hashtable;
import java.net.URL;

public class VWInfoFile
{	
    public static RandomAccessFile raf;
    public static RandomAccessFile tFile;

    public static boolean writePrivateProfileString(String secName,String keyName,
                                    String addStr, String fileName)
    {   
        String str,temp="";
        boolean flag=false;
        String fn1 = "c:\\vwTmpFile.tmp";//this file is created temporarily
        int no;
        try
        {
                File f = new File(fn1);
                if(f.exists()) f.delete();
                raf = new RandomAccessFile(fileName,"rw");
                tFile = new RandomAccessFile(fn1,"rw");
                str = raf.readLine();
                while(str != null)
                {
                        tFile.writeBytes(str + "\r\n");
                        str = str.trim();
                        if((str.charAt(0) == '[') && (str.charAt(str.length()-1) == ']'))
                        {
                                if(str.substring(1,str.length()-1).equalsIgnoreCase(secName))
                                {
                                    while((str = raf.readLine())!= null)
                                    {
                                            if(str.trim().charAt(0) != '[')
                                            {
                                                    str = str.trim();
                                                    no=str.indexOf('=');
                                                    temp=str.substring(0,no);
                                                    if(temp.equalsIgnoreCase(keyName))
                                                    {
                                                            temp = temp + "=" + addStr + "\r\n";
                                                            tFile.writeBytes(temp);
                                                            flag = true;
                                                    }
                                                    else
                                                            tFile.writeBytes(str + "\r\n");
                                            }
                                    } // while
                                    //This is to write new keyname and its vaues
                                    if(!flag)
                                    {
                                            temp = keyName+"="+addStr+"\r\n";
                                            tFile.writeBytes(temp);
                                            flag=true;
                                    }
                                }
                                else
                                {
                                        str = raf.readLine();
                                }
                        }
                        else
                        {
                                str = raf.readLine();
                        }
                } //while str!=null
                if(!flag)
                {
                        tFile.writeBytes("["+secName+"]"+"\r\n");
                        temp = keyName+"="+addStr+"\r\n";
                        tFile.writeBytes(temp);
                }
                tFile.close();
                raf.close();
        }
        catch(IOException e)
        {
        }
                File f1 = new File(fileName);
                File f2 = new File(fn1);
                f1.delete();
                flag = f2.renameTo(new File(fileName));
        return flag;
    }

    public static String getPrivateProfileString(String secName, String keyName,
                                                String defStr, String fileName)
    {
            String str;
            String temp="";
            int no;
            try
            {
                    raf = new RandomAccessFile(fileName,"rw");
                    str = raf.readLine();
    A:	while(str != null)
                    {
                            str = str.trim();
                            if((str.charAt(0) == '[') && (str.charAt(str.length()-1) == ']'))
                            {
                                    if(str.substring(1,str.length()-1).equalsIgnoreCase(secName))
                                    {
                                            while((str = raf.readLine())!= null)
                                            {
                                                if (str.startsWith("!") || str.startsWith("#") || str.length() == 0)
                                                {
                                                    // commented line
                                                    continue;
                                                }
                                                if((str.trim().charAt(0)) != '[')
                                                {
                                                    str = str.trim();
                                                    no=str.indexOf('=');
                                                    temp=str.substring(0,no);
                                                    if(temp.equalsIgnoreCase(keyName))
                                                    {
                                                            temp = str.substring(no+1);
                                                            break A;
                                                    }
                                                }
                                                temp="";
                                            }
                                    }
                            }
                      str = raf.readLine();
                    }
                    raf.close();
            }
            catch(IOException e)
            {
            }
            if(temp=="")
                    return defStr;
            else
                    return temp;
    }

    public static Hashtable getPrivateProfileStrings(String secName,String fileName)
    {
            String str="";
            String value="";
            String keyName="";
            int no;
            Hashtable result=new Hashtable();
            try
            {
                raf = new RandomAccessFile(fileName,"r");
                str = raf.readLine();
                while(str != null)
                {
                            str = str.trim();
                            if((str.charAt(0) == '[') && (str.charAt(str.length()-1) == ']'))
                            {
                                    if(str.substring(1,str.length()-1).equalsIgnoreCase(secName))
                                    {
                                        str = raf.readLine();
                                        while(str!= null)
                                            {
                                                    if((str.trim().charAt(0)) != '[')
                                                    {
                                                            str = str.trim();
                                                            no=str.indexOf('=');
                                                            keyName=str.substring(0,no);
                                                            value = str.substring(no+1);
                                                            result.put(keyName,value);
                                                    }
                                                    keyName="";
                                                    value="";
                                                    str = raf.readLine();
                                            }
                                    }
                            }
                      str = raf.readLine();
                }
                raf.close();
            }
            catch(IOException e){}
            return result;
    }
/*
  URL url =new URL("http://fadi/vwonline/Properties\\ViewWise.properties");
  DataInputStream r = new DataInputStream(url.openConnection().getInputStream());
            System.out.println(r.readLine());
 **/
    public static String getURLProfileString(String secName, String keyName,
                                                String defStr, String fileURL)
    {
            String str;
            String temp="";
            int no;
            try
            {
                URL url =new URL(fileURL);
                DataInputStream raf = new DataInputStream(url.openConnection().getInputStream());
                ///BufferedReader raf = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
                str = raf.readLine();
    A:	while(str != null)
                    {
                            str = str.trim();
                            if((str.charAt(0) == '[') && (str.charAt(str.length()-1) == ']'))
                            {
                                    if(str.substring(1,str.length()-1).equalsIgnoreCase(secName))
                                    {
                                            while((str = raf.readLine())!= null)
                                            {
                                                    if((str.trim().charAt(0)) != '[')
                                                    {
                                                            str = str.trim();
                                                            no=str.indexOf('=');
                                                            temp=str.substring(0,no);
                                                            if(temp.equalsIgnoreCase(keyName))
                                                            {
                                                                    temp = str.substring(no+1);
                                                                    break A;
                                                            }
                                                    }
                                                    temp="";
                                            }
                                    }
                            }
                      str = raf.readLine();
                    }
                    raf.close();
            }
            catch(IOException e)
            {
            }
            if(temp=="")
                    return defStr;
            else
                    return temp;
    }

    public static Hashtable getURLProfileStrings(String secName,String fileURL)
    {        
            String str="";
            String value="";
            String keyName="";
            int no;
            Hashtable result=new Hashtable();
            try
            {
                URL url =new URL(fileURL);
                DataInputStream raf = new DataInputStream(url.openConnection().getInputStream());
                ///BufferedReader raf = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
                str = raf.readLine();
                while(str != null)
                {
                    str = str.trim();
                    if((str.charAt(0) == '[') && (str.charAt(str.length()-1) == ']'))
                    {
                        if(str.substring(1,str.length()-1).equalsIgnoreCase(secName))
                        {
                            while((str = raf.readLine())!= null)
                            {
                                if(str.equals("")) continue;
                                    if((str.trim().charAt(0)) != '[')
                                    {
                                            str = str.trim();
                                            no=str.indexOf('=');
                                            keyName=str.substring(0,no);
                                            value = str.substring(no+1);
                                            result.put(keyName,value);
                                    }
                                    keyName="";
                                    value="";
                            }
                        }
                }
                str = raf.readLine();
            }
            raf.close();
            }
            catch(IOException e){
                System.out.println(e);
                return null;
            }
            return result;
    }
}
