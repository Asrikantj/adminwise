/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWDataStorage<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWUtil;

import java.util.List;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWRecord;
//------------------------------------------------------------------------------
public class VWDataStorage implements VWConstant
{
    public static void createStorage(int roomNum)
    {
        gUsers=new String[roomNum][];
        gDataTypes=new VWRecord[roomNum][];
    }
//------------------------------------------------------------------------------
public static String[] getUsers(int roomId)
    {
        if (gUsers==null || roomId ==0 || gUsers[roomId-1]==null)
            return null;    
        return gUsers[roomId-1];    
    }
//------------------------------------------------------------------------------
public static void setUsers(List usersData,int roomId)
    {
        int count=usersData.size();
        gUsers[roomId-1]=new String [count];
        for(int i=0;i<count;i++)
            gUsers[roomId-1][i]=(String)usersData.get(i);
    }
//------------------------------------------------------------------------------
public static void clearUsers(int roomId)
    {
        gUsers[roomId-1]=null;
    }
//------------------------------------------------------------------------------
public static VWRecord[] getDocTypes(int roomId)
    {
        if (gDataTypes==null || roomId==0)
            return null;    
        return gDataTypes[roomId-1];    
    }
//------------------------------------------------------------------------------
public static void setDocTypes(List data,int roomId)
    {
        int count=data.size();
        gDataTypes[roomId-1]=new VWRecord [count];
        for(int i=0;i<count;i++)
            gDataTypes[roomId-1][i]=new VWRecord((String)data.get(i));
    }
//------------------------------------------------------------------------------
public static void clearDocTypes(int roomId)
    {
        gDataTypes[roomId-1]=null;
    }
//------------------------------------------------------------------------------
private static String[][] gUsers=null;
private static VWRecord[][] gDataTypes=null;
}