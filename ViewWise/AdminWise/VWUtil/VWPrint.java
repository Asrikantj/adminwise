/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWUtil<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWUtil;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.print.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.Dimension;

public class VWPrint implements Printable{
  JTable tableView;
    public VWPrint(JTable table) {
        tableView=table;
        PrinterJob pj=PrinterJob.getPrinterJob();
        pj.setPrintable(VWPrint.this);
        PrintRequestAttributeSet pras = createAttributes();
        if(pj.printDialog(pras))
        try{
            pj.print();
        }catch (Exception PrintException) {}
    }
    
    private PrintRequestAttributeSet createAttributes() {
    	PrintRequestAttributeSet atts = new HashPrintRequestAttributeSet();
    	atts.add(OrientationRequested.LANDSCAPE);
    	return atts;
    }
    
//------------------------------------------------------------------------------
     public int print(Graphics g, PageFormat pageFormat, 
        int pageIndex) throws PrinterException {
     	Graphics2D  g2 = (Graphics2D) g;
     	g2.setColor(Color.black);
     	int fontHeight=g2.getFontMetrics().getHeight();
     	int fontDesent=g2.getFontMetrics().getDescent();
     	//leave room for page number
     	double pageHeight = 
     	  pageFormat.getImageableHeight()-fontHeight;
     	double pageWidth = 
     	  pageFormat.getImageableWidth();
     	double tableWidth = (double) 
          tableView.getColumnModel(
          ).getTotalColumnWidth();
     	double scale = 1; 
     	if (tableWidth >= pageWidth) {
		scale =  pageWidth / tableWidth;
	}

     	double headerHeightOnPage=
                 tableView.getTableHeader(
                 ).getHeight()*scale;
     	double tableWidthOnPage=tableWidth*scale;

     	double oneRowHeight=(tableView.getRowHeight()+
                      tableView.getRowMargin())*scale;
     	int numRowsOnAPage=
              (int)((pageHeight-headerHeightOnPage)/
                                  oneRowHeight);
     	double pageHeightForTable=oneRowHeight*
     	                            numRowsOnAPage;
     	int totalNumPages= 
     	      (int)Math.ceil((
                (double)tableView.getRowCount())/
                                    numRowsOnAPage);
     	if(pageIndex>=totalNumPages) {
                      return NO_SUCH_PAGE;
     	}

     	g2.translate(pageFormat.getImageableX(), 
                       pageFormat.getImageableY());
//bottom center
     	g2.drawString("Page: "+(pageIndex+1),
     	    (int)pageWidth/2-35, (int)(pageHeight
     	    +fontHeight-fontDesent));

     	g2.translate(0f,headerHeightOnPage);
     	g2.translate(0f,-pageIndex*pageHeightForTable);

     	//If this piece of the table is smaller 
     	//than the size available,
     	//clip to the appropriate bounds.
     	if (pageIndex + 1 == totalNumPages) {
           int lastRowPrinted = 
                 numRowsOnAPage * pageIndex;
           int numRowsLeft = 
                 tableView.getRowCount() 
                 - lastRowPrinted;
           g2.setClip(0, 
             (int)(pageHeightForTable * pageIndex),
             (int) Math.ceil(tableWidthOnPage),
             (int) Math.ceil(oneRowHeight * 
                               numRowsLeft));
     	}
     	//else clip to the entire area available.
     	else{    
             g2.setClip(0, 
             (int)(pageHeightForTable*pageIndex), 
             (int) Math.ceil(tableWidthOnPage),
             (int) Math.ceil(pageHeightForTable));        
     	}

     	g2.scale(scale,scale);
     	tableView.paint(g2);
     	g2.scale(1/scale,1/scale);
     	g2.translate(0f,pageIndex*pageHeightForTable);
     	g2.translate(0f, -headerHeightOnPage);
     	g2.setClip(0, 0,
     	  (int) Math.ceil(tableWidthOnPage), 
          (int)Math.ceil(headerHeightOnPage));
     	g2.scale(scale,scale);
     	tableView.getTableHeader().paint(g2);
     	//paint header at top

     	return Printable.PAGE_EXISTS;
   }
}