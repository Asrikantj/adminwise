package ViewWise.AdminWise;

import java.io.StreamTokenizer;
import java.util.StringTokenizer;
import java.util.Vector;

import ViewWise.AdminWise.VWPanel.VWAdminPanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;

public class VWViewerTabManager implements VWConstant {
    private static Vector tabsData;
	private static Vector resultData;
	public static void setActivTabData() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        setupTabUI();
        if(room != null && room.getConnectStatus()==Room_Status_Connect) {
            loadDataForActivTab(room);
        }
        else {
            unloadDataForActivTab();
        }
    }
	//Enhacement:-CV10 SubAdministrator Method added to enable or disable the Tabs based on the login user is sub administrator or not
	public static void getTabsDisplayData(VWRoom room,String user){
		int securityAdmin=0;
		if(room != null && room.getConnectStatus()==Room_Status_Connect)
			tabsData = new Vector();
		AdminWise.gConnector.ViewWiseClient.getSubAdminTabs(room.getId(),user,tabsData);
		securityAdmin=AdminWise.gConnector.ViewWiseClient.isSecurityAdmin(room.getId());
		resultData=new Vector();
		if(tabsData.size()>0&&(room.getConnectStatus()==0)&&(securityAdmin!=1)){
			StringTokenizer st = new StringTokenizer ( tabsData.get(0).toString(), ";");
			while(st.hasMoreTokens())
			{
				resultData.add(st.nextToken());
			}

			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ARCHIVE_ID, false);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RECYCLE_ID, false);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_TEMPLATE_ID, false);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_BACKUP_ID, false);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RESTORE_ID, false);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_REDACTIONS_ID, false);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_PRINCIPAL_ID, false);

			AdminWise.printToConsole("resultData inside getTabsDisplayData :::: "+resultData);
			if(resultData.contains("DocType"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCTYPE_ID, true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCTYPE_ID, false);

			if(resultData.contains("AuditTrail"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_AUDITTRAIL_ID, true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_AUDITTRAIL_ID, false);

			if(resultData.contains("ConnectedUser"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_CONUSER_ID, true);
			else
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_CONUSER_ID, false);

			if(resultData.contains("LockedDocs"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCLOCK_ID, true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCLOCK_ID, false);

			if(resultData.contains("Statistics"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_STATISTIC_ID, true);
			else
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_STATISTIC_ID, false);
			if(resultData.contains("Retention"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RETENTION_ID,true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RETENTION_ID,false);

			if(resultData.contains("Notification"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_NOTIFICATION_ID,true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_NOTIFICATION_ID,false);
			
			if(resultData.contains("Form Template"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_FORM_TEMPLATE_ID,true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_FORM_TEMPLATE_ID,false);
			
			if(resultData.contains("Secure Link"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_SECURE_LINK_ID,true);
			else 
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_SECURE_LINK_ID,false);

			if(resultData.contains("WorkFlows"))
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ROUTE_ID, true);
			else
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ROUTE_ID, false);
			if(resultData.contains("WorkFlow Report")) {
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ROUTEREPORT_ID, true);
			}
			else {
				AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ROUTEREPORT_ID, false);
			}
			
		}else{
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCTYPE_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ARCHIVE_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RECYCLE_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_TEMPLATE_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_BACKUP_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RESTORE_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_REDACTIONS_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_PRINCIPAL_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_AUDITTRAIL_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_CONUSER_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCLOCK_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_STATISTIC_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_DOCLOCK_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_RETENTION_ID,true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_NOTIFICATION_ID,true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_FORM_TEMPLATE_ID,true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_SECURE_LINK_ID,true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ROUTE_ID, true);
			AdminWise.adminPanel.MainTab.setEnabledAt(TAB_ROUTEREPORT_ID, true);
		}

	}
    //--------------------------------------------------------------------------
    public static void loadDataForActivTab(VWRoom room) {
    	AdminWise.printToConsole("loadDataForActivTab :::: "+room);
        int selectTab=AdminWise.adminPanel.MainTab.getSelectedIndex();
        AdminWise.printToConsole("selectTab loadDataForActivTab :::: "+selectTab);
        AdminWise.adminPanel.setWaitPointer();
        try{
            switch(selectTab) {
                case TAB_DOCTYPE_ID:
                    AdminWise.adminPanel.docTypePanel.loadTabData(room);
                    break;
                case TAB_ARCHIVE_ID:
                    AdminWise.adminPanel.archivePanel.loadTabData(room);
                    break;
                case TAB_AUDITTRAIL_ID:
                    AdminWise.adminPanel.auditTrailPanel.loadTabData(room);
                    break;
                case TAB_CONUSER_ID:
                    AdminWise.adminPanel.conUserPanel.loadTabData(room);
                    break;
                case TAB_DOCLOCK_ID:
                    AdminWise.adminPanel.docLockPanel.loadTabData(room);
                    break;
                case TAB_RECYCLE_ID:
                    AdminWise.adminPanel.recyclePanel.loadTabData(room);
                    break;
                case TAB_STATISTIC_ID:
                	AdminWise.printToConsole("TAB_STATISTIC_ID :::: "+TAB_STATISTIC_ID);
                    AdminWise.adminPanel.statisticPanel.loadTabData(room);
                    break;
                /*
                case TAB_PROPERTIES_ID:
                    AdminWise.adminPanel.propertiesPanel.loadTabData(room);
                    break;
                case TAB_LOGVIEWER_ID:
                    AdminWise.adminPanel.logViewerPanel.loadTabData(room);
                    break;
                 */
                case TAB_TEMPLATE_ID:
                    AdminWise.adminPanel.templatePanel.loadTabData(room);
                    break;
                case TAB_BACKUP_ID:
                    AdminWise.adminPanel.backupPanel.loadTabData(room);
                    break;
                case TAB_RESTORE_ID:
                    AdminWise.adminPanel.restorePanel.loadTabData(room);
                    break;
                case TAB_REDACTIONS_ID:
                    AdminWise.adminPanel.redactionsPanel.loadTabData(room);
                    break;
                case TAB_PRINCIPAL_ID:
                    AdminWise.adminPanel.principalPanel.loadTabData(room);
                    break;
                /*case TAB_SECURITY_ID:
                    AdminWise.adminPanel.securityPanel.loadTabData(room);
                    break;*/
                case TAB_ROUTE_ID:
                    AdminWise.adminPanel.routePanel.loadTabData(room);
                    break;
                case TAB_RETENTION_ID:
                    AdminWise.adminPanel.retentionPanel.loadTabData(room);
                    break;
                case TAB_NOTIFICATION_ID:
                    AdminWise.adminPanel.notificationPanel.loadTabData(room);
                    break;
                case TAB_ROUTEREPORT_ID:
                    AdminWise.adminPanel.routeReportPanel.loadTabData(room);
                    break;
                case TAB_FORM_TEMPLATE_ID:
                    AdminWise.adminPanel.formTemplatePanel.loadTabData(room);
                    break;
                case TAB_SECURE_LINK_ID:
                	AdminWise.printToConsole("TAB_SECURE_LINK_ID :::: "+TAB_SECURE_LINK_ID);
                	AdminWise.adminPanel.secureLinkPanel.loadTabData(room);
                	break;
            }
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
        }
    }
    //--------------------------------------------------------------------------
    public static void unloadDataForActivTab() {
        int selectTab=AdminWise.adminPanel.MainTab.getSelectedIndex();
        AdminWise.adminPanel.setWaitPointer();
        try{
            switch(selectTab) {
                case TAB_DOCTYPE_ID:
                    AdminWise.adminPanel.docTypePanel.unloadTabData();
                    break;
                case TAB_ARCHIVE_ID:
                    AdminWise.adminPanel.archivePanel.unloadTabData();
                    break;
                case TAB_AUDITTRAIL_ID:
                    AdminWise.adminPanel.auditTrailPanel.unloadTabData();
                    break;
                case TAB_CONUSER_ID:
                    AdminWise.adminPanel.conUserPanel.unloadTabData();
                    break;
                case TAB_DOCLOCK_ID:
                    AdminWise.adminPanel.docLockPanel.unloadTabData();
                    break;
                case TAB_RECYCLE_ID:
                    AdminWise.adminPanel.recyclePanel.unloadTabData();
                    break;
                case TAB_STATISTIC_ID:
                    AdminWise.adminPanel.statisticPanel.unloadTabData();
                    break;
                /*
                case TAB_PROPERTIES_ID:
                    AdminWise.adminPanel.propertiesPanel.unloadTabData();
                    break;
                case TAB_LOGVIEWER_ID:
                    AdminWise.adminPanel.logViewerPanel.unloadTabData();
                    break;
                 */
                case TAB_TEMPLATE_ID:
                    AdminWise.adminPanel.templatePanel.unloadTabData();
                    break;
                case TAB_BACKUP_ID:
                    AdminWise.adminPanel.backupPanel.unloadTabData();
                    break;
                case TAB_RESTORE_ID:
                    AdminWise.adminPanel.restorePanel.unloadTabData();
                    break;
                case TAB_REDACTIONS_ID:
                    AdminWise.adminPanel.redactionsPanel.unloadTabData();
                    break;
                case TAB_PRINCIPAL_ID:
                    AdminWise.adminPanel.principalPanel.unloadTabData();
                    break;
                /*case TAB_SECURITY_ID:
                    AdminWise.adminPanel.securityPanel.unloadTabData();
                    break;*/
                case TAB_ROUTE_ID:
                	AdminWise.adminPanel.routePanel.unloadTabData();
                    break;
                case TAB_RETENTION_ID:
                	AdminWise.adminPanel.retentionPanel.unloadTabData();
                    break;
                case TAB_NOTIFICATION_ID:
                	AdminWise.adminPanel.notificationPanel.unloadTabData();
                    break;
                case TAB_ROUTEREPORT_ID:
                    AdminWise.adminPanel.routeReportPanel.unloadTabData();
                    break;
                case TAB_FORM_TEMPLATE_ID:
                	AdminWise.adminPanel.formTemplatePanel.unloadTabData();
                    break;
                case TAB_SECURE_LINK_ID:
                    AdminWise.adminPanel.secureLinkPanel.unloadTabData();
                    break;
            }
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
        }
    }
    //--------------------------------------------------------------------------
    public static void setupTabUI() {
        int selectTab=AdminWise.adminPanel.MainTab.getSelectedIndex();
        try{
            switch(selectTab) {
                case TAB_DOCTYPE_ID:
                    AdminWise.adminPanel.docTypePanel.setupUI();
                    break;
                case TAB_ARCHIVE_ID:
                    AdminWise.adminPanel.archivePanel.setupUI();
                    break;
                case TAB_AUDITTRAIL_ID:
                    AdminWise.adminPanel.auditTrailPanel.setupUI();
                    break;
                case TAB_CONUSER_ID:
                    AdminWise.adminPanel.conUserPanel.setupUI();
                    break;
                case TAB_DOCLOCK_ID:
                    AdminWise.adminPanel.docLockPanel.setupUI();
                    break;
                    
                case TAB_RECYCLE_ID:
                    AdminWise.adminPanel.recyclePanel.setupUI();
                    break;
                case TAB_STATISTIC_ID:
                    AdminWise.adminPanel.statisticPanel.setupUI();
                    break;
                case TAB_TEMPLATE_ID:
                    AdminWise.adminPanel.templatePanel.setupUI();
                    break;
                case TAB_BACKUP_ID:
                    AdminWise.adminPanel.backupPanel.setupUI();
                    break;
                case TAB_RESTORE_ID:
                    AdminWise.adminPanel.restorePanel.setupUI();
                    break;
                case TAB_REDACTIONS_ID:
                    AdminWise.adminPanel.redactionsPanel.setupUI();
                    break;
                case TAB_PRINCIPAL_ID:
                    AdminWise.adminPanel.principalPanel.setupUI();
                    break;
                /*case TAB_SECURITY_ID:
                    AdminWise.adminPanel.securityPanel.setupUI();
                    break;*/
                case TAB_ROUTE_ID:
                    AdminWise.adminPanel.routePanel.setupUI();
                    break;
                case TAB_RETENTION_ID:
                    AdminWise.adminPanel.retentionPanel.setupUI();
                    break;
                case TAB_NOTIFICATION_ID:
                    AdminWise.adminPanel.notificationPanel.setupUI();
                    break;
                    
                case  TAB_ROUTEREPORT_ID:
                	  AdminWise.adminPanel.routeReportPanel.setupUI();
                	  break;
                
                case  TAB_FORM_TEMPLATE_ID:
              	  AdminWise.adminPanel.formTemplatePanel.setupUI();
              	  break;
              	  
                case TAB_SECURE_LINK_ID:
                    AdminWise.adminPanel.secureLinkPanel.setupUI();
                    break;
                    
            }
        }
        catch(Exception e){}
    }
}