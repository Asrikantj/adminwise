package ViewWise.AdminWise.VWLogin;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.border.TitledBorder;
import java.awt.Insets;
import java.util.prefs.*;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;

public class VWLoginScreen extends javax.swing.JDialog implements VWLoginConstant,VWConstant
{
	public VWLoginScreen(Frame frame, String title, String[] values)
	{
            super(frame,title,true);
            frame.setIconImage(VWImages.AdminWiseIcon.getImage());
            getContentPane().setBackground(AdminWise.getAWColor());
            getContentPane().setLayout(null);
            String localeLanguage=AdminWise.getLocaleLanguage();
            if(localeLanguage.equals("nl_NL")){
            	 setSize(420,190);
            }else
            setSize(405,190);
            setVisible(false);
            PnlGeneral.setBorder(new javax.swing.border.TitledBorder(lbl_Border_Title));
            PnlGeneral.setLayout(null);
            PnlGeneral.setBackground(AdminWise.getAWColor());
            getContentPane().add(PnlGeneral);
            if(localeLanguage.equals("nl_NL")){
            PnlGeneral.setBounds(6,8,388+15,110);
            }else
            PnlGeneral.setBounds(6,8,388,110);
            JLabel1.setText(lbl_UserName);
            PnlGeneral.add(JLabel1);
            AdminWise.printToConsole("Locale :::"+localeLanguage);
            if(localeLanguage.equals("nl_NL")){
            JLabel1.setBounds(12,24,78+12, AdminWise.gBtnHeight);
            }else{
             JLabel1.setBounds(12,24,78, AdminWise.gBtnHeight);
            }
            JLabel2.setText(lbl_Password);
            PnlGeneral.add(JLabel2);
            if(localeLanguage.equals("nl_NL")){
            JLabel2.setBounds(12,52,68+12,28);
            }else{
            JLabel2.setBounds(12,52,68,28);
            }
            JLabel3.setText(lbl_UseFor);
            PnlGeneral.add(JLabel3);
            if(localeLanguage.equals("nl_NL")){
            JLabel3.setBounds(12,82,152+18,28);
            }else{
            JLabel3.setBounds(12,82,152,28);
            }
            PnlGeneral.add(TxtUserName);
            if(localeLanguage.equals("nl_NL")){
            	TxtUserName.setBounds(108,24,272+8+2,20);
            }else
            	TxtUserName.setBounds(108,24,272,20);
            PnlGeneral.add(TxtPassword);
            if(localeLanguage.equals("nl_NL")){
            	TxtPassword.setBounds(108,56,272+8+2,20);
            }else
            	TxtPassword.setBounds(108,56,272,20);
            
            javax.swing.ButtonGroup group = new javax.swing.ButtonGroup(); 
            RdoThisRoom.setSelected(true);
            group.add(RdoThisRoom); 
            RdoThisRoom.setText(lbl_ViewType_ThisRoom);
            RdoThisRoom.setIcon(VWImages.RadioIcon5); 
            RdoThisRoom.setPressedIcon(VWImages.RadioIcon2); 
            RdoThisRoom.setRolloverIcon(VWImages.RadioIcon3); 
            RdoThisRoom.setRolloverSelectedIcon(VWImages.RadioIcon4); 
            RdoThisRoom.setSelectedIcon(VWImages.RadioIcon1); 
            RdoThisRoom.setFocusPainted(false); 
            RdoThisRoom.setBorderPainted(false); 
            RdoThisRoom.setContentAreaFilled(false); 
            RdoThisRoom.setMargin(new Insets(0,0,0,0)); 
            PnlGeneral.add(RdoThisRoom);
            if(localeLanguage.equals("nl_NL")){
            RdoThisRoom.setBounds(166+20,86,94+6,18);
            }else 
            RdoThisRoom.setBounds(166,86,94,18);
            RdoAllRoom.setText(lbl_ViewType_AllRooms);
            RdoAllRoom.setIcon(VWImages.RadioIcon5); 
            RdoAllRoom.setPressedIcon(VWImages.RadioIcon2); 
            RdoAllRoom.setRolloverIcon(VWImages.RadioIcon3); 
            RdoAllRoom.setRolloverSelectedIcon(VWImages.RadioIcon4); 
            RdoAllRoom.setSelectedIcon(VWImages.RadioIcon1); 
            RdoAllRoom.setFocusPainted(false); 
            RdoAllRoom.setBorderPainted(false); 
            RdoAllRoom.setContentAreaFilled(false); 
            RdoAllRoom.setMargin(new Insets(0,0,0,0)); 
            group.add(RdoAllRoom); 
            PnlGeneral.add(RdoAllRoom);
            if(localeLanguage.equals("nl_NL")){
            RdoAllRoom.setBounds(296+6,86,94,18);
            }else
            RdoAllRoom.setBounds(296,86,94,18);
            BtnLogin.setText(BTN_LOGIN_NAME);
            BtnLogin.setActionCommand(BTN_LOGIN_NAME);
//            BtnLogin.setBackground(java.awt.Color.white);
            getContentPane().add(BtnLogin);
            if(localeLanguage.equals("nl_NL")){
            	 BtnLogin.setBounds(195+13,128,95,25);
            	 //388+15
            }else
            BtnLogin.setBounds(220,128,73,25);//(89,128,73,25);
            BtnLogin.setIcon(VWImages.ConnectIcon);
            BtnCancel.setText(BTN_CANCEL_NAME);
            BtnCancel.setActionCommand(BTN_CANCEL_NAME);
//            BtnCancel.setBackground(java.awt.Color.white);
            getContentPane().add(BtnCancel);
            if(localeLanguage.equals("nl_NL")){
            BtnCancel.setBounds(300+13,128,95,25);//(233,128,73,25); //RdoAllRoom.setBounds(296,86,94,18);
            }else{
             BtnCancel.setBounds(310,128,83,25);
            }
            BtnCancel.setIcon(VWImages.CloseIcon);
            //{{REGISTER_LISTENERS
            RdoThisRoom.setSelected(values[2].equals("1"));
            SymAction lSymAction = new SymAction();
            BtnLogin.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            SymKey aSymKey = new SymKey();
            addKeyListener(aSymKey);
            TxtUserName.addKeyListener(aSymKey);
            TxtPassword.addKeyListener(aSymKey);
            BtnLogin.addKeyListener(aSymKey);
            BtnCancel.addKeyListener(aSymKey);
            getDlgOptions(frame);
            setResizable(false);
            setVisible(true);
	}
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
            {
                BtnLogin.requestFocus();
                BtnLogin_actionPerformed(null);
            }
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
            {
                BtnCancel.requestFocus();
                BtnCancel_actionPerformed(null);
            }
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnLogin)
                        BtnLogin_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);			
        }
    }
//------------------------------------------------------------------------------
	//{{DECLARE_CONTROLS
	javax.swing.JPanel PnlGeneral = new javax.swing.JPanel();
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
	javax.swing.JTextField TxtUserName = new javax.swing.JTextField(50);
	javax.swing.JPasswordField TxtPassword = new javax.swing.JPasswordField(50);
	javax.swing.JRadioButton RdoThisRoom = new javax.swing.JRadioButton();
	javax.swing.JRadioButton RdoAllRoom = new javax.swing.JRadioButton();
	VWLinkedButton BtnLogin = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnLogin_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelFlag=false;
        saveDlgOptions();
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelFlag=true;
        setVisible(false);
    }
//------------------------------------------------------------------------------
    public String[] getValues()
    {
        String[] values=new String[3];
        values[0]=TxtUserName.getText();
        /*
		 * JPasswordField.getText() is replaced with JPasswordField.getPassword() 
		 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
        values[1]=new String(TxtPassword.getPassword());
        values[2]=RdoThisRoom.isSelected()?"0":"1";
        return values;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        if(AdminWise.adminPanel.saveLastUserName)
            prefs.put("UserName",TxtUserName.getText());
    }
//------------------------------------------------------------------------------
    private void getDlgOptions(Frame frame)
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        //Dimension d =VWUtil.getScreenSize();
        //int x=prefs.getInt( "x",d.width/4);
        //int y=prefs.getInt( "y",d.height/3);
        //setLocation(x,y);
        setLocationRelativeTo(frame);
        if(AdminWise.adminPanel.saveLastUserName)
            TxtUserName.setText(prefs.get("UserName",""));   
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
}