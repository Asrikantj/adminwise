package ViewWise.AdminWise.VWLogin;

import ViewWise.AdminWise.AdminWise;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;

public interface VWLoginConstant
{
    String[] YesNoData={"Yes","No"};
    String LBL_DIALOG_NAME= ResourceManager.getDefaultManager().getString("CVProduct.Name") + " "+AdminWise.connectorManager.getString("login.adminlogin");
    String lbl_ViewType_ThisRoom=AdminWise.connectorManager.getString("login.lbl_ViewType_ThisRoom");
    String lbl_ViewType_AllRooms=AdminWise.connectorManager.getString("login.lbl_ViewType_AllRooms");
    String lbl_Border_Title=AdminWise.connectorManager.getString("login.lbl_Border_Title");
    String lbl_UseFor=AdminWise.connectorManager.getString("login.lbl_UseFor")+" ";
    String lbl_UserName=AdminWise.connectorManager.getString("login.username")+" ";
    String lbl_Password=AdminWise.connectorManager.getString("login.password")+" ";
    
    
}