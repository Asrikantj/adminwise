/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWNodeSecurity;

import java.awt.*;
import javax.swing.*;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWImages.VWImages;
import com.computhink.common.AclEntry;
import com.computhink.common.Principal;
import ViewWise.AdminWise.VWConstant;

public class VWDlgAddPrincipals extends javax.swing.JDialog implements VWSecurityConstant,VWConstant
{
	public VWDlgAddPrincipals(Frame parent,Vector selectedPrincipals) 
	{
            super(parent);
            getContentPane().setBackground(AdminWise.getAWColor());
            ///parent.setIconImage(VWImages.AddIcon.getImage());
            setTitle(LBL_LISTPRINCIPALS_NAME);
            setModal(true);
            getContentPane().setLayout(null);
            setSize(283,438);
            setVisible(false);
            JLabel1.setText(LBL_LISTPRINCIPALS_NAME);
            getContentPane().add(JLabel1);
            JLabel1.setBounds(6,4,194, AdminWise.gBtnHeight);
            getContentPane().add(LstPrincipal);
            LstPrincipal.setBounds(4,28,276,366);
            BtnAdd.setText(BTN_ADD_NAME);
            getContentPane().add(BtnAdd);
            BtnAdd.setBounds(136,410,72, AdminWise.gBtnHeight);
//            BtnAdd.setBackground(java.awt.Color.white);
            BtnCancel.setText(BTN_CANCEL_NAME);
            getContentPane().add(BtnCancel);
            BtnCancel.setBounds(208,410,72, AdminWise.gBtnHeight);
//            BtnCancel.setBackground(java.awt.Color.white);
            SymAction lSymAction = new SymAction();
            BtnAdd.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            SymKey aSymKey = new SymKey();
            addKeyListener(aSymKey);
            LstPrincipal.addKeyListener(aSymKey);
            SymWindow aSymWindow = new SymWindow();
            this.addWindowListener(aSymWindow);
            loadPrincipals(selectedPrincipals);
            setResizable(false);
            getDlgOptions();
            setVisible(true);            
	}
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgAddPrincipals.this)
                Dialog_windowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnAdd_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnAdd)
                BtnAdd_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    public void setVisible(boolean b)
    {
        if (b) 
        {
            Dimension d =VWUtil.getScreenSize();
            setLocation(d.width/4,d.height/4);
        }
        super.setVisible(b);
    }
//------------------------------------------------------------------------------
    public void addNotify()
    {
            // Record the size of the window prior to calling parents addNotify.
            Dimension size = getSize();
            super.addNotify();
            if (frameSizeAdjusted)  return;
            frameSizeAdjusted = true;
            // Adjust size of frame according to the insets
            Insets insets = getInsets();
            setSize(insets.left + insets.right + size.width, insets.top + insets.bottom + size.height);
    }
//------------------------------------------------------------------------------
    boolean frameSizeAdjusted = false;
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    VWCheckList LstPrincipal = new VWCheckList();
    VWLinkedButton BtnAdd = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnAdd_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    public Vector getValues()
    {
        Vector selPrincipals=new Vector();
        int count=LstPrincipal.getItemsCount();
        for(int i=0;i<count;i++)
            if(LstPrincipal.getItemIsCheck(i))
                selPrincipals.add(LstPrincipal.getItem(i));
        return selPrincipals;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private void loadPrincipals(Vector selectedPrincipals)
    {
        loadedList=false;
        Vector principalList=VWSecurityConnector.getPrincipals();
        LstPrincipal.loadData(principalList);
        loadedList=true;
        LstPrincipal.setCheckItems(selectedPrincipals);
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt("x", this.getX());
            prefs.putInt("y", this.getY());
        }
        else
        {
            prefs.putInt("x",50);
            prefs.putInt("y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
boolean loadedList=true;
}