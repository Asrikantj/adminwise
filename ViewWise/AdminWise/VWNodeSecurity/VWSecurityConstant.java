package ViewWise.AdminWise.VWNodeSecurity;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWRecord;

public interface VWSecurityConstant
{ 
    String LBL_NODEPRINCIPAL_NAME="	"+AdminWise.connectorManager.getString("VWSecurityConstant.LBL_NODEPRINCIPAL_NAME");
    String LBL_NODEPERMISSIONS_NAME="	"+AdminWise.connectorManager.getString("VWSecurityConstant.LBL_NODEPERMISSIONS_NAME");
    String TOOLTIP_PRINCIPALADD=AdminWise.connectorManager.getString("VWSecurityConstant.TOOLTIP_PRINCIPALADD");
    String TOOLTIP_PRINCIPALDEL=AdminWise.connectorManager.getString("VWSecurityConstant.TOOLTIP_PRINCIPALDEL");
    String TOOLTIP_PRINCIPALCLEAR=AdminWise.connectorManager.getString("VWSecurityConstant.TOOLTIP_PRINCIPALCLEAR");
    String TOOLTIP_PRINCIPALUP=AdminWise.connectorManager.getString("VWSecurityConstant.TOOLTIP_PRINCIPALUP");
    String TOOLTIP_PRINCIPALDOWN=AdminWise.connectorManager.getString("VWSecurityConstant.TOOLTIP_PRINCIPALDOWN");
    String LBL_ADDPRINCIPAL_NAME=AdminWise.connectorManager.getString("VWSecurityConstant.LBL_ADDPRINCIPAL_NAME");

    String[][] FolderPermission={{AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_0"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_1"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_2"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_3"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_4"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_5"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_6"),"0","0"},
    {"		"+AdminWise.connectorManager.getString("VWSecurityConstant.FolderPermission_7"),"0","0"}};
    int[][] FolderPermissionBehavior={{1,2,3,4,5,6,7},{1},{2},{3},{4},{5},{6},{7}};
    
    String[] PermissionColumnNames = {AdminWise.connectorManager.getString("VWSecurityConstant.PermissionColumnNames_0"),AdminWise.connectorManager.getString("VWSecurityConstant.VWSecurityConstant.PermissionColumnNames_1"),AdminWise.connectorManager.getString("VWSecurityConstant.VWSecurityConstant.PermissionColumnNames_2")};
        
}
