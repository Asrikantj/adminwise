
package ViewWise.AdminWise.VWNodeSecurity;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import java.awt.BorderLayout;
import java.awt.Container;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.border.EtchedBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWUtil.VWImageList;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWTree.VWTree;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import javax.swing.JList;
import java.awt.Component;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import javax.swing.JToolBar.Separator;
import ViewWise.AdminWise.VWUtil.VWRecord;
import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.Principal;
import com.computhink.common.Node;

public class VWSecurityPanel extends JPanel implements VWSecurityConstant,VWConstant
{
	public VWSecurityPanel()
	{
	    
	}
        public void setupUI() {
            if(UILoaded) return;
            setLayout(new BorderLayout());
            VWMainPanel.setLayout(null);
            add(VWMainPanel,BorderLayout.CENTER);
            VWMainPanel.setBounds(0,0,605,433);
            PanelCommand.setLayout(null);
            VWMainPanel.add(PanelCommand);
            PanelCommand.setBackground(java.awt.Color.white);
            PanelCommand.setBounds(0,0,168,432);
            Pic.setIconTextGap(0);
            Pic.setAutoscrolls(true);
            PanelCommand.add(Pic);
            Pic.setBounds(8,12,152,132);

            BtnSave.setText(BTN_SAVE_NAME);
            BtnSave.setActionCommand(BTN_SAVE_NAME);
            PanelCommand.add(BtnSave);
//            BtnSave.setBackground(java.awt.Color.white);
            BtnSave.setBounds(12,160,144, AdminWise.gBtnHeight);
            BtnCancel.setText(BTN_CANCEL_NAME);
            BtnCancel.setActionCommand(BTN_CANCEL_NAME);
            PanelCommand.add(BtnCancel);
//            BtnCancel.setBackground(java.awt.Color.white);
            BtnCancel.setBounds(12,195,144, AdminWise.gBtnHeight);

            PanelTable.setBounds(170,0,700,700);
            VWMainPanel.add(PanelTable);
            PanelTable.setLayout(new BorderLayout());
            PanelTable.add(vSpliter,BorderLayout.CENTER);
            PanelList.setLayout(new BorderLayout());
            PanelList.add(JLabel1,BorderLayout.NORTH);
            ////PanelList.add(SPIndicesTable,BorderLayout.CENTER);
            PanelList.add(toolBar,BorderLayout.EAST);
            toolBar.setFloatable(false);
            toolBar.setRollover(true);
            toolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
            toolBar.setSize(16,16);
            toolBar.add(BtnPrincipalAdd);
            BtnPrincipalAdd.setToolTipText(TOOLTIP_INDEXADD);
            BtnPrincipalAdd.setBackground(toolBar.getBackground());
            Separator[] separators=new Separator[5];
            for(int i=0;i<2;i++)
            {
                separators[i]=new Separator();
                separators[i].setOrientation(javax.swing.SwingConstants.VERTICAL);
            }
            toolBar.add(separators[0]); 
            BtnPrincipalDel.setToolTipText(TOOLTIP_PRINCIPALDEL);
            BtnPrincipalDel.setBackground(toolBar.getBackground());
            toolBar.add(BtnPrincipalDel);
            toolBar.add(separators[1]); 
            BtnPrincipalClear.setToolTipText(TOOLTIP_INDEXCLEAR);
            BtnPrincipalClear.setBackground(toolBar.getBackground());
            toolBar.add(BtnPrincipalClear);
            JLabel1.setText(LBL_NODEPRINCIPAL_NAME);
            JLabel2.setText(LBL_NODEPERMISSIONS_NAME);
            JLabel2.setLabelFor(SPPermissionTable);
            PanelProp.setLayout(new BorderLayout());
            PanelProp.add(JLabel2,BorderLayout.NORTH);
            PanelProp.add(SPPermissionTable,BorderLayout.CENTER);

            vSpliter.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
            vSpliter.setTopComponent(PanelList);
            vSpliter.setBottomComponent(PanelProp);

            PanelList.add(hSpliter,BorderLayout.CENTER);
            hSpliter.setOrientation(javax.swing.JSplitPane.HORIZONTAL_SPLIT);
            hSpliter.setLeftComponent(treePanel);
            hSpliter.setRightComponent(lstPrincipal);
            setEnableMode(MODE_UNCONNECT);
            SymComponent aSymComponent = new SymComponent();
            addComponentListener(aSymComponent);
            Pic.setIcon(VWImages.DocTypeImage);
            SymAction lSymAction = new SymAction();
            BtnSave.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            BtnPrincipalAdd.addActionListener(lSymAction);
            BtnPrincipalDel.addActionListener(lSymAction);
            BtnPrincipalClear.addActionListener(lSymAction);            
            PermissionTable.getParent().setBackground(java.awt.Color.white);
            vSpliter.setDividerLocation(0.6);
            vSpliter.doLayout();
            tree.addTreeSelectionListener(new VWTreeSelectionListener());
            lstPrincipal.addListSelectionListener(new VWListSelectionListener());
            repaint();
            doLayout();
            UILoaded=true;
        }
//------------------------------------------------------------------------------
    class VWTreeSelectionListener implements TreeSelectionListener
    {
        public void valueChanged(TreeSelectionEvent e) 
        {
            PermissionTable.resetData();
            lstPrincipal.removeAll();
            int selNodeId=tree.getSelectedNodeId();
            if(selNodeId==0)
                nodeAcl=null;
            else
                nodeAcl=VWSecurityConnector.getNodeAcl(selNodeId);
            List nodePrincipalList=new LinkedList();
            if(nodeAcl != null && nodeAcl.getEntries()!=null)
            {
                for(int i=0;i<nodeAcl.getEntries().size();i++)
                    ///if(((AclEntry)nodeAcl.getEntries().get(i)).getNodeId()==selNodeId)
                nodePrincipalList.add(nodeAcl.getEntries().get(i));
            }
            if(nodeAcl == null || nodeAcl.getEntries()==null || nodePrincipalList.size()==0)
            {
                nodePrincipalList.add("No Principal");
            }
            lstPrincipal.loadData(nodePrincipalList);
            selectedPrincipal=-1;
            setEnableMode (MODE_SELECT);
        }
    }
//------------------------------------------------------------------------------
    class VWListSelectionListener implements javax.swing.event.ListSelectionListener
    {
        public void valueChanged(javax.swing.event.ListSelectionEvent e)
        {
            if(selectedPrincipal>=0) savePrincipalPermissions();
            loadPrincipalPermissions();
            selectedPrincipal=lstPrincipal.getSelectedIndex();
        }
    }
//------------------------------------------------------------------------------
    private void loadPrincipalPermissions()
    {
        stopGridEdit();
        PermissionTable.resetData();
        if(lstPrincipal.getSelectedValue() != null &&
            ! (lstPrincipal.getSelectedValue() instanceof String))
        {
            ////String permissions=((AclEntry)lstPrincipal.getSelectedValue()).getPermissionsString();
            String permissions="";
            if(permissions.equals("")) return;
            
            /*
             System.out.println("getNodeAcl: "+ vwc.getNodeAcl(session, nodeAcl));
            //System.out.println("getDocAcl: "+ vwc.getDocAcl(session, docAcl));
            Iterator iterator = nodeAcl.getEntries().iterator();
            while (iterator.hasNext())
            {
                AclEntry entry = (AclEntry) iterator.next();
                System.out.print("Principal:" + entry.getPrincipal().getId());
                System.out.print("...Node:" + entry.getNodeId());
                System.out.println("...Permisions:" + entry.getPermissionsString());
                System.out.print("...View:" + entry.isViewPermission());
                System.out.print("...Share:" + entry.isSharePermission());
                System.out.print("...Modify:" + entry.isModifyPermission());
                System.out.print("...Delete:" + entry.isDeletePermission());
                System.out.print("...Admin:" + entry.isAdminPermission());
                System.out.println("...FullAccess:" + entry.isFullAccessPermission());
            }
            */
            String[][] nodePermissions = FolderPermission;
            for(int i=0;i<nodePermissions.length;i++)
                nodePermissions[i][1]=permissions.substring(i,i+1);
            PermissionTable.addData(nodePermissions);
        }
    }
//------------------------------------------------------------------------------
    private void savePrincipalPermissions()
    {
        stopGridEdit();
        if(lstPrincipal.getSelectedValue() != null &&
            ! (lstPrincipal.getSelectedValue() instanceof String))
        {
            String[][] data=PermissionTable.getData();
            String strPermission="";
            ///for(int i=0;i<data.length;i++) strPermission+=data[i][1];
                ////((AclEntry)lstPrincipal.getSelectedValue()).setPermissionsString(strPermission);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnSave)
                BtnSave_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
            else if (object == BtnPrincipalAdd)
                BtnPrincipalAdd_actionPerformed(event);
            else if (object == BtnPrincipalDel)
                BtnPrincipalDel_actionPerformed(event);
            else if (object == BtnPrincipalClear)
                BtnPrincipalClear_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
        public javax.swing.JSplitPane vSpliter = new javax.swing.JSplitPane();
        public javax.swing.JSplitPane hSpliter = new javax.swing.JSplitPane();
	javax.swing.JPanel VWMainPanel = new javax.swing.JPanel();
	javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
        javax.swing.JPanel PanelList = new javax.swing.JPanel();
        javax.swing.JPanel PanelTable = new javax.swing.JPanel();
        javax.swing.JPanel PanelProp = new javax.swing.JPanel();
        javax.swing.JToolBar toolBar = new javax.swing.JToolBar();                                                                                       
	javax.swing.JLabel Pic = new javax.swing.JLabel();
	VWLinkedButton BtnSave = new VWLinkedButton(1);
	VWLinkedButton BtnCancel = new VWLinkedButton(1);
        VWLinkedButton BtnPrincipalAdd = new VWLinkedButton(VWImages.AddIcon,false);
        VWLinkedButton BtnPrincipalDel = new VWLinkedButton(VWImages.DelIcon,false);
        VWLinkedButton BtnPrincipalClear = new VWLinkedButton(VWImages.ClearIcon,false);
        public static VWImageList lstPrincipal = new VWImageList(0);
        public VWTree tree = new VWTree();
        javax.swing.JScrollPane treePanel = new javax.swing.JScrollPane(tree);
        public static VWPermissionTable PermissionTable = new VWPermissionTable();
        javax.swing.JScrollPane SPPermissionTable = new javax.swing.JScrollPane(PermissionTable);
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
        EtchedBorder etchedBorder = new EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
//------------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter
    {
        public void componentResized(java.awt.event.ComponentEvent event)
        {
            Object object = event.getSource();
            if (object instanceof VWSecurityPanel)
                VWMainPanel_componentResized(event);
        }
    }
//------------------------------------------------------------------------------
    void VWMainPanel_componentResized(java.awt.event.ComponentEvent event)
    {    
        if(event.getSource() instanceof VWSecurityPanel)
        {
           Dimension mainDimension=this.getSize();
           Dimension PanelDimension=mainDimension;
           Dimension commandDimension=PanelCommand.getSize();
           commandDimension.height=mainDimension.height;
           PanelDimension.width=mainDimension.width-commandDimension.width;
           PanelDimension.height=mainDimension.height;
           PanelCommand.setSize(commandDimension);
           PanelTable.setSize(PanelDimension);
           AdminWise.adminPanel.MainTab.repaint(); 
           vSpliter.setDividerLocation(0.5);
           vSpliter.doLayout();
           hSpliter.setDividerLocation(0.5);
           hSpliter.doLayout();
        }
    }
//------------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom)
    {
        stopGridEdit();
        PermissionTable.addData(FolderPermission);
        if(newRoom.getConnectStatus()!=Room_Status_Connect)
        {
            setEnableMode(MODE_UNCONNECT);
            return;
        }
        if(newRoom.getId()==gCurRoom)   return;
        VWTreeConnector.setTree(tree);
        tree.loadTreeData();
        gCurRoom=newRoom.getId();
        lstPrincipal.removeAll();
        setEnableMode(MODE_UNSELECTED);
    }
//------------------------------------------------------------------------------
    public void unloadTabData()
    {
        stopGridEdit();
        PermissionTable.clearData();
        lstPrincipal.removeAll();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
//------------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(lstPrincipal.getItemsCount()==0 || 
        lstPrincipal.getModel().getElementAt(0) instanceof String)
            return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            savePrincipalPermissions();
            ///System.out.println("nodeAcl->"+nodeAcl.getNodeId());
            Acl newNodeAcl=new Acl(new Node(nodeAcl.getNodeId()));
            ///System.out.println(lstPrincipal.getItemsCount());
            for(int i=0;i<lstPrincipal.getItemsCount();i++)
                newNodeAcl.addEntry((AclEntry)lstPrincipal.getModel().getElementAt(i));
            VWSecurityConnector.setNodeAcl(newNodeAcl);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        loadPrincipalPermissions(); 
    }
//------------------------------------------------------------------------------
    void BtnPrincipalAdd_actionPerformed(java.awt.event.ActionEvent event)
    {
        stopGridEdit();
        Vector selPrincipals=VWSecurityConnector.displayAddPrincipalDlg
            (lstPrincipal.getItems());
        if(selPrincipals!=null && selPrincipals.size()>0)
        {
            int nodeId=tree.getSelectedNodeId();
            List selEntries=new LinkedList();
            for(int i=0;i<selPrincipals.size();i++)
            {
                ///selEntries.add(new AclEntry(new Acl(new Node(nodeId)),(Principal)selPrincipals.get(i)));
                Acl acl=new Acl(new Node(nodeId));
                selEntries.add(acl.newEntry((Principal)selPrincipals.get(i)));
            }
            lstPrincipal.loadData(selEntries);
        }
        setEnableMode(MODE_SELECT);
    }
//------------------------------------------------------------------------------
    void BtnPrincipalDel_actionPerformed(java.awt.event.ActionEvent event)
    {
        /*
        if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    "Delete document type (" + selDocType.getName() +")?")!=javax.swing.JOptionPane.YES_OPTION) return;
        */
        int selectedIndex=lstPrincipal.getSelectedIndex();
        if(selectedIndex>=0)
        {
            lstPrincipal.remove(selectedIndex);
            lstPrincipal.repaint();
        }
    }
//------------------------------------------------------------------------------
    void BtnPrincipalClear_actionPerformed(java.awt.event.ActionEvent event)
    {
        lstPrincipal.removeAll();
        List nodePrincipalList=new LinkedList();
        nodePrincipalList.add("No Principal");
        lstPrincipal.loadData(nodePrincipalList);
        setEnableMode(MODE_SELECT);
    }
//------------------------------------------------------------------------------
    private void setEnableMode(int mode)
    {
        setEnableMode(mode,false);
    }
//------------------------------------------------------------------------------
    private void setEnableMode(int mode,boolean force)
    {   
        ///if(!force && curMode==mode) return;
        curMode=mode;        
        BtnSave.setEnabled(true);
        BtnCancel.setEnabled(true);
        BtnPrincipalAdd.setEnabled(true);
        BtnPrincipalDel.setEnabled(true);
        BtnPrincipalClear.setEnabled(true);
        switch (mode)
        {
            case MODE_SELECT:
                if(lstPrincipal.getItemsCount()==0)
                {
                    BtnPrincipalDel.setEnabled(false);
                    BtnPrincipalClear.setEnabled(false);
                }
                break;
            case MODE_UNSELECTED:
                BtnSave.setEnabled(false);
                BtnCancel.setEnabled(false);
                BtnPrincipalAdd.setEnabled(false);
                BtnPrincipalDel.setEnabled(false);
                BtnPrincipalClear.setEnabled(false);
                break;
            case MODE_UPDATE:                
                break;
            case MODE_UNCONNECT:
                BtnSave.setEnabled(false);
                BtnCancel.setEnabled(false);
                BtnPrincipalAdd.setEnabled(false);
                BtnPrincipalDel.setEnabled(false);
                BtnPrincipalClear.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
//------------------------------------------------------------------------------
    private void stopGridEdit()
    {
        try{
            PermissionTable.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
    }
//------------------------------------------------------------------------------
    private static int gCurRoom=0;
    private static int curMode=-1;
    private List deletedIndices=new LinkedList();
    private Acl nodeAcl=null;
    private int selectedPrincipal=-1;
    private boolean UILoaded=false;
//------------------------------------------------------------------------------
}