/*
                      Copyright (c) 1997-2001
                     Computhink Software

                      All rights reserved.
*/
/**
 * VWSecurityConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWNodeSecurity;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.util.List;
import java.util.Vector;
import ViewWise.AdminWise.VWUtil.VWRecord;
import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.Principal;
import com.computhink.common.Node;


public class VWSecurityConnector implements VWConstant
{
    public static Vector displayAddPrincipalDlg(Vector exceptPrincipals)
    {
        VWDlgAddPrincipals addPrincipalDlg = new VWDlgAddPrincipals(AdminWise.adminFrame,
            exceptPrincipals);
        addPrincipalDlg.requestFocus();
        if(addPrincipalDlg.getCancelFlag()) return null;
        Vector retValues=addPrincipalDlg.getValues();
        addPrincipalDlg.dispose();
        return retValues;
    }
//------------------------------------------------------------------------------
    public static Vector getPrincipals()
    {   
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector principals=new Vector();
        AdminWise.gConnector.ViewWiseClient.getPrincipals(room.getId(),principals);
        ///System.out.println(principals.size());
        return principals;
    }
//------------------------------------------------------------------------------
    public static Acl getNodeAcl(int nodeId)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Acl nodeAcl=new Acl(new Node(nodeId));
        AdminWise.gConnector.ViewWiseClient.getNodeAcl(room.getId(),nodeAcl,false);
        return nodeAcl;
    }
//------------------------------------------------------------------------------
    public static void setNodeAcl(Acl nodeAcl)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.setNodeAcl(room.getId(),nodeAcl);
    }
//------------------------------------------------------------------------------
}