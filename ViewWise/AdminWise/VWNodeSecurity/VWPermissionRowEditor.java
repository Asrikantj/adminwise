/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWSimpleSearchRowEditor<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWNodeSecurity;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWMask.VWMaskField;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWUtil.VWRecord;
import com.computhink.common.Principal;

public class VWPermissionRowEditor implements TableCellEditor,VWConstant{
  
  protected TableCellEditor editor,defaultEditor;
  private VWPermissionTable gTable=null;
//------------------------------------------------------------------------------
  public VWPermissionRowEditor(VWPermissionTable table){
    JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;    
  }
//------------------------------------------------------------------------------
  public Component getTableCellEditorComponent(JTable table,
      Object value,boolean isSelected,int row,int column)
  {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
        return editor.getTableCellEditorComponent(table,value,isSelected,row,column);
  }
//------------------------------------------------------------------------------
  public Object getCellEditorValue(){
      return editor.getCellEditorValue();
  }
//------------------------------------------------------------------------------
  public boolean stopCellEditing(){
    return editor.stopCellEditing();
  }
//------------------------------------------------------------------------------
  public void cancelCellEditing()
  {
    editor.cancelCellEditing();
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(EventObject anEvent){
    
    int column=0,row=0;
    if (anEvent instanceof MouseEvent)
    {
	Point point=((MouseEvent)anEvent).getPoint();
        column = gTable.columnAtPoint(point);
        row = gTable.rowAtPoint(point);
    }
    if(AdminWise.adminPanel.securityPanel.lstPrincipal.getSelectedValue()==null ||
        (AdminWise.adminPanel.securityPanel.lstPrincipal.getSelectedValue()) instanceof String) return false;
    if(column==0 || column==2) return false;
    return true ;
  }
//------------------------------------------------------------------------------
  public void addCellEditorListener(CellEditorListener l){
    editor.addCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public void removeCellEditorListener(CellEditorListener l){
    editor.removeCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public boolean shouldSelectCell(EventObject anEvent){
    return editor.shouldSelectCell(anEvent);
  }
//------------------------------------------------------------------------------
}