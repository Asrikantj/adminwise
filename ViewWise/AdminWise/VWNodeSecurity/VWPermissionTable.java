/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWNodeSecurity;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.ListSelectionModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import ViewWise.AdminWise.VWTable.*;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;

//------------------------------------------------------------------------------
public class VWPermissionTable extends JTable implements VWConstant,VWSecurityConstant
{
    protected VWPermissionTableData m_data;    
    private int indexType=-1;    
    public VWPermissionTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWPermissionTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWPermissionTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        ColoredTableCellRenderer textRenderer=new ColoredTableCellRenderer();
        ///DefaultTableCellRenderer textRenderer =new DefaultTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWPermissionTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWPermissionRowEditor editor=new VWPermissionRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWPermissionTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                {
                }       
                else
                {
                }
            }
        });
        setRowHeight(TableRowHeight);
        
        setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
        Object object = event.getSource();
        if (object instanceof JTable)
            if(event.getModifiers()==event.BUTTON3_MASK)
            VWPermissionTable_RightMouseClicked(event);
            else if(event.getModifiers()==event.BUTTON1_MASK)
                VWPermissionTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
    void VWPermissionTable_LeftMouseClicked(java.awt.event.MouseEvent event)
    {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
           return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
//------------------------------------------------------------------------------
    void VWPermissionTable_RightMouseClicked(java.awt.event.MouseEvent event)
    {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
           return; // no cell found
        if(event.getClickCount() == 1)
                rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
                rDoubleClick(event,row,column);
    }
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public String[] getValues()
  {
        return m_data.getValues();
  }
  //------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
        m_data.insert(new PermissionRowData(data[i]));
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public String[] getRowData(int rowNum)
    {
        return m_data.getRowData(rowNum);
    }
//------------------------------------------------------------------------------
    public int getRowPermissionValue(int rowNum)
    {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[1]);
    }
//------------------------------------------------------------------------------
    public void updateRowData(int rowNum,String[] data)
    {
        m_data.updateRowData(rowNum,data);
        m_data.fireTableDataChanged();
    }
//------------------------------------------------------------------------------
    public void updateRowValue(int rowNum,String value)
    {
        m_data.updateRowValue(rowNum,value);
        m_data.fireTableDataChanged();
    }
//------------------------------------------------------------------------------
    public void clearData()
    {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
//------------------------------------------------------------------------------
    public void resetData()
    {
        m_data.resetData();
        m_data.fireTableDataChanged();
    }
//------------------------------------------------------------------------------
    public int getIndexType()
    {
        return indexType;
    }
//------------------------------------------------------------------------------
    public void setIndexType(int type)
    {
        indexType=type;
    }
//------------------------------------------------------------------------------
    public void ResetDefaultValue(int row)
    {
        m_data.ResetLastRow(row);
        m_data.fireTableDataChanged();
    }
}
//------------------------------------------------------------------------------
class PermissionRowData
{
  public String   m_PermissionName;
  public String   m_PermissionValue;
  public String   m_PermissionEffValue;
  ///public boolean   m_PermissionEffValue;
  
  public PermissionRowData() {
    m_PermissionName = "";
    m_PermissionValue = "";
    m_PermissionEffValue="";
  }
//------------------------------------------------------------------------------
  public PermissionRowData(String PermissionName,String PermissionValue,
                        String PermissionEffValue) 
  {
    m_PermissionName = PermissionName;
    m_PermissionValue = PermissionValue;
    m_PermissionEffValue=PermissionEffValue;
  }
//------------------------------------------------------------------------------
  public PermissionRowData(String str)
  {
    VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
    try
    {
        m_PermissionName = tokens.nextToken();
    }
    catch(Exception e){m_PermissionName=AdminWise.connectorManager.getString("VWPermissionTable.PermissionRowData_0");}
    try
    {
        m_PermissionValue = tokens.nextToken();
    }
    catch(Exception e){m_PermissionValue=AdminWise.connectorManager.getString("VWPermissionTable.PermissionRowData_1");}
    try
    {
        m_PermissionEffValue = tokens.nextToken();
    }
    catch(Exception e){m_PermissionEffValue=AdminWise.connectorManager.getString("VWPermissionTable.PermissionRowData_1");}
  }
//------------------------------------------------------------------------------
  public PermissionRowData(String[] data) 
  {
    int i=0;
    m_PermissionName = data[i++];
    m_PermissionValue = data[i++];
    m_PermissionEffValue = data[i++];
  }
}
//------------------------------------------------------------------------------
class PermissionColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public PermissionColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWPermissionTableData extends AbstractTableModel 
{
  public static final PermissionColumnData m_columns[] = {
    new PermissionColumnData( VWSecurityConstant.PermissionColumnNames[0],0.4F,JLabel.LEFT ),
    new PermissionColumnData( VWSecurityConstant.PermissionColumnNames[1],0.3F, JLabel.LEFT),
    new PermissionColumnData( VWSecurityConstant.PermissionColumnNames[2],0.3F, JLabel.LEFT),
  };
  public static final int COL_PERNAME = 0;
  public static final int COL_PERSTATUS = 1;
  public static final int COL_PEREFFSTATUS = 2;

  protected VWPermissionTable m_parent;
  protected Vector m_vector;

  public VWPermissionTableData(VWPermissionTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(new PermissionRowData((String)data.get(i)));
    }
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        PermissionRowData row =new PermissionRowData(data[i]); 
        m_vector.addElement(row);
    }
  }
//------------------------------------------------------------------------------
public void resetData() {
    int count=getRowCount();
    String[][] data=new String[count][3];
    for(int i=0;i<count;i++)
    {
        PermissionRowData row=(PermissionRowData)m_vector.elementAt(i);
        row.m_PermissionValue="0";
        row.m_PermissionEffValue="0";
    }
  }
//------------------------------------------------------------------------------
public void setValue(int row,int col,String value) 
{
        PermissionRowData rowData=(PermissionRowData)m_vector.elementAt(row);
        if(col==1)
            rowData.m_PermissionValue=value;
        else if(col==2)
            rowData.m_PermissionEffValue=value;
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][3];
    for(int i=0;i<count;i++)
    {
        PermissionRowData row=(PermissionRowData)m_vector.elementAt(i);
        data[i][0]=row.m_PermissionName;
        data[i][1]=row.m_PermissionValue;
        data[i][2]=row.m_PermissionEffValue;
    }
    return data;
  }
//------------------------------------------------------------------------------
public String[] getValues() {
    int count=getRowCount();
    String[] data=new String[count];
    for(int i=0;i<count;i++)
    {
        PermissionRowData row=(PermissionRowData)m_vector.elementAt(i);
        data[i]=row.m_PermissionValue;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] PermissionRowData=new String[3];
    PermissionRowData row=(PermissionRowData)m_vector.elementAt(rowNum);
    PermissionRowData[0]=row.m_PermissionName;
    PermissionRowData[1]=row.m_PermissionValue;
    PermissionRowData[2]=row.m_PermissionEffValue;
    return PermissionRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())    return "";
    PermissionRowData row = (PermissionRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_PERNAME: return row.m_PermissionName;
      case COL_PERSTATUS: return (row.m_PermissionValue.equals("1")?"Yes":"No");
      case COL_PEREFFSTATUS: return (row.m_PermissionEffValue.equals("1")?"Yes":"No");
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    PermissionRowData row = (PermissionRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_PERNAME:
        row.m_PermissionName=svalue;
        break;
      case COL_PERSTATUS:
         row.m_PermissionValue=(svalue.equals("Yes")?"1":"0");
         doBehavior(nRow,nCol,row.m_PermissionValue);
        break;
      case COL_PEREFFSTATUS:
         row.m_PermissionEffValue=(svalue.equals("Yes")?"1":"0");
         doBehavior(nRow,nCol,row.m_PermissionEffValue);
        break;
    }
  }
//------------------------------------------------------------------------------
    private void doBehavior(int nRow,int nCol,String value)
    {
        int relationCount=VWSecurityConstant.FolderPermissionBehavior[nRow].length;
        if(relationCount<=1) return;
        for(int i=0;i<relationCount;i++)
        {
            int selRow=VWSecurityConstant.FolderPermissionBehavior[nRow][i];
            if(nRow==selRow) return;
            if(nCol==COL_PERSTATUS)
                ((PermissionRowData)m_vector.elementAt(selRow)).m_PermissionValue=value;
            else if(nCol==COL_PEREFFSTATUS)
                ((PermissionRowData)m_vector.elementAt(selRow)).m_PermissionEffValue=value;
         }
        fireTableDataChanged();
    }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void ResetLastRow(int curRow)
    {
        int index=m_vector.size()-3;
        if(curRow>=index) return;
        PermissionRowData row = (PermissionRowData)m_vector.elementAt(index+2);
        row.m_PermissionValue="";
    }
//------------------------------------------------------------------------------
    public void insert(PermissionRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
    m_vector.remove(row);
    return true;
  }
//------------------------------------------------------------------------------
    public void updateRowData(int rowNum,String[] data) 
    {
        PermissionRowData row=(PermissionRowData)m_vector.elementAt(rowNum);
        row.m_PermissionName=data[0];
        row.m_PermissionValue=data[1];
        row.m_PermissionEffValue=data[2];
  }
//------------------------------------------------------------------------------
    public void updateRowValue(int rowNum,String value) 
    {
        PermissionRowData row=(PermissionRowData)m_vector.elementAt(rowNum);
        row.m_PermissionValue=value;
  }
//------------------------------------------------------------------------------
}