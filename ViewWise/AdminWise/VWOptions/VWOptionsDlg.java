package ViewWise.AdminWise.VWOptions;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import  java.util.Vector;
import java.awt.Frame;
import java.util.prefs.*;
import ViewWise.AdminWise.VWUtil.VWPrint;

//--------------------------------------------------------------------------
public class VWOptionsDlg extends javax.swing.JDialog implements  VWConstant {
    public VWOptionsDlg(Frame parent) {
        super(parent,LBL_OPTIONS_NAME,true);
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.AdminWiseIcon.getImage());
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(VWOptions,BorderLayout.CENTER);
        setVisible(false);
        VWOptions.setLayout(null);
        VWOptions.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWOptions.add(PanelCommand);
        PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,12,152,132);
        BtnSave.setText(BTN_SAVE_NAME);
        BtnSave.setActionCommand(BTN_SAVE_NAME);
        PanelCommand.add(BtnSave);
//        BtnSave.setBackground(java.awt.Color.white);
        BtnSave.setBounds(12,160,144, AdminWise.gBtnHeight);
        
        BtnPrint.setText(BTN_PRINT_NAME);
        BtnPrint.setActionCommand(BTN_PRINT_NAME);
        PanelCommand.add(BtnPrint);
//        BtnPrint.setBackground(java.awt.Color.white);
        BtnSave.setBounds(12,195,144, AdminWise.gBtnHeight);
        BtnSave.setIcon(VWImages.SaveIcon);
        
        BtnClose.setText(BTN_CLOSE_NAME);
        BtnClose.setActionCommand(BTN_CLOSE_NAME);
        PanelCommand.add(BtnClose);
//        BtnClose.setBackground(java.awt.Color.white);
        BtnClose.setBounds(12,230,144, AdminWise.gBtnHeight);
        BtnClose.setIcon(VWImages.CloseIcon);
        
        PanelTable.setLayout(new BorderLayout());
        VWOptions.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        SymComponent aSymComponent = new SymComponent();
        VWOptions.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.OptionsImage);
        ///Table.addData(OptionsData);
        SymAction lSymAction = new SymAction();
        BtnSave.addActionListener(lSymAction);
        BtnClose.addActionListener(lSymAction);
        BtnPrint.addActionListener(lSymAction);
        Table.addData(getViewWiseOptions());
        SymKey aSymKey = new SymKey();
        BtnSave.addKeyListener(aSymKey);
        BtnClose.addKeyListener(aSymKey);
        Table.addKeyListener(aSymKey);
        getDlgOptions();
        setResizable(false);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        setSize(605,480);
        setVisible(true);
    }
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(java.awt.event.WindowEvent event) {
            Object object = event.getSource();
            if (object == VWOptionsDlg.this)
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnSave_actionPerformed(null);
            else if (event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnSave)
                BtnSave_actionPerformed(event);
            else if (object == BtnClose)
                BtnClose_actionPerformed(event);
            else if (object == BtnClose)
                BtnPrint_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWOptions = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
    VWLinkedButton BtnPrint = new VWLinkedButton(0,true);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWOptionsTable Table = new VWOptionsTable();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWOptions)
                VWOptions_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWOptions_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWOptions) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        saveViewWiseOptions();
        saveDlgOptions();
        setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event) {
        saveDlgOptions();
        setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event) {
        new VWPrint(Table);
    }
    //--------------------------------------------------------------------------
    private void saveDlgOptions() {
        if(!AdminWise.adminPanel.saveDialogPos) return;
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
        prefs.putInt("width", this.getSize().width);
        prefs.putInt("height", this.getSize().height);
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage( getClass() );
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        ///setSize(prefs.getInt("width",600),prefs.getInt("height",400));
        setSize(600,455);
    }
    //--------------------------------------------------------------------------
    private void saveViewWiseOptions() {
        AdminWise.adminPanel.saveViewWiseOptions(Table.getData());
    }
    //--------------------------------------------------------------------------
    private String[][] getViewWiseOptions() {
        return AdminWise.adminPanel.getViewWiseOptions();
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
}