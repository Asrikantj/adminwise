/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWStorageConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWStorage;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.io.*;
import ViewWise.AdminWise.VWUtil.VWRecord;
import com.computhink.common.ServerSchema;
import com.computhink.common.Document;
import com.computhink.common.Node;
import com.computhink.vwc.Session;

public class VWStorageConnector implements VWConstant {
    public static Vector getStorages() {
        Vector dssList = new Vector();
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            AdminWise.gConnector.ViewWiseClient.getDSSList(room.getId(), dssList);
        return dssList;
    }
    //--------------------------------------------------------------------------
    public static int getFolderStorage(int folderId) {
        ServerSchema dssSchema = new ServerSchema();
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null && room.getConnectStatus()!=Room_Status_Connect) return 0;
        AdminWise.gConnector.ViewWiseClient.getDSSforDoc(room.getId(),
        new Document(folderId),dssSchema);
        return dssSchema.getId();
    }
    //--------------------------------------------------------------------------
    public static void setFolderStorage(int nodeId, int storageId, int withInherit) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null && room.getConnectStatus()!=Room_Status_Connect) return ;
        Node node=new Node(nodeId);
        if(getFolderStorage(nodeId)!=storageId)
        {
            node.setStorageId(storageId);
            AdminWise.gConnector.ViewWiseClient.setNodeStorage(room.getId(),node,
            withInherit);
        }
    }
    //--------------------------------------------------------------------------
    public static void updateStorageInfo(ServerSchema storage) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null && room.getConnectStatus()!=Room_Status_Connect) return ;
        AdminWise.gConnector.ViewWiseClient.updateStorageInfo(room.getId(),storage);
    }
    public static boolean isDSSLocationExists(ServerSchema storage, String location){
	storage.setType("Document Storage Server");
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null && room.getConnectStatus()!=Room_Status_Connect) return false;
        if (AdminWise.gConnector.ViewWiseClient.isDSSLocationExists(room.getId(), storage, location) == NO_ERROR){
            return true;
        }
        return false;
	
    }
    public static int addNotificationHistory(String message, int notifyId){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Session session = AdminWise.gConnector.ViewWiseClient.getSession(room.getId());
    	try{
    		AdminWise.gConnector.ViewWiseClient.addNotificationHistory(room.getId(), 0, VWConstant.nodeType_Storage, notifyId, message);
    	}catch(Exception ex){
    		return -1;
    	}
    	return 0;
    }
}