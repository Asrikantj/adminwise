/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWStorage;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.border.TitledBorder;
import java.awt.Insets;
import javax.swing.BoxLayout;
import ViewWise.AdminWise.VWTree.VWTree;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import ViewWise.AdminWise.VWTree.VWTreeNode;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;
import java.util.prefs.*;
import javax.swing.tree.TreePath;
import ViewWise.AdminWise.VWRoom.VWRoom;
import java.util.Date;
import com.computhink.common.ServerSchema;
import com.computhink.vwc.VWClient;

public class VWDlgStorageLocation extends javax.swing.JDialog 
    implements VWConstant
{
	public VWDlgStorageLocation(Frame parent)
	{
            super(parent,LBL_STORAGELOCATION,false);
            ///parent.setIconImage(VWImages.StorageIcon.getImage());
            getContentPane().setBackground(AdminWise.getAWColor());
            getContentPane().setLayout(null);
            treePanel.setBounds(6,6,342,342);
            treePanel.setBorder(titledBorder2);
            treePanel.setBackground(AdminWise.getAWColor());
            getContentPane().add(treePanel);
            ChkEnableInherit.setText(LBL_ENABLEINHRT_NAME);
            getContentPane().add(ChkEnableInherit);
            ChkEnableInherit.setBounds(9,346,300,20);
            ChkEnableInherit.setSelected(true);
            ChkEnableInherit.setBackground(AdminWise.getAWColor());
            storagePanel.setBorder(titledBorder1);
            storagePanel.setLayout(null);
            storagePanel.setBackground(AdminWise.getAWColor());
            getContentPane().add(storagePanel);
            JLabel2.setText(LBL_STORAGE_NAME);
            JLabel2.setLabelFor(CmbName);
            storagePanel.add(JLabel2);
            JLabel2.setBounds(12,24,60,24);
            storagePanel.add(CmbName);
            CmbName.setBounds(60,24,240,24);
            BtnApply.setText(BTN_APPLY_NAME);
            BtnApply.setActionCommand(BTN_APPLY_NAME);
            //BtnApply.setBackground(java.awt.Color.white);
            BtnApply.setIcon(VWImages.OkIcon);
            getContentPane().add(BtnApply);
            BtnRefresh.setText(BTN_REFRESH_NAME);
            BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
            //BtnRefresh.setBackground(java.awt.Color.white);
            getContentPane().add(BtnRefresh);
            BtnRefresh.setIcon(VWImages.RefreshIcon);
            BtnClose.setText(BTN_CLOSE_NAME);
            BtnClose.setActionCommand(BTN_CLOSE_NAME);
            //BtnClose.setBackground(java.awt.Color.white);
            getContentPane().add(BtnClose);
            BtnClose.setIcon(VWImages.CloseIcon);
            setCollapsePos();
            BtnShow.setText(BTN_EXPAND_NAME);
            BtnShow.setActionCommand(BTN_EXPAND_NAME);
            storagePanel.add(BtnShow);
            BtnShow.setBounds(302,24,24, AdminWise.gBtnHeight);
            //BtnShow.setBackground(java.awt.Color.white);
        
            BtnSave.setText(BTN_SAVE_NAME);
            BtnSave.setActionCommand(BTN_SAVE_NAME);
            //BtnSave.setBackground(java.awt.Color.white);
            storagePanel.add(BtnSave);
            BtnSave.setBounds(254,146,72, AdminWise.gBtnHeight);
    		
            LblServerPath.setText(LBL_SERVER_NAME);
            LblServerPath.setLabelFor(txtServer);
            storagePanel.add(LblServerPath);
            LblServerPath.setBounds(12,54,60,24);
            storagePanel.add(txtServer);
            txtServer.setBounds(60,54,266,24);
            
            LblPortPath.setText(LBL_PORT_NAME);
            LblPortPath.setLabelFor(txtPort);
            storagePanel.add(LblPortPath);
            LblPortPath.setBounds(12,84,60,24);
            storagePanel.add(txtPort);
            txtPort.setBounds(60,84,266,24);
            
            LblLocationPath.setText(LBL_STORAGE_LOCATION_NAME);
            LblLocationPath.setLabelFor(txtServerLocation);
            storagePanel.add(LblLocationPath);
            LblLocationPath.setBounds(12,114,60,24);
            storagePanel.add(txtServerLocation);
            txtServerLocation.setBounds(60,114,230,24);
            
            BtnTestConnect.setText(AdminWise.connectorManager.getString("VWDlgStorageLocation.BtnTestConnect_1"));
            storagePanel.add(BtnTestConnect);
            BtnTestConnect.setBounds(292,114,34, AdminWise.gBtnHeight);
            BtnTestConnect.setToolTipText(AdminWise.connectorManager.getString("VWDlgStorageLocation.BtnTestConnect_2"));
            setHostedMode();
            
            
            SymAction lSymAction = new SymAction();
            BtnRefresh.addActionListener(lSymAction);
            BtnApply.addActionListener(lSymAction);
            BtnClose.addActionListener(lSymAction);
            CmbName.addActionListener(lSymAction);
            BtnShow.addActionListener(lSymAction);
            BtnSave.addActionListener(lSymAction);
            BtnTestConnect.addActionListener(lSymAction);
            setResizable(false);
            tree.addTreeSelectionListener(new VWTreeSelectionListener());
            VWTreeConnector.setTree(tree);
            SymKey aSymKey = new SymKey();
            CmbName.addKeyListener(aSymKey);
            tree.addKeyListener(aSymKey);
            BtnApply.addKeyListener(aSymKey);
            BtnClose.addKeyListener(aSymKey);
            getDlgOptions();
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            setVisible(true);
	}
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgStorageLocation.this)
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class VWTreeSelectionListener implements TreeSelectionListener
    {
        public void valueChanged(TreeSelectionEvent e) 
        {
            if(treeLoaded) setFolderStorage();
        }         
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnApply_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnApply)
                BtnApply_actionPerformed(event);
            else if (object == BtnRefresh)
                BtnRefresh_actionPerformed(event);
            else if (object == BtnClose)
                BtnClose_actionPerformed(event);
            else if (object == CmbName)
                CmbName_actionPerformed(event);
            else if (object == BtnShow)
                BtnShow_actionPerformed(event);
            else if (object == BtnSave)
                BtnSave_actionPerformed(event);
            else if (object == BtnTestConnect)
                BtnTestConnect_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
	javax.swing.JPanel storagePanel = new javax.swing.JPanel();
        VWTree tree = new VWTree();
	javax.swing.JScrollPane treePanel = new javax.swing.JScrollPane(tree);
	javax.swing.JComboBox CmbName = new javax.swing.JComboBox();
	javax.swing.JLabel LblServerPath = new javax.swing.JLabel();
        javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
        javax.swing.JLabel LblPortPath = new javax.swing.JLabel();
        javax.swing.JLabel LblLocationPath = new javax.swing.JLabel();
        javax.swing.JCheckBox ChkEnableInherit = new javax.swing.JCheckBox();
	TitledBorder titledBorder1 = new TitledBorder(LBL_STORAGESERVER_NAME);
        TitledBorder titledBorder2 = new TitledBorder(LBL_EXPLORER_NAME);
	VWLinkedButton BtnApply =  new VWLinkedButton(0,true);
	VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
	VWLinkedButton BtnClose = new VWLinkedButton(0,true);
        VWLinkedButton BtnShow = new VWLinkedButton(0,true);
        VWLinkedButton BtnSave = new VWLinkedButton(0,true);
        VWLinkedButton BtnTestConnect = new VWLinkedButton(0,true);
        javax.swing.JTextField txtServer = new javax.swing.JTextField();
        javax.swing.JTextField txtPort = new javax.swing.JTextField();
        javax.swing.JTextField txtServerLocation = new javax.swing.JTextField();
    //--------------------------------------------------------------------------
        void BtnApply_actionPerformed(java.awt.event.ActionEvent event)
        {
        	saveFolderStorage();
        }
        
        public void saveFolderStorage(){
            try
            {
            	int storageId =((ServerSchema)CmbName.getSelectedItem()).getId();
                VWStorageConnector.setFolderStorage(
                ((VWTreeNode)tree.getSelectionPath().getLastPathComponent()).getId(),
                storageId,
                (ChkEnableInherit.isSelected()?1:0));
                String treePath = tree.getSelectionPath().toString();
                treePath = treePath.replaceAll(", ", "/");
                String message = AdminWise.connectorManager.getString("VWDlgStorageLocation.msg_1")+treePath+AdminWise.connectorManager.getString("VWDlgStorageLocation.msg_2")+(ServerSchema)CmbName.getSelectedItem()+"'";
                //Only on change of the storage id we are setting the history
                if(selectedStorageId!=storageId){
                	VWStorageConnector.addNotificationHistory(message, VWConstant.notify_Storage_LocationMod);
                	selectedStorageId = storageId;
                }
            }
            catch(Exception e){}    	
        }
    //--------------------------------------------------------------------------
    public void loadTabData()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect && gCurRoom != room.getId())
        {
            gCurRoom = room.getId();
            treeLoaded = false;
            tree.loadTreeData();
            loadStorageData();
            treeLoaded=true;
            if(CmbName.getItemCount()>0)
            {
                setFolderStorage();
                setEnableMode(MODE_SELECTEDSTORAGE);
            }
            else
            {
                setEnableMode(MODE_UNSELECTEDSTORAGE);
            }
        }
}
    //--------------------------------------------------------------------------
    public void setVisible(boolean b)
    {
        if(b)   loadTabData();
        super.setVisible(b);
    }
    //--------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        this.setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        VWTreeConnector.setTree(tree);
        tree.loadTreeData();
        refreshStorages();
    }
    //--------------------------------------------------------------------------
    private boolean isDefault()
    {
        return ((VWTreeNode)tree.getSelectionPath().getLastPathComponent()).getId()==0;
    }
    //--------------------------------------------------------------------------
    void CmbName_actionPerformed(java.awt.event.ActionEvent event)
    {
        ServerSchema selStorage=(ServerSchema) CmbName.getSelectedItem();
        if(selStorage==null) return;
        if(selStorage.getId()==-1)
        {
            setEnableMode(MODE_UNSELECTEDSTORAGE);
        }
        else if (selStorage.getId()==-2)
        {
            refreshStorages();
            setEnableMode(MODE_SELECTEDSTORAGE);
            
            selStorage = (ServerSchema) CmbName.getSelectedItem();
            txtServer.setText(selStorage.getAddress());
            txtPort.setText(String.valueOf(selStorage.getComport()));
            txtServerLocation.setText(selStorage.getPath());
        }
        else
        {
            setEnableMode(MODE_SELECTEDSTORAGE);
            CmbName.setToolTipText(
            "<html><ul><li><b>Host: "+selStorage.getAddress()+
            "</b></li><li><b>Location: "+selStorage.getPath()+
            "</b></li><li><b>Port: "+selStorage.getComport()+"</b></li></ul></html>");
            
            txtServer.setText(selStorage.getAddress());
            txtPort.setText(String.valueOf(selStorage.getComport()));
            txtServerLocation.setText(selStorage.getPath());
        }		 
    }
    //--------------------------------------------------------------------------
    private void refreshStorages()
    {
    try{    
        loadStorageData();
        if(tree.getSelectionPath()!=null)
            setFolderStorage(((VWTreeNode)tree.getSelectionPath().getLastPathComponent()).getId());
        ///setEnableMode(MODE_UNSELECTEDSTORAGE);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    private void loadStorageData()
    {
        CmbName.removeAllItems();
        ///VWStorageRec storageLabel = new VWStorageRec(-1,LBL_STORAGENAMES_NAME);
        ///CmbName.addItem(storageLabel);
        Vector storageList=VWStorageConnector.getStorages();
        if(storageList==null || storageList.size()==0) return;
        int count=storageList.size();
        for(int i=0;i<count;i++) CmbName.addItem(storageList.get(i));
        ServerSchema storageRefresh = new ServerSchema();
        storageRefresh.setId(-2);
        storageRefresh.setName(LBL_STORAGESERVER_NAME);
        CmbName.addItem(storageRefresh);
    }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode)
    {
        BtnApply.setEnabled(true);
        BtnClose.setEnabled(true);
        BtnRefresh.setEnabled(true); 
        tree.setEnabled(true);
        ChkEnableInherit.setEnabled(true);
        
        CmbName.setVisible(true);
        switch (mode)
        {
            case MODE_UNSELECTEDSTORAGE:
                BtnApply.setEnabled(false);
                break;
            case MODE_EXPAND:
                BtnApply.setEnabled(false);
                BtnClose.setEnabled(false);
                BtnRefresh.setEnabled(false); 
                ChkEnableInherit.setEnabled(false);
                tree.setEnabled(false);
                txtServer.setVisible(true);
                txtPort.setVisible(true);
                txtServerLocation.setVisible(true);
                BtnTestConnect.setVisible(true);
                break;
            case MODE_COLLAPSE:
                tree.setEnabled(true);
                txtServer.setVisible(false);
                txtPort.setVisible(false);
                txtServerLocation.setVisible(false);
                BtnTestConnect.setVisible(false);
                ChkEnableInherit.setEnabled(true);
                break;
        }
    }
    //--------------------------------------------------------------------------
    private void setFolderStorage()
    {
        TreePath treePath = tree.getSelectionPath();
        if(treePath!=null)
        setFolderStorage(((VWTreeNode)treePath.getLastPathComponent()).getId());
    }
    //--------------------------------------------------------------------------
    public int selectedStorageId = -1;
    private void setFolderStorage(int folderId)
    {
        int storageId=VWStorageConnector.getFolderStorage(folderId);
        selectedStorageId = storageId;
        CmbName.setSelectedIndex(setSelectedStorage(storageId));
    }
    //--------------------------------------------------------------------------
    private int setSelectedStorage(int storageId)
    {
        if(storageId==0) return -1;
        int count=CmbName.getItemCount();
        for (int i=0;i<count;i++)
            if(((ServerSchema)CmbName.getModel().getElementAt(i)).getId()==storageId) return i;
        return -1;
    }
    //--------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x",this.getX());
            prefs.putInt( "y",this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
 void BtnShow_actionPerformed(java.awt.event.ActionEvent event)
    {
        ServerSchema selStorage=(ServerSchema)CmbName.getSelectedItem();        
        if(selStorage==null) return;
        if(BtnShow.getText().equalsIgnoreCase(BTN_EXPAND_NAME))
        {
            txtServer.setText(selStorage.getAddress());
            txtPort.setText(String.valueOf(selStorage.getComport()));
            txtServerLocation.setText(selStorage.getPath());
            BtnShow.setText(BTN_COLLAPSE_NAME);
            setExpandPos();
        }
        else if(BtnShow.getText().equalsIgnoreCase(BTN_COLLAPSE_NAME))
        {
            if(!txtServer.getText().equals(selStorage.getAddress()) ||
                !txtPort.getText().equals(String.valueOf(selStorage.getComport())) ||
                !txtServerLocation.getText().equalsIgnoreCase(selStorage.getPath()))
            {	        	
            if(VWMessage.showConfirmDialog((java.awt.Component) this,
            		AdminWise.connectorManager.getString("VWDlgStorageLocation.msg_3")
                    )==javax.swing.JOptionPane.YES_OPTION)
                 BtnSave_actionPerformed(null);
            }
            setCollapsePos();
            BtnShow.setText(BTN_EXPAND_NAME);
        }
    }
    //--------------------------------------------------------------------------
    void setCollapsePos()
    {
    	if(languageLocale.equals("nl_NL")){
    		BtnApply.setBounds(204-57,434,72+25, AdminWise.gBtnHeight);
    		BtnClose.setBounds(276-27,434,72+25, AdminWise.gBtnHeight);
    		BtnRefresh.setBounds(6,434,82+15, AdminWise.gBtnHeight);
    	}else{
    		BtnApply.setBounds(204,434,72, AdminWise.gBtnHeight);
    		BtnClose.setBounds(276,434,72, AdminWise.gBtnHeight);
    		BtnRefresh.setBounds(6,434,82, AdminWise.gBtnHeight);
    	}
        storagePanel.setBounds(6,366,342,60);
        setEnableMode(MODE_COLLAPSE);
        setSize(360,500);
    }
    //--------------------------------------------------------------------------
    void setExpandPos()
    {
    	if(languageLocale.equals("nl_NL")){
    		BtnApply.setBounds(204-57,550,72+25, AdminWise.gBtnHeight);
    		BtnClose.setBounds(276-27,550,72+25, AdminWise.gBtnHeight);
    		BtnRefresh.setBounds(6,550,82+15, AdminWise.gBtnHeight);
    	}else{
    		BtnApply.setBounds(204,550,72, AdminWise.gBtnHeight);
    		BtnClose.setBounds(276,550,72, AdminWise.gBtnHeight);
    		BtnRefresh.setBounds(6,550,82, AdminWise.gBtnHeight);
    	}
        storagePanel.setBounds(6,366,342,180);
        setEnableMode(MODE_EXPAND);
        setSize(360,616);
    }
    //--------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
    	ServerSchema selStorage=(ServerSchema)CmbName.getSelectedItem();
        if(selStorage==null) return;
        if(!txtServer.getText().equals(selStorage.getAddress()) ||
                !txtPort.getText().equals(String.valueOf(selStorage.getComport())) ||
                !txtServerLocation.getText().equals(selStorage.getPath()))
        {
            selStorage.setAddress(txtServer.getText());
            selStorage.setComport(VWUtil.to_Number(txtPort.getText()));
            selStorage.setPath(txtServerLocation.getText());
            //Added in CV10 for azure storage 
            selStorage.setStorageType(selStorage.getStorageType());
            selStorage.setUserName(selStorage.getUserName());
            selStorage.setPassword(selStorage.getPassword());
            VWStorageConnector.updateStorageInfo(selStorage);
        }
        setCollapsePos();
        BtnShow.setText(BTN_EXPAND_NAME);
        saveFolderStorage();
    }
    void BtnTestConnect_actionPerformed(java.awt.event.ActionEvent event)
    {	
	String msg = "";
        ServerSchema selStorage=(ServerSchema)CmbName.getSelectedItem();
        boolean flag = false;
        if(selStorage!=null) 
            flag = VWStorageConnector.isDSSLocationExists(selStorage, txtServerLocation.getText());
        else
            msg = AdminWise.connectorManager.getString("VWDlgStorageLocation.msg_4");
        if (flag)
            JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("VWDlgStorageLocation.msg_5"));
        else
            JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("VWDlgStorageLocation.msg_6"));        
    }
    //--------------------------------------------------------------------------
    
    public void setHostedMode(){
    	boolean isHostedAdmin = false;
 		if(!VWConnector.isServerMachine()){
 			isHostedAdmin = VWConnector.getHostedAdmin();
 		}
 		txtServer.setEditable(!isHostedAdmin);
 		txtPort.setEditable(!isHostedAdmin);
 		txtServerLocation.setEditable(!isHostedAdmin);
    }
    String languageLocale=AdminWise.getLocaleLanguage();
    private static int curMode=-1;
    private boolean treeLoaded=false;
    private static int gCurRoom=0;
    public static void main(String[] args) {
	Frame frame = new Frame(); 
	VWDlgStorageLocation loc = new VWDlgStorageLocation(frame);
	frame.setVisible(true);	
	
    }
}