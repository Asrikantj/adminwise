package ViewWise.AdminWise.VWRecycle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWFind.VWDlgLocation;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.VWAddCustomIndices;
import ViewWise.AdminWise.VWRoute.VWDlgAddDynamicUserIndex;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWPrint;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWUtil;

import java.awt.Font;
import com.computhink.vwc.VWCPreferences;
import java.awt.event.ActionEvent;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class VWRecyclePanel extends JPanel implements  VWConstant {
    public VWRecyclePanel() {
        
    }
    public void setupUI() {	
    	int top = 116; int hGap = 28;
    	int height = 24, width = 144, left = 12, heightGap = 30;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWRecycle.setLayout(null);
        add(VWRecycle,BorderLayout.CENTER);
        VWRecycle.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWRecycle.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/

        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnAddToList.setText(BTN_ADDTOLIST_NAME);
        BtnAddToList.setActionCommand(BTN_ADDTOLIST_NAME);
        PanelCommand.add(BtnAddToList);
        BtnAddToList.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnAddToList.setIcon(VWImages.FindIcon);
        
        top += hGap;
        
        BtnEmptyRecycle.setText(BTN_EMPTY_RECYCLE);
        BtnEmptyRecycle.setActionCommand(BTN_EMPTY_RECYCLE);
        PanelCommand.add(BtnEmptyRecycle);
        BtnEmptyRecycle.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnEmptyRecycle.setIcon(VWImages.DelIcon);
        
        top += hGap;
        BtnPurge.setText(BTN_PURGE_NAME);
        BtnPurge.setActionCommand(BTN_PURGE_NAME);
        PanelCommand.add(BtnPurge);
//      BtnPurge.setBackground(java.awt.Color.white);
        BtnPurge.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnPurge.setIcon(VWImages.DelIcon);
        
        top += hGap;
        BtnPurgeAll.setText(BTN_PURGEALL_NAME);
        BtnPurgeAll.setActionCommand(BTN_PURGEALL_NAME);
        PanelCommand.add(BtnPurgeAll);
//      BtnPurgeAll.setBackground(java.awt.Color.white);
        BtnPurgeAll.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnPurgeAll.setIcon(VWImages.DelIcon);
        
        top += hGap;
        BtnAddIndex.setText(BTN_ADDINDEX_NAME);
        BtnAddIndex.setActionCommand(BTN_ADDINDEX_NAME);
        PanelCommand.add(BtnAddIndex);
//      BtnPurgeAll.setBackground(java.awt.Color.white);
        BtnAddIndex.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnAddIndex.setIcon(VWImages.CustomReportIcon);
        
        top += hGap + 12;
        BtnRestore.setText(BTN_RESTORE_NAME);
        BtnRestore.setActionCommand(BTN_RESTORE_NAME);
        PanelCommand.add(BtnRestore);
//      BtnRestore.setBackground(java.awt.Color.white);
        BtnRestore.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnRestore.setIcon(VWImages.RestoreIcon);
        
        top += hGap;
        BtnRestoreTo.setText(BTN_RESTORETO_NAME);
        BtnRestoreTo.setActionCommand(BTN_RESTORE_NAME);
        PanelCommand.add(BtnRestoreTo);
//      BtnRestoreTo.setBackground(java.awt.Color.white);
        BtnRestoreTo.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnRestoreTo.setIcon(VWImages.RestoreIcon);
        
        top += hGap + 12;
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        PanelCommand.add(BtnRefresh);
//      BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        
        /**
         * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
         */
        BtnProgressClick.setText("");
		PanelCommand.add(BtnProgressClick);
		BtnProgressClick.setBounds(left, top, width, height);
		BtnProgressClick.setVisible(false);
		BtnProgressClick.addActionListener(new SymAction());
		
        top += hGap + 12;
        BtnSaveAs.setText(BTN_SAVEAS_NAME);
        BtnSaveAs.setActionCommand(BTN_SAVEAS_NAME);
        PanelCommand.add(BtnSaveAs);
//      BtnSaveAs.setBackground(java.awt.Color.white);
        BtnSaveAs.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnSaveAs.setIcon(VWImages.SaveAsIcon);
        
        top += hGap;
        BtnPrint.setText(BTN_PRINT_NAME);
        BtnPrint.setActionCommand(BTN_PRINT_NAME);
        PanelCommand.add(BtnPrint);
//      BtnPrint.setBackground(java.awt.Color.white);
        BtnPrint.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnPrint.setIcon(VWImages.PrintIcon);
        
        top += hGap + 12;
        TxtQuickFind.setText("");
        PanelCommand.add(TxtQuickFind);
        TxtQuickFind.setBounds(12, top, AdminWise.gPanelBtnWidth - 40, AdminWise.gBtnHeight);
        TxtQuickFind.setToolTipText(AdminWise.connectorManager.getString("VWRecyclePanel.TxtQuickFind"));
        BtnFind.setIcon(VWImages.FindIcon);
        PanelCommand.add(BtnFind);
        BtnFind.setBounds(12+AdminWise.gPanelBtnWidth - 40 +5, top, 25, AdminWise.gBtnHeight);
        BtnFind.setToolTipText(AdminWise.connectorManager.getString("VWRecyclePanel.BtnFind"));
        
        
        top += hGap + 12;
        pnlNavigation.setLayout(null);
        PanelCommand.add(pnlNavigation);
        pnlNavigation.setBounds(12,top,AdminWise.gPanelBtnWidth,70);
        pnlNavigation.setBackground(AdminWise.getAWColor());
        
        btnFirst.setIcon(VWImages.MoveFirstIcon);
        btnFirst.setToolTipText(AdminWise.connectorManager.getString("VWRecyclePanel.btnFirst"));
        btnFirst.setBounds(0,0,30,22);
        pnlNavigation.add(btnFirst);

        btnPrevious.setIcon(VWImages.MovePreviousIcon);
        btnPrevious.setToolTipText(AdminWise.connectorManager.getString("VWRecyclePanel.btnPrevious"));
        btnPrevious.setBounds(35,0,30,22);
        pnlNavigation.add(btnPrevious);

        btnNext.setIcon(VWImages.MoveNextIcon);
        btnNext.setToolTipText(AdminWise.connectorManager.getString("VWRecyclePanel.btnNext"));
        btnNext.setBounds(81,0,30, 22);
        pnlNavigation.add(btnNext);
        
        btnLast.setIcon(VWImages.MoveLastIcon);
        btnLast.setToolTipText(AdminWise.connectorManager.getString("VWRecyclePanel.btnLast"));
        btnLast.setBounds(115,0,30, 22);
        pnlNavigation.add(btnLast);
        
        btnFirst.setEnabled(false);
        btnLast.setEnabled(false);
        btnNext.setEnabled(false);
        btnPrevious.setEnabled(false);
        
                
        lblCount.setBounds(0,35,150, 22);
        pnlNavigation.add(lblCount);

        
        PanelCommand.add(progress);
        top += hGap;
        progress.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gSmallBtnHeight);
        progress.setMinimum(0);
        progress.setVisible(false);
        PanelTable.setLayout(new BorderLayout());
        VWRecycle.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWRecycle.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.RecycleImage);
        SymAction lSymAction = new SymAction();
        BtnPurge.addActionListener(lSymAction);
        BtnEmptyRecycle.addActionListener(lSymAction);
        BtnAddToList.addActionListener(lSymAction);
        BtnPurgeAll.addActionListener(lSymAction);
        BtnAddIndex.addActionListener(lSymAction);
        BtnRestore.addActionListener(lSymAction);
        BtnRestoreTo.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        BtnSaveAs.addActionListener(lSymAction);
        BtnPrint.addActionListener(lSymAction);
        btnPrevious.addActionListener(lSymAction);
        btnNext.addActionListener(lSymAction);
        btnFirst.addActionListener(lSymAction);
        btnLast.addActionListener(lSymAction);
        BtnFind.addActionListener(lSymAction);
        BtnProgressClick.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
	            Object object = event.getSource();
	            if (object == BtnPurge)
	                BtnPurge_actionPerformed(event);
	            else if (object == BtnPurgeAll)
	                BtnPurgeAll_actionPerformed(event);
	            else if (object == BtnRestore)
	                BtnRestore_actionPerformed(event);
	            else if (object == BtnRestoreTo)
	                BtnRestoreTo_actionPerformed(event);
	            else if (object == BtnRefresh)
	                BtnRefresh_actionPerformed(event);
	            else if (object == BtnSaveAs)
	                BtnSaveAs_actionPerformed(event);
	            else if (object == BtnPrint)
	                BtnPrint_actionPerformed(event);
	            else if (object == BtnEmptyRecycle)
	            	BtnEmptyRecycle_actionPerformed(event);
	            else if (object == btnNext){
	                BtnNextData_actionPerformed(event);
	            }else if (object == btnPrevious)
	                BtnPreviousData_actionPerformed(event);
	            else if (object == btnFirst)
	                BtnFirstData_actionPerformed(event);
	            else if (object == btnLast)
	                BtnLastData_actionPerformed(event);
	            else if(object == BtnFind)
	            	BtnFind_actionPerformed(event);
	            else if(object == BtnAddToList)
	            	BtnAddToList_actionPerformed(event);
	            else if (object == BtnAddIndex)
	                BtnAddIndex_actionPerformed(event);
	            else if(object==BtnProgressClick){
					BtnProgressClick_actionPerformed(event);
				}
	            
	        }
        	
    }
    //--------------------------------------------------------------------------
    Vector customIndexList= new Vector();
    Vector newCustomIndexList=new Vector();
    javax.swing.JPanel VWRecycle = new javax.swing.JPanel();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_RECYCLE_NAME, VWImages.RecycleImage);    
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    javax.swing.JLabel lblCount  = new javax.swing.JLabel();
    VWLinkedButton BtnAddToList = new VWLinkedButton(1);
    VWLinkedButton BtnEmptyRecycle = new VWLinkedButton(1);
    VWLinkedButton BtnPurge = new VWLinkedButton(1);
    VWLinkedButton BtnPurgeAll = new VWLinkedButton(1);
    /**@author apurba.m
     * BtnAddIndex variable added for creating button for adding custom indices in Recycled Documents panel
     */
    VWLinkedButton BtnAddIndex = new VWLinkedButton(1);
    VWLinkedButton BtnRestore = new VWLinkedButton(1);
    VWLinkedButton BtnRestoreTo = new VWLinkedButton(1);
    VWLinkedButton BtnRefresh = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    javax.swing.JTextField TxtQuickFind = new javax.swing.JTextField();
    VWLinkedButton BtnFind = new VWLinkedButton(1);
    
    /**
     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
     */
    JButton BtnProgressClick=new  JButton();
    Lock lock1 = new ReentrantLock();
    String s1=new String(new char[] { 32 });
	String s2=new String(new char[] { 9 });
	JLabel statusLabel=new JLabel("		"+ s1+ s1+AdminWise.connectorManager.getString("progressDialog.statusForRecycled"));
	JDialog processDialog = new JDialog();
	//////////////////////////////////////
    
    JPanel pnlNavigation = new JPanel();
    VWLinkedButton btnNext = new VWLinkedButton(0);
    VWLinkedButton btnPrevious = new VWLinkedButton(0);
    VWLinkedButton btnFirst = new VWLinkedButton(0);
    VWLinkedButton btnLast = new VWLinkedButton(0);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWRecycleTable Table = new VWRecycleTable();
    javax.swing.JProgressBar progress = new javax.swing.JProgressBar();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWRecycle)
                VWRecycle_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWRecycle_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWRecycle) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    public void enableNavigation(boolean flag){
    	btnFirst.setEnabled(flag);
    	btnNext.setEnabled(flag);
    	btnLast.setEnabled(flag);
    	btnPrevious.setEnabled(flag);
    	TxtQuickFind.setEnabled(flag);
    	BtnFind.setEnabled(flag);
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
    	enableNavigation(true);
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        lastRecID=0;
        actionType = 0;
        String findString = TxtQuickFind.getText().trim();
        if(findString!=null && !findString.equals(""))
        	totalRecordCount = VWRecycleConnector.getRecycleDocCount(findString);
        else
        	totalRecordCount = VWRecycleConnector.getRecycleDocCount("");   
        getMoreData("");             
        btnFirst.setEnabled(false);
        btnPrevious.setEnabled(false);        
        if (totalRecordCount > 0 && totalRecordCount > AdminWise.adminPanel.recycleList){
            btnLast.setEnabled(true);
            btnNext.setEnabled(true);            
        }else{
            btnLast.setEnabled(false);
            btnNext.setEnabled(false);
        }
        
        /*try {
            Table.addData(VWRecycleConnector.getRecycle(newRoom, lastRecID));
        }
        catch(Exception e){};*/
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWSaveAsHtml.saveArrayInFile(Table.getReportData(),RecycleColumnNames,RecycleColumnWidths,
            null,(java.awt.Component)this,LBL_RECYCLEDDOC_NAME);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event) {
        new VWPrint(Table);
    }
    public void clearTable() {
        Table.clearData();
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return Table.getRowCount();
    }
    //--------------------------------------------------------------------------
    public void addData(List data) {
        Table.addData(data, true);
    }
    //--------------------------------------------------------------------------
    public void insertData(List data) {
        Table.insertData(data);
    }
    void BtnAddToList_actionPerformed(java.awt.event.ActionEvent event) {
    	VWRecycleConnector.openFindDialog();
    	lblCount.setText("");
    	enableNavigation(false);
    }    
    
    //--------------------------------------------------------------------------    
    void BtnEmptyRecycle_actionPerformed(java.awt.event.ActionEvent event) {
    	if(AdminWise.adminPanel.confirmDelRecycleDoc)
    		if(VWMessage.showConfirmDialog((java.awt.Component) this,
                LBL_PURGEDOCUMENT_NAME)!=javax.swing.JOptionPane.YES_OPTION) return;
    	int docsCountToPurge = AdminWise.adminPanel.recycleList;
    	if(docsCountToPurge<=0)
    		docsCountToPurge = 1000;
    	try{
            AdminWise.adminPanel.setWaitPointer();
            Table.clearData();
            Vector docsToPurge = new Vector();
            do
            {
                docsToPurge = VWRecycleConnector.emptyRecycle(docsCountToPurge);
                if(docsToPurge!=null && docsToPurge.size()>0){
	                viewProgress(docsToPurge.size(),LBL_PURGE_NAME);
	                for(int i=0;i<docsToPurge.size();i++){
	                	int docId = Integer.parseInt(docsToPurge.get(i).toString());
	                	VWRecycleConnector.purgeDoc(docId);
	                    setProgressValue(i,LBL_PURGE_NAME);
	                }
                }
            }while(docsToPurge!=null && !(docsToPurge.size()<docsCountToPurge));
            
            String message = AdminWise.connectorManager.getString("VWRecyclePanel.msg_0");
            int notifyId = VWConstant.notify_Recycle_Empty;
            int ret = VWRecycleConnector.addNotificationHistory(message, notifyId);
            setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
            hideProgress();
        }
    }
    //--------------------------------------------------------------------------
    void BtnNextData_actionPerformed(java.awt.event.ActionEvent event)
    {
	actionType = 1;
    	//Used a vector and sortedset to keep lastRecID, so that previous button can work properly.
    	//When next button is clicked a variable is incremented and when previous is clicked same variable is decresed.
    	try{    
    		String findString = TxtQuickFind.getText().trim();
    		if(findString!=null && !findString.equals(""))
    			getMoreData(findString);
    		else
    			getMoreData("");	    	
	    	if (currentPageNo >= totalPageCount ){//if (currentPageNo > totalPageCount ){
	    	    btnNext.setEnabled(false);
	    	    btnLast.setEnabled(false);
	    	}else{
	    	    btnNext.setEnabled(true);	    	    
	    	    btnLast.setEnabled(true);
	    	}
	    	    btnFirst.setEnabled(true);
	    	    btnPrevious.setEnabled(true);	    	
	    	
    	}catch(Exception e){    		
    		AdminWise.printToConsole("Error in getting next "+e.toString());
    	}
    }
    void BtnPreviousData_actionPerformed(java.awt.event.ActionEvent event)
    {
	try{
	    actionType = 2;
	    lastRecID = firstRecID;
	    String findString = TxtQuickFind.getText().trim();
		if(findString!=null && !findString.equals(""))
			getMoreData(findString);
		else
			getMoreData("");	 
	    if (currentPageNo == 1 ){
		btnPrevious.setEnabled(false);
		btnFirst.setEnabled(false);
	    }else{
		btnFirst.setEnabled(true);
		btnPrevious.setEnabled(true);
	    }
		btnNext.setEnabled(true);	    	    
		btnLast.setEnabled(true);

	}catch(Exception e){
	    AdminWise.printToConsole("Error in geting previous "+e.toString());
	}
    }
    void BtnFirstData_actionPerformed(java.awt.event.ActionEvent event)
    {
    	actionType = 0;
    	lastRecID =0;
    	
    	String findString = TxtQuickFind.getText().trim();
		if(findString!=null && !findString.equals(""))
			getMoreData(findString);
		else
			getMoreData("");
        btnFirst.setEnabled(false);
        btnPrevious.setEnabled(false);
        btnLast.setEnabled(true);
        btnNext.setEnabled(true);
    }
    void BtnLastData_actionPerformed(java.awt.event.ActionEvent event)
    {
	try{
	    actionType = 3;
	    lastRecID =0;
	    
	    String findString = TxtQuickFind.getText().trim();
		if(findString!=null && !findString.equals(""))
			getMoreData(findString);
		else
			getMoreData("");
	    btnLast.setEnabled(false);
	    btnNext.setEnabled(false);
	    btnFirst.setEnabled(true);
	    btnPrevious.setEnabled(true);
	}catch(Exception e){
	    AdminWise.printToConsole("LAST RECORD :"+e.toString());	    
	}
    }
    
    void BtnFind_actionPerformed(java.awt.event.ActionEvent event){
    	try{
    		String findString = TxtQuickFind.getText().trim();
    		totalRecordCount = VWRecycleConnector.getRecycleDocCount(findString);
    		if(!findString.equals("null")){
    			currentPageNo = 1;
    			lastRecID = 0;
        		actionType = 0;
    			getMoreData(findString);
    		}
    		if (totalRecordCount > 0 && totalRecordCount > AdminWise.adminPanel.recycleList){
                btnLast.setEnabled(true);
                System.out.println("Coming inside enabling BtnNext button. Find");
                btnNext.setEnabled(true);            
            }else{
                btnLast.setEnabled(false);
                btnNext.setEnabled(false);
            }       
            btnFirst.setEnabled(false);
            btnPrevious.setEnabled(false);
    	}catch(Exception ex){
    		AdminWise.printToConsole("Exception in Quick find :: "+ex.getMessage());
    	}
    	
    }
    /*public int getMoreData(String quickFind){
    	
    	System.out.println("quickFind :: "+quickFind);
    	int rowCount = AdminWise.adminPanel.recycleList;
        if(rowCount<=0)
            rowCount = 1000;
	
		if (actionType == 3 ){
	            currentPageNo = (totalRecordCount / rowCount);
	            int remainingRows = (totalRecordCount - (currentPageNo * rowCount)); 
	            if (remainingRows > 0 && remainingRows < rowCount){
	            	rowCount = remainingRows;
	            }
		}
		lastRecID = 0;
		actionType = 0;
		rowCount = 0;
    	List data=VWRecycleConnector.getRecycleForQuickFind(lastRecID, actionType, rowCount, quickFind);
        if(data==null || data.size()==0) {
        	Table.clearData();
            lblCount.setText("");
            return 0;
        }else{
        	//This is to update the count
        	actionType = 3;
        	totalRecordCount = data.size();
        }
        try
        {
            VWStringTokenizer tokens=new VWStringTokenizer((String)data.get(data.size()-1), VWConstant.SepChar,false);
            lastRecID=VWUtil.to_Number(tokens.nextToken());//Last Record Id 
            tokens=new VWStringTokenizer((String)data.get(0), VWConstant.SepChar,false);
            firstRecID = VWUtil.to_Number(tokens.nextToken());//First Record Id            
            Table.clearData();
            Table.addData(data);
            if (totalRecordCount < rowCount && totalPageCount <= 1){
	        	btnFirst.setEnabled(false);
	        	btnNext.setEnabled(false);
	        	btnPrevious.setEnabled(false);
	        	btnLast.setEnabled(false);
            }
            updateRecordCount();
            return data.size();
        }
        catch(Exception e){            
            return 0;}
    }*/
    //--------------------------------------------------------------------------
    public int getMoreData(String quickFindString)
    {
    	int rowCount = AdminWise.adminPanel.recycleList;
    	if(rowCount<=0)
    		rowCount = 1000;

    	if (actionType == 3 ){
    		currentPageNo = (totalRecordCount / rowCount);
    		int remainingRows = (totalRecordCount - (currentPageNo * rowCount)); 
    		if (remainingRows > 0 && remainingRows < rowCount){
    			rowCount = remainingRows;
    		}
    	}

    	List data = null;
    	//if(quickFindString!=null && !quickFindString.equals("")){
    	if(quickFindString!=null){
    		data=VWRecycleConnector.getRecycleForQuickFind(lastRecID, actionType, rowCount, quickFindString);
    	}
    	else{
    		data=VWRecycleConnector.getRecycle(lastRecID, actionType, rowCount);
    	}

    	if(data==null || data.size()==0) {
    		Table.clearData();
    		btnNext.setEnabled(false);
    		btnLast.setEnabled(false);
    		lblCount.setText("");
    		return 0;
    	}
    	try
    	{
    		VWStringTokenizer tokens=new VWStringTokenizer((String)data.get(data.size()-1), VWConstant.SepChar,false);
    		lastRecID=VWUtil.to_Number(tokens.nextToken());//Last Record Id 
    		tokens=new VWStringTokenizer((String)data.get(0), VWConstant.SepChar,false);
    		firstRecID = VWUtil.to_Number(tokens.nextToken());//First Record Id            
    		Table.clearData();
    		Table.addData(data);
    		
    		if (totalRecordCount < rowCount && totalPageCount <= 1){
    			btnFirst.setEnabled(false);
    			btnNext.setEnabled(false);
    			btnPrevious.setEnabled(false);
    			btnLast.setEnabled(false);
    		}
    		updateRecordCount();
    		return data.size();
    	}
    	catch(Exception e){            
    		return 0;}
    }
    void updateRecordCount(){
	try{
        int rowCount = AdminWise.adminPanel.recycleList;
        if(rowCount<=0)
            rowCount = 1000;        
        switch(actionType){
        case 0: //First            
            currentPageNo =1;
            break;
        case 1://Next
            currentPageNo++;
            break;
        case 2: //Previous
            currentPageNo--;
            break;
        case 3: //Last
            currentPageNo = (totalRecordCount / rowCount);
            if ((currentPageNo * rowCount) < totalRecordCount){
        	currentPageNo++;
            }
            break;
        default:
            break;
        }
    	int recStart = currentPageNo <= 1?1:(((currentPageNo-1)* rowCount) + 1);  
    	int recEnd = (recStart + rowCount - 1);
    	if (recEnd > totalRecordCount){
    	    recEnd = totalRecordCount;
    	}
    	lblCount.setText( recStart + " - " + recEnd + " / " + totalRecordCount);
    	//totalPageCount = totalRecordCount / rowCount;
    	totalPageCount = (int) Math.ceil(Double.valueOf(totalRecordCount) / Double.valueOf(rowCount));
	}catch(Exception ex){
	    ex.printStackTrace();
	}
    }
    //--------------------------------------------------------------------------
    void BtnPurge_actionPerformed(java.awt.event.ActionEvent event) {
        if(AdminWise.adminPanel.confirmDelRecycleDoc)
            if(VWMessage.showConfirmDialog((java.awt.Component) this,
            LBL_PURGEDOCUMENT_NAME)!=javax.swing.JOptionPane.YES_OPTION) return;
        totalRecordCount = VWRecycleConnector.getRecycleDocCount("");
        try{
            AdminWise.adminPanel.setWaitPointer();
            int[] selectedRows=Table.getSelectedRows();
            int count=selectedRows.length;
            viewProgress(count,LBL_PURGE_NAME);
            for(int i=0;i<count;i++) {
                int row=selectedRows[i]-i;
                VWRecycleConnector.purgeDoc(Table.getRowNodeId(row));
                setProgressValue(i,LBL_PURGE_NAME);
                Table.removeData(row);
            }
            String message = "'"+count+AdminWise.connectorManager.getString("VWRecyclePanel.msg_1");
            int notifyId = VWConstant.notify_Recycle_purgeDocs;
            int ret = VWRecycleConnector.addNotificationHistory(message, notifyId);
            setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
            hideProgress();
        }
    }
    //--------------------------------------------------------------------------
    void BtnPurgeAll_actionPerformed(java.awt.event.ActionEvent event) {
        if(AdminWise.adminPanel.confirmDelRecycleDoc)
            if(VWMessage.showConfirmDialog((java.awt.Component) this,
            LBL_PURGEDOCUMENT_NAME)!=javax.swing.JOptionPane.YES_OPTION) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            int count=Table.getRowCount();
            viewProgress(count,LBL_PURGE_NAME);
            for(int i=0;i<count;i++) {
                VWRecycleConnector.purgeDoc(Table.getRowNodeId(0));
                setProgressValue(i,LBL_PURGE_NAME);
                Table.removeData(0);
            }
            
            String message = "'"+count+AdminWise.connectorManager.getString("VWRecyclePanel.msg_1");
            int notifyId = VWConstant.notify_Recycle_purgeDocs;
            int ret = VWRecycleConnector.addNotificationHistory(message, notifyId);
            
            setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}        
        finally{
            AdminWise.adminPanel.setDefaultPointer();            
            hideProgress();
            totalRecordCount = VWRecycleConnector.getRecycleDocCount("");
        }
    }
    
    /**@author apurba.m
     * Add Index button in Recycled Panel for opening custom indices dialogue box
     * @param event
     */
    void BtnAddIndex_actionPerformed(java.awt.event.ActionEvent event) {
    	lock1.lock();
		BtnProgressClick.doClick();
    	Vector newCustomIndexList = new Vector();
    	Vector finalCustomList = new Vector();
    	AdminWise.adminPanel.setWaitPointer();
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(AdminWise.adminPanel.confirmDelRecycleDoc)
        try{
        	List data;
        	int rowCount = AdminWise.adminPanel.recycleList;
        	Vector settingsValue= new Vector();
        	Vector customIndexList= AdminWise.gConnector.ViewWiseClient.getCustomList(room.getId(), 2, settingsValue);
        	if (customIndexList.size() > 0) {
				String str= "";
				str= str+customIndexList.get(0).toString();
				String[] st1= str.split("\t");
				for(int i=1; i<st1.length;i++){
					newCustomIndexList.add(st1[i]);
				}
				finalCustomList = newCustomIndexList;
			} else {
				finalCustomList= null;
			}
        	data=VWRecycleConnector.getRecycle(lastRecID, actionType, rowCount);
        	VWAddCustomIndices dynamicUserIndexDlg = new VWAddCustomIndices(AdminWise.adminFrame, "", "RD",data,lastRecID,actionType,rowCount,Table,finalCustomList,processDialog);
        }
        catch(Exception e){}        
        finally{
            AdminWise.adminPanel.setDefaultPointer();            
            hideProgress();
            totalRecordCount = VWRecycleConnector.getRecycleDocCount("");
        }
    }
    
    /**
     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
     */
    void BtnProgressClick_actionPerformed(ActionEvent event){
		try{
			lock1.lock();
			statusLabel.paintImmediately(this.getBounds());
			statusLabel.setVisible(true);
			statusLabel.setLayout(new BorderLayout());
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
				fontName = VWCPreferences.getFontName();
			statusLabel.setFont(new Font(fontName, Font.PLAIN, 14));
			statusLabel.validate();
			/*if(scroll.isVisible()==true)
			processDialog.setModal(true);
			else 
				processDialog.setModal(false);*/
			processDialog.getContentPane().add(statusLabel, BorderLayout.CENTER);
			processDialog.setLocationRelativeTo(null);
			processDialog.setTitle("Workflow Report");
			processDialog.setAlwaysOnTop(true);
			processDialog.setSize(390,70);
			processDialog.setResizable(false);
			processDialog.setDefaultCloseOperation(processDialog.DO_NOTHING_ON_CLOSE);
			processDialog.setVisible(true);
			processDialog.setOpacity(1);
			//processDialog.pack();
			lock1.unlock();
			//processDialog.repaint();			

		}catch(Exception e){
			AdminWise.printToConsole("Exception while reviewUpdate action::::"+e.getMessage());
		}
	}
    
    //--------------------------------------------------------------------------
    void BtnRestore_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            int[] selectedRows=Table.getSelectedRows();
            int count=selectedRows.length;
            viewProgress(count,LBL_RESTORE_NAME);
            for(int i=0;i<count;i++) {
                try{
                    int row=selectedRows[i]-i;
                    int nodeId=Table.getRowNodeId(row);
                    VWRecycleConnector.restoreDoc(nodeId,0);
                    Table.removeData(row);
                    setProgressValue(i,LBL_RESTORE_NAME);
                }
                catch(Exception e){};
            }
            
            String message = "'"+count+AdminWise.connectorManager.getString("VWRecyclePanel.mag_2");
            int notifyId = VWConstant.notify_Recycle_RestoreDocs;
            VWRecycleConnector.addNotificationHistory(message, notifyId);
            
            setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
            hideProgress();}
    }
    //--------------------------------------------------------------------------
    void BtnRestoreTo_actionPerformed(java.awt.event.ActionEvent event) {
        int newParentId=0;
        //Commented if and else part to fix the tree loading issue for different room CV83B2
        /*  if(dlgLocation != null) {
            dlgLocation.setVisible(true);
        }
        else {*/
        dlgLocation = new VWDlgLocation(AdminWise.adminFrame,LBL_RESTORETO_NAME);
        dlgLocation.setVisible(true);
        //}
        if(dlgLocation.cancelFlag) return;
        newParentId=dlgLocation.tree.getSelectedNodeId();
        if(newParentId==0 || dlgLocation.tree.getSelectedNodeType()!=Folder_TYPE) return;
        try{
        	AdminWise.adminPanel.setWaitPointer();
        	int[] selectedRows=Table.getSelectedRows();
        	int count=selectedRows.length;
        	/**
        	 * For loop modified to fix stopping the document which is not having the document type
        	 * in cabinet level
        	 * 5/12/2014
        	 */
        	for(int i=0;i<count;i++) {
        		int row=selectedRows[i];
        		int nodeId=Table.getRowNodeId(row);
        		VWRecycleConnector.restoreDoc(nodeId,newParentId);
        		//Table.removeData(row);
        		}
        	BtnRefresh_actionPerformed(event);
        	String message = "'"+count+AdminWise.connectorManager.getString("VWRecyclePanel.mag_2");
        	int notifyId = VWConstant.notify_Recycle_RestoreDocs;
        	VWRecycleConnector.addNotificationHistory(message, notifyId);
        	setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event) {
    	enableNavigation(true);
        lastRecID=0;
        totalRecordCount = VWRecycleConnector.getRecycleDocCount("");
    	currentPageNo = 1;
    	actionType = 0;        
        getMoreData("");
        setEnableMode(MODE_UNSELECTED);
        if (totalRecordCount > 0 && totalRecordCount > AdminWise.adminPanel.recycleList){
            btnLast.setEnabled(true);
            btnNext.setEnabled(true);            
        }else{
            btnLast.setEnabled(false);
            btnNext.setEnabled(false);
        }       
        btnFirst.setEnabled(false);
        btnPrevious.setEnabled(false);
        TxtQuickFind.setText("");
       }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode) {
        BtnPurge.setEnabled(true);
        BtnAddToList.setEnabled(true);
        BtnEmptyRecycle.setEnabled(true);
        BtnPurgeAll.setEnabled(true);
        BtnAddIndex.setEnabled(true);
        BtnRestore.setEnabled(true);
        BtnRestoreTo.setEnabled(true);
        BtnRefresh.setEnabled(true);
        BtnSaveAs.setEnabled(true);
        BtnPrint.setEnabled(true);
        TxtQuickFind.setEnabled(true);
        BtnFind.setEnabled(true);
        switch (mode) {
            case MODE_UNSELECTED:
            	BtnPurge.setEnabled(false);
                BtnRestore.setEnabled(false);
                BtnRestoreTo.setEnabled(false);
                if(Table.getRowCount()==0) {
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                    BtnPurgeAll.setEnabled(false);
                    BtnAddIndex.setEnabled(false);
                    BtnEmptyRecycle.setEnabled(false);
                }
                break;
            case MODE_SELECT:
                if(Table.getRowCount()==0) {
                    BtnPurge.setEnabled(false);
                    BtnEmptyRecycle.setEnabled(false);
                    BtnRestore.setEnabled(false);
                    BtnRestoreTo.setEnabled(false);
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                    BtnPurgeAll.setEnabled(false);
                    BtnAddIndex.setEnabled(false);
                }
                break;
            case MODE_UNCONNECT:
                BtnPurge.setEnabled(false);
                BtnAddToList.setEnabled(false);
                BtnEmptyRecycle.setEnabled(false);
                BtnRestore.setEnabled(false);
                BtnRestoreTo.setEnabled(false);
                BtnRefresh.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                BtnPurgeAll.setEnabled(false);
                BtnAddIndex.setEnabled(false);
                btnNext.setEnabled(false);
                btnPrevious.setEnabled(false);
                btnFirst.setEnabled(false);
                btnLast.setEnabled(false);
                TxtQuickFind.setText("");
                TxtQuickFind.setEnabled(false);
                BtnFind.setEnabled(false);
                gCurRoom=0;
                lblCount.setText("");
                break;
        }
    }
    //--------------------------------------------------------------------------
    private void viewProgress(int max,String action) {
        progress.setMaximum(max);
        progress.setVisible(true);
        progress.setString(action+" [1] from ["+max+"]");
    }
    //--------------------------------------------------------------------------
    private void hideProgress() {
        progress.setVisible(false);
    }
    //--------------------------------------------------------------------------
    private void setProgressValue(int value,String action) {
        progress.setValue(value);
        progress.setString(action+" ["+value+"] from ["+progress.getMaximum()+"]");
        progress.paintImmediately(progress.getVisibleRect());
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    private int lastRecID=0;
    private int firstRecID=0;
    private int actionType=0;    
    private int lastRecordId = 0;
    private int totalRecordCount = 0;
    private int totalPageCount = 0;
    private int currentPageNo = 0;
    public int iVecSelectedRecIdCounter =0;
    
    public Vector vecSelectedRecId = new Vector();
    public TreeSet sortedSelectedRecId = new TreeSet();
    public VWDlgLocation dlgLocation = null;
    private boolean UILoaded=false;
}

