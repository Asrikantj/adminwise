/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRecycle;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;

import com.computhink.common.Document;

import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//------------------------------------------------------------------------------
public class VWRecycleTable extends JTable implements VWConstant
{
    protected VWRecycleTableData m_data;    
    public VWRecycleTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWRecycleTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        ///setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWRecycleTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        
        DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWRecycleTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWRecycleRowEditor editor=new VWRecycleRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWRecycleTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                        AdminWise.adminPanel.recyclePanel.setEnableMode(MODE_SELECT); 
                else
                    AdminWise.adminPanel.recyclePanel.setEnableMode(MODE_UNSELECTED);                
            }
        });
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        setRowHeight(TableRowHeight);
        
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);       
        getColumnModel().getColumn(0).setPreferredWidth(190);
        getColumnModel().getColumn(1).setPreferredWidth(190);
    	getColumnModel().getColumn(2).setPreferredWidth(190);
    	getColumnModel().getColumn(3).setPreferredWidth(190);
    	getColumnModel().getColumn(4).setPreferredWidth(190);
        setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
		Object object = event.getSource();
		if (object instanceof JTable)
			if(event.getModifiers()==event.BUTTON3_MASK)
		        VWRecycleTable_RightMouseClicked(event);
			else if(event.getModifiers()==event.BUTTON1_MASK)
				VWRecycleTable_LeftMouseClicked(event);
	}
	}
//------------------------------------------------------------------------------
void VWRecycleTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWRecycleTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
 public void addData(List list){
	 addData(list, false);
 }
  public void addData(List list, boolean fromDocument)
  {
        m_data.setData(list, fromDocument);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public String[][] getReportData()
  {
      String[][] tableData=m_data.getData();
      int count=tableData.length;
      /**@author apurba.m
       * range of repData has been changed to 100 for custom indices
       */
      String[][] repData=new String[count][100];
      for(int i=0;i<count;i++)
      {
          repData[i][0]=tableData[i][1];
          repData[i][1]=tableData[i][2];
          repData[i][2]=tableData[i][3];
          repData[i][3]=tableData[i][4];
          repData[i][4]=tableData[i][5];
      }
      return repData;
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  
  public void insertData(List list)
  {
    if(list==null || list.size()==0)
        return;  
    for(int i=0;i<list.size();i++)
    {
        m_data.insert(new RecycleRowData((Document)list.get(i)));
    }
    m_data.fireTableDataChanged();
  }
  
  
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new RecycleRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowNodeId(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[0]);
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class RecycleRowData
{
    public String   m_NodeId;
    public String   m_DocName;
    public String   m_Path;
    public String   m_UserName;
    public String   m_Client;
    public String   m_Time;
//------------------------------------------------------------------------------
    public RecycleRowData() {
    m_NodeId="0";
    m_DocName="";
    m_Path="";
    m_UserName="";
    m_Client="";
    m_Time="";
  }
    public RecycleRowData(Document doc)
    {
    	  m_NodeId = String.valueOf(doc.getId());
          m_DocName = doc.getName();
          m_UserName = doc.getDeletedUser();
          m_Path = doc.getDeletedLocation();
          m_Client=doc.getDeletedHost();
          m_Time= doc.getDeletedTime();

    }    
//------------------------------------------------------------------------------
  public RecycleRowData(String RNodeId,String RDocName,String RPath,
    String RUserName,String RClient,String RTime) 
  {
    m_NodeId=RNodeId;
    m_DocName=RDocName;
    m_Path=RPath;
    m_UserName=RUserName;
    m_Client=RClient;
    m_Time=RTime;
  }
//------------------------------------------------------------------------------
  public RecycleRowData(String str)
  {
    VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
    m_NodeId=VWUtil.getFixedValue(tokens.nextToken(),"0");
    m_DocName=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown name>");
    m_UserName=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown user>");
    m_Client=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown client>");
    m_Time=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown time>");
    m_Path=VWUtil.getFixedValue(tokens.nextToken(),"");
    ///m_Path=VWUtil.fixNodePath(VWUtil.getFixedValue(tokens.nextToken(),""));
  }
//------------------------------------------------------------------------------
  public RecycleRowData(String[] data) 
  {
    int i=0;
    m_NodeId=data[i++];
    m_DocName=data[i++];
    m_Path=data[i++];
    m_UserName=data[i++];
    m_Client=data[i++];
    m_Time=data[i];
  }
}
//------------------------------------------------------------------------------
class RecycleColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public RecycleColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWRecycleTableData extends AbstractTableModel 
{
  public static final RecycleColumnData m_columns[] = {
    new RecycleColumnData(VWConstant.RecycleColumnNames[0],0.3F,JLabel.LEFT ),
    new RecycleColumnData(VWConstant.RecycleColumnNames[1],0.4F, JLabel.LEFT),
    new RecycleColumnData(VWConstant.RecycleColumnNames[2],0.1F,JLabel.LEFT),
    new RecycleColumnData(VWConstant.RecycleColumnNames[3],0.1F,JLabel.LEFT),
    new RecycleColumnData(VWConstant.RecycleColumnNames[4],0.1F,JLabel.LEFT)
  };
  public static final int COL_DOCNAME = 0;
  public static final int COL_PATH = 1;
  public static final int COL_USERNAME = 2;
  public static final int COL_Client = 3;
  public static final int COL_TIME = 4;

  protected VWRecycleTable m_parent;
  protected Vector m_vector;

  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;
  
  public VWRecycleTableData(VWRecycleTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
/*public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(new RecycleRowData((String)data.get(i)));
    }
 }*/
public void setData(List data, boolean fromDocument) {
    m_vector.removeAllElements();
    if(data==null || data.size()==0) return;
    int count =data.size();
    for(int i=0;i<count;i++){
    	if ( fromDocument)
    		m_vector.addElement(new RecycleRowData((Document)data.get(i)));
    	else
    		m_vector.addElement(new RecycleRowData((String)data.get(i)));	
    }
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        RecycleRowData row =new RecycleRowData(data[i]); 
        if(i==2)
            m_vector.addElement(new Boolean(true));
        else
            m_vector.addElement(row);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][6];
    for(int i=0;i<count;i++)
    {
        RecycleRowData row=(RecycleRowData)m_vector.elementAt(i);
        data[i][0]=row.m_NodeId;
        data[i][1]=row.m_DocName;
        data[i][2]=row.m_Path;
        data[i][3]=row.m_UserName;
        data[i][4]=row.m_Client;
        data[i][5]=row.m_Time;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] RecycleRowData=new String[6];
    RecycleRowData row=(RecycleRowData)m_vector.elementAt(rowNum);
    RecycleRowData[0]=row.m_NodeId;
    RecycleRowData[1]=row.m_DocName;
    RecycleRowData[2]=row.m_Path;
    RecycleRowData[3]=row.m_UserName;
    RecycleRowData[4]=row.m_Client;
    RecycleRowData[5]=row.m_Time;
    return RecycleRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
  	String str = m_columns[column].m_title;
    if (column==m_sortCol)
        str += m_sortAsc ? " �" : " �";
    return str; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    RecycleRowData row = (RecycleRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_DOCNAME:     return row.m_DocName;
      case COL_PATH:     return row.m_Path;
      case COL_USERNAME:    return row.m_UserName;
      case COL_Client:      return row.m_Client;
      case COL_TIME:        return row.m_Time;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    RecycleRowData row = (RecycleRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_DOCNAME:
        row.m_DocName = svalue; 
        break;
      case COL_PATH:
        row.m_Path = svalue; 
        break;
      case COL_USERNAME:
        row.m_UserName = svalue; 
        break;
      case COL_Client:
        row.m_Client = svalue;
        break;
      case COL_TIME:
        row.m_Time= svalue;
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(RecycleRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
 class RecycleComparator implements Comparator {
     protected int     m_sortCol;
     protected boolean m_sortAsc;
 //--------------------------------------------------------------------------
     public RecycleComparator(int sortCol, boolean sortAsc) {
         m_sortCol = sortCol;
         m_sortAsc = sortAsc;
     }
 //--------------------------------------------------------------------------
     public int compare(Object o1, Object o2) {
         if(!(o1 instanceof RecycleRowData) || !(o2 instanceof RecycleRowData))
             return 0;
         RecycleRowData s1 = (RecycleRowData)o1;
         RecycleRowData s2 = (RecycleRowData)o2;
         int result = 0;
         double d1, d2;
         switch (m_sortCol) {
         	case COL_DOCNAME:
                 result = s1.m_DocName.toLowerCase().compareTo(s2.m_DocName.toLowerCase());
                 break;
            case COL_PATH:
                result = s1.m_Path.toLowerCase().compareTo(s2.m_Path.toLowerCase());
                break;            	
              case COL_USERNAME:
            	result = s1.m_UserName.toLowerCase().compareTo(s2.m_UserName.toLowerCase());
                break;
              case COL_Client:
            	result = s1.m_Client.toLowerCase().compareTo(s2.m_Client.toLowerCase());  
                break;
              case COL_TIME:
            	result = s1.m_Time.toLowerCase().compareTo(s2.m_Time.toLowerCase());  
                break;
         }
         if (!m_sortAsc)
             result = -result;
         return result;
     }
 //--------------------------------------------------------------------------
     public boolean equals(Object obj) {
         if (obj instanceof RecycleComparator) {
        	 RecycleComparator compObj = (RecycleComparator)obj;
             return (compObj.m_sortCol==m_sortCol) &&
             (compObj.m_sortAsc==m_sortAsc);
         }
         return false;
     }
 } 
 
 class ColumnListener extends MouseAdapter {
     protected VWRecycleTable m_table;
 //--------------------------------------------------------------------------
     public ColumnListener(VWRecycleTable table){
         m_table = table;
     }
 //--------------------------------------------------------------------------
     public void mouseClicked(MouseEvent e){
         
/*          if(e.getModifiers()==e.BUTTON3_MASK)
             selectViewCol(e);
         else */if(e.getModifiers()==e.BUTTON1_MASK)
             sortCol(e);
     }
 //--------------------------------------------------------------------------
     private void sortCol(MouseEvent e) {    	 
         TableColumnModel colModel = m_table.getColumnModel();
         int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
         int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
         
         if (modelIndex < 0) return;
         if (m_sortCol==modelIndex)
             m_sortAsc = !m_sortAsc;
         else
             m_sortCol = modelIndex;
         for (int i=0; i < colModel.getColumnCount();i++) {
             TableColumn column = colModel.getColumn(i);
             column.setHeaderValue(getColumnName(column.getModelIndex()));
         }
         m_table.getTableHeader().repaint();
         Collections.sort(m_vector, new RecycleComparator(modelIndex, m_sortAsc));
         m_table.tableChanged(
         new TableModelEvent(VWRecycleTableData.this));
         m_table.repaint();
     }
}
//------------------------------------------------------------------------------
}