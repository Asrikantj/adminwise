/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWRecycleConnector<br>
 *
 * @version     $Revision: 1.8 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRecycle;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import com.computhink.common.Document;
import com.computhink.common.Node;
import com.computhink.common.Search;
import com.computhink.common.Util;
import com.computhink.vwc.Session;

//------------------------------------------------------------------------------
public class VWRecycleConnector implements VWConstant
{
    public static List getRecycle(int lastRowId, int actionType, int rowCount)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            return getRecycle(room, lastRowId, actionType, rowCount);
        return null;
    }
//------------------------------------------------------------------------------
    public static List getRecycle(VWRoom room, int lastRowId, int actionType, int rowCount)
    {
        Vector docs=new Vector();        
        AdminWise.gConnector.ViewWiseClient.getRecycleDocList(room.getId(), rowCount, lastRowId, actionType, docs);
        return docs;
    }
    
//  ------------------------------------------------------------------------------
    public static List getRecycleForQuickFind(int lastRowId, int actionType, int rowCount, String indexValue)
    {
    	Vector docs=new Vector();    
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	if(room != null && room.getConnectStatus()==Room_Status_Connect){
    		AdminWise.gConnector.ViewWiseClient.getRecycleDocList(room.getId(), rowCount, lastRowId, actionType, indexValue, docs);
    		return docs;
    	}else
    		return null;
    }
//------------------------------------------------------------------------------
    public static void purgeDoc(int nodeId)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            AdminWise.gConnector.ViewWiseClient.purgeDocument(room.getId(),
                new Document(nodeId));
    }
    public static Vector emptyRecycle(int docsCountToPurge)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Vector docsToPurge = new Vector();
        if(room != null && room.getConnectStatus()==Room_Status_Connect){
        	AdminWise.gConnector.ViewWiseClient.getDocumentsToPurge(room.getId(), docsCountToPurge, docsToPurge);
        }
    	return docsToPurge;
    }
//------------------------------------------------------------------------------
    public static void restoreDoc(int nodeId,int parentid)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
        {
        	Vector validateDocType = new Vector(); 
            Node node=new Node(nodeId);
            node.setParentId(parentid);
            /** Used to check whether the document type is available for particular node
             *  before restoring the document. If document type not permitted then alert message 
             *  will be displayed and document will not get restored addressed in CV83B2.
             *  
             * */
            AdminWise.gConnector.ViewWiseClient.ValidateNodeDocType(room.getId(), 1, nodeId, parentid==0?nodeId:parentid, validateDocType);
            if(validateDocType!=null&&!validateDocType.isEmpty()&&validateDocType.size()>0&&!((String)validateDocType.get(0)).trim().equalsIgnoreCase("1")){
            	JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("VWRestorePanel.DocumentTypeNotPermitted"));
            	return;
            }
           	AdminWise.gConnector.ViewWiseClient.restoreNodeFromRecycle(room.getId(),node);
           
        }
    }
    public static int getRecycleDocCount(String quickFind)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int docCount = 0;
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
        {
        	if(quickFind!=null && !quickFind.equals("")){
        		docCount = AdminWise.gConnector.ViewWiseClient.getRecycleDocCount(room.getId(), quickFind);
        	}else
        		docCount = AdminWise.gConnector.ViewWiseClient.getRecycleDocCount(room.getId());   
            return docCount;
        }
        return 0;
    }
    public static int addNotificationHistory(String message, int notifyId){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Session session = AdminWise.gConnector.ViewWiseClient.getSession(room.getId());
    	
    	message = message.replaceAll("user", session.user);
    	try{
    		AdminWise.gConnector.ViewWiseClient.addNotificationHistory(room.getId(), 0, VWConstant.nodeType_RecycledDocs, notifyId, message);
    	}catch(Exception ex){
    		return -1;
    	}
    	return 0;
    }
    //--------------------------------------------------------------------------
    public static void openFindDialog(){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	com.computhink.common.vwfind.connector.VWRoom roomInfo = new com.computhink.common.vwfind.connector.VWRoom(room.getId(), room.getName());
    	roomInfo.setServer(room.getServer());
    	roomInfo.setDocuments(new Vector());
        Vector allRooms = new Vector();
        allRooms.add(roomInfo);
        Vector data = new Vector();
        AdminWise.adminPanel.recyclePanel.clearTable();
        //System.out.println("AdminWise.adminPanel.searchHits  " + AdminWise.adminPanel.searchHits);
        AdminWise.gConnector.ViewWiseClient.openFindDialog(room.getId(), AdminWise.adminFrame, allRooms, data, AdminWise.adminPanel.recycleList, "", 0,false,false,1);
        getFindResult(data);
   	 
        //loadDocumentFromFind((Vector)data.get(1), Boolean.valueOf(data.get(0).toString()).booleanValue());
    }

    public static void getFindResult(Vector data){
		if (data != null && data.size() == 2) {
			ArrayList roomSearchData = new ArrayList();
			roomSearchData = (ArrayList) data.get(1);
			boolean isMultipleRoom = false;
			boolean isAppend = Boolean.valueOf(data.get(0).toString()).booleanValue();
			//System.out.println("IsAppend " + isAppend);

			for (int count = 0; count < roomSearchData.size(); count++) {
				com.computhink.common.vwfind.connector.VWRoom searchRoom = (com.computhink.common.vwfind.connector.VWRoom) roomSearchData.get(count);
				int resultCount = searchRoom.getResultCount();
				int totalCount = 0;
				Search currentSearch = searchRoom.getSearch();
				//System.out.println("result Count " + resultCount + " :: "  + searchRoom.getId());
				if (resultCount > 100) isAppend = true;				
				if ( resultCount > 0 && currentSearch != null) {
					
					while(totalCount < resultCount && resultCount > 0)
					{
						if (AdminWise.adminPanel.recycleList > 0){
							currentSearch.setRowNo(-1 * AdminWise.adminPanel.recycleList);
						}
						else {
							currentSearch.setRowNo(-100);	
						}
						Vector searchData = new Vector();
						AdminWise.gConnector.ViewWiseClient.getFindResult(searchRoom.getId(), 
																					currentSearch, 
																					searchData, 0 , false, 1);
						totalCount += searchData.size();
						//System.out.println("searchData " + searchData.size());
						//System.out.println("total Count " + totalCount);
						if (searchData.size() > 0)
							loadDocumentFromFind(searchData, isAppend);
						else 
							break;
						//The following code is used to read the records 100
		    			if (currentSearch != null)
		    				currentSearch.setLastDocId(((Document) searchData.get(searchData.size() - 1)).getId());

					}
				}else{
					/**
					 * Code added to clear the table if there is no result for the search.
					 * 
					 */
					AdminWise.adminPanel.recyclePanel.Table.clearData();
				}
			}
		}    	
    }
    public static void loadDocumentFromFind(List list,boolean append) {
        if(!append) {
            AdminWise.adminPanel.recyclePanel.clearTable();
            if(list!=null) AdminWise.adminPanel.recyclePanel.addData(list);
        }
        else {
            if(list!=null) AdminWise.adminPanel.recyclePanel.insertData(list);
        }
        ///AdminWise.adminPanel.archivePanel.closeFindDlg();
        if(AdminWise.adminPanel.recyclePanel.Table.getRowCount()>0)
            AdminWise.adminPanel.recyclePanel.Table.setRowSelectionInterval(0,0);
    }
    
//------------------------------------------------------------------------------
}
