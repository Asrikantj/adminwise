package ViewWise.AdminWise.VWBackup;

import java.awt.Dimension;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import java.util.List;
import java.util.Vector;
import ViewWise.AdminWise.VWStorage.VWDlgStorageLocation;
import ViewWise.AdminWise.VWFind.VWFindDlg;
import java.awt.Component;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.JDialog;
import ViewWise.AdminWise.VWArchive.VWArchiveConnector;

public class VWBackupPanel extends JPanel implements  VWConstant
{
    public VWBackupPanel()
    {
    }
//--------------------------------------------------------------
    public void setupUI() 
    {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWBackup.setLayout(null);
        add(VWBackup,BorderLayout.CENTER);
        VWBackup.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWBackup.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/

        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);        
        
        BtnAddToList.setText(BTN_ADDTOLIST_NAME);
        BtnAddToList.setActionCommand(BTN_ADDTOLIST_NAME);
        PanelCommand.add(BtnAddToList);
        //BtnAddToList.setBackground(java.awt.Color.white);
        BtnAddToList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnAddToList.setIcon(VWImages.FindIcon);
        
        top += hGap + 12;
        BtnClearList.setText(BTN_CLEAR_NAME);
        BtnClearList.setActionCommand(BTN_CLEAR_NAME);
        PanelCommand.add(BtnClearList);
        //BtnClearList.setBackground(java.awt.Color.white);
        BtnClearList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearList.setIcon(VWImages.ClearIcon);
        
        top += hGap;
        BtnClearSelList.setText(BTN_CLEARSELECTED_NAME);
        BtnClearSelList.setActionCommand(BTN_CLEARSELECTED_NAME);
        PanelCommand.add(BtnClearSelList);
        //BtnClearSelList.setBackground(java.awt.Color.white);
        BtnClearSelList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearSelList.setIcon(VWImages.ClearIcon);
        
        top += hGap + 12;
        BtnBackup.setText(BTN_BACKUP_NAME);
        BtnBackup.setActionCommand(BTN_BACKUP_NAME);
        PanelCommand.add(BtnBackup);
        //BtnBackup.setBackground(java.awt.Color.white);
        BtnBackup.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnBackup.setIcon(VWImages.BackupIcon);
        
        top += hGap + 12;
        BtnOptions.setText(BTN_OPTIONS_NAME);
        BtnOptions.setActionCommand(BTN_OPTIONS_NAME);
        PanelCommand.add(BtnOptions);
        //BtnOptions.setBackground(java.awt.Color.white);
        BtnOptions.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnOptions.setIcon(VWImages.OptionsIcon);
        
        top += hGap + 12;
        BtnSize.setText(BTN_SIZE_NAME);
        BtnSize.setActionCommand(BTN_SIZE_NAME);
        PanelCommand.add(BtnSize);
        //BtnSize.setBackground(java.awt.Color.white);
        BtnSize.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSize.setIcon(VWImages.SizeIcon);
        
        top += hGap;
        TxtSize.setText("");
        PanelCommand.add(TxtSize);
        TxtSize.setBackground(java.awt.Color.white);
        TxtSize.setBounds(12,top,144, AdminWise.gSmallBtnHeight);
        TxtSize.setEditable(false);
        PanelTable.setLayout(new BorderLayout());
        VWBackup.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWBackup.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.BackupImage);
        SymAction lSymAction = new SymAction();
        BtnAddToList.addActionListener(lSymAction);
        BtnClearList.addActionListener(lSymAction);
        BtnClearSelList.addActionListener(lSymAction);
        BtnBackup.addActionListener(lSymAction);
        BtnOptions.addActionListener(lSymAction);
        BtnSize.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnAddToList)
                /*BtnAddToList_actionPerformed(event);*/
            	BtnFind_actionPerformed(event);
            else if (object == BtnClearList)
                BtnClearList_actionPerformed(event);
            else if (object == BtnClearSelList)
                BtnClearSelList_actionPerformed(event);			
            else if (object == BtnBackup)
                BtnBackup_actionPerformed(event);	
            else if (object == BtnOptions)
                BtnOptions_actionPerformed(event);	
            else if (object == BtnSize)
                BtnSize_actionPerformed(event);
        }
    }
//--------------------------------------------------------------
    javax.swing.JPanel VWBackup = new javax.swing.JPanel();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_BACKUP_NAME, VWImages.BackupImage);
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnAddToList = new VWLinkedButton(1);
    VWLinkedButton BtnClearList = new VWLinkedButton(1);
    VWLinkedButton BtnClearSelList = new VWLinkedButton(1);
    VWLinkedButton BtnBackup = new VWLinkedButton(1);
    VWLinkedButton BtnOptions = new VWLinkedButton(1);
    VWLinkedButton BtnSize = new VWLinkedButton(1);
    javax.swing.JTextField TxtSize = new javax.swing.JTextField();
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWBackupTable Table = new VWBackupTable();
//--------------------------------------------------------------
class SymComponent extends java.awt.event.ComponentAdapter
{
    public void componentResized(java.awt.event.ComponentEvent event)
    {
        Object object = event.getSource();
        if (object == VWBackup)
            VWBackup_componentResized(event);
    }
}
//--------------------------------------------------------------
void VWBackup_componentResized(java.awt.event.ComponentEvent event)
{
    if(event.getSource()==VWBackup)
    {
        Dimension mainDimension=this.getSize();
        Dimension panelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        panelDimension.width=mainDimension.width-commandDimension.width;
        PanelTable.setSize(panelDimension);
        PanelCommand.setSize(commandDimension);
        PanelTable.doLayout();
        AdminWise.adminPanel.MainTab.repaint();
    }
}
//--------------------------------------------------------------
    public void loadTabData(VWRoom newRoom)
    {
        if(newRoom.getConnectStatus()!=Room_Status_Connect)
        {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        findDlg=null;
        setEnableMode(MODE_UNSELECTED);
    }
//--------------------------------------------------------------
    public void unloadTabData()
    {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
//--------------------------------------------------------------
    void BtnAddToList_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(findDlg != null)
        {
		 // If already 'Find Panel' Opened, make it null and opened it again.             
         //   findDlg.setAppendEnabled(Table.getRowCount()>0);
         //   findDlg.setVisible(true);
        	findDlg = null;
        }
       // else
        {
            findDlg = new ViewWise.AdminWise.VWFind.VWFindDlg(
                AdminWise.adminFrame,CALLER_BACKUP);
            findDlg.setAppendEnabled(Table.getRowCount()>0);
            findDlg.setVisible(true);
        }
    }
    void BtnFind_actionPerformed(java.awt.event.ActionEvent event) {
    	VWBackupConnector.openFindDialog();
    }
    
//--------------------------------------------------------------
    public void closeFindDlg()
    {
        //findDlg.setVisible(false);
        if(Table.getSelectedRow()>=0)
            setEnableMode(MODE_SELECT);
        else
            setEnableMode(MODE_UNSELECTED);
    }
//--------------------------------------------------------------
    void BtnClearList_actionPerformed(java.awt.event.ActionEvent event)
    {
        clearTable();
        TxtSize.setText("");
        setEnableMode(MODE_UNSELECTED);
    }
//--------------------------------------------------------------
    void BtnClearSelList_actionPerformed(java.awt.event.ActionEvent event)
    {
        Table.deleteSelectedRows();
    }
//--------------------------------------------------------------
    void BtnBackup_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            backupInfoDlg=new VWBackupInfoDlg(AdminWise.adminFrame);
            backupInfoDlg.setVisible(true);
        }
        catch(Exception e){}
        finally
        {
            AdminWise.adminPanel.setDefaultPointer();
        }
        setEnableMode(MODE_UNSELECTED);
    }    
//--------------------------------------------------------------
    void BtnOptions_actionPerformed(java.awt.event.ActionEvent event)
    {
        new VWOptionsDlg(AdminWise.adminFrame);
    }
//--------------------------------------------------------------
    void BtnSize_actionPerformed(java.awt.event.ActionEvent event)
    {
        Vector docIds=Table.getseletedDocIds();        
        TxtSize.setText(VWBackupConnector.getDocsSize(docIds,AdminWise.adminPanel.backupWithVR));
    }    
//--------------------------------------------------------------
    public int getRowCount()
    {
        return Table.getRowCount();
    }
//--------------------------------------------------------------
    public void addData(List data)
    {
        Table.addData(data);
    }
//--------------------------------------------------------------
public void deleteRowOutOfList(List list)
    {
        Table.deleteRowOutOfList(list);
    }
//--------------------------------------------------------------
    public void insertData(List data)
    {
        Table.insertData(data);
    }
//--------------------------------------------------------------
    public void clearTable()
    {
        Table.clearData();
        setEnableMode(MODE_UNSELECTED);
    }
//--------------------------------------------------------------
    public void setEnableMode(int mode)
    {
        switch (mode)
        {
            case MODE_UNSELECTED:
                BtnAddToList.setEnabled(true);
                BtnClearList.setEnabled(true);
                BtnBackup.setEnabled(true);
                BtnOptions.setEnabled(true);
                BtnSize.setEnabled(true);
                BtnClearSelList.setEnabled(false);
                if(getRowCount()==0)
                {
                    BtnClearList.setEnabled(false);
                    BtnBackup.setEnabled(false);
                }else
                	BtnClearList.setEnabled(true);
                BtnSize.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                TxtSize.setText("");
                break;
            case MODE_SELECT:
                BtnAddToList.setEnabled(true);
                BtnOptions.setEnabled(true);
                if(getRowCount()==0)
                {
                    BtnClearList.setEnabled(false);
                    BtnSize.setEnabled(false);
                    BtnClearSelList.setEnabled(false);
                    BtnBackup.setEnabled(false);
                }
                else{
                    BtnClearList.setEnabled(true);
                    BtnClearSelList.setEnabled(true);
                    BtnBackup.setEnabled(true);
                    BtnSize.setEnabled(true);
                }
                break;
           case MODE_UNCONNECT:
                BtnAddToList.setEnabled(false);
                BtnClearList.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                BtnBackup.setEnabled(false);
                BtnOptions.setEnabled(false);
                BtnSize.setEnabled(false);
                TxtSize.setText("");
                gCurRoom=0;
                break;
        }
    }
//--------------------------------------------------------------
    private static int gCurRoom=0;
    public VWDlgStorageLocation storageDlg = null;
    public VWFindDlg findDlg=null;
    public VWBackupInfoDlg backupInfoDlg=null;
    private boolean UILoaded=false;
}