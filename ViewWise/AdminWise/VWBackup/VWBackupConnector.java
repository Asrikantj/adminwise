/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
 */
/**
 * VWBackupConnector<br>
 *
 * @version     $Revision: 1.5 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWBackup;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.Vector;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWRecord;
import java.awt.Component;
import java.io.File;

import com.computhink.common.Constants;
import com.computhink.common.Document;
import com.computhink.common.Search;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.resource.ResourceManager;

import java.util.Date;

public class VWBackupConnector implements VWConstant {
    public static String getDocsSize(Vector docIds, boolean withVR) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            return getDocsSize(docIds, room, withVR);
        return LBL_SIZE0_NAME;
    }
    //--------------------------------------------------------------------------
    public static String getDocsSize(Vector docIds,VWRoom room,boolean withVR) {
        String retSize=LBL_SIZE0_NAME;
        int count=docIds.size();
        long size=0;
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            for( int i=0;i<count;i++) {
                Document doc=new Document(VWUtil.to_Number((String)docIds.get(i)));
                AdminWise.gConnector.ViewWiseClient.getDocumentSize(room.getId(), doc, withVR);
                size+=doc.getVWDoc().getSize();
            }
            String subSize="";
            /*
            if(size>0 && size <(1024*1000))
            {
                retSize=String.valueOf(size/1024);
                    subSize=String.valueOf((double)size%1024);
                    if(subSize!=null && subSize.length()>2)
                        subSize=subSize.substring(0,2);
                    subSize=String.valueOf((double)size%1024).substring(0,2);
                    retSize=retSize+(subSize.equals("")?"":"."+subSize)+LBL_SIZEK_NAME;
            }
            else if(size>0)
            {
                    retSize=String.valueOf(size/1024/1024);
                    subSize=String.valueOf((double)size%(1024*1024));
                    if(subSize!=null && subSize.length()>2)
                        subSize=subSize.substring(0,2);
                    subSize=String.valueOf((double)size%(1024*1024)).substring(0,2);
                    retSize=retSize+(subSize.equals("")?"":"."+subSize)+LBL_SIZEM_NAME;
            }
             */
            if(size>0) {
                ///retSize=String.valueOf(size/1024)+LBL_SIZEK_NAME;
                retSize=String.valueOf(size/1024);
                subSize=String.valueOf((double)size%1024);
                if(subSize!=null && subSize.length()>2)
                    subSize=subSize.substring(0,2);
                subSize=String.valueOf((double)size%1024).substring(0,2);
                retSize=retSize+(subSize.equals("")?"":"."+subSize)+LBL_SIZEK_NAME;
            }
        }
        catch(Exception e){
            }
        finally
        {
            AdminWise.adminPanel.setDefaultPointer();
        }
        return retSize;
    }
    //--------------------------------------------------------------------------
    public static void loadDocumentFromFind(List list,boolean append) {
        if(!append) {
            AdminWise.adminPanel.backupPanel.clearTable();
            if(list!=null) AdminWise.adminPanel.backupPanel.addData(list);
        }
        else {
            if(list!=null) AdminWise.adminPanel.backupPanel.insertData(list);
        }
        ///AdminWise.adminPanel.backupPanel.closeFindDlg();
    }
    //--------------------------------------------------------------------------
    private static void setBackupResult(List BackupResult) {
        int count = BackupResult.size();
        if(count==1) {
            AdminWise.adminPanel.backupPanel.clearTable();
        }
        else if(count>1) {
            List list=new LinkedList();
            for(int i=1;i<count;i++) {
                list.add(new VWRecord((String)BackupResult.get(i),0));
            }
            AdminWise.adminPanel.backupPanel.deleteRowOutOfList(list);
        }
    }
    //--------------------------------------------------------------------------
    public static void backupDocuments(Component parent,String path) {
        int retVal=0;
        Vector backupInfo=new Vector();
        ///String path=AdminWise.adminPanel.backupPath;
        ///String path=new File(backupPath,"Documents").getPath();
        int count=AdminWise.adminPanel.backupPanel.Table.getRowCount();
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int rowIndex=0;
        for(int i=0;i<count;i++) {
            if(AdminWise.adminPanel.backupPanel.backupInfoDlg.cancelAction) break;
            String[] rowData=AdminWise.adminPanel.backupPanel.Table.getRowData(rowIndex);
            int nodeId=VWUtil.to_Number(rowData[6]);
            Document doc=new Document(nodeId);
            String docName=AdminWise.connectorManager.getString("backupConnector.docName")+" ["+room.getName()+"]["+nodeId+"]";
            String docPath=path.endsWith(File.separator)?
                path+docName:path+File.separator+docName;
            AdminWise.adminPanel.backupPanel.backupInfoDlg.setMainText
                (AdminWise.connectorManager.getString("backupConnector.setMainText0")+(i+1)+AdminWise.connectorManager.getString("backupConnector.setMainText1")+count+"]");
            try {
                Vector name=new Vector();
                // getLockOwner logic changed for document routing enhancement 30 Jan 2006
                if(AdminWise.adminPanel.backupType) {
                    AdminWise.gConnector.ViewWiseClient.getLockOwner(room.getId(),
                    doc, name, true);
                    if(name!= null && name.size()>0) {
                    	StringTokenizer st = new StringTokenizer((String) name.get(0), Util.SepChar);
                    	String resultValue = st.nextToken();
                    	if(resultValue.equalsIgnoreCase("0")&&  resultValue.equalsIgnoreCase("1")){
                    		AdminWise.adminPanel.backupPanel.Table.updateStatus(
                                    rowIndex,AdminWise.connectorManager.getString("backupConnector.updateStatus0")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"));
                                    backupInfo.add(AdminWise.connectorManager.getString("backupConnector.updateStatus1")+AdminWise.adminPanel.
                                        backupPanel.Table.getDocName(rowIndex)
                                    +AdminWise.connectorManager.getString("backupConnector.updateStatus2")+" "+ (String)name.get(0)+">");
                                    rowIndex++;
                                    continue;
                    	}else if(!resultValue.equalsIgnoreCase("0")&&  !resultValue.equalsIgnoreCase("1") 
                    			&& !resultValue.equalsIgnoreCase("2")){
                        AdminWise.adminPanel.backupPanel.Table.updateStatus(
                        rowIndex,AdminWise.connectorManager.getString("backupConnector.updateStatus3")+" "+ (String)name.get(0));
                        backupInfo.add(AdminWise.connectorManager.getString("backupConnector.updateStatus1")+AdminWise.adminPanel.
                            backupPanel.Table.getDocName(rowIndex)
	                        +AdminWise.connectorManager.getString("backupConnector.updateStatus2")+" "+ resultValue+">");
                        rowIndex++;
                        continue;
                    	}
                    }
                }
                doc.setName("");
                retVal=AdminWise.gConnector.ViewWiseClient.backupDoc(
                room.getId(), doc,
                AdminWise.adminPanel.backupWithHTML,
                AdminWise.adminPanel.backupWithVR ,
                AdminWise.adminPanel.backupWithAT,
                AdminWise.adminPanel.backupWithReferences,
                AdminWise.adminPanel.backupWithMiniViewer,
                AdminWise.adminPanel.backupWithUnencryptedPages,
                AdminWise.adminPanel.backupPageText,
                path);
                if(retVal==0 || retVal==ViewWiseErrors.documentNotFound) {
                    AdminWise.adminPanel.backupPanel.Table.deleteRow(rowIndex);
                    if(AdminWise.adminPanel.backupType)
                        AdminWise.gConnector.ViewWiseClient.deleteDocument
                            (room.getId(),new Document(nodeId));
                }
                else {
                    AdminWise.adminPanel.backupPanel.Table.updateStatus
                    (rowIndex,AdminWise.gConnector.ViewWiseClient.
                        getErrorDescription(retVal));
                    backupInfo.add(AdminWise.connectorManager.getString("backupConnector.updateStatus1")+ AdminWise.adminPanel.
                        backupPanel.Table.getDocName(rowIndex)
                    +AdminWise.connectorManager.getString("backupConnector.backupInfo0")+AdminWise.gConnector.ViewWiseClient.
                        getErrorDescription(retVal)+">");
                    rowIndex++;
                }
            }
            catch(Exception e){System.out.println(e.getMessage());};
            AdminWise.adminPanel.backupPanel.backupInfoDlg.PrgrsMain.setValue(i+1);
        }
        backupInfo.add(AdminWise.connectorManager.getString("backupConnector.backupInfo1")+" "+String.valueOf(count)+" "+AdminWise.connectorManager.getString("backupConnector.backupInfo2"));
        backupInfo.add(AdminWise.connectorManager.getString("backupConnector.backupInfo3")+" "+String.valueOf(count-backupInfo.size()+1)+" "+AdminWise.connectorManager.getString("backupConnector.backupInfo2"));
        backupInfo.add(AdminWise.connectorManager.getString("backupConnector.backupInfo4")+" "+String.valueOf(backupInfo.size()-2)+" "+AdminWise.connectorManager.getString("backupConnector.backupInfo2"));
        backupInfo.add(AdminWise.connectorManager.getString("backupConnector.backupInfo05")+" "+(new Date()).toString());
        VWUtil.writeListToFile(backupInfo,path.endsWith(File.separator)?
            path+"Backup.log":path+File.separator+"Backup.log",null);
    }
    //--------------------------------------------------------------------------
    public static void openFindDialog(){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	com.computhink.common.vwfind.connector.VWRoom roomInfo = new com.computhink.common.vwfind.connector.VWRoom(room.getId(), room.getName());
    	roomInfo.setServer(room.getServer());
        Vector allRooms = new Vector();
        allRooms.add(roomInfo);
        Vector data = new Vector();
        AdminWise.gConnector.ViewWiseClient.openFindDialog(room.getId(), AdminWise.adminFrame, allRooms, data, AdminWise.adminPanel.searchHits, "", 0);
/*      	 if (data != null && data.size() == 2){
 	    	ArrayList roomSearchData  = new ArrayList();
 	    	roomSearchData = (ArrayList)data.get(1);
 	    	System.out.println("Search Data " + roomSearchData);
 	    	for (int count = 0; count < roomSearchData.size(); count++){
 	    		com.computhink.common.vwfind.connector.VWRoom searchRoom = (com.computhink.common.vwfind.connector.VWRoom)roomSearchData.get(count);
 	    		Vector searchData = searchRoom.getDocuments();
    			loadDocumentFromFind(searchData, Boolean.valueOf(data.get(0).toString()).booleanValue());
 	    	}
 	    
 	    }
*/
        getFindResult(data);
    }
    public static void getFindResult(Vector data){
		if (data != null && data.size() == 2) {
			ArrayList roomSearchData = new ArrayList();
			roomSearchData = (ArrayList) data.get(1);
			boolean isMultipleRoom = false;
			boolean isAppend = Boolean.valueOf(data.get(0).toString()).booleanValue();
			System.out.println("IsAppend " + isAppend);

			for (int count = 0; count < roomSearchData.size(); count++) {
				com.computhink.common.vwfind.connector.VWRoom searchRoom = (com.computhink.common.vwfind.connector.VWRoom) roomSearchData.get(count);
				int resultCount = searchRoom.getResultCount();
				int totalCount = 0;
				Search currentSearch = searchRoom.getSearch();
				System.out.println("result Count " + resultCount + " :: "  + searchRoom.getId());
				if (resultCount > 100) isAppend = true;				
				if ( resultCount > 0 && currentSearch != null) {
					
					while(totalCount < resultCount && resultCount > 0)
					{
						currentSearch.setRowNo(100);
						Vector searchData = new Vector();
						AdminWise.gConnector.ViewWiseClient.getFindResult(searchRoom.getId(), 
																					currentSearch, 
																					searchData);
						totalCount += searchData.size();
						System.out.println("searchData " + searchData.size());
						System.out.println("total Count " + totalCount);
						if (searchData.size() > 0)
							loadDocumentFromFind(searchData, isAppend);
						else 
							break;
						//The following code is used to read the records 100
		    			if (currentSearch != null)
		    				currentSearch.setLastDocId(((Document) searchData.get(searchData.size() - 1)).getId());

					}
				}else{
					/**
					 * Code added to clear the table if there is no result for the search.
					 * Vijaypriya.B.K - 29 April 2009
					 */
					
					AdminWise.printToConsole("cleardata isAppend------------"+isAppend);
					if (!isAppend) {
					AdminWise.adminPanel.backupPanel.Table.clearData();
					}
				}
			}
		}    	
    }    
}