package ViewWise.AdminWise.VWBackup;

import javax.swing.JPanel;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import java.util.prefs.*;
import java.awt.Component;
import java.awt.CardLayout;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;
import java.io.File;
///import com.cct.util.SafeThread;
//------------------------------------------------------------------------------
public class VWBackupInfoDlg extends javax.swing.JDialog implements  VWConstant
{
    public VWBackupInfoDlg(Frame parent)
    {
        super(parent,LBL_BACKUPINFO_NAME,true);
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.BackupIcon.getImage());
        String languageLocale=AdminWise.getLocaleLanguage();
        getContentPane().setLayout(new CardLayout(0,0));
        setSize(335,220);
        ///infoPanel.setSize(335,210);
        confirmPanel.setLayout(null);
        getContentPane().add(AdminWise.connectorManager.getString("backupInfoDlg.name"),confirmPanel);
        confirmPanel.setBounds(0,0,324,192);
        int docCount=AdminWise.adminPanel.backupPanel.Table.getRowCount();
        lblCount.setText((AdminWise.adminPanel.backupType?AdminWise.connectorManager.getString("backupInfoDlg.lblCount0")+" ":AdminWise.connectorManager.getString("backupInfoDlg.lblCount1"))+" "+docCount+" "+AdminWise.connectorManager.getString("backupInfoDlg.lblCount2"));
        confirmPanel.add(lblCount);
        lblCount.setBounds(12,0,300,24);
        JLabel2.setText(AdminWise.connectorManager.getString("backupInfoDlg.JLabel2"));
        confirmPanel.add(JLabel2);
        JLabel2.setBounds(12,36,35,24);
        confirmPanel.add(txtPath);
        txtPath.setBounds(36,36,252,24);
        txtPath.setEditable(false);
        txtPath.setText(AdminWise.adminPanel.backupPath);
        BtnBrowse.setText("...");
//        BtnBrowse.setBackground(java.awt.Color.white);
        confirmPanel.add(BtnBrowse);
        BtnBrowse.setBounds(288,36,29,24);

        lblVersion.setText((AdminWise.adminPanel.backupWithVR?
        		AdminWise.connectorManager.getString("backupInfoDlg.lblwith"):AdminWise.connectorManager.getString("backupInfoDlg.lblwithout"))+" "+AdminWise.connectorManager.getString("backupInfoDlg.lblVersion"));
        confirmPanel.add(lblVersion);
        lblVersion.setBounds(12,72,300,24);

        lblReferences.setText((AdminWise.adminPanel.backupWithReferences?
        		AdminWise.connectorManager.getString("backupInfoDlg.lblwith"):AdminWise.connectorManager.getString("backupInfoDlg.lblwithout"))+" "+AdminWise.connectorManager.getString("backupInfoDlg.lblReferences"));
        confirmPanel.add(lblReferences);
        lblReferences.setBounds(172,72,300,24);

        lblAuditTrail.setText((AdminWise.adminPanel.backupWithAT?
        		AdminWise.connectorManager.getString("backupInfoDlg.lblwith"):AdminWise.connectorManager.getString("backupInfoDlg.lblwithout"))+" "+AdminWise.connectorManager.getString("backupInfoDlg.lblAuditTrail"));
        confirmPanel.add(lblAuditTrail);
        lblAuditTrail.setBounds(12,96,300,24);

        lblMiniViewer.setText((AdminWise.adminPanel.backupWithMiniViewer?
        		AdminWise.connectorManager.getString("backupInfoDlg.lblwith"):AdminWise.connectorManager.getString("backupInfoDlg.lblwithout"))+" "+AdminWise.connectorManager.getString("backupInfoDlg.lblMiniViewer"));
        confirmPanel.add(lblMiniViewer);
        lblMiniViewer.setBounds(172,96,300,24);

        lblProperty.setText((AdminWise.adminPanel.backupWithHTML?
        		AdminWise.connectorManager.getString("backupInfoDlg.lblwith"):AdminWise.connectorManager.getString("backupInfoDlg.lblwithout"))+" "+AdminWise.connectorManager.getString("backupInfoDlg.lblProperty"));
        confirmPanel.add(lblProperty);
        lblProperty.setBounds(12,120,300,24);

        lblEncryptePages.setText((AdminWise.adminPanel.backupWithUnencryptedPages?
        		AdminWise.connectorManager.getString("backupInfoDlg.lblEncryptePages0"):AdminWise.connectorManager.getString("backupInfoDlg.lblEncryptePages1")));
        confirmPanel.add(lblEncryptePages);
        lblEncryptePages.setBounds(172,120,300,24);

        BtnOk.setText(BTN_OK_NAME);
//        BtnOk.setBackground(java.awt.Color.white);
        confirmPanel.add(BtnOk);
        if(languageLocale.equals("nl_NL")){
        BtnOk.setBounds(55,150,82+15,23);
        }else
        	  BtnOk.setBounds(55,150,82,23);
        BtnOk.setIcon(VWImages.OkIcon);
        BtnCCancel.setText(BTN_CANCEL_NAME);
//        BtnCCancel.setBackground(java.awt.Color.white);
        BtnCCancel.setIcon(VWImages.CloseIcon);
        confirmPanel.add(BtnCCancel);
        if(languageLocale.equals("nl_NL")){
        	BtnCCancel.setBounds(187,150,82+15,23);
        }else
        	BtnCCancel.setBounds(187,150,82,23);
        infoPanel.setLayout(null);
        getContentPane().add("Sec",infoPanel);
        BtnCancel.setText(BTN_CANCEL_NAME);
//        BtnCancel.setBackground(java.awt.Color.white);
        infoPanel.add(BtnCancel);
        BtnCancel.setBounds(120,96,91,23);
        BtnCancel.setIcon(VWImages.CloseIcon);
        infoPanel.add(PrgrsMain);
        PrgrsMain.setBounds(12,48,303,19);
        infoPanel.add(LblMain);
        LblMain.setBounds(12,24,303,15);
        PrgrsMain.setStringPainted(true);
        PrgrsMain.setMaximum(docCount);
        PrgrsMain.setMinimum(0);
        LblMain.setText(AdminWise.connectorManager.getString("backupInfoDlg.LblMain")+docCount+"]");
        SymAction lSymAction = new SymAction();
        BtnCancel.addActionListener(lSymAction);
        BtnOk.addActionListener(lSymAction);
        BtnCCancel.addActionListener(lSymAction);
        BtnBrowse.addActionListener(lSymAction);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        getDlgOptions();
        setResizable(false);
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWBackupInfoDlg.this)
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
	//{{DECLARE_CONTROLS
        javax.swing.JPanel infoPanel = new javax.swing.JPanel();
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
	javax.swing.JProgressBar PrgrsMain = new javax.swing.JProgressBar();
	javax.swing.JLabel LblMain = new javax.swing.JLabel();
        javax.swing.JPanel confirmPanel = new javax.swing.JPanel();
	javax.swing.JLabel lblCount = new javax.swing.JLabel();
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
	javax.swing.JTextField txtPath = new javax.swing.JTextField();
	VWLinkedButton BtnBrowse = new VWLinkedButton(0,true);
	javax.swing.JLabel lblVersion = new javax.swing.JLabel();
	javax.swing.JLabel lblAuditTrail = new javax.swing.JLabel();
	javax.swing.JLabel lblProperty = new javax.swing.JLabel();
        javax.swing.JLabel lblReferences = new javax.swing.JLabel();
        javax.swing.JLabel lblMiniViewer = new javax.swing.JLabel();
        javax.swing.JLabel lblEncryptePages = new javax.swing.JLabel();
	VWLinkedButton BtnOk = new VWLinkedButton(0,true);
	VWLinkedButton BtnCCancel = new VWLinkedButton(0,true);

//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
            else if (object == BtnOk)
                BtnOk_actionPerformed(event);
            else if (object == BtnCCancel)
                BtnCCancel_actionPerformed(event);
            else if (object == BtnBrowse)
                BtnBrowse_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelAction=true;
    }
//------------------------------------------------------------------------------
    void BtnCCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(!txtPath.getText().equals(""))
        {
            ((CardLayout)getContentPane().getLayout()).next(getContentPane());
            setSize(335,164);
            getContentPane().doLayout();
            repaint();
            new BackupThread();
            /*
            try{
                AdminWise.adminPanel.setWaitPointer();
                VWBackupConnector.backupDocuments((Component)this.getParent());
            }
            catch(Exception e){}
            finally{
                AdminWise.adminPanel.setDefaultPointer();}
             */
        }
    }
//------------------------------------------------------------------------------
    void BtnBrowse_actionPerformed(java.awt.event.ActionEvent event)
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setCurrentDirectory(new File(AdminWise.adminPanel.backupPath));
        int returnVal=chooser.showSaveDialog((Component)this);
        String backupPath="";
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            backupPath=chooser.getSelectedFile().getPath();
            chooser.setVisible(false);
            chooser=null;
        }
        txtPath.setText(backupPath);
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x1",this.getX());
        prefs.putInt("y1",this.getY());
        prefs.putInt("width1",this.getSize().width);
        prefs.putInt("height1",this.getSize().height);
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x1",50),prefs.getInt("y1",50));
        ///setSize(prefs.getInt("width1",335),prefs.getInt("height1",210));
    }
//------------------------------------------------------------------------------
    public void setMainText(String text)
    {
        LblMain.setText(text);
    }
//------------------------------------------------------------------------------
 private class BackupThread extends Thread/*SafeThread*/
    {
        BackupThread()
        {
            setDaemon(true);
            ///setPriority(java.lang.Thread.MAX_PRIORITY);
            start();
        }
        public void run()
        {
            try{
            AdminWise.adminPanel.setWaitPointer();
            VWBackupConnector.backupDocuments(null,txtPath.getText().trim());
            }
            catch(Exception e){}
            finally{
                AdminWise.adminPanel.setDefaultPointer();            
                ////stop();
                saveDlgOptions();
                AdminWise.adminPanel.backupPanel.setEnableMode(MODE_SELECT);
                setVisible(false);
            }
        }
 }
    public boolean cancelAction=false;
}