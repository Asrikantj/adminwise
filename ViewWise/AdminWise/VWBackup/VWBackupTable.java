/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWBackupTable<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWBackup;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumnModel;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.util.Vector;
import java.util.Comparator;
import java.util.Collections;
import java.util.prefs.Preferences;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.common.Document;
import com.computhink.common.util.VWRefCreator;
//------------------------------------------------------------------------------
public class VWBackupTable extends JTable implements VWConstant
{
    protected VWBackupTableData m_data;
    TableColumn[] ColModel=new TableColumn[8];
    static boolean[] visibleCol={true,true,true,true,true,false,false,false};
    public VWBackupTable(){
        super();
        m_data = new VWBackupTableData(this);
        setAutoCreateColumnsFromModel(false);
        getTableHeader().setReorderingAllowed(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JTextField readOnlyText=new JTextField();
        readOnlyText.setEditable(false);
        Dimension tableWith = getPreferredScrollableViewportSize();

        for (int k = 0; k < VWBackupTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        ///DefaultTableCellRenderer textRenderer =new DefaultTableCellRenderer();
        DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWBackupTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWBackupRowEditor editor=new VWBackupRowEditor(this);
        TableColumn column = new TableColumn
            (k,Math.round(tableWith.width*VWBackupTableData.m_columns[k].m_width), 
                renderer, editor);
        addColumn(column);   
        ColModel[k]=column;
        }
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        setVisibleCols();
        setBackground(java.awt.Color.white);
        ///setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
            if (listSelectionModel.isSelectionEmpty())        
                AdminWise.adminPanel.backupPanel.setEnableMode(MODE_UNSELECTED); 
            else
                AdminWise.adminPanel.backupPanel.setEnableMode(MODE_SELECT);
        	}
        });
        setRowHeight(TableRowHeight);
        
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);        
        getColumnModel().getColumn(0).setPreferredWidth(190);
        getColumnModel().getColumn(1).setPreferredWidth(190);
    	getColumnModel().getColumn(2).setPreferredWidth(190);
    	getColumnModel().getColumn(3).setPreferredWidth(190);
    	getColumnModel().getColumn(4).setPreferredWidth(190);
        setTableResizable();
  }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public int getRowCount()
  {
      if (m_data==null) return 0;
      return m_data.getRowCount();
  }
//------------------------------------------------------------------------------
  public void deleteRow(int row)
  {
        m_data.delete(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteRowOutOfList(List list)
  {
      String[][] data=getData();
      int count=data.length;
      int rowIndex=0;
      for(int i=0;i<count;i++)
      {
        int recIndex=isDocIdInList(list,data[i][5]);
        if(recIndex<0) 
            m_data.delete(rowIndex);
        else
        {    
            VWRecord rec=(VWRecord)list.get(recIndex);
            int docId=0;
            try
            {
                docId=VWUtil.to_Number(rec.getSecName());
            }
            catch(Exception e){}
            ///m_data.updateStatus(rowIndex,docId);
            list.remove(recIndex);
            rowIndex++;
        }
      }
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
private int isDocIdInList(List list,String docId)
{
    int count=0;
    if(list==null || list.size()==0) return -1;
    count=list.size();
    for(int i=0;i<count;i++)
    {
        if(docId.equals(Integer.toString(((VWRecord)list.get(i)).getId())))
            return i;
    }
    return -1;
}
//------------------------------------------------------------------------------
  public void deleteSelectedRows()
  {
        int[] rows=getSelectedRows();
        int count=rows.length;
        for(int i=0;i<count;i++)
        {
            m_data.delete(rows[i]-i);
        }
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(List list)
  {
    if(list==null || list.size()==0)    return;  
    for(int i=0;i<list.size();i++)
    {
        m_data.insert(new BackupRowData((Document)list.get(i)));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
  //------------------------------------------------------------------------------
  public String getDocName(int rowNum)
  {
        String[] data=m_data.getBackupRowData(rowNum);
        return data[0];
  }
//------------------------------------------------------------------------------
  public String[] getBackupRowData(int rowNum)
  {
        return m_data.getBackupRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowDocId(int rowNum)
  {
        String[] row=m_data.getBackupRowData(rowNum);
        //Code modified for getting the document Id. The row[6] has doc id. By - Vijaypriya.B.K 
        return VWUtil.to_Number(row[6]);
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getBackupRowData(rowNum);
  }
//------------------------------------------------------------------------------
    public Vector getseletedDocIds()
    {
        int[] rows=getSelectedRows();
        int count=rows.length;
        String[][] data = m_data.getData();
        Vector docIds=new Vector();
        for(int i=0;i<count;i++)
            docIds.add(data[rows[i]][6]);
        return docIds;
    }
//------------------------------------------------------------------------------
    public String getDocIds(String sep)
    {
        String[][] data = m_data.getData();
        int count=data.length;
        String docIds="";
        for(int i=0;i<count-1;i++)
        {
            docIds+=data[i][5] + sep;
        }
        docIds+=data[count-1][5];
        return docIds;
    }
//------------------------------------------------------------------------------
    private void setVisibleCols()
{
    TableColumnModel model=getColumnModel();
    
    int count =visibleCol.length;
    int i=0;
    while(i<count)
    {
        if (!visibleCol[i])
        {
            TableColumn column = model.getColumn(i);
            model.removeColumn(column);
            count--;
        }
        else
        {
            i++;
        }
    }
        if(count==0)return;
        Dimension tableWith = getPreferredScrollableViewportSize();
        int colWidth=(int)Math.round(tableWith.getWidth()/count);
        for (i=0;i<count;i++)
        {
            model.getColumn(i).setPreferredWidth(colWidth);
        }
        setColumnModel(model);
}
//------------------------------------------------------------------------------
  class SymMouse extends java.awt.event.MouseAdapter
  {
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
    	Object object = event.getSource();
    	if (object instanceof JTable)
        if(event.getModifiers()==event.BUTTON3_MASK)
        	VWBackupTable_RightMouseClicked(event);
        else if(event.getModifiers()==event.BUTTON1_MASK)
            VWBackupTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWBackupTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWBackupTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
    /*
    VWMenu menu=null;
    int roomId=getRowRoomId(row);
    int docId=getRowDocId(row);
    if(VWTableConnector.checkIsDocOpen(roomId,docId))
        if(!AdminWise.gConnector.isDocumentShare(roomId,docId))
        {
            menu=new VWMenu(VWConstant.OpenDocument_TYPE);
            menu.show(this,event.getX(),event.getY()); 
        }
        else
        {
            menu=new VWMenu(VWConstant.OpenShareDocument_TYPE);
            menu.show(this,event.getX(),event.getY()-70); 
        }
     */
}
//------------------------------------------------------------------------------
private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
}
//------------------------------------------------------------------------------
private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
    
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
	loadDocumentInDTC(row);
 }
//------------------------------------------------------------------------------
 private void LoadPropData(int row)
{
}
//------------------------------------------------------------------------------
/**
 * loadDocumentInDTC - This method is used to open the document in DTC
 * Added By - Vijaypriya.B.K on February 05, 2009
 */
 private void loadDocumentInDTC(int row)
{
	int docId = this.getRowDocId(row);
    VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    String room = vwroom.getName();
	try {      
        //create a temp folder to place reference file in it
    	File folder = new File("c:\\VWReferenceFile\\");
        folder.mkdirs();
        //create a reference file to open document
        File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);
        
        //open reference file in ViewWise thus opening doc                        
        try{
            Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
            //String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
            String dtcPath = VWUtil.getHome();
            Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
        }catch(Exception ex){
        	ex.printStackTrace();
        	//JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
        }
    }
    catch (IOException ioe) {
    	ioe.printStackTrace();
    }
	/*
    int docId=getRowDocId(row);
    int roomId=getRowRoomId(row);
    String docName=(String)getValueAt(row,1);
    VWTreeConnector.setTreeNodeSelection(docId,roomId,true);
    VWTableConnector.getDocFile(roomId,docId,docName,VWTreeConnector.getFixTreePath());
     **/
}
 public void updateStatus(int row,String status) {
     m_data.updateStatus(row, status);
 }
//------------------------------------------------------------------------------
}
class BackupRowData
{
  public String  m_DocName;
  public String  m_DocType;
  public String  m_Creator;
  public String  m_CreationDate;
  public String  m_ModifiedDate;
  public String  m_Location;
  public int     m_DocId;
  public int     m_DocTypeId;
  public int     m_PageCount;
  public String  m_Status;
  public BackupRowData() {
  
    m_DocId=0;
    m_DocTypeId=0;
    m_DocName="";
    m_DocType="";
    m_Creator="";
    m_CreationDate="";
    m_ModifiedDate="";
    m_Location="";
    m_PageCount=0;
    m_Status="";
  }
//------------------------------------------------------------------------------
  public BackupRowData(Document doc)
  {
        m_DocId=doc.getId();
        m_DocTypeId=doc.getDocType().getId();
        m_DocName=doc.getName();
        m_DocType=doc.getDocType().getName();
        m_Creator=doc.getCreator();
        m_CreationDate=doc.getCreationDate();
        m_ModifiedDate=doc.getModifyDate();
        m_Location=doc.getLocation();
        m_PageCount=doc.getPageCount();
        m_Status="";
  }
}
//------------------------------------------------------------------------------
class BackupColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public BackupColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWBackupTableData extends AbstractTableModel
{
  public static final BackupColumnData m_columns[] = {
    new BackupColumnData(VWConstant.BackupColumnNames[0],0.3F,JLabel.LEFT ),
    new BackupColumnData(VWConstant.BackupColumnNames[1],0.1F, JLabel.LEFT),
    new BackupColumnData(VWConstant.BackupColumnNames[2],0.1F,JLabel.LEFT),
    new BackupColumnData(VWConstant.BackupColumnNames[3],0.1F,JLabel.LEFT),
    new BackupColumnData(VWConstant.BackupColumnNames[4],0.2F,JLabel.LEFT),
    new BackupColumnData(VWConstant.BackupColumnNames[5],0.1F,JLabel.LEFT),
    new BackupColumnData(VWConstant.BackupColumnNames[6],0.1F,JLabel.LEFT),
    new BackupColumnData(VWConstant.BackupColumnNames[7],0.1F,JLabel.LEFT),
  };
  public static final int COL_DOCNAME=0;
  public static final int COL_DOCTYPE=1;
  public static final int COL_CREATOR=2;
  public static final int COL_CREATIONDATE=3;
  public static final int COL_LOCATION=4;
  public static final int COL_MODIFIEDDATE=5;
  public static final int COL_PAGECOUNT=6;
  public static final int COL_STATUS=7;

  protected VWBackupTable m_parent;
  protected Vector m_vector;
  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;

  public VWBackupTableData(VWBackupTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if(data==null || data.size()==0)
        return;
    int count =data.size();
    for(int i=0;i<count;i++)
        m_vector.addElement(new BackupRowData((Document)data.get(i)));
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][8];
    for(int i=0;i<count;i++)
    {
        int j=0;
        BackupRowData row=(BackupRowData)m_vector.elementAt(i);
        data[i][j++]=row.m_DocName;
        data[i][j++]=row.m_DocType;
        data[i][j++]=row.m_Creator;
        data[i][j++]=row.m_CreationDate;
        data[i][j++]=row.m_Location;
        data[i][j++]=row.m_ModifiedDate;
        data[i][j++]=String.valueOf(row.m_DocId);
        data[i][j++]=String.valueOf(row.m_DocTypeId);
    }
    return data;
  }
//------------------------------------------------------------------------------
public String[] getBackupRowData(int rowNum) 
{
    int count=getRowCount();
    String[] BackupRowData=new String[8];
    int j=0;
    BackupRowData row=(BackupRowData)m_vector.elementAt(rowNum);
    BackupRowData[j++]=row.m_DocName;
    BackupRowData[j++]=row.m_DocType;    
    BackupRowData[j++]=row.m_Creator;
    BackupRowData[j++]=row.m_CreationDate;
    BackupRowData[j++]=row.m_Location;
    BackupRowData[j++]=row.m_ModifiedDate;
    BackupRowData[j++]=String.valueOf(row.m_DocId);
    BackupRowData[j]=String.valueOf(row.m_DocTypeId);
    return BackupRowData;
}
//------------------------------------------------------------------------------
public int getRowCount() 
{
    return m_vector==null?0:m_vector.size();
}
//------------------------------------------------------------------------------
public int getColumnCount()
{ 
    return m_columns.length; 
}
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    ///return m_columns[column].m_title; 
      String str = m_columns[column].m_title;
    if (column==m_sortCol)
      str += m_sortAsc ? " �" : " �";
    return str;
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public java.lang.Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    BackupRowData row = (BackupRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_DOCNAME: return row.m_DocName;  
      case COL_DOCTYPE: return row.m_DocType;
      case COL_CREATOR: return row.m_Creator;         
      case COL_CREATIONDATE: return row.m_CreationDate;
      case COL_LOCATION: return row.m_Location;
      case COL_MODIFIEDDATE: return row.m_ModifiedDate;
      case COL_PAGECOUNT: return String.valueOf(row.m_PageCount);
      case COL_STATUS: return row.m_Status;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    BackupRowData row = (BackupRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
    case COL_DOCNAME: 
        row.m_DocName=svalue;
        break;
    case COL_DOCTYPE:
        row.m_DocType=svalue;
        break;
    case COL_CREATOR:
        row.m_Creator=svalue;  
        break;
    case COL_CREATIONDATE:
        row.m_CreationDate=svalue;
        break;
    case COL_LOCATION:
        row.m_Location=svalue;
        break;
    case COL_MODIFIEDDATE:
        row.m_ModifiedDate=svalue;
        break;
    case COL_PAGECOUNT:
        row.m_PageCount=VWUtil.to_Number(svalue);
        break;
    case COL_STATUS:
        row.m_Status=svalue;
        break;
    }
//------------------------------------------------------------------------------
  }
  public void insert(int row) {
    if (row < 0) row = 0;
    if (row > m_vector.size()) row = m_vector.size();
    m_vector.insertElementAt(new BackupRowData(), row);
  }
//------------------------------------------------------------------------------
    public void insert(int row,BackupRowData BackupRowData){
    if (row < 0)
      row = 0;
    if (row > m_vector.size())
      row = m_vector.size();
    m_vector.insertElementAt(BackupRowData, row);
  }
//------------------------------------------------------------------------------
  public void insert(BackupRowData rowData) {
    int count=m_vector.size();
    boolean found=false;
    for(int i=0;i<count;i++)
    {
        BackupRowData tmpRowData=(BackupRowData)m_vector.get(i);
        if(tmpRowData.m_DocId==rowData.m_DocId )
        {
            found=true;
            break;
        }
    }
    if(!found) m_vector.addElement(rowData);
  }
//----------------------------------------------------------------------------
  public boolean delete(int row) {
    if (row < 0 || row >= m_vector.size())
      return false;
    m_vector.remove(row);
      return true;
  }
  //----------------------------------------------------------------------------
  public void updateStatus(int row,String status) {
      
      ((BackupRowData)m_vector.get(row)).m_Status=status;
      /*
      if((status > VWBackupConstant.BackupErr.length-1) || (status<0))
        ((BackupRowData)m_vector.get(row)).m_Status="Unknown error";
      else
        ((BackupRowData)m_vector.get(row)).m_Status=VWBackupConstant.BackupErr[status];
       **/
  }
//----------------------------------------------------------------------------
public void clear(){
    m_vector.removeAllElements();
  }
//------------------------------------------------------------------------------
   class ColumnListener extends MouseAdapter
  {
    protected VWBackupTable m_table;
//------------------------------------------------------------------------------
    public ColumnListener(VWBackupTable table){
      m_table = table;
    }
//------------------------------------------------------------------------------
    public void mouseClicked(MouseEvent e){
      
        if(e.getModifiers()==e.BUTTON3_MASK)
            selectViewCol(e);
	else if(e.getModifiers()==e.BUTTON1_MASK)
            sortCol(e);
    }
//------------------------------------------------------------------------------
    private void sortCol(MouseEvent e)
    {
        TableColumnModel colModel = m_table.getColumnModel();
        int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
        int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

        if (modelIndex < 0)
        return;
        if (m_sortCol==modelIndex)
        m_sortAsc = !m_sortAsc;
        else
        m_sortCol = modelIndex;

        for (int i=0; i < colModel.getColumnCount();i++) {
        TableColumn column = colModel.getColumn(i);
        column.setHeaderValue(getColumnName(column.getModelIndex()));    
        }
        m_table.getTableHeader().repaint();  
        Collections.sort(m_vector,new BackupComparator(modelIndex,m_sortAsc));
        m_table.tableChanged(new TableModelEvent(VWBackupTableData.this)); 
        m_table.repaint();  
    }
//------------------------------------------------------------------------------
    private void selectViewCol(MouseEvent e)
    {
        javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
        for (int i=0; i < m_table.BackupColumnNames.length; i++){
         javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
                m_table.BackupColumnNames[i],m_table.visibleCol[i]);
        TableColumn column = m_table.ColModel[i];
        subMenu.addActionListener(new ColumnKeeper(column,VWBackupTableData.m_columns[i]));
        menu.add(subMenu);
        }
        menu.show(m_table,e.getX(),e.getY());
    }
//------------------------------------------------------------------------------
  class ColumnKeeper implements java.awt.event.ActionListener
  {
    protected TableColumn m_column;
    protected BackupColumnData  m_colData;

    public ColumnKeeper(TableColumn column,BackupColumnData colData){
      m_column = column;
      m_colData = colData;
    }
//------------------------------------------------------------------------------
    public void actionPerformed(java.awt.event.ActionEvent e) {
      javax.swing.JCheckBoxMenuItem item=(javax.swing.JCheckBoxMenuItem)e.getSource();
      TableColumnModel model = m_table.getColumnModel();
      boolean found=false;
      int i=0;
      int count=m_table.BackupColumnNames.length;
      int colCount=model.getColumnCount();
      while (i<count && !found){
        if(m_table.BackupColumnNames[i].equals(e.getActionCommand()))
            found=true;
        i++;
        }
      i--;
      if (item.isSelected()) {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.addColumn(m_column);
      }
      else {
        if(colCount>1)
        {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.removeColumn(m_column);
        }
      }
      m_table.tableChanged(new javax.swing.event.TableModelEvent(m_table.m_data)); 
      m_table.repaint();
    }
  }
  }
//------------------------------------------------------------------------------
class BackupComparator implements Comparator
{
    protected int     m_sortCol;
    protected boolean m_sortAsc;
//------------------------------------------------------------------------------
    public BackupComparator(int sortCol, boolean sortAsc) {
    m_sortCol = sortCol;
    m_sortAsc = sortAsc;
    }
//------------------------------------------------------------------------------
    public int compare(Object o1, Object o2) {
    if(!(o1 instanceof BackupRowData) || !(o2 instanceof BackupRowData))
      return 0;
    BackupRowData s1 = (BackupRowData)o1;
    BackupRowData s2 = (BackupRowData)o2;
    int result = 0;
    double d1, d2;
    switch (m_sortCol) {
      case COL_DOCNAME:
        result = s1.m_DocName.toLowerCase().compareTo(s2.m_DocName.toLowerCase());
        break;
      case COL_DOCTYPE:
        result = s1.m_DocType.toLowerCase().compareTo(s2.m_DocType.toLowerCase());
        break;
      case COL_CREATOR:
        result = s1.m_Creator.toLowerCase().compareTo(s2.m_Creator.toLowerCase());
        break;
      case COL_CREATIONDATE:
        result = s1.m_CreationDate.toLowerCase().compareTo(s2.m_CreationDate.toLowerCase());
        break;
      case COL_LOCATION:
        result = s1.m_Location.toLowerCase().compareTo(s2.m_Location.toLowerCase());
        break;
      case COL_MODIFIEDDATE:
        result = s1.m_ModifiedDate.toLowerCase().compareTo(s2.m_ModifiedDate.toLowerCase());
        break;
      case COL_PAGECOUNT:
        result = String.valueOf(s1.m_PageCount).compareTo(String.valueOf(s2.m_PageCount));
        break;
      case COL_STATUS:
        result = s1.m_Status.toLowerCase().compareTo(s2.m_Status.toLowerCase());
        break;
    }
    if (!m_sortAsc)
      result = -result;
    return result;
    }
//------------------------------------------------------------------------------
    public boolean equals(Object obj) {
    if (obj instanceof BackupComparator) {
      BackupComparator compObj = (BackupComparator)obj;
      return (compObj.m_sortCol==m_sortCol) && 
        (compObj.m_sortAsc==m_sortAsc);
    }
    return false;
    }    
}
//------------------------------------------------------------------------------
}