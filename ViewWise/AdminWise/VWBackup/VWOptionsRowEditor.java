/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWSimpleSearchRowEditor<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWBackup;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWMask.VWMaskField;
import java.util.Locale;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;
import java.io.File;

public class VWOptionsRowEditor implements TableCellEditor,VWConstant{
  
  protected TableCellEditor editor,defaultEditor;
  protected VWOptionsTable gTable=null;
//------------------------------------------------------------------------------
  public VWOptionsRowEditor(VWOptionsTable table){
    JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;
  }
//------------------------------------------------------------------------------
  public Component getTableCellEditorComponent(JTable table,Object value,
        boolean isSelected, int row, int column){    
    
    if(row==0)
    {
        editor=new DefaultCellEditor(new VWComboBox(BackupType));
    }
    else if(row==1)
    {
        editor=new DefaultCellEditor(new VWComboBox(BackupType));
    }
    else if(row==2)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    else if(row==3)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    else if(row==4)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    else if(row==5)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    else if(row==6)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    else if(row==7)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    else if(row==8)
    {
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    }
    return editor.getTableCellEditorComponent(table,
             value, isSelected, row, column);
  }
//------------------------------------------------------------------------------
  public Object getCellEditorValue(){
    return editor.getCellEditorValue();
  }
//------------------------------------------------------------------------------
  public boolean stopCellEditing(){
    return editor.stopCellEditing();
  }
//------------------------------------------------------------------------------
  public void cancelCellEditing(){
    editor.cancelCellEditing();
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(EventObject anEvent){
      int column=-1,row=-1;  
      if (anEvent instanceof MouseEvent) {
	    Point point=((MouseEvent)anEvent).getPoint();
            column = gTable.columnAtPoint(point);
            row = gTable.rowAtPoint(point);
        }
        if(row==0) loadSelectFolderDialog();
        else if(column==1) return true;
        return false;
  }
//------------------------------------------------------------------------------
  public void addCellEditorListener(CellEditorListener l)
  {
    editor.addCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public void removeCellEditorListener(CellEditorListener l)
  {
    editor.removeCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public boolean shouldSelectCell(EventObject anEvent)
  {
    return editor.shouldSelectCell(anEvent);
  }
//------------------------------------------------------------------------------
  public void loadSelectFolderDialog()
{
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    String backupPath=gTable.getRowValue(0);
    chooser.setCurrentDirectory(new File(backupPath));
    int returnVal = chooser.showOpenDialog(/*(Component)this*/null);
    if(returnVal == JFileChooser.APPROVE_OPTION)
    {
        backupPath=chooser.getSelectedFile().getPath();
        chooser.setVisible(false);
        chooser=null;
    }
    gTable.setRowValue(0,backupPath);
}
//------------------------------------------------------------------------------
}
  