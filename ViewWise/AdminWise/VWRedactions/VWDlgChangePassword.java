/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWRedactions;

import java.awt.Dimension;
import javax.swing.JPanel;

import java.awt.Frame;
import java.awt.BorderLayout;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.awt.Insets;
import java.awt.event.ComponentListener;

import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWMessage.VWMessage;
import javax.swing.border.EtchedBorder;
import javax.swing.JPasswordField;
import ViewWise.AdminWise.VWConstant;

public class VWDlgChangePassword extends java.awt.Dialog implements ComponentListener, VWConstant
{
	Frame f = null;
	public VWDlgChangePassword(Frame parent)
	{
            super(parent,LBL_CHANGEPASSWORD_NAME,true);
            f=parent;
            add(pnlMain,BorderLayout.CENTER);
            setSize(400,245);
            pnlPasswords.add(TxtNewPassword);
            TxtNewPassword.setBounds(120,12,248,24);
            TxtNewPassword.setBorder(etchedBorder);
            
            pnlOptions.setLayout(null);
            OptAllPasswords.setBounds(12,48,120,18);
            pnlOptions.add(OptAllPasswords);
            OptSpecificPassword.setBounds(12,12,120,18);
            pnlOptions.add(OptSpecificPassword);
            pnlOptions.setBounds(0,0,400,70);
            pnlMain.setLayout(null);
            pnlMain.add(pnlOptions);
            OptAllPasswords.setBounds(12,48,120,18);
            TxtOldPassword.setBounds(130,12,248,24);
            pnlOptions.add(TxtOldPassword);
            OptSpecificPassword.setBounds(10,12,120,18);
            OptSpecificPassword.setBorder(etchedBorder);
            pnlPasswords.setBorder(etchedBorder);
            pnlPasswords.setLayout(null);
            pnlMain.add(pnlPasswords);
            pnlPasswords.setBounds(12,74,376,84);
            JLabel2.setText(LBL_NEWPASSWORD);
            pnlPasswords.add(JLabel2);
            JLabel2.setBounds(12,12,96,24);
            
            pnlOptions.add(TxtOldPassword);
            TxtOldPassword.setBounds(130,12,248,24);
            TxtOldPassword.setBorder(etchedBorder);
            JLabel3.setText(LBL_CONFIRMPASSWORD);
            pnlPasswords.add(JLabel3);
            JLabel3.setBounds(12,48,103,18);
            pnlPasswords.add(TxtConfirmPassword);
            TxtConfirmPassword.setBounds(120,48,248,24);
            TxtConfirmPassword.setBorder(etchedBorder);
            BtnOk.setText(BTN_OK_NAME);
            BtnCancel.setText(BTN_CANCEL_NAME);
            pnlButtons.setLayout(null);
            BtnOk.setBounds(71,10,80,25);
            pnlButtons.add(BtnOk);
            BtnCancel.setBounds(239,10,80,25);
            pnlButtons.add(BtnCancel);
            pnlButtons.setBounds(0,170,400,60);
            pnlMain.add(pnlButtons);
            javax.swing.ButtonGroup group = new javax.swing.ButtonGroup(); 
            OptAllPasswords.setSelected(true);
            group.add(OptAllPasswords); 
            OptAllPasswords.setText(LBL_ALLPASSWORD);
            OptAllPasswords.setIcon(VWImages.RadioIcon5); 
            OptAllPasswords.setPressedIcon(VWImages.RadioIcon2); 
            OptAllPasswords.setRolloverIcon(VWImages.RadioIcon3); 
            OptAllPasswords.setRolloverSelectedIcon(VWImages.RadioIcon4); 
            OptAllPasswords.setSelectedIcon(VWImages.RadioIcon1); 
            OptAllPasswords.setFocusPainted(false); 
            OptAllPasswords.setBorderPainted(false); 
            OptAllPasswords.setContentAreaFilled(false); 
            OptAllPasswords.setMargin(new Insets(0,0,0,0)); 
            OptSpecificPassword.setText(LBL_SPECIFICPASSWORD);
            OptSpecificPassword.setIcon(VWImages.RadioIcon5); 
            OptSpecificPassword.setPressedIcon(VWImages.RadioIcon2); 
            OptSpecificPassword.setRolloverIcon(VWImages.RadioIcon3); 
            OptSpecificPassword.setRolloverSelectedIcon(VWImages.RadioIcon4); 
            OptSpecificPassword.setSelectedIcon(VWImages.RadioIcon1); 
            OptSpecificPassword.setFocusPainted(false); 
            OptSpecificPassword.setBorderPainted(false); 
            OptSpecificPassword.setContentAreaFilled(false); 
            OptSpecificPassword.setMargin(new Insets(0,0,0,0)); 
            group.add(OptSpecificPassword); 
            OptAllPasswords_actionPerformed(null);
            SymAction lSymAction = new SymAction();
            BtnOk.addActionListener(lSymAction);
            BtnCancel.addActionListener(lSymAction);
            OptSpecificPassword.addActionListener(lSymAction);
            OptAllPasswords.addActionListener(lSymAction);
            SymKey aSymKey = new SymKey();
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            createFrame();
            setResizable(false);
            addComponentListener(this);
            setVisible(true);
	}
    public void componentMoved(java.awt.event.ComponentEvent event) {
    	pnlMain.repaint();
        pnlOptions.repaint();
        pnlPasswords.repaint();
        pnlButtons.repaint();
        AdminWise.printToConsole("################## Moved0 Dialog Box #####################"+f.getTitle());
    }
    public void componentHidden(java.awt.event.ComponentEvent event) {}
    public void componentShown(java.awt.event.ComponentEvent event) {}
    public void componentResized(java.awt.event.ComponentEvent event) {}
 	    
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgChangePassword.this)
                BtnCancel_actionPerformed(null);
        }
        
        public void windowActivated(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgChangePassword.this){
            	Dimension dim = getSize();
            	pnlOptions.repaint();
            	pnlPasswords.repaint();
            	pnlButtons.repaint();
            }
        }
    }
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(BtnOk.isEnabled() && event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    public void keyReleased(java.awt.event.KeyEvent event)
    {
        String str="";
        Object object = event.getSource();
        if(OptAllPasswords.isSelected())
        {
           if(object==TxtNewPassword)
            { 
        	   /*
       		 * JPasswordField.getText() is replaced with JPasswordField.getPassword() 
       		 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
       		 */
                str=new String(TxtNewPassword.getPassword());
                if(str.equals(""))
                {
                    BtnOk.setEnabled(false);
                }
                else
                {
                    BtnOk.setEnabled(true);
                }
           }
        }
        else
        {
            if(object==TxtConfirmPassword)
            { 
            	 /*
           		 * JPasswordField.getText() is replaced with JPasswordField.getPassword() 
           		 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
           		 */
                str=new String(TxtNewPassword.getPassword());
                if(str.equals("") || new String(TxtOldPassword.getPassword()).equals(""))
                {
                    BtnOk.setEnabled(false);
                }
                else
                {
                    BtnOk.setEnabled(true);
                }
           }
           else if(object==TxtOldPassword)
           {
        	   /*
          		 * JPasswordField.getText() is replaced with JPasswordField.getPassword() 
          		 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
          		 */
                str=new String(TxtNewPassword.getPassword());
                if(str.equals("") || new String(TxtConfirmPassword.getPassword()).equals(""))
                {
                    BtnOk.setEnabled(false);
                }
                else
                {
                    BtnOk.setEnabled(true);
                }
           }
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnOk)
                        BtnOk_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
                else if (object == OptSpecificPassword)
                        OptSpecificPassword_actionPerformed(event);
                else if (object == OptAllPasswords)
                        OptAllPasswords_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
	JPasswordField TxtNewPassword = new JPasswordField();
	javax.swing.JRadioButton OptAllPasswords = new javax.swing.JRadioButton();
	javax.swing.JRadioButton OptSpecificPassword = new javax.swing.JRadioButton();
	
	JPanel pnlMain = new JPanel();
	JPanel pnlOptions = new JPanel();
	JPanel pnlPasswords = new JPanel();
	JPanel pnlButtons = new JPanel();
	
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
	JPasswordField TxtOldPassword = new JPasswordField();
	javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
	JPasswordField TxtConfirmPassword = new JPasswordField();
        EtchedBorder etchedBorder = new EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);	
        VWLinkedButton BtnOk = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {
    	/*
  		 * JPasswordField.getText() is replaced with JPasswordField.getPassword() 
  		 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
  		 */
        if(!new String(TxtNewPassword.getPassword()).equals(new String(TxtConfirmPassword.getPassword())))
        {
            VWMessage.showMessage((java.awt.Component)this,
                VWMessage.ERR_CONFIRM_FAIL,VWMessage.DIALOG_TYPE);
            TxtConfirmPassword.setText("");
            TxtConfirmPassword.requestFocus();
            return;
        }
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void OptSpecificPassword_actionPerformed(java.awt.event.ActionEvent event)
    {
        TxtOldPassword.setEnabled(true);
    }
//------------------------------------------------------------------------------
    void OptAllPasswords_actionPerformed(java.awt.event.ActionEvent event)
    {
        TxtOldPassword.setEnabled(false);
        TxtOldPassword.setText("");
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    public String[] getValues()
    {
        String[] retValue=new String[2];
        /*
  		 * JPasswordField.getText() is replaced with JPasswordField.getPassword() 
  		 * as JPasswordField.getText() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
  		 */
        retValue[0]= new String(TxtNewPassword.getPassword());
        retValue[1]=new String(TxtOldPassword.getPassword());
        return retValue;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    void createFrame()
    {
        Dimension d =VWUtil.getScreenSize();
        setLocation(d.width/4,d.height/3);
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
}