/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWRedactionsConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRedactions;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
//------------------------------------------------------------------------------
public class VWRedactionsConnector implements VWConstant
{
    public static List getRedactionDocs(String userName)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect)
            return null;
        Vector redactions=new Vector();
        AdminWise.gConnector.ViewWiseClient.getUserRedactions(room.getId(),
            userName,redactions);
        return redactions;
    }
//------------------------------------------------------------------------------
    public static void replacePassword(Vector docIdList,String userName,
        String newPassword,String oldPassword)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.setUserRedactions(room.getId(), userName,
        oldPassword, newPassword, docIdList);
    }
//------------------------------------------------------------------------------
    public static Vector getUsers()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room==null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector users=new Vector();
        AdminWise.gConnector.ViewWiseClient.getCreators(room.getId(),users);
        return users;
}
//------------------------------------------------------------------------------
}