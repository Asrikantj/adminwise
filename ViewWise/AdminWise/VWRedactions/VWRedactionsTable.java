/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRedactions;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;

import com.computhink.common.util.VWRefCreator;

import java.awt.Dimension;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.LinkedList;
import java.util.prefs.Preferences;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//------------------------------------------------------------------------------
public class VWRedactionsTable extends JTable implements VWConstant
{
    protected VWRedactionsTableData m_data;
    public VWRedactionsTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWRedactionsTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        ///setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k=0;k<VWRedactionsTableData.m_columns.length;k++) 
        {
            TableCellRenderer renderer;
            DefaultTableCellRenderer textRenderer=new VWTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWRedactionsTableData.m_columns[k].m_alignment);
            renderer=textRenderer;
            VWRedactionsEditor editor=new VWRedactionsEditor(this);
            TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWRedactionsTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                    AdminWise.adminPanel.redactionsPanel.setEnableMode(MODE_SELECT); 
                else
                    AdminWise.adminPanel.redactionsPanel.setEnableMode(MODE_UNSELECTED);                
            }
        });
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);

        setRowHeight(TableRowHeight);
        
        setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
		Object object = event.getSource();
		if (object instanceof JTable)
			if(event.getModifiers()==event.BUTTON3_MASK)
		        VWRedactionsTable_RightMouseClicked(event);
			else if(event.getModifiers()==event.BUTTON1_MASK)
				VWRedactionsTable_LeftMouseClicked(event);
	}
}
//------------------------------------------------------------------------------
void VWRedactionsTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWRedactionsTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    	loadDocumentInDTC(row);

    }
//------------------------------------------------------------------------------
    private void loadDocumentInDTC(int row)
    {
    	int docId = this.getRowRedactionsId(row);
        VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        String room = vwroom.getName();
    	try {      
            //create a temp folder to place reference file in it
        	File folder = new File("c:\\VWReferenceFile\\");
            folder.mkdirs();
            //create a reference file to open document
            File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);
            
            //open reference file in ViewWise thus opening doc                        
            try{
                Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
                //String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
                String dtcPath = VWUtil.getHome();
                Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
            }catch(Exception ex){
            	ex.printStackTrace();
            	//JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
            }
        }
        catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
    public Vector getDocIds(String sep)
    {
        Vector list=new Vector();
        String[][] data = m_data.getData();
        int count=data.length;
        String docIds="";
        for(int i=0;i<count-1;i++)
        {
            docIds+=data[i][0]+sep;
            if(docIds.length()>7000)
            {
                list.add(docIds.substring(0,docIds.length()-sep.length()));
                docIds="";
            }
        }
        docIds+=data[count-1][0];
        list.add(docIds);
        return list;
    }
//------------------------------------------------------------------------------
  public String[][] getReportData()
  {
      String[][] tableData=m_data.getData();
      int count=tableData.length;
      String[][] repData=new String[count][4];
      for(int i=0;i<count;i++)
      {
          repData[i][0]=tableData[i][1];
          repData[i][1]=tableData[i][2];
          repData[i][2]=tableData[i][3];
          repData[i][3]=tableData[i][4];
      }
      return repData;
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new RedactionsRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteSelectedRows()
  {
        int[] rows=getSelectedRows();
        int count=rows.length;
        for(int i=0;i<count;i++)
        {
            m_data.remove(rows[i]-i);
        }
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowRedactionsId(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number((row[0]));
  }
//------------------------------------------------------------------------------
  public int getRowType()
  {
        return getRowType(getSelectedRow());
  }
//------------------------------------------------------------------------------
  public int getRowType(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number((row[6]));
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class RedactionsRowData
{
    public int      m_DocId;
    public String   m_DocName;
    public String   m_CreatorName;
    public String   m_Created;
    public String   m_Modified;


    public RedactionsRowData() {
        m_DocId=0;
        m_DocName="";
        m_CreatorName="";
        m_Created="";
        m_Modified="";
  }
  //----------------------------------------------------------------------------
  public RedactionsRowData(int DocId,String DocName,String CreatorName,
    String Created,String Modified) 
  {
        m_DocId=DocId;
        m_DocName=DocName;
        m_CreatorName=CreatorName;
        m_Created=Created;
        m_Modified=Modified;
  }
  //----------------------------------------------------------------------------
  public RedactionsRowData(String str)
  {
        VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
        try
        {
            m_DocId=VWUtil.to_Number(tokens.nextToken());
        }
        catch(Exception e){m_DocId=0;}
        try
        {
            m_DocName=tokens.nextToken();
        }
        catch(Exception e4){m_DocName=AdminWise.connectorManager.getString("VWRedatcionTable.RedactionsRowData_0");}
        try
        {
            m_CreatorName=tokens.nextToken();
        }
        catch(Exception e4){m_CreatorName=AdminWise.connectorManager.getString("VWRedatcionTable.RedactionsRowData_0");}
        try
        {
            m_Created=tokens.nextToken();
        }
        catch(Exception e4){m_Created=AdminWise.connectorManager.getString("VWRedatcionTable.RedactionsRowData_1");}
        try
        {
            m_Modified=tokens.nextToken();
        }
        catch(Exception e4){m_Modified=AdminWise.connectorManager.getString("VWRedatcionTable.RedactionsRowData_1");}
  }
  //----------------------------------------------------------------------------
  public RedactionsRowData(String[] data) 
  {
    int i=0;
    m_DocId=VWUtil.to_Number(data[i++]);
    m_DocName=data[i++];
    m_CreatorName=data[i++];
    m_Created=data[i++];
    m_Modified=data[i];
  }
}
//------------------------------------------------------------------------------
class RedactionsColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public RedactionsColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWRedactionsTableData extends AbstractTableModel 
{
  public static final RedactionsColumnData m_columns[] = {
    new RedactionsColumnData(VWConstant.RedactionsColumnNames[0],0.4F,JLabel.LEFT ),
    new RedactionsColumnData(VWConstant.RedactionsColumnNames[1],0.2F,JLabel.LEFT),
    new RedactionsColumnData(VWConstant.RedactionsColumnNames[2],0.2F,JLabel.LEFT),
    new RedactionsColumnData(VWConstant.RedactionsColumnNames[3],0.2F,JLabel.LEFT)
  };
  public static final int COL_DOCNAME = 0;
  public static final int COL_CREATORNAME = 1;
  public static final int COL_CREATED = 2;
  public static final int COL_MODIFIED = 3;

  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;
  
  protected VWRedactionsTable m_parent;
  protected Vector m_vector;

  public VWRedactionsTableData(VWRedactionsTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
        m_vector.addElement(new RedactionsRowData((String)data.get(i)));
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        RedactionsRowData row =new RedactionsRowData(data[i]);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][5];
    for(int i=0;i<count;i++)
    {
        RedactionsRowData row=(RedactionsRowData)m_vector.elementAt(i);
        data[i][0]=String.valueOf(row.m_DocId);
        data[i][1]=row.m_DocName;
        data[i][2]=row.m_CreatorName;
        data[i][3]=row.m_Created;
        data[i][4]=row.m_Modified;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] RedactionsRowData=new String[5];
    RedactionsRowData row=(RedactionsRowData)m_vector.elementAt(rowNum);
    RedactionsRowData[0]=String.valueOf(row.m_DocId);
    RedactionsRowData[1]=row.m_DocName;
    RedactionsRowData[2]=row.m_CreatorName;
    RedactionsRowData[3]=row.m_Created;
    RedactionsRowData[4]=row.m_Modified;
    return RedactionsRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
  	String str = m_columns[column].m_title;
    if (column==m_sortCol)
        str += m_sortAsc ? " �" : " �";
    return str; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    RedactionsRowData row = (RedactionsRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_DOCNAME:     return row.m_DocName;
      case COL_CREATORNAME:    return row.m_CreatorName;
      case COL_CREATED:     return row.m_Created;
      case COL_MODIFIED:    return row.m_Modified;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    RedactionsRowData row = (RedactionsRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_DOCNAME:
        row.m_DocName = svalue; 
        break;
      case COL_CREATORNAME:
        row.m_CreatorName = svalue; 
        break;
      case COL_CREATED:
        row.m_Created= svalue;
        break;
    case COL_MODIFIED:
        row.m_Modified= svalue;
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(RedactionsRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
 class RedactionComparator implements Comparator {
     protected int     m_sortCol;
     protected boolean m_sortAsc;
 //--------------------------------------------------------------------------
     public RedactionComparator(int sortCol, boolean sortAsc) {
         m_sortCol = sortCol;
         m_sortAsc = sortAsc;
     }
 //--------------------------------------------------------------------------
     public int compare(Object o1, Object o2) {
         if(!(o1 instanceof RedactionsRowData) || !(o2 instanceof RedactionsRowData))
             return 0;
         RedactionsRowData s1 = (RedactionsRowData)o1;
         RedactionsRowData s2 = (RedactionsRowData)o2;
         int result = 0;
         double d1, d2;
         switch (m_sortCol) {
         	case COL_DOCNAME:
                 result = s1.m_DocName.toLowerCase().compareTo(s2.m_DocName.toLowerCase());
                 break;
            case COL_CREATORNAME:
                result = s1.m_CreatorName.toLowerCase().compareTo(s2.m_CreatorName.toLowerCase());
                break;            	
              case COL_CREATED:
            	result = s1.m_Created.toLowerCase().compareTo(s2.m_Created.toLowerCase());
                break;
              case COL_MODIFIED:
            	result = s1.m_Modified.toLowerCase().compareTo(s2.m_Modified.toLowerCase());  
                break;     
         }
         if (!m_sortAsc)
             result = -result;
         return result;
     }
 //--------------------------------------------------------------------------
     public boolean equals(Object obj) {
         if (obj instanceof RedactionComparator) {
        	 RedactionComparator compObj = (RedactionComparator)obj;
             return (compObj.m_sortCol==m_sortCol) &&
             (compObj.m_sortAsc==m_sortAsc);
         }
         return false;
     }
 } 
 
 class ColumnListener extends MouseAdapter {
     protected VWRedactionsTable m_table;
 //--------------------------------------------------------------------------
     public ColumnListener(VWRedactionsTable table){
         m_table = table;
     }
 //--------------------------------------------------------------------------
     public void mouseClicked(MouseEvent e){
         
/*          if(e.getModifiers()==e.BUTTON3_MASK)
             selectViewCol(e);
         else */if(e.getModifiers()==e.BUTTON1_MASK)
             sortCol(e);
     }
 //--------------------------------------------------------------------------
     private void sortCol(MouseEvent e) {    	 
         TableColumnModel colModel = m_table.getColumnModel();
         int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
         int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
         
         if (modelIndex < 0) return;
         if (m_sortCol==modelIndex)
             m_sortAsc = !m_sortAsc;
         else
             m_sortCol = modelIndex;
         for (int i=0; i < colModel.getColumnCount();i++) {
             TableColumn column = colModel.getColumn(i);
             column.setHeaderValue(getColumnName(column.getModelIndex()));
         }
         m_table.getTableHeader().repaint();
         Collections.sort(m_vector, new RedactionComparator(modelIndex, m_sortAsc));
         m_table.tableChanged(
         new TableModelEvent(VWRedactionsTableData.this));
         m_table.repaint();
     }
} 
}