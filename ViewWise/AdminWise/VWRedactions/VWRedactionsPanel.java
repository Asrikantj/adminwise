package ViewWise.AdminWise.VWRedactions;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWFind.VWFindConnector;
import java.util.List;
import java.util.LinkedList;
import java.awt.Frame;
import ViewWise.AdminWise.VWUtil.VWPrint;
import com.computhink.common.Creator;

public class VWRedactionsPanel extends JPanel implements  VWConstant
{
	public VWRedactionsPanel()
	{
	    
	}
        public void setupUI() {
        	int top = 116; int hGap = 28;
            if(UILoaded) return;
            setLayout(new BorderLayout());
            VWRedactions.setLayout(null);
            add(VWRedactions,BorderLayout.CENTER);
            VWRedactions.setBounds(0,0,605,433);
            PanelCommand.setLayout(null);
            VWRedactions.add(PanelCommand);
            PanelCommand.setBackground(AdminWise.getAWColor());
            PanelCommand.setBounds(0,0,168,432);
/*            Pic.setIconTextGap(0);
            Pic.setAutoscrolls(true);
            PanelCommand.add(Pic);
            Pic.setBounds(8,4,149,92);*/

            PanelCommand.add(picPanel);
            picPanel.setBounds(8,16,149,92);            
            
            PanelCommand.add(CmbUsers);
            CmbUsers.setBackground(java.awt.Color.white);
            CmbUsers.setBorder(etchedBorder); 
            CmbUsers.setBounds(12,top,144, AdminWise.gSmallBtnHeight);
            
            top += hGap + 6;
            BtnGetDocs.setText(BTN_GETDOCS_NAME);
            BtnGetDocs.setActionCommand(BTN_GETDOCS_NAME);
            PanelCommand.add(BtnGetDocs);
            //BtnGetDocs.setBackground(java.awt.Color.white);
            BtnGetDocs.setBounds(12,top,144, AdminWise.gBtnHeight);
            BtnGetDocs.setIcon(VWImages.FindIcon);
            
            top += hGap + 12;
            BtnClear.setText(BTN_CLEARSELECTED_NAME);
            BtnClear.setActionCommand(BTN_CLEARSELECTED_NAME);
            PanelCommand.add(BtnClear);
            //BtnClear.setBackground(java.awt.Color.white);
            BtnClear.setBounds(12,top,144, AdminWise.gBtnHeight);
            BtnClear.setIcon(VWImages.ClearIcon);
            
            top += hGap + 12;
            BtnReplace.setText(BTN_REPLACE_NAME);
            BtnReplace.setActionCommand(BTN_REPLACE_NAME);
            PanelCommand.add(BtnReplace);
            //BtnReplace.setBackground(java.awt.Color.white);
            BtnReplace.setBounds(12,top,144, AdminWise.gBtnHeight);
            BtnReplace.setIcon(VWImages.ReplaceIcon);
            
            top += hGap + 12;
            BtnSaveAs.setText(BTN_SAVEAS_NAME);
            BtnSaveAs.setActionCommand(BTN_SAVEAS_NAME);
            PanelCommand.add(BtnSaveAs);
            //BtnSaveAs.setBackground(java.awt.Color.white);
            BtnSaveAs.setBounds(12,top,144, AdminWise.gBtnHeight);
            BtnSaveAs.setIcon(VWImages.SaveAsIcon);
            
            top += hGap;
            BtnPrint.setText(BTN_PRINT_NAME);
            BtnPrint.setActionCommand(BTN_PRINT_NAME);
            PanelCommand.add(BtnPrint);
            //BtnPrint.setBackground(java.awt.Color.white);
            BtnPrint.setBounds(12,top,144, AdminWise.gBtnHeight);
            BtnPrint.setIcon(VWImages.PrintIcon);
            
            PanelTable.setLayout(new BorderLayout());
            VWRedactions.add(PanelTable);
            PanelTable.setBounds(168,0,433,429);
            SPTable.setOpaque(true);
            PanelTable.add(SPTable,BorderLayout.CENTER);
            SPTable.getViewport().add(Table);
            Table.getParent().setBackground(java.awt.Color.white);
            setEnableMode(MODE_UNCONNECT);
            SymComponent aSymComponent = new SymComponent();
            VWRedactions.addComponentListener(aSymComponent);
            Pic.setIcon(VWImages.RedactionsImage);
            SymAction lSymAction = new SymAction();
            BtnGetDocs.addActionListener(lSymAction);
            BtnClear.addActionListener(lSymAction);
            BtnSaveAs.addActionListener(lSymAction);
            BtnReplace.addActionListener(lSymAction);
            CmbUsers.addActionListener(lSymAction);
            BtnPrint.addActionListener(lSymAction);
            repaint();
            doLayout();
            UILoaded=true;
        }
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnGetDocs)
                    BtnGetDocs_actionPerformed(event);
            else if (object == BtnReplace)
                    BtnReplace_actionPerformed(event);
            else if (object == BtnClear)
                    BtnClear_actionPerformed(event);
            else if (object == BtnSaveAs)
                    BtnSaveAs_actionPerformed(event);
            else if (object == CmbUsers)
                CmbUsers_actionPerformed(event);
            else if (object == BtnPrint)
                BtnPrint_actionPerformed(event);
        }
    }
//--------------------------------------------------------------
    VWPicturePanel picPanel = new VWPicturePanel(TAB_REDACTIONS_NAME, VWImages.RedactionsImage);
    javax.swing.JPanel VWRedactions = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnReplace = new VWLinkedButton(1);
    VWLinkedButton BtnGetDocs = new VWLinkedButton(1);
    VWLinkedButton BtnClear = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWRedactionsTable Table = new VWRedactionsTable();
    VWComboBox CmbUsers = new VWComboBox();
    EtchedBorder etchedBorder = new EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
//--------------------------------------------------------------
class SymComponent extends java.awt.event.ComponentAdapter
{
    public void componentResized(java.awt.event.ComponentEvent event)
    {
            Object object = event.getSource();
            if (object == VWRedactions)
                    VWRedactions_componentResized(event);
    }
}
//--------------------------------------------------------------
void VWRedactions_componentResized(java.awt.event.ComponentEvent event)
{
    if(event.getSource()==VWRedactions)
    {
        Dimension mainDimension=this.getSize();
        Dimension panelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        panelDimension.width=mainDimension.width-commandDimension.width;
        PanelTable.setSize(panelDimension);
        PanelCommand.setSize(commandDimension);
        PanelTable.doLayout();
        AdminWise.adminPanel.MainTab.repaint();
    }
}
//--------------------------------------------------------------       
    public void loadTabData(VWRoom newRoom)
    {
        if(newRoom.getConnectStatus()!=Room_Status_Connect)
        {
            Table.clearData();
            CmbUsers.removeAllItems();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        loadUsers(gCurRoom);
        setEnableMode(MODE_UNSELECTED);
    }
//--------------------------------------------------------------       
    public void unloadTabData()
    {
        Table.clearData();
        CmbUsers.removeAllItems();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
//--------------------------------------------------------------       
    private void loadUsers(int roomId)
{
    CmbUsers.removeAllItems();
    List userList=VWRedactionsConnector.getUsers();
    CmbUsers.addItem(LBL_REDACTIONCREATOR_NAME);
    if (userList==null || userList.size()==0) return ;
    CmbUsers.addItem(LBL_AllUSERS_NAME);
    for(int i=0;i<userList.size();i++)
        CmbUsers.addItem(((Creator)userList.get(i)).toString());
}
//--------------------------------------------------------------
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWSaveAsHtml.saveArrayInFile(Table.getReportData(),
                RedactionsColumnNames,RedactionsColumnWidths,
            null,(java.awt.Component)this,AdminWise.connectorManager.getString("VWRedactionsPanel.msg_1")+" "+ 
                (String)CmbUsers.getSelectedItem());
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
//--------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event)
    {
        new VWPrint(Table);
    }
//--------------------------------------------------------------       
    void BtnGetDocs_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            AdminWise.adminPanel.setWaitPointer();
            Table.clearData();
            try{
                if(CmbUsers.getSelectedIndex()==1)
                {
                    Table.addData(VWRedactionsConnector.getRedactionDocs(""));
                }
                else
                {
                    Table.addData(VWRedactionsConnector.getRedactionDocs
                        ((String)CmbUsers.getSelectedItem()));
                }
            }
            catch(Exception e){};
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        if(Table.getRowCount()>0)
        {
            Table.setRowSelectionInterval(0,0);
            setEnableMode(MODE_SELECT);
        }
        else
        {
            setEnableMode(MODE_UNSELECTED);
        }
    }
//--------------------------------------------------------------
    void BtnReplace_actionPerformed(java.awt.event.ActionEvent event)
    {
        String[] changeOptions=displayChangePasswordDlg();
        if(changeOptions==null) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            String userName="";
            if(CmbUsers.getSelectedIndex()!=1)
                userName=(String)CmbUsers.getSelectedItem();
            VWRedactionsConnector.replacePassword(Table.getDocIds(","),userName,changeOptions[0],changeOptions[1]);
            Table.clearData();
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
//--------------------------------------------------------------
    void BtnClear_actionPerformed(java.awt.event.ActionEvent event)
    {
        Table.deleteSelectedRows();
        setEnableMode(MODE_SELECT);
    }    
//--------------------------------------------------------------
    public void setEnableMode(int mode)
    {
        BtnGetDocs.setEnabled(true);
        BtnReplace.setEnabled(true);
        BtnClear.setEnabled(true);
        BtnSaveAs.setEnabled(true);
        BtnPrint.setEnabled(true);
        CmbUsers.setEnabled(true);
        switch (mode)
        {
            case MODE_UNSELECTED:
                if(CmbUsers.getSelectedIndex()==0)
                    BtnGetDocs.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                BtnReplace.setEnabled(false);
                BtnClear.setEnabled(false);
                break;
            case MODE_SELECT:
                if(Table.getRowCount()==0) 
                {
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                    BtnReplace.setEnabled(false);
                    BtnClear.setEnabled(false); 
                }
                break;
           case MODE_UNCONNECT:
                BtnGetDocs.setEnabled(false);
                BtnClear.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                BtnReplace.setEnabled(false);
                CmbUsers.setEnabled(false);
                gCurRoom=0;
                break;     
        }
    }
//--------------------------------------------------------------
    void CmbUsers_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(CmbUsers.getSelectedIndex()==0){
        	Table.m_data.clear();
        	Table.m_data.fireTableDataChanged();
        	setEnableMode(MODE_UNSELECTED);
        }else{
            setEnableMode(MODE_SELECT);
        }
    }
//--------------------------------------------------------------
    public static String[] displayChangePasswordDlg()
    {
        VWDlgChangePassword changePassDlg= new VWDlgChangePassword(
           AdminWise.adminFrame);
        changePassDlg.requestFocus();
        if(changePassDlg.getCancelFlag()) return null;
        String[] retValues=changePassDlg.getValues();
        changePassDlg.dispose();
        return retValues;
    }
//--------------------------------------------------------------
    private static int gCurRoom=0;
    private boolean UILoaded=false;
}