package ViewWise.AdminWise.VWPrincipal;


import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;

import com.computhink.common.Constants;
import com.computhink.common.Principal;
import com.computhink.common.ViewWiseErrors;

public class VWChangePasswordDlg extends JDialog implements VWConstant {

	private JPanel chandPwdPanel = new JPanel();

	private JLabel lblOldPassword = new JLabel();

	private JLabel lblNewPassword = new JLabel();

	private JLabel lblConfirmPassword = new JLabel();

	private JPasswordField txtOldPassword = new JPasswordField();

	private JPasswordField txtNewPassword = new JPasswordField();

	private JPasswordField txtConfirmPassword = new JPasswordField();
	 String languageLocale=AdminWise.getLocaleLanguage();
	private JButton btnChangePassword = new JButton();
	private JButton btnResetPassword = new JButton();
	private JButton btnCancel = new JButton();

	private Principal selectedPrincipal = null;

	private int retValue = 2;

	public VWChangePasswordDlg(Principal principal) {
		selectedPrincipal = principal;

		getContentPane().setBackground(AdminWise.getAWColor());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle(AdminWise.connectorManager.getString("VWChangePasswordDlg.Title")+"\""+selectedPrincipal.getName()+"\"");
		setModal(true);
		getContentPane().setLayout(null);
		setSize(350, 350);
		setResizable(false);
		if(languageLocale.equals("nl_NL")){
		setBounds(0, 0, 372+20, 177);
		}else
			setBounds(0, 0, 372, 177);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width/2 - getWidth()/2), (screenSize.height/2 - getHeight()/2));

		chandPwdPanel.setBorder(new javax.swing.border.TitledBorder(
				AdminWise.connectorManager.getString("VWChangePasswordDlg.Title")));
		chandPwdPanel.setLayout(null);
		chandPwdPanel.setBackground(AdminWise.getAWColor());
		if(languageLocale.equals("nl_NL")){
		chandPwdPanel.setBounds(6, 8, 355+20, 100);
		}else
			chandPwdPanel.setBounds(6, 8, 355, 100);
		getContentPane().add(chandPwdPanel);

/*		lblOldPassword.setBounds(10, 20, 150, 20);
		lblOldPassword.setText(TXT_OLDPASSWORD_NAME);
		chandPwdPanel.add(lblOldPassword);

		txtOldPassword.setBounds(125, 20, 220, 20);
		txtOldPassword.requestFocus();
		chandPwdPanel.add(txtOldPassword);
*/
		lblNewPassword.setBounds(10, 25, 150, 20);
		lblNewPassword.setText(TXT_NEWPASSWORD_NAME);
		chandPwdPanel.add(lblNewPassword);
		if(languageLocale.equals("nl_NL")){
		txtNewPassword.setBounds(125+20, 25, 220, 20);
		}else
			txtNewPassword.setBounds(125, 25, 220, 20);
		txtNewPassword.requestFocus();
		chandPwdPanel.add(txtNewPassword);

		lblConfirmPassword.setBounds(10, 60, 150, 20);
		lblConfirmPassword.setText(TXT_CONFIRMPASSWORD_NAME);
		chandPwdPanel.add(lblConfirmPassword);
		if(languageLocale.equals("nl_NL")){
		txtConfirmPassword.setBounds(125+20, 60, 220, 20);
		}else
			txtConfirmPassword.setBounds(125, 60, 220, 20);
		txtConfirmPassword.requestFocus();
		chandPwdPanel.add(txtConfirmPassword);


		//btnResetPassword.setBounds(127, 115, 73, 25);
		if(languageLocale.equals("nl_NL")){
		btnResetPassword.setBounds(8, 115, 73+35+15+10+30, 25);
		}else
			btnResetPassword.setBounds(8, 115, 73+35+15+10, 25);
		btnResetPassword.setText(BTN_RESET);
		btnResetPassword.setBackground(AdminWise.getAWColor());
		btnResetPassword.setIcon(VWImages.ClearIcon);
		btnResetPassword.setToolTipText(BTN_RESET);
		getContentPane().add(btnResetPassword);
		if(languageLocale.equals("nl_NL")){
		btnChangePassword.setBounds(207-7, 115, 73+15, 25);
		}
		else
		  btnChangePassword.setBounds(207, 115, 73, 25);
		btnChangePassword.setText(BTN_SAVE_NAME);
		btnChangePassword.setBackground(AdminWise.getAWColor());
		btnChangePassword.setIcon(VWImages.ConnectIcon);
		getContentPane().add(btnChangePassword);
		if(languageLocale.equals("nl_NL")){
			btnCancel.setBounds(287+3, 115, 73+15, 25);
		}
		else
		btnCancel.setBounds(287, 115, 73, 25);
		btnCancel.setText(BTN_CANCEL_NAME);
		btnCancel.setBackground(AdminWise.getAWColor());
		btnCancel.setIcon(VWImages.CloseIcon);
		getContentPane().add(btnCancel);

		SymAction lSymAction = new SymAction();
		btnChangePassword.addActionListener(lSymAction);
		btnResetPassword.addActionListener(lSymAction);
		btnCancel.addActionListener(lSymAction);

		setVisible(true);

	}

	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == btnChangePassword)
				BtnChangePassword_actionPerformed(event);
			else if(object==btnResetPassword)
				BtnRestPassword_actionPerformed(event);
			else if (object == btnCancel)
				BtnCancel_actionPerformed(event);
		}
	}
	

	void BtnRestPassword_actionPerformed(java.awt.event.ActionEvent event) {
		ResetPWD();
	}


	void BtnChangePassword_actionPerformed(java.awt.event.ActionEvent event) {
		ChangeUserPWD();
	}

	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
		closeDialog();
	}
	public void ResetPWD(){
		VWPrincipalConnector.resetUserPWD(selectedPrincipal,"", "","");
		closeDialog();
	}
	public void ChangeUserPWD() {
		String oldPassword = String.valueOf(txtOldPassword.getPassword());
		String newPassword = String.valueOf(txtNewPassword.getPassword());
		String confirmPassword = String.valueOf(txtConfirmPassword.getPassword());

/*		if (oldPassword == null || oldPassword.equals("")) { 
				JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("VWChangePasswordDlg.msg_1"));
				return;
			} 
		
		else*/
			if (((newPassword == null || newPassword.equals("")))) {
				JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("VWChangePasswordDlg.msg_5"));
				return;
			}
			else if (((newPassword == null || newPassword.equals(""))
				&& (confirmPassword == null || confirmPassword.equals(""))))
			{
				retValue = VWPrincipalConnector.ChangeUserPWD(selectedPrincipal,
						oldPassword, newPassword, confirmPassword);
			}
		
		else if (((newPassword != null || !newPassword.equals(""))
				&& (confirmPassword != null || !confirmPassword.equals(""))))
			{
			
			if (!newPassword.equals(confirmPassword))
			{
				JOptionPane.showMessageDialog(this,	AdminWise.connectorManager.getString("VWChangePasswordDlg.msg_2"));
		        return;
			}
			
			else if (newPassword.equals(confirmPassword)) {
				retValue = VWPrincipalConnector.ChangeUserPWD(
						selectedPrincipal, oldPassword, newPassword,
						confirmPassword);
			}
		}

//		AdminWise.printToConsole("value::::" + retValue);

		if (retValue == ViewWiseErrors.Error) {
			JOptionPane
					.showMessageDialog(this, AdminWise.connectorManager.getString("VWChangePasswordDlg.msg_3"));
		} else if (retValue == ViewWiseErrors.NoError) {
			JOptionPane
					.showMessageDialog(null, AdminWise.connectorManager.getString("VWChangePasswordDlg.msg_4"));
		}

		closeDialog();
	}

	private void closeDialog() {

		setVisible(false);
		dispose();
	}

}
