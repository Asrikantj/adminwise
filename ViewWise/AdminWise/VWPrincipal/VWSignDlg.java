package ViewWise.AdminWise.VWPrincipal;

import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import ViewWise.AdminWise.AdminWise;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import java.util.Vector;
import java.awt.Frame;
import java.util.prefs.*;
import java.awt.event.WindowEvent;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.util.*;
import com.gemtools.sigplus.SigPlus;
import java.awt.*;
import javax.swing.*;
import com.gemtools.sigplus.*;
import javax.comm.*;
import java.beans.*;
import java.awt.event.*;
import ViewWise.AdminWise.VWMessage.*;
import ViewWise.AdminWise.VWConstant;

import com.computhink.common.Principal;
import com.computhink.common.Signature;

public class VWSignDlg extends javax.swing.JDialog 
    implements VWConstant,VWMessageConstant {
    public VWSignDlg(Frame parent,Principal principal) {
        super(parent, LBL_SIGN_NAME + " - " + principal.getName(), true);
        this.principal = principal;
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.AdminWiseIcon.getImage());
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(VWOptions,BorderLayout.CENTER);
        setVisible(false);
        VWOptions.setLayout(null);
        VWOptions.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWOptions.add(PanelCommand);
        PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,12,152,132);

        PanelCommand.add(CmbSignType);
        CmbSignType.setBackground(java.awt.Color.white);
        CmbSignType.setBorder(etchedBorder);
        CmbSignType.setBounds(12,160,144, AdminWise.gBtnHeight);
        CmbSignType.setSelectedIndex(0);

        BtnSave.setText(BTN_SAVE_NAME);
        BtnSave.setActionCommand(BTN_SAVE_NAME);
        PanelCommand.add(BtnSave);
//        BtnSave.setBackground(java.awt.Color.white);
        BtnSave.setBounds(12,195,144, AdminWise.gBtnHeight);
        BtnSave.setIcon(VWImages.SaveIcon);
        BtnSave.setEnabled(false);
        BtnDel.setText(BTN_DEL_NAME);
        BtnDel.setActionCommand(BTN_DEL_NAME);
        PanelCommand.add(BtnDel);
//        BtnDel.setBackground(java.awt.Color.white);
        BtnDel.setBounds(12,230,144, AdminWise.gBtnHeight);
        BtnDel.setIcon(VWImages.DelIcon);
       	BtnDel.setEnabled(false);
        txtInfo.setBounds(12,265,144,66);
        txtInfo.setLineWrap(true);
        txtInfo.setWrapStyleWord(true);
        txtInfo.setBackground(java.awt.Color.white);
        txtInfo.setForeground(java.awt.Color.blue);
        txtInfo.setBorder(etchedBorder);
        PanelCommand.add(txtInfo);
        /*
        BtnOptions.setText(BTN_OPTIONS_NAME);
        BtnOptions.setActionCommand(BTN_OPTIONS_NAME);
        PanelCommand.add(BtnOptions);
        BtnOptions.setBackground(java.awt.Color.white);
        BtnOptions.setBounds(12,265,144, AdminWise.gBtnHeight);
        BtnOptions.setIcon(VWImages.OptionsIcon);
        */
        BtnClose.setText(BTN_CLOSE_NAME);
        BtnClose.setActionCommand(BTN_CLOSE_NAME);
        PanelCommand.add(BtnClose);
//        BtnClose.setBackground(java.awt.Color.white);
        BtnClose.setBounds(12,265,144, AdminWise.gBtnHeight);
        BtnClose.setIcon(VWImages.CloseIcon);
        PanelTable.setLayout(new BorderLayout());
        VWOptions.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        PanelTable.add(signPanel,BorderLayout.CENTER);
        PanelTable.setBorder(etchedBorder);

        signPanel.setVisible(false);
        signPanel.setBackground(java.awt.Color.white);
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gc = new GridBagConstraints();
        signPanel.setLayout(gbl);
        Panel controlPanel = new Panel();
        setConstraints(controlPanel, gbl, gc, 0, 0,
        GridBagConstraints.REMAINDER, 1, 0, 0,
        GridBagConstraints.CENTER,
        GridBagConstraints.NONE,0, 0, 0, 0);
        signPanel.add(controlPanel, gc);
        controlPanel.add(connectionChoice);
        controlPanel.add(connectionTablet);
        /*
    	 * JButton.setLabel() is reaplced with JButton.SetText()
    	 * as JButton.setLabel() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
    	 */
        BtnCapture.setText(BTN_CAPTURE_NAME);
        controlPanel.add(BtnCapture);        
        BtnOk.setText(BTN_OK_NAME);
        controlPanel.add(BtnOk);
        BtnClear.setText(BTN_CLEAR_NAME);
        controlPanel.add(BtnClear);
        initConnection();
        String drivername = "com.sun.comm.Win32Driver";
        try
        {
            CommDriver driver = (CommDriver) Class.forName(drivername).newInstance();
            driver.initialize();
        }
        catch (Throwable th)
        {
            /* Discard it */
        }
        try
        {
            ClassLoader cl = (com.gemtools.sigplus.SigPlus.class).getClassLoader();
            sigObj = (SigPlus)Beans.instantiate( cl, "com.gemtools.sigplus.SigPlus" );
            /*
            setConstraints(sigObj, gbl, gc, 0, 1,
            GridBagConstraints.REMAINDER, 1, 1, 1,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, 5, 0, 5, 0);
            signObjPanel.setLayout(new BorderLayout());
            signObjPanel.add(sigObj,BorderLayout.CENTER);
            signObjPanel.setBorder(etchedBorder);
            signObjPanel.add(txtInfo,BorderLayout.SOUTH);
            signPanel.add(signObjPanel, gc);
            */
            signObjPanel.setLayout(new BorderLayout());
            signObjPanel.add(sigObj,BorderLayout.CENTER);
            signObjPanel.setBorder(etchedBorder);
            signPanel.add(signObjPanel, gc);

            signObjPanel.setBounds(new Rectangle(sigObj.getTabletLCDXSize(),
                sigObj.getTabletLCDYSize()));
            /*
            setConstraints(signObjPanel, gbl, gc, 0, 1,
            GridBagConstraints.REMAINDER, 1, 1, 1,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, 5, 0, 5, 0);
            sigObj.clearTablet();
            */
            setConstraints(signObjPanel, gbl, gc, 0, 1,
            GridBagConstraints.REMAINDER, 1, 1, 1,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, 
                signObjPanel.getWidth()-sigObj.getTabletLCDXSize()/2, 
                signObjPanel.getHeight()-sigObj.getTabletLCDYSize()/2, 
                signObjPanel.getWidth()-sigObj.getTabletLCDXSize()/2,
                signObjPanel.getHeight()-sigObj.getTabletLCDYSize()/2);
            sigObj.clearTablet();
            connectionTablet.addItemListener(new ItemListener(){
        public void itemStateChanged(ItemEvent e){
            setTabletModel();
        }
	  });
	connectionChoice.addItemListener(new ItemListener(){
        public void itemStateChanged(ItemEvent e){   
            setTabletComPort();
            /*
            sigObj.setTabletState(0);
            sigObj.setTabletState(1);
             */
        }
	  });
       sigObj.addSigPlusListener( new SigPlusListener()
       {
            public void handleTabletTimerEvent( SigPlusEvent0 evt )
            {
            }
            public void handleNewTabletData( SigPlusEvent0 evt )
            {
            }
            public void handleKeyPadData( SigPlusEvent0 evt )
            {
            }
            });
            }
            catch(Exception e)
            {
            }
        SymComponent aSymComponent = new SymComponent();
        VWOptions.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.OptionsImage);

        SymAction lSymAction = new SymAction();
        BtnSave.addActionListener(lSymAction);
        BtnClose.addActionListener(lSymAction);
        BtnDel.addActionListener(lSymAction);
        BtnOptions.addActionListener(lSymAction);
        CmbSignType.addActionListener(lSymAction);
        BtnCapture.addActionListener(lSymAction);
        BtnOk.addActionListener(lSymAction);
        BtnClear.addActionListener(lSymAction);
        SymKey aSymKey = new SymKey();
        BtnSave.addKeyListener(aSymKey);
        BtnDel.addKeyListener(aSymKey);
        BtnOptions.addKeyListener(aSymKey);
        BtnClose.addKeyListener(aSymKey);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        getDlgOptions();
        setResizable(false);
        sigObj.setBackground(java.awt.Color.gray);
        setVisible(true);
    }
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(WindowEvent event) {
            Object object = event.getSource();
            if (object == VWSignDlg.this)
                BtnClose_actionPerformed(null);
        }
        /*
        public void windowDeactivated(WindowEvent e)
        {
            BtnClose_actionPerformed(null);
        }
         */
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnSave_actionPerformed(null);
            else if (event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnClose_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnSave)
                BtnSave_actionPerformed(event);
            else if (object == BtnDel)
                BtnDel_actionPerformed(event);
            else if (object == BtnOptions)
                BtnOptions_actionPerformed(event);
            else if (object == BtnClose)
                BtnClose_actionPerformed(event);
            else if (object == CmbSignType)
                CmbSignType_actionPerformed(event);
            else if (object == BtnCapture)
                BtnCapture_actionPerformed(event);
            else if (object == BtnOk)
                BtnOk_actionPerformed(event);
            else if (object == BtnClear)
                BtnClear_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWOptions = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnDel = new VWLinkedButton(0,true);
    VWLinkedButton BtnOptions = new VWLinkedButton(0,true);
    VWLinkedButton BtnClose = new VWLinkedButton(0,true);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    public static VWComboBox CmbSignType = new VWComboBox(SignType);
    EtchedBorder etchedBorder = new EtchedBorder
    (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
    JPanel signPanel=new JPanel();
    JPanel signObjPanel=new JPanel();
    SigPlus sigObj = null;
    JButton BtnCapture = new JButton();
    JButton BtnOk = new JButton();
    JButton BtnClear = new JButton();
    javax.swing.JTextArea txtInfo = new javax.swing.JTextArea();
        javax.swing.JScrollPane JSPSQL = new javax.swing.JScrollPane(txtInfo);
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWOptions)
                VWOptions_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWOptions_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWOptions) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event) {
        try
       {
            int retVal=0;
            AdminWise.adminPanel.setWaitPointer();
            String sgnString=getSgnString();
            if(sgnString==null || sgnString.equals("")){
            	VWMessage.showInfoDialog("Blank signature is not allowed!");
            	return;
            }
            certicitedSign.add(sgnString);
            if(CmbSignType.getSelectedIndex()==1)
            {
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    MSG_SAVESIG)==javax.swing.JOptionPane.YES_OPTION) 
                {
                    int ret=0;
                    if((ret=VWPrincipalConnector.addUserSign(getSgnString(),Signature.Authorized_Type))>=0)
                    {
                        txtInfo.setText(MSG_REGSUCCESS);
                        BtnClear_actionPerformed(null);
                    }
                    else
                    {
                        txtInfo.setText(MSG_REGFAIL);
                    }
                }
            }
            else if(CmbSignType.getSelectedIndex()==2)
            {
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    MSG_SAVETEMPLATE)==javax.swing.JOptionPane.YES_OPTION)
                {
                retVal=VWPrincipalConnector.addUserSignTemplate(certicitedSign);
                if(retVal<0)
                {
                    VWMessage.showMessage(null,
                        AdminWise.gConnector.ViewWiseClient.getErrorDescription(
                        retVal), VWMessage.DIALOG_TYPE);
                    return;
                }
                if(VWPrincipalConnector.addUserSign((String)certicitedSign.get(0),
                    Signature.Certified_Type)>=0)
                {
                    txtInfo.setText(MSG_REGTEMPLATESUCCESS);
                    BtnClear_actionPerformed(null);
                }
                else
                {
                    txtInfo.setText(MSG_REGTEMPLATEFAIL);
                }
                }
            }
       }
       catch(Exception e){}
       finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event) {
        sigObj.setTabletState(0);
        sigObj=null;
        saveDlgOptions();
        setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnDel_actionPerformed(java.awt.event.ActionEvent event) {
        try
       {
            if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    AdminWise.connectorManager.getString("VWSignDlg.msg_2"))!=javax.swing.JOptionPane.YES_OPTION) return;
            int ret = -1;
        	AdminWise.adminPanel.setWaitPointer();
            if(CmbSignType.getSelectedIndex()==1)
            {
                ret = VWPrincipalConnector.delUserSign(Signature.Authorized_Type);
            }
            else if(CmbSignType.getSelectedIndex()==2)
            {
                ret = VWPrincipalConnector.delUserSign(Signature.Certified_Type);
            }
            if(ret == 0)
            	txtInfo.setText("Signature deleted.");
       }
       catch(Exception e){}
       finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    void BtnOptions_actionPerformed(java.awt.event.ActionEvent event) {
    }
    //--------------------------------------------------------------------------
    void BtnCapture_actionPerformed(java.awt.event.ActionEvent event) {
        sigObj.setTabletState(0);
        sigObj.setTabletState(1);
        if(sigObj.getTabletState()==1)
        {
            txtInfo.setText((CmbSignType.getSelectedIndex()==2?getNextSignRequest():"Ready"));
            BtnOk.setEnabled(true);
            BtnClear.setEnabled(true);
            BtnOk.setSelected(true);
            BtnSave.setEnabled(false);
            sigObj.setBackground(java.awt.Color.white);
        }
        else
        {
            txtInfo.setText(LBL_DEVICENOTREADY);
        }
    }
        //----------------------------------------------------------------------
        void BtnOk_actionPerformed(java.awt.event.ActionEvent event) {
        try{
        	if(CmbSignType.getSelectedIndex()==1)
            {
            	String sgnString=getSgnString();
            	if(sgnString!=null && !sgnString.trim().equals(""))
            			txtInfo.setText(LBL_SIGCAPTURED);
                BtnSave.setEnabled(true);
                BtnSave.setSelected(true);
                BtnOk.setEnabled(false);
            }
            if(CmbSignType.getSelectedIndex()==2)
            {
                String sgnString=getSgnString();
                if(sgnString==null || sgnString.equals("")) return;
                certicitedSign.add(sgnString);
                if(certicitedSign.size()==7)
                {
                    txtInfo.setText(LBL_SIGTCAPTURED);
                    BtnSave.setEnabled(true);
                    BtnSave.setSelected(true);
                    BtnOk.setEnabled(false);
                }
                else
                {
                    getNextSignRequest();
                    txtInfo.setText(getNextSignRequest());
                    sigObj.clearTablet();
                }
                /*
                try
                {
                    if(certicitedSign.get(curIndex)!=null)
                    {
                        setSgnString((String)certicitedSign.get(curIndex));
                    }
                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                }
                BtnOk.setEnabled(true);
                */
                signPanel.repaint();
                signPanel.doLayout();
            }
        }
        catch(Exception e){
        };
        }
        //----------------------------------------------------------------------
        private String getNextSignRequest()
        {
            if(certicitedSign==null) certicitedSign=new Vector();
            return Signatures[certicitedSign.size()];
        }
        //----------------------------------------------------------------------
        public void BtnClear_actionPerformed(java.awt.event.ActionEvent event) {
            try{
                if(event!=null) txtInfo.setText(LBL_CLEARANDRESIGN);
                BtnSave.setEnabled(false);
                BtnOk.setEnabled(true);
                BtnOk.setSelected(true);
                certicitedSign.removeAllElements();
                sigObj.clearTablet();
            }
            catch(Exception e){};
        }
        //----------------------------------------------------------------------
        public String getSgnString()
        {
        	String signString = "";
        	signString = sigObj.getPdfString()==null?"":sigObj.getSigString();       
            return signString;
        }
        //----------------------------------------------------------------------
        public void setSgnString(String sigString)
        {
            /*
            sigObj.autoKeyStart();
            sigObj.setAutoKeyData("Encryption Data");
            sigObj.autoKeyFinish();
             */
            sigObj.setSigString(sigString);
            sigObj.fireNewData();
            /*
            sigObj.setEncryptionMode(1);
             */
        }
        //----------------------------------------------------------------------
    void CmbSignType_actionPerformed(java.awt.event.ActionEvent event) {
        try{
        	txtInfo.setText("");
            AdminWise.adminPanel.setWaitPointer();
            if(CmbSignType.getSelectedIndex()==0)
            {
                signPanel.setVisible(false);
                BtnDel.setEnabled(false);
                
            }
            else if(CmbSignType.getSelectedIndex()==1)
            {
                signPanel.setVisible(true);
                BtnCapture.setEnabled(true);
                BtnClear.setEnabled(false);
                BtnOk.setEnabled(false);
                certicitedSign=new Vector();
                BtnDel.setEnabled(this.principal.isHasASign());
            }
            else if(CmbSignType.getSelectedIndex()==2)
            {
                signPanel.setVisible(true);
                BtnSave.setEnabled(false);
                BtnCapture.setEnabled(true);
                BtnClear.setEnabled(false);
                BtnOk.setEnabled(false);
                certicitedSign=new Vector();
                BtnDel.setEnabled(this.principal.isHasCSign());
            }
            /*
            sigObj.clearTablet();
            sigObj.setTabletState(0);
            sigObj.setTabletState(1);
             */
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    private void saveDlgOptions() {
        if(!AdminWise.adminPanel.saveDialogPos) return;
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
        prefs.put("connectionChoice",connectionChoice.getSelectedItem());
        prefs.put("connectionTablet",connectionTablet.getSelectedItem());
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        connectionChoice.select(prefs.get("connectionChoice","HSB"));
        connectionTablet.select(prefs.get("connectionTablet","SignatureGem1X5"));
        setTabletComPort();
        setTabletModel();
        /*
        sigObj.setTabletState(0);
        sigObj.setTabletState(1);
         */
        setSize(600,400);
    }
    //--------------------------------------------------------------------------
    private void setTabletComPort()
        {
            if(connectionChoice.getSelectedItem() != "HSB"){
               sigObj.setTabletComPort(connectionChoice.getSelectedItem());
            }
            else{
               sigObj.setTabletComPort("HID1"); //properly set up HSB tablet
            }
        }
    //--------------------------------------------------------------------------
    private void setTabletModel()
        {
            if(connectionTablet.getSelectedItem() != "SignatureGemLCD4X3"){
               sigObj.setTabletModel(connectionTablet.getSelectedItem());
            }
            else{
               sigObj.setTabletModel("SignatureGemLCD4X3New"); //properly set up LCD4X3
            }
        }
    //--------------------------------------------------------------------------
    Choice connectionChoice = new Choice();   
        protected String[] connections = 
        { 
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "USB",
            "HSB"
        };
        Choice connectionTablet = new Choice();   protected String[] tablets = 
        {
           "SignatureGem1X5",
           "SignatureGem4X5",
           "SignatureGemLCD",
           "SignatureGemLCD4X3",
           "ClipGem",
           "ClipGemLGL",
        };
    //--------------------------------------------------------------------------
        private void initConnection()
        {
           for(int i = 0; i < connections.length; i++)
           {
                connectionChoice.add(connections[i]);
           }

           for(int i = 0; i < tablets.length; i++)
           {
                connectionTablet.add(tablets[i]);
           }
        }
    //--------------------------------------------------------------------------
        //Convenience method for GridBagLayout
        private void setConstraints(
        Component comp,
        GridBagLayout gbl,
        GridBagConstraints gc,
        int gridx,
        int gridy,
        int gridwidth,
        int gridheight,
        int weightx,
        int weighty,
        int anchor,
        int fill,
        int top,
        int left,
        int bottom,
        int right)
        {
            gc.gridx = gridx;
            gc.gridy = gridy;
            gc.gridwidth = gridwidth;
            gc.gridheight = gridheight;
            gc.weightx = weightx;
            gc.weighty = weighty;
            gc.anchor = anchor;
            gc.fill = fill;
            gc.insets = new Insets(top, left, bottom, right);
            gbl.setConstraints(comp, gc);
        }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    private Principal principal; 
    Vector certicitedSign=new Vector();
}