/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWSignTable<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWPrincipal;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.Comparator;
import javax.swing.table.TableColumnModel;
import java.util.Collections;
import javax.swing.event.TableModelEvent;
import ViewWise.AdminWise.VWTable.*;

public class VWSignTable extends JTable implements VWConstant
{
    protected VWSignTableData m_data;
    TableColumn[] ColModel=new TableColumn[2];
    static boolean[] visibleCol={true,true};
    public VWSignTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWSignTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ///setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k=0;k<VWSignTableData.m_columns.length; k++) {
            TableCellRenderer renderer;
            ColoredTableCellRenderer textRenderer=new ColoredTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWSignTableData.m_columns[k].m_alignment);
            renderer=textRenderer;
            VWSignRowEditor editor=new VWSignRowEditor(this);
            TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWSignTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        /*
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        setVisibleCols();
        */    
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                /*
                if (!listSelectionModel.isSelectionEmpty())
                    AdminWise.adminPanel.SignPanel.setEnableMode(MODE_SELECT);
                else
                    AdminWise.adminPanel.SignPanel.setEnableMode(MODE_UNSELECTED);
                 */
            }
        });
        setRowHeight(TableRowHeight);
        
        setTableResizable();
    }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    private void setVisibleCols() {
        TableColumnModel model=getColumnModel();
        
        int count =visibleCol.length;
        int i=0;
        while(i<count) {
            if (!visibleCol[i]) {
                TableColumn column = model.getColumn(i);
                model.removeColumn(column);
                count--;
            }
            else {
                i++;
            }
        }
        if(count==0)return;
        Dimension tableWith = getPreferredScrollableViewportSize();
        int colWidth=(int)Math.round(tableWith.getWidth()/count);
        for (i=0;i<count;i++) {
            model.getColumn(i).setPreferredWidth(colWidth);
        }
        setColumnModel(model);
    }
    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWSignTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWSignTable_LeftMouseClicked(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWSignTable_LeftMouseClicked(java.awt.event.MouseEvent event) 
    {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    void VWSignTable_RightMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;
         
        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
         */
    }
    //--------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    public void addData(List list) {
        m_data.setData(list);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void addData(Vector data) {
        m_data.setData(data);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public Vector getData() {
        return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void insertData(Vector data) {
        /*
        for(int i=0;i<data.size();i++) {
            m_data.insert((Sign)data.get(i));
        }
        m_data.fireTableDataChanged();
         */
    }
    //--------------------------------------------------------------------------
    public void insertData() {
        /*
        m_data.insert(Sign);
        m_data.fireTableDataChanged();
         */
    }
    //--------------------------------------------------------------------------
    public Object getRowData(int rowNum) {
        return m_data.getRowData(rowNum);
    }
    /*
    //--------------------------------------------------------------------------
    public Sign getSign() {
        Sign Sign=m_data.getRowData(getSelectedRow());
        return Sign;
    }
    //--------------------------------------------------------------------------
    public Sign getSign(int rowNum) {
        Sign Sign=m_data.getRowData(rowNum);
        return Sign;
    }
    //--------------------------------------------------------------------------
     */
    public void clearData() {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
}
class SignColumnData {
    public String  m_title;
    float m_width;
    int m_alignment;
    
    public SignColumnData(String title, float width, int alignment) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
    }
}
//--------------------------------------------------------------------------
class VWSignTableData extends AbstractTableModel {
    public static final SignColumnData m_columns[] = {
        new SignColumnData(VWConstant.SignColumnNames[0],0.1F,JLabel.LEFT ),
        new SignColumnData(VWConstant.SignColumnNames[1],0.9F, JLabel.LEFT)
    };
    public static final int COL_NAME = 0;
    public static final int COL_TYPE = 1;
    
    protected VWSignTable m_parent;
    protected Vector m_vector;
    protected int m_sortCol = 0;
    protected boolean m_sortAsc = true;
    
    public VWSignTableData(VWSignTable parent) {
        m_parent = parent;
        m_vector = new Vector();
    }
    //--------------------------------------------------------------------------
    public void setData(List data) {
        /*
        m_vector.removeAllElements();
        if (data==null) return;
        int count =data.size();
        for(int i=0;i<count;i++)
            m_vector.addElement(new Sign((String)data.get(i)));
         */
    }
    //--------------------------------------------------------------------------
    public void setData(Vector data) {
        /*
        m_vector.removeAllElements();
        int count =data.size();
        for(int i=0;i<count;i++) {
            m_vector.addElement((Sign)data.get(i));
        }
         */
    }
    //--------------------------------------------------------------------------
    public Vector getData() {
        return m_vector;
    }
    //--------------------------------------------------------------------------
    public Object getRowData(int rowNum) {
        return null;
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return m_vector==null ? 0 : m_vector.size();
    }
    //--------------------------------------------------------------------------
    public int getColumnCount() {
        return m_columns.length;
    }
    //--------------------------------------------------------------------------
    public String getColumnName(int column) {
        String str = m_columns[column].m_title;
            return str;
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
        return true;
    }
    //--------------------------------------------------------------------------
    public Object getValueAt(int nRow, int nCol) {
    /*
        if (nRow < 0 || nRow>=getRowCount())
            return "";
        Sign row = (Sign)m_vector.elementAt(nRow);
        switch (nCol) {
        case COL_NAME:
            if(row.isAdmin())
                    return new ColorData(row.getName(),ColorData.RED);
            return row.getName();
        case COL_TYPE:
            if(row.getType()==Sign.GROUP_TYPE)
                return "Group";
            return "User";
        }
     */
        return "";
    }
    //--------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
        /*
        if (nRow < 0 || nRow >= getRowCount())
            return;
        Sign row = (Sign)m_vector.elementAt(nRow);
        String svalue = value.toString();
        switch (nCol) {
        case COL_NAME:
            row.setName(svalue);
            break;
        case COL_TYPE:
            row.setType(VWUtil.to_Number(svalue));
            break;
        }
         */
    }
    //--------------------------------------------------------------------------
    public void clear() {
        m_vector.removeAllElements();
    }
    //--------------------------------------------------------------------------
    public void insert() {
    }
    //--------------------------------------------------------------------------
    public boolean remove(int row){
        if (row < 0 || row >= m_vector.size())
            return false;
        m_vector.remove(row);
        return true;
    }
    //--------------------------------------------------------------------------
}