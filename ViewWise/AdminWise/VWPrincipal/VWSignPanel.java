package ViewWise.AdminWise.VWPrincipal;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import com.gemtools.sigplus.*;
import javax.comm.*;
import java.io.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import ViewWise.AdminWise.VWConstant;

public class VWSignPanel extends Panel implements Runnable,VWConstant
   {
        SigPlus sigObj = null;
        Thread  eventThread;

	public VWSignPanel()
        {
            GridBagLayout gbl = new GridBagLayout();
            GridBagConstraints gc = new GridBagConstraints();
            setLayout(gbl);
            Panel controlPanel = new Panel();
            setConstraints(controlPanel, gbl, gc, 0, 0,
            GridBagConstraints.REMAINDER, 1, 0, 0,
            GridBagConstraints.CENTER,
            GridBagConstraints.NONE,0, 0, 0, 0);
            add(controlPanel, gc);

            controlPanel.add(connectionChoice);
            controlPanel.add(connectionTablet);

            Button startButton = new Button(BTN_START_NAME);
            controlPanel.add(startButton);

            Button stopButton = new Button(BTN_STOP_NAME);
            controlPanel.add(stopButton);

            Button clearButton = new Button(BTN_CLEAR_NAME);
            controlPanel.add(clearButton);

            ///controlPanel.add(txtPath);

            initConnection();
            String drivername = "com.sun.comm.Win32Driver"; 
            try 
                { 
                CommDriver driver = (CommDriver) Class.forName(drivername).newInstance(); 
                driver.initialize();
                } 
            catch (Throwable th) 
                {
                /* Discard it */
                } 
            try
                {
                ClassLoader cl = (com.gemtools.sigplus.SigPlus.class).getClassLoader();
                sigObj = (SigPlus)Beans.instantiate( cl, "com.gemtools.sigplus.SigPlus" );
            setConstraints(sigObj, gbl, gc, 0, 1,
            GridBagConstraints.REMAINDER, 1, 1, 1,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, 5, 0, 5, 0);
            add(sigObj, gc);
            sigObj.setSize(100,100);
            sigObj.clearTablet();
	   startButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
			    sigObj.setTabletState(0);
			    sigObj.setTabletState(1);
		   }
	  });
	  stopButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
			    sigObj.setTabletState(0);
		   }
	  });
	  clearButton.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
			    sigObj.clearTablet();
		   }
	  });
          /*
	  saveSigButton.addActionListener(new ActionListener(){
	     public void actionPerformed(ActionEvent e){
	        boolean blnExport=false;
	        String path=txtPath.getText();
                int pathlength=path.length();
                if(pathlength!=0)
                {
		   sigObj.autoKeyStart();
		   sigObj.setAutoKeyData("Sample Encryption Data");
		   sigObj.autoKeyFinish();
                   sigObj.setEncryptionMode(1);
	           blnExport = sigObj.exportSigFile(txtPath.getText());
                   if (blnExport==false)
                     {
                        System.out.println("Error writing SIG file");
                     }
                 }
                 else
                 {
                    System.out.println("Please type in full path information to save file");
		 }
              }
	  });
           */
	 connectionTablet.addItemListener(new ItemListener(){
		  public void itemStateChanged(ItemEvent e){
			    
                        if(connectionTablet.getSelectedItem() != "SignatureGemLCD4X3"){
                           sigObj.setTabletModel(connectionTablet.getSelectedItem());
                        }
                        else{
                           sigObj.setTabletModel("SignatureGemLCD4X3New"); //properly set up LCD4X3
                        }        
		  }
	  });
	 connectionChoice.addItemListener(new ItemListener(){
		  public void itemStateChanged(ItemEvent e){   
                        if(connectionChoice.getSelectedItem() != "HSB"){
  	                   sigObj.setTabletComPort(connectionChoice.getSelectedItem());
                        }
                        else{
                           sigObj.setTabletComPort("HID1"); //properly set up HSB tablet
                        }           
		  }
	  });
                sigObj.addSigPlusListener( new SigPlusListener()
                {
                public void handleTabletTimerEvent( SigPlusEvent0 evt )
                {
                }

                public void handleNewTabletData( SigPlusEvent0 evt )
                {
                }

                public void handleKeyPadData( SigPlusEvent0 evt )
                {
                }
                });
                /*
        		 * JDialog.show() is replaced with JDialog.setVisible() 
        		 * as JDialog.show() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
        		 */
                setVisible(true);
                sigObj.setTabletModel("SignatureGem1X5");
                sigObj.setTabletComPort("COM1");
                eventThread = new Thread(this);
                eventThread.start();
                }
        catch ( Exception e )
        {
        return;
        }
        }
        public void run()
        {
        try
           {
           while ( true )
            {
                Thread.sleep(100);
            }
           }
        catch (InterruptedException e)
           {
           }
        }
        TextField txtPath = new TextField("C:\\test.sig", 30);
        Choice connectionChoice = new Choice();   
        protected String[] connections = 
        {
           "COM1", 
           "COM2", 
           "COM3", 
           "COM4",
           "USB", 
           "HSB",  
        };
        Choice connectionTablet = new Choice();   protected String[] tablets = 
        {
           "SignatureGem1X5",
           "SignatureGem4X5",
           "SignatureGemLCD",
           "SignatureGemLCD4X3",
           "ClipGem",
           "ClipGemLGL",
        };
        private void initConnection()
        {
           for(int i = 0; i < connections.length; i++)
           {
                connectionChoice.add(connections[i]);
           }

           for(int i = 0; i < tablets.length; i++)
           {
                connectionTablet.add(tablets[i]);
           }

        }

        //Convenience method for GridBagLayout
        private void setConstraints(
        Component comp,
        GridBagLayout gbl,
        GridBagConstraints gc,
        int gridx,
        int gridy,
        int gridwidth,
        int gridheight,
        int weightx,
        int weighty,
        int anchor,
        int fill,
        int top,
        int left,
        int bottom,
        int right)
        {
                gc.gridx = gridx;
                gc.gridy = gridy;
                gc.gridwidth = gridwidth;
                gc.gridheight = gridheight;
                gc.weightx = weightx;
                gc.weighty = weighty;
                gc.anchor = anchor;
                gc.fill = fill;
                gc.insets = new Insets(top, left, bottom, right);
                gbl.setConstraints(comp, gc);
        }
        public void saveSig(){
              try {

                       sigObj.setTabletState(0);
                       sigObj.setImageJustifyMode(5);
                       sigObj.setImagePenWidth(10);
                       sigObj.setImageXSize(1000);
                       sigObj.setImageYSize(350);
                       BufferedImage sigImage = sigObj.sigImage();
                       int w = sigImage.getWidth(null);
                       int h = sigImage.getHeight(null);
                       int[] pixels = new int[(w * h) * 2];

                       sigImage.setRGB(0, 0, 0, 0, pixels, 0, 0);
                       FileOutputStream fos = new
                       FileOutputStream("c:\\sig.jpg");
                       
                       /*
                        * Commented because the Jpeg pacakage is removed in JDK1.7 and the class has not called by any functionality
                        * Gurumurthy.T.S 01/02/2013, CV8B5-001
                        */
                       /*JPEGImageEncoder jpeg =
                       JPEGCodec.createJPEGEncoder(fos);
                       jpeg.encode(sigImage);*/
                       fos.close();

                       }
                    catch (Throwable th) {
                            th.printStackTrace();
                    }
             };
	     public void loadSig(){
	        boolean blnImport=false;
	        String path=txtPath.getText();
                int pathlength=path.length();
                if(pathlength!=0)
                {
 	           sigObj.autoKeyStart();
		   sigObj.setAutoKeyData("Sample Encryption Data");
		   sigObj.autoKeyFinish();
                   sigObj.setEncryptionMode(1);
		   blnImport = sigObj.importSigFile(txtPath.getText());
                if (blnImport==false)
                     {
                        System.out.println("Error reading SIG file");
                     }
                 }
                 else
                 {
                    System.out.println("Please type in full path information to load file");
		 }
	      };
            }