package ViewWise.AdminWise.VWPrincipal;


import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDlgSaveLocation;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.Principal;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWCUtil;
/**
 *  Enhancement :-security report for user/group to confirm access CV10 Date:- 9-2-2016
 * @author madhavan.b
 * 
 */
public class SecurityReportDlg extends JDialog implements VWConstant {

	private JPanel secruityPanel = new JPanel();


	private JLabel lblsecurityLocation = new JLabel();
	//private JLabel lblreportLocation = new JLabel();
	private JLabel lblStatus = new JLabel();
	private JTextField txtsecurityLocation = new JTextField();
	//private JTextField txtreportLocation = new JTextField();
	public static boolean active = false;
	public VWDlgReportSaveLocation dlgLocation = null;
	VWLinkedButton btnNodeLocation = new VWLinkedButton(2);
	//VWLinkedButton btnReportLocation = new VWLinkedButton(2);
	private JButton btnGenerateReport = new JButton();
	private JButton btnCancel = new JButton();
	private Principal selectedPrincipal = null;
	private JCheckBox chkSelectDoc = new JCheckBox(AdminWise.connectorManager.getString("VWSecuirtyReport.chkIncludeDoc"));
	public int checkBoxValue = 0;
	int reportNodeId=-1;
    String languageLocale=AdminWise.getLocaleLanguage();
	public SecurityReportDlg(Principal principal) {
		selectedPrincipal = principal;
		setCurrentLocation();
		getContentPane().setBackground(AdminWise.getAWColor());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle(AdminWise.connectorManager.getString("SecurityReportDlg.Title")+" "+"For"+" "+"\""+selectedPrincipal.getName()+"\"");
		setModal(true);
		getContentPane().setLayout(null);
		setSize(470, 450);
		setResizable(false);

		setBounds(0,0,470,170);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width/2 - getWidth()/2), (screenSize.height/2 - getHeight()/2));

		secruityPanel.setBorder(new javax.swing.border.TitledBorder(
				AdminWise.connectorManager.getString("SecurityReportDlg.Title")));
		secruityPanel.setLayout(null);
		secruityPanel.setBackground(AdminWise.getAWColor());
		secruityPanel.setBounds(12, 10, 440, 80);
		getContentPane().add(secruityPanel);


		lblsecurityLocation.setBounds(20,35, 150,20);
		lblsecurityLocation.setText(TXT_SECURITY_LOCATION);
		secruityPanel.add(lblsecurityLocation);

		/*lblreportLocation.setBounds(20,62, 150,20);
		lblreportLocation.setText(TXT_REPORT_LOCATION);
		secruityPanel.add(lblreportLocation);*/


		//lblStatus.setBounds(20, 115, 150,20);
		//getContentPane().add(lblStatus);

		txtsecurityLocation.setBounds(110,35,275,20);
		txtsecurityLocation.requestFocus();
		txtsecurityLocation.setEditable(false);
		txtsecurityLocation.setEnabled(false);
		secruityPanel.add(txtsecurityLocation);


		/*txtreportLocation.setBounds(110,63,275,20);
		txtreportLocation.requestFocus();
		txtreportLocation.setEditable(false);
		txtreportLocation.setEnabled(false);
		secruityPanel.add(txtreportLocation);*/

		btnNodeLocation.setIcon(VWImages.BrowseRouteImage);
		btnNodeLocation.addActionListener(new SymAction());
		btnNodeLocation.setBounds(400,35,20,20);
		secruityPanel.add(btnNodeLocation);
		btnNodeLocation.setVisible(true);

		/*btnReportLocation.setIcon(VWImages.BrowseRouteImage);
		btnReportLocation.addActionListener(new SymAction());
		btnReportLocation.setBounds(400,63,20,20);
		secruityPanel.add(btnReportLocation);
		btnReportLocation.setVisible(true);*/
		if(languageLocale.equals("nl_NL")){
		btnGenerateReport.setBounds(245+20+33-30, 100, 73+15, 25);
		}else
			btnGenerateReport.setBounds(245+20+33, 100, 73, 25);
		btnGenerateReport.setText(BTN_GENERATE_REPORT);
		btnGenerateReport.setBackground(AdminWise.getAWColor());
		btnGenerateReport.setIcon(VWImages.OkIcon);
		getContentPane().add(btnGenerateReport);
		btnGenerateReport.addActionListener(new SymAction());
		if(languageLocale.equals("nl_NL")){
		btnCancel.setBounds(325+20+33-15, 100, 73+15, 25);
		}else
			btnCancel.setBounds(325+20+33, 100, 73, 25);
		btnCancel.setText(BTN_CANCEL_NAME);
		btnCancel.setBackground(AdminWise.getAWColor());
		btnCancel.setIcon(VWImages.CloseIcon);
		getContentPane().add(btnCancel);
		btnCancel.addActionListener(new SymAction());
		if(languageLocale.equals("nl_NL")){
		chkSelectDoc.setBounds(20,100,120+30, 25);
		}
		else
			chkSelectDoc.setBounds(20,100,120, 25);
		chkSelectDoc.setBackground(secruityPanel.getBackground());
		chkSelectDoc.addActionListener(new SymAction());
		getContentPane().add(chkSelectDoc);

		setVisible(true);

	}
	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width, this.getSize().height);
		setLocation(point);
		setResizable(false);
	}
	public void setCurrentLocation(VWDlgReportSaveLocation dlg){
		Point point = AdminWise.adminFrame.getCurrentLocation(dlg.getSize().width, dlg.getSize().height);
		dlg.setLocation(point);
	}
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == btnNodeLocation)
				BtnNodeLocation_actionPerformed(event);
			/*if(object==btnReportLocation)
				BtnReportLocation_actionPerformed(event);*/
			if(object==btnCancel)
				BtnCancel_actionPerformed(event);
			if(object==btnGenerateReport)
				BtnGenerateReport_actionPerformed(event);
			if(object==chkSelectDoc)
				chkSelect_actionPerformed(event);
		}
	}

	void chkSelect_actionPerformed(ActionEvent event){
		if (chkSelectDoc.isSelected()) {
			checkBoxValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			checkBoxValue=0;
		}
	}

	/*void BtnReportLocation_actionPerformed(ActionEvent event){
		JFileChooser chooser = new JFileChooser();
		VWFileFilter filter = new VWFileFilter();
		filter.addExtension("html");
		//filter.addExtension("xml");
		filter.setDescription("Html Files");
		chooser.setCurrentDirectory(new File("c:\\"));
		chooser.setFileFilter(filter);
		//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = chooser.showSaveDialog(this);
		if(returnVal==JFileChooser.APPROVE_OPTION){
			String path=chooser.getSelectedFile().getPath();
			if(!path.toUpperCase().endsWith(".HTML") && !path.toUpperCase().endsWith(".XML")) path+=".html";
			this.txtreportLocation.setText(path);
		}	

	}*/

	void BtnNodeLocation_actionPerformed(java.awt.event.ActionEvent event) {
		if (dlgLocation != null) {
			active = false;
			dlgLocation.loadDlgData();
			setCurrentLocation(dlgLocation);
			dlgLocation.setVisible(true);
		} else {
			active = false;
			dlgLocation = new VWDlgReportSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWEndIconProperties.dlgLocation"));
			setCurrentLocation(dlgLocation);
			dlgLocation.setVisible(true);
		}
		if (dlgLocation.cancelFlag || !dlgLocation.cancelFlag ){
			active = true;
		}
		if (dlgLocation.cancelFlag)
			return;
		txtsecurityLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
		reportNodeId = dlgLocation.tree.getSelectedNodeId();
	}

	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
		closeDialog();
	}
	public void ResetPWD(){
		VWPrincipalConnector.resetUserPWD(selectedPrincipal,"", "","");
		closeDialog();
	}

	
	/**
	 * CV10.1 Enhancement: This method is changed for saving the report in DTC
	 * @author apurba.m
	 * @param event
	 */
	public void BtnGenerateReport_actionPerformed(ActionEvent event){		
		String homePath= System.getenv("APPDATA");//loadSaveDialog(parent);
		String documentNameinDTC="Users_and_Groups_Report_"+System.currentTimeMillis();
		String htmlReportName=documentNameinDTC+".html";
		String folderPath=homePath+"\\CV Reports"+"\\";
		AdminWise.printToConsole("homePath Users and Groups ::::"+homePath);
		String pathLocation= folderPath+htmlReportName;
		AdminWise.printToConsole("pathLocation for Users and Groups ::::	"+pathLocation);
		AdminWise.printToConsole("folderPath for Users and Groups ::::	"+folderPath);

		DocType dt= new DocType("CVReports");
		dt.setName("CVReports");
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		int gCurRoom = room.getId();
		Session session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
		int sid= session.sessionId;
		AdminWise.printToConsole("sid Inside SecurityReportDlg :::: "+sid);
		Vector docTypeInfo= new Vector();
		/**
		 * isDocTypeFound method is calling for checking whether "CVReports" doctype exists
		 * or not
		 */
		int docTypeId=AdminWise.gConnector.ViewWiseClient.isDocTypeFound(sid,dt);
		AdminWise.printToConsole("docTypeId in AdminWise.SecurityReportDlg :::: "+docTypeId);

		String panelName= "Users and Groups Panel";
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String[] monthName = {"January", "February",
				"March", "April", "May", "June", "July",
				"August", "September", "October", "November",
		"December"};
		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		int day = Calendar.getInstance().get(Calendar.DATE);
		String dtcPath="CVReports\\"+panelName+"\\"+year+"\\"+month+"\\"+day;
		AdminWise.printToConsole("dtcPath :::: "+dtcPath);

		if(docTypeId > 0){
			AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(sid, docTypeId, docTypeInfo);
			AdminWise.printToConsole("docTypeInfo in AdminWise.SecurityReportDlg ::::"+docTypeInfo);

			if(docTypeInfo.size()>0 && docTypeInfo!=null){
				StringTokenizer st= new StringTokenizer(docTypeInfo.get(0).toString(), "\t");
				int newDocypeId= Integer.parseInt(st.nextToken());
				String docTypeName= st.nextToken();
				String indexName= st.nextToken();
				Index idxName= new Index(indexName);
				idxName.setName(indexName);
				int indexId=AdminWise.gConnector.ViewWiseClient.getIndexId(sid, idxName);
				AdminWise.printToConsole("indexId in AdminWise.VWAddCustomIndices ::::"+indexId);
				if(indexId > 0 && indexName.equals("ReportName")){
					if(generateReport(pathLocation)) {
						int docCreated= AdminWise.gConnector.ViewWiseClient.cvCreateNodePath(sid, "Users and Groups Panel", pathLocation, htmlReportName, folderPath, documentNameinDTC);
						AdminWise.printToConsole("docCreated Successfully and returned to Adminwise :::: "+docCreated);
						if(docCreated == 0){
							Vector templateNameListVector = new Vector();
							String srcFile = "";

							File folder = new File(folderPath.trim());
							AdminWise.printToConsole("Folder Inside VWAddCustomIndices ::::" + folder);

							File[] listOfFiles = folder.listFiles();
							AdminWise.printToConsole("listOfFiles Inside VWAddCustomIndices :::: " + listOfFiles);

							String ext = null; // FilenameUtils.getExtension("/path/to/file/foo.txt");

							for (int i = 0; i < listOfFiles.length; i++) {
								if (listOfFiles[i].isFile()) {
									AdminWise.printToConsole("File ::::" + listOfFiles[i].getName());
									if (listOfFiles[i].getName().endsWith(".html") || listOfFiles[i].getName().endsWith(".HTML")) {
										templateNameListVector.add(listOfFiles[i].getName());
									}
								}
							}
							if(templateNameListVector!=null&&templateNameListVector.size()>0){
								for(int j=0; j<templateNameListVector.size(); j++){
									File reportFile= new File(folderPath+templateNameListVector.get(j).toString());
									AdminWise.printToConsole("reportFile :::: "+reportFile);
									if(reportFile.exists()){
										try {
											AdminWise.printToConsole("File Deleted !!!! "+Files.deleteIfExists(reportFile.toPath()));
										} catch (IOException e) {
											AdminWise.printToConsole("EXCEPTION WHILE DELETING FILE FROM APP DATA LOCATION :::: "+e.getMessage());
											e.printStackTrace();
										}
									}
								}
							}
							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReport.reportCreateSuccess")+" "+"\""+dtcPath+"\"");
						} else if(docCreated == -171){
							JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
						} else if(docCreated == -172){
							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
						} else if(docCreated == -1) {
							JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.createDocGeneralError"));
						}
					} else {
						JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.usersAndGroupsReportGenerationFailed"));
					}

				} else {
					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("customReportError.indexNotExist"));
				}
			}
		} else {
			JOptionPane.showMessageDialog(null,"CVReports  "+AdminWise.connectorManager.getString("customReportError.documentTypeNotExist"));
		}	    	
	}
	/**
	 *  Enhancement :-security report for user/group to confirm access CV10 Date:- 9-2-2016
	 *  Method used to Generate Security report
	 */
	public boolean generateReport(String pathLocation){		
		try{
			int rowCount=0;
			if(txtsecurityLocation.getText().length()<=0){
				JOptionPane.showMessageDialog(this, "Please select node location");
				return false;
			}/*else if(txtreportLocation.getText().length()<=0){
				JOptionPane.showMessageDialog(this, "Please select report location");
				return false;
			}*/

			lblStatus.setText("Generating Report...");
			Vector reportData=new Vector();
			VWPrincipalConnector.generateNodeSecurityReport(selectedPrincipal.getName(), reportNodeId, checkBoxValue,reportData);
			AdminWise.printToConsole("ReportData::::"+reportData);

			Vector htmlContents = new Vector();
			String err = "";
			String title = "Contentverse " + " Security Report";

			htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
			htmlContents.add("<html>");
			htmlContents.add("<head>");
			htmlContents.add("<title>" + title + "</title>");
			htmlContents.add("<style>");
			htmlContents.add("TH {");
			htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#336699'; font-weight:bold");
			htmlContents.add("}");
			htmlContents.add("TD {");
			htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
			htmlContents.add("}");
			htmlContents.add("</style>");
			htmlContents.add("</head>");
			htmlContents.add("<body BGCOLOR='#F7F7E7'>");
			htmlContents.add("<table border=1 width=100% cellspacing=0 cellpadding=0 ><tr><td align=center><font size='4'><b>"+ title + "</b></font></td></tr></table>");
			htmlContents.add("<p></p>");    


			if(reportData.size()<=0) return false;
			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
			StringTokenizer st1 = new StringTokenizer( (String) reportData.elementAt(0), "\t");
			String nodeId1 = st1.nextToken();
			String roomLocation=txtsecurityLocation.getText();
			//String roomName=roomLocation.substring(0,roomLocation.indexOf("\\"));
			String roomName=room.getName();
			String path1 =room.getServer()+"\\"+roomName+"\\"+st1.nextToken();
			String inherited1=st1.nextToken();
			String nodetype1=st1.nextToken();
			String user1=st1.nextToken();
			htmlContents.add("<table border=1><tr><td><font size='2'>Location : </font></td><td><font size='2'>"+path1+"</td></tr></table>");
			htmlContents.add("<table border=1><tr><td><font size='2'>User Name : </font></td><td><font size='2'>"+user1+"</td></tr></table>");
			htmlContents.add("<table border = 1 width=100% cellspacing=0 cellpadding=0>");
			htmlContents.add("<tr><td  BGCOLOR=#cccccc>Node Path</td><td  BGCOLOR=#cccccc>Node Type</td><td  BGCOLOR=#cccccc>Navigate</td><td  BGCOLOR=#cccccc>View</td><td  BGCOLOR=#cccccc>Delete</td>"
					+ "<td BGCOLOR=#cccccc>Share</td><td  BGCOLOR=#cccccc>Modify</td><td  BGCOLOR=#cccccc>Create</td>"
					+ "<td BGCOLOR=#cccccc>Assign</td><td  BGCOLOR=#cccccc>Launch</td><td  BGCOLOR=#cccccc>Export</td>"
					+ "<td BGCOLOR=#cccccc>Print</td><td  BGCOLOR=#cccccc>Mail</td><td  BGCOLOR=#cccccc>SendTo</td>"
					+ "<td BGCOLOR=#cccccc>CheckInOut</td></tr>");

			for (int index = 0; index < reportData.size(); index++) {
				try{

					StringTokenizer st = new StringTokenizer( (String) reportData.elementAt(index), "\t");
					String nodeId = st.nextToken();
					String path = st.nextToken();
					String inherited=st.nextToken();
					String nodetype=st.nextToken();
					String user=st.nextToken();

					htmlContents.add("<tr><td >"+room.getServer()+"\\"+roomName+"\\"+path+ "</td><td >"+nodetype+ "</td><td >"+st.nextToken()+ "</td><td >&nbsp;"+st.nextToken()+ "</td>"
							+ "<td >&nbsp;"+st.nextToken()+ "</td><td >"+st.nextToken()+ "</td><td >&nbsp;"+st.nextToken()+ "</td>"
							+ "<td >&nbsp;"+st.nextToken()+ "</td><td >"+st.nextToken()+ "</td><td >&nbsp;"+st.nextToken()+ "</td>"
							+ "<td >&nbsp;"+st.nextToken()+ "</td><td >"+st.nextToken()+ "</td><td >&nbsp;"+st.nextToken()+ "</td>"
							+ "<td >&nbsp;"+st.nextToken()+ "</td><td >"+st.nextToken()+ "</td></tr>");
					rowCount=rowCount+1;

					htmlContents.add("<p></p>");
				}catch(Exception ex){
					err = "Error generating report - " + ex.getMessage();
					AdminWise.printToConsole("Error generating report"+err);

				}	
			}
			htmlContents.add("</table>");
			htmlContents.add("<table><tr><td>Legend : </td><td>G - Granted, </td><td>D - Denied </td></tr></table>");
			htmlContents.add("<table><tr><td>Row Count : </td><td>"+rowCount+"</td></tr></table>");
			htmlContents.add("<table><tr><td><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></td></tr></table>");
			htmlContents.add("</body></html>");
			try{
				String reportLocation= pathLocation;//txtreportLocation.getText();
				VWCUtil.writeListToFile(htmlContents,reportLocation, err, "UTF-8");
				lblStatus.setText("Report Generated");
				this.dispose();
				return true;
			}
			catch(Exception ex){
				AdminWise.printToConsole("Exception while writing file to generating security Report :::::"+ex.getMessage());
				return false;
			}


		}catch(Exception ex1){
			AdminWise.printToConsole("Exception while generating security Report :::::"+ex1.getMessage());
			return false;
		}

	}
	private void closeDialog() {
		setVisible(false);
		dispose();
	}

}
