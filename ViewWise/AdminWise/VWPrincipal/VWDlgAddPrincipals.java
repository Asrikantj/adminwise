/*
                A basic implementation of the JDialog class.
 */

package ViewWise.AdminWise.VWPrincipal;

import java.awt.*;

import javax.swing.*;

import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import com.computhink.common.Principal;

import javax.swing.border.EtchedBorder;

import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWMessage.VWMessageConstant;
import ViewWise.AdminWise.VWRoom.VWRoom;
import com.computhink.vws.server.VWSPreferences;

public class VWDlgAddPrincipals extends javax.swing.JDialog implements VWConstant {
    public VWDlgAddPrincipals(Frame parent) {
        super(parent);
		/****CV2019 merges - 10.2 line - Search Filter enhancement changes***/
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		int sid = room.getId();
		int schemaType = AdminWise.gConnector.ViewWiseClient.getSSType(sid);
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.AddIcon.getImage());
        setTitle(LBL_ADDPRINCIPAL_NAME);
        setModal(true);
        getContentPane().setLayout(null);
		/****CV2019 merges - 10.2 line - Search Filter enhancement changes***/
        setSize(480,488);
        setVisible(false);
        
        /****************************************/
        /*JLabel2.setText(LBL_SEARCHPRINCIPALS_NAME);
        getContentPane().add(JLabel2);
        JLabel2.setBounds(6,4,194, AdminWise.gBtnHeight);*/
        
        getContentPane().add(TxtSearchPrincipal);
        TxtSearchPrincipal.setBounds(11,12,380,24);

        BtnSearch.setBounds(395,12, 76, AdminWise.gBtnHeight);
        BtnSearch.setText(BTN_SEARCH_NAME);
        BtnSearch.setMnemonic('s');
        getContentPane().add(BtnSearch);
        /****************************************/ 
		/****CV2019 merges - 10.2 line - Search Filter enhancement changes***/       
        if (schemaType != 1) {
	        JLabel2.setText(LBL_SEARCHFILTER_TEXT);
	        getContentPane().add(JLabel2);
	        JLabel2.setBounds(11,30,400,24);
        }
        
        
        JLabel1.setText(LBL_LISTPRINCIPALS_NAME);
        getContentPane().add(JLabel1);
        JLabel1.setBounds(11,56,194, AdminWise.gBtnHeight);
        //  Saad
        //  23-12-04
        scPPrincipals.setViewportView(LstPrincipal);
        scPPrincipals.setBounds(11, 82, 460, 362);
        scPPrincipals.setBorder(etchedBorder);
        getContentPane().add(scPPrincipals);
        
        
        BtnSort.setBounds(395,56, 76, AdminWise.gBtnHeight);
        BtnSort.setText(BTN_SORT_NAME);
        BtnSort.setMnemonic('s');
        getContentPane().add(BtnSort);
        
        //getContentPane().add(LstPrincipal);
        LstPrincipal.setSelectionMode(
            ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //LstPrincipal.setBounds(4,28,276,366);
        //LstPrincipal.setBorder(etchedBorder);
        BtnAdd.setText(BTN_ADD_NAME);
        getContentPane().add(BtnAdd);
        BtnAdd.setBounds(322,452,72, AdminWise.gBtnHeight);
//        BtnAdd.setBackground(java.awt.Color.white);

        BtnCancel.setText(BTN_CANCEL_NAME);
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(400,452,72, AdminWise.gBtnHeight);
//        BtnCancel.setBackground(java.awt.Color.white);
        SymAction lSymAction = new SymAction();
        BtnAdd.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        BtnSort.addActionListener(lSymAction);
        BtnSearch.addActionListener(lSymAction);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        LstPrincipal.addKeyListener(aSymKey);
        SymWindow aSymWindow = new SymWindow();
        this.addWindowListener(aSymWindow);
        //Commented for implementing ldap User/Groups search optimization in LDAP 18/9/2018
        /*loadPrincipals();*/
        setResizable(false);
        getDlgOptions();
        setVisible(true);
    }
    //-----------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(java.awt.event.WindowEvent event) {
            Object object = event.getSource();
            if (object == VWDlgAddPrincipals.this)
                Dialog_windowClosing(event);
        }
    }
    //-----------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event) {
        BtnCancel_actionPerformed(null);
    }
    //-----------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnAdd_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
    //-----------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnAdd)
                BtnAdd_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
            else if (object == BtnSort)
            	BtnSort_actionPerformed(event);
            else if (object == BtnSearch)
            	BtnSearch_actionPerformed(event);
            
        }
    }
    //-----------------------------------------------------------
    public void setVisible(boolean b) {
        if (b) {
            Dimension d =VWUtil.getScreenSize();
            setLocation(d.width/4,d.height/4);
        }
        super.setVisible(b);
    }
    //-----------------------------------------------------------
    public void addNotify() {
        // Record the size of the window prior to calling parents addNotify.
        Dimension size = getSize();
        super.addNotify();
        if (frameSizeAdjusted)  return;
        frameSizeAdjusted = true;
        // Adjust size of frame according to the insets
        Insets insets = getInsets();
        setSize(insets.left + insets.right + size.width, 
            insets.top + insets.bottom + size.height);
    }
    //-----------------------------------------------------------
    boolean frameSizeAdjusted = false;
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JTextField TxtSearchPrincipal = new javax.swing.JTextField(1);
    JList LstPrincipal = new JList();
    JScrollPane scPPrincipals = new JScrollPane();
    VWLinkedButton BtnAdd = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    VWLinkedButton BtnSort = new VWLinkedButton(0,true);
    VWLinkedButton BtnSearch = new VWLinkedButton(0,true);
    Vector principalList = new Vector();
    javax.swing.border.EtchedBorder etchedBorder = 
        new javax.swing.border.EtchedBorder
            (EtchedBorder.LOWERED,java.awt.Color.white,
                java.awt.Color.darkGray);
    //-----------------------------------------------------------
    void BtnAdd_actionPerformed(java.awt.event.ActionEvent event) {
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
    //-----------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
    //-----------------------------------------------------------
    void BtnSort_actionPerformed(java.awt.event.ActionEvent event) {
    	try{
    		if(BtnSort.getText().trim().endsWith("t") || BtnSort.getText().trim().endsWith("<")){
    			BtnSort.setText(BTN_SORT_NAME+">>");
    		}else if(BtnSort.getText().trim().endsWith(">")){
    			BtnSort.setText(BTN_SORT_NAME+"<<");
    		}
    		if(principalList== null || principalList.size() == 0)
    			principalList=VWPrincipalConnector.getDirectoryPrincipals();
	    	if(BtnSort.getText().trim().equalsIgnoreCase(BTN_SORT_NAME+">>"))
		        Collections.sort(principalList, new PrincipalSortComparator(true));
	    	else if(BtnSort.getText().trim().equalsIgnoreCase(BTN_SORT_NAME+"<<"))
	    		Collections.sort(principalList, new PrincipalSortComparator(false));
		    else
		    	Collections.sort(principalList, new PrincipalSortComparator(true));
	    	LstPrincipal.setListData(principalList);
    	}catch(Exception ex){
    		LstPrincipal.setListData(principalList);
    		System.out.println("Error in getting list in sorted order:"+ex.toString());
    	}
    }
    //-----------------------------------------------------------
    
  //-----------------------------------------------------------
    /**
     * Method created for implementing ldap User/Groups search optimization in LDAP 18/9/2018
     *  @param event
     */
    void BtnSearch_actionPerformed(java.awt.event.ActionEvent event) {
    	
    	try{
    		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    		int sid = room.getId();
    		int schemaType = AdminWise.gConnector.ViewWiseClient.getSSType(sid);
    		String principalName = TxtSearchPrincipal.getText();
    		int searchStringCnt = 0;
    		AdminWise.printToConsole("sid :  "+ sid+"::schema type ::"+schemaType+":::principal Name :  "+ principalName);
    		if (principalName.length() > 0) { 
    			searchStringCnt = principalName.length();
    			AdminWise.printToConsole("Search String Cnt :  "+ searchStringCnt);
    		}

    		if (AdminWise.gConnector.ViewWiseClient.getSSType(sid)== 4)
    		{
    			loadPrincipalsNew(principalName);
    			LstPrincipal.setListData(principalList);
    		} else if ((AdminWise.gConnector.ViewWiseClient.getSSType(sid)== 1) && ((searchStringCnt==0) || (principalName.equalsIgnoreCase("*")) || (principalName.equalsIgnoreCase("*.*")))) {
    			loadPrincipalsNew(principalName);
    			LstPrincipal.setListData(principalList);
    		} else if ((AdminWise.gConnector.ViewWiseClient.getSSType(sid)== 1) && (searchStringCnt>0)){
    			VWMessage.showMessage((java.awt.Component) this,"This feature is not available for ADS");
    		}
    	}catch(Exception ex){
    		LstPrincipal.setListData(principalList);
    		AdminWise.printToConsole("exception in Button search ::: "+ex.getMessage());
    	}
    }
    //-----------------------------------------------------------
    public Object[] getValues() {
    	/*
		 * Jlist.getSelectedValues() is replaced with JList.getSelectedValuesList().toArray() 
		 * as JList.getSelectedValues() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
        return LstPrincipal.getSelectedValuesList().toArray();
    }
    //-----------------------------------------------------------
    public boolean getCancelFlag() {
        return cancelFlag;
    }
    //-----------------------------------------------------------
    private void loadPrincipals(){
        principalList=VWPrincipalConnector.getDirectoryPrincipals();
        LstPrincipal.setListData(principalList);
    }
    //-----------------------------------------------------------
    //-----------------------------------------------------------
	/****CV2019 merges - 10.2 line - Search Filter enhancement changes***/
    private void loadPrincipalsNew(String searchForPName){
        principalList=VWPrincipalConnector.getDirectoryPrincipalsNew(searchForPName);       
        LstPrincipal.setListData(principalList);
        if (principalList.size() == 0) {
        	VWMessage.showInfoDialog(VWMessageConstant.ERR_SEARCH_NORESULT);
        }
    }
    //-----------------------------------------------------------
    private void saveDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos) {
            prefs.putInt("x", this.getX());
            prefs.putInt("y", this.getY());
        }
        else {
            prefs.putInt("x",50);
            prefs.putInt("y",50);
        }
    }
    //-----------------------------------------------------------
    private void getDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
    //-----------------------------------------------------------
    boolean cancelFlag=true;
    
    class PrincipalSortComparator implements Comparator {
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public PrincipalSortComparator(boolean sortAsc) {
                m_sortAsc = sortAsc;
        }
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof Principal) || !(o2 instanceof Principal))
                return 0;
            Principal s1 = (Principal)o1;
            Principal s2 = (Principal)o2;
            int result = 0;
            result = s1.getName().toLowerCase().compareTo(s2.getName().toLowerCase());
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof PrincipalSortComparator) {
            	PrincipalSortComparator compObj = (PrincipalSortComparator)obj;
                return (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    }
}