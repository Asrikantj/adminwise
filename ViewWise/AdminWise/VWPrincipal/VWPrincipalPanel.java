package ViewWise.AdminWise.VWPrincipal;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;

import com.computhink.common.Constants;
import com.computhink.common.Principal;
import com.computhink.resource.ResourceManager;
public class VWPrincipalPanel extends JPanel implements  VWConstant, Constants {
    public VWPrincipalPanel() {
    }
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWPrincipal.setLayout(null);
        add(VWPrincipal,BorderLayout.CENTER);
        VWPrincipal.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWPrincipal.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,433);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/

        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnAdd.setText(BTN_ADD_NAME);
        BtnAdd.setActionCommand(BTN_ADD_NAME);
        PanelCommand.add(BtnAdd);
        //BtnAdd.setBackground(java.awt.Color.white);
        BtnAdd.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnAdd.setIcon(VWImages.AddIcon);
        
        top += hGap;
        BtnRemove.setText(BTN_REMOVE_NAME);
        BtnRemove.setActionCommand(BTN_REMOVE_NAME);
        PanelCommand.add(BtnRemove);
        //BtnRemove.setBackground(java.awt.Color.white);
        BtnRemove.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnRemove.setIcon(VWImages.DelIcon);
        
        top += hGap + 12;
        BtnSign.setText(BTN_SIGN_NAME);
        BtnSign.setActionCommand(BTN_SIGN_NAME);
        PanelCommand.add(BtnSign);
        //BtnSign.setBackground(java.awt.Color.white);
        BtnSign.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSign.setIcon(VWImages.UserGroupIcon);
        
        top += hGap;
		//AdminWise.printToConsole("Adding BtnChangePassword");
		BtnChangePassword.setText(BTN_CHANGE_PASSWORD);
		BtnChangePassword.setActionCommand(BTN_CHANGE_PASSWORD);
		PanelCommand.add(BtnChangePassword);
		BtnChangePassword.setBounds(12, top, 144, AdminWise.gBtnHeight);
		BtnChangePassword.setIcon(VWImages.UserEmailIdImage);
		
        top += hGap;
        BtnEmailID.setText(BTN_EMAILID_NAME);
        BtnEmailID.setActionCommand(BTN_EMAILID_NAME);
        PanelCommand.add(BtnEmailID);
        //BtnEmailID.setBackground(java.awt.Color.white);
        BtnEmailID.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnEmailID.setIcon(VWImages.UserEmailIdImage);
        
        top += hGap + 12;
        BtnSync.setText(BTN_SYNC_NAME);
        BtnSync.setActionCommand(BTN_SYNC_NAME);
        PanelCommand.add(BtnSync);
        //BtnSync.setBackground(java.awt.Color.white);
        BtnSync.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSync.setIcon(VWImages.DSNIcon); 
        
        top += hGap + 12;
        BtnSubAdmin.setText(BTN_SUB_ADMIN);
        BtnSubAdmin.setActionCommand(BTN_SUB_ADMIN);
        PanelCommand.add(BtnSubAdmin);
        BtnSubAdmin.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSubAdmin.setIcon(VWImages.SubAdminIcon); 
        
        top += hGap + 12;
        BtnSecurityReport.setText(BTN_SECURITY_REPORT);
        BtnSecurityReport.setActionCommand(BTN_SECURITY_REPORT);
        PanelCommand.add(BtnSecurityReport);
        BtnSecurityReport.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSecurityReport.setIcon(VWImages.SecurityReportIcon); 
        
        top += hGap + 12;
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        PanelCommand.add(BtnRefresh);
        //BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,top,144, AdminWise.gBtnHeight);
        ///BtnRefresh.setBounds(12,230,144, AdminWise.gBtnHeight);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        
        top += hGap + 12;
        BtnSaveAs.setText(BTN_SAVEAS_NAME);
        BtnSaveAs.setActionCommand(BTN_SAVEAS_NAME);
        PanelCommand.add(BtnSaveAs);
        //BtnSaveAs.setBackground(java.awt.Color.white);
        BtnSaveAs.setBounds(12,top,144, AdminWise.gBtnHeight);
        //BtnSaveAs.setBounds(12,265,144, AdminWise.gBtnHeight);
        BtnSaveAs.setIcon(VWImages.SaveAsIcon);
        
        top += hGap + 12;
        lblUsernameFilter.setBounds(12,top,144, 20);
        PanelCommand.add(lblUsernameFilter);
        top += hGap;
        txtUsernameFilter.setBounds(12,top,144, 20);
        TxtUsernameFilterListener txtUsernameFilterListener = new TxtUsernameFilterListener();
        txtUsernameFilter.addKeyListener(txtUsernameFilterListener);
        PanelCommand.add(txtUsernameFilter);
        txtUsernameFilter.setEnabled(true);
        txtUsernameFilter.setVisible(true);
        
        PanelTable.setLayout(new BorderLayout());
        VWPrincipal.add(PanelTable);
        PanelTable.setBounds(168,0,433,400);
        SPTable.setOpaque(true);
        SPTable.setBounds(0,0,975,706);
        PanelTable.add(SPTable, BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWPrincipal.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.SecuriyImage);
        SymAction lSymAction = new SymAction();
        BtnSign.addActionListener(lSymAction);
        BtnRemove.addActionListener(lSymAction);
        BtnAdd.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        BtnSync.addActionListener(lSymAction);
        BtnSaveAs.addActionListener(lSymAction);
        BtnEmailID.addActionListener(lSymAction);
        BtnChangePassword.addActionListener(lSymAction);
        BtnSecurityReport.addActionListener(lSymAction);
        BtnSubAdmin.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    
    
    public Vector getUsers(String user, Vector users) {
    	Vector usersList1 = new Vector();  	
    	if (user.equals("") ){
    		return users;
    	} else {
    		for (int count = 0; count < users.size(); count++) {
    			Principal row = (Principal) users.get(count);
    			String currName = row.getName().toUpperCase();
    			if (currName.startsWith(user.toUpperCase())) {
    				usersList1.add(row);
    			}
    		}
    	}
    	
    	return usersList1;
    }
		
		
    class TxtUsernameFilterListener extends KeyAdapter{    	
    	public void keyReleased(java.awt.event.KeyEvent event){    		
    		Object object = event.getSource();
        	if(object == txtUsernameFilter){
        	String filter = txtUsernameFilter.getText();
        	Vector users = getUsers(filter, userList);
    		Table.addData(users);
    		Table.m_data.fireTableDataChanged();
   		}
    	}
        public void keyTyped(java.awt.event.KeyEvent event) {
            
        }       
        public void keyPressed(java.awt.event.KeyEvent event) {

        }
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnSign)
                BtnSign_actionPerformed(event);
            else if (object == BtnRemove)
                BtnRemove_actionPerformed(event);
            else if (object == BtnAdd)
                BtnAdd_actionPerformed(event);
            else if (object == BtnRefresh)
                BtnRefresh_actionPerformed(event);
            else if (object == BtnSync)
                BtnSync_actionPerformed(event);
            else if (object == BtnSaveAs)
                BtnSaveAs_actionPerformed(event);
            else if (object == BtnEmailID)
                BtnEmailID_actionPerformed(event);
            else if (object == BtnChangePassword)
				BtnChangePassword_actionPerformed(event);
            else if(object==BtnSecurityReport)
            	BtnSecurityReport_actionPerformed(event);
            else if(object==BtnSubAdmin)
            	BtnSubAdmin_actionPerformed(event);
            	
        }
    }
    //-----------------------------------------------------------
    javax.swing.JPanel VWPrincipal = new javax.swing.JPanel();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_PRINCIPAL_NAME, VWImages.SecuriyImage);    
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();

    Vector userList = new Vector();
    
    VWLinkedButton BtnSign = new VWLinkedButton(1);
    VWLinkedButton BtnAdd = new VWLinkedButton(1);
    VWLinkedButton BtnRemove = new VWLinkedButton(1);
    VWLinkedButton BtnRefresh = new VWLinkedButton(1);
    VWLinkedButton BtnSync = new VWLinkedButton(1);
    VWLinkedButton BtnSecurityReport = new VWLinkedButton(1);
    VWLinkedButton BtnSubAdmin = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    public static VWLinkedButton BtnChangePassword = new VWLinkedButton(1);
    public static VWLinkedButton BtnEmailID = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWPrincipalTable Table = new VWPrincipalTable();
    javax.swing.JLabel lblUsernameFilter = new javax.swing.JLabel(AdminWise.connectorManager.getString("VWPrinciplaePnael.lblUsernameFilter"));
    javax.swing.JTextField txtUsernameFilter = new JTextField();
    
    //-----------------------------------------------------------
    
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWPrincipal)
                VWPrincipal_componentResized(event);
        }
    }
    //-----------------------------------------------------------
    void VWPrincipal_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWPrincipal) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //-----------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        try{
            userList = new Vector();
            userList = VWPrincipalConnector.getPrincipals();
            Table.addData(userList);
        }
        catch(Exception e){};
        setEnableMode(MODE_UNSELECTED);
    }
    //-----------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //-----------------------------------------------------------
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWSaveAsHtml.saveArrayInFile(Table.getReportData(),SignatureColumnNames,SignatureColumnWidths,null,(java.awt.Component)this,PRINCIPAL_TOOLTIP);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //-----------------------------------------------------------
    void BtnSign_actionPerformed(java.awt.event.ActionEvent event) {
        Principal principal=Table.getPrincipal();
        VWSignDlg signDlg = new VWSignDlg(AdminWise.adminFrame,principal);
        signDlg.requestFocus();
        signDlg.dispose();
        signDlg=null;
        BtnRefresh_actionPerformed(null);
    }
    //-----------------------------------------------------------
    void BtnEmailID_actionPerformed(java.awt.event.ActionEvent event) {
    	try{
	    	String selUserName = Table.getRowName();
	 	  	String selEmailID = Table.getRowEmailID();
	   		Principal principal=Table.getPrincipal();
	   		String modifiedEmailID = (String) 	JOptionPane.showInputDialog(null,AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_1"),AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_1.1")+"\""+selUserName+"\"",JOptionPane.INFORMATION_MESSAGE,new ImageIcon(""),null,selEmailID);
	   		if(modifiedEmailID!=null){
	   			if(!EmailAddressValidator.isValidEmailAddress(modifiedEmailID)){
	   				VWMessage.showMessage(null, AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_2"));
	   				return;
	   			}		 	  	
	   			principal.setEmail(modifiedEmailID);
	   			VWPrincipalConnector.updatePrincipal(principal);
	   			Table.UpdateEmail(Table.getSelectedRow());
	   		}
    	}catch(Exception e){
    		//JOptionPane.showMessageDialog(null,e.toString(),"",1);
    	}
    }
    // -----------------------------------------------------------    
    void BtnChangePassword_actionPerformed(java.awt.event.ActionEvent event) {
		AdminWise.printToConsole("BtnChangePassword_actionPerformed");
		// AdminWise.printToConsole("new ChangePasswordDlg()==="+new
		Principal principal = Table.getPrincipal();
		int userId = principal.getId();
		VWChangePasswordDlg dlg = new VWChangePasswordDlg(principal);
		AdminWise.printToConsole("dlg : " + dlg);

	}
    
    void BtnSecurityReport_actionPerformed(java.awt.event.ActionEvent event) {
		AdminWise.printToConsole("BtnChangePassword_actionPerformed");
		Principal principal = Table.getPrincipal();
		int userId = principal.getId();
		SecurityReportDlg dlg1 = new SecurityReportDlg(principal);
	}
    
    //Enahcment :- SubAdministrator CV10 added to load the subadministrator dialog
    void BtnSubAdmin_actionPerformed(java.awt.event.ActionEvent event) {
    	String group="";
    	String roles="";
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Vector subAdminData=new Vector();
    	AdminWise.gConnector.ViewWiseClient.getSubAdminRoles(room.getId(),Table.getPrincipal().getName().toString(),subAdminData);
    	if((subAdminData.size()>0)&&(!(subAdminData.isEmpty()))){
    		AdminWise.printToConsole("getSubAdminRoles return valeue :"+subAdminData.elementAt(0));
    		StringTokenizer st = new StringTokenizer ( (String) subAdminData.elementAt(0), "\t");
    		group=st.nextToken();
    		roles=st.nextToken();
    	}
    	if(group.equals("")){
    		SubAdministratorDlg dlg1 = new SubAdministratorDlg(Table.getPrincipal().getName().toString(),roles,0);
    	}else{
    		SubAdministratorDlg dlg1 = new SubAdministratorDlg(Table.getPrincipal().getName().toString(),roles,1);
    	}

    }
    // -----------------------------------------------------------
    void BtnRemove_actionPerformed(java.awt.event.ActionEvent event) {    	
        String principalType="Group";
        if(Table.getRowType()==Principal.USER_TYPE) principalType="User";
        String msg="";
        int[] selRows=Table.getSelectedRows();
        if(selRows.length==1)
            msg="Delete "+principalType+" '"+Table.getRowName()+"'"+"?";
        else
            msg=AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_3");
        if(VWMessage.showWarningDialog((java.awt.Component) this,msg
        )!=javax.swing.JOptionPane.YES_OPTION) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            int deleteRows=0;
            for(int i=0;i<selRows.length;i++)
            {
                if (principalType.equalsIgnoreCase("User")){
                    String ret = VWPrincipalConnector.checkUserInRoute(Table.getRowData(selRows[i]-deleteRows).getId());
                    if ( ret != null && ret.trim().length() > 0 ){
                	JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_4")+Table.getRowData(selRows[i]-deleteRows).getName() + AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_4.1")+" "+ Constants.WORKFLOW_MODULE_NAME.toLowerCase() +"(s)\n" + ret, ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION );
                	continue;
                    }
                }
                if (principalType.equalsIgnoreCase("Group")){
                    String ret = VWPrincipalConnector.checkUserInRoute(Table.getRowData(selRows[i]-deleteRows).getId());
                    if ( ret != null && ret.trim().length() > 0 ){
                	JOptionPane.showMessageDialog(this, AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_4")+Table.getRowData(selRows[i]-deleteRows).getName() + AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_4.1")+" "+ Constants.WORKFLOW_MODULE_NAME.toLowerCase() +"(s)\n" + ret, ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION );
                	continue;
                    }
                }
            	VWPrincipalConnector.deletePrincipal(Table.getRowData(selRows[i]-deleteRows));
                Table.removeData(selRows[i]-deleteRows);
                deleteRows++;
            }
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //-----------------------------------------------------------
    void BtnAdd_actionPerformed(java.awt.event.ActionEvent event) {
        VWDlgAddPrincipals addPrincipalDlg = new VWDlgAddPrincipals(AdminWise.adminFrame);
        addPrincipalDlg.requestFocus();
        Object[] retValues=addPrincipalDlg.getValues();
        if(addPrincipalDlg.getCancelFlag()) retValues=null;
        addPrincipalDlg.dispose();
        if(retValues==null || retValues.length==0) return;
        try{
        	AdminWise.printToConsole("retValues :"+retValues);
            AdminWise.adminPanel.setWaitPointer();
            for(int i=0;i<retValues.length;i++) {
                int ret = VWPrincipalConnector.addPrincipal((Principal)retValues[i]);
                AdminWise.printToConsole("Return value from addPrincipals : "+ret);
            	if(ret < 0){
            		String msg = AdminWise.gConnector.ViewWiseClient.getErrorDescription(ret);
            		//AdminWise.printToConsole("msg :::: "+msg);
            		VWMessage.showMessage(null, msg, VWMessage.DIALOG_TYPE);
            		break;
            	}
            	Table.insertData((Principal)retValues[i]);            	
            }
			/**CV2019 - Added for refersh issue fix after adding user/group from add dialog***/
            BtnRefresh_actionPerformed(null);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //-----------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            Table.clearData();
            userList = new Vector();
            userList = VWPrincipalConnector.getPrincipals();
            Table.addData(userList);
            setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}   
    }
    //-----------------------------------------------------------
    void BtnSync_actionPerformed(java.awt.event.ActionEvent event) {
        String msg=AdminWise.connectorManager.getString("MSG.Resync");
        if(VWMessage.showConfirmDialog((java.awt.Component) this,msg )!=javax.swing.JOptionPane.YES_OPTION) return;
        try{
        	AdminWise.adminPanel.setWaitPointer();
        	Vector routeList = new Vector();
        	VWPrincipalConnector.checkGroupsInRoute(routeList);
        	String processFlag = "0";
        	int selected = -1;
        	if (routeList != null && routeList.size() > 0){
        		msg=AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_5")+" "+ Constants.WORKFLOW_MODULE_NAME +AdminWise.connectorManager.getString("VWPrinciplaePnael.msg_5.1")+" "+ Constants.WORKFLOW_MODULE_NAME.toLowerCase() +"?";

        		//Added for implementaion of syncronizing route users 
        		selected = VWMessage.showOptionDialog(this, msg, SyncOption,SyncOption[1]);
        		if (selected == 1){//1 for End route 
        			processFlag = "1";        	    
        			VWPrincipalConnector.endRouteStatus(routeList);        	    
        		}
        	}
            
        	int ret = VWPrincipalConnector.syncPrincipals();
        	AdminWise.printToConsole("Return value from syncPrincipals : "+ret);
        	if(ret < 0){
        		msg = AdminWise.gConnector.ViewWiseClient.getErrorDescription(ret);
        		//AdminWise.printToConsole("msg :::: "+msg);
        		VWMessage.showMessage(null, msg, VWMessage.DIALOG_TYPE);
        		return;
        	}
        	
        	int drsStatus = VWPrincipalConnector.isDRSEnabled();
        	if (drsStatus == 1){
        		VWPrincipalConnector.seDRSEnable("false");
        	}

        	if(selected!=2){//User selects Cancel
        		VWPrincipalConnector.syncRouteGroups(processFlag);
        		if (drsStatus == 1){
        			VWPrincipalConnector.seDRSEnable("true");
        		}
        	}
        	Table.clearData();
        	Table.addData(VWPrincipalConnector.getPrincipals());
        	setEnableMode(MODE_UNSELECTED);
        	// This is for enabling the DRS server.
        	drsStatus = VWPrincipalConnector.isDRSEnabled();
        	if (drsStatus == 0){
        		VWPrincipalConnector.seDRSEnable("true");
        	}
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}   
    }
    boolean getTableSelectedSubAdminGroup(){
    	boolean tabSelected=false;
    	String namedGroup="";
    	String professionalGroup="";
    	String entrConcurrentGroup="";
    	String profConcurrentGroup="";
    	String namedOnlineGroup="";
    	String concurrentOnlineGrou="";
    	String adminGroup="";
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();	
    	Vector groupsData=new Vector();
    	Vector userData=new Vector();
    	gCurRoom=room.getId();
    	Principal principal = Table.getPrincipal();
    	String user = principal.getName();
    	AdminWise.printToConsole("user >>>>>>>>>>>>>>>>>>>>>"+user);
    	boolean isSubAdminUser = AdminWise.gConnector.ViewWiseClient.checkSubAdminUser(gCurRoom, user);
    	AdminWise.printToConsole("isSubAdminUser >>>>>>>>>>>>>>>>>>>>>"+isSubAdminUser);
    	if (isSubAdminUser) {
    		tabSelected=true;
    	}
    	/*AdminWise.gConnector.ViewWiseClient.getGroupsData(gCurRoom, groupsData);
    	AdminWise.gConnector.ViewWiseClient.getNamedProfessionalUser(gCurRoom, userData);
	    if(userData!=null&&userData.size()>0){
	    	namedGroup=userData.get(0).toString();
	    	professionalGroup=userData.get(1).toString();
	    	adminGroup=userData.get(2).toString();
	    	entrConcurrentGroup=userData.get(3).toString();
	    	profConcurrentGroup=userData.get(4).toString();
	    	namedOnlineGroup=userData.get(5).toString();
	    	concurrentOnlineGrou=userData.get(6).toString();
	    }
    	
    	if ((groupsData != null)&&(groupsData.size()>0)) {
    		for (int i = 0; i < groupsData.size(); i++){
    			principal = Table.getPrincipal();
    			if(!principal.getName().equalsIgnoreCase(namedGroup)){
    				if(!principal.getName().equalsIgnoreCase(professionalGroup)){
    					if(!principal.getName().equalsIgnoreCase(entrConcurrentGroup)){
    						if(!principal.getName().equalsIgnoreCase(profConcurrentGroup)){
    							if(!principal.getName().equalsIgnoreCase(namedOnlineGroup)){
    								if(!principal.getName().equalsIgnoreCase(concurrentOnlineGrou)){
    									if(!principal.getName().equalsIgnoreCase(adminGroup)){
    										tabSelected=true;
    									}
    								}
    							}
    						}
    					}
    				}
    			}
    		}
    	}*/
    	return tabSelected;
    }
    
    //-----------------------------------------------------------
    public void setEnableMode(int mode) {
    	BtnSign.setEnabled(true);
		BtnRefresh.setEnabled(true);
    	BtnSync.setEnabled(true);
		BtnSaveAs.setEnabled(true);
		BtnSubAdmin.setEnabled(true);
//		enabling if hostedadmin registry is false
		boolean isHostedAdmin = false;
		if(!VWConnector.isServerMachine()){
			isHostedAdmin = VWConnector.getHostedAdmin();
		}
		if(!isHostedAdmin){			
			BtnRemove.setEnabled(true);
			BtnAdd.setEnabled(true);		
		}
        txtUsernameFilter.setEnabled(true);
        switch (mode) {
            case MODE_UNSELECTED:
                if(Table.getRowCount()==0) {
                    BtnSaveAs.setEnabled(false);
                }
                BtnRemove.setEnabled(false);
                BtnSign.setEnabled(false);
                BtnEmailID.setEnabled(false);
                BtnChangePassword.setEnabled(false);
                BtnSecurityReport.setEnabled(false);
                BtnSubAdmin.setEnabled(false);
                break;
            case MODE_SELECT:
            	if(Table.getSelectedRowCount()==1){
            		BtnChangePassword.setEnabled(true);
            		BtnSecurityReport.setEnabled(true);
            	
            		if(Table.getRowType()==Principal.USER_TYPE ){
						if (getTableSelectedSubAdminGroup() == true) {
							BtnSubAdmin.setEnabled(true);
						} else {
							BtnSubAdmin.setEnabled(false);
						}
            		}
            		else
            		 BtnSubAdmin.setEnabled(false);
            		BtnEmailID.setEnabled(true);            		
            	}else{
            		BtnChangePassword.setEnabled(false);
            		BtnSecurityReport.setEnabled(false);
            		BtnSubAdmin.setEnabled(false);
            		BtnEmailID.setEnabled(false);
            	}
            	BtnRemove.setEnabled(!isHostedAdmin);
                if(Table.getRowType()==Principal.GROUP_TYPE || Table.getSelectedRows().length>1)
                    BtnSign.setEnabled(false);
                break;
            case MODE_UNCONNECT:
            	BtnChangePassword.setEnabled(false);
            	BtnSecurityReport.setEnabled(false);
            	BtnEmailID.setEnabled(false);
                BtnSign.setEnabled(false);
                BtnRefresh.setEnabled(false);
                BtnSync.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnSubAdmin.setEnabled(false);
                BtnRemove.setEnabled(false);
                BtnAdd.setEnabled(false);
                BtnSign.setEnabled(false);
                txtUsernameFilter.setText("");
                txtUsernameFilter.setEnabled(false);
                gCurRoom=0;
                break;
        }
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(VWPrincipalConnector.isVWSignInstalled(room.getId())!=1)
            BtnSign.setEnabled(false);
    }
    //-----------------------------------------------------------
    public Principal getSelectedPrincipal()
    {
        return Table.getPrincipal();
    }
    //-----------------------------------------------------------
    private static int gCurRoom=0;
    private boolean UILoaded=false;
}

/**
 * A class to provide stronger validation of email addresses.
 * devdaily.com, no rights reserved. :)
 *
 */
class EmailAddressValidator
{
	public static boolean isValidEmailAddress(String emailAddress)
	{
		//an empty string is valid
		if(emailAddress.trim().equals(""))
			return true;
		// a null string is invalid
		if ( emailAddress == null )
			return false;
		
		// a string without a "@" is an invalid email address
		if ( emailAddress.indexOf("@") < 0 )
			return false;
		
		// a string without a "."  is an invalid email address
		if ( emailAddress.indexOf(".") < 0 )
			return false;
		
		if ( lastEmailFieldTwoCharsOrMore(emailAddress) == false )
			return false;
		
			return true;
	}
	
	/**
	 * Returns true if the last email field (i.e., the country code, or something
	 * like .com, .biz, .cc, etc.) is two chars or more in length, which it really
	 * must be to be legal.
	 */
	private static boolean lastEmailFieldTwoCharsOrMore(String emailAddress)
	{
		StringTokenizer st = new StringTokenizer(emailAddress,".");
		String lastToken = null;
		while ( st.hasMoreTokens() )
		{
			lastToken = st.nextToken();
		}
		
		if ( lastToken.length() >= 2 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
