package ViewWise.AdminWise.VWPrincipal;


import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDlgSaveLocation;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

import com.computhink.common.Principal;
import com.computhink.common.RoomProperty;
import com.computhink.manager.VWRoomItem;
import com.computhink.vwc.VWAdmin;
import com.computhink.vwc.VWCUtil;
import com.computhink.vws.server.VWSPreferences;
/**
 *  Enhancement :-SubAdministrator to Enable or disable to tabs in adminwise CV10 Date:- 3-3-2016
 * @author madhavan.b
 * 
 */
public class SubAdministratorDlg extends JDialog implements VWConstant {

	private JPanel tabsPanel = new JPanel();

	private static  VWComboBox cmbSubAdmin = new VWComboBox();

	private JLabel lblSubAdmin = new JLabel();
	private int updFlag=0;

	public static boolean active = false;

	private static boolean subAdminLoaded;

	private static int gCurRoom;
	public VWDlgReportSaveLocation dlgLocation = null;

	private JButton btnSave = new JButton();
	private JButton btnRemove = new JButton();
	private JButton btnCancel = new JButton();

	private JCheckBox chkDocTypeTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkDocTypeTab"));

	private JCheckBox chkAuditTrailTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkAuditTrailTab"));
	private JCheckBox chkConnectedUserTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkConnectedUserTab"));
	private JCheckBox chkLockedDocsTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkLockedDocsTab"));

	private JCheckBox chkStatisticsTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkStatisticsTab"));
	private JCheckBox chkRetentionTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkRetentionTab"));
	private JCheckBox chkNotificationTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkNotificationTab"));
	private JCheckBox chkWorkflowTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkWorkflowTab"));
	private JCheckBox chkWorkflowReportTab = new JCheckBox(AdminWise.connectorManager.getString("VWSubAdmin.chkWorkflowReportTab"));
    String languageLocale=AdminWise.getLocaleLanguage();
	public int chkDocTypeValue = 0;
	public int chkAuditValue=0;
	public int chkConnectedUserValue=0;
	public int chkLockedDocsValue=0;
	public int chkStatisticsValue=0;
	public int chkRetentionValue=0;
	public int chkNotificationValue=0;
	public int chkWorkflowValue=0;
	public int chkWorkflowReportValue=0;

	int reportNodeId=-1;

	public SubAdministratorDlg(String user,String roles,int mode) {
		//AdminWise.printToConsole("group inside constructor:::"+group+"roles::"+roles);
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		if (room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		gCurRoom=room.getId();
		cmbSubAdmin.addItem(user);
		if (mode == 0) {
			updFlag = 0;
			// loadGroups();
		} else {
			updFlag = 1;
			// loadGroups();
		}
		cmbSubAdmin.setSelectedItem(user.toString());
		cmbSubAdmin.setEditable(false);
		cmbSubAdmin.setEnabled(false);
		checkBoxValidation(roles);
		setCurrentLocation();
		getContentPane().setBackground(AdminWise.getAWColor());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle(AdminWise.connectorManager.getString("SUbAdmin.Title"));
		setModal(true);
		getContentPane().setLayout(null);
		setSize(350, 350);
		setResizable(false);
		setBounds(0, 0,360,300);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width/2 - getWidth()/2), (screenSize.height/2 - getHeight()/2));
		if (roles.equals("")||roles.length()==0) {
			tabsPanel.setBorder(new javax.swing.border.TitledBorder(AdminWise.connectorManager.getString("NewSubAdminBorder.Title")));
		} else {
			tabsPanel.setBorder(new javax.swing.border.TitledBorder(AdminWise.connectorManager.getString("UpdSubAdminBorder.Title")));	
		}
		//tabsPanel.setBorder(new javax.swing.border.TitledBorder(AdminWise.connectorManager.getString("SubAdminBorder.Title")));
		tabsPanel.setLayout(null);
		tabsPanel.setBackground(AdminWise.getAWColor());
		tabsPanel.setBounds(15,60,320,170);
		getContentPane().add(tabsPanel);
		if(languageLocale.equals("nl_NL")){
			btnRemove.setBounds(17,240, 73+20+15, 25);
		}else{
			btnRemove.setBounds(17,240, 73, 25);
		}
		btnRemove.setText(BTN_SUBADMIN_REMOVE);
		btnRemove.setBackground(AdminWise.getAWColor());
		btnRemove.setIcon(VWImages.DelIcon);
		getContentPane().add(btnRemove);
		btnRemove.addActionListener(new SymAction());
		if(languageLocale.equals("nl_NL")){
			btnSave.setBounds(180-30,240, 73+15, 25);
		}else
			btnSave.setBounds(180,240, 73, 25);
		btnSave.setText(BTN_SUBADMIN_SAVE);
		btnSave.setBackground(AdminWise.getAWColor());
		btnSave.setIcon(VWImages.SaveIcon);
		getContentPane().add(btnSave);
		btnSave.addActionListener(new SymAction());
		if(languageLocale.equals("nl_NL")){
		btnCancel.setBounds(260-15, 240, 73+15, 25);
		}else
			btnCancel.setBounds(260, 240, 73, 25);	
		btnCancel.setText(BTN_CANCEL_NAME);
		btnCancel.setBackground(AdminWise.getAWColor());
		btnCancel.setIcon(VWImages.CloseIcon);
		getContentPane().add(btnCancel);
		btnCancel.addActionListener(new SymAction());

		lblSubAdmin.setBounds(25,17,120,25);
		lblSubAdmin.setText("Sub Administrator : ");
		lblSubAdmin.setBackground(tabsPanel.getBackground());
		getContentPane().add(lblSubAdmin);

		cmbSubAdmin.setBounds(160,17,170,25);
		cmbSubAdmin.setBackground(tabsPanel.getBackground());
		cmbSubAdmin.addActionListener(new SymAction());
		getContentPane().add(cmbSubAdmin);

		int height=25;
		chkDocTypeTab.setBounds(35,height,120,25);
		chkDocTypeTab.setBackground(tabsPanel.getBackground());
		chkDocTypeTab.addActionListener(new SymAction());
		tabsPanel.add(chkDocTypeTab);

		height=height+25;
		chkAuditTrailTab.setBounds(35,height,120,25);
		chkAuditTrailTab.setBackground(tabsPanel.getBackground());
		chkAuditTrailTab.addActionListener(new SymAction());
		tabsPanel.add(chkAuditTrailTab);

		height=height+25;
		chkConnectedUserTab.setBounds(35,height,120,25);
		chkConnectedUserTab.setBackground(tabsPanel.getBackground());
		chkConnectedUserTab.addActionListener(new SymAction());
		tabsPanel.add(chkConnectedUserTab);

		height=height+25;
		chkLockedDocsTab.setBounds(35,height,120,25);
		chkLockedDocsTab.setBackground(tabsPanel.getBackground());
		chkLockedDocsTab.addActionListener(new SymAction());
		tabsPanel.add(chkLockedDocsTab);

		int height1=25;		
		chkStatisticsTab.setBounds(215,height1,80,25);
		chkStatisticsTab.setBackground(tabsPanel.getBackground());
		chkStatisticsTab.addActionListener(new SymAction());
		tabsPanel.add(chkStatisticsTab);

		height1=height1+25;		
		chkRetentionTab.setBounds(215,height1,80,25);
		chkRetentionTab.setBackground(tabsPanel.getBackground());
		chkRetentionTab.addActionListener(new SymAction());
		tabsPanel.add(chkRetentionTab);


		height1=height1+25;		
		chkNotificationTab.setBounds(215,height1,80,25);
		chkNotificationTab.setBackground(tabsPanel.getBackground());
		chkNotificationTab.addActionListener(new SymAction());
		tabsPanel.add(chkNotificationTab);

		height1=height1+25;		
		chkWorkflowTab.setBounds(215,height1,80,25);
		chkWorkflowTab.setBackground(tabsPanel.getBackground());
		chkWorkflowTab.addActionListener(new SymAction());
		tabsPanel.add(chkWorkflowTab);

		height1=height1+25;		
		chkWorkflowReportTab.setBounds(35,height1,120,25);
		chkWorkflowReportTab.setBackground(tabsPanel.getBackground());
		chkWorkflowReportTab.addActionListener(new SymAction());
		tabsPanel.add(chkWorkflowReportTab);

		if(mode==0){
			chkDocTypeTab.setSelected(true);
			chkAuditTrailTab.setSelected(true);
			chkConnectedUserTab.setSelected(true);
			chkLockedDocsTab.setSelected(true);
			chkStatisticsTab.setSelected(true);
			chkRetentionTab.setSelected(true);
			chkNotificationTab.setSelected(true);
			chkWorkflowTab.setSelected(true);
			chkWorkflowReportTab.setSelected(true);
		}
		setVisible(true);

	}

	public void checkBoxValidation(String roles){
		Vector resultData=new Vector();
		StringTokenizer st = new StringTokenizer ( roles, ";");
		while(st.hasMoreTokens())
		{
			resultData.add(st.nextToken());
		}


		if(resultData.contains("DocType")){
			chkDocTypeTab.setSelected(true);
		}
		else{
			chkDocTypeTab.setSelected(false);
		}

		if(resultData.contains("AuditTrail"))
			chkAuditTrailTab.setSelected(true);
		else
			chkAuditTrailTab.setSelected(false);
		if(resultData.contains("ConnectedUser")){
			chkConnectedUserTab.setSelected(true);
		}
		else{
			chkConnectedUserTab.setSelected(false);
		}
		if(resultData.contains("LockedDocs"))
			chkLockedDocsTab.setSelected(true);
		else
			chkLockedDocsTab.setSelected(false);
		if(resultData.contains("Statistics"))
			chkStatisticsTab.setSelected(true);
		else
			chkStatisticsTab.setSelected(false);
		if(resultData.contains("Retention"))
			chkRetentionTab.setSelected(true);
		else
			chkRetentionTab.setSelected(false);
		if(resultData.contains("Notification"))
			chkNotificationTab.setSelected(true);
		else
			chkNotificationTab.setSelected(false);
		if(resultData.contains("WorkFlows"))
			chkWorkflowTab.setSelected(true);
		else
			chkWorkflowTab.setSelected(false);
		if(resultData.contains("WorkFlow Report"))
			chkWorkflowReportTab.setSelected(true);
		else
			chkWorkflowReportTab.setSelected(false);
		

	}
	public void setCurrentLocation(){
		Point point = AdminWise.adminFrame.getCurrentLocation(this.getSize().width, this.getSize().height);
		setLocation(point);
		setResizable(false);
	}
	public void setCurrentLocation(VWDlgReportSaveLocation dlg){
		Point point = AdminWise.adminFrame.getCurrentLocation(dlg.getSize().width, dlg.getSize().height);
		dlg.setLocation(point);
	}
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();

			if(object==btnCancel)
				BtnCancel_actionPerformed(event);
			if(object==btnSave)
				BtnSave_actionPerformed(event);
			if(object==btnRemove)
				BtnRemove_actionPerformed(event);
			if(object==chkDocTypeTab)
				chkDocType_actionPerformed(event);
			if(object==chkAuditTrailTab)
				chkAudit_actionPerformed(event);
			if(object==chkConnectedUserTab)
				chkConnected_actionPerformed(event);
			if(object==chkLockedDocsTab)
				chkLockedDocsTab_actionPerformed(event);
			if(object==chkStatisticsTab)
				chkStatisticsTab_actionPerformed(event);
			if(object==chkRetentionTab)
				chkRetentionTab_actionPerformed(event);
			if(object==chkNotificationTab)
				chkNotificationTab_actionPerformed(event);
			if(object==chkWorkflowTab)
				chkWorkflowTab_actionPerfrome(event);
			if(object==chkWorkflowReportTab)
				chkWorkflowReportTab_actionPerfrome(event);

			if (object == cmbSubAdmin){
				//CmbSubAdmin_actionPerformed(event);
				AdminWise.printToConsole("Inside combo action");
			}

		}
	}


	public static void loadGroups() {
		String namedGroup="";
		String professionalGroup="";
		String adminGroup="";
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		if (room == null || room.getConnectStatus() != Room_Status_Connect)
			return;
		gCurRoom=room.getId();
		subAdminLoaded=false;
		AdminWise.printToConsole("Inside load grops");
	
		Vector groupsData=new Vector();
		Vector userData=new Vector();		
		AdminWise.gConnector.ViewWiseClient.getGroupsData(gCurRoom, groupsData);
		AdminWise.gConnector.ViewWiseClient.getNamedProfessionalUser(gCurRoom, userData);
		namedGroup=userData.get(0).toString();
		professionalGroup=userData.get(1).toString();
		adminGroup=userData.get(2).toString();
		AdminWise.printToConsole("adminGroupName:::"+adminGroup);
		if ((groupsData != null)&&(groupsData.size()>0)) {
			cmbSubAdmin.removeAllItems();
			cmbSubAdmin.addItem(AdminWise.connectorManager.getString("subAdministrator.AvailableGroups"));
			for (int i = 0; i < groupsData.size(); i++){
				StringTokenizer st = new StringTokenizer( (String) groupsData.elementAt(i), "\t");
				String grouip=st.nextToken();
				String groupName=st.nextToken();
				if(!groupName.equalsIgnoreCase(namedGroup)){
					AdminWise.printToConsole("professionalGroup"+professionalGroup);
					if(!groupName.equalsIgnoreCase(professionalGroup)){
						if(!groupName.equalsIgnoreCase(adminGroup)){
							cmbSubAdmin.addItem(groupName);
						}
					}
				}
			}
		} else {
			cmbSubAdmin.removeAllItems();
			cmbSubAdmin.addItem(new String(AdminWise.connectorManager.getString("subAdministrator.AvailableGroups")));
			cmbSubAdmin.setEnabled(true);
		}

		subAdminLoaded=true;
	}
	


	void chkDocType_actionPerformed(ActionEvent event){
		if (chkDocTypeTab.isSelected()) {
			chkDocTypeValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkDocTypeValue=0;
		}
	}
	void chkAudit_actionPerformed(ActionEvent event){
		if (chkAuditTrailTab.isSelected()) {
			chkAuditValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkAuditValue=0;
		}
	}

	void chkConnected_actionPerformed(ActionEvent event){
		if (chkConnectedUserTab.isSelected()) {
			chkConnectedUserValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkConnectedUserValue=0;
		}
	}

	void chkLockedDocsTab_actionPerformed(ActionEvent event){
		if (chkLockedDocsTab.isSelected()) {
			chkLockedDocsValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkLockedDocsValue=0;
		}
	}

	void chkStatisticsTab_actionPerformed(ActionEvent event){
		if (chkStatisticsTab.isSelected()) {
			chkStatisticsValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkStatisticsValue=0;
		}
	}

	void chkRetentionTab_actionPerformed(ActionEvent event){
		if (chkRetentionTab.isSelected()) {
			chkRetentionValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkRetentionValue=0;
		}
	}

	void chkNotificationTab_actionPerformed(ActionEvent event){
		if (chkNotificationTab.isSelected()) {
			chkNotificationValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkNotificationValue=0;
		}
	}
	void chkWorkflowTab_actionPerfrome(ActionEvent event){
		if (chkWorkflowTab.isSelected()) {
			chkWorkflowValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkWorkflowValue=0;
		}
	}

	void chkWorkflowReportTab_actionPerfrome(ActionEvent event){
		if (chkWorkflowReportTab.isSelected()) {
			chkWorkflowReportValue=1;
			AdminWise.printToConsole("Inside check box is selected");
		} else {
			AdminWise.printToConsole("Inside else");
			chkWorkflowReportValue=0;
		}
	}
	
	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
		closeDialog();
	}
	

	public void BtnRemove_actionPerformed(ActionEvent event){
		String selectedGroupName="";
		String roles="";	
		if(!((cmbSubAdmin.getSelectedItem().toString()).equals(AdminWise.connectorManager.getString("subAdministrator.AvailableGroups"))))	
			selectedGroupName=	cmbSubAdmin.getSelectedItem().toString();
		AdminWise.gConnector.ViewWiseClient.removeSubAdminRoles(gCurRoom,selectedGroupName);
		this.closeDialog();
		cmbSubAdmin.setEnabled(true);
	//	cmbSubAdmin.setEditable(true);
		loadGroups();

	}



	public void BtnSave_actionPerformed(ActionEvent event){
		if(((cmbSubAdmin.getSelectedItem().toString()).equals(AdminWise.connectorManager.getString("subAdministrator.AvailableGroups")))){
			JOptionPane.showMessageDialog(this,"Please select a User");
			return;
		}

		Vector tabContents=new Vector();
		if((chkDocTypeTab.isSelected()==true)||chkDocTypeValue==1){

			tabContents.add("DocType");
		}
		if((chkAuditTrailTab.isSelected()==true)||chkAuditValue==1){
			tabContents.add("AuditTrail");
		}
		if((chkConnectedUserTab.isSelected()==true)||chkConnectedUserValue==1){

			tabContents.add("ConnectedUser");
		}
		if((chkLockedDocsTab.isSelected()==true)||chkLockedDocsValue==1){
			tabContents.add("LockedDocs");
		}
		if((chkStatisticsTab.isSelected()==true)||chkStatisticsValue==1){

			tabContents.add("Statistics");
		}
		if((chkRetentionTab.isSelected()==true)||chkRetentionValue==1){
			tabContents.add("Retention");
		}
		if((chkNotificationTab.isSelected()==true)||chkNotificationValue==1){
			tabContents.add("Notification");
		}

		if((chkWorkflowTab.isSelected()==true)||chkWorkflowValue==1){
			tabContents.add("WorkFlows");
		}

		if((chkWorkflowReportTab.isSelected()==true)||chkWorkflowReportValue==1){
			tabContents.add("WorkFlow Report");
		}

		String selectedUserName="";
		String roles="";	
		if(!((cmbSubAdmin.getSelectedItem().toString()).equals(AdminWise.connectorManager.getString("subAdministrator.AvailableGroups"))))	
			selectedUserName=	cmbSubAdmin.getSelectedItem().toString();
		AdminWise.printToConsole("selectedUserName >>>>>>>>>>>>>>>>>>"+selectedUserName);
		if(tabContents.size()==0){
			JOptionPane.showMessageDialog(this,"Please select atleast one tab");
			return;
		}
		else{	
			if(tabContents.size()>0){
				for(int i=0;i<tabContents.size();i++){
					if(roles.equals(""))
						roles=roles+tabContents.get(i);
					else
						roles=roles+";"+tabContents.get(i);
				}
				AdminWise.printToConsole("before calling setSubAdminRoles........"+gCurRoom);
				AdminWise.gConnector.ViewWiseClient.setSubAdminRoles(gCurRoom,selectedUserName,roles,updFlag);
				AdminWise.printToConsole("after calling setSubAdminRoles........"+updFlag);
			}
		}
		this.closeDialog();

	}
	
	private void closeDialog() {
		setVisible(false);
		dispose();
	}

}
