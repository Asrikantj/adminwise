/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWPrincipalTable<br>
 *
 * @version     $Revision: 1.9 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWPrincipal;

import java.util.Vector;

import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWPanel.VWAdminPanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import com.computhink.common.Principal;
import java.util.Comparator;
import javax.swing.table.TableColumnModel;
import java.util.Collections;
import javax.swing.event.TableModelEvent;
import ViewWise.AdminWise.VWTable.*;

public class VWPrincipalTable extends JTable implements VWConstant {
    protected VWPrincipalTableData m_data;
    TableColumn[] ColModel=new TableColumn[3];
    static boolean[] visibleCol={true,true,true};
    int tabStatusValue=0;
    JMenuItem menuBackupRestore = new JMenuItem();
    JPopupMenu popupMenu=null;
    public static String selectedChar = "";
    public VWPrincipalTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWPrincipalTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        ///setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k=0;k<VWPrincipalTableData.m_columns.length; k++) {
            TableCellRenderer renderer;
            ColoredTableCellRenderer textRenderer=new ColoredTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWPrincipalTableData.m_columns[k].m_alignment);
            renderer=textRenderer;
            VWPrincipalRowEditor editor=new VWPrincipalRowEditor(this);
            TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWPrincipalTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        setVisibleCols();
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        getColumnModel().getColumn(0).setPreferredWidth(250);
        getColumnModel().getColumn(1).setPreferredWidth(250);
        getColumnModel().getColumn(2).setPreferredWidth(250);
        getColumnModel().getColumn(3).setPreferredWidth(250);
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (!listSelectionModel.isSelectionEmpty())
                    AdminWise.adminPanel.principalPanel.setEnableMode(MODE_SELECT);
                else
                    AdminWise.adminPanel.principalPanel.setEnableMode(MODE_UNSELECTED);
            }
        });
        setRowHeight(TableRowHeight);
        
        setTableResizable();
    }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    private void setVisibleCols() {
        TableColumnModel model=getColumnModel();
        
        int count =visibleCol.length;
        int i=0;
        while(i<count) {
            if (!visibleCol[i]) {
                TableColumn column = model.getColumn(i);
                model.removeColumn(column);
                count--;
            }
            else {
                i++;
            }
        }
        if(count==0)return;
        Dimension tableWith = getPreferredScrollableViewportSize();
        int colWidth=(int)Math.round(tableWith.getWidth()/count);
        for (i=0;i<count;i++) {
            model.getColumn(i).setPreferredWidth(colWidth);
        }
        setColumnModel(model);
    }
    
    //  --------------------------------------------------------------------------
    class SymKey implements KeyListener {
    	public void keyPressed(KeyEvent e){
    	}

    	public void keyReleased(KeyEvent e){
    		final int object = e.getKeyCode();
    		if(object == KeyEvent.VK_DOWN ||  object == KeyEvent.VK_UP)
    			lSingleClick(null, 0, 0);
    	}
    	public void keyTyped(KeyEvent e){
    	}
    }

    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWPrincipalTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWPrincipalTable_LeftMouseClicked(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWPrincipalTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    void VWPrincipalTable_RightMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
   
        if(getSelectedRow()==-1) return;
        popupMenu=new JPopupMenu("");
        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.add(menuBackupRestore);
    	Principal principal = getPrincipal();
    	Vector adminGroup=new Vector();           
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	AdminWise.gConnector.ViewWiseClient.getAdminGroup(room.getId(),adminGroup);
		int enableSecurityAdmin=AdminWise.gConnector.ViewWiseClient.enableSecurityAdmin(room.getId());
    	int securityAdmin=AdminWise.gConnector.ViewWiseClient.isSecurityAdmin(room.getId());
    	if(principal.getName().equalsIgnoreCase(adminGroup.get(0).toString())&&(enableSecurityAdmin==1)&&(securityAdmin==1)){
    		Vector tabStatus=new Vector();
			AdminWise.gConnector.ViewWiseClient.getTabStatus(room.getId(), tabStatus);
			if(tabStatus.size()>0&&tabStatus!=null){
				tabStatusValue=Integer.parseInt(tabStatus.get(0).toString());
				if(tabStatusValue==0){
					menuBackupRestore.setLabel("Enable Backup Restore");
				}
				else{
					menuBackupRestore.setLabel("Disable Backup Restore");
				}

			}
        popupMenu.show(this,event.getX(),event.getY());
    	}
        
    	menuBackupRestore.addActionListener(new java.awt.event.ActionListener() {
    		public void actionPerformed(java.awt.event.ActionEvent evt) {
    			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    			if(tabStatusValue==0){
    				AdminWise.gConnector.ViewWiseClient.setToggleStatus(room.getId(),1);
    				menuBackupRestore.setLabel("Disable Backup Restore");
    				AdminWise.gConnector.ViewWiseClient.setTabStatus(room.getId(), 1);
    			}else{
    				AdminWise.gConnector.ViewWiseClient.setToggleStatus(room.getId(),0);
    				menuBackupRestore.setLabel("Enable Backup Restore");
    				AdminWise.gConnector.ViewWiseClient.setTabStatus(room.getId(), 0);
    			}
    		}
    	});
    }
    //--------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
    	if(getRowType()==Principal.USER_TYPE){
    		VWPrincipalPanel.BtnEmailID.setEnabled(true);
    		VWPrincipalPanel.BtnChangePassword.setEnabled(true);
    	}else if(getRowType()==Principal.GROUP_TYPE){
    		VWPrincipalPanel.BtnEmailID.setEnabled(false);
    		VWPrincipalPanel.BtnChangePassword.setEnabled(false);
    	}
    }
    //--------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    public void addData(List list) {
        m_data.setData(list);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void addData(Vector data) {
        m_data.setData(data);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public Vector getData() {
        return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public String[][] getReportData() {
        Vector tableData=m_data.getData();
        int count=tableData.size();
        String[][] repData=new String[count][4];
        for(int i=0;i<count;i++) {
            Principal principal=(Principal)tableData.get(i);
            repData[i][0]=principal.getName();
            repData[i][1]=principal.getType()==Principal.GROUP_TYPE?"Group":"User";
            if(principal.isHasCSign() && principal.isHasASign())
            {
                repData[i][2]="Authorized/Certified";
            }
            else if(principal.isHasASign())
            {
                repData[i][2]="Authorized";
            }
            else if(principal.isHasCSign())
            {
                repData[i][2]="Certified";
            }
            else
            {
                repData[i][2]="NF";
            }           
            repData[i][3] =  (principal.getEmail().trim().equals("")) ? "-" : principal.getEmail(); 
        }
        return repData;
    }
    //--------------------------------------------------------------------------
    public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    // DRS related changes
    public void UpdateEmail(int row) {
        m_data.fireTableDataChanged();
    }
    
    public void insertData(Vector data) {
        for(int i=0;i<data.size();i++) {
            m_data.insert((Principal)data.get(i));
        }
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void insertData(Principal principal) {
        m_data.insert(principal);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public Principal getRowData(int rowNum) {
        return m_data.getRowData(rowNum);
    }
    //--------------------------------------------------------------------------
    public int getRowId(int rowNum) {
        Principal principal=m_data.getRowData(rowNum);
        return principal.getId();
    }
    //--------------------------------------------------------------------------
    public int getRowId() {
        Principal principal=m_data.getRowData(getSelectedRow());
        return principal.getId();
    }
    //--------------------------------------------------------------------------
    public String getRowName(int rowNum) {
        Principal principal=m_data.getRowData(rowNum);
        return principal.getName();
    }
    //--------------------------------------------------------------------------
    //Functions for getting Email ID fields...
    public String getRowEmailID(int rowNum) {
        Principal principal=m_data.getRowData(rowNum);
        return principal.getEmail();
    }
    //--------------------------------------------------------------------------
    public String getRowEmailID() {
        return getRowEmailID(getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public String getRowName() {
        return getRowName(getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public int getRowType() {
        return getRowType(getSelectedRow());
    }
    //--------------------------------------------------------------------------
    public int getRowType(int rowNum) {
        Principal principal=m_data.getRowData(rowNum);
        return principal.getType();
    }
    //--------------------------------------------------------------------------
    public Principal getPrincipal() {
        Principal principal=m_data.getRowData(getSelectedRow());
        return principal;
    }
    //--------------------------------------------------------------------------
    public Principal getPrincipal(int rowNum) {
        Principal principal=m_data.getRowData(rowNum);
        return principal;
    }
    //--------------------------------------------------------------------------
    public void clearData() {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
}
class PrincipalColumnData {
    public String  m_title;
    float m_width;
    int m_alignment;
    
    public PrincipalColumnData(String title, float width, int alignment) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
    }
}
//--------------------------------------------------------------------------
class VWPrincipalTableData extends AbstractTableModel {
    public static final PrincipalColumnData m_columns[] = {
        new PrincipalColumnData(VWConstant.SignatureColumnNames[0],0.5F,JLabel.LEFT ),
        new PrincipalColumnData(VWConstant.SignatureColumnNames[1],0.1F, JLabel.LEFT),
        new PrincipalColumnData(VWConstant.SignatureColumnNames[2],0.4F, JLabel.LEFT),
        new PrincipalColumnData(VWConstant.SignatureColumnNames[3],0.4F, JLabel.LEFT)
    };
    public static final int COL_NAME = 0;
    public static final int COL_TYPE = 1;
    public static final int COL_SIGN = 2;
    public static final int COL_EMAILID = 3;
    
    protected VWPrincipalTable m_parent;
    protected Vector m_vector;
    protected int m_sortCol = 0;
    protected boolean m_sortAsc = true;
    
    public VWPrincipalTableData(VWPrincipalTable parent) {
        m_parent = parent;
        m_vector = new Vector();
    }
    //--------------------------------------------------------------------------
    public void setData(List data) {
        m_vector.removeAllElements();
        if (data==null) return;
        int count =data.size();
        for(int i=0;i<count;i++)
            m_vector.addElement(new Principal((String)data.get(i)));
    }
    //--------------------------------------------------------------------------
    public void setData(Vector data) {
        m_vector.removeAllElements();
        int count =data.size();
        for(int i=0;i<count;i++) {
            m_vector.addElement((Principal)data.get(i));
        }
    }
    //--------------------------------------------------------------------------
    public Vector getData() {
        return m_vector;
    }
    //--------------------------------------------------------------------------
    public Principal getRowData(int rowNum) {
        return (Principal)m_vector.elementAt(rowNum);
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return m_vector==null ? 0 : m_vector.size();
    }
    //--------------------------------------------------------------------------
    public int getColumnCount() {
        return m_columns.length;
    }
    //--------------------------------------------------------------------------
    public String getColumnName(int column) {
        String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
            return str;
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
        return true;
    }
    //--------------------------------------------------------------------------
    public Object getValueAt(int nRow, int nCol) {
        if (nRow < 0 || nRow>=getRowCount())
            return "";
        Principal row = (Principal)m_vector.elementAt(nRow);        
        switch (nCol) {
        case COL_NAME:
        	AdminWise.printToConsole(row.getName()+" is Admin :"+row.isAdmin());
        	AdminWise.printToConsole(row.getName()+" is SubAdmin :"+row.isSubAdmin());
            if(row.isAdmin())
                return new ColorData(row.getName(),ColorData.RED);
            else if(row.isSubAdmin())
                return new ColorData(row.getName(),java.awt.Color.blue);
            return row.getName();
        case COL_TYPE:
            if(row.getType()==Principal.GROUP_TYPE)
                return "Group";
            return "User";
        case COL_SIGN:
            if(row.isHasCSign() && row.isHasASign())
            {
                return "Authorized/Certified";
            }
            else if(row.isHasASign())
            {
                return "Authorized";
            }
            else if(row.isHasCSign())
            {
                return "Certified";
            }
            else
            {
                return "NF";
            }
        case COL_EMAILID:
        	return row.getEmail();
        }
        return "";
    }
    //--------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
        if (nRow < 0 || nRow >= getRowCount())
            return;
        Principal row = (Principal)m_vector.elementAt(nRow);
        String svalue = value.toString();
        switch (nCol) {
        case COL_NAME:
            row.setName(svalue);
            break;
        case COL_TYPE:
            row.setType(VWUtil.to_Number(svalue));
            break;
        case COL_SIGN:
        }
    }
    //--------------------------------------------------------------------------
    public void clear() {
        m_vector.removeAllElements();
    }
    //--------------------------------------------------------------------------
    public void insert(Principal principal) {
        m_vector.addElement(principal);
    }
    //--------------------------------------------------------------------------
    public boolean remove(int row){
        if (row < 0 || row >= m_vector.size())
            return false;
        m_vector.remove(row);
        return true;
    }
    //--------------------------------------------------------------------------
    class ColumnListener extends MouseAdapter {
        protected VWPrincipalTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWPrincipalTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
            
            if(e.getModifiers()==e.BUTTON3_MASK)
                selectViewCol(e);
            else if(e.getModifiers()==e.BUTTON1_MASK)
                sortCol(e);
        }
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new PrincipalComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWPrincipalTableData.this));
            m_table.repaint();
        }
    //--------------------------------------------------------------------------
        private void selectViewCol(MouseEvent e) {
            javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
            for (int i=0; i < m_table.SignatureColumnNames.length; i++){
                javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
                m_table.SignatureColumnNames[i],m_table.visibleCol[i]);
                TableColumn column = m_table.ColModel[i];
                subMenu.addActionListener(new ColumnKeeper(column,VWPrincipalTableData.m_columns[i]));
                menu.add(subMenu);
            }
            menu.show(m_table,e.getX(),e.getY());
        }
    //--------------------------------------------------------------------------
        class ColumnKeeper implements java.awt.event.ActionListener {
            protected TableColumn m_column;
            protected PrincipalColumnData  m_colData;
            
            public ColumnKeeper(TableColumn column,PrincipalColumnData colData){
                m_column = column;
                m_colData = colData;
            }
    //--------------------------------------------------------------------------
            public void actionPerformed(java.awt.event.ActionEvent e) {
                javax.swing.JCheckBoxMenuItem item=(javax.swing.JCheckBoxMenuItem)e.getSource();
                TableColumnModel model = m_table.getColumnModel();
                boolean found=false;
                int i=0;
                int count=m_table.SignatureColumnNames.length;
                int colCount=model.getColumnCount();
                while (i<count && !found){
                    if(m_table.SignatureColumnNames[i].equals(e.getActionCommand()))
                        found=true;
                    i++;
                }
                i--;
                if (item.isSelected()) {
                    m_table.visibleCol[i]=!m_table.visibleCol[i];
                    model.addColumn(m_column);
                }
                else {
                    if(colCount>1) {
                        m_table.visibleCol[i]=!m_table.visibleCol[i];
                        model.removeColumn(m_column);
                    }
                }
                m_table.tableChanged(new javax.swing.event.TableModelEvent(m_table.m_data));
                m_table.repaint();
            }
        }
    }
    //--------------------------------------------------------------------------
    class PrincipalComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public PrincipalComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof Principal) || !(o2 instanceof Principal))
                return 0;
            Principal s1 = (Principal)o1;
            Principal s2 = (Principal)o2;
            int result = 0;
            double d1, d2;
            switch (m_sortCol) {
                case COL_NAME:
                    result = s1.getName().toLowerCase().compareTo(s2.getName().toLowerCase());
                    break;
                case COL_TYPE:
                    result = String.valueOf(s1.getType()).compareTo
                        (String.valueOf(s2.getType()));
                    break;
                case COL_SIGN:
                	String firstData = "", secondData = "";
                    if(s1.isHasCSign() && s1.isHasASign()){
                        firstData =  "Authorized/Certified";
                    }else if(s1.isHasASign()){
                    	firstData =   "Authorized";
                    }else if(s1.isHasCSign()){
                    	firstData =   "Certified";
                    }else
                    	firstData =   "NF";
                   
                    
                    if(s2.isHasCSign() && s2.isHasASign()){
                    	secondData =  "Authorized/Certified";
                    }else if(s2.isHasASign()){
                    	secondData =   "Authorized";
                    }else if(s2.isHasCSign()){
                    	secondData =   "Certified";
                    }else
                    	secondData =   "NF";
                   
                	result = firstData.toLowerCase().compareTo(secondData.toLowerCase());
                	break;
                case COL_EMAILID:
                	result = s1.getEmail().toLowerCase().compareTo(s2.getEmail().toLowerCase());
                	break;
            }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof PrincipalComparator) {
                PrincipalComparator compObj = (PrincipalComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    }
}