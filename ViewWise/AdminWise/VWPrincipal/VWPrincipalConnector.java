/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWPrincipalConnector<br>
 *
 * @version     $Revision: 1.8 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWPrincipal;

import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;

import com.computhink.common.Constants;
import com.computhink.common.Principal;
import com.computhink.common.RouteMasterInfo;

import java.io.File;
import ViewWise.AdminWise.VWUtil.VWProperties;
import com.computhink.vwc.VWCUtil;
import com.computhink.common.Signature;

public class VWPrincipalConnector implements VWConstant {

    public static Vector getPrincipals() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector principals=new Vector();
        AdminWise.gConnector.ViewWiseClient.getPrincipals(room.getId(), principals);
        return principals;
    }
    //--------------------------------------------------------------------------
    public static void updateServerUsers() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        AdminWise.gConnector.ViewWiseClient.resyncDirectory(room.getId());
    }
    //--------------------------------------------------------------------------
    public static void deletePrincipal(Principal principal) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        AdminWise.gConnector.ViewWiseClient.removePrincipal(room.getId(),
        principal);
    }
    //--------------------------------------------------------------------------
    public static Vector getDirectoryPrincipals() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect)
            return null;
        Vector principals=new Vector();
        AdminWise.gConnector.ViewWiseClient.getDirectoryPrincipals(room.getId(),
        principals);
        return principals;
    }
  //--------------------------------------------------------------------------
  //Method added  for implementing ldap User/Groups search optimization in LDAP 18/9/2018
    public static Vector getDirectoryPrincipalsNew(String searchPrincipalName) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect)
            return null;
        /**CV2019 - Administration add dialog User/Group search issue fix**/
        Vector<Principal> principals = AdminWise.gConnector.ViewWiseClient.getDirectoryPrincipalsNew(room.getId(), searchPrincipalName);
        return principals;
    }
     //--------------------------------------------------------------------------
    public static int syncPrincipals() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        return AdminWise.gConnector.ViewWiseClient.syncPrincipals(room.getId());
    }
	/****CV2019 merges from 10.2 line--------------------------------***/
    public static int syncPrincipals(Principal principal) {
    	AdminWise.printToConsole("Inside syncprincipals type ::::"+principal.getType());
    	AdminWise.printToConsole("Inside syncprincipals name::::"+principal.getName());
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        return AdminWise.gConnector.ViewWiseClient.syncPrincipals(room.getId(), principal);
    }
    //--------------------------------------------------------------------------
    public static int addPrincipal(Principal principal) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        return AdminWise.gConnector.ViewWiseClient.addPrincipal(room.getId(), principal);
    }
    //--------------------------------------------------------------------------
    public static int addUserSign(String signStr,int signType) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        Principal principal=AdminWise.adminPanel.principalPanel.getSelectedPrincipal();
        if(principal.getType()==Principal.USER_TYPE)
        {
            File signFolder=new File(
                System.getProperty("java.io.tmpdir")+"sign");
            signFolder.mkdirs();
            File signFile=new File(signFolder,  principal.getName()+".emf");
            VWCUtil.createSignImage(signFile.getPath(), signStr);
            return AdminWise.gConnector.ViewWiseClient.setUserSign(room.getId(),
                principal.getName(), signType, signFile.getPath());
        }
        return 0;
    }
    //--------------------------------------------------------------------------
    public static int delUserSign(int signType) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return -1;
        Principal principal=AdminWise.adminPanel.principalPanel.getSelectedPrincipal();
        if(principal.getType()==Principal.USER_TYPE)
        {
            return AdminWise.gConnector.ViewWiseClient.removeUserSign(room.getId(), 
                principal.getName(), signType);
        }
        return -1;
    }
    //--------------------------------------------------------------------------
    public static int addUserSignTemplate(Vector signs) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int ret=0;
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return 0;
        Principal principal=AdminWise.adminPanel.principalPanel.getSelectedPrincipal();
        if(principal.getType()==Principal.USER_TYPE)
        {
            File signFile=new File(
                System.getProperty("java.io.tmpdir"),"sign"+File.separatorChar+principal.getName()+".tmp");
            ret=AdminWise.gConnector.ViewWiseClient.createSignTemplate(signs,signFile.getPath());
            if(ret < 0) return ret;
            /*
            String signTmplateStr=VWCUtil.createSign(signs);
            VWUtil.writeStringToFile(signTmplateStr,signFile.getPath());
            */
            addUserSign((String)signs.get(0),Signature.Certified_Type);
            return AdminWise.gConnector.ViewWiseClient.setUserSign(room.getId(),
                principal.getName(), Signature.Template_Type, signFile.getPath());
        }
        return 0;
    }
    
    public static int isVWSignInstalled(int roomId)
    { 
        return AdminWise.gConnector.ViewWiseClient.isVWSignInstalled(roomId);
    }
    //-------------------------------------------------------------------------- 
    //DRS related changes
    //Function to update mail principal for mail.
    public static void updatePrincipal(Principal principal) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        AdminWise.gConnector.ViewWiseClient.updatePrincipal(room.getId(),principal);
    }
    //--------------------------------------------------------------------------
    public static String checkUserInRoute(int principalId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector result = new Vector();
        int retValue = AdminWise.gConnector.ViewWiseClient.isUserInRoute(room.getId(), principalId, result);
        String routeList = "";
        if (retValue > 0 && result != null){
            for (int index = 0; index < result.size(); index++){
        	routeList += result.get(index).toString() + "\n";        	
            }
        }
        return routeList;
    }
    public static void checkGroupsInRoute(Vector routeList){	
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();        
        int retValue = AdminWise.gConnector.ViewWiseClient.checkGroupsInRoute(room.getId(), routeList);
    }

    public static void syncRouteGroups(String processFlag){	
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();        
        int retValue = AdminWise.gConnector.ViewWiseClient.syncRouteGroups(room.getId(), processFlag);
        int delEmptyUsers = AdminWise.gConnector.ViewWiseClient.delEmptyUsers(room.getId());
    }

    public static void endRouteStatus(Vector routeList){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	String actionResult = Constants.Route_Status_End;
	String comments =Constants.PRODUCT_NAME + " Ended for group synchronizing.";
	Vector routeEndedDocList = new Vector();    
	for (int i = 0; i < routeList.size(); i++){
	    RouteMasterInfo master = (RouteMasterInfo) routeList.get(i);
	    if (routeEndedDocList.contains(master.getDocId())) continue;	    
	    master.setComments(comments);
	    AdminWise.gConnector.ViewWiseClient.moveDocOnApproveReject(room.getId(), master, actionResult, comments, -1);
	    routeEndedDocList.add(master.getDocId());
	}
    }

    public static void seDRSEnable(String status ){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	AdminWise.gConnector.ViewWiseClient.setDRSEnabled(room.getId(), status);
    }

    public static int isDRSEnabled(){
	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	int drsStatus = AdminWise.gConnector.ViewWiseClient.isDRSEnabled(room.getId());
	return drsStatus;
    }
    
    /**
     * Function ChangeUserPWD
     * Input oldPassword newPassword confirmPassword
     * Output Integer
     */
    public static int ChangeUserPWD(Principal principal,
    		String oldPassword, String newPassword, String confirmPassword) {

    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	return AdminWise.gConnector.ViewWiseClient.ChangeUserPWD(room.getId(), principal, oldPassword, newPassword, confirmPassword);
    }
	public static int resetUserPWD(Principal selectedPrincipal,
			String oldPassword, String newPassword, String confirmPassword) {
		// TODO Auto-generated method stub
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	return AdminWise.gConnector.ViewWiseClient.resetUserPWD(room.getId(), selectedPrincipal, oldPassword, newPassword, confirmPassword);
		
	}
	
	   public static int generateNodeSecurityReport(String selectedPrincipal,int reportNodeId,int checkBoxValue,Vector reportData) {

	    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	    	return AdminWise.gConnector.ViewWiseClient.generateNodeSecurityReport( room.getId(),selectedPrincipal, reportNodeId, checkBoxValue,reportData);
	    }
	
	
}