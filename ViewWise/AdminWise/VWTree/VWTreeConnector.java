/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWTreeConnector<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWTree;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.util.Hashtable;
import java.util.Enumeration;
import javax.swing.tree.DefaultMutableTreeNode;

import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.AdminWise;
import javax.swing.tree.TreePath;
import javax.swing.CellEditor;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.plaf.TreeUI;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWUtil.VWDataStorage;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWRoom.VWRoom;
import java.util.Date;
import com.computhink.common.Node;

public class VWTreeConnector implements VWConstant
{
    public static DefaultTreeModel loadVWRooms(){
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null ;
        VWTreeNode rootNode=new VWTreeNode(0,room.getName(),Room_TYPE,0,room.getId());
        rootNode=loadNodeChildren(rootNode);
        DefaultTreeModel treeModel=new DefaultTreeModel(rootNode);
        tree.setModel(treeModel);
        tree.setSelectionRow(0);
        return treeModel;
    }
//------------------------------------------------------------------------------
    public static DefaultTreeModel loadVWRooms(List nodeList, Vector securityList, Vector nodeProperties){
    	//AdminWise.printToConsole("VWT loadVWRooms " + securityList.size());
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        VWTreeNode rootNode=new VWTreeNode(0,room.getName(),Room_TYPE,0,room.getId());
        rootNode=loadNodeChildren(rootNode,nodeList, securityList, nodeProperties);
        DefaultTreeModel treeModel=new DefaultTreeModel(rootNode);
        tree.setModel(treeModel);
        tree.setSelectionRow(0);
        return treeModel;
    }
//------------------------------------------------------------------------------
    private static boolean nodeExist(List list,int nodeId,int begin)
    {    
        int count =list.size();
        for(int i=begin;i<count;i++)
        {
            VWTreeNode newNode=(VWTreeNode)list.get(i);
            if(newNode.getId()==nodeId) return true;
        }
        return false;
    }
//------------------------------------------------------------------------------
    private static VWTreeNode getParentNode(int roomId,int parentId,Hashtable roomNodes){
        
        return (VWTreeNode)roomNodes.get(new String(roomId+"_"+parentId));
    }
//------------------------------------------------------------------------------    
    private static int fixNodeType(VWTreeNode parent)
    {
        if (parent.getType()==Room_TYPE)
            return Cabinet_TYPE;
        else if(parent.getType()==Cabinet_TYPE)
            return Drawer_TYPE;
        else if(parent.getType()==Drawer_TYPE)
            return Folder_TYPE;
        else if(parent.getType()==Folder_TYPE)
            return Folder_TYPE;
        return  Folder_TYPE;
    }
//------------------------------------------------------------------------------
    public static int getNextType(int curType)
    {
        if (curType==Folder_TYPE)
            return curType;
        return curType+1;
    }
//------------------------------------------------------------------------------
    public static VWTreeNode loadNodeChildren(VWTreeNode node)
    {
        if (node==null) return node;
        node.removeAllChildren();
        ///tree.collapsePath(nodePath);
        int serverId=node.getRoomId();
        if(serverId==0) return node;
        return loadRoomData(getNodeData(serverId,node),node);
        ///return loadRoomData(getRoomData(serverId),node);
    }
//------------------------------------------------------------------------------
    public static VWTreeNode loadNodeChildren(VWTreeNode node,List nodeList, Vector securityList, Vector nodeProperties)
    {
        if (node==null) return node;
        node.removeAllChildren();
        ///tree.collapsePath(nodePath);
        int serverId=node.getRoomId();
        if(serverId==0) return node;
        return loadRoomData(nodeList,node, securityList, nodeProperties);
    }
//------------------------------------------------------------------------------
    public static List getRoomData(int serverId)
    {
        Vector nodes=new Vector();
        Node serverNode=new Node(0);
        AdminWise.gConnector.ViewWiseClient.getNodeContents(serverId, serverNode, 0, nodes);
        if (nodes==null || nodes.size()==0) return null;
        ///VWTreeNode parentNode=pNode;
        List list=new LinkedList();
        int dataCount=nodes.size();
        for(int i=0;i<dataCount;i++)
        {
            VWTreeNode node = new VWTreeNode("");
            node.setTreeNodeInfo((Node)nodes.get(i));
            node.setRoomId(serverId);
            list.add(node);
        }
        return list;
    }
//------------------------------------------------------------------------------
    public static VWTreeNode loadRoomData(List nodeList,VWTreeNode pNode){
    	return loadRoomData(nodeList, pNode, null, null);
    }
    public static VWTreeNode loadRoomData(List nodeList,VWTreeNode pNode, Vector securityList, Vector nodeProperties){
        int nodeCount=0;
        if (pNode==null) return pNode;
        List child = new LinkedList();
        int roomId=pNode.getRoomId();
        ///DefaultTreeModel treeModel = (DefaultTreeModel)tree.getModel();
        if(nodeList==null || nodeList.size()==0) return pNode;
        nodeCount=nodeList.size();
        VWTreeNode parentFolder=pNode;
        VWTreeNode setParentFolder=pNode;
        Hashtable roomNodes=new Hashtable();
        for(int i=0;i<nodeCount;i++)
        {
            VWTreeNode newNode=(VWTreeNode)nodeList.get(i);
            if(newNode.getParentId()==pNode.getId())
            {
                parentFolder=pNode;
            }
            else
            {
                VWTreeNode tmpParentNode=getParentNode(roomId,newNode.getParentId(),roomNodes);
                if(tmpParentNode==null)
                {
                    if(nodeExist(nodeList,newNode.getParentId(),i+1))
                    {
                        nodeList.add(nodeList.get(i));
                        nodeCount++;
                    }
                    continue;
                }
                else
                {   
                    int parentId=tmpParentNode.getId();
                    boolean found=false;
                    for(int j=0;j<parentFolder.getChildCount();j++)
                        if(parentId==((VWTreeNode)parentFolder.getChildAt(j)).getId())
                        {
                            parentFolder=(VWTreeNode)parentFolder.getChildAt(j);
                            found=true;
                            break;
                        }
                        if(!found) 
                        {
                            gNode=null;
                            getRefParent(parentId,pNode,false);
                            parentFolder=gNode;
                        }
                }
            }
            if (newNode.getType()==0) newNode.setType(fixNodeType(parentFolder));
            //// add the security permission to node object
            try{            	
            	//AdminWise.printToConsole("Security Size " + securityList.size());
            	if (securityList != null && securityList.size() > 0){
            		int len = securityList.size();
            		String buffer = securityList.get(len-1).toString();
            	//AdminWise.printToConsole(" isContain : "  + buffer);
            	if (buffer.toUpperCase().indexOf(newNode.getName().toUpperCase()) != -1){
            		//AdminWise.printToConsole(" Security Exist for this node!");
            		TreePath tp=new TreePath(parentFolder.getPath());
            		Object[] pathes=tp.getPath();
                    int pCount=pathes.length;
                    String nodePath="";
                    for(int p=1;p<pCount;p++){
                        nodePath+="\\"+pathes[p].toString();
                    }
                    nodePath+="\\"+newNode.getName();
                    //AdminWise.printToConsole(" newNode path " + nodePath + " : " + newNode.getName());
                    for(int count=0; count < securityList.size()-1; count++){
                        VWTreeNode node = (VWTreeNode) securityList.get(count);
                    	//AdminWise.printToConsole(" security VWpath " + node.getVWPath());
                        if (node.getVWPath().equalsIgnoreCase(nodePath)){
                        	//AdminWise.printToConsole(" security path " + node.getVWPath());  	
                        	newNode.setUserGroupSecurityList(node.getUserGroupSecurityList());
                        	newNode.setVWPath(nodePath);                        	
                        }
                    }
            	}
            	}
            	
            }catch(Exception ex){
            	//AdminWise.printToConsole(" Ex in Security update " + ex.getMessage());
            }
            
            ///// add the node Prperties
            
            try{            	
            	//AdminWise.printToConsole("Node Properties Size " + nodeProperties.size());
            	if (nodeProperties != null && nodeProperties.size() > 0){
            		int len = nodeProperties.size();
            		String buffer = nodeProperties.get(len-1).toString();            	
            	if (buffer.indexOf(newNode.getName()) != -1){
            		//AdminWise.printToConsole(" Node Properties Exist for this node!" + newNode.getName());
            		TreePath tp=new TreePath(parentFolder.getPath());
            		Object[] pathes=tp.getPath();
                    int pCount=pathes.length;
                    String nodePath="";
                    for(int p=1;p<pCount;p++){
                        nodePath+="\\"+pathes[p].toString();
                    }
                    nodePath+="\\"+newNode.getName();
                    //AdminWise.printToConsole(" newNode path " + nodePath + " : " + newNode.getName());
                    for(int count=0; count < nodeProperties.size()-1; count++){
                        VWTreeNode node = (VWTreeNode) nodeProperties.get(count);
                    	//AdminWise.printToConsole(" Node Properties VWpath " + node.getVWPath());
                        if (node.getVWPath().equalsIgnoreCase(nodePath)){
                        	//AdminWise.printToConsole(" security path " + node.getVWPath());
                        	newNode.setDefaultDocType(node.getDefaultDocType()); 
                        	newNode.setNodePropertiesList(node.getNodePropertiesList());
                        	newNode.setVWPath(nodePath);                        	
                        	//AdminWise.printToConsole(" Default document type " + node.getDefaultDocType());
                        	//AdminWise.printToConsole(" NodePropertiesList " + node.getNodePropertiesList());
                        }
                    }
            	}
            	}
            	
            }catch(Exception ex){
            	//AdminWise.printToConsole(" Ex in Security update " + ex.getMessage());
            }
            
            parentFolder.add((DefaultMutableTreeNode)newNode);
            /*
            treeModel.insertNodeInto((DefaultMutableTreeNode)newNode,
            (DefaultMutableTreeNode)parentFolder,
            ((DefaultMutableTreeNode)parentFolder).getChildCount());
             **/
            if(!roomNodes.containsKey(new String(roomId + "_" +newNode.getId())))
                roomNodes.put(new String(roomId + "_" + newNode.getId()),newNode);
            newNode=null;
        }
        return pNode;
    }    
//------------------------------------------------------------------------------
    public static boolean setTreeNodeSelection(int nodeId,boolean find)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return false;
        Vector nodes=new Vector();
        /**
         * CV10.2 - Updated for workflow report enhancement changes
        */
        int parentId = 0;
        int levels = 1;
        AdminWise.gConnector.ViewWiseClient.getNodeParents(room.getId(), new Node(nodeId), nodes);     
        AdminWise.printToConsole("getNodeParents result :"+nodes);
        if(nodes==null||nodes.size()==0) return false;
        int count =nodes.size();
        int[] nodeIds=new int[count];
        for(int i =0;i<count;i++)   {
        	nodeIds[i]=((Node)nodes.get(i)).getId();
        }
        if(nodeIds!=null && nodeIds.length>0){
        	if(find)
            	parentId = nodeIds[0];
        	else
        		parentId = nodeIds[1];
        }
        int pathCount = find?count+levels:count+levels-1;
        VWTreeNode[] treeModel=new VWTreeNode[pathCount];
        treeModel[0]=(VWTreeNode)tree.getPathForRow(0).getLastPathComponent();
 
        VWTreeNode parentNode=treeModel[0];
        TreeModel actTreeModel = tree.getModel();
        int lastRecIndex=find?0:1;
        for(int i=count-1,j=1;i>=lastRecIndex;i--,j++) {
            if(actTreeModel.getChildCount(parentNode)==1 &&
            ((VWTreeNode)actTreeModel.getChild(parentNode,0)).getId()==-1) {
                VWTreeNode[] curTreeModel=new VWTreeNode[j];
                for(int index=0;index<j;index++)
                    curTreeModel[index]=treeModel[index];
                VWTreeConnector.Refresh(new TreePath(curTreeModel));
            }
            treeModel[j]= VWTreeConnector.getChildNodeWithId(room.getId(),nodeIds[i],
            parentNode,actTreeModel);
            if(treeModel[j]==null) return false;
            parentNode=treeModel[j];
        }
        TreePath path=new TreePath(treeModel);  
        TreePath selPath = null;        
        int row = levels;
        tree.setSelectionRow(1);
        try{
	        while (true) {	        	
	            selPath = tree.getPathForRow(row);
	            if(selPath== null) break; 
	            int len=selPath.toString().length();
	            if(path.toString().indexOf(selPath.toString().substring(0,len-1))==0) {
	            	VWTreeNode vwTreeNode = tree.getSelectedVWTreeNode(selPath);//(VWTreeNode)selPath.getLastPathComponent();
	                tree.expandRow(row);
	                tree.setSelectionRow(row);
	                tree.scrollRowToVisible(row);
	                if(parentId == vwTreeNode.getId()/* && selPath.toString().trim().equalsIgnoreCase(path.toString().trim())*/ )
	                	break;
	            }
	            row++;
	        }
	        gActionTreePath=selPath;            
        }catch(Exception ex){
        	System.out.println("Error while locating the path " + ex.getMessage());
        }
        /***End of CV10.2 - workflow enhancement changes*****/
        return true;
    }
//------------------------------------------------------------------------------
    private static VWTreeNode getChildNodeWithId(int roomId,int ChildNodeId,
        VWTreeNode parentNode,TreeModel treeModel)
    {
        int childCount=treeModel.getChildCount(parentNode);
        for(int i=0;i<childCount;i++)
        {
            VWTreeNode childNode=(VWTreeNode)treeModel.getChild(parentNode,i);
            if(childNode.getId()==ChildNodeId) return childNode;
        }
        return null;
    }
//------------------------------------------------------------------------------
    public static VWTreeNode getSelectedNode()
    {
        //return (VWTreeNode)tree.getSelectionPath().getLastPathComponent();
    	/**
    	 * Code added for handling the null pointer exception coming on the File Menu hanged issue.
    	 * Author : Vijaypriya.B.K
    	 * Modified on: Dec 05 2008
    	 */
    	if (VWTreeConnector.gActionTreePath == null ) return new VWTreeNode(0, "", 0, 0, 0);
    	return getSelectedNode(VWTreeConnector.gActionTreePath.getLastPathComponent());      
    	
    }
    	//------------------------------------------------------------------------------
public static void getRefParent(int parentId,VWTreeNode curNode,boolean found){                    
    
        if(found) return;
        for(int j=0;j<curNode.getChildCount();j++)
        {
            VWTreeNode childNode = (VWTreeNode)curNode.getChildAt(j);
            if(parentId == childNode.getId())
            {
                gNode=childNode;
                found=true;
            }
            getRefParent(parentId,childNode,found);
        }
    }
//------------------------------------------------------------------------------
    public static void setTree(VWTree curTree)
    {
        tree=curTree;
    }
//------------------------------------------------------------------------------
        public static boolean Refresh(TreePath selPath)
    {
        if(selPath==null) return false;
        VWTreeNode node =(VWTreeNode)selPath.getLastPathComponent();
        int nodeType=node.getType();
        if(nodeType!=AllRoom_TYPE && nodeType!=Room_TYPE){
            nodeRefresh(selPath);
        }
        treeShortMode=false;
        return true;
    }
//------------------------------------------------------------------------------
    private static void nodeRefresh(TreePath selPath)
    {
        removeChildren(selPath);
        loadNodeChildren(selPath);
        tree.expandPath(selPath);
    }
    
    private static void removeChildren(TreePath selPath)
    {
        DefaultMutableTreeNode node =(DefaultMutableTreeNode)selPath.getLastPathComponent();
        DefaultTreeModel treeModel=(DefaultTreeModel)tree.getModel();
        while(node.getChildCount()>0)
            treeModel.removeNodeFromParent((DefaultMutableTreeNode)node.getChildAt(0));
    }
    
    public static int loadNodeChildren(TreePath nodePath)
    {
        if (nodePath==null) return 0;
        VWTreeNode node=(VWTreeNode)nodePath.getLastPathComponent();
        node.removeAllChildren();
        tree.collapsePath(nodePath);
        int serverId=node.getRoomId();
        if(serverId==0) return 0;
        return loadNodeData(getNodeData(serverId,node),node);
    }
    
    public static int loadNodeData(List nodeList,VWTreeNode pNode){
        
        int nodeCount=0;
        if (pNode==null) return nodeCount;
        List child = new LinkedList();
        int roomId=pNode.getRoomId();
        DefaultTreeModel treeModel = (DefaultTreeModel)tree.getModel();
        if(nodeList==null || nodeList.size()==0) return nodeCount;
        nodeCount=nodeList.size();
        VWTreeNode parentFolder=pNode;
        VWTreeNode setParentFolder=pNode;
        Hashtable roomNodes=new Hashtable();
        for(int i=0;i<nodeCount;i++)
        {
            VWTreeNode newNode=(VWTreeNode)nodeList.get(i);
            if(newNode.getParentId()==pNode.getId())
            {
                parentFolder=pNode;
            }
            else
            {
                VWTreeNode tmpParentNode=getParentNode(roomId,newNode.getParentId(),roomNodes);
                if(tmpParentNode==null)
                {
                    if(nodeExist(nodeList,newNode.getParentId(),i+1))
                    {
                        nodeList.add(nodeList.get(i));
                        nodeCount++;
                    }
                    continue;
                }
                else
                {   
                    int parentId=tmpParentNode.getId();
                    boolean found=false;
                    for(int j=0;j<parentFolder.getChildCount();j++)
                        if(parentId==((VWTreeNode)parentFolder.getChildAt(j)).getId())
                        {
                            parentFolder=(VWTreeNode)parentFolder.getChildAt(j);
                            found=true;
                            break;
                        }
                        if(!found) 
                        {
                            gNode=null;
                            getRefParent(parentId,pNode,false);
                            parentFolder=gNode;
                        }
                }
            }
            if (newNode.getType()==0) newNode.setType(fixNodeType(parentFolder));
            treeModel.insertNodeInto((DefaultMutableTreeNode)newNode,
            (DefaultMutableTreeNode)parentFolder,
            ((DefaultMutableTreeNode)parentFolder).getChildCount());
            if(!roomNodes.containsKey(new String(roomId + "_" +newNode.getId())))
                roomNodes.put(new String(roomId + "_" + newNode.getId()),newNode);
            newNode=null;
        }
        return nodeCount;
    }
    public static List getNodeData(int serverId,VWTreeNode pNode)
    {
        int nodeId=0;
        if(pNode!=null) nodeId=pNode.getId();
        Vector nodes=new Vector();
        Node serverNode=new Node(nodeId);
        //AdminWise.gConnector.ViewWiseClient.getNodeContents(serverId, serverNode, 
        //        nodeId, nodes);        

        AdminWise.gConnector.ViewWiseClient.getNodeContentsWithAcl(serverId, serverNode, 
        		1, nodes, 1);
        if (nodes==null || nodes.size()==0) return null;
        
        VWTreeNode parentNode=pNode;
        List list=new LinkedList();
        int dataCount=nodes.size();
        for(int i=0;i<dataCount;i++)
        {
            VWTreeNode node = new VWTreeNode("");
            node.setTreeNodeInfo((Node)nodes.get(i));
            node.setRoomId(serverId);
            list.add(node);
            VWTreeNode loadingNode=new VWTreeNode(-1,"Loading...",Folder_TYPE,node.getId(),serverId);
            loadingNode.fixType(node.getType());
            list.add(loadingNode);
        }
        return list;
    }
    public static void openSecuriy(int serverId, int nodeId){
    	//AdminWise.printToConsole("Server Id : " + serverId + " : Node Id " + nodeId);
    	AdminWise.gConnector.ViewWiseClient.setSecurity(serverId, new Node(nodeId));
    }

    public static VWTreeNode getSelectedNode(Object nodeObj)
    {
        VWTreeNode node=null;
        try
        {
            if(nodeObj instanceof VWTreeNode)
                node=(VWTreeNode)nodeObj;
            else if(nodeObj instanceof DefaultMutableTreeNode)
            {
                if(((DefaultMutableTreeNode)nodeObj).getUserObject() instanceof String)
                {
                    VWTreeNode parent=null;//(VWTreeNode)((DefaultMutableTreeNode)nodeObj).getParent();
                    Object parentObj=((DefaultMutableTreeNode)nodeObj).getParent();
                    if(parentObj instanceof VWTreeNode)
                        parent=(VWTreeNode)parentObj;
                    else if(parentObj instanceof DefaultMutableTreeNode)
                        parent=(VWTreeNode)((DefaultMutableTreeNode)parentObj).getUserObject();
                    
                    int newNodeType=getNextType(parent.getType());
                    String nodeName=(String)((DefaultMutableTreeNode)nodeObj).getUserObject();
                    node=new VWTreeNode(nodeName,newNodeType);
                    node.setParentId(parent.getId());
                    node.setRoomId(parent.getRoomId());
                }
                else if(((DefaultMutableTreeNode)nodeObj).getUserObject() instanceof VWTreeNode)
                {
                    node=(VWTreeNode)((DefaultMutableTreeNode)nodeObj).getUserObject();
                }
            }
        }
        catch(Exception e)
        {
            return new VWTreeNode(0, "", 0, 0, 0);
        }
        return node;
    }
    public static boolean NewNode(int Type) {   
    	try{
    		tree.expandPath(tree.getSelectionPath());
    	}catch(Exception ex){
    		
    	}
        VWTreeNode node=(VWTreeNode)VWTreeConnector.gActionTreePath.getLastPathComponent();
        
        int roomId=node.getRoomId();
        if (roomId==0) return false;
        VWTreeNode newNode=new VWTreeNode("New Folder",Type);
        newNode.setRoomId(roomId);
        newNode.setParentId(node.getId());
        //newNode.setRoomId(node.getRoomId());
        Node serverNode=new Node(newNode.getName(), Node.FOLDER, node.getId());
        int retVal=VWConnector.ViewWiseClient.createNode(roomId,serverNode);
        if (retVal<0)   return false;
        newNode.setId(serverNode.getId());        
        node.add(newNode);        
        //((DefaultTreeModel)tree.getModel()).nodeChanged((VWTreeNode)gActionTreePath.getLastPathComponent());
        TreePath currentSelection = new TreePath(newNode.getPath());
        ((DefaultTreeModel)tree.getModel()).reload();
        //tree.setEditable(true);
        //tree.startEditingAtPath(currentSelection);       
        tree.collapsePath(gActionTreePath);
        tree.expandPath(gActionTreePath);
        gActionTreePath = currentSelection;
        Rename();
        return true;    	    	
    	
    } 
    public static boolean DynamicNode(int Type) {   
    	try{
    		tree.expandPath(tree.getSelectionPath());
    	}catch(Exception ex){
    		
    	}
        VWTreeNode node=(VWTreeNode)VWTreeConnector.gActionTreePath.getLastPathComponent();
        
        int roomId=node.getRoomId();
        if (roomId==0) return false;
        VWTreeNode newNode=new VWTreeNode("<<Index>>",Type);
        newNode.setRoomId(roomId);
        newNode.setParentId(node.getId());
        //newNode.setRoomId(node.getRoomId());
        Node serverNode=new Node(newNode.getName(), Node.FOLDER, node.getId());
        //int retVal=VWConnector.ViewWiseClient.createNode(roomId,serverNode);
        //if (retVal<0)   return false;
        newNode.setId(serverNode.getId());
        //tree.getCellEditor().getTreeCellEditorComponent(tree, node, true, true, true, 5);
        
        node.add(newNode);        
        TreePath currentSelection = new TreePath(newNode.getPath());
        ((DefaultTreeModel)tree.getModel()).reload();
        tree.setEditable(true);
        //tree.setEditable(true);
        //tree.startEditingAtPath(currentSelection);       
        tree.collapsePath(gActionTreePath);
        tree.expandPath(gActionTreePath);
        gActionTreePath = currentSelection;
        Rename();
        return true;    	    	
    	
    }
    
    public static void Rename() {
        tree.setEditable(true);
        tree.startEditingAtPath(gActionTreePath);
    }
    public static boolean RenameNode(boolean cancel) {
	System.out.println("RenameNode " + cancel);
        CellEditor cellEditor=tree.getCellEditor();
        String newName = "";
        boolean isDynamic = false;
        VWIndexRec index = null;
        if (cellEditor.getCellEditorValue() instanceof VWIndexRec){
            System.out.println("RenameNode isDynamic " + cancel);
            isDynamic = true;
            index = (VWIndexRec)cellEditor.getCellEditorValue();
            newName=index.getName();
            System.out.println("RenameNode newName " + cancel);
        }
        else
            newName=(String)cellEditor.getCellEditorValue();
       
        VWTreeNode node =getSelectedNode();        
        int serverId=node.getRoomId();
        System.out.println("serverId " + serverId + " : " + node.getName());
        if (isDynamic){
            System.out.println("isDynamic  " + index.getName() + " : " + index.getId());
            Node serverNode=new Node(node.getId(),newName);
            serverNode.setParentId(node.getParentId());
            node.setId(node.getId());
            node.setName(newName);
            node.setIndexId(index.getId());
            ((DefaultMutableTreeNode)tree.getSelectionPath().getLastPathComponent()).setUserObject(node);            
            return true;
        }
        if(cancel && node.getId()==0)
        {
            DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
            MutableTreeNode canceledNode = (MutableTreeNode)gActionTreePath.getLastPathComponent();
            model.removeNodeFromParent(canceledNode);
            return false;
        }
        if (serverId==0) return false;
        tree.setEditable(false);
        Node serverNode=new Node(node.getId(),newName);
        int retVal=0;
        if(node.getId()==0)
        {
            serverNode.setParentId(node.getParentId());
            retVal=VWConnector.ViewWiseClient.createNode(serverId,serverNode);
            if (retVal==0) 
            {
                //VWMessage.showMessage(VWMessage.ERR_NODE_ADD,VWMessage.DIALOG_TYPE);
                return false;
            }
            node.setId(serverNode.getId());
            ((DefaultMutableTreeNode)tree.getSelectionPath().getLastPathComponent()).setUserObject(node);
            
        }
        else
        {
            if (newName.equals("") || newName.trim().length() == 0) return false;
            retVal=VWConnector.ViewWiseClient.renameNode(serverId,serverNode);
            if (retVal<0) 
            {
                //VWMessage.showMessage(VWMessage.ERR_NODE_RENAME,VWMessage.DIALOG_TYPE);
                return false;
            }
            node.setName(newName);
        }
        System.out.println("Returning ");
        return true;
    }
    ///private static VWTree tree=ViewWise.AdminWise.VWFind.VWFindDlg.Navigator;
    private static VWTree tree=null;
    public static TreePath gActionTreePath=null;
    private static boolean treeShortMode=false;
    private static TreePath openedFolder=null;
    public static VWTreeNode gNode=null;
}