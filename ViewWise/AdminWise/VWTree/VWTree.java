/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWTree<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:fa  dish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWTree;

import java.awt.Component;
import java.util.EventObject;
import java.util.List;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTree;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWMenu.VWMenu;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

import javax.swing.event.TreeWillExpandListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.tree.ExpandVetoException;
import com.computhink.common.Node;
//--------------------------------------------------------------------------
public class VWTree extends JTree implements VWConstant{
    VWTreeRenderer renderer ;
    public boolean singleNodeSelection = false;
    public VWCellEditorListener cellListener = new VWCellEditorListener();
    public VWTree(){
        super();
        setBorder(null);
        renderer = new VWTreeRenderer();        
        setEditable(true);
        setCellRenderer(renderer);
        //setCellEditor(new VWTreeCellEditor(this, renderer));
        setTreeOptions();
        setEditable(false);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);        
        ((TreeCellEditor)getCellEditor()).addCellEditorListener(cellListener);
        
        
        addTreeWillExpandListener(new TreeWillExpandListener() {
            public void treeWillExpand(TreeExpansionEvent evt)
            throws ExpandVetoException {
                VWTreeNode expansionNode=(VWTreeNode)evt.getPath().getLastPathComponent();
                if(expansionNode.getChildCount()==1 && ((VWTreeNode)expansionNode.getChildAt(0)).getId()<0)
                    VWTreeConnector.Refresh(evt.getPath());
            }
            public void treeWillCollapse(TreeExpansionEvent evt)
            throws ExpandVetoException {
            }
        }
        );
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);  
        /**
         * CV10.2 - Added for workflow report - cabinet selection issue fix. Added by Vanitha.S
        */
        this.getCellEditor().addCellEditorListener(new CellEditorListener(){
            public void editingCanceled(ChangeEvent e){
            }
            public void editingStopped(ChangeEvent e){
                if(getSelectedNode()!=(VWTreeNode)getModel().getRoot()) {
                    String newValue=getCellEditor().getCellEditorValue().toString().trim();
                    if(newValue.equals("")) newValue=getSelectedNode().getName();
                    getSelectedNode().setName(newValue);
                }
            }
        });
        /*************End of CV10.2 workflow changes******************/
    }
    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof VWTree){
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWTree_RightMouseClicked(event);
            }
        }
    }
    //--------------------------------------------------------------------------
    void VWTree_RightMouseClicked(java.awt.event.MouseEvent event){
        
        int selRow = getRowForLocation(event.getX(),event.getY());
        TreePath seltedPath = this.getSelectionPath();
        if(selRow != -1) {
            TreePath selPath = getPathForLocation(event.getX(),event.getY());
            if(seltedPath!=selPath) {
                seltedPath=selPath;
                this.setSelectionPath(selPath);
            }
        }
        if(seltedPath==null) return;
        if(event.getClickCount() == 1) {
        	VWTreeConnector.gActionTreePath=seltedPath;
            rSingleClick(event,(VWTreeNode)seltedPath.getLastPathComponent());
        }
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,VWTreeNode node){
    
    /*VWMenu menu=null;
    VWTreeNode rootNode=(VWTreeNode)getPathForRow(0).getLastPathComponent();
    if(node==rootNode)
    {
        menu=new VWMenu(Room_TYPE);
        menu.show(this,event.getX()-20,event.getY());
    }
    if (node.getType() >= Cabinet_TYPE){
        menu=new VWMenu(node.getType());
        menu.show(this,event.getX()-20,event.getY());    	
    }
	
	if (node.getType() >= Folder_TYPE){
	    VWMenu menu=null;
	    menu=new VWMenu(node.getType());
	    menu.show(this,event.getX()-20,event.getY());    
	}*/
    }
    //--------------------------------------------------------------------------
    private void setTreeOptions(){
        
        /**
         * CV10.1 Enhancement :-Multiple delete option in administration template.
         * Modified by :- Apurba.M
         */
        AdminWise.printToConsole("check singleNodeSelection in setTreeOptions :"+singleNodeSelection);
        /**
         * CV10.2 - condition added for workflow report - cabinet selection issue fix. Added by Vanitha.S
        */
        if (singleNodeSelection) {
        	getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        } else {
        	getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        }
        setRootVisible(true);
        putClientProperty("JTree.lineStyle","Angled");
    }
    //--------------------------------------------------------------------------
    public void loadTreeData(){
        treeModel=VWTreeConnector.loadVWRooms();
    }
    //--------------------------------------------------------------------------
    public void loadTreeData(List nodeList, Vector securityList, Vector nodeProperties){
        treeModel=VWTreeConnector.loadVWRooms(nodeList, securityList, nodeProperties);
    }
    //--------------------------------------------------------------------------
    private VWTreeNode getSelectedNode(){
        TreePath   selPath = getSelectionPath();
        if(selPath != null) return (VWTreeNode)selPath.getLastPathComponent();
        return null;
    }
    //--------------------------------------------------------------------------
    public int getSelectedNodeType() {
        TreePath   selPath = getSelectionPath();
        if(selPath != null) return ((VWTreeNode)selPath.getLastPathComponent()).getType();
        return -1;
    }
    //--------------------------------------------------------------------------
    public int getSelectedNodeId(){
        VWTreeNode selNode=getSelectedNode();
        if(selNode!=null)
            return selNode.getId();
        return 0;
    }
    //--------------------------------------------------------------------------
    public String getSelectedNodeStrPath(){
        String selPath=getSelectionPath().toString();        
        if(selPath==null || selPath.equals("")) return "";
        selPath=selPath.substring(1,selPath.length()-1);
        selPath=selPath.replace(',','\\');
        return selPath;
    }
    public int getSelectedIndexId(){
	VWTreeNode node = (VWTreeNode) getSelectionPath().getLastPathComponent();	
	return node.getIndexId();
    }
    public String getSelectedIndexName(){
	VWTreeNode node = (VWTreeNode) getSelectionPath().getLastPathComponent();	
	return node.getName();
    }    
    //--------------------------------------------------------------------------
    public VWTreeNode GetNodeFromPosition(int posX,int posY){
        TreePath selPath = getPathForLocation(posX,posY);
        return (VWTreeNode)selPath.getLastPathComponent();
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyPressed(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            if (object instanceof VWTree)
                Tree_keyPressed(event);
        }
    }
    //--------------------------------------------------------------------------
    void Tree_keyPressed(java.awt.event.KeyEvent event) {
        if(event.getKeyChar()==(char)'*') {
            expandAll();
        }
        else if(event.getKeyChar()==(char)'-') {
            colapseAll();
        }
        else if(event.getKeyCode()==event.VK_ENTER) {
        }
    }
    //--------------------------------------------------------------------------
    public void setSelectionPath(int nodeId,boolean source) {
        try{
            if(!VWTreeConnector.setTreeNodeSelection(nodeId,source))
               setSelectionRow(0);
        }
        catch(Exception e){setSelectionRow(0);};
    }
    //--------------------------------------------------------------------------
    public void addNode(int nodeId) {
        VWTreeNode parentNode=getSelectedNode();
        if(parentNode == null) return;
        VWTreeNode newNode=new VWTreeNode(nodeId,"New Node");
        newNode.setParentId(parentNode.getId());
        newNode.setType(parentNode.getType()!=Folder_TYPE?parentNode.getType()+1:Folder_TYPE);
        treeModel.insertNodeInto(newNode,parentNode,0);
        expandPath(getSelectionPath());
    }
    //--------------------------------------------------------------------------
    public void removeNode() {
        removeNode(getSelectedNode());
    }
    //--------------------------------------------------------------------------
    public void removeNode(VWTreeNode node){
        
        if(node == null) return;
        if(node != (VWTreeNode)treeModel.getRoot()) {
            treeModel.removeNodeFromParent(node);
        }
    }
    //--------------------------------------------------------------------------
    public void renameNode(){
        getUI().startEditingAtPath(this,getSelectionPath());
    }
    //--------------------------------------------------------------------------
    private void renameNode(VWTreeNode node,String newName){
        node.setName(newName);
    }
    //--------------------------------------------------------------------------
    public VWTreeNode updateNodeParent(VWTreeNode parentNode,VWTreeNode newNode){
        
        if(parentNode == null||newNode == null) return null;
        int parentId=parentNode.getId();
        int newParentId=newNode.getParentId();
        if (parentId!=newParentId) newNode.setParentId(parentId);
        return newNode;
    }
    //--------------------------------------------------------------------------
    public void copyNode(VWTreeNode newParentNode,VWTreeNode node) {
    }
    //--------------------------------------------------------------------------
    public void cutNode(VWTreeNode newParentNode,VWTreeNode node){
        if(newParentNode == null || node == null) return;
        node = updateNodeParent(newParentNode,node);
        removeNode(node);
        newParentNode.add(node);
    }
    //--------------------------------------------------------------------------
    public void expandAll() {
        for(int i=0;i<getRowCount();i++) expandRow(i);
    }
    //--------------------------------------------------------------------------
    public void colapseAll() {
        for(int i=getRowCount()-1;i>0;i--) collapseRow(i);
    }
    //--------------------------------------------------------------------------
    public Vector getLastPaths() {
        Vector paths=new Vector();
        int count=getRowCount();
        String nodePath="";
        for(int i=1;i<count;i++) {
            TreePath treePath=getPathForRow(i);
            VWTreeNode treeNode=(VWTreeNode)treePath.getLastPathComponent();
            TreeModel treeModel=getModel();
            if(treeModel.getChildCount(treeNode)==0) {
                treePath.getPathCount();
                Object[] pathes=treePath.getPath();
                int pCount=pathes.length;
                nodePath="";
                for(int p=0;p<pCount;p++)
                    nodePath+=pathes[p].toString()+((char)9);
                ///nodePath=nodePath.substring(1,nodePath.length()-1);
                paths.add(nodePath);
            }
        }
        return paths;
    }
    public Vector getSecurityInfo() {
    	Vector data = new Vector();
    	int count=getRowCount();
    	String nodePath="";
    		for(int i=1;i<count;i++) {
    			TreePath treePath=getPathForRow(i);
    			VWTreeNode treeNode=(VWTreeNode)treePath.getLastPathComponent();
    			//AdminWise.printToConsole("data on security " + treeNode.getUserGroupSecurityList().size());
    			if(treeNode.getUserGroupSecurityList().size() > 0){
    				/////
    				Object[] pathes=treePath.getPath();
    				int pCount=pathes.length;
    				nodePath="";
    				for(int p=1;p<pCount;p++){
    					nodePath+="\\"+pathes[p].toString();
    				}
    				Node node = new Node(treeNode.getId());
    				node.setName(treeNode.getName());
    				node.setUserGroupSecurityList(treeNode.getUserGroupSecurityList());
    				//AdminWise.printToConsole("Path : " + nodePath);               
    				node.setPath(nodePath);
    				data.add(node);
    			}
    		}
    	return data;
    }
    // This method add to get the node properties from treenode.
    public Vector getNodePropertiesInfo() {
        Vector data = new Vector();
        int count=getRowCount();
        String nodePath="";
        try{
        for(int i=1;i<count;i++) {
        	TreePath treePath=getPathForRow(i);
        	VWTreeNode treeNode=(VWTreeNode)treePath.getLastPathComponent();
        	//AdminWise.printToConsole("data on Node Properties " + treeNode.getNodePropertiesList().size());
        	if(treeNode.getNodePropertiesList()!=null && treeNode.getNodePropertiesList().size() > 0){
        		Object[] pathes=treePath.getPath();
        		int pCount=pathes.length;
        		nodePath="";
        		for(int p=1;p<pCount;p++){
        			nodePath+="\\"+pathes[p].toString();
        		}
        		Node node = new Node(treeNode.getId());
        		node.setName(treeNode.getName());
        		node.setDefaultDocType(treeNode.getDefaultDocType());
        		node.setNodePropertiesList(treeNode.getNodePropertiesList());
        		//AdminWise.printToConsole("Path : " + nodePath);
        		//AdminWise.printToConsole("Default Doc Type : " + treeNode.getDefaultDocType());
        		//AdminWise.printToConsole("Node Priperties : " + treeNode.getNodePropertiesList());
        		node.setPath(nodePath);
        		data.add(node);
        	}else{
        		/*
        		* Check for default document type, if it exist then export it into vwt file eventhough node properties index values are not exist.
        		* This is the fix for notification 1197 - Node property for the folder is not copied when they try coping folder structure using the templates tab
        		*/
        		if(treeNode.getDefaultDocType()!=null && treeNode.getDefaultDocType().trim().length()>0){
        			Object[] pathes=treePath.getPath();
            		int pCount=pathes.length;
            		nodePath="";
            		for(int p=1;p<pCount;p++){
            			nodePath+="\\"+pathes[p].toString();
            		}
            		Node node = new Node(treeNode.getId());
            		node.setName(treeNode.getName());
            		node.setDefaultDocType(treeNode.getDefaultDocType());
            		node.setPath(nodePath);
            		data.add(node);
        		}
        	}
        }
        }catch(Exception ex){}
        return data;
    }    
    //--------------------------------------------------------------------------
    public Vector getAllNodes() {
        expandAll();
        Vector nodesList=new Vector();
        int count=getRowCount();
        for(int i=1;i<count;i++)
            nodesList.add(getPathForRow(i).getLastPathComponent());
        return nodesList;
    }
    
    /**
     * CV10.2 - Added for Workflow report - Cabinet selection issue fix. Added by Vanitha.S
     * @param seltedPath
     * @return
     */
    public VWTreeNode getSelectedVWTreeNode(TreePath seltedPath)
    {
        if(seltedPath==null)
            seltedPath = getSelectionPath();
        Object obj=seltedPath.getLastPathComponent();
        VWTreeNode treeNode=null;
        if(obj instanceof VWTreeNode)
        {
            treeNode=(VWTreeNode)seltedPath.getLastPathComponent();
        }
        else if(obj instanceof DefaultMutableTreeNode)
        {    
            if(((DefaultMutableTreeNode)obj).getUserObject() instanceof String)
            {
                ////VWTreeNode parent=(VWTreeNode)((DefaultMutableTreeNode)obj).getParent();
                VWTreeNode parent=null;
                Object parentObj=((DefaultMutableTreeNode)obj).getParent();
                
                if(parentObj instanceof VWTreeNode)
                    parent=(VWTreeNode)parentObj;
                else if(parentObj instanceof DefaultMutableTreeNode)
                    parent=(VWTreeNode)((DefaultMutableTreeNode)parentObj).getUserObject();
                
                int newNodeType=VWTreeConnector.getNextType(parent.getType());
                String nodeName=(String)((DefaultMutableTreeNode)obj).getUserObject();
                treeNode=new VWTreeNode(nodeName,newNodeType);
                treeNode.setParentId(parent.getId());
                treeNode.setRoomId(parent.getRoomId());
            }
            else if(((DefaultMutableTreeNode)obj).getUserObject() instanceof VWTreeNode)
            {
                treeNode=(VWTreeNode)((DefaultMutableTreeNode)obj).getUserObject();
            }
        }
        return treeNode;
    }
    class VWCellEditorListener implements CellEditorListener {
        public void editingCanceled(ChangeEvent e){
            setEditable(false);
            System.out.println("editingCanceled");
            VWTreeConnector.RenameNode(true);
        }
        public void editingStopped(ChangeEvent e){
            setEditable(false);
            System.out.println("editingStopped");
            VWTreeConnector.RenameNode(false);
        }
    }
    
    //--------------------------------------------------------------------------
    private static int CurRoomId=0;
    private DefaultTreeModel treeModel=null;
    
    class VWTreeCellEditor extends DefaultTreeCellEditor {
	JComboBox cmbList = null;
	DefaultCellEditor editor = null;
	VWTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer) {
	    super(tree, renderer);
	}
	VWTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer,
		      TreeCellEditor editor) {
	    super(tree, renderer, editor);	    
	}	
/*	public Component getTreeCellEditorComponent(JTree tree, Object value,
		boolean isSelected,
		boolean expanded,
		boolean leaf, int row) {
	    System.out.println("getTreeCellEditorComponent ");
	    VWTreeNode node = (VWTreeNode) value;
	    System.out.println("getTreeCellEditorComponent "+ node.getName());
	    if (((VWTreeNode)value).getType() == 5){
		if (cmbList == null)
		{
		    cmbList = new JComboBox(loadIndicesList());
		    editor = new DefaultCellEditor(cmbList); 
		    editor.addCellEditorListener(cellListener);
		    tree.setCellEditor(editor);    
		}
		((VWTreeNode)value).setName(cmbList.getSelectedItem().toString());
		System.out.println("getTreeCellEditorComponent combo value " + value);
	    }	      	  
	    return super.getTreeCellEditorComponent(tree, value, isSelected, expanded, leaf, row);
	}*/
/*	public boolean isCellEditable(EventObject event) {
	    Object node = tree.getLastSelectedPathComponent();
	      if ((node != null) && (node instanceof TreeNode)) {
	        TreeNode treeNode = (TreeNode) node;
	        return treeNode.isLeaf();
	      }
	      return false;
	}*/
	public Vector loadIndicesList(){
	    	Vector data = new Vector();
		VWIndexRec[] indices = VWDocTypeConnector.getIndices(false);
		for(int count=0; count<indices.length;count++){
			VWIndexRec vwIndexRec = (VWIndexRec)indices[count];
			if(vwIndexRec.getType() == 5)
				continue;
			data.add(indices[count]);
			
		}
		return data;
	}
	
    }
    /*CV10.2 - This method has added for single tree node selection.Added by Vanitha.S*******/
    public void setSingleNodeSelection(boolean flag) {
    	this.singleNodeSelection = flag;
    	setTreeOptions();
    }
}

