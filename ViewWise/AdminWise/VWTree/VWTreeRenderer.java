
/**
    Custom Tree renderer and Editor

 */
package ViewWise.AdminWise.VWTree;

import javax.swing.JTree;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Component;

import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.DefaultTreeCellRenderer;

import ViewWise.AdminWise.VWTree.VWTree.VWCellEditorListener;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.net.MalformedURLException;

import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import ViewWise.AdminWise.VWConstant;

class VWTreeRenderer extends DefaultTreeCellRenderer
{
    JComboBox cmbList = null;
    protected static ImageIcon[] gComponentImages = {
	VWImages.AllRoomIcon,VWImages.AllRoomIcon,VWImages.AllRoomIcon,
	VWImages.RoomConnectIcon,VWImages.RoomDownIcon,VWImages.RoomUnConnectIcon,
	VWImages.CabinetCollapsedIcon,VWImages.CabinetExpandedIcon,VWImages.CabinetCollapsedIcon,
	VWImages.DrawerCollapsedIcon,VWImages.DrawerExpandedIcon,VWImages.DrawerCollapsedIcon,
	VWImages.FolderCollapsedIcon,VWImages.FolderExpandedIcon,VWImages.FolderCollapsedIcon,
	VWImages.CheckOutIcon,VWImages.CheckOutIcon,VWImages.CheckOutIcon,
	VWImages.CheckInIcon,VWImages.CheckInIcon,VWImages.CheckInIcon,
	VWImages.SessionIcon,VWImages.SessionIcon,VWImages.SessionIcon,
	VWImages.SessionIcon,VWImages.SessionIcon,VWImages.SessionIcon,
	VWImages.FolderCollapsedIcon,VWImages.FolderExpandedIcon,VWImages.FolderCollapsedIcon};
    public Icon[] getImage(int itemType) {
	Icon[] retIcons=new Icon[3];
	retIcons[0]=gComponentImages[itemType];
	retIcons[1]=gComponentImages[itemType+1];
	retIcons[2]=gComponentImages[itemType+2];

	return retIcons;
    }
    public java.awt.Component getTreeCellRendererComponent(JTree tree,
	    java.lang.Object value,
	    boolean selected,
	    boolean expanded,
	    boolean leaf,
	    int row,
	    boolean hasFocus)
    {
	boolean isComponentNode =(value instanceof VWTreeNode);                                     
	if(isComponentNode){
	    int type = ((VWTreeNode)value).getType();
	    boolean isOpen = ((VWTreeNode)value).getIsOpen();
	    int connectStatus = ((VWTreeNode)value).getConnectStatus();

	    Icon[] show = getImage(type*3);
	    if( show != null) {

		setClosedIcon(show[0]);
		setOpenIcon(show[1]);
		setLeafIcon(show[2]);
	    }
	    if(isOpen && type==VWConstant.Folder_TYPE) {
		setClosedIcon(show[0]);
		setOpenIcon(show[1]);
		setLeafIcon(show[1]);
	    }
	    if(type==VWConstant.Room_TYPE) {
		if(connectStatus==VWConstant.Room_Status_Down)
		{
		    setClosedIcon(show[0]);
		    setOpenIcon(show[1]);
		    setLeafIcon(show[1]);
		}
		else
		{
		    setClosedIcon(show[0]);
		    setOpenIcon(show[0]);
		    setLeafIcon(show[2]);
		}
	    }
	}
	/*System.out.println("Type " + ((VWTreeNode)value).getType() + " : " + ((VWTreeNode)value).getName());
	TreeCellEditor editor = null;
	if (((VWTreeNode)value).getType() == 5){

	    if (cmbList == null)
	    {
		cmbList = new JComboBox(loadIndicesList());
	    }
	    editor = new DefaultCellEditor(cmbList);
	    editor.addCellEditorListener(((VWTree)tree).cellListener);		

	    ((VWTreeNode)value).setName(cmbList.getSelectedItem().toString());
	    System.out.println("Return combo value " + value);
	    tree.setCellEditor(editor);
	}*/
	Component ret = super.getTreeCellRendererComponent(
		tree,value,selected,expanded,leaf,row,hasFocus);
	/*
         // 
         // can also be done by subclassing tree and its concert value to text
         if(isComponentNode) {
             String text = ((DefaultMutableTreeNode)value).getDisplayText();
             setText(text); 
         }
	 */
	return(ret);
    }
    public Vector loadIndicesList(){
	Vector data = new Vector();
	VWIndexRec[] indices = VWDocTypeConnector.getIndices(false);
	for(int count=0; count<indices.length;count++){
	    VWIndexRec vwIndexRec = (VWIndexRec)indices[count];
	    if(vwIndexRec.getType() == 5)
		continue;
	    data.add(indices[count]);

	}
	return data;
    }
}
