/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWTreeItem<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWTree;

import java.util.Vector;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWUtil;
import com.computhink.common.Node;

public class VWTreeNode extends DefaultMutableTreeNode implements VWConstant {
    private int id;
    private String name;
    private String description;
    private int type;
    private int parentId;
    private int roomId;
    private boolean isOpen=false;
    private int connectStatus=0;//0 UnConnect;1 Connect;2 Down
    private Vector userGroupSecurityList = new Vector();
    private String defaultDocType = new String();
    private Vector nodePropertiesList = new Vector();
    private String VWPath ="";
    private boolean isNodeUpdate = false;// When update from template, checking the node is updated the parent id or not
    private int indexId;
    //--------------------------------------------------------------------------
    public VWTreeNode(String nodeName) {
        super(nodeName);
        id=0;
        name=nodeName;
        description="";
        type=0;
        parentId=0;
    }
    //--------------------------------------------------------------------------
    public VWTreeNode(int id, String name) {
        this(name);
        this.id=id;
    }
    //--------------------------------------------------------------------------
    public VWTreeNode(String name, int type) {
        
        this(name);
        this.type=type;
    }
    //--------------------------------------------------------------------------
    public VWTreeNode(String name, int type, int parentId) {
        this(name);
        this.type=type;
        this.parentId=parentId;
    }
    //--------------------------------------------------------------------------
    public VWTreeNode(int id, String name, int type) {
        this(name);
        this.id=id;
        this.type=type;
    }
    //--------------------------------------------------------------------------
    public VWTreeNode(int id, String name, int type, int parentId) {
        this(name);
        this.id = id;
        this.type = type;
        this.parentId = parentId;
    }
    //--------------------------------------------------------------------------
    public VWTreeNode(int id, String name, int type, int parentId,int roomId) {
        this(name);
        this.id = id;
        this.type = type;
        this.parentId = parentId;
        this.roomId = roomId;
    }
    //--------------------------------------------------------------------------
    public void setTreeNodeInfo(String nodeInfo) {
        VWStringTokenizer tokenizer=new VWStringTokenizer(nodeInfo,SepChar,false);
        this.id = VWUtil.to_Number(tokenizer.nextToken());
        setName(tokenizer.nextToken());
        this.type = VWUtil.to_Number(tokenizer.nextToken());
        this.parentId = VWUtil.to_Number(tokenizer.nextToken());
    }
    //--------------------------------------------------------------------------
    public void setTreeNodeInfo(Node node) {
        this.id = node.getId();
        setName(node.getName());
        this.type = 0;
        this.parentId = node.getParentId();
        this.userGroupSecurityList = node.getUserGroupSecurityList(); 
        this.defaultDocType = node.getDefaultDocType();
        this.nodePropertiesList = node.getNodePropertiesList();
        
        //this.sessionId=node.getSessionId();
    }
    //--------------------------------------------------------------------------
    public int getId() {
        return id;
    }
    //--------------------------------------------------------------------------
    public void setId(int id) {
        this.id = id;
    }
    //--------------------------------------------------------------------------
    public int getRoomId() {
        return roomId;
    }
    //--------------------------------------------------------------------------
    public void setRoomId(int id) {
        this.roomId = id;
    }
    //--------------------------------------------------------------------------
    public String getName() {
        return name.trim();
    }
    //--------------------------------------------------------------------------
    public byte[] getNameBytes() {
        return name.getBytes();
    }
    //--------------------------------------------------------------------------
    public boolean setName(String name) {
        this.setUserObject(name);
        this.name = name.trim();
        return true;
    }
    //--------------------------------------------------------------------------
    public String getDescription() {
        return description;
    }
    //--------------------------------------------------------------------------
    public boolean setDescription(String description) {
        this.description = description;
        return true;
    }
    //--------------------------------------------------------------------------
    public int getType() {
        return type;
    }
    //--------------------------------------------------------------------------
    public boolean setType(int type) {
        this.type = type;
        return true;
    }
    //--------------------------------------------------------------------------
    public int getParentId() {
        return parentId;
    }
    //--------------------------------------------------------------------------
    public boolean setParentId(int parentId) {
        this.parentId = parentId;
        return true;
    }
    //--------------------------------------------------------------------------
    public void fixType(int parentType) {
        this.type=parentType==0?Cabinet_TYPE:this.type;
        this.type=parentType==1?Drawer_TYPE:this.type;
        this.type=parentType==2?Folder_TYPE:this.type;
        this.type=parentType==3?Folder_TYPE:this.type;
    }
    //--------------------------------------------------------------------------
    public String toString() {
        return getName();
    }
    //--------------------------------------------------------------------------
    public boolean getIsOpen() {
        return isOpen;
    }
    //--------------------------------------------------------------------------
    public void setIsOpen(boolean nodeIsOpen) {
        this.isOpen = nodeIsOpen;
    }
    //--------------------------------------------------------------------------
    public int getConnectStatus() {
        return connectStatus;
    }
    //--------------------------------------------------------------------------
    public void setConnectStatus(int roomStatus) {
        this.connectStatus = roomStatus;
    }
    //--------------------------------------------------------------------------
	/**
	 * @return Returns the userGroupSecurityList.
	 */
	public Vector getUserGroupSecurityList() {
		return userGroupSecurityList;
	}
	/**
	 * @param userGroupSecurityList The userGroupSecurityList to set.
	 */
	public void setUserGroupSecurityList(Vector userGroupSecurityList) {
		this.userGroupSecurityList = userGroupSecurityList;
	}
	/**
	 * @return Returns the path.
	 */
	public String getVWPath() {
		return VWPath;
	}
	/**
	 * @param path The path to set.
	 */
	public void setVWPath(String path) {
		this.VWPath = path;
	}
	/**
	 * @return Returns the defaultDocType.
	 */
	public String getDefaultDocType() {
		return defaultDocType;
	}
	/**
	 * @param defaultDocType The defaultDocType to set.
	 */
	public void setDefaultDocType(String defaultDocType) {
		this.defaultDocType = defaultDocType;
	}
	/**
	 * @return Returns the nodePropertiesList.
	 */
	public Vector getNodePropertiesList() {
		return nodePropertiesList;
	}
	/**
	 * @param nodePropertiesList The nodePropertiesList to set.
	 */
	public void setNodePropertiesList(Vector nodePropertiesList) {
		this.nodePropertiesList = nodePropertiesList;
	}
	/**
	 * @return Returns the isNodeUpdate.
	 */
	public boolean isNodeUpdate() {
		return isNodeUpdate;
	}
	/**
	 * @param isNodeUpdate The isNodeUpdate to set.
	 */
	public void setNodeUpdate(boolean isNodeUpdate) {
		this.isNodeUpdate = isNodeUpdate;
	}
	public int getIndexId() {
	    return indexId;
	}
	public void setIndexId(int indexId) {
	    this.indexId = indexId;
	}
}