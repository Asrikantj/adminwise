package ViewWise.AdminWise.VWArchive;

import java.awt.Dimension;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import java.util.List;
import java.util.Vector;
import ViewWise.AdminWise.VWStorage.VWDlgStorageLocation;
import ViewWise.AdminWise.VWFind.VWFindDlg;
import ViewWise.AdminWise.VWBackup.VWBackupConnector;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.JFileChooser;
import java.io.File;
import javax.swing.JOptionPane;
import com.computhink.common.ServerSchema;
import com.computhink.common.Document;
import com.computhink.common.ViewWiseErrors;

public class VWArchivePanel extends JPanel implements VWConstant {
    public VWArchivePanel() {
    }
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWArchive.setLayout(null);
        add(VWArchive,BorderLayout.CENTER);
        VWArchive.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWArchive.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/
        
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnLocation.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_LOCATIONDEF_NAME"));
        BtnLocation.setActionCommand(BTN_ADDTOLIST_NAME);
        PanelCommand.add(BtnLocation);
        //BtnLocation.setBackground(java.awt.Color.white);
        BtnLocation.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnLocation.setIcon(VWImages.StorageIcon);
        top += hGap;
        BtnAddToList.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_ADDTOLIST_NAME"));
        BtnAddToList.setActionCommand(BTN_ADDTOLIST_NAME);
        PanelCommand.add(BtnAddToList);
        //BtnAddToList.setBackground(java.awt.Color.white);
        BtnAddToList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnAddToList.setIcon(VWImages.FindIcon);
        BtnClearList.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_CLEAR_NAME"));
        BtnClearList.setActionCommand(BTN_CLEAR_NAME);
        PanelCommand.add(BtnClearList);
        //BtnClearList.setBackground(java.awt.Color.white);
        top += hGap + 12;
        BtnClearList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearList.setIcon(VWImages.ClearIcon);
        BtnClearSelList.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_CLEARSELECTED_NAME"));
        BtnClearSelList.setActionCommand(BTN_CLEARSELECTED_NAME);
        PanelCommand.add(BtnClearSelList);
        top += hGap;
        //BtnClearSelList.setBackground(java.awt.Color.white);
        BtnClearSelList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearSelList.setIcon(VWImages.ClearIcon);
        BtnMove.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_MOVE_NAME"));
        BtnMove.setActionCommand(BTN_MOVE_NAME);
        BtnMove.setIcon(VWImages.StorageIcon);
        PanelCommand.add(BtnMove);
        top += hGap + 12;
        //BtnMove.setBackground(java.awt.Color.white);
        BtnMove.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnCopy.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_COPY_NAME"));
        BtnCopy.setActionCommand(BTN_COPY_NAME);
        PanelCommand.add(BtnCopy);
        top += hGap;
        //BtnCopy.setBackground(java.awt.Color.white);
        BtnCopy.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnCopy.setIcon(VWImages.CopyIcon);
        BtnSize.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_SIZE_NAME"));
        BtnSize.setActionCommand(BTN_SIZE_NAME);
        PanelCommand.add(BtnSize);
        top += hGap + 12;
        //BtnSize.setBackground(java.awt.Color.white);
        BtnSize.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSize.setIcon(VWImages.SizeIcon);
        TxtSize.setText("");
        PanelCommand.add(TxtSize);
        TxtSize.setBackground(java.awt.Color.white);
        top += hGap;
        TxtSize.setBounds(12,top,144, AdminWise.gSmallBtnHeight);
        TxtSize.setEditable(false);
        PanelCommand.add(progress);
        top += hGap;
        progress.setBounds(12,top,144, AdminWise.gBtnHeight);
        progress.setMinimum(0);
        progress.setVisible(false);
        progress.setStringPainted(true);
        PanelTable.setLayout(new BorderLayout());
        VWArchive.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWArchive.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.ArchiveImage);
        SymAction lSymAction = new SymAction();
        BtnAddToList.addActionListener(lSymAction);
        BtnClearList.addActionListener(lSymAction);
        BtnClearSelList.addActionListener(lSymAction);
        BtnMove.addActionListener(lSymAction);
        BtnCopy.addActionListener(lSymAction);
        BtnSize.addActionListener(lSymAction);
        BtnLocation.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnAddToList)
                /*BtnAddToList_actionPerformed(event);*/
            	BtnFind_actionPerformed(event);
            else if (object == BtnClearList)
                BtnClearList_actionPerformed(event);
            else if (object == BtnClearSelList)
                BtnClearSelList_actionPerformed(event);
            else if (object == BtnMove)
                BtnMove_actionPerformed(event);
            else if (object == BtnCopy)
                BtnCopy_actionPerformed(event);
            else if (object == BtnSize)
                BtnSize_actionPerformed(event);
            else if (object == BtnLocation)
                BtnLocation_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWArchive = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_ARCHIVE_NAME, VWImages.ArchiveImage);
    VWLinkedButton BtnAddToList = new VWLinkedButton(1);
    VWLinkedButton BtnClearList = new VWLinkedButton(1);
    VWLinkedButton BtnClearSelList = new VWLinkedButton(1);
    VWLinkedButton BtnMove = new VWLinkedButton(1);
    VWLinkedButton BtnCopy = new VWLinkedButton(1);
    VWLinkedButton BtnSize = new VWLinkedButton(1);
    VWLinkedButton BtnLocation = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    javax.swing.JTextField TxtSize = new javax.swing.JTextField();
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWArchiveTable Table = new VWArchiveTable();
    javax.swing.JProgressBar progress = new javax.swing.JProgressBar();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWArchive)
                VWArchive_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWArchive_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWArchive) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnFind_actionPerformed(java.awt.event.ActionEvent event) {
    	VWArchiveConnector.openFindDialog();
    }
    
    void BtnAddToList_actionPerformed(java.awt.event.ActionEvent event) {
        if(findDlg != null) {
        	// If already 'Find Panel' Opened, make it null and opened it again.             
        	//findDlg.setAppendEnabled(Table.getRowCount()>0);
            //findDlg.setVisible(true);
        	findDlg = null;
        }
        //else 
        {
            findDlg = new ViewWise.AdminWise.VWFind.VWFindDlg(AdminWise.adminFrame,
                CALLER_ARCHIVE);
            findDlg.setAppendEnabled(Table.getRowCount()>0);
            findDlg.setVisible(true);
        }
    }
    //--------------------------------------------------------------------------
    void BtnLocation_actionPerformed(java.awt.event.ActionEvent event) {
        if(storageDlg==null) {
            storageDlg = new VWDlgStorageLocation(AdminWise.adminFrame);
        }
        else {
        	storageDlg.setHostedMode();
        	storageDlg.setVisible(true);
            storageDlg.requestFocus();
        }
    }
    //--------------------------------------------------------------------------
    public void closeFindDlg() {
        //findDlg.setVisible(false);
        if(Table.getSelectedRow() >=0)
            setEnableMode(MODE_SELECT);
        else
            setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    void BtnClearList_actionPerformed(java.awt.event.ActionEvent event) {
        clearTable();
    }
    //--------------------------------------------------------------------------
    void BtnClearSelList_actionPerformed(java.awt.event.ActionEvent event) {
        Table.deleteSelectedRows();
    }
    //--------------------------------------------------------------------------
    void BtnMove_actionPerformed(java.awt.event.ActionEvent event) 
    {
    	 Vector docIds=Table.getseletedDocIds();
         String size = VWBackupConnector.getDocsSize(docIds, true);
         
        if(BtnMove.getText().equals(BTN_CANCELMOVE_NAME))
        {
            cancelMove=true;
            return;
        }
        new moveThread((java.awt.Component) this);
        this.paintImmediately(this.getVisibleRect());
    }
    //--------------------------------------------------------------------------
    void BtnCopy_actionPerformed(java.awt.event.ActionEvent event) {
        if(BtnCopy.getText().equals(BTN_CANCELCOPY_NAME))
        {
            cancelCopy=true;
            return;
        }
        new copyThread((java.awt.Component) this);
        this.paintImmediately(this.getVisibleRect());
    }
    //--------------------------------------------------------------------------
    private String getFolder() {
        JFileChooser fc = new JFileChooser(new File(""));
        fc.setFileSelectionMode(fc.DIRECTORIES_ONLY);
        fc.setCurrentDirectory(new File("c:\\"));
        fc.showDialog(null, LBL_SELECT_NAME);
        String selPath=fc.getSelectedFile().getPath();
        if(!new File(selPath).canWrite())
        {
            VWMessage.showMessage((java.awt.Component) this,
            		AdminWise.connectorManager.getString("vwarchpanel.msg1")+" "+ selPath +" "+AdminWise.connectorManager.getString("vwarchpanel.msg2"));
            return "";
        }
    	return selPath;
    }
    //--------------------------------------------------------------------------
    void BtnSize_actionPerformed(java.awt.event.ActionEvent event) {
        Vector docIds=Table.getseletedDocIds();
        TxtSize.setText(VWBackupConnector.getDocsSize(docIds, true));
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return Table.getRowCount();
    }
    //--------------------------------------------------------------------------
    public void addData(List data) {
        Table.addData(data);
    }
    //--------------------------------------------------------------------------
    public void deleteRowOutOfList(List list) {
        Table.deleteRowOutOfList(list);
    }
    //--------------------------------------------------------------------------
    public void insertData(List data) {
        Table.insertData(data);
    }
    //--------------------------------------------------------------------------
    public void clearTable() {
        Table.clearData();
        TxtSize.setText("");
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode) {
        BtnAddToList.setVisible(true);
        BtnClearList.setVisible(true);
        BtnClearSelList.setVisible(true);
        TxtSize.setVisible(true);
        BtnMove.setVisible(true);
        progress.setVisible(false);
        BtnCopy.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_COPY_NAME"));
        BtnMove.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_MOVE_NAME"));
        switch (mode) {
            case MODE_UNSELECTED:
                BtnLocation.setEnabled(true);
                BtnAddToList.setEnabled(true);
                BtnClearSelList.setEnabled(false);
                BtnMove.setEnabled(false);
                BtnCopy.setEnabled(false);
                if(getRowCount()>0)
                    BtnClearList.setEnabled(true);
                else
                    BtnClearList.setEnabled(false);
                BtnSize.setEnabled(false);
                break;
            case MODE_SELECT:
                BtnLocation.setEnabled(true);
                BtnAddToList.setEnabled(true);
                BtnClearSelList.setEnabled(true);
                BtnMove.setEnabled(true);
                BtnCopy.setEnabled(true);
                if(getRowCount()>0)
                    BtnClearList.setEnabled(true);
                else
                    BtnClearList.setEnabled(false);
                BtnSize.setEnabled(true);
                break;
            case MODE_UNCONNECT:
                BtnLocation.setEnabled(false);
                BtnAddToList.setEnabled(false);
                BtnClearList.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                BtnMove.setEnabled(false);
                BtnCopy.setEnabled(false);
                BtnSize.setEnabled(false);
                gCurRoom=0;
                break;
            case MODE_COPY:
                BtnLocation.setEnabled(false);
                BtnAddToList.setEnabled(false);
                BtnClearList.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                BtnMove.setEnabled(false);
                BtnCopy.setEnabled(true);
                BtnCopy.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_CANCELCOPY_NAME"));
                BtnSize.setEnabled(false);
                TxtSize.setVisible(false);
                progress.setVisible(true);
                progress.setMaximum(Table.getSelectedRowCount());
                break;
            case MODE_MOVE:
                BtnLocation.setEnabled(false);
                BtnAddToList.setEnabled(false);
                BtnClearList.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                BtnCopy.setEnabled(false);
                BtnMove.setEnabled(true);
                BtnMove.setText(AdminWise.connectorManager.getString("vwarchpanel.BTN_CANCELMOVE_NAME"));
                BtnSize.setEnabled(false);
                TxtSize.setVisible(false);
                progress.setVisible(true);
                progress.setMaximum(Table.getRowCount());
                break;
        }
    }
    //--------------------------------------------------------------------------
    public void setProgressValue(int value,String action) {
        progress.setValue(value);
        progress.setString(String.valueOf(value)+" / "+progress.getMaximum());
        progress.paintImmediately(progress.getVisibleRect());
    }
    
    public void setInitialProgressValue(int value,String action) {
        progress.setValue(0);
        progress.setString(String.valueOf(value)+" / "+String.valueOf(value));
        progress.paintImmediately(progress.getVisibleRect());
    }
    
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    public VWDlgStorageLocation storageDlg = null;
    public VWFindDlg findDlg=null;
    private boolean UILoaded=false;
    private boolean cancelCopy =false;
    private boolean cancelMove =false;
    //--------------------------------------------------------------------------
 private class copyThread extends Thread
    {
        java.awt.Component container=null;
        copyThread(java.awt.Component container)
        {
            this.container=container;
            setDaemon(true);
            ///setPriority(java.lang.Thread.MAX_PRIORITY);
            start();
        }
        public void run()
        {
            try
        {
            setInitialProgressValue(0,AdminWise.connectorManager.getString("vwarchpanel.LBL_COPY_NAME"));
            setEnableMode(MODE_COPY);
            VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
            Vector docIds=Table.getseletedDocIds();
            String toPath=getFolder();
            if(toPath==null || toPath.equals("")) return;
            
            //In following two VWMessage.showOptionDialog() call return type is checked. -1 and 1 is consdered as a Cancel. 
            //And 0 as Proceed.
            int copyRes = VWMessage.showOptionDialog(container,
            		AdminWise.connectorManager.getString("vwarchpanel.LBL_COPY_NAME") + String.valueOf(docIds.size()) 
                        +Msg_Move_Confirm + toPath + " ?",
            optionsPC,optionsPC[1] );
            if( (copyRes==1) || (copyRes==-1) ) return;
            if(VWArchiveConnector.checkIsAnyDocIsLocked(docIds)>0) {
            	int docLockedRes = VWMessage.showOptionDialog(container,
                        Wrn_Locked_Copy, optionsPC,optionsPC[1] );
                        
                if( (docLockedRes==1) || (docLockedRes==-1) ) return;
            }
            int count=docIds.size();
            int sCount=0;
            int overwrite=-1;
            for(int i=0;i<count;i++) 
            {
                String sDocID = (String)docIds.get(i);
                if(overwrite!=1)
                {
                    File docFile = new File(toPath, sDocID);
                    if(docFile.exists() && overwrite==3) continue;
                    if(docFile.exists() && (overwrite<0 || overwrite==0 || overwrite==2))
                    {
                         overwrite=VWMessage.showOptionDialog(container,
                             Wrn_overwrite_1 + sDocID +Wrn_overwrite_2 +  toPath + 
                             Wrn_overwrite_3, optionsYANA,optionsYANA[2]);
                         if(overwrite==2 || overwrite==3) continue;
                    }
                }
                int result = VWArchiveConnector.copyDoc(VWUtil.to_Number(sDocID), toPath, room);
                setProgressValue(i + 1, LBL_COPY_NAME);
                if(result==0) sCount++;
                if(cancelCopy)
                {   
                    break;
                }
            }
            VWMessage.showInfoDialog(container,
            String.valueOf(sCount)+Msg_Copy_Success_1+
            String.valueOf(Table.getSelectedRowCount()) +
            Msg_Copy_Success_2 + toPath);
        }
        catch(Exception e)
        {
        }
        finally
        {
            cancelCopy=false;
            setEnableMode(MODE_SELECT);
        }
        }
 }
    //-----------------------------------------------------------
 private class moveThread extends Thread
    {
        java.awt.Component container=null;
        moveThread(java.awt.Component container)
        {
            this.container=container;
            setDaemon(true);
            ///setPriority(java.lang.Thread.MAX_PRIORITY);
            start();
        }
        public void run()
        {
        try
        {
            ServerSchema storage=null;
            String stagingPath=null;
            VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
            Vector docIds=Table.getDocIds();
            setEnableMode(MODE_MOVE);
            if(VWArchiveConnector.checkIsAnyDocIsLocked(docIds)>0) {
                VWMessage.showMessage(container,Wrn_Locked_Archive);
                return;
            }
            Object[] values=VWArchiveConnector.displayMoveDlg();
            AdminWise.adminPanel.paintImmediately(AdminWise.adminPanel.getVisibleRect());
            if(values!=null) {
                stagingPath=(String)values[0];
                storage=(ServerSchema)values[1];
            }              
            int ret = VWMessage.showOptionDialog(container,
                    LBL_MOVE_NAME + String.valueOf(docIds.size()) + Msg_Move_Confirm 
                    + storage.getName() + " ?", optionsPC,optionsPC[1] );
            if(ret==1 || ret==-1) return;
            int count=docIds.size();
            int sCount=0;
            int overwrite=-1;
            for(int i=0;i<count;i++) 
            {
                String sDocID = (String)docIds.get(i);
                if(overwrite!=1)
                {
                    int docFileExists=0;
                    /*
                    int docFileExists=AdminWise.gConnector.ViewWiseClient.
                        isDocumentExists(room.getId(), new Document(VWUtil.to_Number(sDocID)), 
                        storage);
                     */
                    if(docFileExists==1 && overwrite==3) continue;
                    if(docFileExists==1 && (overwrite<0 || overwrite==0 || overwrite==2))
                    {
                         overwrite=VWMessage.showOptionDialog(container,
                             Wrn_overwrite_1 + sDocID +Wrn_overwrite_2 + 
                                storage.getName() + Wrn_overwrite_3,
                                optionsYANA,optionsYANA[2]);
                         if(overwrite==2 || overwrite==3) continue;
                    }
                }
                int result = VWArchiveConnector.moveDocs(VWUtil.to_Number(sDocID), 
                    storage, room);
                setProgressValue(i + 1, LBL_MOVE_NAME);
                if(result==0) sCount++;
                if(cancelMove)
                {                        
                    break;
                }
                if(result == ViewWiseErrors.dssCriticalError){
                	JOptionPane.showMessageDialog(null, AdminWise.gConnector.
                			ViewWiseClient.getErrorDescription(result),"Adminwise", JOptionPane.ERROR_MESSAGE);
                	return;
                }
            }
            VWMessage.showInfoDialog(container,
            String.valueOf(sCount)+Msg_Move_Success_1+
            String.valueOf(Table.getRowCount()) +
            Msg_Move_Success_2 + storage.getName());
        }
        catch(Exception e)
        {
        }
        finally
        {
            cancelMove=false;
            setEnableMode(MODE_SELECT);
        }
        }
}
}