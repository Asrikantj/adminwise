/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWArchiveConnector<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWArchive;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWStorage.VWStorageConnector;
import ViewWise.AdminWise.VWUtil.VWRecord;
import com.computhink.common.Document;
import com.computhink.common.Search;

import java.io.File;
import com.computhink.common.ServerSchema;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;

//--------------------------------------------------------------------------
public class VWArchiveConnector implements  VWConstant {

    public static int checkIsAnyDocIsLocked(Vector docIds)
    {
        List lockedDocs=
                    ViewWise.AdminWise.VWDocLock.VWDocLockConnector.getDocLock();
        for(int i=0;i<lockedDocs.size();i++)
        {
            VWStringTokenizer tokens=new VWStringTokenizer(
                    (String)lockedDocs.get(i),VWConstant.SepChar,false);
            if(docIds.contains(tokens.nextToken()))
                return 1;
        }
        return 0;
    }
    //--------------------------------------------------------------------------
    public static int copyDoc(int docId,String locationPath,VWRoom room) {
        Document doc = new Document(docId);
        return AdminWise.gConnector.ViewWiseClient.getDocFolder
        (room.getId(), doc, locationPath+File.separator+String.valueOf(docId));
    }
    //--------------------------------------------------------------------------
    public static int moveDocs(int docId, ServerSchema destinationDSS, VWRoom room) 
    {
        int result=0;
        Document doc = new Document(docId);
        if(VWStorageConnector.getFolderStorage(docId)!=destinationDSS.getId())
        {
            result=AdminWise.gConnector.ViewWiseClient.moveDocFolder
            (room.getId(), doc, destinationDSS);
            if(result== 0) {
                VWStorageConnector.setFolderStorage(docId, destinationDSS.getId(), 0);
            }
        }
        return result;
    }
    //--------------------------------------------------------------------------
    public static Object[] displayMoveDlg() {
        VWDlgArchiveMove moveDlg= new VWDlgArchiveMove(AdminWise.adminFrame,
        VWStorageConnector.getStorages());
        moveDlg.requestFocus();
        if(moveDlg.getCancelFlag()) return null;
        Object[] retValues=moveDlg.getValues();
        moveDlg.dispose();
        return retValues;
    }
    //--------------------------------------------------------------------------
    public static void loadDocumentFromFind(List list,boolean append) {
        if(!append) {
            AdminWise.adminPanel.archivePanel.clearTable();
            if(list!=null) AdminWise.adminPanel.archivePanel.addData(list);
        }
        else {
            if(list!=null) AdminWise.adminPanel.archivePanel.insertData(list);
        }
        ///AdminWise.adminPanel.archivePanel.closeFindDlg();
        if(AdminWise.adminPanel.archivePanel.Table.getRowCount()>0)
            AdminWise.adminPanel.archivePanel.Table.setRowSelectionInterval(0,0);
    }
    //--------------------------------------------------------------------------
    private static void setArchiveResult(List archiveResult) {
        int count = archiveResult.size();
        if(count==1) {
            AdminWise.adminPanel.archivePanel.clearTable();
        }
        else if(count>1) {
            List list=new LinkedList();
            for(int i=1;i<count;i++) {
                list.add(new VWRecord((String)archiveResult.get(i),0));
            }
            AdminWise.adminPanel.archivePanel.deleteRowOutOfList(list);
        }
        /*
        "<Number - 2><DocumentName>TAB<NodeId>"
        2 Loked
        /3,4 DataBase Error
        /5 No Pages
        /6 File Copy Error
        / 7 File Move Error>
         */
    }
    //--------------------------------------------------------------------------
    public static void openFindDialog(){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	com.computhink.common.vwfind.connector.VWRoom roomInfo = new com.computhink.common.vwfind.connector.VWRoom(room.getId(), room.getName());
    	roomInfo.setServer(room.getServer());
    	roomInfo.setDocuments(new Vector());
        Vector allRooms = new Vector();
        allRooms.add(roomInfo);
        Vector data = new Vector();
        System.out.println("AdminWise.adminPanel.searchHits  " + AdminWise.adminPanel.searchHits);
        AdminWise.gConnector.ViewWiseClient.openFindDialog(room.getId(), AdminWise.adminFrame, allRooms, data, AdminWise.adminPanel.searchHits, "", 0);
/*   	 if (data != null && data.size() == 2){
	    	ArrayList roomSearchData  = new ArrayList();
	    	roomSearchData = (ArrayList)data.get(1);
	    	System.out.println("Search Data " + roomSearchData);
	    	for (int count = 0; count < roomSearchData.size(); count++){
	    		com.computhink.common.vwfind.connector.VWRoom searchRoom = (com.computhink.common.vwfind.connector.VWRoom)roomSearchData.get(count);
	    		Vector searchData = searchRoom.getDocuments();
	    		loadDocumentFromFind(searchData, Boolean.valueOf(data.get(0).toString()).booleanValue());
	    	}
	    
	    }*/
   	getFindResult(data);
   	 
        //loadDocumentFromFind((Vector)data.get(1), Boolean.valueOf(data.get(0).toString()).booleanValue());
    }

    public static void getFindResult(Vector data){
		if (data != null && data.size() == 2) {
			ArrayList roomSearchData = new ArrayList();
			roomSearchData = (ArrayList) data.get(1);
			boolean isMultipleRoom = false;
			boolean isAppend = Boolean.valueOf(data.get(0).toString()).booleanValue();
			System.out.println("IsAppend " + isAppend);

			for (int count = 0; count < roomSearchData.size(); count++) {
				com.computhink.common.vwfind.connector.VWRoom searchRoom = (com.computhink.common.vwfind.connector.VWRoom) roomSearchData.get(count);
				int resultCount = searchRoom.getResultCount();
				int totalCount = 0;
				Search currentSearch = searchRoom.getSearch();
				System.out.println("result Count " + resultCount + " :: "  + searchRoom.getId());
				if (resultCount > 100) isAppend = true;				
				if ( resultCount > 0 && currentSearch != null) {
					
					while(totalCount < resultCount && resultCount > 0)
					{
						currentSearch.setRowNo(100);
						Vector searchData = new Vector();
						AdminWise.gConnector.ViewWiseClient.getFindResult(searchRoom.getId(), 
																					currentSearch, 
																					searchData);
						totalCount += searchData.size();
						System.out.println("searchData " + searchData.size());
						System.out.println("total Count " + totalCount);
						if (searchData.size() > 0)
							loadDocumentFromFind(searchData, isAppend);
						else 
							break;
						//The following code is used to read the records 100
		    			if (currentSearch != null)
		    				currentSearch.setLastDocId(((Document) searchData.get(searchData.size() - 1)).getId());

					}
				}else{
					/**
					 * Code added to clear the table if there is no result for the search.
					 * Vijaypriya.B.K - 21 April 2009
					 */
					if (!isAppend) {
					AdminWise.adminPanel.archivePanel.Table.clearData();
					}
				}
			}
		}    	
    }
    
    
}