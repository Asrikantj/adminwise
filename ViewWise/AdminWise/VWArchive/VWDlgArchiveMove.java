package ViewWise.AdminWise.VWArchive;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWUtil;
import javax.swing.border.TitledBorder;
import java.awt.Insets;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.util.List;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWConstant;

public class VWDlgArchiveMove extends javax.swing.JDialog implements VWConstant {
    public VWDlgArchiveMove(Frame frame,List locations) {
        super(frame,LBL_DOCMOVE_NAME,true);
        getContentPane().setBackground(AdminWise.getAWColor());
        getContentPane().setLayout(null);
        setSize(400,160);
        setVisible(false);
        /*
        getContentPane().add(JLabel1);
        JLabel1.setBounds(12,24,100, AdminWise.gBtnHeight);
        getContentPane().add(TxtPath);
        TxtPath.setBounds(100,24,279, AdminWise.gBtnHeight);
         */
        getContentPane().add(JLabel2);
        JLabel2.setBounds(12,/*60*/24,100, AdminWise.gBtnHeight);
        getContentPane().add(CmbLocation);
        CmbLocation.setBounds(100,/*60*/24,279, AdminWise.gBtnHeight);
        BtnOk.setText(AdminWise.connectorManager.getString("general.ok"));
        getContentPane().add(BtnOk);
        BtnOk.setBounds(235,100,72, AdminWise.gBtnHeight);
        //BtnOk.setBackground(java.awt.Color.white);
        BtnOk.setIcon(VWImages.OkIcon);
        BtnCancel.setText(AdminWise.connectorManager.getString("general.cancel"));
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(307,100,72, AdminWise.gBtnHeight);
        //BtnCancel.setBackground(java.awt.Color.white);
        BtnCancel.setIcon(VWImages.CloseIcon);
        CmbLocation.addItems(locations);
        SymAction lSymAction = new SymAction();
        BtnOk.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        TxtPath.addKeyListener(aSymKey);
        createFrame();
        setResizable(false);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        setVisible(true);
    }
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(java.awt.event.WindowEvent event) {
            Object object = event.getSource();
            if (object == VWDlgArchiveMove.this)
                BtnCancel_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnOk)
                BtnOk_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel(LBL_STAGINGPATH_NAME);
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel(LBL_STORAGESERVER_NAME);
    javax.swing.JTextField TxtPath = new javax.swing.JTextField();
    VWComboBox CmbLocation = new VWComboBox();
    VWLinkedButton BtnOk = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0, true);
    //--------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event) {
        cancelFlag=false;
        this.setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
        cancelFlag=true;
        this.setVisible(false);
    }
    //--------------------------------------------------------------------------
    public Object[] getValues() {
        Object[] values=new Object[2];
        values[0]=TxtPath.getText();
        values[1]=CmbLocation.getSelectedItem();
        return values;
    }
    //--------------------------------------------------------------------------
    public boolean getCancelFlag() {
        return cancelFlag;
    }
    //--------------------------------------------------------------------------
    void createFrame() {
        Dimension d =VWUtil.getScreenSize();
        setLocation(d.width/4,d.height/3);
    }
    //--------------------------------------------------------------------------
    boolean cancelFlag=true;
}