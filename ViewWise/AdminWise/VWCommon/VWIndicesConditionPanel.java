package ViewWise.AdminWise.VWCommon;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;

import com.computhink.vwc.VWClient;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWCommon.VWIndicesConditionsTable;
import ViewWise.AdminWise.VWCommon.VWIndicesConditionPanel;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWIndicesConditionPanel extends JPanel implements VWConstant{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	VWComboBox cmbDocTypesList = new VWComboBox();
	VWComboBox cmbIndicesList = new VWComboBox();
	VWComboBox cmbOperator = new VWComboBox();
	static JTextField txtvalue1 = new JTextField();
	static JTextField txtvalue2 = new JTextField();
	public JLabel lblDays= new JLabel(AdminWise.connectorManager.getString("indicesConditionPanel.lblDays"));
	
	JPanel pnlTable = new JPanel();
	
	public SymAction symAction = new SymAction();
	JButton btnAdd = new JButton();
	JButton btnRemove = new JButton();
	JButton btnRefresh = new JButton();
	static VWIndicesConditionsTable tblConditions = new VWIndicesConditionsTable();
	static Vector vecTableData = new Vector();
	
	VWComboBox cmbSelctionValues = new VWComboBox();
	VWDateComboBox dateValue1 = new VWDateComboBox();
	VWDateComboBox dateValue2 = new VWDateComboBox();
	
	JScrollPane spTable = new JScrollPane(tblConditions);
	public static DefaultListModel listTasksModel = new DefaultListModel();
	JList listTasks = new JList(listTasksModel);
	JScrollPane spListTasks = new JScrollPane(listTasks);
	JButton btnUp = new JButton("");
	JButton btnDown = new JButton("");
	
	public static boolean active = false;
	private boolean taskSequenceChanged = false;
	int curDocTypeSelected = -1;
	
	
	int dlgWidth = 492, dlgHeight = 480;
	int defaultDlgHeight = 285; 
	public VWIndicesConditionPanel(){
		
	}
	public VWIndicesConditionPanel(Frame parent){
		super();		
		if (active)
		{
			requestFocus();
			//toFront();
			return;
		} 
		VWClient vwc = (VWClient)AdminWise.gConnector.ViewWiseClient;
		setSize(dlgWidth, defaultDlgHeight);
		//setModal(true);
		setLocation(350,250);
		//getContentPane().setLayout(null);
		
		initTablePanel();
		setTablePanelMode(true);
		pnlTable.setBounds(7, 68, 468,213);
		//pnlTable.setBackground(Color.gray);		
		//getContentPane().add(pnlTable);
		
	}
	
	private void viewMode(){
		//Need to change the BtnRouteLocation for Retention.
		if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 2)
			BtnRouteLocation.setEnabled(false);
		else if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 1 || 
				AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 3 || AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 4)
			BtnRouteLocation.setEnabled(true);
		else
			BtnRouteLocation.setEnabled(true);
	}
	
	public JPanel initTablePanel(){
		int left = 10;
		pnlTable.setLayout(null);
		
		JLabel indexlineBorderLabel =  new JLabel("");
		JLabel indexLabel =  new JLabel(AdminWise.connectorManager.getString("indicesConditionPanel.indexLabel"));
		indexLabel.setBounds(5, 2, 100, 13);
		pnlTable.add(indexLabel);
		
		Border border1 = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		indexlineBorderLabel.setBorder(border1);
		indexLabel.setForeground(Color.BLUE);
		indexlineBorderLabel.setBounds(90, 8, 360, 2);
		pnlTable.add(indexlineBorderLabel);
		
		//TitledBorder border = new javax.swing.border.TitledBorder("Index Condition");
		//border.setTitleColor(Color.BLUE);
		//pnlTable.setBorder(border);
		
		lblDocTypes.setText(AdminWise.connectorManager.getString("indicesConditionPanel.lblDocTypes")+" ");
		lblDocTypes.setBounds(left , 20, 85, 20);
		pnlTable.add(lblDocTypes);
		
		cmbDocTypesList.setBounds(left + 90, 20, 251, 20);
		//PR Comment
		loadDocTypes();
		cmbDocTypesList.addActionListener(symAction);
		pnlTable.add(cmbDocTypesList);

		btnRefresh.setBounds(left+345 , 20, 20, 20);
		btnRefresh.setToolTipText(AdminWise.connectorManager.getString("indicesConditionPanel.btnRefresh"));
		btnRefresh.setIcon(VWImages.RefreshIcon);
		btnRefresh.addActionListener(new SymAction());
		pnlTable.add(btnRefresh);
		
		
		lblIndexField.setText(AdminWise.connectorManager.getString("indicesConditionPanel.lblIndexField")+" ");
		lblIndexField.setBounds(left , 55, 85, 20);
		pnlTable.add(lblIndexField);
		
		cmbIndicesList.setBounds(left + 90 , 55 , 251, 20);
		//loadIndicesList();
		//panelStartIconRoute.add(cmbIndiceList);
		cmbIndicesList.addItemListener(new ListSymAction());		
		pnlTable.add(cmbIndicesList);
		
		int rowPosition = 81;
		lblCondition.setText(AdminWise.connectorManager.getString("indicesConditionPanel.lblCondition")+" ");
		lblCondition.setBounds(left , rowPosition, 85, 20);
		pnlTable.add(lblCondition);
		
		loadStringOperators();
		cmbOperator.setBounds(left + 90, rowPosition, 90, 20);
		cmbOperator.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent ie){				
				VWIndexRec selectedIndexObj = (VWIndexRec) cmbIndicesList.getSelectedItem();
				int type = selectedIndexObj.getType();				
				if(type==2){
					cmbSelctionValues.setVisible(false);
					if(cmbOperator.getSelectedIndex()==COND_STARTS_AFTER){
						dateValue1.setVisible(false);
						dateValue1.setText("");
						dateValue2.setVisible(false);
						dateValue2.setText("");
						txtvalue1.setVisible(true);
						lblDays.setVisible(true);
						txtvalue2.setVisible(false);
					}
					else{
						lblDays.setVisible(false);
						dateValue1.setVisible(true);
						if(cmbOperator.getSelectedIndex()==COND_BETWEEN){							
							dateValue2.setVisible(true);
						}
						else{
							dateValue2.setVisible(false);
							dateValue2.setText("");
						}
						txtvalue1.setVisible(false);
						txtvalue2.setVisible(false);
					}
				}
				else{				
					cmbSelctionValues.setVisible(false);
					dateValue1.setVisible(false);
					dateValue1.setText("");
					dateValue2.setVisible(false);
					dateValue2.setText("");
					txtvalue1.setVisible(true);
					lblDays.setVisible(false);
					if(cmbOperator.getSelectedIndex()==COND_BETWEEN){
						txtvalue2.setVisible(true);
					}else{	
						txtvalue2.setVisible(false);
						txtvalue2.setText("");
					}
				}
				if(type==4){					
					if(cmbOperator.getSelectedIndex()==0){						
						txtvalue1.setVisible(false);
						dateValue1.setVisible(false);
						cmbSelctionValues.setVisible(true);						
					}
					else{
						txtvalue1.setVisible(true);
						dateValue1.setVisible(false);
						cmbSelctionValues.setVisible(false);
					}
				}
			}
		});
		pnlTable.add(cmbOperator);
		
		txtvalue1.setBounds(left + 182, rowPosition, 100,20);
		pnlTable.add(txtvalue1);
		
		lblDays.setBounds(left + 287, rowPosition, 100,20);
		pnlTable.add(lblDays);
		lblDays.setVisible(false);		
		
		dateValue1.setText("");
		dateValue1.setBounds(left + 182, rowPosition, 100,20);
		dateValue1.setDateFormat("MM/dd/yyyy");
		pnlTable.add(dateValue1);								
		dateValue1.setVisible(false);
		
		cmbSelctionValues.setBounds(left + 182, rowPosition, 158,20);		
		pnlTable.add(cmbSelctionValues);								
		cmbSelctionValues.setVisible(false);
				
		txtvalue2.setText("");
		txtvalue2.setBounds(left + 287, rowPosition, 100, 20);
		txtvalue2.setVisible(false);
		pnlTable.add(txtvalue2);
		
		dateValue2.setText("");
		dateValue2.setBounds(left + 287, rowPosition, 100, 20);
		dateValue2.setDateFormat("MM/dd/yyyy");
		pnlTable.add(dateValue2);								
		dateValue2.setVisible(false);

		
		//loadLogicalOperators();
		//cmbLogicalOperator.setBounds(268, 28, 60, 20);
		//pnlTable.add(cmbLogicalOperator);
		
		//btnAdd.setText("Add");
		btnAdd.setToolTipText(AdminWise.connectorManager.getString("indicesConditionPanel.btnAddToolTip"));
		btnAdd.setBounds(415, rowPosition + 7, 20, 20);
		btnAdd.setMnemonic('A');
		pnlTable.add(btnAdd);
		btnAdd.setIcon(VWImages.plusIcon);
		btnAdd.addActionListener(new SymButton());
		
		//btnRemove.setText("Remove");
		btnRemove.setToolTipText(AdminWise.connectorManager.getString("indicesConditionPanel.btnRemoveToolTip"));
		btnRemove.setBounds(438, rowPosition + 7, 20, 20);
		btnRemove.setMnemonic('R');
		btnRemove.setIcon(VWImages.minusIcon);
		pnlTable.add(btnRemove);
		btnRemove.addActionListener(new SymButton());
		
		spTable.setBounds(10, 110, 450, 105);
		pnlTable.add(spTable);
		//setTitle("Indices Condition Panel");
		return pnlTable;
	}
	
	public class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if(object == btnUp){
				btnUp_ActionPerformed(event);
			}else if(object == btnDown){
				btnDown_ActionPerformed(event);        				
			}else if(object == btnRefresh){
				btnRefresh_ActionPerformed(event);
			}else if(object == cmbDocTypesList){
				cmbDocTypesList_ActionPerformed();
			}			
		}
	}
	
	private void loadValues()
	{
	    cmbSelctionValues.removeAllItems();
	    VWIndexRec indexRec = (VWIndexRec) cmbIndicesList.getSelectedItem();
	    Vector strValues = indexRec.selectionValues;
	    if(strValues==null || strValues.equals("")) return;
	    for(int i=0;i<strValues.size();i++){
		cmbSelctionValues.addItem(strValues.get(i));
	    }
	}
	public void loadDocTypes() {
        cmbDocTypesList.removeAllItems();
        cmbDocTypesList.addItem(new VWDocTypeRec(-1,LBL_DOCTYPE_NAME_1,false));
        Vector data=VWDocTypeConnector.getDocTypes();
        if (data!=null)
            for (int i=0;i<data.size();i++)
                cmbDocTypesList.addItem(data.get(i));
    }
	
	public void loadIndicesForSelectedDocType(){
		if(cmbDocTypesList.getSelectedIndex()>0){
			VWDocTypeRec selDocType=(VWDocTypeRec) cmbDocTypesList.getSelectedItem();		
			List indicesList =  VWDocTypeConnector.getDTIndices(selDocType.getId());
		    if(indicesList==null || indicesList.size()==0) {
		        return;
		    }	  
		    try{
		    	if(cmbIndicesList.getItemCount()>0)
		    		cmbIndicesList.removeAllItems();
			}catch(Exception ex){
				//System.out.println("Error1 "+ex.toString());
			}		
			for(int count=0; count<indicesList.size();count++){
				try{
					VWIndexRec indexRec = (VWIndexRec) indicesList.get(count);
					String type = indexRec.getType()+"";
					//Exclude Sequence datatype from the display
					if(type.trim().equalsIgnoreCase("5")){
						continue;
					}
					cmbIndicesList.addItem(indexRec);
				}catch(Exception ex){
					//System.out.println(" addItem Error2 "+ex.toString());
				}
			}
		}
		else{
			loadIndicesList();
		}
		cmbIndicesList.setSelectedIndex(0);
	}
	public void setTablePanelMode(boolean mode){
		if(mode){
			pnlTable.setVisible(true);
		}
		else{
			pnlTable.setVisible(false);
		}
	}
	public void loadIndicesList(){
	    VWIndexRec[] indices = VWDocTypeConnector.getIndices(false);
	    try{
		if((cmbIndicesList!=null)&& cmbIndicesList.getItemCount()>0)
		    cmbIndicesList.removeAllItems();
	    }catch(Exception ex){
		//System.out.println("Error in loadIndicesList "+ex.toString());
	    }
	    try{
		for(int count=0; count<indices.length;count++){
		    VWIndexRec vwIndexRec = (VWIndexRec)indices[count];
		    if(vwIndexRec.getType() == 5)
			continue;
		    cmbIndicesList.addItem(indices[count]);
		}			
	    }catch(Exception ex){
		//System.out.println("<<Error in loadIndicesList "+ex.toString());
	    }			
	    if(cmbIndicesList.getItemCount()>0)
		cmbIndicesList.setSelectedIndex(0);
	}
	
	class ListSymAction implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent iEvent){
			Object object=iEvent.getSource();
			if(object==cmbRouteStart){
				cmbRouteStart_ItemStateChanged();
			} else if(object==cmbIndicesList){
				cmbIndicesList_ItemStateChanged();
			}					
		}
	}
	class SymButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(ae.getSource() == btnAdd){
				//Allow maximum 10 conditions.
				if(tblConditions.getRowCount()>=10){
					active = false;
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg0"), "Info", JOptionPane.OK_OPTION);
					active = true;					
					return;
				}
				curDocTypeSelected = cmbDocTypesList.getSelectedIndex();
				String selIndiceName = ((VWIndexRec)cmbIndicesList.getSelectedItem()).toString();
				//Check for duplicate indice entry.
				VWIndexRec vwIndexRec = (VWIndexRec) cmbIndicesList.getSelectedItem();
				String indexId = String.valueOf(vwIndexRec.getId());
				String indexTypeId = ""+vwIndexRec.getType();
				if(tblConditions.isDuplicate(indexId)){
					active = false;
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg1"), "Info", JOptionPane.OK_OPTION);
					active = true;					
					return;
				}				
				String selOperator = cmbOperator.getSelectedItem().toString();
				String selValue1 = "";
				String selValue2 = "";

				int type = vwIndexRec.getType();//getIndexType(selIndiceName);
				int selectedOPIndex = cmbOperator.getSelectedIndex();

				//NUMERIC TYPE condition checking
				if(type == 1){
					if( (!VWUtil.isNumeric(txtvalue1.getText(), false)) ){
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg2"), "Info", JOptionPane.OK_OPTION);
						txtvalue1.requestFocus();
						active = true;
						return;
					}
					if( (selectedOPIndex==COND_BETWEEN  && !VWUtil.isNumeric(txtvalue2.getText(), false)) ){
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg3"), "Info", JOptionPane.OK_OPTION);
						txtvalue2.requestFocus();
						active = true;
						return;
					}
				}
				//End...						
				
				if (type == 2 && selectedOPIndex==COND_BETWEEN){
					String selDateValue1 = dateValue1.getSelectedItem().toString();					
					String selDateValue2 = dateValue2.getSelectedItem().toString();
					if(selDateValue1.trim().equals("")){
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg4"), "Info", JOptionPane.OK_OPTION);
						active = true;
						dateValue1.requestFocus();
						return;
					}else if(selDateValue2.trim().equals("")){
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg5"), "Info", JOptionPane.OK_OPTION);
						active = true;
						dateValue2.requestFocus();
						return;
					}
				}
				
				if (type == 2 && selectedOPIndex==COND_STARTS_AFTER){
					if(!VWUtil.isNumeric(txtvalue1.getText(), true)){					
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg6"), "Info", JOptionPane.OK_OPTION);
						active = true;
						txtvalue1.requestFocus();
						return;
					}									
				}
				
				if (type == 2 && selectedOPIndex!=COND_STARTS_AFTER){
					selValue1 = dateValue1.getSelectedItem().toString();
					selValue2 = dateValue2.getSelectedItem().toString();
				}else{
					selValue1 = txtvalue1.getText();
					selValue2 = txtvalue2.getText();					
				}				
				
				//Selection
				if (type == 4){
					 if(selectedOPIndex==0){
						 selValue1=cmbSelctionValues.getSelectedItem().toString();
					 }
					 else{
						 selValue1 = txtvalue1.getText();
					 }
				}
				else{
					
				}
				//End...
				
				if(selValue1.trim().equals("")){
					active = false;
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg7"), "Info", JOptionPane.OK_OPTION);
					active = true;
					if (type == 2)
						dateValue1.requestFocus();
					else
						txtvalue1.requestFocus();
				}else{
					String selLogicalOperator = "AND";
					if(selValue2.trim().equals("")){
						selValue2="-";
					}
					String curSelectedData = "";
					if(vecTableData.size()==0)
						curSelectedData = "	"+selIndiceName + "\t" + selOperator + "\t" +  selValue1 +
							"\t" +"-" + "\t"+selValue2 + "\t"+indexId+"\t"+indexTypeId;
					else
						curSelectedData = "	"+selIndiceName + "\t" + selOperator + "\t" +  selValue1 + 
							"\t" +selLogicalOperator + "\t"+selValue2 + "\t"+indexId+"\t"+indexTypeId;
					
					vecTableData.add(curSelectedData);
					tblConditions.addData(vecTableData);
				}
			}
			
			if(ae.getSource() == btnRemove){
				BtnRemove_actionPerformed(ae);
			}
		}
	}
	
	public void clearTableAndRelatedData(){
		try{
			vecTableData = new Vector();
			tblConditions.clearData();
			txtRouteLocation.setText("");			

		}catch(Exception ex){}
	}
	
	public void clearAll(){
		try{
			curDocTypeSelected = -1;
			this.cmbDocTypesList.setSelectedIndex(0);
			this.cmbIndicesList.setSelectedIndex(0);
			this.cmbOperator.setSelectedIndex(0);
		}catch(Exception ex){
			//System.out.println("Error in clear :"+ex.toString());
		}
	}
	public void clearAllText(){
		txtvalue1.setText("");
		txtvalue2.setText("");
		dateValue1.setText("");
		dateValue2.setText("");
	}
	public void loadOperators(){
		cmbOperator.addItem(">");
		cmbOperator.addItem(">=");
		cmbOperator.addItem("<");
		cmbOperator.addItem("<=");
		cmbOperator.addItem("=");
		cmbOperator.addItem("!=");
	}
	public void loadStringOperators(){
		cmbOperator.addItem("=");
		cmbOperator.addItem("like");
	}
	public void loadIntDateOperators(boolean displayStartsAfter){
		cmbOperator.addItem(">");
		cmbOperator.addItem(">=");
		cmbOperator.addItem("<");
		cmbOperator.addItem("<=");
		cmbOperator.addItem("=");
		cmbOperator.addItem("!=");
		cmbOperator.addItem(COND_BETWEEN_STR);
		if(displayStartsAfter)
			cmbOperator.addItem(COND_STARTS_AFTER_STR);
	}
	
	public void btnUp_ActionPerformed(ActionEvent event){
		try{
			//CODE FOR MOVING UP...			
			int selectedIndex = listTasks.getSelectedIndex();
			if(selectedIndex>0){
				String selectedItem1 = String.valueOf(listTasksModel.get(selectedIndex-1));
				String selectedItem2 = String.valueOf(listTasksModel.get(selectedIndex));    				
				listTasksModel.remove(selectedIndex-1);
				listTasksModel.add(selectedIndex-1,selectedItem1);
				listTasksModel.remove(selectedIndex);
				listTasksModel.add(selectedIndex-1,selectedItem2);
				listTasks.setSelectedIndex(selectedIndex-1);
				taskSequenceChanged = true;
			}
		}catch(Exception e){
			//System.out.println("Exception in Up BTN Click :"+e.toString());
		}
		//END...		
	}
	
	
	public void btnDown_ActionPerformed(ActionEvent event){
		try{
			//CODE FOR MOVING DOWN...				
			int selectedIndex = listTasks.getSelectedIndex();
			if(selectedIndex>=0 && selectedIndex<listTasks.getModel().getSize()){
				String selectedItem1 = String.valueOf(listTasksModel.get(selectedIndex+1));
				String selectedItem2 = String.valueOf(listTasksModel.get(selectedIndex));
				listTasksModel.remove(selectedIndex+1);
				listTasksModel.add(selectedIndex+1,selectedItem1);
				listTasksModel.remove(selectedIndex);
				listTasksModel.add(selectedIndex+1,selectedItem2);
				listTasks.setSelectedIndex(selectedIndex+1);
				taskSequenceChanged = true;
			}
		}catch(Exception e){
			//System.out.println("Exception in Down BTN Click :"+e.toString());
		}
		//END...
	} 
	public void btnRefresh_ActionPerformed(ActionEvent event){
		try{
			int curselected = cmbDocTypesList.getSelectedIndex();
			if(curselected>0){
				loadIndicesForSelectedDocType();
			}else{
				loadDocTypes();
				loadIndicesList();
			}
		}catch(Exception ex){
		}
	
	}
	public void cmbDocTypesList_ActionPerformed(){
		try{
			int rowCount = tblConditions.getRowCount();
			int curselected = cmbDocTypesList.getSelectedIndex();
			if(curDocTypeSelected != curselected){
				if( (rowCount>0)  && (curDocTypeSelected!=-1) ){
					if(AdminWise.adminPanel.routePanel.CURRENT_MODE != 2 && curDocTypeSelected != 0 || 
							(curselected>0 && curDocTypeSelected == 0 && AdminWise.adminPanel.routePanel.CURRENT_MODE != 2)){
						active = false;
						VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg8"));
						active = true;
						cmbDocTypesList.setSelectedIndex(curDocTypeSelected);
						return;
					}
				}
				else{
					clearAllText();
					loadIndicesForSelectedDocType();
				}
			}
		}catch(Exception e){
			//System.out.println("EXCEPTION :"+e.toString());
		}
	}
	
	//Item State Changed Event for RouteStart Combo...
	public void cmbRouteStart_ItemStateChanged(){
			if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[1])){
				txtRouteLocation.setText("");
				lblTriggerAt.setVisible(false);
				//lblDocTypes.setVisible(false);
				txtRouteLocation.setVisible(false);
				BtnRouteLocation.setVisible(false);
				panelStartIconRoute.setBounds(7, 5, 472, 48);
				panelSeqTask.setBounds(7, 63, 472, 115);
				//BtnSave.setBounds(305, 185,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				//BtnCancel.setBounds(387, 185,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
/*				lblTaskSequenceTitle.setBounds(10, 60, 240, 22);
				spListTasks.setBounds(10, 80, 425, 80);
				btnUp.setBounds(436, 80, 24, 22);
				btnDown.setBounds(436, 105, 24, 22);				
				BtnSave.setBounds(293, 175, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(380, 175, 80, AdminWise.gBtnHeight);*/
				
				setTablePanelMode(false);
				setSize(dlgWidth, 250);
			}
			else if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[0])){
				lblTriggerAt.setVisible(true);
				//lblDocTypes.setVisible(false);
				//lblIndexField.setVisible(false);
				txtRouteLocation.setVisible(true);
				txtRouteLocation.setText("");
				BtnRouteLocation.setVisible(true);
				
				lblTriggerAt.setText(AdminWise.connectorManager.getString("indicesConditionPanel.lblTriggerAt")+" ");
				panelStartIconRoute.setBounds(7, 5, 472, 85);
				panelSeqTask.setBounds(7, 90, 472, 115);
				//BtnSave.setBounds(305, 212,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				//BtnCancel.setBounds(387, 212,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				
				
/*				lblTaskSequenceTitle.setBounds(10, 105, 240, 22);
				spListTasks.setBounds(10, 125, 425, 80);
				btnUp.setBounds(436, 125, 24, 22);
				btnDown.setBounds(436, 150, 24, 22);				
				BtnSave.setBounds(300, 220, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(382, 220, 80, AdminWise.gBtnHeight);
*/				
				setTablePanelMode(false);
				setSize(dlgWidth, 277);
				
			}else if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[2])){
				//lblIndexField.setVisible(true);
				lblTriggerAt.setVisible(false);
				//lblDocTypes.setVisible(true);
				txtRouteLocation.setVisible(false);
				BtnRouteLocation.setVisible(false);
				clearAll();
				clearTableAndRelatedData();
				//lblTriggerAt.setText("Select document type: ");
				//lblIndexField.setText("Select index field: ");		
				panelStartIconRoute.setBounds(7, 5, 472, 58);
				pnlTable.setBounds(7, 63, 472, 228);
				panelSeqTask.setBounds(7, 293, 472, 115);
				//BtnSave.setBounds(305, 415,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				//BtnCancel.setBounds(387, 415,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				
/*				lblTaskSequenceTitle.setBounds(10, 250, 240, 22);
				spListTasks.setBounds(10, 270, 425,80);
				btnUp.setBounds(436, 270, 24, 22);
				btnDown.setBounds(436, 295, 24, 22);				
				BtnSave.setBounds(300, 365, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(382, 365, 80, AdminWise.gBtnHeight);*/
				setTablePanelMode(true);
				setSize(dlgWidth,dlgHeight);
			}
	}
	public void cmbIndicesList_ItemStateChanged(){
		if(cmbIndicesList!=null && cmbIndicesList.getItemCount()>0){
			VWIndexRec selectedIndexObj = (VWIndexRec) cmbIndicesList.getSelectedItem();
			clearAllText();
			int type = selectedIndexObj.getType();
			//if selected index is Date object
			if(type==2){
				dateValue1.setVisible(true);
				txtvalue1.setVisible(false);
				txtvalue2.setVisible(false);
			}
			else{
				dateValue1.setVisible(false);
				dateValue2.setVisible(false);
				txtvalue1.setVisible(true);
			}
			cmbOperator.removeAllItems();
			if(type==0 || type == 3){
				loadStringOperators();
				txtvalue1.setVisible(true);
				txtvalue2.setVisible(false);
			}else if(type==1){
				loadIntDateOperators(false);
			}else if(type==2){
				loadIntDateOperators(true);
			}
			else if(type == 4){
				loadStringOperators();
				loadValues();
				cmbSelctionValues.setVisible(true);
			}
			else{
				loadOperators();
			}
		}
	}
	void BtnRemove_actionPerformed(java.awt.event.ActionEvent event){
		try{				
			if (tblConditions == null || tblConditions.getSelectedRows().length <= 0){
						active =false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,AdminWise.connectorManager.getString("indicesConditionPanel.optionpaneMsg9"));
						active = true;
						return;
			}
			int selRows[] = tblConditions.getSelectedRows();
			for(int curRow=(selRows.length-1); curRow>=0; curRow--){
				vecTableData.remove(selRows[curRow]);
				tblConditions.m_data.delete(selRows[curRow]);
				tblConditions.m_data.fireTableDataChanged();
			}
			if(tblConditions.getRowCount()==0){
				curDocTypeSelected = -1;
			}
		}catch(Exception e){
			//AdminWise.printToConsole("Error :"+e.toString());
		}
	}
	
	JLabel lblDocTypes = new JLabel();
	JLabel lblIndexField = new JLabel();
	JLabel lblCondition = new JLabel();
	JLabel lblTriggerAt = new JLabel();
	JComboBox cmbRouteStart = new JComboBox();
	static JTextField txtRouteLocation = new JTextField();
	JPanel panelStartIconRoute = new JPanel();
	JPanel panelSeqTask = new JPanel();
	VWLinkedButton BtnRouteLocation = new VWLinkedButton(2);
	
	public static void main(String[] args) {
		/*try{
			String plasticLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}
		catch(Exception se){
		}
		JFrame frame = new JFrame();
		new VWImages();
		VWIndicesConditionPanel setting = new VWIndicesConditionPanel(null);
		
		//frame.add(setting);
		setting.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.pack();*/
		try {
			String plasticLookandFeel = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		} catch (Exception se) {
		}
		JFrame frame = new JFrame();
		VWIndicesConditionPanel panel = new VWIndicesConditionPanel();
		frame.add(panel);
		//panel.setupUI();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*
		 * JFrame.resize() is replaced with JFrame.setSize() as JFrame.setSize() is deprecated
		 * Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
		frame.setSize(800, 600);
	}
}