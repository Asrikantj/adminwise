/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWATTable<br>
 *
 * @version     $Revision: 1.4 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWAuditTrail;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumnModel;
import java.awt.Dimension;
import java.util.List;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;

import java.util.Enumeration;
import java.util.Vector;
import java.util.Comparator;
import java.util.Collections;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWUtil.VWUtil;

//--------------------------------------------------------------------------
public class VWATTable extends JTable implements VWConstant {
    protected VWATTableData m_data;
    TableColumn[] ColModel=new TableColumn[8];
    static boolean[] visibleCol={true,true,true,true,true,true,false,false};
    public VWATTable(){
        super();
        m_data = new VWATTableData(this);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JTextField readOnlyText=new JTextField();
        readOnlyText.setEditable(false);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWATTableData.m_columns.length; k++) {
            TableCellRenderer renderer;
            ///DefaultTableCellRenderer textRenderer =new DefaultTableCellRenderer();
            DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWATTableData.m_columns[k].m_alignment);
            renderer = textRenderer;
            VWATRowEditor editor=new VWATRowEditor(this);
            TableColumn column = new TableColumn
            (k,Math.round(tableWith.width*VWATTableData.m_columns[k].m_width),
            renderer, editor);
            addColumn(column);
            ColModel[k]=column;
        }
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        setVisibleCols();
        setBackground(java.awt.Color.white);
        ///setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
                if (listSelectionModel.isSelectionEmpty())
                    AdminWise.adminPanel.auditTrailPanel.setEnableMode(MODE_UNSELECTED);
                else
                    AdminWise.adminPanel.auditTrailPanel.setEnableMode(MODE_SELECT);
            }
        });
        setRowHeight(TableRowHeight);

    	int tableWidth = 970; //getWidth();
    	int visibleColums = getColumnCount();
    	int individualColWidth = tableWidth / visibleColums;
    	for(int count=0;count<visibleColums;count++){
    		getColumnModel().getColumn(count).setPreferredWidth(individualColWidth);
    	}

    	getColumnModel().addColumnModelListener(new ColumnMovementListener());
    	
    	setTableResizable();
    }
    
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    
    /**
     * 
     * @author Nishad Nambiar
     * Desc  :This class is added for handling column Add or Remove Events.
     * 		  Columns are resized according to the number of columns available.
     * 		  Logic is - Individual column width = 970 / Number Of Columns Available. 
     * Date	 :29-Aug-2007
     *
     */
    public class ColumnMovementListener implements TableColumnModelListener
    {
        public void columnAdded(TableColumnModelEvent e)
        {
        	int tableWidth = 970; //getWidth();
        	int visibleColums = getColumnCount();
        	int individualColWidth = tableWidth / visibleColums;
        	for(int count=0;count<visibleColums;count++){
        		getColumnModel().getColumn(count).setPreferredWidth(individualColWidth);
        	}
        }
        public void columnRemoved(TableColumnModelEvent e)
        {
        	int tableWidth = 970; //getWidth();
        	int visibleColums = getColumnCount();
        	int individualColWidth = tableWidth / visibleColums;
        	for(int count=0;count<visibleColums;count++){
        		getColumnModel().getColumn(count).setPreferredWidth(individualColWidth);
        	}
        }
        public void columnMoved(TableColumnModelEvent e)
        {

        }
        
        public void columnMarginChanged(ChangeEvent e)
        {
        }
        
        public void columnSelectionChanged(ListSelectionEvent e)
        {
        }
    }
    
    //--------------------------------------------------------------------------
    public void addData(List list) {
        m_data.setData(list);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void insertData(List list) {
        m_data.insert(list);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void clearData() {
        m_data.clear();
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        if (m_data==null) return 0;
        return m_data.getRowCount();
    }
    //--------------------------------------------------------------------------
    public void deleteRow(int row) {
        m_data.delete(row);
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public void deleteSelectedRows() {
        int[] rows=getSelectedRows();
        int count=rows.length;
        for(int i=0;i<count;i++) {
            m_data.delete(rows[i]-i);
        }
        m_data.fireTableDataChanged();
    }
    //--------------------------------------------------------------------------
    public Vector getIds() {
        String[][] data=m_data.getData();
        int count=data.length;
        Vector idsList =new Vector();
        for(int i=0;i<count;i++) {
            idsList.add(data[i][0]);
        }
        return idsList;
    }
    //--------------------------------------------------------------------------
    public String[][] getData() {
        return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public String[][] getReportData() {
        String[][] tableData=m_data.getData();
        int count=tableData.length;
        /**@author apurba.m
         * range of repData has been changed to 100 for custom indices
         */
        String[][] repData=new String[count][100];
        for(int i=0;i<count;i++) {
            repData[i][0]=tableData[i][3];
            repData[i][1]=tableData[i][2];
            repData[i][2]=tableData[i][4];
            repData[i][3]=tableData[i][7];
            repData[i][4]=tableData[i][5];
            repData[i][5]=tableData[i][6];
            repData[i][6]=tableData[i][8]==null?"":tableData[i][8];
            repData[i][7]=tableData[i][9]==null?"":tableData[i][9];
        }
        return repData;
    }
    //--------------------------------------------------------------------------
    public String[] getATRowData(int rowNum) {
        return m_data.getATRowData(rowNum);
    }
    //--------------------------------------------------------------------------
    public int getRowDocId(int rowNum) {
        String[] row=m_data.getATRowData(rowNum);
        return VWUtil.to_Number(row[5]);
    }
    //--------------------------------------------------------------------------
    public String getseletedDocIds(String sep) {
        int[] rows=getSelectedRows();
        int count=rows.length;
        String[][] data = m_data.getData();
        String docIds="";
        for(int i=0;i<count-1;i++) {
            docIds+=data[rows[i]][5] + sep;
        }
        docIds+=data[rows[count-1]][5];
        return docIds;
    }
    //--------------------------------------------------------------------------
    public String getDocIds(String sep) {
        String[][] data = m_data.getData();
        int count=data.length;
        String docIds="";
        for(int i=0;i<count-1;i++) {
            docIds+=data[i][5] + sep;
        }
        docIds+=data[count-1][5];
        return docIds;
    }
    //--------------------------------------------------------------------------
    private void setVisibleCols() {
        TableColumnModel model=getColumnModel();
        
        int count =visibleCol.length;
        int i=0;
        while(i<count) {
            if (!visibleCol[i]) {
                TableColumn column = model.getColumn(i);
                model.removeColumn(column);
                count--;
            }
            else {
                i++;
            }
        }
        if(count==0)return;
        Dimension tableWith = getPreferredScrollableViewportSize();
        int colWidth=(int)Math.round(tableWith.getWidth()/count);
        for (i=0;i<count;i++) {
            model.getColumn(i).setPreferredWidth(colWidth);
        }
        setColumnModel(model);
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }
    //--------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
        public void mouseClicked(java.awt.event.MouseEvent event) {
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWATTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWATTable_LeftMouseClicked(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWATTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            lSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            lDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    void VWATTable_RightMouseClicked(java.awt.event.MouseEvent event) {
        Point origin = event.getPoint();
        int row = rowAtPoint(origin);
        int column = columnAtPoint(origin);
        if (row == -1 || column == -1)
            return; // no cell found
        if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
        else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
    }
    //--------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
    /*
    VWMenu menu=null;
    int roomId=getRowRoomId(row);
    int docId=getRowDocId(row);
    if(VWTableConnector.checkIsDocOpen(roomId,docId))
        if(!AdminWise.gConnector.isDocumentShare(roomId,docId))
        {
            menu=new VWMenu(VWConstant.OpenDocument_TYPE);
            menu.show(this,event.getX(),event.getY());
        }
        else
        {
            menu=new VWMenu(VWConstant.OpenShareDocument_TYPE);
            menu.show(this,event.getX(),event.getY()-70);
        }
     */
    }
    //--------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
    }
    //--------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
        
    }
    //--------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
        LoadRowData(row);
    }
    //--------------------------------------------------------------------------
    private void LoadPropData(int row) {
    }
    //--------------------------------------------------------------------------
    private void LoadRowData(int row) {
    /*
    int docId=getRowDocId(row);
    int roomId=getRowRoomId(row);
    String docName=(String)getValueAt(row,1);
    VWTreeConnector.setTreeNodeSelection(docId,roomId,true);
    VWTableConnector.getDocFile(roomId,docId,docName,VWTreeConnector.getFixTreePath());
     **/
    }
    //--------------------------------------------------------------------------
}
class ATRowData {
    public int     m_RecId;
    public int     m_ObjectId;
    public String  m_ObjectType;
    public String  m_ObjectName;
    public String  m_Action;
    public String  m_UserName;
    public String  m_CLIENT;
    public String  m_DateTime;
    public String  m_Location;
    public String  m_Description;
    
    public ATRowData() {
        
        m_RecId=0;
        m_ObjectId=0;
        m_ObjectType="";
        m_ObjectName="";
        m_Action="";
        m_UserName="";
        m_CLIENT="";
        m_DateTime="";
        m_Location="";
        m_Description="";
    }
    //--------------------------------------------------------------------------
    public ATRowData(String string) {
        if(string==null || string.equals("")) return;
        VWStringTokenizer tokens=new VWStringTokenizer(string,VWConstant.SepChar,false);
        m_RecId=VWUtil.to_Number(tokens.nextToken());
        m_ObjectId=VWUtil.to_Number(tokens.nextToken());
        m_ObjectType=VWConstant.ATStrObjects[VWUtil.to_Number(tokens.nextToken())];
        m_ObjectName=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown name>");
        m_UserName=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown user>");
        m_CLIENT=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown host>");
        m_DateTime=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown date>");
        int index=VWUtil.to_Number(tokens.nextToken());
        AdminWise.printToConsole("index ------>"+index);
        /***Below condition has added for Document Resubmitted to Workflow*****/
        if (index == 73)
        	index = 70;
        m_Action=VWConstant.ATStrActions[index];
        AdminWise.printToConsole("after index.....");
        m_Location=VWUtil.getFixedValue(tokens.nextToken(),"");
        ///m_Location=VWUtil.fixNodePath(VWUtil.getFixedValue(tokens.nextToken(),""));
        m_Description=VWUtil.getFixedValue(tokens.nextToken(),"");
    }
}
//--------------------------------------------------------------------------
class ATColumnData {
    public String  m_title;
    float m_width;
    int m_alignment;
    
    public ATColumnData(String title, float width, int alignment) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
    }
}
//--------------------------------------------------------------------------
class VWATTableData extends AbstractTableModel {
    public static final ATColumnData m_columns[] = {
        new ATColumnData(VWConstant.ATColumnNames[0],0.8F,JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[1],0.8F, JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[2],0.2F,JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[3],0.1F,JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[4],0.3F,JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[5],0.4F,JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[6],0.3F,JLabel.LEFT),
        new ATColumnData(VWConstant.ATColumnNames[7],0.8F,JLabel.LEFT)
    };
    public static final int COL_OBJECTNAME=0;
    public static final int COL_OBJECTTYPE=1;
    public static final int COL_ACTION=2;
    public static final int COL_DATETIME=3;
    public static final int COL_USERNAME=4;
    public static final int COL_CLIENT=5;
    public static final int COL_LOCATION=6;
    public static final int COL_DESCRIPTION=7;
    
    protected VWATTable m_parent;
    protected Vector m_vector;
    protected int m_sortCol = 0;
    protected boolean m_sortAsc = true;
    
    public VWATTableData(VWATTable parent) {
        m_parent = parent;
        m_vector = new Vector();
    }
    //--------------------------------------------------------------------------
    public void setData(List data) {
        m_vector.removeAllElements();
        if(data==null || data.size()==0)
            return;
        int count =data.size();
        for(int i=0;i<count;i++)
        {
            m_vector.addElement(new ATRowData((String)data.get(i)));
        }
    }
    //--------------------------------------------------------------------------
    public String[][] getData() {
        int count=getRowCount();
        String[][] data=new String[count][10];
        for(int i=0;i<count;i++) {
            int j=0;
            ATRowData row=(ATRowData)m_vector.elementAt(i);
            data[i][j++]=String.valueOf(row.m_RecId);
            data[i][j++]=String.valueOf(row.m_ObjectId);
            data[i][j++]=row.m_ObjectType;
            data[i][j++]=row.m_ObjectName;
            data[i][j++]=row.m_Action;
            data[i][j++]=row.m_UserName;
            data[i][j++]=row.m_CLIENT;
            data[i][j++]=row.m_DateTime;
            data[i][j++]=row.m_Location;
            data[i][j++]=row.m_Description;
        }
        return data;
    }
    //--------------------------------------------------------------------------
    public String[] getATRowData(int rowNum) {
        int count=getRowCount();
        String[] ATRowData=new String[10];
        int j=0;
        ATRowData row=(ATRowData)m_vector.elementAt(rowNum);
        ATRowData[j++]=String.valueOf(row.m_RecId);
        ATRowData[j++]=String.valueOf(row.m_ObjectId);
        ATRowData[j++]=row.m_ObjectType;
        ATRowData[j++]=row.m_ObjectName;
        ATRowData[j++]=row.m_Action;
        ATRowData[j++]=row.m_UserName;
        ATRowData[j++]=row.m_CLIENT;
        ATRowData[j++]=row.m_DateTime;
        ATRowData[j++]=row.m_Location;
        ATRowData[j++]=row.m_Description;
        
        return ATRowData;
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return m_vector==null ? 0 : m_vector.size();
    }
    //--------------------------------------------------------------------------
    public int getColumnCount() {
        return m_columns.length;
    }
    //--------------------------------------------------------------------------
    public String getColumnName(int column) {
        ///return m_columns[column].m_title;
        String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
            return str;
    }
    //--------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
        return true;
    }
    //--------------------------------------------------------------------------
    public java.lang.Object getValueAt(int nRow, int nCol) {
        if (nRow < 0 || nRow>=getRowCount())
            return "";
        ATRowData row = (ATRowData)m_vector.elementAt(nRow);
        switch (nCol) {
            case COL_OBJECTNAME: return row.m_ObjectName;
            case COL_OBJECTTYPE: return row.m_ObjectType;
            case COL_ACTION: return row.m_Action;
            case COL_USERNAME: return row.m_UserName;
            case COL_CLIENT: return row.m_CLIENT;
            case COL_DATETIME: return row.m_DateTime;
            case COL_LOCATION: return row.m_Location;
            case COL_DESCRIPTION: return row.m_Description;
        }
        return "";
    }
    //--------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
        if (nRow < 0 || nRow >= getRowCount())
            return;
        ATRowData row = (ATRowData)m_vector.elementAt(nRow);
        String svalue = value.toString();
        
        switch (nCol) {
            case COL_OBJECTNAME:
                row.m_ObjectName=svalue;
                break;
            case COL_OBJECTTYPE:
                row.m_ObjectType=svalue;
                break;
            case COL_ACTION:
                row.m_Action=svalue;
                break;
            case COL_USERNAME:
                row.m_UserName=svalue;
                break;
            case COL_CLIENT:
                row.m_CLIENT=svalue;
                break;
            case COL_DATETIME:
                row.m_DateTime=svalue;
            case COL_LOCATION:
                row.m_Location=svalue;
            case COL_DESCRIPTION:
                row.m_Description=svalue;
                break;
        }
    }
    //-----------------------------------------------------------
    public void insert(List data) {
        if(data==null || data.size()==0)
            return;
        int row=m_vector.size();
        int count =data.size();
        for(int i=0;i<count;i++)
        {
            m_vector.insertElementAt(new ATRowData((String)data.get(i)),row++);
        }
    }
    //-----------------------------------------------------------
    public boolean delete(int row) {
        if (row < 0 || row >= m_vector.size())
            return false;
        m_vector.remove(row);
        return true;
    }
    //--------------------------------------------------------------------------
    public void clear(){
        m_vector.removeAllElements();
    }
    //--------------------------------------------------------------------------
    class ColumnListener extends MouseAdapter {
        protected VWATTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWATTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
            
            if(e.getModifiers()==e.BUTTON3_MASK)
                selectViewCol(e);
            else if(e.getModifiers()==e.BUTTON1_MASK)
                sortCol(e);
        }
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new ATComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWATTableData.this));
            m_table.repaint();
        }
    //--------------------------------------------------------------------------
        private void selectViewCol(MouseEvent e) {
            javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
            for (int i=0; i < m_table.ATColumnNames.length; i++){
                javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
                m_table.ATColumnNames[i],m_table.visibleCol[i]);
                TableColumn column = m_table.ColModel[i];
                subMenu.addActionListener(new ColumnKeeper(column,VWATTableData.m_columns[i]));
                menu.add(subMenu);
            }
            menu.show(m_table,e.getX(),e.getY());
        }
    //--------------------------------------------------------------------------
        class ColumnKeeper implements java.awt.event.ActionListener {
            protected TableColumn m_column;
            protected ATColumnData  m_colData;
            
            public ColumnKeeper(TableColumn column,ATColumnData colData){
                m_column = column;
                m_colData = colData;
            }
    //--------------------------------------------------------------------------
            public void actionPerformed(java.awt.event.ActionEvent e) {
                javax.swing.JCheckBoxMenuItem item=(javax.swing.JCheckBoxMenuItem)e.getSource();
                TableColumnModel model = m_table.getColumnModel();
                boolean found=false;
                int i=0;
                int count=m_table.ATColumnNames.length;
                int colCount=model.getColumnCount();
                while (i<count && !found){
                    if(m_table.ATColumnNames[i].equals(e.getActionCommand()))
                        found=true;
                    i++;
                }
                i--;
                if (item.isSelected()) {
                    m_table.visibleCol[i]=!m_table.visibleCol[i];
                    model.addColumn(m_column);
                }
                else {
                    if(colCount>1) {
                        m_table.visibleCol[i]=!m_table.visibleCol[i];
                        model.removeColumn(m_column);
                    }
                }
                m_table.tableChanged(new javax.swing.event.TableModelEvent(m_table.m_data));
                m_table.repaint();
            }
        }
    }
    //--------------------------------------------------------------------------
    class ATComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public ATComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof ATRowData) || !(o2 instanceof ATRowData))
                return 0;
            ATRowData s1 = (ATRowData)o1;
            ATRowData s2 = (ATRowData)o2;
            int result = 0;
            double d1, d2;
            switch (m_sortCol) {
                case COL_OBJECTNAME:
                    result = s1.m_ObjectName.toLowerCase().compareTo(s2.m_ObjectName.toLowerCase());
                    break;
                case COL_OBJECTTYPE:
                    result = s1.m_ObjectType.toLowerCase().compareTo(s2.m_ObjectType.toLowerCase());
                    break;
                case COL_ACTION:
                    result = s1.m_Action.toLowerCase().compareTo(s2.m_Action.toLowerCase());
                    break;
                case COL_CLIENT:
                    result = s1.m_CLIENT.toLowerCase().compareTo(s2.m_CLIENT.toLowerCase());
                    break;
                case COL_USERNAME:
                    result = s1.m_UserName.toLowerCase().compareTo(s2.m_UserName.toLowerCase());
                    break;
                case COL_DATETIME:
                    result = s1.m_DateTime.toLowerCase().compareTo(s2.m_DateTime.toLowerCase());
                    break;
                case COL_LOCATION:
                    result = s1.m_Location.toLowerCase().compareTo(s2.m_Location.toLowerCase());
                    break;
                case COL_DESCRIPTION:
                    result = s1.m_Description.toLowerCase().compareTo(s2.m_Description.toLowerCase());
                    break;
            }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof ATComparator) {
                ATComparator compObj = (ATComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    }
    //--------------------------------------------------------------------------
}

