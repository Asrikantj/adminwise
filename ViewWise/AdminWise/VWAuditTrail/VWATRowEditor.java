/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWArchiveRowEditor<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWAuditTrail;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWFind.VWFindConnector;

public class VWATRowEditor implements TableCellEditor {
  
  protected TableCellEditor editor, defaultEditor;
  protected VWATTable gTable=null;

  /**
   * Constructs a EachRowEditor.
   * create default editor 
   *
   * @see TableCellEditor
   * @see DefaultCellEditor
   */ 
  public VWATRowEditor(VWATTable table) {
    JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;
  }
  
  /**
   * @param row    table row
   * @param editor table cell editor
   */
  
  public Component getTableCellEditorComponent(JTable table,
      Object value, boolean isSelected, int row, int column) {
        
    editor=defaultEditor;     
    return editor.getTableCellEditorComponent(table,
             value, isSelected, row, column);
  }

  public Object getCellEditorValue() {
    return editor.getCellEditorValue();
  }
  public boolean stopCellEditing() {
    return editor.stopCellEditing();
  }
  public void cancelCellEditing() {
    editor.cancelCellEditing();
  }
  public boolean isCellEditable(EventObject anEvent) {
    return false;
  }
  public void addCellEditorListener(CellEditorListener l) {
    editor.addCellEditorListener(l);
  }
  public void removeCellEditorListener(CellEditorListener l) {
    editor.removeCellEditorListener(l);
  }
  public boolean shouldSelectCell(EventObject anEvent) {
    return editor.shouldSelectCell(anEvent);
  }
}
  