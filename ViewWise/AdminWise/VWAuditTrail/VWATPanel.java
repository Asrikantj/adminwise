package ViewWise.AdminWise.VWAuditTrail;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.computhink.vwc.VWCPreferences;

import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRecycle.VWRecycleConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.VWAddCustomIndices;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.LinkedList;
import ViewWise.AdminWise.VWStorage.VWDlgStorageLocation;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.io.File;
import java.util.Calendar;
import java.util.prefs.Preferences;

import ViewWise.AdminWise.VWUtil.VWPrint;

public class VWATPanel extends JPanel implements VWConstant
{
    public VWATPanel()
    {   
    }
    public void setupUI()
    {
    	int top = 116; int hGap = 28;
    	int height = 24, width = 144, left = 12, heightGap = 30;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWAT.setLayout(null);
        add(VWAT,BorderLayout.CENTER);
        VWAT.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWAT.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/
        
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnSetting.setText(AdminWise.connectorManager.getString("atPanel.BTN_SETTING_NAME"));
        BtnSetting.setActionCommand(BTN_GETDATA_NAME);
        PanelCommand.add(BtnSetting);
        //BtnSetting.setBackground(java.awt.Color.white);
        BtnSetting.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSetting.setIcon(VWImages.OptionsIcon);
        BtnGetData.setText(AdminWise.connectorManager.getString("atPanel.BTN_GETDATA_NAME"));
        BtnGetData.setActionCommand(BTN_GETDATA_NAME);
        PanelCommand.add(BtnGetData);
        top += hGap + 6;
        //BtnGetData.setBackground(java.awt.Color.white);
        BtnGetData.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnGetData.setIcon(VWImages.FindIcon);
        
        top += hGap+6;
        BtnAddIndex.setText(BTN_ADDINDEX_NAME);
        BtnAddIndex.setActionCommand(BTN_PURGEALL_NAME);
        PanelCommand.add(BtnAddIndex);
//      BtnPurgeAll.setBackground(java.awt.Color.white);
        BtnAddIndex.setBounds(12,top,AdminWise.gPanelBtnWidth, AdminWise.gBtnHeight);
        BtnAddIndex.setIcon(VWImages.CustomReportIcon);
        
        /**
         * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
         */
        BtnProgressClick.setText("");
		PanelCommand.add(BtnProgressClick);
		BtnProgressClick.setBounds(left, top, width, height);
		BtnProgressClick.setVisible(false);
		BtnProgressClick.addActionListener(new SymAction());
        
        //Commented for CV83B2 changes
       /* BtnClearList.setText(AdminWise.connectorManager.getString("atPanel.BTN_PURGEDATA_NAME"));
        BtnClearList.setActionCommand(BTN_PURGEDATA_NAME);
        PanelCommand.add(BtnClearList);
        top += hGap + 12;*/
        //BtnClearList.setBackground(java.awt.Color.white);
        /*BtnClearList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearList.setIcon(VWImages.DelIcon);
        BtnClearSelList.setText(AdminWise.connectorManager.getString("atPanel.BTN_CLEARSELECTED_NAME"));
        BtnClearSelList.setActionCommand(BTN_CLEARSELECTED_NAME);
        PanelCommand.add(BtnClearSelList);
        top += hGap;*/
        //BtnClearSelList.setBackground(java.awt.Color.white);
        BtnClearSelList.setBounds(12, top,144, AdminWise.gBtnHeight);
        BtnClearSelList.setIcon(VWImages.ClearIcon);
        BtnSaveAs.setText(AdminWise.connectorManager.getString("atPanel.BTN_SAVEASHTML_NAME"));
        BtnSaveAs.setActionCommand(BTN_SAVEASHTML_NAME);
        PanelCommand.add(BtnSaveAs);
        top += hGap+6;
        //BtnSaveAs.setBackground(java.awt.Color.white);
        BtnSaveAs.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSaveAs.setIcon(VWImages.SaveAsIcon);
        BtnPrint.setText(AdminWise.connectorManager.getString("atPanel.BTN_PRINT_NAME"));
        BtnPrint.setActionCommand(BTN_PRINT_NAME);
        PanelCommand.add(BtnPrint);
        top += hGap+6;
        //BtnPrint.setBackground(java.awt.Color.white);
        BtnPrint.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnPrint.setIcon(VWImages.PrintIcon);

      /*
       * Desc    :Added new UI buttons for Navigation
       * 		  User can set the record count and retrieve records based on the max count entered in Settings dialog.
       * Author  :Nishad Nambiar
       * Date    :26-Jun-2007 
       */  
        
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setLayout(null);
        pnlNavigation.setLayout(null);
        PanelCommand.add(pnlNavigation);
        pnlNavigation.setBackground(AdminWise.getAWColor());
        top += hGap + 12;
        pnlNavigation.setBounds(12,top,144,32);

        btnFirst.setIcon(VWImages.MoveFirstIcon);
        btnFirst.setToolTipText(AdminWise.connectorManager.getString("atPanel.first"));
        btnFirst.setBounds(0,0,30,22);
        pnlNavigation.add(btnFirst);

        btnPrevious.setIcon(VWImages.MovePreviousIcon);
        btnPrevious.setToolTipText(AdminWise.connectorManager.getString("atPanel.previous"));
        btnPrevious.setBounds(35,0,30,22);
        pnlNavigation.add(btnPrevious);

        btnNext.setIcon(VWImages.MoveNextIcon);
        btnNext.setToolTipText(AdminWise.connectorManager.getString("atPanel.next"));
        btnNext.setBounds(81,0,30, 22);
        pnlNavigation.add(btnNext);
        
        btnLast.setIcon(VWImages.MoveLastIcon);
        btnLast.setToolTipText(AdminWise.connectorManager.getString("atPanel.last"));
        btnLast.setBounds(115,0,30, 22);
        pnlNavigation.add(btnLast);
        
        btnFirst.setEnabled(false);
        btnLast.setEnabled(false);
        btnNext.setEnabled(false);
        btnPrevious.setEnabled(false);
        
        BtnGetMoreData.setText(AdminWise.connectorManager.getString("atPanel.BTN_MOREDATA_NAME"));
        BtnGetMoreData.setActionCommand(BTN_MOREDATA_NAME);
        PanelCommand.add(BtnGetMoreData);
        //BtnGetMoreData.setBackground(java.awt.Color.white);
        top += hGap;
        BtnGetMoreData.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnGetMoreData.setIcon(VWImages.AddIcon);
        BtnGetMoreData.setVisible(false);

        PanelCommand.add(progress);
        top += hGap;
        progress.setBounds(12,top,144, AdminWise.gBtnHeight);
        progress.setMinimum(0);
        progress.setVisible(false);
        
        PanelTable.setLayout(new BorderLayout());
        VWAT.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWAT.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.ATImage);
        SymAction lSymAction = new SymAction();
        BtnGetData.addActionListener(lSymAction);
        BtnAddIndex.addActionListener(lSymAction);
        BtnClearList.addActionListener(lSymAction);
        BtnClearSelList.addActionListener(lSymAction);
        BtnSaveAs.addActionListener(lSymAction);
        BtnSetting.addActionListener(lSymAction);
        BtnPrint.addActionListener(lSymAction);
        BtnGetMoreData.addActionListener(lSymAction);
        btnNext.addActionListener(lSymAction);
        btnPrevious.addActionListener(lSymAction);
        btnFirst.addActionListener(lSymAction);
        btnLast.addActionListener(lSymAction);
        BtnProgressClick.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
//--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnGetData)
                BtnGetData_actionPerformed(event);
            else if (object == BtnClearList)
                BtnClearList_actionPerformed(event);
            else if (object == BtnClearSelList)
                BtnClearSelList_actionPerformed(event);	
            else if (object == BtnSaveAs)
                BtnSaveAs_actionPerformed(event);
            else if (object == BtnSetting)
                BtnSetting_actionPerformed(event);
            else if (object == BtnPrint)
                BtnPrint_actionPerformed(event);
           // else if (object == BtnGetMoreData)
                //BtnGetMoreData_actionPerformed(event);
            else if (object == btnNext)
                BtnGetNextData_actionPerformed(event);
            else if (object == btnPrevious)
                BtnGetPreviousData_actionPerformed(event);
            else if (object == btnFirst)
                BtnGetFirstData_actionPerformed(event);
            else if (object == btnLast)
                BtnGetLastData_actionPerformed(event);
            else if (object == BtnAddIndex)
                BtnAddIndex_actionPerformed(event);
            else if(object==BtnProgressClick){
				BtnProgressClick_actionPerformed(event);
			}
        }
    }
//--------------------------------------------------------------------------
    VWPicturePanel picPanel = new VWPicturePanel(TAB_AUDITTRAIL_NAME, VWImages.ATImage);
    javax.swing.JPanel VWAT = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnGetData = new VWLinkedButton(1);
    /**@author apurba.m
     * BtnAddIndex variable added for creating button for adding custom indices in AT panel
     */
    VWLinkedButton BtnAddIndex = new VWLinkedButton(1);
    VWLinkedButton BtnClearList = new VWLinkedButton(1);
    VWLinkedButton BtnClearSelList = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    VWLinkedButton BtnSetting = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    VWLinkedButton BtnGetMoreData = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWATTable Table = new VWATTable();
    VWDlgATDataFilter dlgATDataFilter=null;
    javax.swing.JProgressBar progress = new javax.swing.JProgressBar();
    JPanel pnlNavigation = new JPanel();
    VWLinkedButton btnNext = new VWLinkedButton(0);
    VWLinkedButton btnPrevious = new VWLinkedButton(0);
    VWLinkedButton btnFirst = new VWLinkedButton(0);
    VWLinkedButton btnLast = new VWLinkedButton(0);
    
    /**
     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
     */
    JButton BtnProgressClick=new  JButton();
    Lock lock1 = new ReentrantLock();
    String s1=new String(new char[] { 32 });
	String s2=new String(new char[] { 9 });
	JLabel statusLabel=new JLabel("		"+ s1+ s1+AdminWise.connectorManager.getString("progressDialog.statusForAT"));
	JDialog processDialog = new JDialog();
    
//--------------------------------------------------------------------------
class SymComponent extends java.awt.event.ComponentAdapter
{
    public void componentResized(java.awt.event.ComponentEvent event)
    {
        Object object = event.getSource();
        if (object == VWAT)
            VWAT_componentResized(event);
    }
}
//--------------------------------------------------------------------------
void VWAT_componentResized(java.awt.event.ComponentEvent event)
{
    if(event.getSource()==VWAT)
    {
        Dimension mainDimension=this.getSize();
        Dimension panelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        panelDimension.width=mainDimension.width-commandDimension.width;
        PanelTable.setSize(panelDimension);
        PanelCommand.setSize(commandDimension);
        PanelTable.doLayout();
        AdminWise.adminPanel.MainTab.repaint();
    }
}
//--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom)
    {
        if(newRoom.getConnectStatus()!=Room_Status_Connect)
        {
            unloadTabData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        unloadTabData();
        setEnableMode(MODE_UNSELECTED);
    }
//--------------------------------------------------------------------------
    public void unloadTabData()
    {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    
    /**
     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
     */
    void BtnProgressClick_actionPerformed(ActionEvent event){
		try{
			lock1.lock();
			statusLabel.paintImmediately(this.getBounds());
			statusLabel.setVisible(true);
			statusLabel.setLayout(new BorderLayout());
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
				fontName = VWCPreferences.getFontName();
			statusLabel.setFont(new Font(fontName, Font.PLAIN, 14));
			statusLabel.validate();
			/*if(scroll.isVisible()==true)
			processDialog.setModal(true);
			else 
				processDialog.setModal(false);*/
			processDialog.getContentPane().add(statusLabel, BorderLayout.CENTER);
			processDialog.setLocationRelativeTo(null);
			processDialog.setTitle("Custom Indices");
			processDialog.setAlwaysOnTop(true);
			processDialog.setSize(390,70);
			processDialog.setResizable(false);
			processDialog.setDefaultCloseOperation(processDialog.DO_NOTHING_ON_CLOSE);
			processDialog.setVisible(true);
			processDialog.setOpacity(1);
			//processDialog.pack();
			lock1.unlock();
			//processDialog.repaint();			

		}catch(Exception e){
			AdminWise.printToConsole("Exception while reviewUpdate action::::"+e.getMessage());
		}
	}
//--------------------------------------------------------------------------
    void BtnGetData_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(dlgATDataFilter != null)
        {
        	dlgATDataFilter.loadDlgData();
        	dlgATDataFilter.setVisible(true);
            
        }
        else
        {
            dlgATDataFilter = new VWDlgATDataFilter(AdminWise.adminFrame);
            dlgATDataFilter.setVisible(true);
        }
        if(!dlgATDataFilter.getCancelFlag())
        {
            Filter=dlgATDataFilter.getValues();
            ///for(int i=0;i<Filter.size();i++)
                ////System.out.println((String)Filter.get(i));
            if(!dlgATDataFilter.getViewFlag())
            {
                Table.clearData();
                progress.setMinimum(0);
                progress.setMaximum(100);
                progress.setValue(0);
                progress.setString("0");
                
                //Stop calling the Thread, and call the function getMoreData() individually.
                //new getDataThread();
                
                lastRecID=0;
                getMoreData();
                
                vecSelectedRecId.removeAllElements();
                sortedSelectedRecId.clear();
                iVecSelectedRecIdCounter = 0;
                
                iVecSelectedRecIdCounter++;
                vecSelectedRecId.add(lastRecID);
                
                sortedSelectedRecId.add(lastRecID);
                
                btnFirst.setEnabled(true);
                btnLast.setEnabled(true);
                btnNext.setEnabled(true);
                btnPrevious.setEnabled(true);
                
                /*
                BtnGetMoreData.setVisible(false);
                Vector data=VWATConnector.getData(Filter,0);
                try
                {
                    Table.addData(data);
                }
                catch(java.lang.OutOfMemoryError e)
                {
                    System.out.println(e.getMessage());
                }
                if(VWUtil.to_Number((String)Filter.get(Filter.size()-1))==data.size())
                {
                    VWStringTokenizer tokens=new VWStringTokenizer((String)data.get(data.size()-1),VWConstant.SepChar,false);
                    lastRecID=VWUtil.to_Number(tokens.nextToken());
                    BtnGetMoreData.setVisible(true);
                }
                 */
                setEnableMode(MODE_UNSELECTED);
                //Desc   :Select 'All Objects' by default
                //Author :Nishad Nambiar
                //Date   :13-Mar-2007
                dlgATDataFilter.CmbObject.setSelectedIndex(0);
            }
            else
            {
                String archivePath=AdminWise.adminPanel.getATOptions()[0][1];
                File archivePathFile=new File(archivePath);
                if(archivePath==null || archivePath.equals("") || !archivePathFile.exists())
                {
                    VWMessage.showNoticeDialog((java.awt.Component) this,
                        VWMessage.ERR_ARCHIVE_NOTFOUND);
                    return;
                }
                if(!archivePathFile.canWrite())
                {
                    VWMessage.showNoticeDialog((java.awt.Component) this,
                        VWMessage.ERR_ARCHIVE_NOTWRITABLE);
                    return;
                }
                AdminWise.adminPanel.setWaitPointer();
                List ATData=VWATConnector.getData(Filter,0);
                AdminWise.printToConsole("ATData Size on GETData Button ::::"+ATData.size());
                Vector recIds=new Vector();
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    VWMessage.MSG_ATDEL_1+ATData.size()+VWMessage.MSG_ATDEL_2
                    )!=javax.swing.JOptionPane.YES_OPTION)
                {
                    AdminWise.adminPanel.setDefaultPointer();
                    return;
                }
                try{
                    int count=ATData.size();
                    String[][] repData=new String[count][8];
                    for(int i=0;i<count;i++)
                    {
                        VWStringTokenizer tokens=new VWStringTokenizer((String)ATData.get(i),VWConstant.SepChar,false);
                        recIds.add(tokens.nextToken());
                        tokens.nextToken();
                        repData[i][1]=VWConstant.ATStrObjects[VWUtil.to_Number(tokens.nextToken())];
                        repData[i][0]=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown name>");
                        repData[i][4]=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown user>");
                        repData[i][5]=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown client>");
                        repData[i][3]=VWUtil.getFixedValue(tokens.nextToken(),"<Unknown date>");
                        repData[i][2]=VWConstant.ATStrActions[VWUtil.to_Number(tokens.nextToken())];
                        repData[i][6]=VWUtil.getFixedValue(tokens.nextToken(),"");
                        ///m_Location=VWUtil.fixNodePath(VWUtil.getFixedValue(tokens.nextToken(),""));
                        repData[i][7]=VWUtil.getFixedValue(tokens.nextToken(),"");
                    }
                    archivePath=archivePath.endsWith(File.separator)?archivePath:archivePath+File.separator;
                    Calendar rightNow = Calendar.getInstance();
                    archivePath=archivePath+rightNow.get(Calendar.YEAR) +
                    (rightNow.get(Calendar.MONTH)+1)+
                    rightNow.get(Calendar.DAY_OF_MONTH)+
                    rightNow.get(Calendar.HOUR_OF_DAY)+
                    rightNow.get(Calendar.MINUTE)+
                    rightNow.get(Calendar.SECOND)+".html";
                    if(VWSaveAsHtml.saveArrayInFile(repData,ATColumnNames,ATColumnWidths,
                    archivePath,(java.awt.Component)this,LBL_AT_NAME))
                    {
                        VWATConnector.delData(recIds,archivePath);
                        VWMessage.showInfoDialog(VWMessage.MSG_ATARCHIVE_SUCCESS);
                    }
                    else
                    {
                        VWMessage.showInfoDialog(VWMessage.MSG_ATARCHIVE_FAILED);
                    }
                }
                catch(Exception e){
                    VWMessage.showInfoDialog(VWMessage.MSG_ATARCHIVE_FAILED);
                }
                finally{AdminWise.adminPanel.setDefaultPointer();}                
            }
        }
        else{
        	dlgATDataFilter.CmbObject.setSelectedIndex(0);
        }
    }
    //-----------------------------------------------------------
    void BtnGetMoreData_actionPerformed(java.awt.event.ActionEvent event)
    {
       if(BtnGetMoreData.getText().equals(BTN_STOP_NAME))
       {
           stopGetData=1;
       }
       else
       {
           stopGetData=0;
           new getDataThread();
           /*
            AdminWise.adminPanel.setDefaultPointer();
            try
            {
                if(getMoreData())
                {
                    setEnableMode(MODE_UNSELECTED);
                    BtnGetMoreData.setVisible(true);
                }
                else
                {
                    BtnGetMoreData.setVisible(false);
                }
            }
            catch(Exception e){}
            finally{AdminWise.adminPanel.setDefaultPointer();}
            */
       }
    }

    /*
     * Desc    :Function for Enable/Disable Navigation Buttons.
     * Author  :Nishad Nambiar
     * Date    :26-Jun-2007 
     */  

    void enableNavigation(){
    	if(lastRecID > chunkSize){
    		btnLast.setEnabled(true);
    	}
    	else{
    		btnLast.setEnabled(false);
    	}
    }
    
    /*
     * Desc    :Functions for Navigation Buttons.
     * Author  :Nishad Nambiar
     * Date    :26-Jun-2007 
     */  
    void BtnGetNextData_actionPerformed(java.awt.event.ActionEvent event)
    {
    	//Used a vector and sortedset to keep lastRecID, so that previous button can work properly.
    	//When next button is clicked a variable is incremented and when previous is clicked same variable is decresed.
    	try{
	    	int ret = getMoreData();
	    	sortedSelectedRecId.add(lastRecID); 
	    	Iterator iterator = sortedSelectedRecId.iterator();
	    	vecSelectedRecId.clear();
	    	int count = 0;
	    	String strElements[] = new String[sortedSelectedRecId.size()];
	    	while(iterator.hasNext()){
	    		strElements[count] = String.valueOf(iterator.next());
	    		count++;
	    	}
	    	iVecSelectedRecIdCounter=0;
	    	if(strElements.length>0){
	    		for(int x=(strElements.length-1);x>0;x--){
	    			vecSelectedRecId.add(strElements[x]);
	    			iVecSelectedRecIdCounter++;
	    		}
	    	}
	    	if(ret == 0){
	    		btnNext.setEnabled(false);	
	    		btnLast.setEnabled(false);
	    	}
	    	else{
	    		btnNext.setEnabled(true);
	    		btnLast.setEnabled(true);
	 	    	btnPrevious.setEnabled(true);
	    	    btnFirst.setEnabled(true);
	
	    	}
    	}catch(Exception e){
    		AdminWise.printToConsole(e.toString());
    	}
    }
    
    void BtnGetPreviousData_actionPerformed(java.awt.event.ActionEvent event)
    {
    	try{
	    	if( (vecSelectedRecId.size()>0) && (iVecSelectedRecIdCounter>0) ){
		    	iVecSelectedRecIdCounter--;
		    	lastRecID = Integer.parseInt(String.valueOf(vecSelectedRecId.get(iVecSelectedRecIdCounter)));
		    	btnNext.setEnabled(true);
		    	btnLast.setEnabled(true);
	    	}
	    	else if(iVecSelectedRecIdCounter==0){
	    		lastRecID=0;
	    		btnPrevious.setEnabled(false);
	    		btnFirst.setEnabled(false);
	    	//	getMoreData();
	    	}
	    	else{
	    		btnPrevious.setEnabled(false);
	    		btnFirst.setEnabled(false);
	    	}
	    	int ret = getMoreData();
    	}catch(Exception e){
    		AdminWise.printToConsole(e.toString());
    	}
    }
    
    void BtnGetFirstData_actionPerformed(java.awt.event.ActionEvent event)
    {
    	lastRecID = 0;
        int ret = getMoreData();
        	btnFirst.setEnabled(false);
        	btnPrevious.setEnabled(false);
        	btnLast.setEnabled(true);
        	btnNext.setEnabled(true);
    }
    
    void BtnGetLastData_actionPerformed(java.awt.event.ActionEvent event)
    {
    	try{
	    	lastRecID = Integer.parseInt(String.valueOf(vecSelectedRecId.get((vecSelectedRecId.size()-1))));
	    	int ret = getMoreData();
	    		btnLast.setEnabled(false);
	    		btnNext.setEnabled(false);
	    		btnFirst.setEnabled(true);
	    		btnPrevious.setEnabled(true);
    	}catch(Exception e){
    		AdminWise.printToConsole("LAST RECORD :"+e.toString());
    	}
    }
    
    /**@author apurba.m
     * For getting custom index dialogue box in AT Panel
     * @param event
     */
    void BtnAddIndex_actionPerformed(java.awt.event.ActionEvent event) {
    	lock1.lock();
		BtnProgressClick.doClick();
    	Vector newCustomIndexList = new Vector();
    	Vector finalCustomList = new Vector();
    	AdminWise.adminPanel.setWaitPointer();
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(AdminWise.adminPanel.confirmDelRecycleDoc)
        try{
        	Vector settingsValue= new Vector();
        	Vector customIndexList= AdminWise.gConnector.ViewWiseClient.getCustomList(room.getId(), 3, settingsValue);
        	if (customIndexList.size() > 0) {
				String str= "";
				str= str+customIndexList.get(0).toString();
				String[] st1= str.split("\t");
				for(int i=1; i<st1.length;i++){
					newCustomIndexList.add(st1[i]);
				}
				finalCustomList = newCustomIndexList;
			} else {
				finalCustomList= null;
			}
        	Vector data=VWATConnector.getData(Filter,0);
//        	processDialog.setVisible(false);
//			processDialog.dispose();
        	VWAddCustomIndices dynamicUserIndexDlg = new VWAddCustomIndices(AdminWise.adminFrame, "", "AT",data,Filter,lastRecID,Table,finalCustomList,processDialog);
        }
        catch(Exception e){}        
        finally{
            AdminWise.adminPanel.setDefaultPointer();            
            hideProgress();
            //totalRecordCount = VWATConnector.getA;
        }
    }

    private void hideProgress() {
        progress.setVisible(false);
    }
    
    //-----------------------------------------------------------
    /*
    public int getMoreData()
    {
        Vector data=VWATConnector.getData(Filter,lastRecID);
        if(data==null || data.size()==0) return 0;
        try
        {
            VWStringTokenizer tokens=new VWStringTokenizer((String)data.get(data.size()-1),
                VWConstant.SepChar,false);
            lastRecID=VWUtil.to_Number(tokens.nextToken());
            ///System.out.println("lastRecID ->"+lastRecID);
            
            Table.insertData(data);
            if(VWUtil.to_Number((String)Filter.get(Filter.size()-1))>data.size())
                return 0;
            return data.size();
        }
        catch(Exception e){return 0;}
    }
    */

    public int getMoreData()
    {
        Vector data=VWATConnector.getData(Filter,lastRecID);
        if(data==null || data.size()==0) return 0;
        try
        {
        	//Find the max record id.
        	if(lastRecID == 0){
        		VWStringTokenizer tokens=new VWStringTokenizer((String)data.get(data.size()-1),VWConstant.SepChar,false);
        		maxRecID = VWUtil.to_Number(tokens.nextToken());
        	}
            VWStringTokenizer tokens=new VWStringTokenizer((String)data.get(data.size()-1),
                VWConstant.SepChar,false);
            lastRecID=VWUtil.to_Number(tokens.nextToken());
            Table.clearData();
            Table.insertData(data);
            if(VWUtil.to_Number((String)Filter.get(Filter.size()-1))>data.size())
                return 0;
            return data.size();
        }
        catch(Exception e){return 0;}
    }
    
    //--------------------------------------------------------------------------
    void BtnSetting_actionPerformed(java.awt.event.ActionEvent event)
    {
    	/**
    	 * If and else part comment in CV83B3 for RoomName loading issue in Audit trail dialog
    	 * Modified date:-22/9/2015
    	 * Modified by Madhavan
    	 */
    	
    	//if(settingDlg==null)
    		settingDlg = new VWDlgATSetting(AdminWise.adminFrame);
    	/*else
    		settingDlg.setVisible(true);*/
    }
    //--------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event)
    {
        new VWPrint(Table);
    }
    //--------------------------------------------------------------------------
    void BtnClearList_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(AdminWise.adminPanel.confirmATDelFromDB)
        if(VWMessage.showConfirmDialog((java.awt.Component) this,
            VWMessage.MSG_ATDEL_1+Table.getRowCount()+VWMessage.MSG_ATDEL_3)!=javax.swing.JOptionPane.YES_OPTION) return;
        Vector recIds=Table.getIds();
        VWATConnector.delData(recIds,null);
        clearTable();
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    void BtnClearSelList_actionPerformed(java.awt.event.ActionEvent event)
    {
        Table.deleteSelectedRows();
    }
    //--------------------------------------------------------------------------
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            AdminWise.adminPanel.setWaitPointer();
            boolean  multipleFiles = true;
            	VWSaveAsHtml.saveArrayInFile(Table.getReportData(),ATColumnNames,ATColumnWidths,
            null,(java.awt.Component)this,LBL_AT_NAME,  multipleFiles);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }    
    //--------------------------------------------------------------------------
    public int getRowCount()
    {
        return Table.getRowCount();
    }
    //--------------------------------------------------------------------------
    public void addData(List data)
    {
        Table.addData(data);
    }
    //--------------------------------------------------------------------------
    public void clearTable()
    {
        Table.clearData();
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode)
    {
        BtnGetData.setVisible(true);
        BtnAddIndex.setVisible(true);
        BtnClearList.setVisible(true);
        BtnClearSelList.setVisible(true);
        BtnSaveAs.setVisible(true);
        BtnPrint.setVisible(true);
        BtnGetData.setEnabled(true);
        BtnAddIndex.setEnabled(true);
        BtnClearList.setEnabled(true);
        BtnClearSelList.setEnabled(true);
        BtnSaveAs.setEnabled(true);
        BtnPrint.setEnabled(true);
        switch (mode)
        {
            case MODE_UNSELECTED:
                BtnSetting.setEnabled(true);
                BtnGetData.setEnabled(true);
                BtnClearList.setEnabled(true);
                BtnClearSelList.setEnabled(false);
                if(getRowCount()==0)    
                {
                    BtnClearList.setEnabled(false);
                    BtnSaveAs.setEnabled(false);         
                    BtnPrint.setEnabled(false);
                    BtnAddIndex.setEnabled(false);
                }
                break;
            case MODE_SELECT:
                BtnSetting.setEnabled(true);
                BtnGetData.setEnabled(true);
                BtnClearSelList.setEnabled(true);
                if(getRowCount()==0)    
                {
                    BtnClearList.setEnabled(false);
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                    BtnAddIndex.setEnabled(false);
                }
                break;
            case MODE_UNCONNECT:
                BtnSetting.setEnabled(false);
                BtnGetData.setEnabled(false);
                BtnAddIndex.setEnabled(false);
                BtnClearList.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                BtnGetMoreData.setVisible(false);
                lastRecID=0;
                stopGetData=0;
                gCurRoom=0;
                break;
        }
    }
    //--------------------------------------------------------------------------
    private class getDataThread extends Thread
    {
        getDataThread()
        {
            progress.setVisible(true);
            progress.setStringPainted(true);
            BtnGetMoreData.setText(AdminWise.connectorManager.getString("atPanel.BTN_STOP_NAME"));
            BtnGetMoreData.setVisible(true);
            setDaemon(true);
            setPriority(java.lang.Thread.MIN_PRIORITY);
            start();
        }
        public void run()
        {
            try
            {
                int retCount=0;
                ///System.out.println("stopGetData ->"+stopGetData);
                while((retCount=getMoreData())>0 && stopGetData==0)
                {
                    try
                    {
                        progress.setValue(progress.getValue()+1);
                        progress.setString(String.valueOf(
                            VWUtil.to_Number(progress.getString())+retCount));
                    }
                    catch(Exception e){System.out.println(e.getMessage());}
                }
            }
            catch(Exception e){}
            finally{
                progress.setVisible(false);
                //progress.setValue(0);
                //progress.setString("0");
                if(stopGetData>0)
                {
                    BtnGetMoreData.setVisible(true);
                    BtnGetMoreData.setText(AdminWise.connectorManager.getString("atPanel.BTN_MOREDATA_NAME"));
                }
                else
                {
                    BtnGetMoreData.setVisible(false);
                }
                stopGetData=0;
                lastRecID=0;
            }
            /*
            int curResultCount=0;
            try
            {
                 if(find(roomId,srch,append)>0)
                 {
                    srch.setRowNo(chunkSize);
                    if(!searchCancel)
                    {
                        curResultCount=getFindResult(roomId,srch,append);
                        resultCount+=curResultCount;
                    }
                    while(!searchCancel && curResultCount>0 && resultCount<serverResultCount)
                    {
                        resultCount+=getFindResult(roomId,srch,true);
                    }
                 }
            }
            catch(Exception e){}
            finally{
                ViewWise.AdminWise.VWFind.VWFindPanel.setFindMode(true);
                searchCancel=false;
                resultCount=0;
            }
             */
        }
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    VWDlgATSetting settingDlg = null;
    private boolean UILoaded=false;
    private Vector Filter=new Vector();
    private int lastRecID=0;
    private int maxRecID=0;
    public static int chunkSize=VWDlgATDataFilter.iChunkSize;
    
    public int iVecSelectedRecIdCounter =0;
    private int totalRecordCount = 0;
    
    public Vector vecSelectedRecId = new Vector();
    public TreeSet sortedSelectedRecId = new TreeSet();
    
    private int stopGetData=0;
}