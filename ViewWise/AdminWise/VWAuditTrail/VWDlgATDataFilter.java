/*
                A basic implementation of the JDialog class.
 */

package ViewWise.AdminWise.VWAuditTrail;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWRecord;
import javax.swing.border.TitledBorder;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWFind.VWFindConnector;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMask.VWMaskField;
import java.text.DateFormat;
import java.util.Date;
import java.io.File;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;

public class VWDlgATDataFilter extends javax.swing.JDialog implements VWConstant{
    public VWDlgATDataFilter(Frame parent) {
        super(parent);
        getContentPane().setBackground(AdminWise.getAWColor());
        ///parent.setIconImage(VWImages.FilterIcon.getImage());
        setTitle(LBL_ATSETTING_NAME);
        setModal(true);
        getContentPane().setLayout(null);
        String languageLocale=AdminWise.getLocaleLanguage();
        if(languageLocale.equals("nl_NL")){
        	setSize(320+20,/*312*/354);
        }else
        	setSize(320,/*312*/354);
        setVisible(false);
        JPanel1.setLayout(null);
        lblObjectEvent.setForeground(new Color(7, 38, 58));
        lblUserName.setForeground(new Color(7, 38, 58));
        lblDate.setForeground(new Color(7, 38, 58));
        getContentPane().add(lblObjectEvent);
        getContentPane().add(lineObjectEvent);
        lblObjectEvent.setBounds(8,4,120,24);
        lineObjectEvent.setBorder(BorderFactory.createLineBorder(Color.gray));	 
        if(languageLocale.equals("nl_NL")){
        lineObjectEvent.setBounds(84+20,16,224,1);
        }else
        lineObjectEvent.setBounds(84,16,224,1);
        getContentPane().add(JPanel1);
        //JPanel1.setBorder(titledBorder1);
        if(languageLocale.equals("nl_NL")){
        JPanel1.setBounds(4,4,314+20,84);
        }else
          JPanel1.setBounds(4,4,314,84);
        JLabel1.setText(LBL_OBJECT_NAME);
        JPanel1.add(JLabel1);
        JLabel1.setBounds(10,22,80, AdminWise.gBtnHeight);
        JPanel1.add(CmbObject);
        if(languageLocale.equals("nl_NL")){
        	CmbObject.setBounds(80+20,24,224,20);
        }else
        	CmbObject.setBounds(80,24,224,20);
        JLabel2.setText(LBL_EVENT_NAME);
        JPanel1.add(JLabel2);
        JLabel2.setBounds(10,54,80, AdminWise.gBtnHeight);
        JPanel1.add(CmbEvent);
        if(languageLocale.equals("nl_NL")){
        	CmbEvent.setBounds(80+20,56,224,20);
        }else
        	CmbEvent.setBounds(80,56,224,20);
        JPanel2.setLayout(null);
        
        getContentPane().add(lblUserName);
        getContentPane().add(lineUserName);
        if(languageLocale.equals("nl_NL")){
        	lblUserName.setBounds(8,92,120+15,24);
        }else
        	lblUserName.setBounds(8,92,120,24);
        lineUserName.setBorder(BorderFactory.createLineBorder(Color.gray));	    
        if(languageLocale.equals("nl_NL")){
        lineUserName.setBounds(84+20,104,224,1);
        }else
        	 lineUserName.setBounds(84,104,224,1);	
        
        getContentPane().add(JPanel2);
        //JPanel2.setBorder(titledBorder2);
        if(languageLocale.equals("nl_NL")){
        JPanel2.setBounds(4,92,314+20,84);
        }else
        JPanel2.setBounds(4,92,314,84);
        JLabel3.setText(LBL_USERNAME_NAME);
        JPanel2.add(JLabel3);
        if(languageLocale.equals("nl_NL")){
        JLabel3.setBounds(10,20,80+20,24);
        }else
        JLabel3.setBounds(10,20,80,24);
        JPanel2.add(CmbUserName);
        if(languageLocale.equals("nl_NL")){
        	CmbUserName.setBounds(80+20,24,224,20);
        }else
        	CmbUserName.setBounds(80,24,224,20);

        JLabel4.setText(LBL_CLIENTIP_NAME);
        JPanel2.add(JLabel4);
        JLabel4.setBounds(10,54,80, AdminWise.gBtnHeight);
        JPanel2.add(TxtClientIP);
        if(languageLocale.equals("nl_NL")){
        	TxtClientIP.setBounds(80+20,56,224,20);
        }else
        	TxtClientIP.setBounds(80,56,224,20);
        
        JPanel3.setLayout(null);
        getContentPane().add(lblDate);
        getContentPane().add(lineDate);
        getContentPane().add(JPanel3);
        
        lblDate.setBounds(8,180,120,24);
        lineDate.setBorder(BorderFactory.createLineBorder(Color.gray));	    
        if(languageLocale.equals("nl_NL")){
        lineDate.setBounds(84+20,192,224,1);
        }else 
        	  lineDate.setBounds(84,192,224,1);	
        //JPanel3.setBorder(titledBorder3);
        if(languageLocale.equals("nl_NL")){
        	JPanel3.setBounds(4,180,314+20,84);
        }else
        	JPanel3.setBounds(4,180,314,84);
        JLabel5.setText(LBL_DATEFROM_NAME);
        JPanel3.add(JLabel5);
        JLabel5.setBounds(10,20,80,24);
        JPanel3.add(DateTime1);
        if(languageLocale.equals("nl_NL")){
        	DateTime1.setBounds(80+20,24,224,20);
        }else 
        	DateTime1.setBounds(80,24,224,20);
        DateTime1.setDateFormat("MM/dd/yyyy");
        ///DateTime1.setDateFormat("yyyy-MM-dd");
        //DateTime1.setEditable(true);
        /*
        DateTime1.setDateFormat("yyyy-MM-dd hh:mm:ss");
        DateTime1.setEditable(true);
        */        
        JLabel6.setText(LBL_DATETO_NAME);
        JPanel3.add(JLabel6);
        JLabel6.setBounds(10,54,80, AdminWise.gBtnHeight);
        JPanel3.add(DateTime2);
        if(languageLocale.equals("nl_NL")){
        	DateTime2.setBounds(80+20,56,224,20);
        }else
        	DateTime2.setBounds(80,56,224,20);
        //DateTime2.setDateFormat("yyyy-MM-dd");
        DateTime2.setDateFormat("MM/dd/yyyy");
        //DateTime2.setEditable(true);
        /*
        DateTime2.setDateFormat("yyyy-MM-dd hh:mm:ss");
        DateTime2.setEditable(true);
         */
        JPanel4.setLayout(null);
        getContentPane().add(JPanel4);
        //JPanel4.setBorder(titledBorder4);
        if(languageLocale.equals("nl_NL")){
        JPanel4.setBounds(4,248,314+20,62);
    	}else{
    	JPanel4.setBounds(4,248,314,62);
    	}
        JLabel7.setText(LBL_CHUNKSIZE_NAME);
        JPanel4.add(JLabel7);
        
        if(languageLocale.equals("nl_NL"))
        	JLabel7.setBounds(90+20,22,180, AdminWise.gBtnHeight);
        else
        	JLabel7.setBounds(120,22,180, AdminWise.gBtnHeight);
        JPanel4.add(txtChunkSize);
        if(languageLocale.equals("nl_NL"))
        txtChunkSize.setBounds(260+20,24,44,20);
        else
        txtChunkSize.setBounds(260,24,44,20);
        txtChunkSize.setColumns(3);
        BtnView.setText(BTN_VIEW_NAME);
        BtnView.setActionCommand(BTN_VIEW_NAME);
        getContentPane().add(BtnView);
        if(languageLocale.equals("nl_NL")){
        	 BtnView.setBounds(160-20,320,72+20, AdminWise.gBtnHeight);
        }
        else
        BtnView.setBounds(160,320,72, AdminWise.gBtnHeight);
        //BtnView.setBackground(java.awt.Color.white);
        BtnView.setIcon(VWImages.FindIcon);
        BtnCancel.setText(BTN_CLOSE_NAME);
        BtnCancel.setActionCommand(BTN_CLOSE_NAME);
        getContentPane().add(BtnCancel);
        if(languageLocale.equals("nl_NL")){
        	 BtnCancel.setBounds(240+2,320,72+15, AdminWise.gBtnHeight);
        }else
        BtnCancel.setBounds(240,320,72, AdminWise.gBtnHeight);
        //BtnCancel.setBackground(java.awt.Color.white);
        BtnCancel.setIcon(VWImages.CloseIcon);
        BtnArchive.setText(BTN_ARCHIVE_NAME);
        BtnArchive.setActionCommand(BTN_ARCHIVE_NAME);
        getContentPane().add(BtnArchive);
        if(languageLocale.equals("nl_NL")){
        BtnArchive.setBounds(6,320,82+10, AdminWise.gBtnHeight);
        }else
         BtnArchive.setBounds(6,320,82, AdminWise.gBtnHeight);
        //BtnArchive.setBackground(java.awt.Color.white);
        BtnArchive.setIcon(VWImages.SaveIcon);
        loadDlgData();
        SymAction lSymAction = new SymAction();
        BtnView.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        BtnArchive.addActionListener(lSymAction);
        CmbObject.addActionListener(lSymAction);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        CmbObject.addKeyListener(aSymKey);
        CmbEvent.addKeyListener(aSymKey);
        CmbUserName.addKeyListener(aSymKey);
        TxtClientIP.addKeyListener(aSymKey);
        DateTime1.addKeyListener(aSymKey);
        DateTime2.addKeyListener(aSymKey);
        SymWindow aSymWindow = new SymWindow();
        this.addWindowListener(aSymWindow);
        setResizable(false);
        getDlgOptions();
        CmbObject.setSelectedIndex(0);
        
        JPanel1.setBackground(AdminWise.getAWColor());
        JPanel2.setBackground(AdminWise.getAWColor());
        JPanel3.setBackground(AdminWise.getAWColor());
        JPanel4.setBackground(AdminWise.getAWColor());
        
        txtChunkSize.addFocusListener(new SymFocusAction());
        
    }
    
    /*
     * Desc    :Add FocusListener to the chunkSize TextBox, 
     * 			So that validations can be done.
     * 			Validations include maximum 10000 records, minimum 10 records and numbers only formats.
     * Author  :Nishad Nambiar
     * Date    :26-Jun-2007 
     */  

    class SymFocusAction implements FocusListener{
    	public void focusGained(FocusEvent fe){
    		
    	}
    	public void focusLost(FocusEvent fe){
       		if(fe.getSource()==txtChunkSize){
    			try{
    				int chunkSize = Integer.parseInt(txtChunkSize.getText().trim()); 
    				if(chunkSize>10000){
    					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("atDataFilter.msgChunkSize0"));
        				txtChunkSize.setText("10000");
    				}
    				else if (chunkSize <1){
    					JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("atDataFilter.msgChunkSize1"));
        				txtChunkSize.setText("1");    					
    				}
    			}catch(NumberFormatException nfe){
    				AdminWise.printToConsole(nfe.toString());
    				JOptionPane.showMessageDialog(null,AdminWise.connectorManager.getString("atDataFilter.msgChunkSize2"));
    				txtChunkSize.setText("10000");
    			}
    		}
    	}
    }
    //--------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(java.awt.event.WindowEvent event) {
            Object object = event.getSource();
            if (object == VWDlgATDataFilter.this)
                Dialog_windowClosing(event);
        }
    }
    //--------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event) {
        BtnCancel_actionPerformed(null);
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            Object object = event.getSource();
            String strKey=event.getKeyText(event.getKeyChar());
            if(object==DateTime1 && strKey.equals("Escape"))
                DateTime1.setSelectedIndex(-1);
            else if(object==DateTime2 && strKey.equals("Escape"))
                DateTime2.setSelectedIndex(-1);
            else if(strKey.equals("Enter"))
                BtnView_actionPerformed(null);
            else if(strKey.equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
    //--------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnView)
                BtnView_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
            else if (object == BtnArchive)
                BtnArchive_actionPerformed(event);
            else if (object == CmbObject)
                CmbObject_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    public void setVisible(boolean b) {
        if (b) {
            Dimension d =VWUtil.getScreenSize();
            setLocation(d.width/4,d.height/4);
        }
        super.setVisible(b);
    }
    //--------------------------------------------------------------------------
    public void addNotify() {
        Dimension size = getSize();
        super.addNotify();
        if (frameSizeAdjusted)  return;
        frameSizeAdjusted = true;
        Insets insets = getInsets();
        setSize(insets.left+insets.right+size.width,insets.top+insets.bottom+size.height);
    }
    //--------------------------------------------------------------------------
    boolean frameSizeAdjusted = false;
    javax.swing.JPanel JPanel1 = new javax.swing.JPanel();
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    VWComboBox CmbObject = new VWComboBox();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    VWComboBox CmbEvent = new VWComboBox();
    javax.swing.JPanel JPanel2 = new javax.swing.JPanel();
    javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
    VWComboBox CmbUserName = new VWComboBox();
    javax.swing.JLabel JLabel4 = new javax.swing.JLabel();
    javax.swing.JTextField TxtClientIP = new javax.swing.JTextField();
    javax.swing.JPanel JPanel3 = new javax.swing.JPanel();
    javax.swing.JPanel JPanel4 = new javax.swing.JPanel();
    javax.swing.JLabel JLabel5 = new javax.swing.JLabel();
    VWDateComboBox DateTime1 = new VWDateComboBox();
    javax.swing.JLabel JLabel6 = new javax.swing.JLabel();
    VWDateComboBox DateTime2 = new VWDateComboBox();
    javax.swing.JLabel JLabel7 = new javax.swing.JLabel();
    ///javax.swing.JTextField txtChunkSize = new javax.swing.JTextField();
    VWMaskField txtChunkSize = new VWMaskField(VWMaskField.USNUMBER_TYPE,5,false);
    VWLinkedButton BtnView = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    VWLinkedButton BtnArchive = new VWLinkedButton(0,true);
    /*TitledBorder titledBorder1 = new TitledBorder(LBL_OBJECT_EVENT_NAME);
    TitledBorder titledBorder2 = new TitledBorder(LBL_USER_CLIENT_NAME);
    TitledBorder titledBorder3 = new TitledBorder(LBL_DATE_NAME);
    TitledBorder titledBorder4 = new TitledBorder(LBL_DATACHUNK_NAME);*/
    javax.swing.JLabel lblObjectEvent = new javax.swing.JLabel(LBL_OBJECT_EVENT_NAME);
    javax.swing.JLabel lblUserName = new javax.swing.JLabel(LBL_USER_CLIENT_NAME);
    javax.swing.JLabel lblDate = new javax.swing.JLabel(LBL_DATE_NAME);    
    
    javax.swing.JLabel lineObjectEvent = new javax.swing.JLabel();
    javax.swing.JLabel lineUserName = new javax.swing.JLabel();
    javax.swing.JLabel lineDate = new javax.swing.JLabel();   
    
    public static int iChunkSize=5000;
    //--------------------------------------------------------------------------
    public Vector getValues() {
        Vector listValue=new Vector();
        String str="";
        listValue.add("0");
        listValue.add(Integer.toString(((VWRecord)CmbObject.getSelectedItem()).getId()));
        if(CmbUserName.getSelectedIndex()>0) {
            str=((String)CmbUserName.getSelectedItem()).trim();
            listValue.add(str);
        }
        else {
            listValue.add(".");
        }
        str=TxtClientIP.getText().trim();
        listValue.add(str.equals("")?".":str);
        if(DateTime1.getSelectedItem()!=null)
        {
        	str = com.computhink.common.util.VWUtil.convertDate(DateTime1.getSelectedItem().toString(),"MM/dd/yyyy","yyyyMMdd");
            listValue.add(str.equals("")?".":str);
        }
        else
        {
            listValue.add(".");
        }
        if(DateTime2.getSelectedItem()!=null)
        {
        	str = com.computhink.common.util.VWUtil.convertDate(DateTime2.getSelectedItem().toString(),"MM/dd/yyyy","yyyyMMdd");
            listValue.add(str.equals("")?".":str);
        }
        else
        {
            listValue.add(".");
        }

        listValue.add(Integer.toString(((VWRecord)CmbEvent.getSelectedItem()).getId()));
        int chunkSize=VWUtil.to_Number(txtChunkSize.getFormatedValue());
        if(chunkSize<=0) 
                chunkSize=100;
        listValue.add(String.valueOf(chunkSize));
        return listValue;
    }
    //--------------------------------------------------------------------------
    void BtnView_actionPerformed(java.awt.event.ActionEvent event) {
        saveDlgOptions();
        cancelFlag=false;
        viewFlag=false;
        this.setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnArchive_actionPerformed(java.awt.event.ActionEvent event) {
        String archivePath=AdminWise.adminPanel.getATOptions()[0][1];
        File archivePathFile=new File(archivePath);
        if(archivePath==null || archivePath.equals("") || !archivePathFile.exists())
        {
            VWMessage.showNoticeDialog((java.awt.Component) this,
                VWMessage.ERR_ARCHIVE_NOTFOUND);
            return;
        }
        if(!archivePathFile.canWrite())
        {
            VWMessage.showNoticeDialog((java.awt.Component) this,
                VWMessage.ERR_ARCHIVE_NOTWRITABLE);
            return;
        }
        saveDlgOptions();
        viewFlag=true;
        cancelFlag=false;
        this.setVisible(false);
    }
    //--------------------------------------------------------------------------
    void CmbObject_actionPerformed(java.awt.event.ActionEvent event) {
        CmbEvent.removeAllItems();
        /* Purpose:  Added for ARS service Master enable and disable
        * Created By: C.Shanmugavalli		Date:	22 Sep 2006
        */
        VWRecord selObject=(VWRecord)CmbObject.getSelectedItem();
        if(selObject != null){
        	//If Selected 'All Objects'
        	if(CmbObject.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.allObjects)){
        		CmbEvent.addAllEventsForARS(ATEvents[selObject.getId()], VWDocTypeConnector.isARSEnabled());
        	}
        	//Before Making settings
        	else if(VWDlgATSetting.auditTrialSettings.size()==0){
        		CmbEvent.addAllItemsForARS(ATEvents[selObject.getId()], VWDocTypeConnector.isARSEnabled());
        	}
        	//After making settings
        	else{
        		CmbEvent.addItemsForARS(ATEvents[selObject.getId()], VWDocTypeConnector.isARSEnabled());
        	}
        }
    }
    //--------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
        saveDlgOptions();
        cancelFlag=true;
        viewFlag=false;
        this.setVisible(false);
    }
    //--------------------------------------------------------------------------
    public boolean getCancelFlag() {
        return cancelFlag;
    }
    //--------------------------------------------------------------------------
    public boolean getViewFlag() {
        return viewFlag;
    }
    //--------------------------------------------------------------------------
    private void saveDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos) {
            prefs.putInt("x", this.getX());
            prefs.putInt("y", this.getY());
        }
        else {
            prefs.putInt("x",50);
            prefs.putInt("y",50);
        }
        prefs.putInt("ChunkSize",VWUtil.to_Number(txtChunkSize.getText()));
        //Update variable.
        iChunkSize = VWUtil.to_Number(txtChunkSize.getText());
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        txtChunkSize.setText(String.valueOf(prefs.getInt("ChunkSize",100)));
    }
    //--------------------------------------------------------------------------
    public void loadDlgData() {
        CmbObject.removeAllItems();
        CmbEvent.removeAllItems();
        CmbUserName.removeAllItems();
        CmbObject.addItems(ATObjects);
        //The below line is commented, because it was loading all events, when next time the dialog 
        //was opened.
        //CmbEvent.addItems(ATEvents[2]);        
        CmbUserName.addItems(VWATConnector.getUsers());
    }
    //--------------------------------------------------------------------------
    boolean cancelFlag=true;
    boolean viewFlag=false;
    public static void main(String[] args) {
    	Frame frame = new Frame();
		VWDlgATDataFilter filter =  new VWDlgATDataFilter(AdminWise.adminFrame);
		filter.setVisible(true);		
	}
}