/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWAuditTrail;

import java.awt.*;
import javax.swing.*;

import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWRecycle.VWRecycleConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWImages.VWImages;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;
import ViewWise.AdminWise.VWUtil.*;
import java.io.File;
import ViewWise.AdminWise.VWConstant;

public class VWDlgATSetting extends javax.swing.JDialog implements VWConstant
{
    public VWDlgATSetting(Frame parent) 
    {
        super(parent);
        ///parent.setIconImage(VWImages.ATIcon.getImage());
        getContentPane().setBackground(AdminWise.getAWColor());
        setTitle(BTN_SETTING_NAME);
        setModal(true);
        getContentPane().setLayout(null);
        setSize(283,438);
        setVisible(false);
        VWRoom room=AdminWise.adminPanel.roomPanel.getSelectedRoom();
        JLabel1.setText(BTN_SETTING_NAME+" "+AdminWise.connectorManager.getString("dlgAtSetting.for")+" "+room.getName());
        getContentPane().add(JLabel1);
        JLabel1.setBounds(6,4,194, AdminWise.gBtnHeight);
        getContentPane().add(LstAtSettings);
        LstAtSettings.setBounds(4,28,276,366);
        
        /*
        ///LstAtSettings.setBounds(4,28,276,250);
        LstAtSettings.setBounds(4,28,276,178);
        periodPanel.setBorder(titledBorder1);
        periodPanel.setLayout(null);
        getContentPane().add(periodPanel);
        ///periodPanel.setBounds(4,290,276,102);
        periodPanel.setBounds(4,218,276,102);
        RdoNever.setText("Never");
        RdoNever.setSelected(true);
        TxtDays.setEnabled(false);
        JLabel2.setEnabled(false);
        periodPanel.add(RdoNever);
        RdoNever.setBounds(12,16,216, AdminWise.gBtnHeight);
        group.add(RdoNever); 
        RdoNever.setIcon(VWImages.RadioIcon5); 
        RdoNever.setPressedIcon(VWImages.RadioIcon2); 
        RdoNever.setRolloverIcon(VWImages.RadioIcon3); 
        RdoNever.setRolloverSelectedIcon(VWImages.RadioIcon4); 
        RdoNever.setSelectedIcon(VWImages.RadioIcon1); 
        RdoNever.setFocusPainted(false); 
        RdoNever.setBorderPainted(false); 
        RdoNever.setContentAreaFilled(false); 
        RdoNever.setMargin(new Insets(0,0,0,0));
        RdoBefore.setText("Archive before");
        periodPanel.add(RdoBefore);
        RdoBefore.setBounds(12,40,96,24);
        RdoBefore.setIcon(VWImages.RadioIcon5); 
        RdoBefore.setPressedIcon(VWImages.RadioIcon2); 
        RdoBefore.setRolloverIcon(VWImages.RadioIcon3); 
        RdoBefore.setRolloverSelectedIcon(VWImages.RadioIcon4); 
        RdoBefore.setSelectedIcon(VWImages.RadioIcon1); 
        RdoBefore.setFocusPainted(false); 
        RdoBefore.setBorderPainted(false); 
        RdoBefore.setContentAreaFilled(false); 
        RdoBefore.setMargin(new Insets(0,0,0,0)); 
        group.add(RdoBefore); 
        periodPanel.add(TxtDays);
        TxtDays.setBounds(108,40,100, AdminWise.gBtnHeight);
        JLabel2.setText("Day(s)");
        periodPanel.add(JLabel2);
        JLabel2.setBounds(216,40,48, AdminWise.gBtnHeight);
        JLabel3.setText("Last archive date : unknown");
        periodPanel.add(JLabel3);
        JLabel3.setBounds(12,72,252,24);
        JLabel3.setBorder(titledBorder2);
        periodPanel.add(JLabel3);
        */
        LstAtSettings.setBounds(4,28,276,294);
        archivePathPanel.setBorder(titledBorder3);
        archivePathPanel.setLayout(null);
        getContentPane().add(archivePathPanel);
        archivePathPanel.setBackground(AdminWise.getAWColor());
        ///archivePathPanel.setBounds(4,332,276,60);
        archivePathPanel.setBounds(4,334,276,60);
        BtnBrowse.setText(AdminWise.connectorManager.getString("dlgAtSetting.browse"));
        BtnBrowse.setActionCommand(AdminWise.connectorManager.getString("dlgAtSetting.browse"));
        archivePathPanel.add(BtnBrowse);
        //BtnBrowse.setBackground(java.awt.Color.white);
        BtnBrowse.setBounds(210,24,60, AdminWise.gBtnHeight);
        archivePathPanel.add(txtArchivePath);
        txtArchivePath.setBounds(12,24,196,24);
        txtArchivePath.setEditable(false);
        
        BtnOk.setText(BTN_OK_NAME);
        getContentPane().add(BtnOk);
        BtnOk.setBounds(136,410,72, AdminWise.gBtnHeight);
        //BtnOk.setBackground(java.awt.Color.white);
        BtnRefresh.setText(BTN_REFRESH_NAME);
        getContentPane().add(BtnRefresh);
        if(languageLocale.equals("nl_NL")){
        	  BtnRefresh.setBounds(6,410,72+25, AdminWise.gBtnHeight);
        }else
        BtnRefresh.setBounds(6,410,72, AdminWise.gBtnHeight);
        //BtnRefresh.setBackground(java.awt.Color.white);
        BtnCancel.setText(BTN_CANCEL_NAME);
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(208,410,72, AdminWise.gBtnHeight);
        //BtnCancel.setBackground(java.awt.Color.white);
        LstAtSettings.loadData(ATActions);
        SymAction lSymAction = new SymAction();
        BtnOk.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        RdoNever.addActionListener(lSymAction);
        RdoBefore.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        BtnBrowse.addActionListener(lSymAction);
        SymKey aSymKey=new SymKey();
        addKeyListener(aSymKey);
        LstAtSettings.addKeyListener(aSymKey);
        SymWindow aSymWindow=new SymWindow();
        this.addWindowListener(aSymWindow);                
        setResizable(false);
        loadSettings();
        getDlgOptions();
        setVisible(true);
        //}}
    }
//------------------------------------------------------------------------------
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWDlgATSetting.this)
                Dialog_windowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnOk_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
            Object object = event.getSource();
            if (object == BtnOk)
                    BtnOk_actionPerformed(event);
            else if (object == BtnCancel)
                    BtnCancel_actionPerformed(event);
            else if (object == BtnRefresh)
                    BtnRefresh_actionPerformed(event);
             else if (object == RdoNever)
                    RdoNever_actionPerformed(event);
             else if (object == RdoBefore)
                    RdoBefore_actionPerformed(event);
            else if (object == BtnBrowse)
                    BtnBrowse_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    public void setVisible(boolean b)
    {
        if (b) 
        {
            Dimension d =VWUtil.getScreenSize();
            setLocation(d.width/4,d.height/4);
            loadSettings();
        }
        super.setVisible(b);
    }
//------------------------------------------------------------------------------
    public void addNotify()
    {
        Dimension size = getSize();
        super.addNotify();
        if(frameSizeAdjusted)  return;
        frameSizeAdjusted = true;
        Insets insets = getInsets();
        setSize(insets.left + insets.right + size.width, insets.top + 
            insets.bottom + size.height);
    }
//------------------------------------------------------------------------------
    boolean frameSizeAdjusted = false;
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    VWCheckList LstAtSettings = new VWCheckList();
    VWLinkedButton BtnOk = new VWLinkedButton(0,true);
    VWLinkedButton BtnRefresh = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    javax.swing.JPanel periodPanel = new javax.swing.JPanel();
    TitledBorder titledBorder1 = new TitledBorder(LBL_RETENTION_PERIOD_NAME);
    TitledBorder titledBorder2 = new TitledBorder("");
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
    javax.swing.JTextField TxtDays = new javax.swing.JTextField();
    javax.swing.JRadioButton RdoNever = new javax.swing.JRadioButton();
    javax.swing.JRadioButton RdoBefore = new javax.swing.JRadioButton();
    javax.swing.ButtonGroup group = new javax.swing.ButtonGroup();
    javax.swing.JPanel archivePathPanel = new javax.swing.JPanel();
    VWLinkedButton BtnBrowse = new VWLinkedButton(0,true);
    javax.swing.JTextField txtArchivePath = new javax.swing.JTextField();
    TitledBorder titledBorder3 = new TitledBorder(LBL_ARCHIVE_NAME);
//------------------------------------------------------------------------------
    void BtnOk_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveATSettings();
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event)
    {
        loadSettings();
    }
//------------------------------------------------------------------------------
    void BtnBrowse_actionPerformed(java.awt.event.ActionEvent event)
    {
        loadSelectFolderDialog();
    }
//------------------------------------------------------------------------------
    void RdoNever_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(RdoNever.isSelected())
        {
            TxtDays.setText("");
            TxtDays.setEnabled(false);
            JLabel2.setEnabled(false);
        }
    }
//------------------------------------------------------------------------------
void RdoBefore_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(RdoBefore.isSelected())
        {
            TxtDays.setEnabled(true);
            JLabel2.setEnabled(true);
        }
    }
//------------------------------------------------------------------------------
    public void saveATSettings()
    {
    	//Adds the selected list info in this.auditTrialSettings Vector, so that in Filter this Vector can be used for cross check.
    	//Author : Nishad Nambiar
    	this.auditTrialSettings.removeAllElements();
        int count=LstAtSettings.getItemsCount();
        //Desc   :Adding 'All Objects' by default.
        //Author :Nishad Nambiar
        //Date   :13-Mar-2007
        auditTrialSettings.add(VWConstant.allEvents);
        String settings = "";
        for(int i=0;i<count;i++){
        	
        	VWATConnector.saveATSettings(((VWRecord)LstAtSettings.getItem(i)).getId(),LstAtSettings.getItemIsCheck(i)?1:0);
        	if(LstAtSettings.getItemIsCheck(i)){
        		auditTrialSettings.add(LstAtSettings.getItem(i).toString().trim());
        		settings = settings + ((VWRecord)LstAtSettings.getItem(i)).getName() + "," ;
        	}
        }
        settings = settings.substring(0, settings.length()-1);
        
        String message = AdminWise.connectorManager.getString("dlgAtSetting.message0")+settings+AdminWise.connectorManager.getString("dlgAtSetting.message1");
        int notifyId = VWConstant.notify_AT_UpdateSet;
        
        int ret = VWATConnector.addNotificationHistory(message, notifyId);
        
        saveATOptions();
    }
//------------------------------------------------------------------------------
    private void loadSettings()
    {
        LstAtSettings.setCheckItems(VWATConnector.getATSettings());
       //Enhancment CV10   set item 1 disable is added to disable the open document checbox in audit trail.
        LstAtSettings.setItemEnable(1,false);
        LstAtSettings.setItemEnable(29,false);
        LstAtSettings.setItemEnable(30,false);
        LstAtSettings.setItemEnable(31,false);
        //Disable Security Settings 
        LstAtSettings.setItemEnable(32,false);
        LstAtSettings.setItemEnable(33,false);
        LstAtSettings.setItemEnable(34,false);
        
        getATOptions();
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
  public void loadSelectFolderDialog()
{
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    
    String backupPath=txtArchivePath.getText();
    chooser.setCurrentDirectory(new File(backupPath));
    int returnVal = chooser.showSaveDialog(/*(Component)this*/null);
    if(returnVal == JFileChooser.APPROVE_OPTION)
    {
        backupPath=chooser.getSelectedFile().getPath();
        chooser.setVisible(false);
        chooser=null;
    }
    txtArchivePath.setText(backupPath);
}
//------------------------------------------------------------------------------
    private void saveATOptions()
    {
        String[][] atOptions=new String[1][2];
        atOptions[0][0]="Archive path";
        atOptions[0][1]=txtArchivePath.getText();
        AdminWise.adminPanel.saveATOptions(atOptions);
    }
//------------------------------------------------------------------------------
    private String[][] getATOptions()
    {
        String[][] ATOptions=AdminWise.adminPanel.getATOptions();
        txtArchivePath.setText(ATOptions[0][1]);
        return ATOptions;
    }
//------------------------------------------------------------------------------
    private boolean cancelFlag=true;
    String languageLocale=AdminWise.getLocaleLanguage();
    public static Vector auditTrialSettings = new Vector();
}
