/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWArchiveConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWAuditTrail;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import com.computhink.common.Creator;
import com.computhink.vwc.Session;
//--------------------------------------------------------------
public class VWATConnector implements VWConstant {
    public static List getATSettings() {
        List retList=new LinkedList();
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector settings=new Vector();
        AdminWise.gConnector.ViewWiseClient.getATSettings(room.getId(), settings);
        if(settings!=null && settings.size()>0) {
            int count=settings.size();
            for(int i=0;i<count;i++) {
                VWStringTokenizer tokens=new VWStringTokenizer((String)settings.get(i),SepChar,false);
                int recId=VWUtil.to_Number(tokens.nextToken());
                boolean recPositive=tokens.nextToken().equals("1");
                if(recPositive) retList.add(new VWRecord(recId,""));
            }
        }
        return retList;
    }
    //-----------------------------------------------------------
    public static void saveATSettings(int id, int enabled) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.setATSetting(room.getId(),
        id, enabled);
    }
    //-----------------------------------------------------------
    public static String[] getUsers() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) 
            return null;
        Vector users=new Vector();
        AdminWise.gConnector.ViewWiseClient.getCreators(room.getId(),users);
        if(users == null || users.size()==0) return null;
        return addFixFieldToUsersArr(users);
    }
    //-----------------------------------------------------------
    private static String[] addFixFieldToUsersArr(Vector users) {
        String[] data=null;
        int count=users.size();
        data = new String[count+1];
        data[0]=LBL_ALLUSERS_NAME;
        for(int i=0;i<count;i++)
            data[i+1]=((Creator)users.get(i)).toString();
        return data;
    }
    //-----------------------------------------------------------
    public static Vector getData(Vector filterList, int lastRecId) {
        if(filterList==null || filterList.size()==0) return null;
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return null;
        Vector data=new Vector();
        filterList.add(String.valueOf(lastRecId));
        AdminWise.gConnector.ViewWiseClient.getATData(room.getId(),filterList,data);
        filterList.remove(filterList.size()-1);
        return data;
    }
    //-----------------------------------------------------------
    public static void delData(Vector idsList, String archivePath) {
        if(idsList==null || idsList.size()==0) return ;
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return ;
        AdminWise.gConnector.ViewWiseClient.detATData(room.getId(),
            idsList,archivePath);
    }
    
    public static int addNotificationHistory(String message, int notifyId){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Session session = AdminWise.gConnector.ViewWiseClient.getSession(room.getId());
    	
    	message = message.replaceAll("user", session.user);
    	try{
    		AdminWise.gConnector.ViewWiseClient.addNotificationHistory(room.getId(), 0, VWConstant.nodeType_AT, notifyId, message);
    	}catch(Exception ex){
    		return -1;
    	}
    	return 0;
    }
    //-----------------------------------------------------------
    /*
    private class getDataThread extends Thread 
    {
        int roomId;
        Vector filterList;

        getDataThread(int roomId, Vector filterList)
        {
            this.roomId=roomId;
            this.filterList.addAll(filterList);
            setDaemon(true);
            setPriority(java.lang.Thread.MIN_PRIORITY);
            start();
        }
        public void run()
        {
            int lastRecId=0;
            try
            {
                Vector data=getData(filterList, lastRecId) {
                if(data.size()>0)
                {
                    data.get(data.size()-1)
                    lastRecId
                    if(!searchCancel)
                        resultCount+=getFindResult(roomId,srch,append);
                    while(!searchCancel && resultCount<serverResultCount)
                    {
                        resultCount+=getFindResult(roomId,srch,true);
                    }
                 }
            }
            }
            catch(Exception e){}
            finally{
                ViewWise.AdminWise.VWFind.VWFindPanel.setFindMode(true);
                searchCancel=false;
                resultCount=0;
            }
        }
     */
}