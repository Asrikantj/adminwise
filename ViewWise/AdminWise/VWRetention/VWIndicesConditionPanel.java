package ViewWise.AdminWise.VWRetention;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;

import com.computhink.ars.server.ARSPreferences;
import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.IndicesCondition;
import com.computhink.common.Util;
import com.computhink.vwc.VWClient;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRetention.VWIndicesConditionsTable;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWIndicesConditionPanel extends JPanel implements VWConstant{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	VWComboBox cmbDocTypesList = new VWComboBox();
	VWComboBox cmbIndicesList = new VWComboBox();
	VWComboBox cmbOperator = new VWComboBox();
	static JTextField txtvalue1 = new JTextField();
	static JTextField txtvalue2 = new JTextField();
	public JLabel lblDays= new JLabel(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblDays"));
	
	JPanel pnlTable = new JPanel();
	
	public SymAction symAction = new SymAction();
	JButton btnAdd = new JButton();
	JButton btnRemove = new JButton();
	JButton btnRefresh = new JButton();
	static VWIndicesConditionsTable tblConditions = new VWIndicesConditionsTable();
	static Vector vecTableData = new Vector();
	
	VWComboBox cmbSelctionValues = new VWComboBox();
	VWDateComboBox dateValue1 = new VWDateComboBox();
	VWDateComboBox dateValue2 = new VWDateComboBox();
	
	JScrollPane spTable = new JScrollPane(tblConditions);
	public static DefaultListModel listTasksModel = new DefaultListModel();
	JList listTasks = new JList(listTasksModel);
	JScrollPane spListTasks = new JScrollPane(listTasks);
	JButton btnUp = new JButton("");
	JButton btnDown = new JButton("");
	
	public static boolean active = false;
	public static boolean flag = false;
	public static boolean alterConditionSettingsFlag = true;
	private boolean taskSequenceChanged = false;
	public int curDocTypeSelected = -1;
	
	
	int dlgWidth = 492, dlgHeight = 480;
	int defaultDlgHeight = 285; 
	public VWIndicesConditionPanel(){
		
	}
	public VWIndicesConditionPanel(Frame parent){
		super();		
		if (active)
		{
			requestFocus();
			return;
		} 
		VWClient vwc = (VWClient)AdminWise.gConnector.ViewWiseClient;
		setSize(dlgWidth, defaultDlgHeight);
		setLocation(350,250);
		
		initTablePanel();
		setTablePanelMode(true);
		pnlTable.setBounds(7, 68, 468,213);
	}
	
	private void viewMode(){
		//Need to change the BtnRouteLocation for Retention.
		if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 2)
			BtnRouteLocation.setEnabled(false);
		else if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 1 || 
				AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 3 || AdminWise.adminPanel.retentionPanel.CURRENT_MODE == 4)
			BtnRouteLocation.setEnabled(true);
		else
			BtnRouteLocation.setEnabled(true);
	}
	
	public JPanel initTablePanel(){
		
		if(VWRetentionPanel.ARSFLAG)
			flag = true;
		else
			flag = false;
		
		int left = 10;
		pnlTable.setLayout(null);
		pnlTable.setBounds(0, 0, 300, 800);
		pnlTable.setBackground(java.awt.Color.white);
		
		JLabel indexlineBorderLabel =  new JLabel("");
		JLabel indexLabel =  new JLabel(AdminWise.connectorManager.getString("VWIndicesConditionPanel.indexLabel"));
		if(flag)
			indexLabel.setBounds(left, 2, 200, 13);//This is for ARS
		else
			indexLabel.setBounds(left-5, 2, 100, 13);
		pnlTable.add(indexLabel);
		
		Border border1 = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		indexlineBorderLabel.setBorder(border1);
		indexLabel.setForeground(Color.BLUE);
		if(flag)
			indexlineBorderLabel.setBounds(100, 8, 358, 2);// This is for ARS
		else
			indexlineBorderLabel.setBounds(90, 8, 360, 2);//width 360
		pnlTable.add(indexlineBorderLabel);
		
		txtRouteLocation.setVisible(false);
		
		//TitledBorder border = new javax.swing.border.TitledBorder("Index Condition");
		//border.setTitleColor(Color.BLUE);
		//pnlTable.setBorder(border);
		
		lblDocTypes.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblDocTypes")+" ");
		if(flag)
			lblDocTypes.setBounds(left , 20, 90, 20);
		else
			lblDocTypes.setBounds(left , 20, 85, 20);
		pnlTable.add(lblDocTypes);
		
		if(flag)
			cmbDocTypesList.setBounds(left + 100, 20, 301, 20);
		else
			cmbDocTypesList.setBounds(left + 90, 20, 251, 20);
		//PR Comment
		//loadDocTypes(); //Commented to check Whether the panel is getting displayed in the Retention window.
		cmbDocTypesList.addActionListener(symAction);
		pnlTable.add(cmbDocTypesList);

		if(flag)
			btnRefresh.setBounds(415 , 20, 20, 20);
		else
			btnRefresh.setBounds(left+345 , 20, 20, 20);
			
		btnRefresh.setToolTipText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.btnRefresh"));
		btnRefresh.setIcon(VWImages.RefreshIcon);
		btnRefresh.addActionListener(new SymAction());
		pnlTable.add(btnRefresh);
		
		
		lblIndexField.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblIndexField")+" ");
		lblIndexField.setBounds(left , 52, 85, 20);
		pnlTable.add(lblIndexField);
		
		if(flag)
			cmbIndicesList.setBounds(left + 100 , 52 , 301, 20);
		else
			cmbIndicesList.setBounds(left + 90 , 52 , 251, 20);
		//loadIndicesList();
		//panelStartIconRoute.add(cmbIndiceList);
		cmbIndicesList.addItemListener(new ListSymAction());		
		pnlTable.add(cmbIndicesList);
		
		int rowPosition = 81;
		lblCondition.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblCondition_1")+" ");
		lblCondition.setBounds(left , rowPosition, 85, 20);
		pnlTable.add(lblCondition);
		
		loadStringOperators();
		if(flag)
			cmbOperator.setBounds(left + 110, rowPosition, 90, 20);
		else
			cmbOperator.setBounds(left + 90, rowPosition, 90, 20);
		cmbOperator.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent ie){				
				VWIndexRec selectedIndexObj = (VWIndexRec) cmbIndicesList.getSelectedItem();
				int type = selectedIndexObj.getType();
				/*if(VWRetentionPanel.ARSFLAG)
					flag = true;
				else
					flag = false;*/
				if(type==2){// Date
					if(flag){
						//ARS - for date datatype, display the text box 
						cmbSelctionValues.setVisible(false);
						dateValue1.setVisible(false);
						dateValue1.setText("");
						dateValue2.setVisible(false);
						dateValue2.setText("");
						txtvalue1.setVisible(true);
						lblDays.setVisible(false);
						if(cmbOperator.getSelectedIndex()==COND_BETWEEN){
							txtvalue2.setVisible(true);
						}else{	
							txtvalue2.setVisible(false);
							txtvalue2.setText("");
						}
					}else{
						// This code is for Routing
						cmbSelctionValues.setVisible(false);
						if(cmbOperator.getSelectedIndex()==COND_STARTS_AFTER){
							dateValue1.setVisible(false);
							dateValue1.setText("");
							dateValue2.setVisible(false);
							dateValue2.setText("");
							txtvalue1.setVisible(true);
							lblDays.setVisible(true);
							txtvalue2.setVisible(false);
						}
						else{
							lblDays.setVisible(false);
							dateValue1.setVisible(true);
							if(cmbOperator.getSelectedIndex()==COND_BETWEEN){							
								dateValue2.setVisible(true);
							}
							else{
								dateValue2.setVisible(false);
								dateValue2.setText("");
							}
							txtvalue1.setVisible(false);
							txtvalue2.setVisible(false);
						}
					}
				}
				else{				
					cmbSelctionValues.setVisible(false);
					dateValue1.setVisible(false);
					dateValue1.setText("");
					dateValue2.setVisible(false);
					dateValue2.setText("");
					txtvalue1.setVisible(true);
					lblDays.setVisible(false);
					if(cmbOperator.getSelectedIndex()==COND_BETWEEN){
						txtvalue2.setVisible(true);
					}else{	
						txtvalue2.setVisible(false);
						txtvalue2.setText("");
					}
				}
				if(type==4){					
					if(cmbOperator.getSelectedIndex()==0){						
						txtvalue1.setVisible(false);
						dateValue1.setVisible(false);
						cmbSelctionValues.setVisible(true);						
					}
					else{
						txtvalue1.setVisible(true);
						dateValue1.setVisible(false);
						cmbSelctionValues.setVisible(false);
					}
				}
			}
		});
		pnlTable.add(cmbOperator);
		
		if(flag)
			txtvalue1.setBounds(left + 202, rowPosition, 100,20);
		else
			txtvalue1.setBounds(left + 182, rowPosition, 100,20);
		pnlTable.add(txtvalue1);
		
		lblDays.setBounds(left + 287, rowPosition, 100,20);
		pnlTable.add(lblDays);
		lblDays.setVisible(false);		
		
		dateValue1.setText("");
		dateValue1.setBounds(left + 182, rowPosition, 100,20);
		dateValue1.setDateFormat("MM/dd/yyyy");
		pnlTable.add(dateValue1);								
		dateValue1.setVisible(false);
		
		if(flag)
			cmbSelctionValues.setBounds(left + 192, rowPosition, 158,20);
		else
			cmbSelctionValues.setBounds(left + 182, rowPosition, 158,20);
		pnlTable.add(cmbSelctionValues);								
		cmbSelctionValues.setVisible(false);
				
		txtvalue2.setText("");
		txtvalue2.setBounds(left + 295, rowPosition, 100, 20);
		txtvalue2.setVisible(false);
		pnlTable.add(txtvalue2);
		
		dateValue2.setText("");
		dateValue2.setBounds(left + 287, rowPosition, 100, 20);
		dateValue2.setDateFormat("MM/dd/yyyy");
		pnlTable.add(dateValue2);								
		dateValue2.setVisible(false);

		
		//loadLogicalOperators();
		//cmbLogicalOperator.setBounds(268, 28, 60, 20);
		//pnlTable.add(cmbLogicalOperator);
		
		//btnAdd.setText("Add");
		btnAdd.setToolTipText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.btnAdd"));
		btnAdd.setBounds(415, rowPosition + 7, 20, 20);
		btnAdd.setMnemonic('A');
		pnlTable.add(btnAdd);
		btnAdd.setIcon(VWImages.plusIcon);
		btnAdd.addActionListener(new SymButton());
		
		//btnRemove.setText("Remove");
		btnRemove.setToolTipText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.btnRemove"));
		btnRemove.setBounds(438, rowPosition + 7, 20, 20);
		btnRemove.setMnemonic('R');
		btnRemove.setIcon(VWImages.minusIcon);
		pnlTable.add(btnRemove);
		btnRemove.addActionListener(new SymButton());
		
		spTable.setBounds(10, 110, 450, 105);
		pnlTable.add(spTable);
		//setTitle("Indices Condition Panel");
		return pnlTable;
	}
	
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if(object == btnUp){
				btnUp_ActionPerformed(event);
			}else if(object == btnDown){
				btnDown_ActionPerformed(event);        				
			}else if(object == btnRefresh){
				btnRefresh_ActionPerformed(event);
			}else if(object == cmbDocTypesList){
				//curDocTypeSelected = -1;
				cmbDocTypesList_ActionPerformed();
			}			
		}
	}
	
	private void loadValues()
	{
	    cmbSelctionValues.removeAllItems();
	    VWIndexRec indexRec = (VWIndexRec) cmbIndicesList.getSelectedItem();
	    Vector strValues = indexRec.selectionValues;
	    if(strValues==null || strValues.equals("")) return;
	    for(int i=0;i<strValues.size();i++){
		cmbSelctionValues.addItem(strValues.get(i));
	    }
	}
	public void loadDocTypes() {
        cmbDocTypesList.removeAllItems();
        cmbDocTypesList.addItem(new VWDocTypeRec(-1,LBL_DOCTYPE_NAME_1,false));
        Vector data = VWDocTypeConnector.getDocTypes();
        if (data!=null){
            for (int i=0;i<data.size();i++){
                cmbDocTypesList.addItem(data.get(i));
            }
        }
    }
	
	public void loadIndicesForSelectedDocType(){
		if(cmbDocTypesList.getSelectedIndex()>0){
			VWDocTypeRec selDocType=(VWDocTypeRec) cmbDocTypesList.getSelectedItem();		
			List indicesList =  VWDocTypeConnector.getDTIndices(selDocType.getId());
			if(indicesList==null || indicesList.size()==0) {
		        return;
		    }
			indexListCount = indicesList.size();
		    try{
		    	if(cmbIndicesList.getItemCount()>0)
		    		cmbIndicesList.removeAllItems();
			}catch(Exception ex){
				//System.out.println("Error1 "+ex.toString());
			}	
			for(int count=0; count<indicesList.size();count++){
				try{
					VWIndexRec indexRec = (VWIndexRec) indicesList.get(count);
					String type = indexRec.getType()+"";
					//Exclude Sequence datatype from the display
					if(type.trim().equalsIgnoreCase("5")){
						continue;
					}
					cmbIndicesList.addItem(indexRec);
				}catch(Exception ex){
					//System.out.println(" addItem Error2 "+ex.toString());
				}
			}
			VWRetentionPanel.selIndicesCount = cmbIndicesList.getItemCount();
			VWRetentionPanel.indexFlag = false;
			if(flag){// Only for ARS - adding created and modified
				AdminWise.printToConsole("For loadIndicesForSelectedDocType !!!! ");
				loadIndicesForARS();
			}
			
		}
		else{
			if(flag){//Only for ARS
				cmbIndicesList.removeAllItems();
				if(alterConditionSettingsFlag){
					indexConditionSettingsARS();
				}
				loadIndicesForARS();
			}
			else{
				indexConditionSettings();
				loadIndicesList();
			}
		}
		cmbIndicesList.setSelectedIndex(0);
	}
	public void setTablePanelMode(boolean mode){
		if(mode){
			pnlTable.setVisible(true);
		}
		else{
			pnlTable.setVisible(false);
		}
	}
	public void indexConditionSettings(){
		cmbOperator.setVisible(true);
		cmbOperator.setEnabled(true);
		if(flag){
			cmbOperator.setBounds(110, 81, 90, 20);
			txtvalue1.setBounds(202, 81, 100, 20);//(192, 81, 100, 20);
		}else{
			cmbOperator.setBounds(100, 81, 90, 20);
			txtvalue1.setBounds(202, 81, 100, 20);
		}
		txtvalue2.setVisible(true);
		lblCondition.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblCondition_1"));		
	}
	public void indexConditionSettingsARS(){
		loadDateOperators();
		cmbOperator.setBounds( 100+10, 81, 90, 20);
		cmbOperator.setVisible(true);
		//change the possition of textValue to be in the operator list box.
		if(flag)
			txtvalue1.setBounds(110+100, 81, 100, 20);
		else
			txtvalue1.setBounds(100+100, 81, 100, 20);
		txtvalue1.setVisible(true);
		txtvalue2.setVisible(false);
		lblCondition.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblCondition_2")+" ");
		
		dateValue1.setVisible(false);
		dateValue1.setText("");
		dateValue2.setVisible(false);
		dateValue2.setText("");
	}
	public void loadIndicesForARS(){
	    try{
	    	if(!VWRetentionPanel.indexFlag){
	    		VWRetentionPanel.selIndicesCount = cmbIndicesList.getItemCount();
	    		if(VWRetentionPanel.selIndicesCount>0)
	    			VWRetentionPanel.indexFlag = true;
	    	}
	    	AdminWise.printToConsole("cmbDocTypesList.getSelectedIndex() before adding indices :::: "+cmbDocTypesList.getSelectedIndex());
	    	if(cmbDocTypesList.getSelectedIndex()==-1 || cmbDocTypesList.getSelectedIndex()==0){// || cmbDocTypesList.getSelectedIndex()>0){
	    		// Only for ARS - for non of the document type got selected also include Created and Modified
	    		try{
	    			cmbIndicesList.removeAllItems();

	    			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	    			Vector indices=new Vector();


	    			AdminWise.adminPanel.setWaitPointer();
	    			AdminWise.gConnector.ViewWiseClient.getIndices(room.getId(), 0, indices);

	    			AdminWise.printToConsole("All indices for selected doc type = -1 :::: "+indices);

	    			for(int i=0; i<indices.size(); i++){
	    				StringTokenizer st = new StringTokenizer(indices.get(i).toString(), "\t");

	    				int indexType = 0;
	    				int indexId = 0;
	    				Index indexName = null;
	    				String indexName1 = "";

	    				if(st.hasMoreTokens()){
	    					indexId = Integer.parseInt(st.nextToken());
	    					indexName1 = st.nextToken();
	    					indexType = Integer.parseInt(st.nextToken());
	    				}

	    				if(indexType == 2){
	    					indexName = new Index(indexId);
	    					indexName.setName(indexName1);
	    					indexName.setType(indexType);
	    					VWIndexRec indexDate = new VWIndexRec(indexName);
	    					cmbIndicesList.addItem(indexDate);
	    				}
	    			}

	    			Index create = new Index(-1);
	    			create.setName(AdminWise.connectorManager.getString("VWIndicesConditionPanel.createDate"));
	    			create.setType(2);
	    			Index modifyInd = new Index(-2);
	    			modifyInd.setName(AdminWise.connectorManager.getString("VWIndicesConditionPanel.modifiedDate"));
	    			modifyInd.setType(2);

	    			VWIndexRec indexCreated = new VWIndexRec(create);
	    			VWIndexRec indexModified = new VWIndexRec(modifyInd);
	    			cmbIndicesList.addItem(indexCreated);
	    			cmbIndicesList.addItem(indexModified);
	    			AdminWise.adminPanel.setDefaultPointer();

	    		}catch(Exception ex){
	    			AdminWise.printToConsole("EXCEPTION IS :::: "+ex.getMessage());
	    		}
	    	}else if(cmbDocTypesList.getSelectedIndex()>0){
	    		
//	    		cmbIndicesList.removeAllItems();
	    		
				AdminWise.printToConsole("Inside load indices for created and modified date for > 0 !!!! ");
				Index create = new Index(-1);
				create.setName(AdminWise.connectorManager.getString("VWIndicesConditionPanel.createDate"));
				create.setType(2);
				Index modifyInd = new Index(-2);
				modifyInd.setName(AdminWise.connectorManager.getString("VWIndicesConditionPanel.modifiedDate"));
				modifyInd.setType(2);
				
				VWIndexRec indexCreated = new VWIndexRec(create);
				VWIndexRec indexModified = new VWIndexRec(modifyInd);
				
				if(VWRetentionPanel.selIndicesCount == cmbIndicesList.getItemCount()){
					alterConditionSettingsFlag = false;
					cmbIndicesList.addItem(indexCreated);
					cmbIndicesList.addItem(indexModified);
				}else if(cmbIndicesList.getItemCount()>VWRetentionPanel.selIndicesCount){//4>2
					try{
						cmbIndicesList.removeItemAt(cmbIndicesList.getItemCount()-1);
						cmbIndicesList.removeItemAt(cmbIndicesList.getItemCount()-1);
						alterConditionSettingsFlag = false;
						cmbIndicesList.addItem(indexCreated);
						cmbIndicesList.addItem(indexModified);
					}catch(Exception ex){
						//System.out.println("ex is : "+ex.getMessage());
					}
				}
			}
	    }catch(Exception ex){
		//System.out.println("<<Error in loadIndicesList "+ex.toString());
	    }			
	    if(cmbIndicesList.getItemCount()>0)
	    	cmbIndicesList.setSelectedIndex(0);
	}
	public void loadIndicesList(){
	    VWIndexRec[] indices = VWDocTypeConnector.getIndices(false);
	    try{
		if((cmbIndicesList!=null)&& cmbIndicesList.getItemCount()>0)
		    cmbIndicesList.removeAllItems();
	    }catch(Exception ex){
	    	//System.out.println("Error in loadIndicesList "+ex.toString());
	    }
	    try{
			for(int count=0; count<indices.length;count++){
			    VWIndexRec vwIndexRec = (VWIndexRec)indices[count];
			    if(vwIndexRec.getType() == 5)
				continue;
			    cmbIndicesList.addItem(indices[count]);
			}	
			if(flag){// Only for ARS - for none of the document type got selected also include Created and Modified
				VWIndexRec indexCreated = new VWIndexRec(-1,AdminWise.connectorManager.getString("VWIndicesConditionPanel.created"));
				VWIndexRec indexModified = new VWIndexRec(-2,AdminWise.connectorManager.getString("VWIndicesConditionPanel.modified"));
				alterConditionSettingsFlag = false;
				cmbIndicesList.addItem(indexCreated);
				cmbIndicesList.addItem(indexModified);
			}
	    }catch(Exception ex){
		//System.out.println("<<Error in loadIndicesList "+ex.toString());
	    }			
	    if(cmbIndicesList.getItemCount()>0)
		cmbIndicesList.setSelectedIndex(0);
	}
	
	class ListSymAction implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent iEvent){
			Object object=iEvent.getSource();
			if(object==cmbRouteStart){
				cmbRouteStart_ItemStateChanged();
			} else if(object==cmbIndicesList){
				cmbIndicesList_ItemStateChanged();
			}					
		}
	}
	class SymButton implements ActionListener{
		public void actionPerformed(ActionEvent ae){
			if(ae.getSource() == btnAdd){
				AdminWise.printToConsole("inside add.............");
				/**CV2019 - Issue fixes***/
				keepDocType = false;
				try{
					int roomId = AdminWise.adminPanel.roomPanel.getSelectedRoom().getId();
					Vector keepDocTypeFlag = new Vector();
	                int retVal = VWConnector.ViewWiseClient.getKeepDocType(roomId, keepDocTypeFlag);
	                if(retVal!=-1){
	                	String keepdoctype = "";
	                	if(keepDocTypeFlag!=null && keepDocTypeFlag.size()>0){
	                		keepdoctype = keepDocTypeFlag.get(0).toString();
	                		
	                		if(keepdoctype.equalsIgnoreCase("true"))
		                		keepDocType = true;
		                	else 
		                		keepDocType = false;
	                	}
	                	
	                }
				}catch(Exception ex){	
					AdminWise.printToConsole("Exception in  getKeepDocType method :::: "+ex.getMessage());
				}
				AdminWise.printToConsole("cmbDocTypesList.getSelectedItem().toString() :::: "+cmbDocTypesList.getSelectedItem().toString());
//				if(cmbDocTypesList.getSelectedItem().toString().equals(LBL_DOCTYPE_NAME_1)){
//					AdminWise.printToConsole("LBL_DOCTYPE_NAME_1 :::: "+LBL_DOCTYPE_NAME_1);
//					cmbDocTypesList.setSelectedIndex(-1);
//					cmbDocTypesList.setSelectedItem(LBL_DOCTYPE_NAME_1);
//					AdminWise.printToConsole("LBL_DOCTYPE_NAME_1 set as :::: "+cmbDocTypesList.getSelectedIndex());
//				}
				AdminWise.printToConsole("keepDocType :"+keepDocType);
                AdminWise.printToConsole("cmbDocTypesList.getSelectedIndex() Add button click :::: "+cmbDocTypesList.getSelectedIndex());
				if((keepDocType) && cmbDocTypesList.getSelectedIndex()==0){
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_1"), AdminWise.connectorManager.getString("Administration.Title"),JOptionPane.OK_OPTION);
					cmbDocTypesList.requestFocus();
					return;
				}
				
				//Allow maximum 10 conditions.
				if(tblConditions.getRowCount()>=10){
					active = false;
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_2"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
					active = true;					
					return;
				}
				curDocTypeSelected = cmbDocTypesList.getSelectedIndex();
				String selIndiceName = ((VWIndexRec)cmbIndicesList.getSelectedItem()).toString();
				//Check for duplicate indice entry.
				VWIndexRec vwIndexRec = (VWIndexRec) cmbIndicesList.getSelectedItem();
				String indexId = String.valueOf(vwIndexRec.getId());
				String indexTypeId = ""+vwIndexRec.getType();
				if(tblConditions.isDuplicate(indexId)){
					active = false;
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_3"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
					active = true;					
					return;
				}
				String selOperator = "";
				if(cmbOperator.isVisible()){//Not required if the cmbOperator is invisible for Date ARS
					selOperator = cmbOperator.getSelectedItem().toString();	
				}
				
				String selValue1 = "";
				String selValue2 = "";

				int type = vwIndexRec.getType();//getIndexType(selIndiceName);
				int selectedOPIndex = cmbOperator.getSelectedIndex();

				//NUMERIC TYPE condition checking
				if(type == 1){
					if( (!VWUtil.isNumeric(txtvalue1.getText(), false)) ){
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_4"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
						txtvalue1.requestFocus();
						active = true;
						return;
					}
					if( (selectedOPIndex==COND_BETWEEN  && !VWUtil.isNumeric(txtvalue2.getText(), false)) ){
						active = false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_4"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
						txtvalue2.requestFocus();
						active = true;
						return;
					}
				}
				//End...						
				
				if(flag){
					//For ARS - date datatype, text box will be displayed
					selValue1 = txtvalue1.getText();
					//CV10.1 Enchancement modified the operator = with >= (or) <=
					//selOperator = "=";
					selOperator = cmbOperator.getSelectedItem().toString();
					if(type==2){
						if( (!VWUtil.isNumeric(txtvalue1.getText(), false)) ){
							active = false;
							JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_4"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
							txtvalue1.requestFocus();
							active = true;
							return;
						}
						if(!txtvalue1.getText().trim().equals("") && (!isNumeric(txtvalue1.getText()))){
							//validating the no.of days field to be restricted to 9 digits.
							JOptionPane.showMessageDialog(VWIndicesConditionPanel.this,	AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_5"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
							txtvalue1.requestFocus();
							return;
						}
					}
				}else{
					// This for Routing - date will have the date combobox
					if (type == 2 && selectedOPIndex==COND_BETWEEN){
						String selDateValue1 = dateValue1.getSelectedItem().toString();					
						String selDateValue2 = dateValue2.getSelectedItem().toString();
						if(selDateValue1.trim().equals("")){
							active = false;
							JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_6"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
							active = true;
							dateValue1.requestFocus();
							return;
						}else if(selDateValue2.trim().equals("")){
							active = false;
							JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_7"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
							active = true;
							dateValue2.requestFocus();
							return;
						}
					}
					
					if (type == 2 && selectedOPIndex==COND_STARTS_AFTER){
						if(!VWUtil.isNumeric(txtvalue1.getText(), true)){					
							active = false;
							JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_8"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
							active = true;
							txtvalue1.requestFocus();
							return;
						}									
					}
					
					if (type == 2 && selectedOPIndex!=COND_STARTS_AFTER){
						selValue1 = dateValue1.getSelectedItem().toString();
						selValue2 = dateValue2.getSelectedItem().toString();
					}else{
						selValue1 = txtvalue1.getText();
						selValue2 = txtvalue2.getText();					
					}
				}
				//Selection
				if (type == 4){
					 if(selectedOPIndex==0){
						 selValue1=cmbSelctionValues.getSelectedItem().toString();
					 }
					 else{
						 selValue1 = txtvalue1.getText();
					 }
				}
				else{
					
				}
				//End...
				if(flag && txtvalue2.isVisible())
					selValue2 = txtvalue2.getText();
				
				if(selValue1.trim().equals("")){
					active = false;
					JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_9"), AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.OK_OPTION);
					active = true;
					//For ARS - text field will be displayed for date datatype
					if(flag){
						txtvalue1.requestFocus();
					}else{
						if (type == 2)
							dateValue1.requestFocus();
						else
							txtvalue1.requestFocus();	
					}
					
				}else{
					String selLogicalOperator = "AND";
					if(selValue2.trim().equals("")){
						selValue2="-";
					}
					String curSelectedData = "";
					
					if(flag && type!=2){
						selOperator = cmbOperator.getSelectedItem().toString();
					}
					
					if(flag && type==2){
						AdminWise.adminPanel.retentionPanel.TxtNotifyBefore.setEnabled(true);
					}
					
					if(vecTableData.size()==0)
						curSelectedData = "	"+selIndiceName + "\t" + selOperator + "\t" +  selValue1 +
							"\t" +"-" + "\t"+selValue2 + "\t"+indexId+"\t"+indexTypeId;
					else
						curSelectedData = "	"+selIndiceName + "\t" + selOperator + "\t" +  selValue1 + 
							"\t" +selLogicalOperator + "\t"+selValue2 + "\t"+indexId+"\t"+indexTypeId;
					
					
					vecTableData.add(curSelectedData);
					tblConditions.addData(vecTableData);
				}
			}
			
			if(ae.getSource() == btnRemove){
				BtnRemove_actionPerformed(ae);
			}
		}
	}
	
	public boolean isNumeric(String value){
		boolean isValid = false;
		int numVal =0;
		try{
			numVal = Integer.parseInt(value);	

			if(numVal>0 && numVal<1000000000)
				isValid = true;
			else
				isValid = false;

		}catch(NumberFormatException nfe){
			isValid = false;
			return isValid;			
		}		
		return isValid;
	}
	public void clearTableAndRelatedData(){
		try{
			vecTableData = new Vector();
			tblConditions.clearData();
			txtRouteLocation.setText("");			
		}catch(Exception ex){
			//System.out.println("Value of ex is : "+ex);
		}
	}
	
	public void clearAll(){
		try{
			curDocTypeSelected = -1;
			if(flag)
				this.cmbDocTypesList.setSelectedIndex(-1);
			else
				this.cmbDocTypesList.setSelectedIndex(0);
			this.cmbIndicesList.setSelectedIndex(0);
			if(!flag){
				this.cmbOperator.setSelectedIndex(0);
			}
		}catch(Exception ex){
			//System.out.println("VWIndicesConditionPanel - Error in clear :"+ex.toString());
		}
	}
	public void clearAllText(){
		txtvalue1.setText("");
		txtvalue2.setText("");
		dateValue1.setText("");
		dateValue2.setText("");
	}
	public void loadOperators(){
		cmbOperator.addItem(">");
		cmbOperator.addItem(">=");
		cmbOperator.addItem("<");
		cmbOperator.addItem("<=");
		cmbOperator.addItem("=");
		cmbOperator.addItem("!=");
	}
	
	public void loadDateOperators(){
		cmbOperator.addItem(COND_STARTS_BEFORE_STR);
		cmbOperator.addItem(COND_STARTS_AFTER_STR);
	}
	public void loadStringOperators(){
		cmbOperator.addItem("=");
		cmbOperator.addItem("like");
	}
	public void loadIntDateOperators(boolean displayStartsAfter){
		cmbOperator.addItem(">");
		cmbOperator.addItem(">=");
		cmbOperator.addItem("<");
		cmbOperator.addItem("<=");
		cmbOperator.addItem("=");
		cmbOperator.addItem("!=");
		cmbOperator.addItem(COND_BETWEEN_STR);
		if(displayStartsAfter)
			cmbOperator.addItem(COND_STARTS_AFTER_STR);
	}
	
	public void btnUp_ActionPerformed(ActionEvent event){
		try{
			//CODE FOR MOVING UP...			
			int selectedIndex = listTasks.getSelectedIndex();
			if(selectedIndex>0){
				String selectedItem1 = String.valueOf(listTasksModel.get(selectedIndex-1));
				String selectedItem2 = String.valueOf(listTasksModel.get(selectedIndex));    				
				listTasksModel.remove(selectedIndex-1);
				listTasksModel.add(selectedIndex-1,selectedItem1);
				listTasksModel.remove(selectedIndex);
				listTasksModel.add(selectedIndex-1,selectedItem2);
				listTasks.setSelectedIndex(selectedIndex-1);
				taskSequenceChanged = true;
			}
		}catch(Exception e){
			//System.out.println("Exception in Up BTN Click :"+e.toString());
		}
		//END...		
	}
	
	
	public void btnDown_ActionPerformed(ActionEvent event){
		try{
			//CODE FOR MOVING DOWN...				
			int selectedIndex = listTasks.getSelectedIndex();
			if(selectedIndex>=0 && selectedIndex<listTasks.getModel().getSize()){
				String selectedItem1 = String.valueOf(listTasksModel.get(selectedIndex+1));
				String selectedItem2 = String.valueOf(listTasksModel.get(selectedIndex));
				listTasksModel.remove(selectedIndex+1);
				listTasksModel.add(selectedIndex+1,selectedItem1);
				listTasksModel.remove(selectedIndex);
				listTasksModel.add(selectedIndex+1,selectedItem2);
				listTasks.setSelectedIndex(selectedIndex+1);
				taskSequenceChanged = true;
			}
		}catch(Exception e){
			//System.out.println("Exception in Down BTN Click :"+e.toString());
		}
		//END...
	} 
	public void btnRefresh_ActionPerformed(ActionEvent event){
		try{
			int curselected = cmbDocTypesList.getSelectedIndex();
			if(curselected>0){
				loadIndicesForSelectedDocType();
			}else{
				loadDocTypes();
				if(flag){// Only for ARS
					AdminWise.printToConsole("btnRefresh_ActionPerformed !!!! ");
					loadIndicesForARS();
				}
				else{
					loadIndicesList();
				}
			}
		}catch(Exception ex){
		}
	
	}
	public void cmbDocTypesList_ActionPerformed(){
		try{
			int rowCount = tblConditions.getRowCount();
			int curselected = cmbDocTypesList.getSelectedIndex();
			
			if(VWRetentionPanel.modeUpdate&&rowCount==0)
				curDocTypeSelected=-1;
			
			//curDocTypeSelected - From ARS curDocTypeSelected is set to -3 to avoid this check
			if(curDocTypeSelected != curselected){//&& curDocTypeSelected!=-3
				if(curDocTypeSelected!=-3 && curDocTypeSelected!=-1){
					if(flag){
						if( (rowCount>0) ){//&& (curDocTypeSelected!=-1) - They can created retention without selecting any document type
							if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE != 2 && curDocTypeSelected != 0 || 
									(curselected>0 && curDocTypeSelected == 0 && AdminWise.adminPanel.retentionPanel.CURRENT_MODE != 2)){
								active = false;
								VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_10"));
								active = true;
								cmbDocTypesList.setSelectedIndex(curDocTypeSelected);
								return;
							}
						}
					}else{
						if( (rowCount>0)  && (curDocTypeSelected!=-1) ){
							if(AdminWise.adminPanel.retentionPanel.CURRENT_MODE != 2 && curDocTypeSelected != 0 || 
									(curselected>0 && curDocTypeSelected == 0 && AdminWise.adminPanel.retentionPanel.CURRENT_MODE != 2)){
								active = false;
								VWMessage.showInfoDialog(this,AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_10"));
								active = true;
								cmbDocTypesList.setSelectedIndex(curDocTypeSelected);
								return;
							}
						}
					}
					
				}
				else{
					clearAllText();
					loadIndicesForSelectedDocType();
				}
			}else{
				if(flag){//Only for ARS
					if(alterConditionSettingsFlag){
						indexConditionSettingsARS();
					}
					AdminWise.printToConsole("For cmbDocTypesList_ActionPerformed !!!! ");
					loadIndicesForARS();
				}
			}
		}catch(Exception e){
			//System.out.println("EXCEPTION :"+e.toString());
		}
	}
	
	//Item State Changed Event for RouteStart Combo...
	public void cmbRouteStart_ItemStateChanged(){
			if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[1])){
				txtRouteLocation.setText("");
				lblTriggerAt.setVisible(false);
				//lblDocTypes.setVisible(false);
				txtRouteLocation.setVisible(false);
				BtnRouteLocation.setVisible(false);
				panelStartIconRoute.setBounds(7, 5, 472, 48);
				panelSeqTask.setBounds(7, 63, 472, 115);
				//BtnSave.setBounds(305, 185,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				//BtnCancel.setBounds(387, 185,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
/*				lblTaskSequenceTitle.setBounds(10, 60, 240, 22);
				spListTasks.setBounds(10, 80, 425, 80);
				btnUp.setBounds(436, 80, 24, 22);
				btnDown.setBounds(436, 105, 24, 22);				
				BtnSave.setBounds(293, 175, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(380, 175, 80, AdminWise.gBtnHeight);*/
				
				setTablePanelMode(false);
				setSize(dlgWidth, 250);
			}
			else if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[0])){
				lblTriggerAt.setVisible(true);
				//lblDocTypes.setVisible(false);
				//lblIndexField.setVisible(false);
				txtRouteLocation.setVisible(true);
				txtRouteLocation.setText("");
				BtnRouteLocation.setVisible(true);
				
				lblTriggerAt.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.location")+" ");
				panelStartIconRoute.setBounds(7, 5, 472, 85);
				panelSeqTask.setBounds(7, 90, 472, 115);
				//BtnSave.setBounds(305, 212,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				//BtnCancel.setBounds(387, 212,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				
				
/*				lblTaskSequenceTitle.setBounds(10, 105, 240, 22);
				spListTasks.setBounds(10, 125, 425, 80);
				btnUp.setBounds(436, 125, 24, 22);
				btnDown.setBounds(436, 150, 24, 22);				
				BtnSave.setBounds(300, 220, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(382, 220, 80, AdminWise.gBtnHeight);
*/				
				setTablePanelMode(false);
				setSize(dlgWidth, 277);
				
			}else if(cmbRouteStart.getSelectedItem().toString().trim().equalsIgnoreCase(VWConstant.lstStartWhen[2])){
				//lblIndexField.setVisible(true);
				lblTriggerAt.setVisible(false);
				//lblDocTypes.setVisible(true);
				txtRouteLocation.setVisible(false);
				BtnRouteLocation.setVisible(false);
				clearAll();
				clearTableAndRelatedData();
				//lblTriggerAt.setText("Select document type: ");
				//lblIndexField.setText("Select index field: ");		
				panelStartIconRoute.setBounds(7, 5, 472, 58);
				pnlTable.setBounds(7, 63, 472, 228);
				panelSeqTask.setBounds(7, 293, 472, 115);
				//BtnSave.setBounds(305, 415,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				//BtnCancel.setBounds(387, 415,  AdminWise.gBtnWidth, AdminWise.gBtnHeight);
				
/*				lblTaskSequenceTitle.setBounds(10, 250, 240, 22);
				spListTasks.setBounds(10, 270, 425,80);
				btnUp.setBounds(436, 270, 24, 22);
				btnDown.setBounds(436, 295, 24, 22);				
				BtnSave.setBounds(300, 365, 80, AdminWise.gBtnHeight);
				BtnCancel.setBounds(382, 365, 80, AdminWise.gBtnHeight);*/
				setTablePanelMode(true);
				setSize(dlgWidth,dlgHeight);
			}
	}
	public void cmbIndicesList_ItemStateChanged(){
		if(cmbIndicesList!=null && cmbIndicesList.getItemCount()>0){
			VWIndexRec selectedIndexObj = (VWIndexRec) cmbIndicesList.getSelectedItem();
			clearAllText();
			int type = selectedIndexObj.getType();
			//if selected index is Date object
			if(type==2){
				dateValue1.setVisible(true);
				txtvalue1.setVisible(false);
				txtvalue2.setVisible(false);
				if(flag){//Only for ARS
					if(alterConditionSettingsFlag){
						indexConditionSettingsARS();	
					}
				}else{
					indexConditionSettings();
				}
			}
			else{
				dateValue1.setVisible(false);
				dateValue2.setVisible(false);
				txtvalue1.setVisible(true);
				indexConditionSettings();
			}
			
			cmbOperator.removeAllItems();
			if(type==0 || type == 3){
				loadStringOperators();
				txtvalue1.setVisible(true);
				txtvalue2.setVisible(false);
			}else if(type==1){
				loadIntDateOperators(false);
			}else if(type==2){
				if(flag){ // For ARS - need to display text for date datatype
					indexConditionSettingsARS();
					/*cmbOperator.setVisible(false);
					txtvalue1.setBounds(100, 81, 100, 20);
					//loadStringOperators();
					txtvalue1.setVisible(true);*/
					txtvalue2.setVisible(false);	
				}else{
					indexConditionSettings();
					cmbOperator.setVisible(true);
					// check whether tbe txtvalue1 is coming in the correct place
					loadIntDateOperators(true);
				}
			}
			else if(type == 4){
				loadStringOperators();
				loadValues();
				cmbSelctionValues.setVisible(true);
			}
			else{
				loadOperators();
			}
		}
	}
	void BtnRemove_actionPerformed(java.awt.event.ActionEvent event){
		try{				
			if (tblConditions == null || tblConditions.getSelectedRows().length <= 0){
						active =false;
						JOptionPane.showMessageDialog(VWIndicesConditionPanel.this, AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_11"));
						active = true;
						return;
			}
			int selRows[] = tblConditions.getSelectedRows();
			for(int curRow=(selRows.length-1); curRow>=0; curRow--){
				vecTableData.remove(selRows[curRow]);
				tblConditions.m_data.delete(selRows[curRow]);
				tblConditions.m_data.fireTableDataChanged();
			}
			if(tblConditions.getRowCount()==0){
				curDocTypeSelected = -1;
			}
		}catch(Exception e){
			//AdminWise.printToConsole("Error :"+e.toString());
		}
	}
	
	JLabel lblDocTypes = new JLabel();
	JLabel lblIndexField = new JLabel();
	JLabel lblCondition = new JLabel();
	JLabel lblTriggerAt = new JLabel();
	JComboBox cmbRouteStart = new JComboBox();
	static JTextField txtRouteLocation = new JTextField();
	JPanel panelStartIconRoute = new JPanel();
	JPanel panelSeqTask = new JPanel();
	VWLinkedButton BtnRouteLocation = new VWLinkedButton(2);
	public int indexListCount = 0;
	private boolean keepDocType;
	public static void main(String[] args) {
		/*try{
			String plasticLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}
		catch(Exception se){
		}
		JFrame frame = new JFrame();
		new VWImages();
		VWIndicesConditionPanel setting = new VWIndicesConditionPanel(null);
		
		//frame.add(setting);
		setting.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.pack();*/
		try {
			String plasticLookandFeel = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		} catch (Exception se) {
		}
		JFrame frame = new JFrame();
		VWIndicesConditionPanel panel = new VWIndicesConditionPanel();
		frame.add(panel);
		//panel.setupUI();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*
		 * JFrame.resize() is replaced with JFrame.setSize() as JFrame.setSize() is deprecated
		 * Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
		frame.setSize(800, 600);
	}
	public JPanel getIndicesCondition(){
		return this.initTablePanel();
	}
	
	public int getDocTypeId()
    {
		return ((VWDocTypeRec)cmbDocTypesList.getSelectedItem()).getId();
    }
	public String[][] getData() {
		return tblConditions.getData();
	}
	/**
	 * insertData - This method is used to set the index table values in to the indicesConditionTable. 
	 * @param data
	 */
	public void insertData(String[][] data){
		tblConditions.insertData(data);
	}
	public void addData(String[][] data){
		tblConditions.addData(data);
	}
	public void addData(List list){
		tblConditions.addData(list);
	}
	public void enableARSDateSettings(){
		if(flag){ // For ARS - need to display text for date datatype
			lblCondition.setText(AdminWise.connectorManager.getString("VWIndicesConditionPanel.lblCondition_2")+" ");
			cmbOperator.setVisible(false);
			txtvalue1.setBounds(100, 81, 100, 20);
			txtvalue1.setVisible(true);
		}
	}
	/**
	 * setSelectedDocType - This method is used to  set the document Type Id
	 * @param docTypeId
	 */
	public void setSelectedDocType(int docTypeId){
		
		for(int count=0;count<cmbDocTypesList.getItemCount();count++){
			try{
				VWDocTypeRec selDocType = (VWDocTypeRec) cmbDocTypesList.getItemAt(count);
				int curDocTypeId = selDocType.getId();
				if(curDocTypeId == docTypeId){
					cmbDocTypesList.setSelectedIndex(count);
					curDocTypeSelected = count;
					break;
				}
			}catch(Exception e){
				//System.out.println("EXCEPTION in setSelectedDocType() method :"+e.toString());
			}
		}
	}
}