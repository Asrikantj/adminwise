/**
 * 
 */
package ViewWise.AdminWise.VWRetention;

/**
 * @author nishad.n
 *
 */
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import ViewWise.AdminWise.VWConstant;

import com.computhink.common.Util;
import com.computhink.vwc.VWTableResizer;

//--------------------------------------------------------------------------
public class VWIndicesConditionsTable extends JTable {//implements VWCConstants {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	protected VWIndicesConditionsTableData m_data;
	public int ttlColumns=0;
	public int summaryColumn=0;
	public VWIndicesConditionsTable(){
		super();
		getTableHeader().setReorderingAllowed(false);
		m_data = new VWIndicesConditionsTableData(this);
		setCellSelectionEnabled(false);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		Dimension tableWith = getPreferredScrollableViewportSize();
		for (int k = 0; k < VWIndicesConditionsTableData.m_columns.length; k++) {
			TableCellRenderer renderer;
			
			DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
			textRenderer.setHorizontalAlignment(VWIndicesConditionsTableData.m_columns[k].m_alignment);
			renderer = textRenderer;
			VWIndicesConditionsRowEditor editor=new VWIndicesConditionsRowEditor(this);
			TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWIndicesConditionsTableData.m_columns[k].m_width),renderer,editor);
			addColumn(column);
		}
		
		ttlColumns = getColumnModel().getColumnCount();
		summaryColumn = ttlColumns-1;
		getColumnModel().getColumn((summaryColumn)).setPreferredWidth(150);

		setRowSelectionAllowed(true);
		setBackground(java.awt.Color.white);
		//To get horizontal scroll bar
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		SymMouse aSymMouse = new SymMouse();
		addMouseListener(aSymMouse);
		setRowHeight(20);
		
		JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        if(VWRetentionPanel.ARSFLAG)
        	header.setBackground(Color.white);
        
        setTableResizable();        
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);        
        //Hide INDEX ID and TYPE Column
		getColumnModel().getColumn(m_data.COL_INDICEID).setPreferredWidth(0); 
		getColumnModel().getColumn(m_data.COL_INDICEID).setMinWidth(0);
		getColumnModel().getColumn(m_data.COL_INDICEID).setMaxWidth(0);
		getColumnModel().getColumn(m_data.COL_INDEXTYPEID).setPreferredWidth(0); 
		getColumnModel().getColumn(m_data.COL_INDEXTYPEID).setMinWidth(0);
		getColumnModel().getColumn(m_data.COL_INDEXTYPEID).setMaxWidth(0);	
/*		
		int tblWidth = 608;		
		int colWidth = (tblWidth/getColumnCount()-2);
		for(int count=0;count<getColumnCount()-2;count++)			
			getColumnModel().getColumn(count).setPreferredWidth(colWidth); 
*/
	}
	
	public void setTableResizable() {
		// Resize Table
	  	new VWTableResizer(this);
	}

//	--------------------------------------------------------------------------
	class SymMouse extends java.awt.event.MouseAdapter{
		public void mouseClicked(java.awt.event.MouseEvent event) {
			Object object = event.getSource();
			if (object instanceof JTable)
				if(event.getModifiers()==event.BUTTON3_MASK)
					VWDocRouteTable_RightMouseClicked(event);
				else if(event.getModifiers()==event.BUTTON1_MASK)
					VWDocRouteTable_LeftMouseClicked(event);
		}
	}
//	--------------------------------------------------------------------------
	void VWDocRouteTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			lSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			lDoubleClick(event,row,column);
	}
//	--------------------------------------------------------------------------
	void VWDocRouteTable_RightMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			rSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			rDoubleClick(event,row,column);
	}
//	--------------------------------------------------------------------------
	private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	public void addData(List list) {
		m_data.setData(list);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public void addData(String[][] data) {
		m_data.setData(data);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public String[][] getData() {
		return m_data.getData();
	}
//	--------------------------------------------------------------------------
	public void removeData(int row) {
		m_data.remove(row);
		m_data.fireTableDataChanged();
	}
	public void removeData() {
		for(int i = 0; i<m_data.getRowCount(); i++){
			m_data.remove(i);
		}
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	
	public void insertData(String[][] data) {
		for(int i=0;i<data.length;i++) {
			m_data.insert(new RouteSummaryRowData(data[i]));
		}
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		return m_data.getRowData(rowNum);
	}
//	--------------------------------------------------------------------------
	public int getRowDocLockId(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return Util.to_Number((row[0]));
	}
//	--------------------------------------------------------------------------
	public String getRowDocName(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return row[1];
	}
//	--------------------------------------------------------------------------
	public int getRowType() {
		return getRowType(getSelectedRow());
	}
//	--------------------------------------------------------------------------
	public int getRowType(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return Util.to_Number((row[6]));
	}
//	--------------------------------------------------------------------------
	public void clearData() {
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public boolean isDuplicate(String indiceId) {
		for(int count=0;count<m_data.m_vector.size();count++){
			String curRowData[] = getRowData(count);
			if(curRowData[m_data.COL_INDICEID].trim().equalsIgnoreCase(indiceId)){
				return true;
			}
		}
		return false;
	}
//	--------------------------------------------------------------------------
}
class RouteSummaryRowData {
	public String   m_IndiceName;
	public String   m_Operator;
	public String   m_Value1;
	public String   m_LogOperator;
	public String   m_Value2;
	public String   m_IndiceId;
	public String   m_IndexTypeId;

	public RouteSummaryRowData() {
		m_IndiceName="";
		m_Operator="";
		m_Value1="";
		m_LogOperator="";
		m_Value2="";
		m_IndiceId = "";
		m_IndexTypeId = "";
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData(String indiceName, String operator, String value1,String value2,
			String logOperator, String indiceId, String indexTypeId) {
		m_IndiceName = indiceName;
		m_Operator = operator; 
		m_Value1 = value1;
		m_Value2 = value2;
		m_LogOperator = logOperator;
		m_IndiceId = indiceId;
		m_IndexTypeId = indexTypeId;
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData(String str) {
		StringTokenizer tokens=new StringTokenizer(str,Util.SepChar,false);		
		try {		
		m_IndiceName = tokens.nextToken();
		m_Operator = tokens.nextToken();
		m_Value1=tokens.nextToken();		
		m_LogOperator=Util.getFixedValue(tokens.nextToken(),"");
		m_Value2=tokens.nextToken();
		m_IndiceId=tokens.nextToken();
		m_IndexTypeId=tokens.nextToken();
		}
		catch(Exception ex){
			//System.out.println(" RouteSummaryRowData Exception  " + ex.getMessage() );
		}
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData(String[] data) {
		int i=0;
		m_IndiceName = data[i++];
		m_Operator = data[i++];		
		m_Value1 = data[i++];
		m_LogOperator = data[i++];
		m_Value2 = data[i++];
		m_IndiceId = data[i++];
		m_IndexTypeId = data[i++];
	}
}
//--------------------------------------------------------------------------
class RouteSummaryColumnData {
	public String  m_title;
	float m_width;
	int m_alignment;
	
	public RouteSummaryColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//--------------------------------------------------------------------------
class VWIndicesConditionsTableData extends AbstractTableModel {
	public static final RouteSummaryColumnData m_columns[] = {
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[4],0.18F,JLabel.LEFT ),
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[0],0.3F, JLabel.LEFT),
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[1],0.16F,JLabel.LEFT),
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[2],0.17F,JLabel.LEFT),
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[3],0.17F,JLabel.LEFT),
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[5],0.0F,JLabel.LEFT),
		new RouteSummaryColumnData(VWConstant.RouteConditionsColNames[6],0.0F,JLabel.LEFT),
	};
	public static final int COL_LOGOPERATOR= 0;
	public static final int COL_INDICENAME = 1;
	public static final int COL_OPERATOR = 2;
	public static final int COL_VALUE1= 3;
	public static final int COL_VALUE2 = 4;
	public static final int COL_INDICEID = 5;
	public static final int COL_INDEXTYPEID = 6;
    protected int m_sortCol = 0;
	protected boolean m_sortAsc = true;
	protected VWIndicesConditionsTable m_parent;
	protected Vector m_vector;
	
	public VWIndicesConditionsTableData(VWIndicesConditionsTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}
//	--------------------------------------------------------------------------
	
	
	public void setData(List data) {
		m_vector.removeAllElements();
		if (data==null) return;
		int count =data.size();
		for(int i=0;i<count;i++)
			m_vector.addElement(new RouteSummaryRowData((String)data.get(i)));
	}
//	--------------------------------------------------------------------------
	public void setData(String[][] data) {
		m_vector.removeAllElements();
		int count =data.length;
		for(int i=0;i<count;i++) {
			RouteSummaryRowData row =new RouteSummaryRowData(data[i]);
			if(i==2)
				m_vector.addElement(new Boolean(true));
			else
				m_vector.addElement(row);
		}
	}
//	--------------------------------------------------------------------------
	public String[][] getData() {
		int count=getRowCount();
		String[][] data=new String[count][7];
		for(int i=0;i<count;i++) {
			RouteSummaryRowData row=(RouteSummaryRowData)m_vector.elementAt(i);
			data[i][0]=row.m_IndiceName;
			data[i][1]=row.m_Operator;

			data[i][2]=row.m_Value1;
			data[i][3]=row.m_LogOperator;
			data[i][4]=row.m_Value2;
			data[i][5]=row.m_IndiceId;
			data[i][6]=row.m_IndexTypeId;
		}
		return data;
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		String[] RouteSummaryRowData=new String[7];
		RouteSummaryRowData row=(RouteSummaryRowData)m_vector.elementAt(rowNum);
		RouteSummaryRowData[0]=row.m_IndiceName;
		RouteSummaryRowData[1]=row.m_Operator;

		RouteSummaryRowData[2]=row.m_Value1;
		RouteSummaryRowData[3]=row.m_LogOperator;
		RouteSummaryRowData[4]=row.m_Value2;
		RouteSummaryRowData[5]=row.m_IndiceId;
		RouteSummaryRowData[6]=row.m_IndexTypeId;
		return RouteSummaryRowData;
	}
	
	public RouteSummaryRowData getRowData0(int rowNum) {
		RouteSummaryRowData row=(RouteSummaryRowData)m_vector.elementAt(rowNum);
		return row;
	}
	
//	--------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size();
	}
//	--------------------------------------------------------------------------
	public int getColumnCount() {
		return m_columns.length;
	}
//	--------------------------------------------------------------------------
	public String getColumnName(int column) {
		String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
        return str;
	}
//	--------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return true;
	}
//	--------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		RouteSummaryRowData row = (RouteSummaryRowData)m_vector.elementAt(nRow);		
		switch (nCol) {
		case COL_INDICENAME:	 return (row.m_IndiceName.trim().equals("-"))?"":row.m_IndiceName;
		case COL_OPERATOR:	 return (row.m_Operator.trim().equals("-"))?"":row.m_Operator;
		case COL_VALUE1:     return row.m_Value1 == null ?"":((row.m_Value1.trim().equals("-"))?"":row.m_Value1);
		case COL_LOGOPERATOR:     return (row.m_LogOperator.trim().equals("-"))?"":row.m_LogOperator;
		case COL_VALUE2:     return row.m_Value2 == null ?"":((row.m_Value2.trim().equals("-"))?"":row.m_Value2);
		case COL_INDICEID:     return row.m_IndiceId == null ?"":((row.m_IndiceId.trim().equals("-"))?"":row.m_IndiceId);
		case COL_INDEXTYPEID:     return row.m_IndexTypeId == null ?"":((row.m_IndexTypeId.trim().equals("-"))?"":row.m_IndexTypeId);
		}
		return "";
	}
//	--------------------------------------------------------------------------
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		RouteSummaryRowData row = (RouteSummaryRowData)m_vector.elementAt(nRow);
		String svalue = value.toString().trim().equals("-")?" ":value.toString();
		
		switch (nCol) {
		case COL_INDICENAME:
			row.m_IndiceName = svalue;
			break;
		case COL_OPERATOR:
			row.m_Operator = svalue;
			break;	
			
		case COL_VALUE1:
			row.m_Value1 = svalue;
			break;
		case COL_LOGOPERATOR:
			row.m_LogOperator = svalue;
			break;
		case COL_VALUE2:
			row.m_Value2 = svalue;
			break;
		case COL_INDICEID:
			row.m_IndiceId = svalue;
			break;
		case COL_INDEXTYPEID:
			row.m_IndiceId = svalue;
			break;
		}
	}
//	--------------------------------------------------------------------------
	public void clear() {
		m_vector.removeAllElements();
	}
//	--------------------------------------------------------------------------
	public void insert(RouteSummaryRowData AdvanceSearchRowData) {
		m_vector.addElement(AdvanceSearchRowData);
	}
//	--------------------------------------------------------------------------
	public boolean remove(int row){
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
//	--------------------------------------------------------------------------
	
	 public boolean delete(int row) {
	    if (row < 0 || row >= m_vector.size())
	      return false;
	    m_vector.remove(row);
	    return true;
	  }
	 
	 public boolean deleteAll() {
		 for(int count=0;count<m_vector.size();count++){
			 m_vector.remove(count);
		 }
	    return true;
	  }
	 
	
    class ColumnListener extends MouseAdapter {
        protected VWIndicesConditionsTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWIndicesConditionsTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
        	if(e.getModifiers()==e.BUTTON1_MASK){
                //sortCol(e);
            }
        }
        
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {    	 
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new DocRouteSummaryComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWIndicesConditionsTableData.this));
            m_table.repaint();
        }
   }
    
    
    
    class DocRouteSummaryComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public DocRouteSummaryComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof RouteSummaryRowData) || !(o2 instanceof RouteSummaryRowData))
                return 0;
            RouteSummaryRowData s1 = (RouteSummaryRowData)o1;
            RouteSummaryRowData s2 = (RouteSummaryRowData)o2;
            int result = 0;
            double d1, d2;
            switch (m_sortCol) {           
            	case COL_INDICENAME:
                    result = s1.m_IndiceName.toLowerCase().compareTo(s2.m_IndiceName.toLowerCase());
                    break;
                case COL_OPERATOR:
                   result = s1.m_Operator.toLowerCase().compareTo(s2.m_Operator.toLowerCase());
                   break;  
                case COL_VALUE1:
                    result = s1.m_Value1.toLowerCase().compareTo(s2.m_Value1.toLowerCase());
                    break;  
                case COL_LOGOPERATOR:
                    result = s1.m_LogOperator.toLowerCase().compareTo(s2.m_LogOperator.toLowerCase());
                    break;  
                case COL_VALUE2:
                    result = s1.m_Value2.toLowerCase().compareTo(s2.m_Value2.toLowerCase());
                    break;
                case COL_INDICEID:
                    result = s1.m_IndiceId.toLowerCase().compareTo(s2.m_IndiceId.toLowerCase());
                    break;
                case COL_INDEXTYPEID:
                    result = s1.m_IndexTypeId.toLowerCase().compareTo(s2.m_IndexTypeId.toLowerCase());
                    break;
           }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof DocRouteSummaryComparator) {
            	DocRouteSummaryComparator compObj = (DocRouteSummaryComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    } 
    
    
}
class VWTableCellRenderer extends DefaultTableCellRenderer
{
    public Component getTableCellRendererComponent(JTable table,Object value,
        boolean isSelected,boolean hasFocus,int row,int column) 
    {
    	if(!VWRetentionPanel.ARSFLAG){
	        if(row%2==0)
	            setBackground(new Color(15658734));
	        else
	            setBackground(Color.white);
    	}else{
    		setBackground(Color.white);
    	}
    	return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }

}