package ViewWise.AdminWise.VWRetention;

import java.util.Vector;

import com.computhink.common.DocType;
import com.computhink.common.IndicesCondition;
import com.computhink.common.VWRetention;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWRoom.VWRoom;

public class VWRetentionConnector implements VWConstant {
	
	public static Vector getRetentionSettings(int retentionId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector<VWRetention> resultRetention = new Vector<VWRetention>();
        Vector retention =new Vector();
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getRetentionSettings(room.getId(), retentionId, retention);
            int count = retention.size();
            /**
             * Name, 
             * Id, 
             * DocTypeId, 
             * EnableRetention, 
             * DisposalActionId,
             * MoveDocLocation,  
             * PolicyCreatedDate
             */
            if(retention!=null && retention.size()>0){
            	for(int i=0; i<retention.size(); i++){
            		//System.out.println("retention.get(i).toString() : "+retention.get(i).toString());
	            	resultRetention.add(new VWRetention(retention.get(i).toString()));
            	}
            }
        }
        catch(Exception e){
        }
        finally{AdminWise.adminPanel.setDefaultPointer();
        }
        return resultRetention;
    }
	
	public static Vector getRetentionIndicesCondition(int retentionId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector resultIndicesConditions =new Vector();
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getRetentionIndexInfo(room.getId(), retentionId, resultIndicesConditions);
            /**
             * RetentionId, 
             * DocTypeId, 
             * IndexId, 
             * ComparisonOper, 
             * IndexValue1, 
             * IndexValue2, 
             * LogicalOper
             * Yet to get the id of the indexinfo table and indexName
             */
            
/*            if(resultIndicesConditions!=null && resultIndicesConditions.size()>0){
            	System.out.println("Size is: "+resultIndicesConditions.size());
            	for(int i=0; i<resultIndicesConditions.size(); i++){
            		System.out.println("resultIndicesConditions("+i+") :: "+resultIndicesConditions.get(i));
            	}
            }
*/
        }
        catch(Exception e){
        }
        finally{AdminWise.adminPanel.setDefaultPointer();
        }
        return resultIndicesConditions;
    }
	
	public static int setRetentionSettings(VWRetention retentionBean) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int retentionId = -1;
        Vector retention = new Vector();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return -1;
        retentionId =  AdminWise.gConnector.ViewWiseClient.setRetentionSettings(room.getId(), retentionBean, retention);
        return retentionId;
    }
	public static void setIndexConditions(VWRetention retentionBean) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.setIndexConditions(room.getId(), retentionBean);
    }
	
	public static void delRetentionSettings(VWRetention retentionBean, int deleteOption) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.delRetentionSettings(room.getId(), retentionBean, deleteOption);
        
        //VW_ARS_DelRetentionSettings Parameters required are Retentionid and Delete Flag (0 or 1), 
        //			0 is for updating the deleted column with 1 and 1 is for permanent deletion
        //VW_ARS_DelIndexInfo -- Parameters required are Retentionid, this will delete entries from ARSIndexInfo permanently
    }

	public static int checkRetentionInUse(int retentionId) {
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int retId = -1;
        Vector retention = new Vector();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return -1;
        retId =  AdminWise.gConnector.ViewWiseClient.checkRetentionInUse(room.getId(), retentionId, retention);
        return retId;
	}
	
	public static boolean isUniqueRetentionName(String retentionName, int retentionId){
		boolean result = true;
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		int isExist = AdminWise.gConnector.ViewWiseClient.isRetentionNameFound(room.getId(), retentionName);
		if (isExist == 0) return true;
		if (isExist != retentionId) return false;
		if (retentionId == 0 && isExist > 0) return false;			 
		return result;
	}

	public static void delIndexInfo(VWRetention retentionBean) {
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return;
        AdminWise.gConnector.ViewWiseClient.delIndexInfo(room.getId(), retentionBean);
	}
	
	public static Vector getRetentionCompletedDocs(int retentionId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector resultCompletedDocs=new Vector();
        try
        {
            AdminWise.adminPanel.setWaitPointer();
            AdminWise.gConnector.ViewWiseClient.getRetentionCompletedDocs(room.getId(), retentionId, resultCompletedDocs);
        }
        catch(Exception e){
        }
        finally{AdminWise.adminPanel.setDefaultPointer();
        }
        return resultCompletedDocs;
    }

	public static int getRetentionDocumentList(int retentionId) {
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int retVal = -1;
        Vector retentionDocs = new Vector();
        if(room == null || room.getConnectStatus()!=Room_Status_Connect) return -1;
        AdminWise.gConnector.ViewWiseClient.getRetentionDocumentList(room.getId(), retentionId, retentionDocs);
        if(retentionDocs!=null &&retentionDocs.size()>0){
        	//System.out.println("retentionDocs size is : "+retentionDocs.size());
        	retVal = 1;
        }
        return retVal;
	}
	
	public static Vector getFreezedDocs() {
	    VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
	    Vector resultFreezedDocs=new Vector();
	    try
	    {
		AdminWise.adminPanel.setWaitPointer();
		AdminWise.gConnector.ViewWiseClient.getFreezeDocs(room.getId(),  resultFreezedDocs);
	    }
	    catch(Exception e){
	    }
	    finally{AdminWise.adminPanel.setDefaultPointer();
	    }
	    return resultFreezedDocs;
	}	
	
	public static int CheckNotificationExist(int retentionId){
		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		int ret = 0;
		try{
			AdminWise.gConnector.ViewWiseClient.CheckNotificationExist(room.getId(), retentionId, nodeType_Retention, notify_Retention_Update);
		}catch(Exception ex){
			
		}
		return ret;
	}
}
