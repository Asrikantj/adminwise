package ViewWise.AdminWise.VWRetention;

/**
 *  @author Vijaypriya.B.K
 *  VWRetentionPanel class used to Create/Update/Delete Retention  
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.NumberFormatter;

import org.jhotdraw.framework.DrawingView;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDlgSaveLocation;
import ViewWise.AdminWise.VWFind.VWDlgLocation;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRecycle.VWRecycleConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWRoute.VWAddCustomIndices;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWUtil.VWUtil;

import com.computhink.ars.server.ARSServer;
import com.computhink.common.Constants;
import com.computhink.common.IndicesCondition;
import com.computhink.common.Node;
import com.computhink.common.VWRetention;
import com.computhink.common.util.JTextFieldLimit;

import java.awt.Font;
import com.computhink.vwc.VWCPreferences;
import java.awt.event.ActionEvent;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class VWRetentionPanel extends JPanel implements VWConstant, Constants {
	
	private static final long serialVersionUID = 1L;
	public VWRetentionPanel() {
		setupUI();
	}
	
	private boolean purgeDocsFlag;
	
	public void setupUI() {
		int top = 116; int hGap = 28;
		if (UILoaded)
			return;
		int height = 24, width = 144, left = 12, heightGap = 30;
		setLayout(new BorderLayout());
		String localLanguage=AdminWise.getLocaleLanguage();
		VWRetentionSetting.setLayout(null);
		add(VWRetentionSetting, BorderLayout.CENTER);
		PanelCommand.setLayout(null);
		VWRetentionSetting.add(PanelCommand);
		PanelCommand.setBackground(AdminWise.getAWColor());
		PanelCommand.setBounds(0, 0, 168, 432);
/*		Pic.setIconTextGap(0);
		Pic.setAutoscrolls(true);
		PanelCommand.add(Pic);
		Pic.setBounds(8,4,149,92);*/

	    PanelCommand.add(picPanel);
	    picPanel.setBounds(8,16,149,92);
	        
		lblAvailableRetention.setText(LBL_AVAILABLERETENTIONS);
		PanelCommand.add(lblAvailableRetention);
		lblAvailableRetention.setBackground(java.awt.Color.white);
		lblAvailableRetention.setBounds(left, top, width, height);
		
		top += hGap;
		PanelCommand.add(cmbAvailableRetention);
		cmbAvailableRetention.setBackground(java.awt.Color.white);
		cmbAvailableRetention.setBounds(left-1, top, width + 4, height - 2);
		//cmbAvailableRetention.addItem(new VWRetention(-1,LBL_RETENTION_NAME));
		
		top += hGap + 12;
		BtnNewRetention.setText(BTN_NEWRETENTION_NAME);
		BtnNewRetention.setIcon(VWImages.NewRouteIcon);
		BtnNewRetention.setActionCommand(BTN_NEWRETENTION_NAME);
		PanelCommand.add(BtnNewRetention);
		BtnNewRetention.setBounds(left, top, width, height);
		BtnNewRetention.setVisible(true);
		//BtnNewRetention.setIcon(VWImages.DocLockIcon);
		
		/**
	     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
	     */
		BtnProgressClick.setText("");
		PanelCommand.add(BtnProgressClick);
		BtnProgressClick.setBounds(left, top, width, height);
		BtnProgressClick.setVisible(false);
		BtnProgressClick.addActionListener(new SymAction());
		/////////////////////////////////////////////
		
		top += hGap;
		BtnUpdate.setText(BTN_UPDATE_NAME);
		BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
		PanelCommand.add(BtnUpdate);
		BtnUpdate.setBounds(left, top, width, height);
		BtnUpdate.setIcon(VWImages.DocLockIcon);
		
		top += hGap;
		BtnDelete.setText(BTN_DELETE_NAME);
		BtnDelete.setActionCommand(BTN_DELETE_NAME);
		PanelCommand.add(BtnDelete);
		BtnDelete.setBounds(left, top, width, height);
		BtnDelete.setIcon(VWImages.DelIcon);
		
		top += hGap;
		BtnSaveAs.setText(BTN_SAVE_AS_NAME);
		BtnSaveAs.setActionCommand(BTN_SAVE_AS_NAME);
		PanelCommand.add(BtnSaveAs);
		BtnSaveAs.setBounds(left, top, width, height);
		BtnSaveAs.setIcon(VWImages.SaveAsIcon);
		
		
		top += hGap + 12;
		BtnRetentionAddIndex.setText(BTN_ADDINDEX_NAME);
		BtnRetentionAddIndex.setActionCommand(BTN_ADDINDEX_NAME);
		PanelCommand.add(BtnRetentionAddIndex);
		BtnRetentionAddIndex.setBounds(left, top, width, height);
		BtnRetentionAddIndex.setIcon(VWImages.CustomReportIcon);
		BtnRetentionAddIndex.setEnabled(false);
		
		top += hGap + 12;
		BtnRetentionCompletedDocs.setText(BTN_COMPLETEDDOCS_NAME);
		BtnRetentionCompletedDocs.setActionCommand(BTN_COMPLETEDDOCS_NAME);
		PanelCommand.add(BtnRetentionCompletedDocs);
		BtnRetentionCompletedDocs.setBounds(left, top, width, height);
		BtnRetentionCompletedDocs.setIcon(VWImages.SummaryIcon);
		BtnRetentionCompletedDocs.setEnabled(false);
		
		top += hGap + 12;
		if(localLanguage.equals("nl_NL")){
        	String str=BTN_FREEZEDOCS_NAME.substring(0,12);
            String srt1=BTN_FREEZEDOCS_NAME.substring(12,BTN_FREEZEDOCS_NAME.length());
            BtnFreezeDocs.setText("<html><br>"+str+"<br>"+srt1+"</html>");
            BtnFreezeDocs.setFocusable(false);
		}else
		BtnFreezeDocs.setText(BTN_FREEZEDOCS_NAME);
		BtnFreezeDocs.setActionCommand(BTN_FREEZEDOCS_NAME);
		PanelCommand.add(BtnFreezeDocs);
		if(localLanguage.equals("nl_NL")){
			BtnFreezeDocs.setBounds(left, top, width, height+15);
		}else
		BtnFreezeDocs.setBounds(left, top, width, height);
		BtnFreezeDocs.setIcon(VWImages.SummaryIcon);
		BtnFreezeDocs.setEnabled(false);
		
		top += heightGap;
		PanelTable.setLayout(new BorderLayout());
		VWRetentionSetting.add(PanelTable);
		PanelTable.setBounds(168, 0, 700,500);
		SPTable.setBounds(0, 0, 902, 659);
		SPTable.setOpaque(true);
		PanelTable.add(SPTable, BorderLayout.CENTER);
		SPTable.getViewport().add(Table);
		Table.getParent().setBackground(java.awt.Color.white);
		
		//VP Comment
		setEnableMode(MODE_UNCONNECT);

		initRetention();
		PanelRetentionSetting.setVisible(false);
		PanelRetentionSetting.setEnabled(false);
		
		SymComponent aSymComponent = new SymComponent();
		VWRetentionSetting.addComponentListener(aSymComponent);
		Pic.setIcon(VWImages.DocRetentionImage);
		SymAction lSymAction = new SymAction();
		
		BtnNewRetention.addActionListener(lSymAction);
		BtnDelete.addActionListener(lSymAction);
		BtnSaveAs.addActionListener(lSymAction);
		BtnUpdate.addActionListener(lSymAction);
		BtnRetentionCompletedDocs.addActionListener(lSymAction);
		BtnRetentionAddIndex.addActionListener(lSymAction);
		BtnFreezeDocs.addActionListener(lSymAction);
		cmbDisposalActions.addActionListener(lSymAction);
		BtnBrowse.addActionListener(lSymAction);
		BtnLocation.addActionListener(lSymAction);
		btnRetentionLocation.addActionListener(lSymAction);
		BtnProgressClick.addActionListener(lSymAction);
		
		Table.getParent().setBackground(java.awt.Color.white);
		PanelRetentionSetting.setAutoscrolls(true);
		PanelRetentionSetting.setPreferredSize(new Dimension(900, 640));	
		//Scrollbar was not displaying.
		//Table.setPreferredSize(new Dimension(900,640));
		SPTable.setPreferredSize(new Dimension(700, 500));
		repaint();
		doLayout();		
		UILoaded = true;
	}
	
	private void updatePurgeDocsRegistry(){
		try{
			Vector purgeDocsFlagVec = new Vector();
		    int ret = VWConnector.ViewWiseClient.getPurgeDocs(gCurRoom, purgeDocsFlagVec);
		    if(purgeDocsFlagVec!=null && purgeDocsFlagVec.size()>0){
		    	String flag = purgeDocsFlagVec.get(0).toString();
		    	if(flag.equalsIgnoreCase("true"))
		    		purgeDocsFlag = true;
		    	else
		    		purgeDocsFlag = false;
		    }
			}catch(Exception ex){
				//System.out.println("Exception is : "+ex.getMessage());
			}	
			cmbDisposalActions.removeAllItems();
			//ARSActionsWithoutPurge
			if(purgeDocsFlag){
				cmbDisposalActions.addItems(ARSActions);
			}
			else{
				cmbDisposalActions.addItems(ARSActionsWithoutPurge);
			}
			
	}
	
	private void initRetention() {
		int height = 20, width = 100, left = 0, top = 0, txtWidth = 400, heightGap = 30, leftGap = 60;		
		PanelRetentionSetting.setLayout(null);
		PanelRetentionSetting.setBounds(0, 0, 700, 700);
		PanelTable.add(PanelRetentionSetting, BorderLayout.CENTER);

		PanelRetentionSetting.setBackground(Color.WHITE);
		left = 27;
		top = 85;
		leftGap = 27;
		
		PanelRetentionSetting.add(lblRetentionName);
		lblRetentionName.setBounds(55, 30, width, height);
		lblRetentionName.setText(LBL_RETENTIONNAME);
		
		PanelRetentionSetting.add(txtRetentionName);
		txtRetentionName.setBounds(155, 30, txtWidth-46, height);;
		txtRetentionName.setDocument(new JTextFieldLimit(50));
		txtRetentionName.setToolTipText(AdminWise.connectorManager.getString("VWRetentionPanel.txtRetentionName"));
		txtRetentionName.setBackground(java.awt.Color.white);
		
		PanelRetentionSetting.add(lblRetentionLocation);
		lblRetentionLocation.setBounds(55, 67, width, height);
		lblRetentionLocation.setText(LBL_RETENTIONLOCATION);
		
		PanelRetentionSetting.add(txtRetentionLocation);
		left = left + width + 28;
		txtRetentionLocation.setBounds(left, 67, txtWidth-74, height);
		txtRetentionLocation.setDocument(new JTextFieldLimit(1000));
		//txtRetentionLocation.setToolTipText(AdminWise.connectorManager.getString("VWRetentionPanel.txtRetentionName"));
		txtRetentionLocation.setBackground(java.awt.Color.white);
		txtRetentionLocation.setEditable(false);
		
		btnRetentionLocation.setBounds(487, 67, 24, height);
		btnRetentionLocation.setBorder(getBorder());
		btnRetentionLocation.setBackground(java.awt.Color.white);
        btnRetentionLocation.setIcon(VWImages.BrowseRouteImage);
		PanelRetentionSetting.add(btnRetentionLocation);
		btnRetentionLocation.setVisible(true);
		
		
		SymAction lSymAction = new SymAction();
		cmbAvailableRetention.addActionListener(lSymAction);
		left = leftGap;
		top = top + heightGap;
		try {
			JPanel indicesConditionPanel = (JPanel)indexConditionPanel.getIndicesCondition();
			indicesConditionPanel.setBounds(45, 100, 700, 220);
			indicesConditionPanel.setBackground(java.awt.Color.WHITE);
			PanelRetentionSetting.add(indicesConditionPanel);
		} catch (Exception e) {
			//System.out.println("Exception : "+e.toString()+"\n\n");
			//JOptionPane.showMessageDialog(null,e.toString());
		}
		
		left = leftGap;
		top = top + heightGap;
		
		PanelRetentionSetting.add(lblNotifyBefore1);
		lblNotifyBefore1.setBounds(55, 327, 172, height);
		lblNotifyBefore1.setText(LBL_NOTIFY_BEFORE1);
		lblNotifyBefore1.setVisible(true);
		PanelRetentionSetting.add(TxtNotifyBefore);
		TxtNotifyBefore.setBounds(180, 327, 30, height);
		TxtNotifyBefore.setVisible(true);
		TxtNotifyBefore.setBackground(java.awt.Color.white);
		PanelRetentionSetting.add(lblNotifyBefore2);
		lblNotifyBefore2.setBounds(215, 327, 30, height);
		lblNotifyBefore2.setText(LBL_NOTIFY_BEFORE2);
        
		
		chkSendMail.setBounds(55,357,300,20);
		PanelRetentionSetting.add(chkSendMail);
		chkSendMail.setVisible(true);
		chkSendMail.setBackground(java.awt.Color.white);
		spEMailIds.setBounds(55,377,450,67);
		taEMailIds.setToolTipText(AdminWise.connectorManager.getString("VWRetentionPanel.taEMailIds"));
		taEMailIds.setDocument(new JTextFieldLimit(255));
		PanelRetentionSetting.add(spEMailIds);
		spEMailIds.setVisible(true);	
		
        
		PanelRetentionSetting.add(lblAction);
		lblAction.setBounds(55, 459, 400, height);
		lblAction.setText(LBL_RETENTON_ACTION);
		
		left = leftGap;
		top = top + heightGap;
		PanelRetentionSetting.add(cmbDisposalActions);
		cmbDisposalActions.setBackground(java.awt.Color.white);
		cmbDisposalActions.setBounds(55, 479, 450, height);
		
		//ARSActionsWithoutPurge
		if(purgeDocsFlag){
			cmbDisposalActions.addItems(ARSActions);
		}
		else{
			cmbDisposalActions.addItems(ARSActionsWithoutPurge);
		}
		
		JLabel5.setText(LBL_MOVETO);
		PanelRetentionSetting.add(JLabel5);
        JLabel5.setBounds(55, 509, 144, height);
        JLabel5.setVisible(false);

        PanelRetentionSetting.add(TxtMovePath);
        TxtMovePath.setBounds(55, 529, 280, height);
        TxtMovePath.setVisible(false);
        TxtMovePath.setBackground(java.awt.Color.white);

        PanelRetentionSetting.add(BtnLocation);
        BtnLocation.setBounds(345, 529, 24, height);
        BtnLocation.setBackground(java.awt.Color.white);
        BtnLocation.setVisible(false);
        BtnLocation.setEnabled(true);
        BtnLocation.setIcon(VWImages.BrowseRouteImage);

        PanelRetentionSetting.add(BtnBrowse);
        BtnBrowse.setBounds(345,529,24, height);
        BtnBrowse.setBackground(java.awt.Color.white);
        BtnBrowse.setVisible(false);
        BtnBrowse.setEnabled(true);
        BtnBrowse.setIcon(VWImages.BrowseRouteImage);
		
		Border borderBtm = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
		retentionLineBorderbottom.setBorder(borderBtm);
		PanelRetentionSetting.add(retentionLineBorderbottom);
		
		chkEnableRetention.setText(AdminWise.connectorManager.getString("VWRetentionPanel.chkEnableRetention"));
		chkEnableRetention.setToolTipText(CHK_RETENTION_POLICYE);
		PanelRetentionSetting.add(chkEnableRetention);
        chkEnableRetention.setBackground(java.awt.Color.white);
        chkEnableRetention.addActionListener(lSymAction);
        viewEmailUI(true);
	}
	
	private void loadAvailableRetention(){
		retentionLoaded = false;
		try{
			Vector vRetentionList = new Vector();
			VWRetention retBean = null;
			vRetentionList = VWRetentionConnector.getRetentionSettings(0);
			if (vRetentionList != null && vRetentionList.size()>0) {
				cmbAvailableRetention.removeAllItems();
				cmbAvailableRetention.addItem(new VWRetention(-1,LBL_RETENTION_NAME));
				retBean = new VWRetention(vRetentionList.size());
				for (int i = 0; i < vRetentionList.size(); i++){
					retBean = (VWRetention)vRetentionList.get(i);
					cmbAvailableRetention.addItem(retBean);
				}
				cmbAvailableRetention.addItem(new VWRetention(-2,LBL_REFRESH_NAME));
				CURRENT_MODE = 2;
			} else {
				cmbAvailableRetention.removeAllItems();
				cmbAvailableRetention.addItem(new VWRetention(-1,LBL_RETENTION_NAME));
			}
		}catch(Exception ex) {
			
		}
		retentionLoaded = true;
	}
	
	private Vector sortRetentionList(Vector retentionList) {
		Vector retentionVector = new Vector();
		try {
			VWRetention retInfo[] = new VWRetention[retentionList.size()];
			for (int count = 0; count < retentionList.size(); count++) {
				retInfo[count] = (VWRetention) retentionList.get(count);
			}
			Arrays.sort(retInfo);
			for (int count = 0; count < retInfo.length; count++) {
				retentionVector.add(retInfo[count]);
			}
		} catch (Exception e) {
			// JOptionPane.showMessageDialog(null,e.toString());
		}
		return retentionVector;
	}
	
	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnNewRetention) {
				BtnNewRetention_actionPerformed(event);
			} else if (object == BtnUpdate) {
				BtnUpdate_actionPerformed(event);
			} else if (object == BtnDelete) {
				BtnDelete_actionPerformed(event);
			} else if (object == cmbAvailableRetention) {
				CmbAvailableRetention_actionPerformed(event);
			} else if (object == cmbDisposalActions) {
            	CmbDisposalActions_actionPerformed(event);
			}else if (object == chkEnableRetention){
				ChkEnableRetention_actionPerformed(event); //For this retention it has no action
			}else if (object == BtnBrowse){
				BtnBrowse_actionPerformed(event);
			}else if (object == BtnLocation){
				BtnLocation_actionPerformed(event);
			}else if (object == BtnSaveAs){
				BtnSaveAs_actionPerformed(event);
			}else if (object == BtnRetentionCompletedDocs){
				BtnRetentionCompletedDocs_actionPerformed(event);
			}else if (object == BtnRetentionAddIndex){
				BtnRetentionAddIndex_actionPerformed(event);
			}else if (object == BtnFreezeDocs){
				BtnFreezedDocs_actionPerformed(event);
			}else if (object == btnRetentionLocation){
				BtnRetentionLocation_ActionPerformed(event);
			} else if(object==BtnProgressClick){
				BtnProgressClick_actionPerformed(event);
			}
		}
	}
	
	 private void stopGridEdit() {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
    }
	
	void BtnNewRetention_actionPerformed(java.awt.event.ActionEvent event) {
		

        if(BtnNewRetention.getText().equals(BTN_NEWRETENTION_NAME)) {
        	selIndicesCount = 0;
        	indexFlag = false;
        	retentionBean.setId(0);
        	selRetentionId = -1;
        	CURRENT_MODE = 1;
        	clearAll();
        	indexConditionPanel.enableARSDateSettings();
            stopGridEdit();
            SPTable.getViewport().add(PanelRetentionSetting);
            PanelRetentionSetting.setVisible(true);
            inUpdateMode = false;
            if(TxtNotifyBefore.getText().trim().length()!=0){
            	TxtNotifyBefore.setText("");	
            }
            setEnableMode(MODE_NEW);
        }
        else if(BtnNewRetention.getText().equals(BTN_SAVE_NAME)) {
            try{
                AdminWise.adminPanel.setWaitPointer();
                stopGridEdit();
                if (gCurRoom != 0) {
    				if (!validateRetentionData())
    					return;
    				if(!txtRetentionLocation.getText().equalsIgnoreCase(""))
    					retentionBean.setRetentionLocationId(String.valueOf(parentNodeId));
    				else
    					retentionBean.setRetentionLocationId("0");
    				retentionBean.setName(txtRetentionName.getText());
	                int docTypeId = indexConditionPanel.getDocTypeId();
	                retentionBean.setDocTypeId(docTypeId);
	
	                if(chkEnableRetention.isSelected())
	                	retentionBean.setStatus(1);
	                else
	                	retentionBean.setStatus(0);
	                
	                int DisposalActionID = ((VWRecord)cmbDisposalActions.getSelectedItem()).getId();
	                retentionBean.setActionId(DisposalActionID);
	                
	                //String actionLocation = TxtMovePath.getText();
	                //MoveLocation is changed to nodeId of the selected tree path.
	                String actionLocation  = "";
	                if(DisposalActionID == 1){
	                	actionLocation = String.valueOf(nodeId);
	                }else{
	                	actionLocation = TxtMovePath.getText();
	                }
	                /**
	                 * Update the location column only for Move, Generate Report and Backup final action
	                 */
	                if(actionLocation.length()!=0 && (DisposalActionID == 1 || DisposalActionID == 2 || DisposalActionID == 5)){
	                	retentionBean.setLocation(actionLocation);
	                }
	                else
	                	retentionBean.setLocation("-");
	                
	                String [][] conditionTableData = indexConditionPanel.getData();
	                
	                int indicesConditionCount = conditionTableData.length;
	                Vector<IndicesCondition> indexConditionVector = new Vector<IndicesCondition>();
	                boolean flagNotify = false;
	                for(int i=0; i<indicesConditionCount; i++){
	                	int j = 0;
	                	indicesConditionBean = new IndicesCondition(100);
	                	indicesConditionBean.setIndexName(conditionTableData[i][j++]);
	                	indicesConditionBean.setComparisonOpert(conditionTableData[i][j++]);
	                	indicesConditionBean.setIndexValue1(conditionTableData[i][j++]);
	                	indicesConditionBean.setLogicalOpert(conditionTableData[i][j++]);
	                	indicesConditionBean.setIndexValue2(conditionTableData[i][j++]);
	                	indicesConditionBean.setIndexId(Integer.parseInt(conditionTableData[i][j++]));
	                	indicesConditionBean.setIndexTypeId(Integer.parseInt(conditionTableData[i][j++]));

	                	//If Date datatype is not exist in the retention criteria clear the value for notify before field.
	                	if(!flagNotify && indicesConditionBean.getIndexTypeId()==2){
	                		flagNotify = true;
	                	}
	                	j=0;
	                	indexConditionVector.add(indicesConditionBean);
	                }
	                
	                retentionBean.setIndicesCondition(indexConditionVector);
	    			
	                if(selRetentionId!=-1){
	                	/*
	                	 * This delRetentionSettings method is calling the stored procedure VW_ARS_DelRetentionSettings
	                	 * The parameters passing to this sp is retentionId and 1 
	                	 * (1 is for deleting the complete retention which intern delete the index info) 
	                	 * (0 for updating the delete column to 1)
	                	 */
	                	retentionBean.setId(selRetentionId);
	                	/*int deleteOption = 1; // For Permenent delete - not in use
	                	VWRetentionConnector.delRetentionSettings(retentionBean, deleteOption);*/
	                }
	                if(!flagNotify){
	                	TxtNotifyBefore.setText("");
	                }
	                
	                int NotifyBefore = 0;
	                int textLength = TxtNotifyBefore.getText().trim().length();
	                if(TxtNotifyBefore.getText()!="" && textLength!=0)
	                	NotifyBefore = Integer.valueOf(TxtNotifyBefore.getText());
	                retentionBean.setNotifyBefore(NotifyBefore);

	                retentionBean.setReportLocation(actionLocation);//Same as move location for email
	                
	                int sendMail = 0;
	                if(chkSendMail.isSelected()){
	                	sendMail = 1;
	                }else{
	                	sendMail = 0;
	                }
	                retentionBean.setSendMail(sendMail);
	                
	                String emailId = ""; 
	                if(taEMailIds.isVisible()){
	                	emailId = (taEMailIds.getText());
	                }
	                retentionBean.setMailIds(emailId);
	                
/*	                int retentionID = VWRetentionConnector.setRetentionSettings(retentionBean);
	    			retentionBean.setId(retentionID);
	    			if(selRetentionId!=-1){//For Update
	    				VWRetentionConnector.delIndexInfo(retentionBean);
	    			}
*/	    			
/*
 * Calling the following procedure to update retention
 */
	                if (retentionBean.getId() > 0){//update retention	                	
	                	if (!retentionBean.isSameObject(selRetention)){ //if any changes in retention    	                	
	                		VWRetentionConnector.delRetentionSettings(retentionBean, 0);
	                		/*
	                		 * Notification enhancement.
	                		 * If any notification is exist then send the retention id for setRetentionSettings, else set it to zero.
	                		 */
	                		int settings = VWRetentionConnector.CheckNotificationExist(retentionBean.getId());
	                		if(settings<0)
	                			retentionBean.setId(0);
	                		int retentionID = VWRetentionConnector.setRetentionSettings(retentionBean);
	                		retentionBean.setId(retentionID);
	                		VWRetentionConnector.setIndexConditions(retentionBean);
	                	}else{//no changes only update the 
	                		int retentionID = VWRetentionConnector.setRetentionSettings(retentionBean);
	                	}
	                }else if (retentionBean.getId() == 0){//new retention
	                	int retentionID = VWRetentionConnector.setRetentionSettings(retentionBean);
	                	retentionBean.setId(retentionID);
	                	VWRetentionConnector.setIndexConditions(retentionBean);	                	
	                }
	    			
	    			cmbAvailableRetention.setSelectedIndex(0);
					SPTable.getViewport().add(Table);
					PanelRetentionSetting.setVisible(false);
                }
				setEnableMode(MODE_CONNECT);
				CURRENT_MODE = 0;
            }
            catch(Exception e){
            	e.printStackTrace();
            	//System.out.println("Exception is : "+e.getMessage());
            }
            finally{AdminWise.adminPanel.setDefaultPointer();}
        }
    }
	void BtnUpdate_actionPerformed(java.awt.event.ActionEvent event) {
		if (BtnUpdate.getText().equals(BTN_UPDATE_NAME)) {
			boolean checkFlag = canWeUpdateDelete();
			if (!checkFlag) return;
			inUpdateMode = true;
			CURRENT_MODE = 3;
			setEnableMode(MODE_UPDATE);
		} else if (BtnUpdate.getText().equals(BTN_CANCEL_NAME)) {
			inUpdateMode = false;
			SPTable.getViewport().add(Table);
			cmbAvailableRetention.setSelectedIndex(0);
			PanelRetentionSetting.setVisible(false);
			clearAll();
			CURRENT_MODE = 0;
			setEnableMode(MODE_CONNECT);
		}
	}
	
	void BtnDelete_actionPerformed(java.awt.event.ActionEvent event) {
		int deleteOption = 1; // Sending 1 for delete operation. If there are no documents then do permanent delete else mark update column to 1 
		int selectedIndex = cmbAvailableRetention.getSelectedIndex();
		
		boolean checkFlag = canWeUpdateDelete();
		if (!checkFlag) return;
		if (selectedIndex > 0) {
			VWRetention retentionObj = new VWRetention(0);
			retentionObj  = (VWRetention)cmbAvailableRetention.getSelectedItem();

			int retVal = VWMessage.showConfirmDialog(AdminWise.adminFrame,
					AdminWise.connectorManager.getString("VWRetentionPanel.msg_1"));
			if (retVal == 0) {
				VWRetentionConnector.delRetentionSettings(retentionObj, deleteOption);
				setEnableMode(MODE_CONNECT);
				
				stopGridEdit();
	            SPTable.getViewport().add(Table);
				loadAvailableRetention();	
			}
		}
	}
	
	private void CmbAvailableRetention_actionPerformed(ActionEvent event) {
		if(!retentionLoaded) return;
        try{
            AdminWise.adminPanel.setWaitPointer();
            BtnRetentionAddIndex.setEnabled(false);
            selIndicesCount = 0;
        	indexFlag = false;
            loadRetentionData();
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
	}
	/*
     * Disposal method drow down actions
     */
    void CmbDisposalActions_actionPerformed(java.awt.event.ActionEvent event){

    	VWRecord selObject=(VWRecord)cmbDisposalActions.getSelectedItem();
    	
    	if(selObject !=null && selObject.getId()==0){
        	viewEmailUI(false);
        }else if(selObject != null && selObject.getId() == 1 ){
        	BtnLocation.setVisible(true);
        	BtnLocation.setEnabled(true);
	       	BtnBrowse.setVisible(false);
        	BtnBrowse.setEnabled(false);
        	JLabel5.setVisible(true);
        	JLabel5.setText(LBL_MOVETO);
        	TxtMovePath.setVisible(true);
        	TxtMovePath.setText("");
        	TxtMovePath.setEnabled(false);
        	viewEmailUI(true);
        }else if(selObject != null && selObject.getId() == 2 ){// This is for generate report
        	BtnLocation.setVisible(false);
        	BtnLocation.setEnabled(false);
        	BtnBrowse.setVisible(true);
        	BtnBrowse.setEnabled(true);
        	JLabel5.setVisible(true);
        	JLabel5.setText(LBL_SAVEIN);
        	TxtMovePath.setVisible(true);
        	TxtMovePath.setText("");
        	//For adding UNC path it is made as enabled
        	TxtMovePath.setEnabled(true);
        	viewEmailUI(true);
        }else if(selObject != null && selObject.getId() == 5 ){
        	BtnLocation.setVisible(false);
        	BtnLocation.setEnabled(false);
        	BtnBrowse.setVisible(true);
        	BtnBrowse.setEnabled(true);
        	JLabel5.setVisible(true);
        	JLabel5.setText(LBL_MOVETO);
        	TxtMovePath.setVisible(true);
        	TxtMovePath.setText("");
        	//For adding UNC path it is made as enabled
        	TxtMovePath.setEnabled(true);
        	//TxtMovePath.setEnabled(false);
        	viewEmailUI(true);
        }else if(selObject !=null && (selObject.getId()==3 || selObject.getId()==4)){
        	viewEmailUI(false);
        }else{
        	BtnLocation.setVisible(false);
        	BtnLocation.setEnabled(false);
        	BtnBrowse.setVisible(false);
        	BtnBrowse.setEnabled(false);
        	TxtMovePath.setVisible(false);
        	TxtMovePath.setEnabled(false);
        	JLabel5.setVisible(false);
        	viewEmailUI(true);
        }
    }
    
    /*
     * This method is used to enable the email UI settings
     */
    private void viewEmailUI(boolean flag) {
    	if(flag){
    		retentionLineBorderbottom.setBounds(55, 569, 457, 2);
    		chkEnableRetention.setBounds(55, 579, 155, 20);
    	}else{
    		JLabel5.setVisible(false);
    		TxtMovePath.setVisible(false);
    		BtnBrowse.setVisible(false);
    		BtnLocation.setVisible(false);
    		retentionLineBorderbottom.setBounds(55, 514, 457, 2);
    		chkEnableRetention.setBounds(55, 524, 155, 20);
    	}
	}

	void ChkEnableRetention_actionPerformed(java.awt.event.ActionEvent event)
    {
		
		String options[]={"Yes", "No"};
		if(chkEnableRetention.isSelected()){		
			int result =  JOptionPane.showOptionDialog(this, AdminWise.connectorManager.getString("VWRetentionPanel.msg_2"), AdminWise.connectorManager.getString("Administration.Title"), 
	                 JOptionPane.YES_NO_OPTION, 
	                 JOptionPane.QUESTION_MESSAGE, 
	                 null, options, "Yes");
			
			if(result == 0)
				chkEnableRetention.setSelected(true);
			else
				chkEnableRetention.setSelected(false);
		}
    	
        /*if(chkEnableRetention.isSelected())
        {
        	cmbDisposalActions.setEnabled(true);
            cmbDisposalActions.removeAllItems();
    		cmbDisposalActions.addItems(ARSActions);
        }
        else
        {
            cmbDisposalActions.removeAllItems();
    		cmbDisposalActions.addItems(ARSActions);
            cmbDisposalActions.setEnabled(false);
        }*/
    }
    void BtnBrowse_actionPerformed(java.awt.event.ActionEvent event)
    {
		loadSelectFolderDialog();
 		//loadSaveDialog();
	}
    /*
     * To get the tree structure
     */
	void BtnLocation_actionPerformed(java.awt.event.ActionEvent event)
    {
		if(dlgLocation != null)
		{
		    dlgLocation.loadDlgData();
		    if(nodeId!=dlgLocation.tree.getSelectedNodeId())
		    	dlgLocation.tree.setSelectionPath(nodeId,true);
		  	dlgLocation.setVisible(true);
		}
		else
		{
		    dlgLocation = new VWDlgSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWRetentionPanel.dlgLocation"));
		    dlgLocation.tree.setSelectionPath(nodeId,true);
		    dlgLocation.setVisible(true);
		}
		if(dlgLocation.cancelFlag) return;

		TxtMovePath.setText(dlgLocation.tree.getSelectedNodeStrPath());
		nodeId=dlgLocation.tree.getSelectedNodeId();
	}
	
	void BtnSaveAs_actionPerformed(ActionEvent event) {
		try {
			if (Table.isEditing()) {
				Table.getCellEditor().cancelCellEditing();
			}
			CURRENT_MODE = 4;// save as
			if(!PanelRetentionSetting.isVisible())
				PanelRetentionSetting.setVisible(true);
			SPTable.getViewport().add(PanelRetentionSetting);
			int selectedIndex = cmbAvailableRetention.getSelectedIndex();
			if (selectedIndex > 0) {
				VWRetention rentention = (VWRetention) cmbAvailableRetention.getSelectedItem();
				viewMode(false);
				selRetentionId = 0;
				retentionBean.setId(selRetentionId);
				txtRetentionName.setFocusable(true);
				setEnableMode(MODE_UPDATE);
			}
			
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception ... BtnSaveAs_actionPerformed "
					+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
	}
	
	void BtnFreezedDocs_actionPerformed(ActionEvent ecent){
	    Table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    Vector retFreezedDocs = new Vector();
	    try{
	    	BtnUpdate.setEnabled(false);
	    	BtnDelete.setEnabled(false);
	    	BtnSaveAs.setEnabled(false);
	    	BtnRetentionCompletedDocs.setEnabled(false);
	    	cmbAvailableRetention.setSelectedIndex(0);
	    	
	    	retFreezedDocs = VWRetentionConnector.getFreezedDocs();
	    	Table.addData(retFreezedDocs);
	    	Table.m_data.fireTableDataChanged();
	    	/*	if(retFreezedDocs!=null && retFreezedDocs.size()>0){
		    		BtnFreezeDocs.setEnabled(false);
				}
	    	 */
	    	SPTable.getViewport().add(Table);
	    	PanelRetentionSetting.setVisible(false);
	    } catch (Exception ex) {
	    	AdminWise.printToConsole("Exception ... BtnFreezedDocs_actionPerformed  "+ ex.getMessage());
	    	setEnableMode(MODE_CONNECT);
	    }		
	}
	void BtnRetentionCompletedDocs_actionPerformed(ActionEvent event) {
		if(cmbAvailableRetention.getSelectedItem().toString().trim().equalsIgnoreCase("<Available "+ Constants.WORKFLOW_MODULE_NAME +"s>"))
			return;
		Table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Vector retCompletedDocs = new Vector();
		try {
			if(!cmbAvailableRetention.getSelectedItem().toString().trim().equalsIgnoreCase("<Available "+ Constants.WORKFLOW_MODULE_NAME +"s>")){
				BtnUpdate.setEnabled(false);
				BtnSaveAs.setEnabled(false);
				VWRetention selRetention = (VWRetention) cmbAvailableRetention.getSelectedItem();
				int retentionId = selRetention.getId();
				globalRetentionId=retentionId;
				retCompletedDocs = VWRetentionConnector.getRetentionCompletedDocs(retentionId);
				Table.addData(retCompletedDocs);
				Table.m_data.fireTableDataChanged();

				if(retCompletedDocs!=null && retCompletedDocs.size()>0){
					BtnRetentionCompletedDocs.setEnabled(false);
					BtnRetentionAddIndex.setEnabled(true);
				} else {
					BtnRetentionAddIndex.setEnabled(false);
				}
				SPTable.getViewport().add(Table);
				PanelRetentionSetting.setVisible(false);
			}
		} catch (Exception ex) {
			AdminWise.printToConsole("Exception ... BtnRouteCompletedDocuments_actionPerformed "+ ex.getMessage());
			setEnableMode(MODE_CONNECT);
		}
	}
	
	/**
	 * @author apurba.m
	 * @param event
	 * CV10.1 Enhancement: Added for opening pop up with all indices list.
	 */
	void BtnRetentionAddIndex_actionPerformed(ActionEvent event) {
		lock1.lock();
		BtnProgressClick.doClick();
		Vector newCustomIndexList = new Vector();
    	Vector finalCustomList = new Vector();
    	AdminWise.adminPanel.setWaitPointer();
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(AdminWise.adminPanel.confirmDelRecycleDoc)
        try{
        	Vector data = new Vector();
        	Vector settingsValue= new Vector();
        	Vector customIndexList= AdminWise.gConnector.ViewWiseClient.getCustomList(room.getId(), 4, settingsValue);
        	if (customIndexList.size() > 0) {
				String str= "";
				str= str+customIndexList.get(0).toString();
				String[] st1= str.split("\t");
				for(int i=1; i<st1.length;i++){
					newCustomIndexList.add(st1[i]);
				}
				finalCustomList = newCustomIndexList;
			} else {
				finalCustomList= null;
			}
        	data=VWRetentionConnector.getRetentionCompletedDocs(globalRetentionId);
        	VWAddCustomIndices dynamicUserIndexDlg = new VWAddCustomIndices(AdminWise.adminFrame, "", "RP",data,globalRetentionId,Table,finalCustomList,processDialog);
        }
        catch(Exception e){}        
        finally{
            AdminWise.adminPanel.setDefaultPointer();            
//            hideProgress();
        }
	}
	
	/**
     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
     */
	void BtnProgressClick_actionPerformed(ActionEvent event){
		try{
			lock1.lock();
			statusLabel.paintImmediately(this.getBounds());
			statusLabel.setVisible(true);
			statusLabel.setLayout(new BorderLayout());
			String fontName="Arial";
			if (VWCPreferences.getFontName().trim().length()> 0)
				fontName = VWCPreferences.getFontName();
			statusLabel.setFont(new Font(fontName, Font.PLAIN, 14));
			statusLabel.validate();
			/*if(scroll.isVisible()==true)
			processDialog.setModal(true);
			else 
				processDialog.setModal(false);*/
			processDialog.getContentPane().add(statusLabel, BorderLayout.CENTER);
			processDialog.setLocationRelativeTo(null);
			processDialog.setTitle("Workflow Report");
			processDialog.setAlwaysOnTop(true);
			processDialog.setSize(390,70);
			processDialog.setResizable(false);
			processDialog.setDefaultCloseOperation(processDialog.DO_NOTHING_ON_CLOSE);
			processDialog.setVisible(true);
			processDialog.setOpacity(1);
			//processDialog.pack();
			lock1.unlock();
			//processDialog.repaint();			

		}catch(Exception e){
			AdminWise.printToConsole("Exception while reviewUpdate action::::"+e.getMessage());
		}
	}
	
	void BtnRetentionLocation_ActionPerformed(ActionEvent event){
		if(dlgLocation != null)
		{
		    dlgLocation.loadDlgData();
		    if(nodeId!=dlgLocation.tree.getSelectedNodeId())
		    	dlgLocation.tree.setSelectionPath(nodeId,true);
		  	dlgLocation.setVisible(true);
		}
		else
		{
		    dlgLocation = new VWDlgSaveLocation(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWRetentionPanel.dlgLocation"));
		    dlgLocation.tree.setSelectionPath(nodeId,true);
		    dlgLocation.setVisible(true);
		}
		if(dlgLocation.cancelFlag) return;

		parentNodeId=dlgLocation.tree.getSelectedNodeId();
        this.txtRetentionLocation.setText(dlgLocation.tree.getSelectedNodeStrPath());
	}
	
	public boolean validateRetentionData() {
		try {
			if(txtRetentionName.getText() == null || txtRetentionName.getText().trim().equals("")){
				VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRetentionPanel.msg_3"));
				txtRetentionName.requestFocus();
				return false;
			}
			if (!VWRetentionConnector.isUniqueRetentionName(txtRetentionName.getText(), retentionBean.getId())){
				VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWRetentionPanel.msg_4"), VWMessage.DIALOG_TYPE);
				txtRetentionName.requestFocus();
				return false;
			}
			if(txtRetentionLocation.getText().trim().isEmpty()||txtRetentionLocation.getText().trim().equalsIgnoreCase(" ")){
				if(indexConditionPanel.tblConditions.getRowCount()<=0){
					VWMessage.showMessage(this, AdminWise.connectorManager.getString("VWRetentionPanel.msg_5"), VWMessage.DIALOG_TYPE);
					return false;
				}
			}
			/**This condition has added in CV2019***/
			if (indexConditionPanel != null) {
				AdminWise.printToConsole("indexConditionPanel.cmbDocTypesList.getSelectedIndex()......"+indexConditionPanel.cmbDocTypesList.getSelectedIndex());
				if(indexConditionPanel.cmbDocTypesList.getSelectedIndex()==0) {
					try {
						int sessionID = AdminWise.adminPanel.roomPanel.getSelectedRoom().getId();
						Vector keepDocTypeFlag = new Vector();
						int retVal = VWConnector.ViewWiseClient.getKeepDocType(sessionID, keepDocTypeFlag);
						AdminWise.printToConsole("getKeepDocType return value :"+keepDocTypeFlag);
						if (retVal != -1) {
							String keepdoctype = null;
							if (keepDocTypeFlag != null && keepDocTypeFlag.size() > 0) {								
								keepdoctype = keepDocTypeFlag.get(0).toString();
								AdminWise.printToConsole("keepdoctype :"+keepDocTypeFlag);
								if (keepdoctype != null && keepdoctype.equalsIgnoreCase("true")) {
									AdminWise.printToConsole("Please select doc type");
									VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWIndicesConditionPanel.msg_1"));
									return false;
								}
							}
	
						}
					} catch (Exception ex) {
						AdminWise.printToConsole("Exception in  getKeepDocType method :::: " + ex.getMessage());
					}
				}
			}
			/****************************************/
            if( !TxtNotifyBefore.getText().trim().equals("") && (!VWUtil.isNumeric(TxtNotifyBefore.getText(), true)) ){
            	VWMessage.showMessage(this,	AdminWise.connectorManager.getString("VWRetentionPanel.msg_6"));
				TxtNotifyBefore.requestFocus();
				return false;
			}
			
			if(TxtMovePath.isVisible() && TxtMovePath.getText().trim().equals("")){
				VWMessage.showMessage(this, AdminWise.connectorManager.getString("VWRetentionPanel.msg_7"), VWMessage.DIALOG_TYPE);
				BtnLocation.requestFocus();
				BtnBrowse.requestFocus();
				return false;
			}
			if(chkSendMail.isSelected()){
				if(taEMailIds.getText().trim().equals("")){
					VWMessage.showMessage(this, AdminWise.connectorManager.getString("VWRetentionPanel.msg_8"));
					return false;
				}
			}

			//Display this message only for the retention creation and not for updation
			//This is removed after adding the functionality on check box to enable retention
			/*if(!inUpdateMode && chkEnableRetention.isSelected()){
				VWMessage.showInfoDialog(this, "Retention applies for existing documents also.");
			}*/
		} catch (Exception e) {
			return false;
			//System.out.println("Exception validateRetentionData" + e.getMessage());
		}
		return true;
	}
	
	public boolean isPositive(String value){
		boolean isValid = false;
		int numVal =0;
		try{
			numVal = Integer.parseInt(value);	

			if(numVal>0)
				isValid = true;
			else
				isValid = false;

		}catch(NumberFormatException nfe){
			isValid = false;
			return isValid;			
		}		
		return isValid;
	}
	
	public boolean isNumeric(String value){
		boolean isValid = false;
		int numVal =0;
		try{
			numVal = Integer.parseInt(value);	

			if(numVal>0 && numVal<100)
				isValid = true;
			else
				isValid = false;

		}catch(NumberFormatException nfe){
			isValid = false;
			return isValid;			
		}		
		return isValid;
	}
	
    public void loadSelectFolderDialog()
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        String backupPath=TxtMovePath.getText();
        chooser.setCurrentDirectory(new File(backupPath));
        int returnVal = chooser.showSaveDialog(/*(Component)this*/null);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            backupPath=chooser.getSelectedFile().getPath();
            chooser.setVisible(false);
            chooser=null;
        }
        TxtMovePath.setText(backupPath);
    }
    
	private void loadRetentionData() {
		
		updatePurgeDocsRegistry();
        stopGridEdit();
        loadDocTypes();
        
        selRetention = (VWRetention) cmbAvailableRetention.getSelectedItem();
       
        if(selRetention==null) return;
        if(selRetention.getId()==-1) {
        	selRetentionId = -1;
        	loadAvailableRetention();
        	stopGridEdit();
        	Table.clearData();
        	SPTable.getViewport().add(Table);
            setEnableMode(MODE_UNSELECTED);
            BtnRetentionAddIndex.setEnabled(false);
        }
        else if (selRetention.getId()==-2) {
        	selRetentionId = -1;
            loadAvailableRetention();
            stopGridEdit();
            Table.clearData();
            SPTable.getViewport().add(Table);
            setEnableMode(MODE_UNSELECTED);
            BtnRetentionAddIndex.setEnabled(false);
        }
        else {
        	//If RetentionId is available, get the retention data and set to the UI
        	BtnRetentionAddIndex.setEnabled(false);
        	int retentionId = selRetention.getId();
        	selRetentionId = retentionId;
        	retentionBean.setId(selRetentionId);
        	enableNotify = false;
        	
        	// Check for isRetentionInUse
        	int isRetentionInUse = VWRetentionConnector.checkRetentionInUse(retentionId);
        	

        	Vector<VWRetention> retentionSettings = new Vector<VWRetention>();
        	Vector indicesCondition = new Vector();
        	retentionSettings = VWRetentionConnector.getRetentionSettings(retentionId);
        	indicesCondition = VWRetentionConnector.getRetentionIndicesCondition(retentionId);
        	IndicesCondition indicesConditionBean = null; 
        	
        	SPTable.getViewport().add(PanelRetentionSetting);
			PanelRetentionSetting.setVisible(true);
			
			txtRetentionName.setText(selRetention.getName()) ;
			txtRetentionLocation.setText(selRetention.getRetentionLocation()) ;
			parentNodeId=Integer.valueOf(selRetention.getRetentionLocationId());

			//There is no 0 days to notify.
			int notifyValue = selRetention.getNotifyBefore();
			if(notifyValue==0)
				TxtNotifyBefore.setText("");
			else
				TxtNotifyBefore.setText(String.valueOf(notifyValue));
			
			boolean statusRetention = false;
			if(selRetention.getStatus()==0)
				statusRetention = false;
			else
				statusRetention = true;
			chkEnableRetention.setSelected(statusRetention);
			int indicesCount = 0;
			if(indicesCondition!=null && indicesCondition.size()>0){
				indicesCount = indicesCondition.size();
			}
			String conditionTableData [][] = new String [indicesCount][7];
			indexConditionPanel.clearTableAndRelatedData();
			int indexType = 0;
			Vector srcIndicesCondition = new Vector();
			for(int i=0; i<indicesCount; i++){
				indicesConditionBean = new IndicesCondition(indicesCondition.get(i).toString()); 
            	int j = 0;
            	String curSelectedData = "";
            	srcIndicesCondition.add(indicesConditionBean);
            	if(indexConditionPanel.vecTableData.size()==0){
            		curSelectedData = "	"+indicesConditionBean.getIndexName() + "\t" + 
            		indicesConditionBean.getComparisonOpert() + "\t" +
            		indicesConditionBean.getIndexValue1() +"\t"+
            		"-" + "\t"+
            		indicesConditionBean.getIndexValue2() + "\t"+
            		String.valueOf(indicesConditionBean.getIndexId())+"\t"+
            		String.valueOf(indicesConditionBean.getIndexTypeId());
            		
            		indexType = indicesConditionBean.getIndexTypeId();
            	}else{
            		curSelectedData = "	"+indicesConditionBean.getIndexName() + "\t" + 
            		indicesConditionBean.getComparisonOpert() + "\t" +  
            		indicesConditionBean.getIndexValue1() +"\t"+
            		indicesConditionBean.getLogicalOpert() + "\t"+
            		indicesConditionBean.getIndexValue2() + "\t"+
            		String.valueOf(indicesConditionBean.getIndexId())+"\t"+
            		String.valueOf(indicesConditionBean.getIndexTypeId());
            		indexType = indicesConditionBean.getIndexTypeId();
            	}
            	
            	if(!enableNotify){
            		if(indexType == 2){
            			enableNotify = true;
            		}
            	}
            	
            	indexConditionPanel.vecTableData.add(curSelectedData);
				
            }
			indexConditionPanel.addData(indexConditionPanel.vecTableData);
			indexConditionPanel.setSelectedDocType(selRetention.getDocTypeId());
			indexConditionPanel.loadIndicesForSelectedDocType();
			selRetention.setIndicesCondition(srcIndicesCondition);
            int DisposalActionsIndex = selRetention.getActionId();
            if(DisposalActionsIndex==4){
            	VWRecord vwRecord = new VWRecord(4,AdminWise.connectorManager.getString("VWRetentionPanel.vwRecord")+" " + PRODUCT_NAME + ".");
            	VWRecord check = (VWRecord)cmbDisposalActions.getItemAt(4);
            	if(!check.getName().equalsIgnoreCase(AdminWise.connectorManager.getString("VWRetentionPanel.vwRecord")+"  " + PRODUCT_NAME + ".")){
            		cmbDisposalActions.insertItemAt(vwRecord,4);
            		cmbDisposalActions.setSelectedItem(vwRecord);
            	}else{
            		cmbDisposalActions.setSelectedIndex(DisposalActionsIndex);
            	}
            	
            }else{
            	if(!purgeDocsFlag && DisposalActionsIndex == 5){
            		cmbDisposalActions.setSelectedIndex(cmbDisposalActions.getItemCount()-1);
            	}else
            		cmbDisposalActions.setSelectedIndex(DisposalActionsIndex);

            	if(!purgeDocsFlag){
	            	VWRecord check = (VWRecord)cmbDisposalActions.getItemAt(4);
	            	if(check.getName().equalsIgnoreCase(AdminWise.connectorManager.getString("VWRetentionPanel.vwRecord")+"  " + PRODUCT_NAME + "."))
	            		cmbDisposalActions.removeItemAt(4);
            	}
            }
            
            if(DisposalActionsIndex == 1 || (DisposalActionsIndex == 2) || (DisposalActionsIndex == 5) ){
    			String temp = selRetention.getLocation();
    			
    			if(DisposalActionsIndex == 1 ){
    				Vector locationInfo = new Vector();				
    				int roomId = AdminWise.adminPanel.roomPanel.getSelectedRoom().getId();//Using Node Id get the location Path
        			if(!selRetention.getLocation().equals("-1")){//having the node value
        				nodeId = Integer.parseInt(selRetention.getLocation());
        				if(nodeId!=0){
        					AdminWise.gConnector.ViewWiseClient.getNodeParents(roomId, new Node(selRetention.getLocation()), locationInfo);
        					StringBuffer path = new StringBuffer(AdminWise.adminPanel.roomPanel.getSelectedRoom().getName());
        					Node node = null;
        					for(int i=locationInfo.size()-1;i>=0; i--){
        						try{
        							node = (Node)locationInfo.get(i);
        							path.append("\\" + node.getName());
        						}catch(Exception e){
        							path = new StringBuffer();
        						}
        					}		
        					TxtMovePath.setText(path.toString());
        					TxtMovePath.setVisible(true);
        				}
    					//txtActionLocation.setText(path.toString());
    					/*if(temp!=null && !temp.equals("") && !temp.equals("-") && temp.indexOf(":")!=-1)
    					temp = temp.substring(temp.indexOf(":")+1, temp.length());*/
    				}
    				if(temp!=null && !temp.equals("") && !temp.equals("-") && temp.indexOf(":")!=-1)
    					temp = temp.substring(temp.indexOf(":")+1, temp.length());
    			}else{
    				//CV10.1 IF condition added for Retention Template Enhancement.
    				if(temp.equals("0"))
    					TxtMovePath.setText("");
    				else
    					TxtMovePath.setText(temp);
    				AdminWise.printToConsole("Inside TxtMovePath.getText() ::::::::::"+TxtMovePath.getText());
    				TxtMovePath.setVisible(true);
    			}
    		}
            
            int sendMail = selRetention.getSendMail();
            if(sendMail==1)
            	chkSendMail.setSelected(true);
            else
            	chkSendMail.setSelected(false);

            String mailIds = selRetention.getMailIds().trim();
            if(mailIds.equalsIgnoreCase("-"))
            	taEMailIds.setText("");
            else if(mailIds.equalsIgnoreCase("0"))
            	taEMailIds.setText("");
            else
            	taEMailIds.setText(mailIds);

            if(DisposalActionsIndex == 1 || DisposalActionsIndex == 5){
    			JLabel5.setText(LBL_MOVETO);
    		}
    		else if(DisposalActionsIndex == 2){
    			JLabel5.setText(LBL_SAVEIN);
    		}
    		CURRENT_MODE = 2;
            
    		setEnableMode(MODE_SELECT);
    		
    		if(isRetentionInUse>0){
        		BtnRetentionCompletedDocs.setEnabled(true);
        	}else{
        		BtnRetentionCompletedDocs.setEnabled(false);
        	}
    		
    		/*if(enableUpdateDelete>0){
    			BtnUpdate.setEnabled(false);
        		BtnDelete.setEnabled(false);
    		}else if(enableUpdateDelete==-1){
    			BtnUpdate.setEnabled(true);
        		BtnDelete.setEnabled(true);    			
    		}*/
    		
/*    		int isARSRunnning = VWConnector.ViewWiseClient.getARSServer(gCurRoom);
    		
    		if (isARSRunnning == 0){ //ARS Service is runnting
    		int enableUpdateDelete = VWRetentionConnector.getRetentionDocumentList(retentionId);
    			if (enableUpdateDelete == -1 ){
    				BtnUpdate.setEnabled(true);
            		BtnDelete.setEnabled(true);
    			}else if(enableUpdateDelete > 0){
    				BtnUpdate.setEnabled(false);
            		BtnDelete.setEnabled(false);
    			}
    		}else if(isARSRunnning==-1){//ARS Service not runnting
    			BtnUpdate.setEnabled(true);
        		BtnDelete.setEnabled(true);
    		}*/
			BtnUpdate.setEnabled(true);
    		BtnDelete.setEnabled(true);
    		
        	BtnSaveAs.setEnabled(true);
        }
    }
	
	public boolean canWeUpdateDelete(){
		int isARSRunnning = VWConnector.ViewWiseClient.getARSServer(gCurRoom);
		
		if (isARSRunnning == 0){ //ARS Service is runnting
			VWRetention selRetention = (VWRetention) cmbAvailableRetention.getSelectedItem();
			int retentionId = selRetention.getId();
			int enableUpdateDelete = VWRetentionConnector.getRetentionDocumentList(retentionId);
			if (enableUpdateDelete == -1 ){
				return true;
			}else if(enableUpdateDelete > 0){
				VWMessage.showMessage(this, AdminWise.connectorManager.getString("VWRetentionPanel.msg_9"));
				return false;
			}
		}else if(isARSRunnning==-1){//ARS Service not runnting
			return true;
		}
		return false;
	}
	class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
        	try {
        		PanelRetentionSetting.repaint();
        	} catch (Exception e) {
        		JOptionPane.showMessageDialog(null,e.toString());
        	}
        }
    }
	
	public void clearAll() {
		try{
			txtRetentionName.setText("");
			TxtMovePath.setText("");
			chkSendMail.setSelected(false);
			taEMailIds.setText("");
			TxtNotifyBefore.setText("");
			indexConditionPanel.clearAll();
			indexConditionPanel.clearAllText();
			indexConditionPanel.clearTableAndRelatedData();
			cmbDisposalActions.setSelectedIndex(0);
			chkEnableRetention.setSelected(false);
			cmbAvailableRetention.setSelectedIndex(0);
		}catch(Exception ex){
			//System.out.println("VWRetentionpanel - Error in clear :"+ex.toString());
		}
	}
	
	
	class SymComponent extends java.awt.event.ComponentAdapter {
		public void componentResized(java.awt.event.ComponentEvent event) {
			Object object = event.getSource();
			if (object == VWRetentionSetting)
				VWRetention_componentResized(event);
		}
	}
	
	// --------------------------------------------------------------------------
	void VWRetention_componentResized(java.awt.event.ComponentEvent event) {
		if (event.getSource() == VWRetentionSetting) {
			Dimension mainDimension = this.getSize();
			Dimension panelDimension = mainDimension;
			Dimension commandDimension = PanelCommand.getSize();
			commandDimension.height = mainDimension.height;
			panelDimension.width = mainDimension.width - commandDimension.width;
			PanelTable.setSize(panelDimension);
			PanelCommand.setSize(commandDimension);
			SPTable.setSize(panelDimension);
			SPTable.revalidate();
			PanelTable.doLayout();								
		}
	}
	
	// --------------------------------------------------------------------------
	
	// --------------------------------------------------------------------------
	public void setEnableMode(int mode) {
		currentMode = mode;
		cmbAvailableRetention.setEnabled(true);
		cmbAvailableRetention.setVisible(true);
		switch (mode) {
		case MODE_CONNECT:
			CURRENT_MODE = 0;
			cmbAvailableRetention.setEnabled(true);
			BtnDelete.setVisible(true);
			lblAvailableRetention.setVisible(true);
			cmbAvailableRetention.setVisible(true);
			BtnSaveAs.setVisible(true);
			
			BtnNewRetention.setEnabled(true);
			BtnNewRetention.setText(BTN_NEWRETENTION_NAME);
			BtnDelete.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			cmbAvailableRetention.setEnabled(true);
			BtnSummary.setVisible(true);
			BtnRetentionCompletedDocs.setVisible(true);
			BtnRetentionAddIndex.setVisible(true);
			BtnRetentionAddIndex.setEnabled(false);
			BtnFreezeDocs.setVisible(true);
			BtnFreezeDocs.setEnabled(true);
			loadAvailableRetention();
			viewMode(false);
			break;
			
		case MODE_UNSELECTED:
			BtnNewRetention.setText(BTN_NEWRETENTION_NAME);
			BtnNewRetention.setIcon(VWImages.NewRouteIcon);
			BtnNewRetention.setEnabled(true);
			BtnDelete.setEnabled(false);
			BtnSaveAs.setEnabled(false);
			BtnRetentionCompletedDocs.setEnabled(false);
			BtnRetentionAddIndex.setEnabled(false);
			BtnFreezeDocs.setEnabled(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnUpdate.setEnabled(false);
			BtnSaveAs.setVisible(true);
			BtnDelete.setVisible(true);
			lblAvailableRetention.setVisible(true);
			cmbAvailableRetention.setEnabled(true);
			cmbAvailableRetention.setVisible(true);
			viewMode(false);
			break;
			
		case MODE_SELECT:
			BtnNewRetention.setEnabled(true);
			BtnNewRetention.setText(BTN_NEWRETENTION_NAME);
			BtnNewRetention.setIcon(VWImages.NewRouteIcon);
			BtnDelete.setEnabled(true);
			BtnSaveAs.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnDelete.setVisible(true);
			viewMode(false);
			lblAvailableRetention.setVisible(true);
			cmbAvailableRetention.setVisible(true);
			BtnRetentionCompletedDocs.setVisible(true);
			BtnRetentionAddIndex.setVisible(true);
			BtnFreezeDocs.setVisible(true);
			BtnFreezeDocs.setEnabled(true);
			break;
			
		case MODE_UNCONNECT:	
			CURRENT_MODE = 0;
			cmbAvailableRetention.removeAllItems();
			cmbAvailableRetention.addItem(new VWRetention(-1,LBL_RETENTION_NAME));
			retentionLoaded = false;
			cmbAvailableRetention.setVisible(true);
			
			BtnNewRetention.setText(BTN_NEWRETENTION_NAME);
			BtnNewRetention.setIcon(VWImages.NewRouteIcon);
			BtnNewRetention.setEnabled(false);
			BtnNewRetention.setVisible(true);
			
			BtnDelete.setEnabled(false);
			BtnDelete.setVisible(true);
			
			BtnSaveAs.setEnabled(false);
			BtnSaveAs.setVisible(true);
			
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateRouteImage);
			BtnUpdate.setEnabled(false);
			
			lblAvailableRetention.setVisible(true);
			
			BtnRetentionCompletedDocs.setEnabled(false);
			BtnRetentionAddIndex.setEnabled(false);
			BtnRetentionCompletedDocs.setVisible(true);
			BtnRetentionAddIndex.setVisible(true);
			BtnFreezeDocs.setVisible(true);
			BtnFreezeDocs.setEnabled(false);
			cmbAvailableRetention.setEnabled(false);
			gCurRoom = 0;			
			break;
			
		case MODE_NEW:
			loadDocTypes();
			BtnNewRetention.setEnabled(true);
			BtnNewRetention.setText(BTN_SAVE_NAME);
			BtnNewRetention.setIcon(VWImages.SaveIcon);
			BtnSaveAs.setVisible(false);
			BtnRetentionCompletedDocs.setVisible(false);
			BtnRetentionAddIndex.setVisible(false);
			BtnFreezeDocs.setVisible(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			lblAvailableRetention.setVisible(false);
			cmbAvailableRetention.setVisible(false);
			txtRetentionName.requestFocus();
			TxtNotifyBefore.setText("");
			txtRetentionLocation.setText("");
			viewMode(true);
			TxtNotifyBefore.setEnabled(false);
			break;

		case MODE_UPDATE:
			BtnNewRetention.setEnabled(true);
			BtnNewRetention.setText(BTN_SAVE_NAME);
			BtnNewRetention.setIcon(VWImages.SaveIcon);
			BtnSaveAs.setVisible(false);
			BtnRetentionCompletedDocs.setVisible(false);
			BtnRetentionAddIndex.setVisible(false);
			BtnFreezeDocs.setVisible(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			viewMode(true);
			BtnDelete.setVisible(false);
			lblAvailableRetention.setVisible(false);
			cmbAvailableRetention.setVisible(false);
			if(enableNotify){
				TxtNotifyBefore.setEnabled(true);
			}else{
				TxtNotifyBefore.setEnabled(false);
			}
			modeUpdate=true;
			break;
		case MODE_ENABLE:
			break;
		case MODE_DISABLE:
			BtnRetentionCompletedDocs.setVisible(false);
			BtnRetentionAddIndex.setVisible(false);
			BtnFreezeDocs.setVisible(false);
			break;
		}
		if (BtnUpdate.isEnabled() == false) {
			this.vApproversList.removeAllElements();
		}
	}
	
	private void viewMode(boolean flag) {
		txtRetentionName.setEnabled(flag);
		txtRetentionLocation.setEnabled(flag);
		btnRetentionLocation.setEnabled(flag);
		indexConditionPanel.cmbDocTypesList.setEnabled(flag);
		indexConditionPanel.btnRefresh.setEnabled(flag);
		indexConditionPanel.cmbIndicesList.setEnabled(flag);
		indexConditionPanel.cmbOperator.setEnabled(flag);
		indexConditionPanel.txtvalue1.setEnabled(flag);
		indexConditionPanel.btnAdd.setEnabled(flag);
		indexConditionPanel.btnRemove.setEnabled(flag);
		TxtNotifyBefore.setEnabled(flag);
		cmbDisposalActions.setEnabled(flag);
		BtnBrowse.setEnabled(flag);
		BtnLocation.setEnabled(flag);
		chkEnableRetention.setEnabled(flag);
		chkSendMail.setEnabled(flag);
		taEMailIds.setEnabled(flag);
		
		txtRetentionName.setBackground(java.awt.Color.white);
		indexConditionPanel.cmbDocTypesList.setBackground(java.awt.Color.white);
		indexConditionPanel.btnRefresh.setBackground(java.awt.Color.white);
		indexConditionPanel.cmbIndicesList.setBackground(java.awt.Color.white);
		indexConditionPanel.cmbOperator.setBackground(java.awt.Color.white);
		indexConditionPanel.txtvalue1.setBackground(java.awt.Color.white);
		indexConditionPanel.btnAdd.setBackground(java.awt.Color.white);
		indexConditionPanel.btnRemove.setBackground(java.awt.Color.white);
		indexConditionPanel.spTable.getViewport().setBackground(Color.white);
		TxtNotifyBefore.setBackground(java.awt.Color.WHITE);
		cmbDisposalActions.setBackground(java.awt.Color.white);
		BtnBrowse.setBackground(java.awt.Color.white);
		BtnLocation.setBackground(java.awt.Color.white);
		chkEnableRetention.setBackground(java.awt.Color.white);
		chkSendMail.setBackground(java.awt.Color.white);
		taEMailIds.setBackground(java.awt.Color.white);
	}

	// --------------------------------------------------------------------------
	VWPicturePanel picPanel = new VWPicturePanel(TAB_RETENTION_NAME, VWImages.DocRetentionImage);
	javax.swing.JPanel VWRetentionSetting = new javax.swing.JPanel();
	
	javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
	
	javax.swing.JPanel PanelRetentionSetting = new javax.swing.JPanel();
	
	JLabel Pic = new JLabel();
	JLabel lblAvailableRetention = new JLabel();	
	JLabel lblRetentionLocation = new JLabel();
	JLabel lblRetentionName = new JLabel();
	JLabel retentionLineBorderbottom =  new JLabel("");
	
	JTextField txtRetentionLocation = new JTextField();
	JTextField txtRetentionName = new JTextField();
	JTextField TxtMovePath = new JTextField();
	JTextField TxtNotifyBefore = new JTextField();
	//JFormattedTextField TxtNotifyBefore = new JFormattedTextField(getNF());
	
	JLabel lblTriggerAt = new JLabel();
	JLabel lblRoomLocation = new JLabel();
	JLabel lblCaption = new JLabel(LBL_ROUTINGTITLE);
	
	VWComboBox cmbAvailableRetention = new VWComboBox();
	VWComboBox cmbDisposalActions = new VWComboBox();
	JCheckBox chkEnableRetention = new JCheckBox();
	JCheckBox chkSendMail = new JCheckBox(AdminWise.connectorManager.getString("VWRetentionPanel.chkSendMail"));
	private JTextArea taEMailIds = new JTextArea(2,4);
	private JScrollPane spEMailIds = new JScrollPane(taEMailIds);
	JCheckBox chkReadOnly = new JCheckBox();
	
	DefaultListModel availableListModel = new DefaultListModel();

	JList lstAvailableUsers = new JList(availableListModel);	
	VWLinkedButton BtnNewRetention = new VWLinkedButton(1);	
	VWLinkedButton BtnUpdate = new VWLinkedButton(1);	
	VWLinkedButton BtnDelete = new VWLinkedButton(1);	
	VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
	VWLinkedButton BtnLocation = new VWLinkedButton(1);
	VWLinkedButton btnRetentionLocation = new VWLinkedButton(1);
    VWLinkedButton BtnBrowse = new VWLinkedButton(1);
    
	VWLinkedButton BtnSummary = new VWLinkedButton(1);
	VWLinkedButton BtnRetentionCompletedDocs = new VWLinkedButton(1);
	VWLinkedButton BtnFreezeDocs = new VWLinkedButton(1);
	/**
     * CV10.1 Enhancement: Added for implementing progress dialog on clicking of Custom Report Button
     */
	JButton BtnProgressClick=new  JButton();
    Lock lock1 = new ReentrantLock();
    String s1=new String(new char[] { 32 });
	String s2=new String(new char[] { 9 });
	JLabel statusLabel=new JLabel("		"+ s1+ s1+AdminWise.connectorManager.getString("progressDialog.statusForRetention"));
	JDialog processDialog = new JDialog();
	///////////////////////////////////////////
	/**
	 * @author apurba.m
	 * CV10.1 Enhancement: Added for custom indices
	 */
	VWLinkedButton BtnRetentionAddIndex = new VWLinkedButton(1);
	
	JPanel PanelTable = new JPanel();	
	JScrollPane SPTable = new JScrollPane();	
	JScrollPane SPAvailableUsers = new JScrollPane();	
	JScrollPane SPApprovers = new JScrollPane();
	
	VWRetentionTable Table = new VWRetentionTable();
	
	Vector vApproversList = new Vector();	
	JLabel lblUNCInfo = null;
	VWRetentionTableData retentionTabData= new VWRetentionTableData(Table);
	
	
	public static boolean ARSFLAG = true;
	private int selRetentionId = -1;
	public VWDlgSaveLocation dlgLocation = null;	
	public VWDlgLocation dlgRetentionLocation = null;
	public  int parentNodeId=0;
	private boolean retentionLoaded = false;	
	private static int gCurRoom = 0;
	private int retUpdate;
	private int retDelete;
	private boolean UILoaded = false;
	
	public static boolean usersModified = false;
	public static String selectedRetentionName = "";
	private int currentMode = -1;
	private int globalRetentionId=0;
	private JLabel lblAction = new JLabel();
	private JLabel lblNotifyBefore1 = new JLabel();
	private JLabel lblNotifyBefore2 = new JLabel();
	javax.swing.JLabel JLabel5 = new javax.swing.JLabel();
	
	public static boolean inUpdateMode = false;

	
	VWRetention retentionBean = new VWRetention(0);
	VWRetention selRetention =  new VWRetention(0);
	IndicesCondition indicesConditionBean; 
	// This is to get the indices condition panel 
	VWIndicesConditionPanel indexConditionPanel = new VWIndicesConditionPanel();

	DrawingView view = null;	
	public int CURRENT_MODE = 0;
	private int totalRecordCount = 0;
	
	public static boolean taskSequenceAdded = false;		
	public static int endDistStartX = 250;
	public static int endDistStartY = 52;
	public static int endDistEndX = 350;
	public static int endDistEndY = 550;
	public static boolean enableNotify = false;
	public static int selIndicesCount = 0;
	public static boolean indexFlag = false;
	public static boolean modeUpdate = false;
	
	private int nodeId=0;
	//public static VWIndicesConditionPanel vwIndicesConditionPanel = new VWIndicesConditionPanel(); 
	VWLinkedButton btnEnlarge = new VWLinkedButton(1);
	public int tableWidth = 700;
	public static void main(String[] args) {
		try {
			String plasticLookandFeel = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		} catch (Exception se) {
		}
		JFrame frame = new JFrame();
		VWRetentionPanel panel = new VWRetentionPanel();
		frame.add(panel);
		panel.setupUI();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*
		 * JFrame.resize() is replaced with JFrame.setSize() as JFrame.resize() is deprecated
		 * Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
		frame.setSize(800, 600);
	}
	public void loadDocTypes(){
		//To avoid the index table check for different doctype index fields
		indexConditionPanel.curDocTypeSelected = -3;
		indexConditionPanel.loadDocTypes();
	}
	public void loadTabData(VWRoom newRoom)
	{
	    if(newRoom.getConnectStatus()!=Room_Status_Connect)
	    {
	        Table.clearData();
	        //CmbUsers.removeAllItems();
	        return;
	    }
	    if(newRoom.getId()==gCurRoom)return;
	    gCurRoom=newRoom.getId();
	    loadAvailableRetention();
	    SPTable.getViewport().add(Table);
	    Table.clearData();
	    UILoaded = true;
	    setEnableMode(MODE_UNSELECTED);
	    
	    /**
	     * if (newRoom.getId() == gCurRoom)
			return;
		loadAvailableRoutes();
		PanelRetentionSetting.setVisible(false);
		SPTable.getViewport().add(Table);
		loadDocumentsInRoute();
		UILoaded = true;
		setEnableMode(MODE_UNSELECTED);
		lblTableHeader.setText(LBL_DOCS_IN_ROUTE_TABLE_HEADER);
	     */
	}
	public void unloadTabData() {
		SPTable.getViewport().add(Table);
		PanelRetentionSetting.setVisible(false);
		Table.clearData();
		repaint();
		gCurRoom = 0;
		setEnableMode(MODE_UNCONNECT);
	}
	private NumberFormatter getNF()
    {
        DecimalFormat decimalFormat = new DecimalFormat("##");
        NumberFormatter numberFormatter = new NumberFormatter(decimalFormat);
        numberFormatter.setOverwriteMode(true);
        numberFormatter.setAllowsInvalid(false);
        return(numberFormatter);
    }
}
