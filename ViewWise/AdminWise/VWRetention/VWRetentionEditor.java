/*
                      Copyright (c) 2010-2014
                      Computhink Software - Syntaxsoft
                      All rights reserved.
*/

/**
 * VWRetentionEditor<br>
 *
 * @version     $Revision: 1.1 $
 * @author      Vijaypriya.B.K
**/
package ViewWise.AdminWise.VWRetention;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;

public class VWRetentionEditor implements TableCellEditor,VWConstant{
  protected TableCellEditor editor,defaultEditor;
  protected VWRetentionTable gTable=null;
 //------------------------------------------------------------------------------
  public VWRetentionEditor(VWRetentionTable table){
    JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;
  }
//------------------------------------------------------------------------------
  public Component getTableCellEditorComponent(JTable table,
      Object value, boolean isSelected, int row, int column){    
    return defaultEditor.getTableCellEditorComponent(table,value,isSelected,row,column);
  }
//------------------------------------------------------------------------------
  public Object getCellEditorValue(){
    return editor.getCellEditorValue();
  }
//------------------------------------------------------------------------------
  public boolean stopCellEditing(){
    return editor.stopCellEditing();
  }
//------------------------------------------------------------------------------
  public void cancelCellEditing(){
    editor.cancelCellEditing();
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(EventObject anEvent){
    return false;
  }
//------------------------------------------------------------------------------
  public void addCellEditorListener(CellEditorListener l){
    editor.addCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public void removeCellEditorListener(CellEditorListener l){
    editor.removeCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public boolean shouldSelectCell(EventObject anEvent){
    return editor.shouldSelectCell(anEvent);
  }
//------------------------------------------------------------------------------
}
  