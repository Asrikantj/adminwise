/*
					  Copyright (c) 2010-2014
                      Computhink Software - Syntaxsoft
                      All rights reserved.
*/

/**
 * VWRetentionTable<br>
 *
 * @version     $Revision: 1.4 $
 * @author      Vijaypriya.B.K
**/
package ViewWise.AdminWise.VWRetention;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;

import com.computhink.common.util.VWRefCreator;

import java.awt.Dimension;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.LinkedList;
import java.util.prefs.Preferences;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//------------------------------------------------------------------------------
public class VWRetentionTable extends JTable implements VWConstant
{
    protected VWRetentionTableData m_data;
    public VWRetentionTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWRetentionTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        ///setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k=0;k<VWRetentionTableData.m_columns.length;k++) 
        {
            TableCellRenderer renderer;
            DefaultTableCellRenderer textRenderer=new VWTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWRetentionTableData.m_columns[k].m_alignment);
            renderer=textRenderer;
            VWRetentionEditor editor=new VWRetentionEditor(this);
            TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWRetentionTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);

        setRowHeight(TableRowHeight);
        //setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        getColumnModel().getColumn(0).setPreferredWidth(194);
        getColumnModel().getColumn(1).setPreferredWidth(194);
    	getColumnModel().getColumn(2).setPreferredWidth(90);
    	getColumnModel().getColumn(3).setPreferredWidth(90);
    	getColumnModel().getColumn(4).setPreferredWidth(194);    
    	
        setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
		Object object = event.getSource();
		if (object instanceof JTable)
			if(event.getModifiers()==event.BUTTON3_MASK)
				VWRetentionTable_RightMouseClicked(event);
			else if(event.getModifiers()==event.BUTTON1_MASK)
				VWRetentionTable_LeftMouseClicked(event);
	}
}
//------------------------------------------------------------------------------
void VWRetentionTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWRetentionTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    	loadDocumentInDTC(row);
    }

//  ------------------------------------------------------------------------------
    /**
     * loadDocumentInDTC - This method is used to open the document in DTC
     */
     private void loadDocumentInDTC(int row)
    {
    	int docId = this.getRowDocId(row);
        VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        String room = vwroom.getName();
    	try {      
            //create a temp folder to place reference file in it
        	File folder = new File("c:\\VWReferenceFile\\");
            folder.mkdirs();
            //create a reference file to open document
            File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);

            //open reference file in ViewWise thus opening doc                        
            try{
                Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
                //String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
                String dtcPath = VWUtil.getHome();
                Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
            }catch(Exception ex){
            	ex.printStackTrace();
            	//JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
            }
        }
        catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    	/*
        int docId=getRowDocId(row);
        int roomId=getRowRoomId(row);
        String docName=(String)getValueAt(row,1);
        VWTreeConnector.setTreeNodeSelection(docId,roomId,true);
        VWTableConnector.getDocFile(roomId,docId,docName,VWTreeConnector.getFixTreePath());
         **/
    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
    public Vector getDocIds(String sep)
    {
        Vector list=new Vector();
        String[][] data = m_data.getData();
        int count=data.length;
        String docIds="";
        for(int i=0;i<count-1;i++)
        {
            docIds+=data[i][0]+sep;
            if(docIds.length()>7000)
            {
                list.add(docIds.substring(0,docIds.length()-sep.length()));
                docIds="";
            }
        }
        docIds+=data[count-1][0];
        list.add(docIds);
        return list;
    }
//------------------------------------------------------------------------------
  public String[][] getReportData()
  {
      String[][] tableData=m_data.getData();
      int count=tableData.length;
      /**@author apurba.m
       * range of repData has been changed to 100 for custom indices
       */
      String[][] repData=new String[count][100];
      for(int i=0;i<count;i++)
      {
          repData[i][0]=tableData[i][1];
          repData[i][1]=tableData[i][2];
          repData[i][2]=tableData[i][3];
          repData[i][3]=tableData[i][4];
      }
      return repData;
  }
  
  /**
   * @author apurba.m
   * @return
   * CV.1 ENhancement: Added for custom indices 
   */
  public String[][] getReportDataForCustomIndex()
  {
      String[][] tableData=m_data.getDataForCustomIndex();
      AdminWise.printToConsole("tableData ::::"+tableData);
      int count=tableData.length;
      /**@author apurba.m
       * range of repData has been changed to 100 for custom indices
       */
      String[][] repData=new String[count][100];
      for(int i=0;i<count;i++)
      {
          repData[i][0]=tableData[i][1];
          repData[i][1]=tableData[i][2];
          repData[i][2]=tableData[i][3];
          repData[i][3]=tableData[i][4];
          repData[i][4]=tableData[i][5];
      }
      return repData;
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new RetentionsRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteSelectedRows()
  {
        int[] rows=getSelectedRows();
        int count=rows.length;
        for(int i=0;i<count;i++)
        {
            m_data.remove(rows[i]-i);
        }
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowRetentionsId(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number((row[0]));
  }
//------------------------------------------------------------------------------
  public int getRowType()
  {
        return getRowType(getSelectedRow());
  }
//------------------------------------------------------------------------------
  public int getRowType(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number((row[6]));
  }
//------------------------------------------------------------------------------  
  public int getRowDocId(int rowNum)
  {
        String[] row=m_data.getRetentionRowData(rowNum);
        //Code is to get the document Id. The row[1] has doc id 
        return VWUtil.to_Number(row[1]);
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class RetentionsRowData
{
	//50 Report2 55 108 3 RetDocType ARSAdmin Mar  2 2010  5:13PM
	//RetentionName, DocumentName, DocTypeName, CreatorName, ProcessedDate
    public String   m_RetentionId;
	public String   m_RetentionName;
    public String   m_DocName;
    public String   m_DocTypeName;
    public String   m_CreatorName;
    public String   m_ProcessedDate;
    public int m_docId;

    public RetentionsRowData() {
    	m_RetentionId="";
    	m_RetentionName="";
    	m_docId = -1;
        m_DocName="";
        m_DocTypeName="";
        m_CreatorName="";
        m_ProcessedDate="";
  }
  
    /**
     * @author apurba.m
     * @param RetentionId
     * @param RetentionName
     * @param DocName
     * @param DocTypeName
     * @param Created
     * @param ProcessedDate
     * CV10.1 Enhancement: RetentionId added as parameter for generating HTML report with custom indices
     */
  public RetentionsRowData(String RetentionId,String RetentionName,String DocName,String DocTypeName,
    String Created,String ProcessedDate) 
  {
	  m_RetentionId=RetentionId;
	  m_RetentionName=RetentionName;
      m_DocName=DocName;
      m_DocTypeName=DocTypeName;
      m_CreatorName=Created;
      m_ProcessedDate=ProcessedDate;
  }
  //----------------------------------------------------------------------------
  public RetentionsRowData(String str)
  {
	VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
        //50 Report2 63 103 3 RetDocType ARSAdmin Mar  3 2010  3:42PM
        //int retId = Integer.parseInt(tokens.nextToken());//RetentionId
	try{
		m_RetentionId=tokens.nextToken();//RetentionId
	}catch (Exception e){
		m_RetentionId=AdminWise.connectorManager.getString("VWRetentionTable.RetentionsRowData_1");
	}
	
        try
        {
            m_RetentionName = tokens.nextToken();
        }
        catch(Exception e){m_RetentionName=AdminWise.connectorManager.getString("VWRetentionTable.RetentionsRowData_1");}
        m_docId = Integer.parseInt(tokens.nextToken());//RetentionId//DocumentId
        try
        {
            m_DocName=tokens.nextToken();
        }
        catch(Exception e4){m_DocName=AdminWise.connectorManager.getString("VWRetentionTable.RetentionsRowData_1");}
        
        int docTypeId = Integer.parseInt(tokens.nextToken());//RetentionId//DocTypeId
        try
        {
            m_DocTypeName=tokens.nextToken();
        }
        catch(Exception e4){m_DocTypeName=AdminWise.connectorManager.getString("VWRetentionTable.RetentionsRowData_1");}
        try
        {
            m_CreatorName=tokens.nextToken();
        }
        catch(Exception e4){m_CreatorName=AdminWise.connectorManager.getString("VWRetentionTable.RetentionsRowData_1");}
        try
        {
            m_ProcessedDate=tokens.nextToken();
        }
        catch(Exception e4){m_ProcessedDate=AdminWise.connectorManager.getString("VWRetentionTable.RetentionsRowData_2");}
  }
  //----------------------------------------------------------------------------
  public RetentionsRowData(String[] data) 
  {
    int i=0;
    int retId=0;
    int docId=0;
    int docTypeId=0;
    
    retId = VWUtil.to_Number(data[i++]);//Not required to display
    m_RetentionName=data[i++];
    docId = VWUtil.to_Number(data[i++]);//Not required to display
    m_DocName=data[i++];
    docTypeId = VWUtil.to_Number(data[i++]);//Not required to display
    m_DocTypeName =data[i++];
    m_CreatorName=data[i++];
    m_ProcessedDate=data[i++];
  }
}
//------------------------------------------------------------------------------
class RetentionsColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public RetentionsColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWRetentionTableData extends AbstractTableModel 
{
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  public static final RetentionsColumnData m_columns[] = {
	new RetentionsColumnData(VWConstant.RetentionsColumnNames[0],0.3F,JLabel.LEFT ),//RetentionName
	new RetentionsColumnData(VWConstant.RetentionsColumnNames[1],0.2F,JLabel.LEFT),//DocumentName
	new RetentionsColumnData(VWConstant.RetentionsColumnNames[2],0.2F,JLabel.LEFT),//DocTypeName
	new RetentionsColumnData(VWConstant.RetentionsColumnNames[3],0.1F,JLabel.LEFT),//Creator
	new RetentionsColumnData(VWConstant.RetentionsColumnNames[4],0.2F,JLabel.LEFT)//PrcessedDate
  };
  public static final int COL_RETNAME = 0;
  public static final int COL_DOCNAME = 1;
  public static final int COL_DOCTYPENAME = 2;
  public static final int COL_CREATORNAME = 3;
  public static final int COL_PROCESSED = 4;

  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;
  
  protected VWRetentionTable m_parent;
  protected Vector m_vector;

  public VWRetentionTableData(VWRetentionTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
  public String[] getRetentionRowData(int rowNum) 
  {
      int count=getRowCount();
      String[] RetentionRowData=new String[8];
      int j=0;
      RetentionsRowData row=(RetentionsRowData)m_vector.elementAt(rowNum);
      
      RetentionRowData[j++]=row.m_RetentionName;
      RetentionRowData[j++]=String.valueOf(row.m_docId);
      RetentionRowData[j++]=row.m_DocName;    
      RetentionRowData[j++]=row.m_DocTypeName;
      RetentionRowData[j++]=row.m_CreatorName;
      RetentionRowData[j++]=row.m_ProcessedDate;
      return RetentionRowData;
  }
//  ------------------------------------------------------------------------------
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();    
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
        m_vector.addElement(new RetentionsRowData((String)data.get(i)));
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
    	RetentionsRowData row =new RetentionsRowData(data[i]);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][5];
    for(int i=0;i<count;i++)
    {
    	RetentionsRowData row=(RetentionsRowData)m_vector.elementAt(i);
        data[i][0]=row.m_RetentionName;
        data[i][1]=row.m_DocName;
        data[i][2]=row.m_DocTypeName;
        data[i][3]=row.m_CreatorName;
        data[i][4]=row.m_ProcessedDate;
    }
    return data;
  }

/**
 * @author apurba.m
 * @return
 * CV10.1 Enhancement: Added for getting retention data with custom indices
 */
public String[][] getDataForCustomIndex() {
    int count=getRowCount();
    AdminWise.printToConsole("count ::::"+count);
    String[][] data=new String[count][7];
    for(int i=0;i<count;i++)
    {
    	RetentionsRowData row=(RetentionsRowData)m_vector.elementAt(i);
    	data[i][0]=row.m_RetentionId;
        data[i][1]=row.m_RetentionName;
        data[i][2]=row.m_DocName;
        data[i][3]=row.m_DocTypeName;
        data[i][4]=row.m_CreatorName;
        data[i][5]=row.m_ProcessedDate;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] RetentionsRowData=new String[5];
    RetentionsRowData row=(RetentionsRowData)m_vector.elementAt(rowNum);
    RetentionsRowData[0]=row.m_RetentionName;
    RetentionsRowData[1]=row.m_DocName;
    RetentionsRowData[2]=row.m_DocTypeName;
    RetentionsRowData[3]=row.m_CreatorName;
    RetentionsRowData[4]=row.m_ProcessedDate;
    
    return RetentionsRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
  	String str = m_columns[column].m_title;
    if (column==m_sortCol)
        str += m_sortAsc ? " �" : " �";
    return str; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    RetentionsRowData row = (RetentionsRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_RETNAME:     return row.m_RetentionName;
      case COL_DOCNAME:     return row.m_DocName;
      case COL_DOCTYPENAME:     return row.m_DocTypeName;
      case COL_CREATORNAME:    return row.m_CreatorName;
      case COL_PROCESSED:     return row.m_ProcessedDate;
      
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    RetentionsRowData row = (RetentionsRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_RETNAME:
        row.m_RetentionName = svalue; 
        break;
      case COL_DOCNAME:
          row.m_DocName = svalue; 
          break;
      case COL_DOCTYPENAME:
          row.m_DocTypeName = svalue; 
          break;
      case COL_CREATORNAME:
        row.m_CreatorName = svalue; 
        break;
      case COL_PROCESSED:
        row.m_ProcessedDate= svalue;
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(RetentionsRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
 class RetentionComparator implements Comparator {
     protected int     m_sortCol;
     protected boolean m_sortAsc;
 //--------------------------------------------------------------------------
     public RetentionComparator(int sortCol, boolean sortAsc) {
         m_sortCol = sortCol;
         m_sortAsc = sortAsc;
     }
 //--------------------------------------------------------------------------
     public int compare(Object o1, Object o2) {
         if(!(o1 instanceof RetentionsRowData) || !(o2 instanceof RetentionsRowData))
             return 0;
         RetentionsRowData s1 = (RetentionsRowData)o1;
         RetentionsRowData s2 = (RetentionsRowData)o2;
         int result = 0;
         double d1, d2;
         switch (m_sortCol) {
         	case COL_RETNAME:
         		 result = s1.m_RetentionName.toLowerCase().compareTo(s2.m_RetentionName.toLowerCase());
             	 break;
         	case COL_DOCNAME:
                 result = s1.m_DocName.toLowerCase().compareTo(s2.m_DocName.toLowerCase());
                 break;
         	case COL_DOCTYPENAME:
                result = s1.m_DocTypeName.toLowerCase().compareTo(s2.m_DocTypeName.toLowerCase());
                break;
            case COL_CREATORNAME:
                result = s1.m_CreatorName.toLowerCase().compareTo(s2.m_CreatorName.toLowerCase());
                break;            	
              case COL_PROCESSED:
            	result = s1.m_ProcessedDate.toLowerCase().compareTo(s2.m_ProcessedDate.toLowerCase());
                break;
         }
         if (!m_sortAsc)
             result = -result;
         return result;
     }
 //--------------------------------------------------------------------------
     public boolean equals(Object obj) {
         if (obj instanceof RetentionComparator) {
        	 RetentionComparator compObj = (RetentionComparator)obj;
             return (compObj.m_sortCol==m_sortCol) &&
             (compObj.m_sortAsc==m_sortAsc);
         }
         return false;
     }
 } 
 
 class ColumnListener extends MouseAdapter {
     protected VWRetentionTable m_table;
 //--------------------------------------------------------------------------
     public ColumnListener(VWRetentionTable table){
         m_table = table;
     }
 //--------------------------------------------------------------------------
     public void mouseClicked(MouseEvent e){
         
/*          if(e.getModifiers()==e.BUTTON3_MASK)
             selectViewCol(e);
         else */if(e.getModifiers()==e.BUTTON1_MASK)
             sortCol(e);
     }
 //--------------------------------------------------------------------------
     private void sortCol(MouseEvent e) {    	 
         TableColumnModel colModel = m_table.getColumnModel();
         int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
         int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
         
         if (modelIndex < 0) return;
         if (m_sortCol==modelIndex)
             m_sortAsc = !m_sortAsc;
         else
             m_sortCol = modelIndex;
         for (int i=0; i < colModel.getColumnCount();i++) {
             TableColumn column = colModel.getColumn(i);
             column.setHeaderValue(getColumnName(column.getModelIndex()));
         }
         m_table.getTableHeader().repaint();
         Collections.sort(m_vector, new RetentionComparator(modelIndex, m_sortAsc));
         m_table.tableChanged(
         new TableModelEvent(VWRetentionTableData.this));
         m_table.repaint();
     }
} 
}