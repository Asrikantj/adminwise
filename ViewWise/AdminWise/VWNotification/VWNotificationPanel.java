package ViewWise.AdminWise.VWNotification;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import com.computhink.common.Constants;
import com.computhink.common.Creator;
import com.computhink.common.NotificationSettings;
import com.computhink.common.Principal;
import com.computhink.common.RouteInfo;
import com.computhink.common.VWRetention;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.Session;
import com.computhink.vwc.notification.VWNotificationConstants;
import com.computhink.vwc.notification.VWNotificationSettingsPanel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConnector;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRetention.VWRetentionConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;

public class VWNotificationPanel extends JPanel implements VWConstant, VWNotificationConstants{
	private static final long serialVersionUID = 1L;
	public VWNotificationPanel() {
		setupUI();
	}

	public void setupUI() {
		try{
			int top = 116; int hGap = 28;
			if (UILoaded)
				return;
			int height = 24, width = 144, left = 12, heightGap = 30;
			setLayout(new BorderLayout());
			VWNotificationSetting.setLayout(null);
			add(VWNotificationSetting, BorderLayout.CENTER);
			PanelCommand.setLayout(null);
			VWNotificationSetting.add(PanelCommand);
			PanelCommand.setBackground(AdminWise.getAWColor());
			PanelCommand.setBounds(0, 0, 168, 432);

			PanelCommand.add(picPanel);
			picPanel.setBounds(8,16,149,92);
			lblUsersList.setText(LBL_USERS_LIST);
			PanelCommand.add(lblUsersList);
			lblUsersList.setBackground(java.awt.Color.white);
			lblUsersList.setBounds(left, top, width, height);

			top += hGap;
			PanelCommand.add(cmbUsersList);
			cmbUsersList.setBackground(java.awt.Color.white);
			cmbUsersList.setBounds(left-1, top, width + 4, height - 2);

			top += hGap + 12;
			BtnNotifications.setText(BTN_NOTIFICATION_NAME);
			BtnNotifications.setIcon(VWImages.NotifyIcon); 
			BtnNotifications.setActionCommand(BTN_NOTIFICATION_NAME);
			PanelCommand.add(BtnNotifications);
			BtnNotifications.setBounds(left, top, width, height);
			BtnNotifications.setVisible(true);

			top += hGap;
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setActionCommand(BTN_UPDATE_NAME);
			PanelCommand.add(BtnUpdate);
			BtnUpdate.setBounds(left, top, width, height);
			BtnUpdate.setIcon(VWImages.UpdateIcon);

			top += hGap;
			BtnDelete.setText(BTN_DELETE_NAME);
			BtnDelete.setActionCommand(BTN_DELETE_NAME);
			PanelCommand.add(BtnDelete);
			BtnDelete.setBounds(left, top, width, height);
			BtnDelete.setIcon(VWImages.DelIcon);

			top += heightGap;
			PanelTable.setLayout(new BorderLayout());
			VWNotificationSetting.add(PanelTable);
			PanelTable.setBounds(168, 0, 700,500);
			SPTable.setBounds(0, 0, 902, 659);
			SPTable.setOpaque(true);
			PanelTable.add(SPTable, BorderLayout.CENTER);
			SPTable.getViewport().add(Table);
			Table.getParent().setBackground(java.awt.Color.white);

			setEnableMode(MODE_UNCONNECT);

			initNotification();

			PanelNotificationSetting.setVisible(false);
			PanelNotificationSetting.setEnabled(false);

			SymComponent aSymComponent = new SymComponent();
			VWNotificationSetting.addComponentListener(aSymComponent);

			SymAction lSymAction = new SymAction();
			BtnNotifications.addActionListener(lSymAction);
			BtnUpdate.addActionListener(lSymAction);
			BtnDelete.addActionListener(lSymAction);

			SymItem symItem = new SymItem();
			cmbUsersList.addItemListener(symItem);
			cmbModule.addItemListener(symItem);
			cmbName.addItemListener(symItem);		

			Table.getParent().setBackground(java.awt.Color.white);
			PanelNotificationSetting.setAutoscrolls(true);
			PanelNotificationSetting.setPreferredSize(new Dimension(900, 640));	

			SPTable.setPreferredSize(new Dimension(700, 500));
			repaint();
			doLayout();		
			UILoaded = true;
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while setting UI : "+ex.getMessage());
		}
	}

	private void initNotification() {
		try{
			PanelNotificationSetting.setLayout(null);
			PanelNotificationSetting.setBounds(0, 0, 700, 700);
			PanelTable.add(PanelNotificationSetting, BorderLayout.CENTER);

			PanelNotificationSetting.setBackground(Color.WHITE);
			int x=160, y=100, h=20, gap=10, x1=250;

			lblModule.setText(AdminWise.connectorManager.getString("VWNotificationPanel.lblModule"));
			lblModule.setBounds(x, y, 80, h);
			PanelNotificationSetting.add(lblModule);
			if(languageLocale.equals("nl_NL")){
				cmbModule.setBounds(x1+70, y, 150, h);	
			}else
			cmbModule.setBounds(x1, y, 150, h);
			cmbModule.setBackground(Color.white);

			for(int i=2; i<NotificationModulesAdminWise.length; i++)	//i=2 - To avoid Folder and Docment modules in AdminWise
				cmbModule.addItem(NotificationModulesAdminWise[i]);

			PanelNotificationSetting.add(cmbModule);

			y += h+gap;
			lblName.setText(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("Notification.LBL.Name"));
			lblName.setBounds(x, y, 100, h);
			PanelNotificationSetting.add(lblName);
			if(languageLocale.equals("nl_NL")){
				cmbName.setBounds(x1+70, y, 150, h);	
			}else
			cmbName.setBounds(x1, y, 150, h);
			cmbName.setBackground(Color.white);
			PanelNotificationSetting.add(cmbName);

			y += h+gap;
			if(languageLocale.equals("nl_NL")){
				settingsPanel.setBounds(x-10, y, 388+150, 315);
			}else{
				settingsPanel.setBounds(x-10, y, 388, 315);
			}
			PanelNotificationSetting.add(settingsPanel);
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while initialising notification settings panel :: "+ex.getMessage());
		}
	}

	private void loadInitSettings() {
		try{
			int selIndex = (cmbModule.getSelectedIndex() + 2); // +2 is to avoid Document and Folder Notification Settings
			settingsPanel.updateSessionInfo(VWConnector.ViewWiseClient, gCurRoom, selIndex);
			settingsPanel.setNotifications(selIndex);
			switch(selIndex){
	
			case DOCUMENT_TYPE: loadDocTypes();
			break;
	
			case RETENTION: loadRetentionNames(); 
			break;
	
			case ROUTE: loadRouteNames(); 
			break;
	
			case STORAGE_MANAGEMENT:
			case AUDIT_TRAIL:
			case RECYCLE_DOCUMENTS:	
			case RESTORE_DOCUMENTS:
			case REDACTIONS:
				lblName.setVisible(false);
				cmbName.setVisible(false);
				if(languageLocale.equals("nl_NL")){
					settingsPanel.setBounds(150, 130, 388+100, 315);
				}else					
				settingsPanel.setBounds(150, 130, 388, 315);
				break;
			}
			cmbName.addItem(LBL_REFRESH_NAME);
			int nodeId = getNodeId(selIndex);
			loadSettings(nodeId, selIndex);
		}catch(Exception e){
			AdminWise.printToConsole("Exception in loadInitSettings() : "+e.getMessage());
		}
	}

	private void loadDocTypes() {
		try{
			cmbName.removeAllItems();
			cmbName.setVisible(true);
			lblName.setText(AdminWise.connectorManager.getString("VWNotificationPanel.DoclblName"));
			lblName.setVisible(true);
			if(languageLocale.equals("nl_NL")){
				settingsPanel.setBounds(150, 160, 388+150, 315);
			}else
			settingsPanel.setBounds(150, 160, 388, 315);
			Vector docTypes = VWDocTypeConnector.getDocTypes();
			if (docTypes != null && docTypes.size()>0){
				for (int i=0; i<docTypes.size(); i++){
					cmbName.addItem(docTypes.get(i));
				}
			}else{
				cmbName.addItem("");
			}
		}catch(Exception e){
			AdminWise.printToConsole("Exception while loading document types : "+e.getMessage());
			e.printStackTrace();
		}
	}

	private void loadRouteNames() {
		try{
			Vector vRouteList = new Vector();
			lblName.setText(ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("Notification.LBL.Name"));
			lblName.setVisible(true);
			if(languageLocale.equals("nl_NL")){
				settingsPanel.setBounds(150, 160, 388+150, 315);
			}else
			settingsPanel.setBounds(150, 160, 388, 315);
			 //To get all the route names pass -1 for routeId
			AdminWise.gConnector.ViewWiseClient.getRouteInfo(gCurRoom, -1, vRouteList);

			cmbName.removeAllItems();
			cmbName.setVisible(true);
			if(vRouteList != null && vRouteList.size()>0){
				for(int i=0; i<vRouteList.size(); i++){
					cmbName.addItem(vRouteList.get(i));
				}
			}else{
				cmbName.addItem("");
			}
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while loading route names :: "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void loadRetentionNames() {
		try{
			Vector vRetList = new Vector();
			lblName.setText(AdminWise.connectorManager.getString("VWNotificationPanel.loadRetentionNames"));
			lblName.setVisible(true);
			if(languageLocale.equals("nl_NL")){
				settingsPanel.setBounds(150, 160, 388+150, 315);
			}else
			settingsPanel.setBounds(150, 160, 388, 315);
			vRetList = VWRetentionConnector.getRetentionSettings(0);//AdminWise.gConnector.ViewWiseClient.getRetentionSettings(gCurRoom, 0, vRetList);
			cmbName.removeAllItems();
			cmbName.setVisible(true);

			if(vRetList != null && vRetList.size()>0){
				for(int i=0; i<vRetList.size(); i++){
					cmbName.addItem(vRetList.get(i));
				}
			}else{
				cmbName.addItem("");
			}
		}catch(Exception e){
			AdminWise.printToConsole("Exception while loading retention names :: "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void setEnableMode(int mode) {
		AdminWise.printToConsole("Mode :: "+mode);	
		switch (mode) {
		case MODE_CONNECT:
			CURRENT_MODE = 0;
			cmbUsersList.setEnabled(true);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setVisible(true);
			BtnDelete.setEnabled(false);			
			lblUsersList.setVisible(true);
			cmbUsersList.setVisible(true);
			BtnNotifications.setEnabled(true);
			BtnNotifications.setText(BTN_NOTIFICATION_NAME);
			BtnNotifications.setIcon(VWImages.NotifyIcon);
			loadUsers();
			//loadAvailableRetention();
			//viewMode(false);
			break;

		case MODE_UNSELECTED:
			BtnNotifications.setText(BTN_NOTIFICATION_NAME);
			BtnNotifications.setIcon(VWImages.NotifyIcon); 
			BtnNotifications.setEnabled(true);
			BtnUpdate.setEnabled(false);			
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(false);			
			BtnDelete.setVisible(true);
			lblUsersList.setVisible(true);
			cmbUsersList.setEnabled(true);
			cmbUsersList.setVisible(true);
			//viewMode(false);
			break;

		case MODE_SELECT:
			BtnNotifications.setEnabled(true);
			BtnNotifications.setText(BTN_NOTIFICATION_NAME);
			BtnNotifications.setIcon(VWImages.NotifyIcon);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(true);
			BtnDelete.setVisible(true);
			//viewMode(false);
			lblUsersList.setVisible(true);
			cmbUsersList.setVisible(true);
			
			NotificationSettings settings = Table.getRowData(Table.getSelectedRow());
			if(settings != null && (settings.getNodeType() == FOLDER || settings.getNodeType() == DOCUMENT))
					BtnUpdate.setEnabled(false);
				
			break;
			
		case MODE_DELETE:
			BtnNotifications.setEnabled(true);
			BtnNotifications.setText(BTN_NOTIFICATION_NAME);
			BtnNotifications.setIcon(VWImages.NotifyIcon);
			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(true);
			BtnDelete.setVisible(true);
			//viewMode(false);
			lblUsersList.setVisible(true);
			cmbUsersList.setVisible(true);
			break;

		case MODE_UNCONNECT:
			CURRENT_MODE = 0;
			cmbUsersList.removeAllItems();
			cmbUsersList.addItem(LBL_USERS_NAME);
			cmbUsersList.setVisible(true);

			BtnNotifications.setText(BTN_NOTIFICATION_NAME);
			BtnNotifications.setIcon(VWImages.NotifyIcon);
			BtnNotifications.setEnabled(false);
			BtnNotifications.setVisible(true);

			BtnUpdate.setEnabled(false);
			BtnUpdate.setVisible(true);
			BtnUpdate.setText(BTN_UPDATE_NAME);
			BtnUpdate.setIcon(VWImages.UpdateIcon);
			BtnDelete.setEnabled(false);
			BtnDelete.setVisible(true);
			
			lblUsersList.setVisible(true);
			cmbUsersList.setEnabled(false);
			gCurRoom = 0;			
			break;

		case MODE_NEW:
			BtnNotifications.setEnabled(true);
			BtnNotifications.setText(BTN_SAVE_NAME);
			BtnNotifications.setIcon(VWImages.SaveIcon);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			BtnDelete.setEnabled(false);
			//BtnNotifications.setIcon(null);
			lblUsersList.setVisible(false);
			cmbUsersList.setVisible(false);
			cmbModule.setEnabled(true);
			cmbName.setEnabled(true);
			//viewMode(true);			
			break;		
			
		case MODE_UPDATE:
			BtnNotifications.setEnabled(true);
			BtnNotifications.setText(BTN_SAVE_NAME);
			BtnNotifications.setIcon(VWImages.SaveIcon);
			BtnUpdate.setVisible(true);
			BtnUpdate.setEnabled(true);
			BtnUpdate.setText(BTN_CANCEL_NAME);
			BtnUpdate.setIcon(VWImages.CloseIcon);
			BtnDelete.setVisible(false);
			BtnDelete.setEnabled(false);
			lblUsersList.setVisible(false);
			cmbUsersList.setVisible(false);
			cmbModule.setEnabled(false);
			cmbName.setEnabled(false);
			//viewMode(true);			
			break;		
		}		
	}

	public void cmbModule_actionPerformed(ItemEvent event) {
		loadInitSettings();
	}

	public void loadTabData(VWRoom newRoom)
	{
		try{
			if(newRoom.getConnectStatus()!=Room_Status_Connect)
			{
				Table.clearData();
				//CmbUsers.removeAllItems();
				return;
			}
			if(newRoom.getId()==gCurRoom)return;
			gCurRoom=newRoom.getId();
			session = AdminWise.gConnector.ViewWiseClient.getSession(gCurRoom);
			loadUsers();
			loadInitSettings();
			SPTable.getViewport().add(Table);
			Table.clearData();
			UILoaded = true;
			setEnableMode(MODE_UNSELECTED);
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while loading tab data : "+ex.getMessage());
		}
	}

	private void loadUsers()
	{
		try{
			cmbUsersList.removeAllItems();
			Vector users = new Vector();
			AdminWise.gConnector.ViewWiseClient.getPrincipals(gCurRoom, users);
			cmbUsersList.addItem(LBL_USERS_NAME);
			
			if (users==null || users.size()==0) 
				return ;
			
			for(int i=0;i<users.size();i++){
				Principal user = (Principal) users.get(i);
				user.setTrimType(true);
				if(user != null && !user.isGroup()){
					cmbUsersList.addItem(user);
				}
			}
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while loading users : "+ex.getMessage());
		}
	}

	public void unloadTabData() {
		try{
		SPTable.getViewport().add(Table);
		//PanelNotificationSettings.setVisible(false);
		Table.clearData();
		repaint();
		gCurRoom = 0;
		setEnableMode(MODE_UNCONNECT);
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while unloading tab data: "+ex.getMessage());
		}
	}

	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == BtnNotifications) {
				BtnNotifications_actionPerformed(event);
			}else if (object == BtnUpdate) {
				BtnUpdate_actionPerformed(event);
			} else if (object == BtnDelete) {
				BtnDelete_actionPerformed(event);
			} 
		}
	}
	
	class SymItem implements java.awt.event.ItemListener {
		public void itemStateChanged(ItemEvent event) {
			try{
				Object object = event.getSource();
				int state = event.getStateChange();
				if(state == ItemEvent.SELECTED){
					if(object == cmbUsersList){
						cmbUsersList_actionPerformed(event);
					}
					else if(object == cmbModule){					
						cmbModule_actionPerformed(event);					
					}
					else if(object == cmbName){
						cmbName_actionPerformed(event);
					}
				}
			}catch(Exception e){
				AdminWise.printToConsole("Exception in item state changed :: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}

	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
			try {
				PanelNotificationSetting.repaint();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,e.toString());
			}
		}
	}

	class SymComponent extends java.awt.event.ComponentAdapter {
		public void componentResized(java.awt.event.ComponentEvent event) {
			Object object = event.getSource();
			if (object == VWNotificationSetting)
				VWNotification_componentResized(event);
		}
	}

	void VWNotification_componentResized(java.awt.event.ComponentEvent event) {
		if (event.getSource() == VWNotificationSetting) {
			Dimension mainDimension = this.getSize();
			Dimension panelDimension = mainDimension;
			Dimension commandDimension = PanelCommand.getSize();
			commandDimension.height = mainDimension.height;
			panelDimension.width = mainDimension.width - commandDimension.width;
			PanelTable.setSize(panelDimension);
			PanelCommand.setSize(commandDimension);
			SPTable.setSize(panelDimension);
			SPTable.revalidate();
			PanelTable.doLayout();								
		}
	}

	public void BtnUpdate_actionPerformed(ActionEvent event) {
		if(BtnUpdate.getText().equals(BTN_UPDATE_NAME)){
			int selectedRow = Table.getSelectedRow();
			int moduleId = -1, referenceId = -1;
			
			CURRENT_MODE = 1;
			stopGridEdit();

			if(selectedRow != -1){
				NotificationSettings settings = Table.getRowData(selectedRow);
				if(settings != null){
					moduleId = settings.getNodeType();
					referenceId = settings.getReferenceId();
					
					if(moduleId >= 2){
						cmbModule.setSelectedIndex(moduleId-2); //-2 is to avoid folder and document settings
						setCmbName(moduleId, referenceId);
						settingsPanel.loadUI(settings, getUserMailId());
						String userMailId = getUserMailId();
						settingsPanel.setUserMailId(userMailId);
						SPTable.getViewport().add(PanelNotificationSetting);
						PanelNotificationSetting.setVisible(true);
						setEnableMode(MODE_UPDATE);
					}					
				}
			}
		}
		else if(BtnUpdate.getText().equals(BTN_CANCEL_NAME)){
			SPTable.getViewport().add(Table);
			cmbUsersList.setSelectedIndex(0);
			PanelNotificationSetting.setVisible(false);
			//clearAll();
			CURRENT_MODE = 0;
			setEnableMode(MODE_CONNECT);
		}
		
	}

	public void cmbName_actionPerformed(ItemEvent event) {
		try{
			if(cmbName.getSelectedItem().toString().trim().equalsIgnoreCase(LBL_REFRESH_NAME)){
				loadInitSettings();
			}
			
			int selIndex = (cmbModule.getSelectedIndex() + 2); // +2 is to avoid Document and Folder Notification Settings
			int nodeId = getNodeId(selIndex);
			loadSettings(nodeId, selIndex);
		}catch(Exception e){
			AdminWise.printToConsole("Exception in cmbName_actionPerformed : "+e.getMessage());
		}
	}

	public void cmbUsersList_actionPerformed(ItemEvent event) {
		try{
			if(cmbUsersList.getSelectedIndex() == 0){
				Table.clearData();
				return;
			}
			
			Principal user = (Principal)cmbUsersList.getSelectedItem();
			int isAdmin = 0;
			String userName = "";
			
			Vector settings = new Vector();
			//set module to "-" to get notifications from all modules 
			String module = "-"; 
			int ret = 0;
			if(user != null){
				isAdmin = (user.isAdmin() == true ? 1 : 0);
				userName = user.getName();
				
				ret = AdminWise.gConnector.ViewWiseClient.getAllNotifications(gCurRoom, isAdmin, userName, module, settings);			
			}
			//Table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			Table.addData(settings);
			setEnableMode(MODE_UNSELECTED);
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while cmbUsersList action performed : "+ex.getMessage());
		}		
	}

	public void BtnDelete_actionPerformed(ActionEvent event) {
		try{
			if(BtnDelete.getText().equals(BTN_DELETE_NAME)){
				int retVal = VWMessage.showConfirmDialog(AdminWise.adminFrame,AdminWise.connectorManager.getString("VWNotificationPanel.deletemsg"));
				if (retVal == 0) {
					int[] selectedRows = Table.getSelectedRows();
					
					for(int i=0; i<selectedRows.length; i++){
						int row = selectedRows[i];
						NotificationSettings settings = Table.getRowData(row); 					
						int ret = AdminWise.gConnector.ViewWiseClient.deleteNotificationSettings(gCurRoom, settings);
					}
					Table.deleteSelectedRows();
					
					BtnUpdate.setEnabled(false);
					BtnDelete.setEnabled(false);
				}
			}
			
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while deleting notification : "+ex.getMessage());
		}
	}

	public void BtnNotifications_actionPerformed(ActionEvent event) {
		try{
			if(BtnNotifications.getText().equals(BTN_NOTIFICATION_NAME)) {
				int moduleId = -1, referenceId = -1;
				
				cmbModule.setSelectedIndex(0);
				if(cmbName.getItemCount() > 0)
					cmbName.setSelectedIndex(0);

				moduleId = DOCUMENT_TYPE;
				referenceId = getNodeId(DOCUMENT_TYPE);
				loadSettings(referenceId, moduleId);
				
				String userMailId = getUserMailId();
				settingsPanel.setUserMailId(userMailId);
				SPTable.getViewport().add(PanelNotificationSetting);
				PanelNotificationSetting.setVisible(true);
				
				setEnableMode(MODE_NEW);
			}
			else if(BtnNotifications.getText().equals(BTN_SAVE_NAME)){
				int nodeType = cmbModule.getSelectedIndex() + 2; //+2 is to avoid document and folder settings
				int nodeId = getNodeId(nodeType);
				
				NotificationSettings settings = new NotificationSettings();
				int ret = settingsPanel.getNotifictionSettings(settings);
				settings.setReferenceId(nodeId);
				settings.setNodeType(nodeType);
				settings.setUserName(getUserName());
				
				// Notify mail and Include options are not available
				settings.setProcessType(0);
				settings.setAttachment("00");
				String moduleName = SELECT_MODULE_NAME;//Select A Module
				
				if(nodeType==nodeType_DT)
					moduleName= moduleName.replace(AdminWise.connectorManager.getString("VWNotificationPanel.lblModule"),AdminWise.connectorManager.getString("VWNotificationPanel.moduleName_0")); 
				if(nodeType==nodeType_Retention)
					moduleName= moduleName.replace(AdminWise.connectorManager.getString("VWNotificationPanel.lblModule"),AdminWise.connectorManager.getString("VWNotificationPanel.moduleName_1"));
				if(nodeType==nodeType_Route)
					moduleName= moduleName.replace(AdminWise.connectorManager.getString("VWNotificationPanel.lblModule"),ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"));
				
				if(ret >= 0){
					if(cmbName.getSelectedItem().toString().trim().equals("") && 
							(nodeType == nodeType_DT || nodeType == nodeType_Retention || nodeType == nodeType_Route)){
						JOptionPane.showMessageDialog(this, moduleName, NOTIFI_TITLE, JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					ret = AdminWise.gConnector.ViewWiseClient.saveNotificationSettings(gCurRoom, settings);
					SPTable.getViewport().add(Table);
					cmbUsersList.setSelectedIndex(0);
					PanelNotificationSetting.setVisible(false);
					CURRENT_MODE = 0;
					setEnableMode(MODE_CONNECT);
				}
			}
		}catch(Exception e){
			AdminWise.printToConsole("Exception in BtnNotification_actionPerformed() :: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void setCmbName(int moduleId, int referenceId) {
		try{
			switch(moduleId){
			case DOCUMENT_TYPE: setDocTypeName(referenceId);
			break;

			case RETENTION: setRetentionName(referenceId);
			break;

			case ROUTE: setRouteName(referenceId);
			break;
			}						
		}catch(Exception e){
			AdminWise.printToConsole("Exception while setting CmbName : "+e.getMessage());
		}
	}

	private void setRouteName(int referenceId) {
		try{
			int recId;
			int itemCount = cmbName.getItemCount();
			for(int i=0; i<itemCount; i++){
				RouteInfo routeInfo = (RouteInfo) cmbName.getItemAt(i);
				if(routeInfo != null){
					recId = routeInfo.getRouteId();
					if(recId == referenceId){
						cmbName.setSelectedIndex(i);
					}
				}
			}
		}catch(Exception e){
			AdminWise.printToConsole("Exception while setting route Name : "+e.getMessage());
		}
	}

	private void setRetentionName(int referenceId) {
		try{
			int recId;
			int itemCount = cmbName.getItemCount();
			for(int i=0; i<itemCount; i++){
				VWRetention retention = (VWRetention) cmbName.getItemAt(i);
				if(retention != null){
					recId = retention.getId();
					if(recId == referenceId){
						cmbName.setSelectedIndex(i);
					}
				}
			}
		}catch(Exception e){
			AdminWise.printToConsole("Exception while setting Retention Name : "+e.getMessage());
		}
	}

	private void setDocTypeName(int referenceId) {
		try{
			int recId;
			int itemCount = cmbName.getItemCount();
			for(int i=0; i<itemCount; i++){
				VWDocTypeRec docTypeRec = (VWDocTypeRec) cmbName.getItemAt(i);
				if(docTypeRec != null){
					recId = docTypeRec.getId();
					if(recId == referenceId){
						cmbName.setSelectedIndex(i);
					}
				}
			}
		}catch(Exception e){
			AdminWise.printToConsole("Exception while setting DocType Name : "+e.getMessage());
		}
	}

	private String getUserMailId() {
		int ret = 0;
		String userMailId = ""; 
		Vector mailId = new Vector();
		try{
			ret = AdminWise.gConnector.ViewWiseClient.getLoggedInUserMailId(gCurRoom, mailId);
			if(mailId!=null && mailId.size()>0){
				if(mailId.get(0) != null){
					userMailId = (mailId.get(0) != null ? mailId.get(0).toString().trim() : "");
				}
			}
		}catch(Exception e){
			if(userMailId == null)
				userMailId = "";
		}
		return userMailId;
	}

	private String getUserName() {
		String userName = "";
		try{
			if(session != null)
				userName = session.user;
		}catch(Exception ex){
			userName = "";
		}
		return userName;
	}

	private int getNodeId(int nodeType) {
		int nodeId = 0; 
		try{
			switch(nodeType){
			case DOCUMENT_TYPE:	
				VWDocTypeRec docTypeRec = (VWDocTypeRec) cmbName.getSelectedItem();
				if(docTypeRec != null){
					nodeId = docTypeRec.getId();
				}
				break;
	
			case RETENTION: 
				VWRetention retention = (VWRetention) cmbName.getSelectedItem();
				if(retention != null){
					nodeId = retention.getId();
				}
				break;
	
			case ROUTE:
				RouteInfo routeInfo = (RouteInfo) cmbName.getSelectedItem();
				if(routeInfo != null){
					nodeId = routeInfo.getRouteId();
				}
				break;
			}
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while getting node id : "+ex.getMessage());
		}
		return nodeId;
	}
	
	public void loadSettings(int nodeId, int nodeType) {
		try{
			int ret = 0;
			String userName = getUserName();
			String module = NotificationModules[nodeType];
			NotificationSettings settings = new NotificationSettings();
			ret = AdminWise.gConnector.ViewWiseClient.loadNotificationSettings(gCurRoom, nodeId, userName, module, settings);
			AdminWise.printToConsole("Return value from loadNFSettings :: "+ret);
		
			if(settings != null){
				settingsPanel.loadUI(settings, getUserMailId());
			}
		}catch(Exception ex){
			AdminWise.printToConsole("Exception while loading settings : "+ex.getMessage());
		}
	}

	private void stopGridEdit() {
		try{
			Table.getCellEditor().stopCellEditing();
		}
		catch(Exception e){};
	}

	//------------------------------------------------------------------------------------------------------------

	public int CURRENT_MODE = 0;
	private static int gCurRoom = 0;
	private boolean UILoaded = false;
	public Session session = null;

	VWPicturePanel picPanel = new VWPicturePanel(TAB_NOTIFICATION_NAME, VWImages.NotifyImage);
	JPanel VWNotificationSetting = new JPanel();
	JPanel PanelCommand = new JPanel();
	JPanel PanelNotificationSetting = new JPanel();
	JPanel PanelTable = new JPanel();

	VWLinkedButton BtnNotifications = new VWLinkedButton(1);
	VWLinkedButton BtnUpdate = new VWLinkedButton(1);
	VWLinkedButton BtnDelete = new VWLinkedButton(1);
	
	JLabel lblModule = new JLabel();
	JLabel lblName = new JLabel();
	JLabel lblUsersList = new JLabel();
	VWComboBox cmbUsersList = new VWComboBox();
	VWComboBox cmbModule = new VWComboBox();
	VWComboBox cmbName = new VWComboBox();

	JScrollPane SPTable = new JScrollPane();
	VWNotificationTable Table = new VWNotificationTable();
	 String languageLocale=AdminWise.getLocaleLanguage();
	VWNotificationSettingsPanel settingsPanel = new VWNotificationSettingsPanel(VWConnector.ViewWiseClient,gCurRoom,DOCUMENT_TYPE);

}
