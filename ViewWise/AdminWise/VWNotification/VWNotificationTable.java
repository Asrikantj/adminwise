package ViewWise.AdminWise.VWNotification;


import java.io.File;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.event.TableModelEvent;

import com.computhink.common.Notification;
import com.computhink.common.NotificationSettings;
import com.computhink.common.util.VWRefCreator;

import java.awt.Dimension;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.prefs.Preferences;

import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWNotificationTable extends JTable implements VWConstant{

	protected VWNotificationTableData m_data;
	public VWNotificationTable(){
		super();
		getTableHeader().setReorderingAllowed(false);
		m_data = new VWNotificationTableData(this);
		setCellSelectionEnabled(false);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data); 
//		/setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		Dimension tableWith = getPreferredScrollableViewportSize();
		for (int k=0;k<VWNotificationTableData.m_columns.length;k++) 
		{
			TableCellRenderer renderer;
			DefaultTableCellRenderer textRenderer=new VWTableCellRenderer();
			textRenderer.setHorizontalAlignment(VWNotificationTableData.m_columns[k].m_alignment);
			renderer=textRenderer;
			VWNotificationEditor editor=new VWNotificationEditor(this);
			TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWNotificationTableData.m_columns[k].m_width),renderer,editor);
			addColumn(column);
		}
		setRowSelectionAllowed(true);
		setBackground(java.awt.Color.white);

		SymMouse aSymMouse = new SymMouse();
		addMouseListener(aSymMouse);
		JTableHeader header = getTableHeader();
		header.setUpdateTableInRealTime(false);
		header.addMouseListener(m_data.new ColumnListener(this));
		header.setReorderingAllowed(true);

		setRowHeight(TableRowHeight);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		getColumnModel().getColumn(0).setPreferredWidth(140);
		getColumnModel().getColumn(1).setPreferredWidth(180);
		getColumnModel().getColumn(2).setPreferredWidth(420);
		getColumnModel().getColumn(3).setPreferredWidth(108);    
		setTableResizable();
	}

	public void setTableResizable() {
//		Resize Table
		new VWTableResizer(this);
	}
//	------------------------------------------------------------------------------
	class SymMouse extends java.awt.event.MouseAdapter{
		public void mouseClicked(java.awt.event.MouseEvent event)
		{
			Object object = event.getSource();
			if (object instanceof JTable)
				if(event.getModifiers()==event.BUTTON3_MASK)
					VWNotificationTable_RightMouseClicked(event);
				else if(event.getModifiers()==event.BUTTON1_MASK)
					VWNotificationTable_LeftMouseClicked(event);
		}
		
		public void mouseReleased(java.awt.event.MouseEvent event){
			Object object = event.getSource();
			if(object instanceof JTable){
				int rowCount = getSelectedRowCount();
				
				if(rowCount == 1)
					AdminWise.adminPanel.notificationPanel.setEnableMode(MODE_SELECT);
				else if(rowCount > 1)
					AdminWise.adminPanel.notificationPanel.setEnableMode(MODE_DELETE);
				else
					AdminWise.adminPanel.notificationPanel.setEnableMode(MODE_UNSELECTED);
			}
		}
	}
//	------------------------------------------------------------------------------
	void VWNotificationTable_LeftMouseClicked(java.awt.event.MouseEvent event)
	{
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			lSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			lDoubleClick(event,row,column);
	}
//	------------------------------------------------------------------------------
	void VWNotificationTable_RightMouseClicked(java.awt.event.MouseEvent event)
	{
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			rSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			rDoubleClick(event,row,column);
	}
//	------------------------------------------------------------------------------
	private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
	{
		/*
			VWMenu menu=null;
			if(getSelectedRow()==-1) return;

			menu=new VWMenu(VWConstant.RefDocument_TYPE);
			menu.show(this,event.getX(),event.getY());
		 */
	}
//	------------------------------------------------------------------------------
	private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
	{

	}
//	------------------------------------------------------------------------------
	private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
	{
		/*if(getSelectedRow() != -1)
			AdminWise.adminPanel.notificationPanel.setEnableMode(MODE_SELECT);
		else
			AdminWise.adminPanel.notificationPanel.setEnableMode(MODE_UNSELECTED);*/
	}
//	------------------------------------------------------------------------------
	private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
	{
		//loadDocumentInDTC(row);
	}

//	------------------------------------------------------------------------------
	/**
	 * loadDocumentInDTC - This method is used to open the document in DTC
	 */
	private void loadDocumentInDTC(int row)
	{
		int docId = this.getRowDocId(row);
		VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
		String room = vwroom.getName();
		try {      
//			create a temp folder to place reference file in it
			File folder = new File("c:\\VWReferenceFile\\");
			folder.mkdirs();
//			create a reference file to open document
			File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);

//			open reference file in ViewWise thus opening doc                        
			try{
				Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
//				String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
				String dtcPath = VWUtil.getHome();
				Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
			}catch(Exception ex){
				ex.printStackTrace();
//				JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
			}
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}	
	}
//	------------------------------------------------------------------------------
	public void addData(List list)
	{
		m_data.setData(list);
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
	public void addData(String[][] data)
	{
		m_data.setData(data);
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
	public String[][] getData()
	{
		return m_data.getData();
	}
//	------------------------------------------------------------------------------
	public Vector getDocIds(String sep)
	{
		Vector list=new Vector();
		String[][] data = m_data.getData();
		int count=data.length;
		String docIds="";
		for(int i=0;i<count-1;i++)
		{
			docIds+=data[i][0]+sep;
			if(docIds.length()>7000)
			{
				list.add(docIds.substring(0,docIds.length()-sep.length()));
				docIds="";
			}
		}
		docIds+=data[count-1][0];
		list.add(docIds);
		return list;
	}
//	------------------------------------------------------------------------------
	public String[][] getReportData()
	{
		String[][] tableData=m_data.getData();
		int count=tableData.length;
		String[][] repData=new String[count][4];
		for(int i=0;i<count;i++)
		{
			repData[i][0]=tableData[i][1];
			repData[i][1]=tableData[i][2];
			repData[i][2]=tableData[i][3];
			repData[i][3]=tableData[i][4];
		}
		return repData;
	}
//	------------------------------------------------------------------------------
	public void removeData(int row) {
		m_data.remove(row);
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
	public void insertData(String[][] data)
	{
		for(int i=0;i<data.length;i++)
		{
			m_data.insert(new NotificationRowData(data[i]));
		}
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
	public void deleteSelectedRows()
	{
		int[] rows=getSelectedRows();
		int count=rows.length;
		for(int i=0;i<count;i++)
		{
			m_data.remove(rows[i]-i);
		}
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
	
	public NotificationSettings getRowData(int rowNum)
	{
		return m_data.getRowData(rowNum);
	}
	
//	------------------------------------------------------------------------------  
	public int getRowDocId(int rowNum)
	{
		String[] row=m_data.getNotificationRowData(rowNum);
//		Code is to get the document Id. The row[1] has doc id 
		return VWUtil.to_Number(row[1]);
	}
//	------------------------------------------------------------------------------
	public void clearData()
	{
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
}
class NotificationRowData
{
	public int   m_ModuleId;
	public int 	m_ReferenceId;
	public String   m_Module;	
	public String   m_Name;	
	public String 	m_Creator;
	public String   m_Condition;
	public String   m_LastAlert;
	public Notification[] m_notification;
	public int m_CaptureType;
	public String m_Emails;
	public int m_ProcessType;
	public String m_Attachment;
	//public int m_Inherit;
	public int m_FolderInherit;
	public int m_DocumentInherit;

	public NotificationRowData() {
		m_Module="";
		m_Name="";
		m_Condition="";
		m_LastAlert="";
	}
//	----------------------------------------------------------------------------
	public NotificationRowData(String module,String name,String condition,
			String lastAlert) 
	{
		m_Module = module;
		m_Name = name;
		m_Condition = condition;
		m_LastAlert = lastAlert;
	}
//	----------------------------------------------------------------------------
	public NotificationRowData(String str)
	{
		VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
		//tokens.nextToken();//NotificationId
		try
		{
			m_Module=tokens.nextToken();
		}
		catch(Exception e4){m_Module=AdminWise.connectorManager.getString("VWNotificationTable.NotificationRowData");}
		try
		{
			m_Name = tokens.nextToken();
		}
		catch(Exception e){m_Name=AdminWise.connectorManager.getString("VWNotificationTable.NotificationRowData");}
		try
		{
			m_Condition=tokens.nextToken();
		}
		catch(Exception e4){m_Condition=AdminWise.connectorManager.getString("VWNotificationTable.NotificationRowData");}
		try
		{
			m_LastAlert=tokens.nextToken();
		}
		catch(Exception e4){m_LastAlert=AdminWise.connectorManager.getString("VWNotificationTable.NotificationRowData");}

	}
//	----------------------------------------------------------------------------
	public NotificationRowData(String[] data) 
	{
		int i=0;
		int notificationId=0;

		//notificationId = VWUtil.to_Number(data[i++]);//Not required to display
		m_Module=data[i++];
		m_Name=data[i++];
		m_Condition=data[i++];
		m_LastAlert =data[i++];

	}
	public NotificationRowData(NotificationSettings settings) {
		try{
			m_Module = settings.getNotificationModule();
			m_ModuleId = settings.getNodeType();
			m_Name = settings.getNodeName();
			m_ReferenceId = settings.getReferenceId();
			m_Creator = settings.getUserName();
			m_Condition = settings.getAllNotificationEvents();
			m_LastAlert = settings.getLastNotifyTime();
			m_notification = settings.getNotifications();
			m_CaptureType = settings.getNotifyCaptureType();
			m_ProcessType = settings.getProcessType();
			m_Emails = settings.getEmails();
			m_Attachment = settings.getAttachment();
			m_FolderInherit = settings.getFolderInherit();
			m_DocumentInherit = settings.getDocumentInherit();
		}catch(Exception e){
			AdminWise.printToConsole("Exception in NotificationRowData :: "+e.getMessage());
			e.printStackTrace();
		}
	}
}
//------------------------------------------------------------------------------
class NotificationColumnData
{
	public String  m_title;
	float m_width;
	int m_alignment;

	public NotificationColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//------------------------------------------------------------------------------
class VWNotificationTableData extends AbstractTableModel 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final NotificationColumnData m_columns[] = {
		new NotificationColumnData(VWConstant.NotificationColumnNames[0],0.3F,JLabel.LEFT ),//Module
		new NotificationColumnData(VWConstant.NotificationColumnNames[1],0.2F,JLabel.LEFT),//Name
		new NotificationColumnData(VWConstant.NotificationColumnNames[2],0.2F,JLabel.LEFT),//Condition
		new NotificationColumnData(VWConstant.NotificationColumnNames[3],0.1F,JLabel.LEFT),//Last Alert
	};
	public static final int COL_NOTIFICATION_MODULE = 0;
	public static final int COL_NOTIFICATION_NAME = 1;
	public static final int COL_NOTIFICATION_CONDITION = 2;
	public static final int COL_NOTIFICATION_LASTALERT = 3;

	protected int m_sortCol = 0;
	protected boolean m_sortAsc = true;

	protected VWNotificationTable m_parent;
	protected Vector m_vector;

	public VWNotificationTableData(VWNotificationTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}
	public String[] getNotificationRowData(int rowNum) 
	{
		int count=getRowCount();
		String[] NotificationRowData=new String[4];
		int j=0;
		NotificationRowData row=(NotificationRowData)m_vector.elementAt(rowNum);

		NotificationRowData[j++]=row.m_Module;
		NotificationRowData[j++]=row.m_Name;
		NotificationRowData[j++]=row.m_Condition;
		NotificationRowData[j++]=row.m_LastAlert;
		return NotificationRowData;
	}
//	------------------------------------------------------------------------------
//	------------------------------------------------------------------------------
	public void setData(List data) {
		try{
			m_vector.removeAllElements();
			if (data==null) return;
			int count =data.size();
			for(int i=0;i<count;i++)
				m_vector.addElement(new NotificationRowData((NotificationSettings)data.get(i)));
		}catch(Exception e){
			AdminWise.printToConsole("Exception while setting data :: "+e.getMessage());
			e.printStackTrace();
		}
	}
//	------------------------------------------------------------------------------
	public void setData(String[][] data) {
		m_vector.removeAllElements();
		int count =data.length;
		for(int i=0;i<count;i++)
		{
			NotificationRowData row =new NotificationRowData(data[i]);
		}
	}
//	------------------------------------------------------------------------------
	public String[][] getData() {
		int count=getRowCount();
		String[][] data=new String[count][4];
		for(int i=0;i<count;i++)
		{
			NotificationRowData row=(NotificationRowData)m_vector.elementAt(i);
			data[i][0]=row.m_Module;
			data[i][1]=row.m_Name;
			data[i][2]=row.m_Condition;
			data[i][3]=row.m_LastAlert;			
		}
		return data;
	}
//	------------------------------------------------------------------------------
	public NotificationSettings getRowData(int rowNum) {
		NotificationSettings settings = new NotificationSettings();
		NotificationRowData row=(NotificationRowData)m_vector.elementAt(rowNum);
		settings.setNodeType(row.m_ModuleId);
		settings.setReferenceId(row.m_ReferenceId);
		settings.setUserName(row.m_Creator);
		settings.setNodeName(row.m_Name);
		settings.setLastNotifyTime(row.m_LastAlert);
		settings.setNotifications(row.m_notification);
		settings.setAttachment(row.m_Attachment);
		settings.setEmails(row.m_Emails);
		settings.setFolderInherit(row.m_FolderInherit);
		settings.setDocumentInherit(row.m_DocumentInherit);
		settings.setNotifyCaptureType(row.m_CaptureType);
		settings.setProcessType(row.m_ProcessType);
		
		return settings;
	}
//	------------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size(); 
	}
//	------------------------------------------------------------------------------
	public int getColumnCount() { 
		return m_columns.length; 
	} 
//	------------------------------------------------------------------------------
	public String getColumnName(int column) { 
		String str = m_columns[column].m_title;
		if (column==m_sortCol)
			str += m_sortAsc ? " �" : " �";
		return str; 
	}
//	------------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return true;
	}
//	------------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";

		NotificationRowData row = (NotificationRowData)m_vector.elementAt(nRow);

		switch (nCol) {
		case COL_NOTIFICATION_MODULE:     return row.m_Module;
		case COL_NOTIFICATION_NAME:     return row.m_Name;
		case COL_NOTIFICATION_CONDITION:     return row.m_Condition;
		case COL_NOTIFICATION_LASTALERT:    return row.m_LastAlert;
		}
		return "";
	}
//	------------------------------------------------------------------------------
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		NotificationRowData row = (NotificationRowData)m_vector.elementAt(nRow);
		String svalue = value.toString();

		switch (nCol) {
		case COL_NOTIFICATION_MODULE:
			row.m_Module = svalue; 
			break;
		case COL_NOTIFICATION_NAME:
			row.m_Name = svalue; 
			break;
		case COL_NOTIFICATION_CONDITION:
			row.m_Condition = svalue; 
			break;
		case COL_NOTIFICATION_LASTALERT:
			row.m_LastAlert = svalue; 
			break;			
		}
	}
//	------------------------------------------------------------------------------
	public void clear()
	{
		m_vector.removeAllElements();
	}
//	------------------------------------------------------------------------------
	public void insert(NotificationRowData AdvanceSearchRowData) 
	{
		m_vector.addElement(AdvanceSearchRowData);
	}
//	------------------------------------------------------------------------------
	public boolean remove(int row){
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
//	------------------------------------------------------------------------------
	class NotificationComparator implements Comparator {
		protected int     m_sortCol;
		protected boolean m_sortAsc;
//		--------------------------------------------------------------------------
		public NotificationComparator(int sortCol, boolean sortAsc) {
			m_sortCol = sortCol;
			m_sortAsc = sortAsc;
		}
//		--------------------------------------------------------------------------
		public int compare(Object o1, Object o2) {
			if(!(o1 instanceof NotificationRowData) || !(o2 instanceof NotificationRowData))
				return 0;
			NotificationRowData s1 = (NotificationRowData)o1;
			NotificationRowData s2 = (NotificationRowData)o2;
			int result = 0;
			double d1, d2;
			switch (m_sortCol) {
			case COL_NOTIFICATION_MODULE:
				result = s1.m_Module.toLowerCase().compareTo(s2.m_Module.toLowerCase());
				break;
			case COL_NOTIFICATION_NAME:
				result = s1.m_Name.toLowerCase().compareTo(s2.m_Name.toLowerCase());
				break;
			case COL_NOTIFICATION_CONDITION:
				result = s1.m_Condition.toLowerCase().compareTo(s2.m_Condition.toLowerCase());
				break;
			case COL_NOTIFICATION_LASTALERT:
				result = s1.m_LastAlert.toLowerCase().compareTo(s2.m_LastAlert.toLowerCase());
				break;
			}
			if (!m_sortAsc)
				result = -result;
			return result;
		}
//		--------------------------------------------------------------------------
		public boolean equals(Object obj) {
			if (obj instanceof NotificationComparator) {
				NotificationComparator compObj = (NotificationComparator)obj;
				return (compObj.m_sortCol==m_sortCol) &&
				(compObj.m_sortAsc==m_sortAsc);
			}
			return false;
		}
	} 

	class ColumnListener extends MouseAdapter {
		protected VWNotificationTable m_table;
//		--------------------------------------------------------------------------
		public ColumnListener(VWNotificationTable table){
			m_table = table;
		}
//		--------------------------------------------------------------------------
		public void mouseClicked(MouseEvent e){

			/*          if(e.getModifiers()==e.BUTTON3_MASK)
selectViewCol(e);
else */if(e.getModifiers()==e.BUTTON1_MASK)
	sortCol(e);
		}
//		--------------------------------------------------------------------------
		private void sortCol(MouseEvent e) {    	 
			TableColumnModel colModel = m_table.getColumnModel();
			int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
			int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

			if (modelIndex < 0) return;
			if (m_sortCol==modelIndex)
				m_sortAsc = !m_sortAsc;
			else
				m_sortCol = modelIndex;
			for (int i=0; i < colModel.getColumnCount();i++) {
				TableColumn column = colModel.getColumn(i);
				column.setHeaderValue(getColumnName(column.getModelIndex()));
			}
			m_table.getTableHeader().repaint();
			Collections.sort(m_vector, new NotificationComparator(modelIndex, m_sortAsc));
			m_table.tableChanged(
					new TableModelEvent(VWNotificationTableData.this));
			m_table.repaint();
		}
	} 
}

