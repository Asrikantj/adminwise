package ViewWise.AdminWise.VWTemplate;

import javax.swing.JPanel;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import java.util.prefs.*;
import java.awt.Component;
///import com.cct.util.SafeThread;
//------------------------------------------------------------------------------
public class VWTemplateInfoDlg extends javax.swing.JDialog implements  VWConstant, Constants
{
    public VWTemplateInfoDlg(Frame parent)
    {
            super(parent,ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("VWTemplateInfoDlg.Title"),false);
            ///parent.setIconImage(VWImages.RestoreIcon.getImage());
            //getContentPane().setLayout(null);
            getContentPane().setBackground(AdminWise.getAWColor());
            setSize(335,220);
            infoPanel.setLayout(null);
            getContentPane().add(infoPanel);
            BtnCancel.setText(AdminWise.connectorManager.getString("general.cancel"));
            //BtnCancel.setBackground(java.awt.Color.white);
            infoPanel.add(BtnCancel);
            BtnCancel.setBounds(120,144,91,23);
            BtnCancel.setIcon(VWImages.CloseIcon);
            infoPanel.add(PrgrsMain);
            PrgrsMain.setBounds(12,108,303,19);
            infoPanel.add(PrgrsDoc);
            PrgrsDoc.setBounds(12,48,303,19);
            infoPanel.add(LblMain);
            LblMain.setBounds(12,84,303,15);
            infoPanel.add(LblDoc);
            LblDoc.setBounds(12,24,303,15);
            PrgrsMain.setMaximum(100);
            PrgrsMain.setMinimum(0);
            LblMain.setText(AdminWise.connectorManager.getString("general.LblMain"));
            SymAction lSymAction = new SymAction();
            BtnCancel.addActionListener(lSymAction);
            getDlgOptions();
            setResizable(false);            
            SymWindow aSymWindow = new SymWindow();
            addWindowListener(aSymWindow);
            setVisible(true);
            new SaveTemplateThread();
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWTemplateInfoDlg.this)
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    //{{DECLARE_CONTROLS
    javax.swing.JPanel infoPanel = new javax.swing.JPanel();
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    javax.swing.JProgressBar PrgrsMain = new javax.swing.JProgressBar();
    javax.swing.JProgressBar PrgrsDoc = new javax.swing.JProgressBar();
    javax.swing.JLabel LblMain = new javax.swing.JLabel();
    javax.swing.JLabel LblDoc = new javax.swing.JLabel();
    //}}
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnCancel)
                    BtnCancel_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelAction=true;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x1", this.getX());
        prefs.putInt("y1", this.getY());
        prefs.putInt("width1", this.getSize().width);
        prefs.putInt("height1", this.getSize().height);
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x1",50),prefs.getInt("y1",50));
        setSize(prefs.getInt("width1",335),prefs.getInt("height1",210));
    }
//------------------------------------------------------------------------------
    public void setMainText(String text)
    {
        LblMain.setText(text);
    }
//------------------------------------------------------------------------------
    public void setDocText(String text)
    {
        LblDoc.setText(text);
    }
//------------------------------------------------------------------------------
 private class SaveTemplateThread extends Thread/*SafeThread*/
    {
        SaveTemplateThread()
        {
            setDaemon(true);
            ///setPriority(java.lang.Thread.MAX_PRIORITY);
            start();
        }
        public void run()
        {
            try{
                AdminWise.adminPanel.setWaitPointer();
                //if security is not checked, no need to update security details in vwt file.
                
                VWTemplateConnector.exportData(AdminWise.adminPanel.templatePanel.fileName,
                    AdminWise.adminPanel.templatePanel.tree.getLastPaths(),
                    AdminWise.adminPanel.templatePanel.docTypeTable.getData(),
                    AdminWise.adminPanel.templatePanel.isSecurityInclude ? AdminWise.adminPanel.templatePanel.tree.getSecurityInfo() : null,
                    AdminWise.adminPanel.templatePanel.isNodePropertiesInclude ? AdminWise.adminPanel.templatePanel.tree.getNodePropertiesInfo() : null,
                    AdminWise.adminPanel.templatePanel.isRetentionInclude ? AdminWise.adminPanel.templatePanel.loadAvailableRetentions():null,
                    AdminWise.adminPanel.templatePanel.isNotificationInclude ? AdminWise.adminPanel.templatePanel.loadAvailableNotifications():null,
                    (Component)AdminWise.adminPanel.templatePanel);
            }
            catch(Exception e){}
            finally{
                AdminWise.adminPanel.setDefaultPointer();
                ////stop();
                saveDlgOptions();
                setVisible(false);
            }            
            return ;
        }
        int parentNodeId=0;
 }
//------------------------------------------------------------------------------
    public boolean cancelAction=false;
}