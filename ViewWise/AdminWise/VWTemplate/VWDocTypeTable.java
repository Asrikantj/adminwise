/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWTemplate;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.Dimension;
import java.util.List;
import java.util.LinkedList;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
//------------------------------------------------------------------------------
public class VWDocTypeTable extends JTable implements VWConstant
{
    protected VWDocTypeTableData m_data;    
    public VWDocTypeTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWDocTypeTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        ///setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWDocTypeTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        
        DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWDocTypeTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWDocTypeRowEditor editor=new VWDocTypeRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWDocTypeTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {                
            }
        });
        setRowHeight(TableRowHeight);
        
        setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
		Object object = event.getSource();
		if (object instanceof JTable)
			if(event.getModifiers()==event.BUTTON3_MASK)
		        VWDocTypeTable_RightMouseClicked(event);
			else if(event.getModifiers()==event.BUTTON1_MASK)
				VWDocTypeTable_LeftMouseClicked(event);
	}
	}
//------------------------------------------------------------------------------
void VWDocTypeTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWDocTypeTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public Vector getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public void addData(VWDocTypeRec[] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
 public void insertData(VWDocTypeRec[] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(data[i]);
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
public void insertData(VWDocTypeRec data)
  {
        m_data.insert(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public VWDocTypeRec getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowDocTypeId(int rowNum)
  {
        VWDocTypeRec row=m_data.getRowData(rowNum);
        return row.getId();
  }
//------------------------------------------------------------------------------
  public int[] getRowDocTypeIds()
  {
        return m_data.getDataIds();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class DocTypeColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public DocTypeColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWDocTypeTableData extends AbstractTableModel 
{
  public static final DocTypeColumnData m_columns[] = {
    new DocTypeColumnData(VWConstant.TemplateColumnNames[0],0.4F,JLabel.LEFT ),
    new DocTypeColumnData(VWConstant.TemplateColumnNames[1],0.4F, JLabel.LEFT),
    new DocTypeColumnData(VWConstant.TemplateColumnNames[2],0.2F,JLabel.LEFT),
  };
  public static final int COL_DTNAME = 0;
  public static final int COL_KEYNAME = 1;
  public static final int COL_INDICESNO = 2;

  protected VWDocTypeTable m_parent;
  protected Vector m_vector;

  public VWDocTypeTableData(VWDocTypeTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(data.get(i));
    }
  }
//------------------------------------------------------------------------------
public void setData(VWDocTypeRec[] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(data[i]);
    }
  }
//------------------------------------------------------------------------------  
public Vector getData() {
    return m_vector;
  }
//------------------------------------------------------------------------------
  public int[] getDataIds() {
    int count=getRowCount();
    int[] data=new int[count];
    for(int i=0;i<count;i++)
    {
        VWDocTypeRec row=(VWDocTypeRec)m_vector.elementAt(i);
        data[i]=row.getId();
    }
    return data;
  }
//------------------------------------------------------------------------------
  public VWDocTypeRec getRowData(int rowNum) 
  {
    return (VWDocTypeRec)m_vector.elementAt(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    VWDocTypeRec row = (VWDocTypeRec)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_DTNAME:     return row.getName();
      case COL_KEYNAME:    return row.getKeyName();
      case COL_INDICESNO:  return row.getNumOfInices();
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    VWDocTypeRec row = (VWDocTypeRec)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_DTNAME:
        row.setName(svalue);
        break;
      case COL_KEYNAME:
        row.setKeyName(svalue);
        break;
      case COL_INDICESNO:
        row.setNumOfInices(svalue);
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(VWDocTypeRec rowData) 
    {
        m_vector.addElement(rowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
}