/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWTableConnector<br>
 *
 * @version     $Revision: 1.20 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWTemplate;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.List;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWRetention.VWRetentionConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWTree.*;
import ViewWise.AdminWise.VWArchive.VWArchiveConnector;
import java.awt.Frame;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import java.util.Hashtable;
import java.io.*;
import ViewWise.AdminWise.VWUtil.VWInfoFile;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import java.awt.Component;
import ViewWise.AdminWise.VWDocType.*;
import java.awt.Component;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import ViewWise.AdminWise.VWUtil.VWSAXParser;

import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.Constants;
import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.IndicesCondition;
import com.computhink.common.Node;
import com.computhink.common.NodeIndexRules;
import com.computhink.common.Notification;
import com.computhink.common.NotificationSettings;
import com.computhink.common.Util;
import com.computhink.common.VWRetention;
import com.computhink.resource.ResourceManager;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vwc.Session;

public class VWTemplateConnector implements VWConstant, Constants
{       
   public static Vector getDocTypes()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector docTypeInfo=new Vector();
        AdminWise.gConnector.ViewWiseClient.getDocTypeInfo(room.getId(),0,docTypeInfo);
        if(docTypeInfo==null || docTypeInfo.size()==0) return null;
        int count=docTypeInfo.size();
        Vector docTypesList=new Vector();
        for(int i=0;i<count;i++) docTypesList.add(new VWDocTypeRec((String)docTypeInfo.get(i),1));
        return docTypesList;
    }
//------------------------------------------------------------------------------
    public static int checkDocTypeIsFound(String docTypeName)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
         return AdminWise.gConnector.ViewWiseClient.isDocTypeFound(room.getId(), 
            new DocType(0,docTypeName));
    }
//------------------------------------------------------------------------------
    public static int checkIndexIsFound(VWIndexRec indexRec)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Index index=new Index(0);
        index.setName(indexRec.getName());
        index.setType(indexRec.getType());
        index.setMask(indexRec.getMask());
        index.setSystem(indexRec.getSVid());
        return AdminWise.gConnector.ViewWiseClient.isIndexFound(room.getId(),index,2);
    }   
//------------------------------------------------------------------------------
    public static int addUniqueNode(int ParentId,String nodeName,boolean checkOnly)
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        
        Node node=new Node(0,nodeName);
        node.setParentId(ParentId);        
        return AdminWise.gConnector.ViewWiseClient.createNode
            (room.getId(), node,(checkOnly?2:1));
    }
//------------------------------------------------------------------------------
    public static Vector loadDTTFromFile()
    {
        Vector docTypeList=new Vector();
        VWSAXParser parser = new VWSAXParser();
        parser.getParser(TFileName);
        templateDTData=parser.getDocumentInfo();
        if(templateDTData==null || templateDTData.size()==0) 
            return docTypeList;
        String prefix="ViewWiseTemplate\\";
        int recCount=getIntValue(prefix+"DTCount");
        if(recCount==0) return docTypeList;
        prefix+="VWDocType";
        for(int i=1;i<=recCount;i++)
        {
            prefix="ViewWiseTemplate\\VWDocType"+i+"\\";
            String recName=getValue(prefix+"Name");
            String recAutoMail_To=getValue(prefix+"AutoMail_To");
            String recAutoMail_Cc=getValue(prefix+"AutoMail_Cc");
            String recAutoMail_Subject=getValue(prefix+"AutoMail_Subject");
            String recSearchInContents=getValue(prefix+"SearchInContents");
            String recIndexCount=getValue(prefix+"IndexCount");
            String recKeyName=getValue(prefix+"KeyName");
            
            VWDocTypeRec docTypeRec=new VWDocTypeRec(recName,recAutoMail_To,recAutoMail_Cc,
                recAutoMail_Subject,recSearchInContents.equals("1") || recSearchInContents.equals("true"),recIndexCount,recKeyName);
            docTypeRec.VREnable=Integer.toString(getIntValue(prefix+"VREnable"));
            docTypeRec.VRInitVersion=Integer.toString(getIntValue(prefix+"VRInitVersion"));
            docTypeRec.VRInitRevision=Integer.toString(getIntValue(prefix+"VRInitRevision"));
            docTypeRec.VRPagesChange=Integer.toString(getIntValue(prefix+"VRPagesChange"));
            docTypeRec.VRIncPagesChange=Integer.toString(getIntValue(prefix+"VRIncPagesChange"));
            docTypeRec.VRPageChange=Integer.toString(getIntValue(prefix+"VRPageChange"));
            docTypeRec.VRIncPageChange=Integer.toString(getIntValue(prefix+"VRIncPageChange"));
            docTypeRec.VRPropertiesChange=Integer.toString(getIntValue(prefix+"VRPropertiesChange"));
            docTypeRec.VRIncPropertiesChange=Integer.toString(getIntValue(prefix+"VRIncPropertiesChange"));
            docTypeList.add(docTypeRec);
        }
            return docTypeList;
    }
//------------------------------------------------------------------------------
    public static Vector loadDFSFromFile()
    {
        Vector templateFSList=new Vector();
        String prefix="ViewWiseTemplate\\FileSystem\\";
        int recCount=getIntValue(prefix+"FolderCount");
        if(recCount==0) return templateFSList;
        Hashtable nodesTable=new Hashtable();
        maxNodeIndex=1;
        prefix="ViewWiseTemplate\\FileSystem\\Folder";
        for(int i=1;i<=recCount;i++)
        {
            String recStr=getValue(prefix+i);
            VWStringTokenizer tokens = new VWStringTokenizer(recStr,SepChar,false);
            int nodeParent=0;
            String strKey="";
            String nodeName="";
            VWTreeNode treeNode=null;
            tokens.nextToken();
            while(tokens.hasMoreTokens())
            {
                nodeName=tokens.nextToken().trim();
                strKey=Integer.toString(nodeParent)+nodeName;
                if(nodesTable.containsKey(strKey))
                {
                    treeNode=(VWTreeNode)nodesTable.get(strKey);
                    nodeParent=treeNode.getId();
                }
                else
                {
                    treeNode=new VWTreeNode(maxNodeIndex,nodeName);
                    treeNode.setParentId(nodeParent);
                    nodesTable.put(Integer.toString(nodeParent)+nodeName,treeNode);
                    templateFSList.add(treeNode);
                    nodeParent=maxNodeIndex;
                    maxNodeIndex++;
                }
            }
        }      
        return templateFSList;
    }
    
    public static Vector loadSecurityFromFile()
    {
        Vector templateSecurityList=new Vector();
        String prefix="ViewWiseTemplate\\Security\\";
        int recCount=getIntValue(prefix+"SecurityCount");
        AdminWise.printToConsole("Security Rec Count " + recCount);
        if(recCount==0) return templateSecurityList;
        Hashtable nodesTable=new Hashtable();
        prefix="ViewWiseTemplate\\Security\\VWSecurity";
        StringBuffer buffer = new StringBuffer();
        for (int count = 1; count <= recCount; count++){
        	 String recStr=getValue(prefix+count);        	 
        	 recStr=getValue(prefix+count + "\\Name");
        	 AdminWise.printToConsole("Security Name " + recStr);
        	 VWTreeNode node = new VWTreeNode(recStr);
        	 node.setName(recStr);
        	 buffer.append(recStr + ",");
        	 node.setVWPath(getValue(prefix+count + "\\Path"));
        	 int entryCount = Integer.parseInt(getValue(prefix+count + "\\AclEntryCount"));
        	 AdminWise.printToConsole("Entry Count " + entryCount);
        	 Vector securityInfo = new Vector();
        	 for(int i = 0 ; i < entryCount; i++){
        		 String sInfo = "";
        		 sInfo = getValue(prefix+ count + "\\UserGroupName" + i) + "\t" + getValue(prefix+ count + "\\Permission" + i);
        		 AdminWise.printToConsole("Security sInfo " + sInfo);
        		 securityInfo.add(sInfo);
        	 }
        	 node.setUserGroupSecurityList(securityInfo);
        	 AdminWise.printToConsole("Added the node object ");
        	 templateSecurityList.add(node);
        	 node = null;
        }
        AdminWise.printToConsole("Size " + templateSecurityList.size());
        AdminWise.printToConsole("buffer " + buffer);
        templateSecurityList.add(buffer);
        AdminWise.printToConsole("Size " + templateSecurityList.size());
        return templateSecurityList;
    }
    public static Vector loadNodePropertiesFromFile()
    {
        Vector templateNodePropertiesList=new Vector();
        String prefix="ViewWiseTemplate\\NodeProperties\\";
        int recCount=getIntValue(prefix+"NodePropertiesCount");
        //AdminWise.printToConsole("Node Properties Rec Count " + recCount);
        if(recCount==0) return templateNodePropertiesList;
        Hashtable nodesTable=new Hashtable();
        prefix="ViewWiseTemplate\\NodeProperties\\VWNodeProperties";
        StringBuffer buffer = new StringBuffer();
        for (int count = 1; count <= recCount; count++){
        	 String recStr=getValue(prefix+count);        	 
        	 recStr=getValue(prefix+count + "\\Name");
        	 //AdminWise.printToConsole("Node Properties Name " + recStr);
        	 VWTreeNode node = new VWTreeNode(recStr);
        	 node.setName(recStr);
        	 buffer.append(recStr + ",");
        	 node.setVWPath(getValue(prefix+count + "\\Path"));
        	 node.setDefaultDocType(getValue(prefix+count + "\\DefaultDocType"));
        	 int entryCount = Integer.parseInt(getValue(prefix+count + "\\IndexCount"));
        	 //AdminWise.printToConsole("Entry Count " + entryCount);
        	 Vector nodePropertiesInfo = new Vector();
        	 for(int i = 0 ; i < entryCount; i++){
        		 String sInfo = "";
        		 sInfo = getValue(prefix+ count + "\\Index" + i) + "\t" + getValue(prefix+ count + "\\Value" + i);
        		 //AdminWise.printToConsole("Node Prioperties Info " + sInfo);
        		 nodePropertiesInfo.add(sInfo);
        	 }
        	 node.setNodePropertiesList(nodePropertiesInfo);
        	 AdminWise.printToConsole("Added the node object ");
        	 templateNodePropertiesList.add(node);
        	 node = null;
        }
        //AdminWise.printToConsole("Size " + templateNodePropertiesList.size());
        //AdminWise.printToConsole("buffer " + buffer);
        templateNodePropertiesList.add(buffer);
        //AdminWise.printToConsole("Size " + templateNodePropertiesList.size());
        return templateNodePropertiesList;
    }    
    
  //------------------------------------------------------------------------------
    /***@author madhavan.b
     * Enhancement:- CV10.1 Method added to load the retention template from file.
     * @return
     */
    public static Vector loadRetentionFromFile() {
    	// TODO Auto-generated method stub
    	AdminWise.printToConsole("loadRetentionFromFile ::::::::::::::::::");
    	if(VWTemplatePanel.isRetentionInclude==true){
    		Vector templateRetentionList=new Vector();
    		String prefix="ViewWiseTemplate\\RetentionCount";
    		int recCount=Integer.parseInt(getValue(prefix));
    		AdminWise.printToConsole("recCount ::::::::::::::"+recCount);
    		if(recCount==0) return templateRetentionList;
    		Hashtable nodesTable=new Hashtable();
    		prefix="ViewWiseTemplate\\Retention";
    		StringBuffer buffer = new StringBuffer();
    		for (int count = 1; count <= recCount; count++){
    			String totalPrefix=prefix+count;
    			AdminWise.printToConsole("totalPrefix ::::::"+totalPrefix);
    			String retName=getValue(totalPrefix+"\\Name");     
    			AdminWise.printToConsole("recStr::::::::"+retName);
    			buffer.append(retName + ",");
    			String retSettings=getValue(totalPrefix+"\\Retentionsettings");  
    			AdminWise.printToConsole("retSettings :::::::"+retSettings);
    			int retIndexCount=Integer.parseInt(getValue(totalPrefix+"\\RetentionIndexcount"));
    			AdminWise.printToConsole("retIndexCount:::::::::::::::"+retIndexCount);
    			Vector indexInfo=new Vector();
    			for(int j=1;j<=retIndexCount;j++){
    				String retionIndexTagName=totalPrefix+"\\RetentionIndex"+j;
    				String retionIndexInfo=retionIndexTagName+"\\IndexInfo";
    				AdminWise.printToConsole("retionIndexInfo::::::"+retionIndexInfo);
    				AdminWise.printToConsole("getValue(retionIndexInfo) :::::"+getValue(retionIndexInfo));
    				indexInfo.add(getValue(retionIndexInfo));
    				AdminWise.printToConsole("indexInfo 1"+indexInfo);
    			}
    			RetentionTemplateData templatedata=new RetentionTemplateData(retName,retSettings,indexInfo);
    			templateRetentionList.add(templatedata);
    			templatedata=null;
    			AdminWise.printToConsole("indexInfo 2:::"+indexInfo);
    		}
    		//templateRetentionList.add(buffer);
    		return templateRetentionList;
    	}else
		return null;
    }
//------------------------------------------------------------------------------
    /***@author madhavan.b
     * Enhancement:- CV10.1 Method added to load the notification template from xml file.
     * @return
     */
    public static Vector loadNotificationFromFile() {
    	// TODO Auto-generated method stub
    	if(VWTemplatePanel.isNotificationInclude==true){
    		AdminWise.printToConsole("loadNotificationFromFile ::::::::::::::::::");
    		Vector templateNotificationList=new Vector();
    		String prefix="ViewWiseTemplate\\NotificationCount";
    		int recCount=Integer.parseInt(getValue(prefix));
    		AdminWise.printToConsole("notificationrecCount ::::::::::::::"+recCount);
    		if(recCount==0) return templateNotificationList;
    		Hashtable nodesTable=new Hashtable();
    		prefix="ViewWiseTemplate\\Notification";
    		StringBuffer buffer = new StringBuffer();
    		for (int count = 1; count <= recCount; count++){
    			String totalPrefix=prefix+count;
    			AdminWise.printToConsole("totalPrefix ::::::"+totalPrefix);
    			String notificationSettings=getValue(totalPrefix+"\\NotificationSettings");     
    			AdminWise.printToConsole("notificationSettings::::::::"+notificationSettings);
    			buffer.append(notificationSettings + ",");
    			NotificationSettings settings=new NotificationSettings();
    			try{
    				StringTokenizer notificationTokens = new StringTokenizer(notificationSettings, "\t");
    				settings.setId(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setReferenceId(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setNodeName(notificationTokens.nextToken());
    				settings.setNodeType(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setNotifyCaptureType(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setProcessType(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setFolderInherit(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setDocumentInherit(Integer.parseInt(notificationTokens.nextToken()));
    				settings.setUserName(notificationTokens.nextToken());
    				settings.setIpAddress(notificationTokens.nextToken());
    				settings.setAttachment(notificationTokens.nextToken());
    				settings.setEmails(notificationTokens.nextToken());
    				settings.setLastNotifyTime(notificationTokens.nextToken());
    			}catch(Exception e){
    				AdminWise.printToConsole("Exception while setting notification settings"+e.getMessage());
    			}

    			Vector moduleInfo=new Vector();
    			String moduleCount=totalPrefix+"\\NotificationModuleCount";
    			int totalModule=Integer.parseInt(getValue(moduleCount));
    			AdminWise.printToConsole("totalModule ::::"+totalModule);
    			Notification[] notifications = null;
    			Vector<Notification> temp = new Vector<Notification>();
    			for(int j=1;j<=totalModule;j++){
    				String strval=totalPrefix+"\\NotificationModule"+j;
    				AdminWise.printToConsole("strval ::::::"+strval);
    				String moduleString=getValue(strval+"\\ModuleInfo");
    				AdminWise.printToConsole("moduleString ::::::"+moduleString);
    				try{
    					StringTokenizer st = new StringTokenizer(moduleString, "\t");
    					while(st.hasMoreTokens()){
    						Notification notification = new Notification();
    						notification.setId(Integer.parseInt(st.nextToken()));
    						notification.setNotifyDesc(st.nextToken());
    						notification.setModule(st.nextToken());
    						notification.setSettingsId(Integer.parseInt(st.nextToken()));;
    						temp.add(notification);				
    					}

    				}catch(Exception e){
    					AdminWise.printToConsole("Exception while setting notification 2222::::"+e.getMessage());
    				}

    			}
    			if(temp != null && temp.size() > 0){
    				notifications = new Notification[temp.size()];
    				for(int i=0; i<temp.size(); i++){
    					notifications[i] = temp.get(i);
    				}
    			}
    			settings.setNotifications(notifications);
    			AdminWise.printToConsole("settings.getNotifcation lenght::::::::::"+settings.getNotifications().length);
    			templateNotificationList.add(settings);
    		}
    		return templateNotificationList;
    	}else
    		return null;
    }
  //------------------------------------------------------------------------------  
    
    
    
    //Vector retentionList,
    public static void importToRoom(Vector dttDataList,Vector fstDataList,Vector retentionList,Vector notification,Component component)
    {
        Vector errorList=new Vector();      
        errorList=importDTT(errorList,dttDataList);
       	errorList=importFST(errorList,fstDataList);
       	errorList=importRetention(errorList,retentionList);
       	errorList=importNotification(errorList,notification);
       	
        if(errorList.size()>0)
        {
            String logFileName=TFileName;
            while(!logFileName.endsWith("."))
            logFileName=logFileName.substring(0,logFileName.length()-1);
            String FileNoExt=logFileName;
            logFileName+="log";
            VWUtil.writeListToFile(errorList,logFileName,component);
            File importFile=new File(TFileName);  
            File newFile=new File(FileNoExt + "bak1");
            if(importFile.exists()) importFile.renameTo(newFile);
        }
    }
/*
Issue No / Purpose:  <644/Removing items from a template in the template tab prior to applying the template to a room can cause document corruption>
Created by: <Pandiya Raj.M>
Date: <07 Jul 2006>
Add a new method "getPosition()" to get the document type position from the 'template file'(.vwt) 
*/
    private static int getPosition(String dtName){
    	int currentPosition = 0;
    	List dataListFromFile =VWTemplateConnector.loadDTTFromFile();
    	for(int count=0;count<dataListFromFile.size();count++){
    		VWDocTypeRec docTypeRec=(VWDocTypeRec)dataListFromFile.get(count);
    		if (docTypeRec.getName().equals(dtName)){
    			currentPosition = count;    			
    			return ++currentPosition;
    		}
    	}
    	return currentPosition;
    }
//------------------------------------------------------------------------------
    public static Vector importDTT(Vector errorList,Vector dataList)
    {
        int count=dataList==null?0:dataList.size();        
        for(int i=0;i<count;i++)
        {
            VWDocTypeRec docTypeRec=(VWDocTypeRec)dataList.get(i);
            if(checkDocTypeIsFound(docTypeRec.getName())>0)
            {
                errorList.add(AdminWise.connectorManager.getString("VWTemplateConnector.errList_1")+" " + docTypeRec.getName() +" "+AdminWise.connectorManager.getString("VWTemplateConnector.errList_2"));
                continue;
            }
//get the correct position of the current document type
            int selectedDT = getPosition(docTypeRec.getName());
            Vector indicesList=new Vector();
            int dtindexCount=VWUtil.to_Number(docTypeRec.getNumOfInices());
            String indexFail="";
            for(int j=0;j<dtindexCount;j++)
            {
                if(docTypeRec.getId()==0)
                {
// Pass the 'selectedDT' as a position of the document type.
                    VWIndexRec indexRec=getIndexRecFromFile(selectedDT,j+1);
                    int retCheck=checkIndexIsFound(indexRec);
                    if(retCheck==0)
                    {
                        indicesList.add(indexRec);
                    }
                    else if (retCheck>0)
                    {
                    	if (retCheck  == 999999999){
                    		VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
                    		Index index = new Index(0);
                    		index.setName(indexRec.getName());
                    		index.setMask(indexRec.getMask());
                    		index.setType(indexRec.getType());
                    		int indexId = AdminWise.gConnector.ViewWiseClient.getIndexId(room.getId(), index);
                    		indexRec.setId(indexId);
                    	}else{
                    		indexRec.setId(retCheck);
                    	}
                        indicesList.add(indexRec);
                    }
                    else
                    {
                        indexFail=indexRec.getName();
                        continue;
                    }
                }
            }
            if(!indexFail.equals(""))
            {
               errorList.add("Index field "+indexFail+" exists with mismatched properties");
                continue; 
            }
            docTypeRec.setId(VWDocTypeConnector.updateDocType(docTypeRec));
            for(int k=0;k<dtindexCount;k++)
            {
                VWIndexRec indexRec=(VWIndexRec)indicesList.get(k);
                if(indexRec.getId()==0)
                {
                    if(indexRec.getType()==Index.IDX_SELECTION)
                    {
                        String selValues=(String)templateDTData.get("ViewWiseTemplate\\VWDocType"+(selectedDT)+"\\DTIndex"+(k+1)+"\\SelectionValues");
                        indexRec.setIndexSelectionValuesStr(selValues);
                    }
                    indexRec.setId(VWDocTypeConnector.updateIndex(indexRec));
                }
            // Pass the correct position of the document type.
                indexRec=updateIndexToDTIndex(indexRec,selectedDT,k+1);
                VWDocTypeConnector.updateDTIndex(docTypeRec.getId(),indexRec,k+1);             
            }
            if(!docTypeRec.getAutoMail_To().equals("")||
                !docTypeRec.getAutoMail_Cc().equals("")||
                !docTypeRec.getAutoMail_Subject().equals(""))
            {
                String autoMail_To=docTypeRec.getAutoMail_To();
                int sepIndex=0;
                if((sepIndex=autoMail_To.indexOf("|"))>0)
                {
                    String indexName=autoMail_To.substring(0,sepIndex);
                    for(int s=0;s<dtindexCount;s++)
                    {
                        VWIndexRec indexRec=(VWIndexRec)indicesList.get(s);
                        if(indexName.equalsIgnoreCase(indexRec.getName()))
                        {
                            docTypeRec.setAutoMail_To(indexRec.getName()+"|"+indexRec.getId());
                            break;
                        }
                    }
                }
                String autoMail_Cc=docTypeRec.getAutoMail_Cc();
                sepIndex=0;
                if((sepIndex=autoMail_Cc.indexOf("|"))>0)
                {
                    String indexName=autoMail_Cc.substring(0,sepIndex);
                    for(int s=0;s<dtindexCount;s++)
                    {
                        VWIndexRec indexRec=(VWIndexRec)indicesList.get(s);
                        if(indexName.equalsIgnoreCase(indexRec.getName()))
                        {
                            docTypeRec.setAutoMail_Cc(indexRec.getName()+"|"+indexRec.getId());
                            break;
                        }
                    }
                }
                String autoMail_Subject=docTypeRec.getAutoMail_Subject();
                sepIndex=0;
                if(autoMail_Subject.indexOf("|")>0)
                {
                    VWStringTokenizer tokens = new VWStringTokenizer(autoMail_Subject,"|",false);
                    autoMail_Subject="";
                    while (tokens.hasMoreTokens())
                    {
                        String indexName=tokens.nextToken();
                        indexName=indexName.substring(1,indexName.length()-1);
                        for(int s=0;s<dtindexCount;s++)
                        {
                            VWIndexRec indexRec=(VWIndexRec)indicesList.get(s);
                            if(indexName.equalsIgnoreCase(indexRec.getName()))
                            {
                                autoMail_Subject+="["+indexRec.getName()+"]"+"|"+indexRec.getId()+"|";
                                break;
                            }
                        }
                        tokens.nextToken();
                    }
                    if(autoMail_Subject.endsWith("|"))
                        autoMail_Subject=autoMail_Subject.substring(0,autoMail_Subject.length()-1);
                    docTypeRec.setAutoMail_Subject(autoMail_Subject);
                    }
                VWDocTypeConnector.updateDocTypeAutoMail(docTypeRec);  
            }
            DocType docType =new DocType(docTypeRec.getId());
            docType.setVREnable(docTypeRec.VREnable);
            docType.setVRInitVersion(docTypeRec.VRInitVersion);
            docType.setVRInitRevision(docTypeRec.VRInitRevision);
            docType.setVRPagesChange(docTypeRec.VRPagesChange);
            docType.setVRIncPagesChange(docTypeRec.VRIncPagesChange);
            docType.setVRPageChange(docTypeRec.VRPageChange);
            docType.setVRIncPageChange(docTypeRec.VRIncPageChange);
            docType.setVRPropertiesChange(docTypeRec.VRPropertiesChange);
            docType.setVRIncPropertiesChange(docTypeRec.VRIncPropertiesChange);
            VWDocTypeConnector.setDocTypeVerRev(docType);
        }
        templateDTData=null;
        return errorList;
    }
//------------------------------------------------------------------------------
    public static Vector importFST(Vector errorList,Vector templateFSList)
    {
    	int count=templateFSList==null?0:templateFSList.size();
    	Vector indicesList =  new Vector();
    	Vector docTypes = new Vector();
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();  
    	try{
    		if(VWTemplatePanel.isNodePropertiesInclude){
    			int ret = AdminWise.gConnector.ViewWiseClient.getAllDocTypeIndices(room.getId(), 0, indicesList);
    			int retDT = AdminWise.gConnector.ViewWiseClient.getDocTypes(room.getId(),docTypes);
    		}
    	}catch(Exception ex){errorList.add("Error while loading the document type and indices for Node Properties!");}
    	//AdminWise.printToConsole("Data on DT : " + docTypes);
    	for(int i=0;i<count;i++)
    	{
    		try{
    			VWTreeNode treeNode=(VWTreeNode)templateFSList.get(i);
    			//AdminWise.printToConsole("VWPath " + treeNode.getVWPath());
    			Vector security = treeNode.getUserGroupSecurityList();
    			//if (security != null)
    			//AdminWise.printToConsole("Security info " + security.size());
    			int nodeId=addUniqueNode(treeNode.getParentId(),treeNode.getName(),false);
    			// If include security is not checked, don't do security copy.
    			if(VWTemplatePanel.isSecurityInclude){
    				if (nodeId > 0 && (security != null && security.size() > 0)){

    					for (int sCount =0 ; sCount< security.size(); sCount++){
    						StringTokenizer tokens = new StringTokenizer(security.get(sCount).toString(), "\t");
    						String userGroup = tokens.nextToken();
    						String permission = tokens.nextToken();
    						int result =AdminWise.gConnector.ViewWiseClient.copyNodeSecurity(room.getId(), nodeId,  userGroup, permission);
    						switch(result){
    						case 0:
    							errorList.add("Successfully Adding Security Entry for the node " + treeNode.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permission);
    							break;
    						case -1:
    							errorList.add(treeNode.getName() + " : User/Group Name -> " + userGroup + " not found!");
    							break;
    						case -2:
    							errorList.add(treeNode.getName() + " : User/Group Name -> " + userGroup + " : Principal not found!");	            			
    							break;
    						case -3: 
    							errorList.add(treeNode.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permission + " : Given Permission and effective Permission are same.");
    							break;
    						case -4:
    							errorList.add(treeNode.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permission + " : Given permission same as effective permission, remove the existing security permission!");
    							break;
    						default:
    							errorList.add("Error While Adding Security Entry for the node " + treeNode.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permission);	            			
    						}	            				 
    					}

    				}
    			}
    			if(VWTemplatePanel.isNodePropertiesInclude){
    				Vector nodePropertiesList = treeNode.getNodePropertiesList();
    				String defaultdocType = treeNode.getDefaultDocType();
    				int docTypeId = -1;
    				if ( docTypes != null && docTypes.size() > 0){
    					for(int cnt=0; cnt < docTypes.size(); cnt++){
    						DocType dType = (DocType)docTypes.get(cnt);
    						if (dType.getName().equals(defaultdocType.trim())){            		
    							docTypeId = dType.getId();
    							break;
    						}            		
    					}
    				}
    				if (nodePropertiesList != null && nodePropertiesList.size() > 0 &&  indicesList != null && indicesList.size() > 0){            	            
    					NodeIndexRules rulesObject = new NodeIndexRules();
    					rulesObject.setNodeId(nodeId);
    					ArrayList indicesRulesList = new ArrayList();
    					//AdminWise.printToConsole("nodeId     " + nodeId);
    					for(int cnt=0; cnt < nodePropertiesList.size(); cnt++){
    						StringTokenizer tokens = new StringTokenizer(nodePropertiesList.get(cnt).toString(), "\t");
    						String idxName = tokens.nextToken();
    						String idxValue = tokens.nextToken();
    						int idxId = 0;
    						//AdminWise.printToConsole("Index name    *" + idxName+"*");
    						//AdminWise.printToConsole("Index Value    *" + idxValue+"*");
    						for (int idxCnt = 0; idxCnt < indicesList.size() && idxId == 0; idxCnt++){	            		
    							Index sIdx = (Index)indicesList.get(idxCnt);	            		
    							if (sIdx.getName().equals(idxName)){
    								idxId = sIdx.getId();
    								break;
    							}
    						}	            	
    						if (idxId != 0){
    							indicesRulesList.add(idxId + Util.SepChar + idxValue.trim());	            		
    							//AdminWise.printToConsole("Selected Index Id  " + idxId);
    						}
    					}
    					//AdminWise.printToConsole("indicesRulesList " + indicesRulesList);            
    					//AdminWise.printToConsole("IndexOf " + indicesList.indexOf(treeNode.getName()));
    					if (docTypeId != -1 || indicesRulesList.size() > 0) {
    						rulesObject.setDocTypeId(docTypeId);
    						rulesObject.setIndicesRulesList(indicesRulesList);
    						rulesObject.setIndicesCount(nodePropertiesList.size());        
    						//System.out.println("Count " + countIndex);	            
    						int result = AdminWise.gConnector.ViewWiseClient.updateNodeIndexRules(room.getId(), rulesObject);
    						if (result != 0)
    							errorList.add("Error While Adding Node Properties for the node " + treeNode.getName());
    						else
    							errorList.add("Successfully Adding Node Properties for the node " + treeNode.getName());
    					}
    				}else if(docTypeId != -1){
    					/*
    					 * This is fix for notification 1197 - Node property for the folder is not copied when they try coping folder structure using the templates tab
    					 * Even though node properties list is empty, need to create node index rules for the selection of default document type. 
    					 */
    					NodeIndexRules rulesObject = new NodeIndexRules();
    					rulesObject.setNodeId(nodeId);
    					rulesObject.setDocTypeId(docTypeId);
    					rulesObject.setIndicesRulesList(null);
    					rulesObject.setIndicesCount(0);        
    					//System.out.println("Count " + countIndex);	            
    					int result = AdminWise.gConnector.ViewWiseClient.updateNodeIndexRules(room.getId(), rulesObject);
    					if (result != 0)
    						errorList.add("Error While Adding Node Properties for the node " + treeNode.getName());
    					else
    						errorList.add("Successfully Adding Node Properties for the node " + treeNode.getName());
    				}
    			}
    			if(nodeId<=0)
    			{
    				errorList.add("Error while adding node "+treeNode.getName()+"");
    				break;
    			}
    			for(int k=i+1;k<count;k++)
    			{
    				int parentId=((VWTreeNode)templateFSList.get(k)).getParentId();
    				//errorList.add("Node Name " + ((VWTreeNode)templateFSList.get(k)).getName() + " : " + parentId); 
    				if(parentId==treeNode.getId() && !((VWTreeNode)templateFSList.get(k)).isNodeUpdate()  ){
    					try{
    						((VWTreeNode)templateFSList.get(k)).setParentId(nodeId);
    						((VWTreeNode)templateFSList.get(k)).setNodeUpdate(true);
    						//errorList.add("Set the Parent Id for the Node Name " + ((VWTreeNode)templateFSList.get(k)).getName() + " : " + ((VWTreeNode)templateFSList.get(k)).getParentId());
    					}catch(Exception ex){
    						errorList.add("Error on the set the Parent Id : " + ex.getMessage());
    					}
    				}
    			}
    			treeNode.setId(nodeId);
    		}catch(Exception ex){errorList.add("Exception in adding the Node " + i + " : " + ex.getMessage());}
    	}
    	templateFSList=null;
    	return errorList;
    }
 //-------------------------------------------------------------------------------------------
    /**@author madhavan.b
     * Enhancement :- CV10.1 Retention template 
     * @param errorList
     * @param retentionList
     * @return
     */
    private static Vector importRetention(Vector errorList, Vector retentionList) {
    	// TODO Auto-generated method stub
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	int count=retentionList==null?0:retentionList.size();
    	if(VWTemplatePanel.isRetentionInclude==false){
    		AdminWise.printToConsole("Inside retention is false::::::::");
    		return errorList;
    	}
    	if(retentionList==null&&retentionList.size()==0)
    		return errorList;
    	try{
    	for(int i=0;i<=retentionList.size();i++)
    	{
    		boolean flag=false;
    		Vector modifiedIndexVect=new Vector();
    		int newRetentionId=0;
    		IndicesCondition indicesConditionBean =null;
    		RetentionTemplateData retentiontemplateData=(RetentionTemplateData)retentionList.get(i);
    		String retentionName= retentiontemplateData.getRetentionName();
    		Vector retentionIndex=retentiontemplateData.getRetentionIndex();
    		String retentionSettings=retentiontemplateData.getRetentionSettings();
    		AdminWise.printToConsole(" retentionData.getRetentionIndex()::::"+retentionIndex );
    		AdminWise.printToConsole("retentionData.getRetentionSettings()"+retentionSettings);
    		AdminWise.printToConsole("retentionName::::::::::::"+retentionName);
    		int isExist = AdminWise.gConnector.ViewWiseClient.isRetentionNameFound(room.getId(),retentionName);
    		session = AdminWise.gConnector.ViewWiseClient.getSession(room.getId());
    		if(isExist > 0){
    			errorList.add("Retention : "+retentionName+" Already exist in room::::::::::");
    			continue;
    		}else{
    		if(retentionIndex.size()>0&&retentionIndex!=null){
    			AdminWise.printToConsole("Inside null conditon ");
    			for(int j=0;j<retentionIndex.size();j++){
    				String totalIndex=retentionIndex.get(j).toString();
    				String splitIndex[]=totalIndex.split(Util.SepChar);
    				int doctypeId=Integer.parseInt(splitIndex[0]);
    				String docTypeName=splitIndex[1];
    				int indexId=Integer.parseInt(splitIndex[2]);
    				String indexName=splitIndex[3];
    				if(isExist <= 0){
    					int modifiedDoctypeId=AdminWise.gConnector.ViewWiseClient.isRetentionDocTypeFound(room.getId(),docTypeName);
    					if(doctypeId>0) {
    						if(doctypeId!=modifiedDoctypeId){
    							splitIndex[0]=String.valueOf(modifiedDoctypeId);
    						}
    						int modifiedIndexId= AdminWise.gConnector.ViewWiseClient.isRetentionDTIndexFound(room.getId(),splitIndex[0],indexName);
    						AdminWise.printToConsole("Modified index id :::::::"+modifiedIndexId);
    						if(indexId>0){
    							if(indexId!=modifiedIndexId){
    								splitIndex[2]=String.valueOf(modifiedIndexId);

    							}else if(indexId==-1){
    								splitIndex[2]="Created Date";
    							}else if(indexId==-2)
    								splitIndex[2]="Modified Date";
    						}
    					}else{
    						errorList.add("Error  Retention Doctype not exist in Room ");
    						break;
    					}
    				}
    				AdminWise.printToConsole("splitIndex[2]:::::::"+splitIndex[2]);
    				
    				String splitSettings[]=retentionSettings.split(Util.SepChar);

    				String nodePath=splitSettings[splitSettings.length-2].trim();
    				int nodeId=0;
    				if(splitSettings[splitSettings.length-2].length()>0){
    					nodeId=AdminWise.gConnector.ViewWiseClient.checkCVSearchNode(room.getId(),nodePath);
    					AdminWise.printToConsole("nodeId  after passing node path:::::::::::"+nodeId); 
    					if(nodeId>0){
    						splitSettings[splitSettings.length-3]=String.valueOf(nodeId);
    					}else if(nodeId==0){
    						splitSettings[splitSettings.length-3]="0";
    					}
    				}
    				int actioId=Integer.parseInt(splitSettings[4]);
    				AdminWise.printToConsole("actionId :::::::::::::::"+actioId);
    				
    				if(actioId==1){
    					String moveNodePath=splitSettings[splitSettings.length-1].trim();
    					int moveNodeId=0;
    					if(moveNodePath.length()>0){
    						moveNodeId=AdminWise.gConnector.ViewWiseClient.checkCVSearchNode(room.getId(),moveNodePath);
    						AdminWise.printToConsole("moveNodeId  after passing node path:::::::::::"+moveNodeId); 
    						if(moveNodeId>0){
    							splitSettings[5]=String.valueOf(moveNodeId);
    						}else if(moveNodeId==0){
    							splitSettings[5]="0";
    						}
    					}
    				}
    				String modifiedRetentionSetting="";
    				for(int retRecCount=0;retRecCount<splitSettings.length;retRecCount++){
    					if(modifiedRetentionSetting.length()>0){
    						modifiedRetentionSetting=modifiedRetentionSetting+Util.SepChar+splitSettings[retRecCount]+Util.SepChar;
    					}else{
    						modifiedRetentionSetting=splitSettings[retRecCount]+Util.SepChar; 
    					}
    				}
    				AdminWise.printToConsole("modifiedRetentionSetting ::::::::::::::::::"+modifiedRetentionSetting);
    				VWRetention retention=new VWRetention(modifiedRetentionSetting,session.user);
    				retention.setId(0);
    				retention.setDocTypeId(Integer.parseInt(splitIndex[0]));
    				Vector retentionId=new Vector();
    				int isExist1 = AdminWise.gConnector.ViewWiseClient.isRetentionNameFound(room.getId(),retentionName);
    				if(isExist1<=0){
    					newRetentionId= AdminWise.gConnector.ViewWiseClient.setRetentionSettings(room.getId(),retention,retentionId);
    				}
    				AdminWise.printToConsole("newRetentionId:::::::::::::"+newRetentionId);
    				String modifiedretentionIdx="";
    				for(int k=0;k<splitIndex.length;k++){
    					if(modifiedretentionIdx.length()>0){
    						modifiedretentionIdx=modifiedretentionIdx+Util.SepChar+splitIndex[k]+Util.SepChar;
    					}else{
    						modifiedretentionIdx=splitIndex[k]+Util.SepChar;
    					}
    				}
    				AdminWise.printToConsole("modifiedretentionIdx::::::::::"+modifiedretentionIdx);
    				if(modifiedretentionIdx.length()>0){
    					retention.setId(newRetentionId);
    					indicesConditionBean = new IndicesCondition(true,modifiedretentionIdx);
    					modifiedIndexVect.add(indicesConditionBean);
    					if(j==retentionIndex.size()-1){
    						AdminWise.printToConsole("modifiedIndexVect :::::::::::::::::"+modifiedIndexVect);
    						retention.setIndicesCondition(modifiedIndexVect);
    						AdminWise.gConnector.ViewWiseClient.setIndexConditions(room.getId(),retention);
    					}
    				}
    				AdminWise.printToConsole("modifiedretentionIndex::::::"+modifiedIndexVect);
    			}
    		}else{
    			AdminWise.printToConsole("Inside else");
    			String splitSettings[]=retentionSettings.split(Util.SepChar);

    			String nodePath=splitSettings[splitSettings.length-2].trim();
    			int nodeId=0;
    			if(splitSettings[splitSettings.length-2].length()>0){
    				nodeId=AdminWise.gConnector.ViewWiseClient.checkCVSearchNode(room.getId(),nodePath);
    				AdminWise.printToConsole("nodeId  after passing node path in else:::::::::::"+nodeId); 
    				if(nodeId>0){
    					splitSettings[splitSettings.length-3]=String.valueOf(nodeId);
    				}else if(nodeId==0){
    					splitSettings[splitSettings.length-3]="0";
    				}
    			}
    			String moveNodePath=splitSettings[splitSettings.length-1].trim();
    			int moveNodeId=0;
    			if(moveNodePath.length()>0){
    				moveNodeId=AdminWise.gConnector.ViewWiseClient.checkCVSearchNode(room.getId(),moveNodePath);
    				AdminWise.printToConsole("moveNodeId  after passing node path in else:::::::::::"+moveNodeId); 
    				if(moveNodeId>0){
    					splitSettings[5]=String.valueOf(moveNodeId);
    				}else if(moveNodeId==0){
    					splitSettings[5]="0";
    				}
    			}
    			String modifiedRetentionSetting="";
    			for(int retRecCount=0;retRecCount<splitSettings.length;retRecCount++){
    				if(modifiedRetentionSetting.length()>0){
    					modifiedRetentionSetting=modifiedRetentionSetting+Util.SepChar+splitSettings[retRecCount]+Util.SepChar;
    				}else{
    					modifiedRetentionSetting=splitSettings[retRecCount]+Util.SepChar; 
    				}
    			}
    			AdminWise.printToConsole("modifiedRetentionSetting in else::::::::::::::::::"+modifiedRetentionSetting);
    			VWRetention retention=new VWRetention(modifiedRetentionSetting,session.user);
    			retention.setId(0);
    			retention.setDocTypeId(-1);
    			Vector retentionId=new Vector();
    			AdminWise.printToConsole("retentionName::::::::::::"+retentionName);
    			int isExist1 = AdminWise.gConnector.ViewWiseClient.isRetentionNameFound(room.getId(),retentionName);
    			AdminWise.printToConsole("isRetention exist :::::::::::::"+isExist1);
    			if(isExist1<=0){
    				newRetentionId= AdminWise.gConnector.ViewWiseClient.setRetentionSettings(room.getId(),retention,retentionId);
    			}
    		}
    	}
    }
  }catch(Exception e){
	  AdminWise.printToConsole("Exception while import retention template ::::"+e.getMessage());
	 // errorList.add("Exception while Importing the retention template ::::"+e.getMessage());
	  //return errorList;
  }
    	return errorList;
    }
    
//------------------------------------------------------------------------------
    private static Vector importNotification(Vector errorList, Vector notificationList) {
    	try{
    		AdminWise.printToConsole("importNotification::::::::"+notificationList.size());
    		if(notificationList==null&&notificationList.size()==0)
        		return errorList;
    		for(int i=0;i<notificationList.size();i++){
    			AdminWise.printToConsole("The value of i ::::::"+i);
    			NotificationSettings settings=(NotificationSettings) notificationList.get(i);
    			VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    			if(settings.getNodeType()==8){
    				AdminWise.printToConsole("Inside nodetype is 8:::::"+settings.getNodeName());
    				AdminWise.printToConsole("Inside nodetype is 8:::::"+settings.getId());
    				int retentionId =AdminWise.gConnector.ViewWiseClient.isRetentionNameFound(room.getId(),settings.getNodeName());
    				if(retentionId>0){
    					AdminWise.printToConsole("Inside if of 8");
    					settings.setReferenceId(retentionId);
    					AdminWise.gConnector.ViewWiseClient.saveNotificationSettings(room.getId(),settings);
    				}else{
    					AdminWise.printToConsole("Inside else of 8");
    					errorList.add("Rention :"+settings.getNodeName()+"does'nt exist in room");
    				}
    			}else if(settings.getNodeType()==9){
    				AdminWise.printToConsole("Inside nodetype is 9:::::"+settings.getNodeName());
    				AdminWise.printToConsole("Inside nodetype is 9:::::"+settings.getId());
    				int routeId = AdminWise.gConnector.ViewWiseClient.isRouteNameFound(room.getId(), settings.getNodeName());
    				if(routeId>0){
    					settings.setReferenceId(routeId);
    					AdminWise.printToConsole("Inside if of 9");
    					AdminWise.gConnector.ViewWiseClient.saveNotificationSettings(room.getId(),settings);
    				}else{
    					AdminWise.printToConsole("Inside else of 9");
    					AdminWise.printToConsole("Workflow :"+settings.getNodeName()+"does'nt exist in room");
    					errorList.add("Workflow :"+settings.getNodeName()+"does'nt exist in room");
    					AdminWise.printToConsole("After workflow ::::");
    				}
    			}else if(settings.getNodeType()==2){
    				AdminWise.printToConsole("Inside nodetype is 2:::::"+settings.getNodeName());
    				AdminWise.printToConsole("Inside nodetype is 2:::::"+settings.getId());
    				int docTypeId=AdminWise.gConnector.ViewWiseClient.isDocTypeFound(room.getId(),new DocType(0,settings.getNodeName()));
    				if(docTypeId>0){
    					settings.setReferenceId(docTypeId);
    					AdminWise.printToConsole("Inside if of 2");
    					AdminWise.gConnector.ViewWiseClient.saveNotificationSettings(room.getId(),settings);
    				}else{
    					AdminWise.printToConsole("Inside else of 2");
    					errorList.add("DocumentType :"+settings.getNodeName()+"does'nt exist in room");
    				}
    			}else{
    				AdminWise.printToConsole("Inside final else settings:::"+settings.getId());
    				AdminWise.gConnector.ViewWiseClient.saveNotificationSettings(room.getId(),settings);
    			}

    		}
    	}catch(Exception e){
    		AdminWise.printToConsole("Exception exception e::::"+e.getMessage());
    	}
    	return errorList;
    }
  //------------------------------------------------------------------------------    
    public static void loadRoomData(int maxTreeNode)
    {
        TFileName="";
        templateDTData=null;
        maxNodeIndex=maxTreeNode;
    }
//------------------------------------------------------------------------------
    public static void loadFromFile(String fileName)
    {
        if(fileName==null || fileName.equals("")) return;
        if(fileName.toUpperCase().endsWith(TemplateExt))
        {
            TFileName=fileName;
            TDirPath=fileName.substring(0,fileName.length()-3);
        }
    }
//------------------------------------------------------------------------------
    private static VWIndexRec getIndexRecFromFile(int dtNumber,int indexId)
    {
        if(templateDTData==null) return null;
        String prefix="ViewWiseTemplate\\VWDocType"+dtNumber+"\\DTIndex"+indexId+"\\";
        VWIndexRec indexRec=new VWIndexRec(0,getValue(prefix+"Name"));
        indexRec.setType(getIntValue(prefix+"Type"));
        indexRec.setMask(getValue(prefix+"Mask"));
        indexRec.setDefault(getValue(prefix+"Default"));
        indexRec.setSVid(getIntValue(prefix+"SVid"));
        indexRec.setInfo(getValue(prefix+"Info"));
        indexRec.setDescription(getValue(prefix+"Description"));
        return indexRec;
    }
//------------------------------------------------------------------------------
    private static VWIndexRec updateIndexToDTIndex(VWIndexRec indexRec,int dtNumber,int indexId)
    {
        String prefix="ViewWiseTemplate\\VWDocType"+dtNumber+"\\DTIndex"+indexId+"\\";
        indexRec.setMask(getValue(prefix+"NewMask"));
        indexRec.setDefault(getValue(prefix+"NewDefault"));
        indexRec.setKey(getValue(prefix+"DTKey").equalsIgnoreCase("true"));
        indexRec.setRequired(getValue(prefix+"Required").equalsIgnoreCase("true"));
        indexRec.setInfo(getValue(prefix+"NewInfo"));
        indexRec.setDescription(getValue(prefix+"NewDescription"));
        return indexRec;
    }
//------------------------------------------------------------------------------
    public static void ClearDTT(boolean withPath)
    {
        if(withPath) 
        {
            TFileName="";
            templateDTData=null;
        }
    }
//------------------------------------------------------------------------------
    public static void ClearFST(boolean withPath)
    {
        if(withPath) TFileName="";
        maxNodeIndex=1;
    }
//------------------------------------------------------------------------------    
    private static Vector exportDTT(Vector docTypeList,Component component,Vector templateList)
    {
        int count=docTypeList.size();
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setMaximum(count);
        templateList.add("<DTCount>"+count+"</DTCount>");
        for(int i=1;i<=count;i++)
        {
            if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) break;
            templateList.add("<VWDocType"+i+">");
            VWDocTypeRec recDocType=(VWDocTypeRec)docTypeList.get(i-1);
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+recDocType.getName()+"] Data");
            templateList.add("<Name>"+VWUtil.fixXMLString(recDocType.getName())+"</Name>");
            templateList.add("<SearchInContents>"+recDocType.getEnableSIC()+"</SearchInContents>");
            templateList.add("<AutoMail_To>"+VWUtil.fixXMLString(recDocType.getAutoMail_To())+"</AutoMail_To>");
            templateList.add("<AutoMail_Cc>"+VWUtil.fixXMLString(recDocType.getAutoMail_Cc())+"</AutoMail_Cc>");
            templateList.add("<AutoMail_Subject>"+VWUtil.fixXMLString(recDocType.getAutoMail_Subject())+"</AutoMail_Subject>");
            templateList.add("<KeyName>"+VWUtil.fixXMLString(recDocType.getKeyName())+"</KeyName>");
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+recDocType.getName()+"] Indices");
            if(recDocType.getId()>0)
            {
                List docTypeIndices=VWDocTypeConnector.getDTIndices(recDocType.getId(), "dontPopulateSelectionValues");
                int indicesCount=docTypeIndices.size();
                templateList.add("<IndexCount>"+indicesCount+"</IndexCount>");
                for(int j=1;j<=indicesCount;j++)
                {
                    templateList.add("<DTIndex"+j+">");
                    VWIndexRec indexDTRec=(VWIndexRec)docTypeIndices.get(j-1);
                    templateList.add("<NewMask>"+VWUtil.fixXMLString(indexDTRec.getMask())+"</NewMask>");
                    templateList.add("<NewDefault>"+VWUtil.fixXMLString(indexDTRec.getDefault())+"</NewDefault>");
                    templateList.add("<Required>"+indexDTRec.getRequired()+"</Required>");
                    templateList.add("<IndexOrder>"+j+"</IndexOrder>");
                    templateList.add("<NewInfo>"+VWUtil.fixXMLString(indexDTRec.getInfo())+"</NewInfo>");
                    templateList.add("<NewDescription>"+VWUtil.fixXMLString(indexDTRec.getDescription())+"</NewDescription>");
                    templateList.add("<DTKey>"+indexDTRec.getKey()+"</DTKey>");
                    VWIndexRec indexRec=VWDocTypeConnector.getIndex(indexDTRec.getId());
                    templateList.add("<Name>"+VWUtil.fixXMLString(indexRec.getName())+"</Name>");
                    templateList.add("<Type>"+indexRec.getType()+"</Type>");
                    templateList.add("<Mask>"+VWUtil.fixXMLString(indexRec.getMask())+"</Mask>");
                    templateList.add("<Default>"+VWUtil.fixXMLString(indexRec.getDefault())+"</Default>");
                    templateList.add("<SVid>"+indexRec.getSVid()+"</SVid>");
                    templateList.add("<Info>"+indexRec.getInfo()+"</Info>");
                    templateList.add("<Description>"+VWUtil.fixXMLString(indexRec.getDescription())+"</Description>");
                    if(indexRec.getType()==Index.IDX_SELECTION){
                        templateList.add("<SelectionValues>"+
                        VWUtil.fixXMLString(getSelectionValuesString(indexRec.getIndexSelectionValues()))+"</SelectionValues>");
                    }
                    
                    templateList.add("</DTIndex"+j+">");
                }
            }
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+recDocType.getName()+"] Version Data");
            DocType docType=VWDocTypeConnector.getDocTypeVerRev(recDocType.getId());
            if(docType!=null && !docType.getVREnable().equals("0"))
            {
                templateList.add("<VREnable>"+docType.getVREnable()+"</VREnable>");
                templateList.add("<VRInitVersion>"+docType.getVRInitVersion()+"</VRInitVersion>");
                templateList.add("<VRInitRevision>"+docType.getVRInitRevision()+"</VRInitRevision>");
                templateList.add("<VRPagesChange>"+docType.getVRPagesChange()+"</VRPagesChange>");
                templateList.add("<VRIncPagesChange>"+docType.getVRIncPagesChange()+"</VRIncPagesChange>");
                templateList.add("<VRPageChange>"+docType.getVRPageChange()+"</VRPageChange>");
                templateList.add("<VRIncPageChange>"+docType.getVRIncPageChange()+"</VRIncPageChange>");
                templateList.add("<VRPropertiesChange>"+docType.getVRPropertiesChange()+"</VRPropertiesChange>");
                templateList.add("<VRIncPropertiesChange>"+docType.getVRIncPropertiesChange()+"</VRIncPropertiesChange>");
            }
            else
            {
                templateList.add("<VREnable>0</VREnable>");
                templateList.add("<VRInitVersion>0</VRInitVersion>");
                templateList.add("<VRInitRevision>0</VRInitRevision>");
                templateList.add("<VRPagesChange>0</VRPagesChange>");
                templateList.add("<VRIncPagesChange>0</VRIncPagesChange>");
                templateList.add("<VRPageChange>0</VRPageChange>");
                templateList.add("<VRIncPageChange>0</VRIncPageChange>");
                templateList.add("<VRPropertiesChange>0</VRPropertiesChange>");
                templateList.add("<VRIncPropertiesChange>0</VRIncPropertiesChange>");
            }
            templateList.add("</VWDocType"+i+">");
            AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setValue(i);
        }
        return templateList;
}

    private static Vector exportSecurityDetails(Vector securityList,Component component,Vector templateList)
    {
        int count=securityList.size();
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setMaximum(count);        
        templateList.add("<Security>");
        templateList.add("<SecurityCount>"+count+"</SecurityCount>");
        Node node= null;
        for(int i=1;i<=count;i++)
        {
            if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) break;
            templateList.add("<VWSecurity"+i+">");
            node=(Node) securityList.get(i-1);
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+node.getName()+"] Data");
            templateList.add("<Name>"+VWUtil.fixXMLString(node.getName())+"</Name>");
            templateList.add("<Path>"+VWUtil.fixXMLString(node.getPath())+"</Path>");
            Vector securityDetails = new Vector();
            securityDetails = node.getUserGroupSecurityList();
            templateList.add("<AclEntryCount>"+securityDetails.size()+"</AclEntryCount>");
            for(int sCount= 0; sCount < securityDetails.size(); sCount++){
            	StringTokenizer tokens = new StringTokenizer(securityDetails.get(sCount).toString(), "\t");
            	templateList.add("<UserGroupName" + sCount + ">"+VWUtil.fixXMLString(tokens.nextToken().trim())+"</UserGroupName"  + sCount + ">");
            	templateList.add("<Permission" + sCount + ">" +VWUtil.fixXMLString(tokens.nextToken().trim())+"</Permission"  + sCount + ">");
            }           
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+node.getName()+"] Security Data");
            templateList.add("</VWSecurity"+i+">");
            AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setValue(i);
        }
        templateList.add("</Security>");
        return templateList;
}

    private static Vector exportNodeProperties(Vector nodePropertiesList,Component component,Vector templateList)
    {
        int count=nodePropertiesList.size();
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setMaximum(count);        
        templateList.add("<NodeProperties>");
        templateList.add("<NodePropertiesCount>"+count+"</NodePropertiesCount>");
        Node node= null;
        for(int i=1;i<=count;i++)
        {
            if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) break;
            templateList.add("<VWNodeProperties"+i+">");
            node=(Node) nodePropertiesList.get(i-1);
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+node.getName()+"] Data");
            templateList.add("<Name>"+VWUtil.fixXMLString(node.getName())+"</Name>");
            templateList.add("<Path>"+VWUtil.fixXMLString(node.getPath())+"</Path>");
            Vector nodeProperties = new Vector();
            nodeProperties = node.getNodePropertiesList();
            templateList.add("<DefaultDocType>"+node.getDefaultDocType()+"</DefaultDocType>");
            templateList.add("<IndexCount>"+nodeProperties.size()+"</IndexCount>");
            /*
            * For default document type we are creating the node properties without index and values
            * Fix for the notification 1197 - Node property for the folder is not copied when they try coping folder structure using the templates tab
            */
            if(nodeProperties!=null && nodeProperties.size()>0){
	            for(int sCount= 0; sCount < nodeProperties.size(); sCount++){
	            	StringTokenizer tokens = new StringTokenizer(nodeProperties.get(sCount).toString(), "\t");
	            	templateList.add("<Index" + sCount + ">"+VWUtil.fixXMLString(tokens.nextToken().trim())+"</Index"  + sCount + ">");
	            	templateList.add("<Value" + sCount + ">" +VWUtil.fixXMLString(tokens.nextToken().trim())+"</Value"  + sCount + ">");
	            }    
            }
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read ["+node.getName()+"] Node Properties Data");
            templateList.add("</VWNodeProperties"+i+">");
            AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setValue(i);
        }
        templateList.add("</NodeProperties>");
        return templateList;
}    
//------------------------------------------------------------------------------
    /**
     * CV10.1 Enhancment added for writing retention tags
     * @param retentionList
     * @param component
     * @param templateList
     * @return
     */
    private static Vector exportRetention(Vector retentionList,Vector docTypeList,
    		Component component, Vector templateList) {
    	AdminWise.printToConsole("Inside export retention");
    	int count=retentionList.size();
    	AdminWise.printToConsole("counter:::::"+count);
    	AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setMaximum(count);      
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	
    	if(retentionList.size()>0&retentionList!=null){
    		Vector retdoctypeList=new Vector();
			AdminWise.gConnector.ViewWiseClient.getRetentionDoctypes(room.getId(),0,retdoctypeList);
			AdminWise.printToConsole("retdoctypeList :::::::::"+retdoctypeList);
			AdminWise.printToConsole("docTypeList :::::::::"+docTypeList);
			String msg="";
			if(VWTemplatePanel.isRetentionInclude==true){
				Vector selDocTypesList=VWTemplatePanel.docTypeTable.getData();
				Vector tableDocTypeList=new Vector();
				for(int i=0;i<selDocTypesList.size();i++){
					tableDocTypeList.add(selDocTypesList.get(i).toString());
				}
				AdminWise.printToConsole("selDocTypesList :::::::::::"+selDocTypesList);
				if(selDocTypesList==null||selDocTypesList.size()==0){
					VWMessage.showInfoDialog("Please add all documentTypes in Retention");
					return null;
				}
				if(retdoctypeList!=null&&retdoctypeList.size()>0){
					for(int j=0;j<retdoctypeList.size();j++){
						if(!tableDocTypeList.contains(retdoctypeList.get(j).toString())){
							AdminWise.printToConsole("Inside if condition:::::::::");
							AdminWise.printToConsole("retdoctypeList :::::"+retdoctypeList.get(j).toString());
							msg=msg+retdoctypeList.get(j)+";";
							AdminWise.printToConsole("msg :::::::::::"+msg);
						}

					}
					if(!msg.equals("")){
						VWMessage.showInfoDialog("Please add documentTypes :"+msg+"in Retention");
						return null;
					}
				}
			}
			
    		templateList.add("<RetentionCount>"+count+"</RetentionCount>");
    		for(int i=0;i<count;i++)
    		{
    			if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) break;

    			StringTokenizer tokens = new StringTokenizer(retentionList.get(i).toString(), "\t");
    			int retentionId=Integer.parseInt(tokens.nextToken());
    			
    			String retentionName=tokens.nextToken();
    			int isExist = AdminWise.gConnector.ViewWiseClient.isRetentionNameFound(room.getId(),retentionName);
    			if(isExist > 0){
    				int retNumber=i+1;
    				templateList.add("<Retention"+retNumber+">");
    				templateList.add("<Name>"+VWUtil.fixXMLString(retentionName)+"</Name>");	
    				templateList.add("<Retentionsettings>"+VWUtil.fixXMLString(retentionList.get(i).toString())+"</Retentionsettings>");	
    				Vector indexInfo=new Vector();
    				AdminWise.gConnector.ViewWiseClient.getIndexInfoTemplate(room.getId(),retentionId,indexInfo);
    				if(indexInfo.size()>0&&indexInfo!=null){
    					templateList.add("<RetentionIndexcount>"+indexInfo.size()+"</RetentionIndexcount>");
    					for(int k=0;k<indexInfo.size();k++){
    						int indexNumber=k+1;
    						templateList.add("<RetentionIndex"+indexNumber+">");
    						templateList.add("<IndexInfo>"+VWUtil.fixXMLString(indexInfo.get(k).toString())+"</IndexInfo>");
    						templateList.add("</RetentionIndex"+indexNumber+">");
    					}
    				}else{
    					templateList.add("<RetentionIndexcount>"+"0"+"</RetentionIndexcount>");
    					templateList.add("<RetentionIndex"+"0"+">");
						templateList.add("<IndexInfo>"+"</IndexInfo>");
						templateList.add("</RetentionIndex"+"0"+">");
    				}
    				templateList.add("</Retention"+retNumber+">");
    			}
    		}
    	}
    	return templateList;
    }
//-------------------------------------------------------------------------------
    private static Vector exportNotification(Vector notification,
    		Vector templateList) {
    	// TODO Auto-generated method stub
    	Vector count=new Vector();
    	for(int k=0;k<notification.size();k++){
    		NotificationSettings setting=(NotificationSettings)notification.get(k);
    		if(setting.getNodeType()>1){
    			count.add(k);
    		}
    	}  
    	templateList.add("<NotificationCount>"+count.size()+"</NotificationCount>");
    	for(int j=0;j<notification.size();j++){
    		NotificationSettings setting=(NotificationSettings)notification.get(j);
    		if(VWTemplatePanel.isNotificationInclude==true){
    			//CV10.1 Enhancement If condition added to avoid folder and document notification.
    			if(setting.getNodeType()>1){
    				int notificationCount=j+1;
    				templateList.add("<Notification"+notificationCount+">");
    				String notificationSettings=String.valueOf(setting.getId())+Util.SepChar+String.valueOf(setting.getReferenceId())+Util.SepChar+setting.getNodeName()+Util.SepChar+String.valueOf(setting.getNodeType())+
    						Util.SepChar+String.valueOf(setting.getNotifyCaptureType())+Util.SepChar+String.valueOf(setting.getProcessType())+Util.SepChar+String.valueOf(setting.getFolderInherit())+	 
    						Util.SepChar+String.valueOf(setting.getDocumentInherit())+Util.SepChar+setting.getUserName()+
    						Util.SepChar+setting.getIpAddress()+Util.SepChar+setting.getAttachment()+Util.SepChar+setting.getEmails()+Util.SepChar+setting.getLastNotifyTime();
    				templateList.add("<NotificationSettings>"+VWUtil.fixXMLString(notificationSettings)+"</NotificationSettings>");	
    				Notification[] notificatArray=setting.getNotifications();
    				templateList.add("<NotificationModuleCount>"+notificatArray.length+"</NotificationModuleCount>");
    				for(int i=0;i<notificatArray.length;i++){
    					int moduleCount=i+1;
    					notificatArray[i].getId();
    					notificatArray[i].getModule();
    					notificatArray[i].getNotifyDesc();
    					notificatArray[i].getSettingsId();
    					String notificationModule=String.valueOf(notificatArray[i].getId())+Util.SepChar+notificatArray[i].getModule()+Util.SepChar+
    							notificatArray[i].getNotifyDesc()+Util.SepChar+String.valueOf(notificatArray[i].getSettingsId());	
    					templateList.add("<NotificationModule"+moduleCount+">");
    					templateList.add("<ModuleInfo>"+VWUtil.fixXMLString(notificationModule)+"</ModuleInfo>");
    					templateList.add("</NotificationModule"+moduleCount+">");
    				}
    				templateList.add("</Notification"+notificationCount+">");
    			} 
    		}

    	}
    	return templateList;
    }
   // -------------------------------------------------------------------------------
    
    
    private static Vector exportFST(Vector pathes,Component component,Vector templateList)
    {
        templateList.add("<FileSystem>");
        int count=pathes.size();
        templateList.add("<FolderCount>"+count+"</FolderCount>");
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setMaximum(count);
        for(int i=1;i<=count;i++)
        {
            if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) break;
            AdminWise.adminPanel.templatePanel.templateInfoDlg.setDocText("Read information for ["+(String)pathes.get(i-1)+"]");
            templateList.add("<Folder"+i+">"+VWUtil.fixXMLString((String)pathes.get(i-1))+"</Folder"+i+">");
            AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsDoc.setValue(i);
        }
        templateList.add("</FileSystem>");
        return templateList;
}
//------------------------------------------------------------------------------
    public static void exportData(String fileName,Vector pathes,Vector docTypeList,Vector securityDetails, Vector nodePropertiesList,Vector retentionList,
    		Vector notificationList, Component component)
    {
    	AdminWise.printToConsole("retentionList ::::"+retentionList);
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsMain.setValue(0);
        Vector templateList=new Vector();
        //Change from UTF-16 to UTF-8 for BUG-3252 template import not working
        templateList.add("<?xml version='1.0' encoding='UTF-8'?>");
        templateList.add("<ViewWiseTemplate>");
        if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) return;
        templateList=exportFST(pathes,component,templateList);
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsMain.setValue(25);
        AdminWise.adminPanel.templatePanel.templateInfoDlg.setMainText("Read document type data");
        if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) return;
        templateList=exportDTT(docTypeList,component,templateList);
        if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) return;
//      If include security is not checked, don't export security details.
        if(securityDetails!=null && securityDetails.size()>0){
        	templateList=exportSecurityDetails(securityDetails,component,templateList);
        }
        if(nodePropertiesList !=null && nodePropertiesList.size()>0){
        	templateList=exportNodeProperties(nodePropertiesList,component,templateList);
        }
        
        /**@author madhavan.b
         * Enhacement CV10.1 Retention:- Condition added for Retention Template
         */
        if(retentionList !=null && retentionList.size()>0){
        	templateList=exportRetention(retentionList,docTypeList,component,templateList);
        	if(templateList==null){
        		return;
        	}
        }else{
        	templateList.add("<RetentionCount>"+"0"+"</RetentionCount>");
        }
        if(notificationList!=null&&notificationList.size()>0){
        	templateList=exportNotification(notificationList,templateList);
        }else{
        	templateList.add("<NotificationCount>"+"0"+"</NotificationCount>");
        }
        
        
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsMain.setValue(75);
        templateList.add("</ViewWiseTemplate>");
        fileName=fileName.toLowerCase().endsWith(".vwt")?fileName:fileName+".vwt";
        AdminWise.adminPanel.templatePanel.templateInfoDlg.setMainText("Write data");
        if(AdminWise.adminPanel.templatePanel.templateInfoDlg.cancelAction) return;
        VWUtil.writeListToFile(templateList,fileName,component);
        AdminWise.adminPanel.templatePanel.templateInfoDlg.PrgrsMain.setValue(100);
    }
//------------------------------------------------------------------------------
public static void checkTemplateFiles()
{
    ///File propFolder=new File("C:\\ViewWise\\Client\\Properties\\ViewWiseRooms.properties");   
    String home =VWUtil.getHome();
    if(home==null) return;
    File propFolder=new File(home);
    propFolder=propFolder.getParentFile();
    File templateFile=new File(propFolder,TemplatesFolder_Name);
    if(!templateFile.exists() || !templateFile.isDirectory())
    {
        templateFile=new File(propFolder,TemplateFolder_Name);
        if(!templateFile.exists() || !templateFile.isDirectory()) return;
    }
    
    String[] filesList=templateFile.list();
    int count=filesList.length;
    boolean displaymessage=false;
    for(int i=0;i<count;i++)
    {
        if((filesList[i].equals("")) || (!filesList[i].toUpperCase().endsWith(TemplateExt)))
        continue;
        String templateName=filesList[i].substring(0,filesList[i].length()-4);
        if(!displaymessage)
        if(AdminWise.adminPanel.confirmImportTemplateFiles)
        if(VWMessage.showConfirmDialog((Component)AdminWise.adminPanel,
        		ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("VWTemplateConnector.MSG"))!=javax.swing.JOptionPane.YES_OPTION) return;
        displaymessage=true;
        try{
            AdminWise.adminPanel.setWaitPointer();
            for(int j=i+1;j<count;j++)
                if(filesList[j].substring(0,filesList[j].length()-4).equals(templateName))
                    filesList[j]="";
            
            File file=new File(templateFile,filesList[i]);
            loadFromFile(file.getPath());
            Vector retentionList=VWTemplateConnector.loadRetentionFromFile();
            Vector notificationList=VWTemplateConnector.loadNotificationFromFile();
            importToRoom(loadDTTFromFile(),loadDFSFromFile(),retentionList,notificationList,(Component)AdminWise.adminPanel);
        }
        catch(Exception e){AdminWise.adminPanel.setDefaultPointer();}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
}
//------------------------------------------------------------------------------
 private static String getValue(String key)
 {
     String value ="";
     try{
        value=(String)templateDTData.get(key);
        if(value==null) value="";
        return value;
     }
     catch(Exception e){}
     return "";
 }
//------------------------------------------------------------------------------
 private static int getIntValue(String key)
 {
     String value="";
     try{
        value=(String)templateDTData.get(key);
        if(value==null) return 0;
        return VWUtil.to_Number(value);
     }
     catch(Exception e){}
     return 0;
 }
    //-----------------------------------------------------------
    
    public static String getSelectionValuesString(Vector list) {
        String strValues="";
        try
        {
            AdminWise.adminPanel.setWaitPointer(); 
            //Vector list = VWDocTypeConnector.getSelectionValues(indexId);
            
            if(list==null || list.size()==0) return null;
            int count=list.size();
            for(int i=0;i<count-1;i++)
                strValues+=((String)list.get(i)).trim() + "|";
            strValues+=((String)list.get(count-1)).trim();
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        return strValues;
    }
    //-----------------------------------------------------------
public static int maxNodeIndex=1;
public static String TFileName="";
public static String TDirPath="";
public static Session session=null;
public static Hashtable templateDTData=null;
//------------------------------------------------------------------------------

}