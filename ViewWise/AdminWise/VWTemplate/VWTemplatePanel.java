package ViewWise.AdminWise.VWTemplate;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.util.List;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JToolBar.Separator;
import javax.swing.border.EtchedBorder;
import javax.swing.tree.DefaultTreeModel;

import com.computhink.common.Constants;
import com.computhink.common.NotificationSettings;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.Session;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRetention.VWRetentionConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWTree.VWTree;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import ViewWise.AdminWise.VWTree.VWTreeNode;
import ViewWise.AdminWise.VWUtil.VWFileFilter;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWTemplatePanel extends JPanel implements VWConstant, Constants {
    public VWTemplatePanel() {
        
    }
    //-----------------------------------------------------------
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWTemplate.setLayout(null);
        add(VWTemplate,BorderLayout.CENTER);
        VWTemplate.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWTemplate.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
/*        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        if(localLanguage.equals("nl_NL")){
        	String str3=BTN_LOADROOMDATA_NAME.substring(0,15);
            String srt4=BTN_LOADROOMDATA_NAME.substring(15,BTN_LOADROOMDATA_NAME.length());
            	BtnLoadRoomData.setText("<html><br>"+str3+"<br>"+srt4+"</html>");
            	BtnLoadRoomData.setFocusable(false);
        }else
          BtnLoadRoomData.setText(BTN_LOADROOMDATA_NAME);
        
        BtnLoadRoomData.setActionCommand(BTN_LOADROOMDATA_NAME);
        PanelCommand.add(BtnLoadRoomData);
        //BtnLoadRoomData.setBackground(java.awt.Color.white);
        if(localLanguage.equals("nl_NL")){
        BtnLoadRoomData.setBounds(12,top-15,144, AdminWise.gBtnHeight+15);
        }else
        BtnLoadRoomData.setBounds(12,top,144, AdminWise.gBtnHeight);
        
        BtnLoadRoomData.setIcon(VWImages.AllRoomIcon);
        
        top += hGap + 12;
        if(localLanguage.equals("nl_NL")){
        	String str5=BTN_LOADROOMDATA_NAME.substring(0,15);
            String srt6=BTN_LOADROOMDATA_NAME.substring(15,BTN_LOADROOMDATA_NAME.length());
        
        BtnLoadFromFile.setText("<html><br>"+str5+"<br>"+srt6+"</html>");
        BtnLoadFromFile.setFocusable(false);
        }else
        	BtnLoadFromFile.setText(BTN_LOADFROMFILE_NAME);	
        
        BtnLoadFromFile.setActionCommand(BTN_LOADFROMFILE_NAME);
        PanelCommand.add(BtnLoadFromFile);
        //BtnLoadFromFile.setBackground(java.awt.Color.white);
        if(localLanguage.equals("nl_NL")){
        BtnLoadFromFile.setBounds(12,top-15,144, AdminWise.gBtnHeight+15);
        }else
        	BtnLoadFromFile.setBounds(12,top,144, AdminWise.gBtnHeight);	
        BtnLoadFromFile.setIcon(VWImages.LoadIcon);
        
        top += hGap;
        BtnImport.setText(BTN_IMPORT_NAME);
        BtnImport.setActionCommand(BTN_IMPORT_NAME);
        PanelCommand.add(BtnImport);
        //BtnImport.setBackground(java.awt.Color.white);
        BtnImport.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnImport.setIcon(VWImages.LoadIcon);
        
        top += hGap + 12;
        BtnExport.setText(BTN_SAVETOFILE_NAME);
        BtnExport.setActionCommand(BTN_SAVETOFILE_NAME);
        PanelCommand.add(BtnExport);
        //BtnExport.setBackground(java.awt.Color.white);
        BtnExport.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnExport.setIcon(VWImages.ExportIcon);
        
        top += hGap + 12;
        BtnClear.setText(BTN_CLEAR_NAME);
        BtnClear.setActionCommand(BTN_CLEAR_NAME);
        PanelCommand.add(BtnClear);
//        BtnClear.setBackground(java.awt.Color.white);
        BtnClear.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClear.setIcon(VWImages.ClearIcon);
        
        top += hGap + 12;
        ChkSecurityInclude.setText(CHK_INCLUDE_SECURITY);
        ChkSecurityInclude.setToolTipText(CHK_INCLUDE_SECURITY_TOOLTIP);
        PanelCommand.add(ChkSecurityInclude);
        ChkSecurityInclude.setBackground(AdminWise.getAWColor());
        ChkSecurityInclude.setBounds(14,top,144, AdminWise.gBtnHeight);
        
        top += hGap;
        String str1=CHK_INCLUDE_NODE_PROPERTIES.substring(0,21);
        String srt2=CHK_INCLUDE_NODE_PROPERTIES.substring(21,CHK_INCLUDE_NODE_PROPERTIES.length());
        
        if(localLanguage.equals("nl_NL")){
        ChkNodePropertiesInclude.setText("<html><br>"+str1+"<br>"+srt2+"</html>");
        }else 
        	 ChkNodePropertiesInclude.setText(CHK_INCLUDE_NODE_PROPERTIES);
        ChkNodePropertiesInclude.setToolTipText(CHK_INCLUDE_NODE_PROPERTIES_TOOLTIP);
        PanelCommand.add(ChkNodePropertiesInclude);
        ChkNodePropertiesInclude.setBackground(AdminWise.getAWColor());
        if(localLanguage.equals("nl_NL")){
        	   ChkNodePropertiesInclude.setBounds(14,top,144, AdminWise.gBtnHeight+20);    
        	   ChkNodePropertiesInclude.setFocusable(false);
        }else
        ChkNodePropertiesInclude.setBounds(14,top,144, AdminWise.gBtnHeight);       
        
        
        top += hGap;
        PanelCommand.add(ChkIncludeRetention);
        ChkIncludeRetention.setText(CHK_INCLUDE_RETENTION);
        ChkIncludeRetention.setBounds(14,top,144, AdminWise.gBtnHeight);
        ChkIncludeRetention.setVisible(true);
        
        top += hGap;
        PanelCommand.add(ChkIncludeNotification);
        ChkIncludeNotification.setText(CHK_INCLUDE_NOTIFICATION);
        ChkIncludeNotification.setBounds(14,top,144, AdminWise.gBtnHeight);
        ChkIncludeNotification.setVisible(true);
        
        
        
        PanelTable.setBounds(170,0,700,500);
        JLabel1.setText(LBL_NAVIGATORDATA_NAME);
        JLabel1.setBackground(Color.WHITE);
        JLabel1.setForeground(new Color(7, 38, 58));
        JLabel2.setText(LBL_DOCTYPES_NAME);
        JLabel2.setBackground(Color.WHITE);
        JLabel2.setForeground(new Color(7, 38, 58));
        JLabel2.setLabelFor(SPDocTypeTable);
        PanelProp.setLayout(new BorderLayout());
        PanelProp.add(JLabel2,BorderLayout.NORTH);
        PanelProp.add(SPDocTypeTable,BorderLayout.CENTER);
        PanelProp.add(tableToolBar,BorderLayout.EAST);
        PanelProp.setBackground(Color.WHITE);
        
        VWTemplate.add(PanelTable);
        PanelTable.setLayout(new BorderLayout());
        PanelTable.add(spliter,BorderLayout.CENTER);
        PanelTree.setLayout(new BorderLayout());
        PanelTree.add(JLabel1,BorderLayout.NORTH);
        PanelTree.add(treePanel,BorderLayout.CENTER);
        PanelTree.add(treeToolBar,BorderLayout.EAST);
        PanelTree.setBackground(Color.WHITE);
        
        tree.setEditable(true);
        treeToolBar.setFloatable(false);
        treeToolBar.setRollover(true);
        treeToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        treeToolBar.setSize(16,16);
        
        Separator[] separators=new Separator[7];
        for(int i=0;i<7;i++) {
            separators[i]=new Separator();
            separators[i].setOrientation(javax.swing.SwingConstants.VERTICAL);
        }
        BtnNodeExpandAll.setToolTipText(TOOLTIP_NODEEXPANDALL);
        BtnNodeExpandAll.setBackground(treeToolBar.getBackground());
        treeToolBar.add(BtnNodeExpandAll);
        treeToolBar.add(separators[0]);
        
        treeToolBar.add(BtnNodeNew);
        BtnNodeNew.setToolTipText(TOOLTIP_NODENEW);
        BtnNodeNew.setBackground(treeToolBar.getBackground());
        treeToolBar.add(separators[1]);
        
        BtnNodeDel.setToolTipText(TOOLTIP_NODEDEL);
        BtnNodeDel.setBackground(treeToolBar.getBackground());
        treeToolBar.add(BtnNodeDel);
        treeToolBar.add(separators[2]);
        
        BtnNodeClear.setToolTipText(TOOLTIP_NODECLEAR);
        BtnNodeClear.setBackground(treeToolBar.getBackground());
        treeToolBar.add(BtnNodeClear);
        treeToolBar.add(separators[3]);
        
        tableToolBar.setFloatable(false);
        tableToolBar.setRollover(true);
        tableToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        tableToolBar.setSize(16,16);
        
        tableToolBar.add(BtnDTAdd);
        BtnDTAdd.setToolTipText(TOOLTIP_DTADD);
        BtnDTAdd.setBackground(tableToolBar.getBackground());
        tableToolBar.add(separators[4]);
        
        BtnDTDel.setToolTipText(TOOLTIP_DTDEL);
        BtnDTDel.setBackground(tableToolBar.getBackground());
        tableToolBar.add(BtnDTDel);
        tableToolBar.add(separators[5]);
        
        BtnDTClear.setToolTipText(TOOLTIP_DTCLEAR);
        BtnDTClear.setBackground(tableToolBar.getBackground());
        tableToolBar.add(BtnDTClear);
        tableToolBar.add(separators[6]);
        
        spliter.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        spliter.setTopComponent(PanelTree);
        spliter.setBottomComponent(PanelProp);
        spliter.setDividerLocation(0.6);
        spliter.doLayout();
        
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.DocTypeImage);
        SymAction lSymAction = new SymAction();
        BtnLoadRoomData.addActionListener(lSymAction);
        BtnLoadFromFile.addActionListener(lSymAction);
        BtnExport.addActionListener(lSymAction);
        BtnClear.addActionListener(lSymAction);
        BtnNodeNew.addActionListener(lSymAction);
        BtnDTAdd.addActionListener(lSymAction);
        BtnDTDel.addActionListener(lSymAction);
        BtnDTClear.addActionListener(lSymAction);
        BtnNodeClear.addActionListener(lSymAction);
        BtnNodeDel.addActionListener(lSymAction);
        BtnImport.addActionListener(lSymAction);
        BtnNodeExpandAll.addActionListener(lSymAction);
        ChkSecurityInclude.addActionListener(lSymAction);
        ChkNodePropertiesInclude.addActionListener(lSymAction);
        ChkIncludeRetention.addActionListener(lSymAction);
        ChkIncludeNotification.addActionListener(lSymAction);
        docTypeTable.getParent().setBackground(java.awt.Color.white);
        docTypeTable.getSelectionModel().addListSelectionListener
        (new VWListSelectionListener());
        createRoomNode();
        resizePanel();
        UILoaded=true;
    }
    //-----------------------------------------------------------
    class VWListSelectionListener implements javax.swing.event.ListSelectionListener {
        public void valueChanged(javax.swing.event.ListSelectionEvent e) {
            if(docTypeTable.getSelectedRow()>=0)
                BtnDTDel.setEnabled(true);
            else
                BtnDTDel.setEnabled(false);
        }
    }
    //-----------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnLoadRoomData)
                BtnLoadRoomData_actionPerformed(event);
            else if (object == BtnLoadFromFile)
                BtnLoadFromFile_actionPerformed(event);
            else if (object == BtnExport)
                BtnExport_actionPerformed(event);
            else if (object == BtnClear)
                BtnClear_actionPerformed(event);
            else if (object == BtnImport)
                BtnImport_actionPerformed(event);
            else if (object == BtnNodeNew)
                BtnNodeNew_actionPerformed(event);
            else if (object == BtnDTAdd)
                BtnDTAdd_actionPerformed(event);
            else if (object == BtnDTDel)
                BtnDTDel_actionPerformed(event);
            else if (object == BtnDTClear)
                BtnDTClear_actionPerformed(event);
            else if (object == BtnNodeClear)
                BtnNodeClear_actionPerformed(event);
            else if (object == BtnNodeDel)
                BtnNodeDel_actionPerformed(event);
            else if (object == BtnNodeExpandAll)
                BtnNodeExpandAll_actionPerformed(event);
            else if (object == ChkSecurityInclude)
            	ChkSecurityInclude_actionPerformed(event);
            else if (object == ChkNodePropertiesInclude)
            	ChkNodePropertiesInclude_actionPerformed(event);
            else if(object ==ChkIncludeRetention)
            	ChkIncludeRetention_actionPerformed(event);
            else if(object ==ChkIncludeNotification)
            	ChkIncludeNotification_actionPerformed(event);
            
        }
    }
    //-----------------------------------------------------------
    //{{DECLARE_CONTROLS
    public javax.swing.JSplitPane spliter = new javax.swing.JSplitPane();
    public VWTree tree = new VWTree();
    VWPicturePanel picPanel = new VWPicturePanel(TAB_TEMPLATE_NAME, VWImages.TemplateImage);
    javax.swing.JScrollPane treePanel = new javax.swing.JScrollPane(tree);
    javax.swing.JPanel VWTemplate = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JPanel PanelTree = new javax.swing.JPanel();
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JPanel PanelProp = new javax.swing.JPanel();
    javax.swing.JToolBar tableToolBar = new javax.swing.JToolBar();
    javax.swing.JToolBar treeToolBar = new javax.swing.JToolBar();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnLoadRoomData = new VWLinkedButton(1);
    VWLinkedButton BtnLoadFromFile = new VWLinkedButton(1);
    VWLinkedButton BtnExport = new VWLinkedButton(1);
    VWLinkedButton BtnImport = new VWLinkedButton(1);
    VWLinkedButton BtnClear = new VWLinkedButton(1);
    VWLinkedButton BtnNodeExpandAll = new VWLinkedButton(VWImages.FolderExpandedIcon,false);
    VWLinkedButton BtnNodeNew = new VWLinkedButton(VWImages.NewIcon,false);
    VWLinkedButton BtnNodeDel = new VWLinkedButton(VWImages.DelIcon,false);
    VWLinkedButton BtnNodeClear = new VWLinkedButton(VWImages.ClearIcon,false);
    VWLinkedButton BtnDTAdd = new VWLinkedButton(VWImages.AddIcon,false);
    VWLinkedButton BtnDTDel = new VWLinkedButton(VWImages.DelIcon,false);
    VWLinkedButton BtnDTClear = new VWLinkedButton(VWImages.ClearIcon,false);
    public static VWDocTypeTable docTypeTable = new VWDocTypeTable();
    javax.swing.JScrollPane SPDocTypeTable = new javax.swing.JScrollPane(docTypeTable);
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JCheckBox ChkSecurityInclude = new javax.swing.JCheckBox();
    javax.swing.JCheckBox ChkNodePropertiesInclude = new javax.swing.JCheckBox();
    javax.swing.JCheckBox ChkIncludeRetention = new javax.swing.JCheckBox();
    javax.swing.JCheckBox ChkIncludeNotification = new javax.swing.JCheckBox();
    javax.swing.border.EtchedBorder etchedBorder = new javax.swing.border.EtchedBorder
    (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
    //}}
    //-----------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object instanceof VWTemplatePanel)
                VWTemplate_componentResized(event);
        }
    }
    //-----------------------------------------------------------
    void VWTemplate_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource() instanceof VWTemplatePanel) {
            resizePanel();
        }
    }
    private void resizePanel() {
        Dimension mainDimension=getSize();
        Dimension PanelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        PanelDimension.width=mainDimension.width-commandDimension.width;
        PanelDimension.height=mainDimension.height;
        PanelCommand.setSize(commandDimension);
        PanelTable.setSize(PanelDimension);
        PanelProp.setSize(PanelDimension);
        AdminWise.adminPanel.MainTab.repaint();
        spliter.setDividerLocation(0.6);
        spliter.doLayout();
        doLayout();
    }
    //-----------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        stopGridEdit();
        if(newRoom.getId()==gCurRoom) return;
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            setEnableMode(MODE_UNCONNECT);
            return;
        }
        gCurRoom=newRoom.getId();
        stopGridEdit();
        BtnClear_actionPerformed(null);
        docTypesList=null;
        setEnableMode(MODE_UNSELECTED);
    }
    //-----------------------------------------------------------
    public void unloadTabData() {
        stopGridEdit();
        BtnClear_actionPerformed(null);
        docTypesList=null;
        setEnableMode(MODE_UNCONNECT);
    }
    //-----------------------------------------------------------
    private void createRoomNode() {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        createRoomNode(room);
    }
    //-----------------------------------------------------------
    private void createRoomNode(VWRoom newRoom) {
        VWTreeConnector.setTree(tree);
        VWTreeNode rootNode=null;
        if(newRoom!=null)
            rootNode=new VWTreeNode(0,newRoom.getName(),Room_TYPE,0,newRoom.getId());
        else
            rootNode=new VWTreeNode(0,ResourceManager.getDefaultManager().getString("CVProduct.Name") + "Room",Room_TYPE,0,0);
        tree.setModel(new DefaultTreeModel(rootNode));
        tree.setSelectionRow(0);
    }
    //-----------------------------------------------------------
    void BtnLoadRoomData_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWTreeConnector.setTree(tree);
            tree.loadTreeData();
            ///tree.expandAll();
            VWTemplateConnector.loadRoomData(tree.getRowCount());
            docTypesList=VWTemplateConnector.getDocTypes();
            docTypeTable.addData(docTypesList);
            setEnableMode(MODE_SELECT);
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //-----------------------------------------------------------
    void BtnLoadFromFile_actionPerformed(java.awt.event.ActionEvent event) {
        String fileName=loadOpenDialog((Component)this);
        if(fileName.equals("")) return;
        VWTemplateConnector.loadFromFile(fileName);
        try{
            AdminWise.adminPanel.setWaitPointer();
            loadDDTFromFile();
            loadDFSFromFile();
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    public int applyVWT(String fileName){
    	try{
			ChkSecurityInclude.setSelected(true);
			ChkIncludeRetention.setSelected(true);
			ChkNodePropertiesInclude.setSelected(true);
			ChkIncludeNotification.setSelected(true);
			VWTemplateConnector.loadFromFile(fileName);
		    loadDDTFromFile();
		    loadDFSFromFile();
		   // VWTemplateConnector.importToRoom(docTypeTable.getData(),tree.getAllNodes(),(Component)this);
		  }catch(Exception ex){
		  	//System.out.println("Exception occurred : " + ex.getMessage());
		  	return -1;
		  }
		    return 0;
    }
    //-----------------------------------------------------------
    private void loadDDTFromFile() {
        List docTypeList=VWTemplateConnector.loadDTTFromFile();
        clearDT(false);
        docTypeTable.addData(docTypeList);
        setEnableMode(MODE_SELECT);
    }
    //-----------------------------------------------------------
    private void loadDFSFromFile() {
        List templateFSList=VWTemplateConnector.loadDFSFromFile();
        //// Reading the security and assigned to tree data.
        Vector templateSecurityList=VWTemplateConnector.loadSecurityFromFile();
        //// Reading the Node Properties and assigned to tree data.
        Vector templateNodeProperties = VWTemplateConnector.loadNodePropertiesFromFile();
        //AdminWise.printToConsole("loadDFSFromFile templateSecurityList and node properties list");
        Vector retentionList=VWTemplateConnector.loadRetentionFromFile();
        AdminWise.printToConsole("retentionList ::::::::::"+retentionList);
        clearNode(false);
        if(templateSecurityList!=null && templateSecurityList.size()>0){
        	isSecurityInclude = true;//ChkSecurityInclude.isSelected();
        } else 	isSecurityInclude = false;
        	
        if(templateNodeProperties !=null && templateNodeProperties.size()>0){
        	isNodePropertiesInclude = true;//ChkNodePropertiesInclude.isSelected();
        } else 	isNodePropertiesInclude = false;      
     /*  if(retentionList!=null&&retentionList.size()>0){
    	   isRetentionInclude=true;
    	   AdminWise.printToConsole("Inside retention is true :::::::::"+isRetentionInclude);
       }else {
    	   isRetentionInclude=false;
    	   AdminWise.printToConsole("Inside else retention is false :::::::::"+isRetentionInclude);
       }*/
        
        	setEnableMode(MODE_SELECT);
        	ChkSecurityInclude.setSelected(isSecurityInclude);
        	//ChkSecurityInclude.setEnabled(isSecurityInclude);
        	ChkNodePropertiesInclude.setSelected(isNodePropertiesInclude);
        	//ChkNodePropertiesInclude.setEnabled(isNodePropertiesInclude);
        	//ChkIncludeRetention.setSelected(isRetentionInclude);

        	tree.loadTreeData(templateFSList, isSecurityInclude?templateSecurityList:null, isNodePropertiesInclude?templateNodeProperties:null );
        
    }
    //-----------------------------------------------------------
    void BtnExport_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            fileName=loadSaveDialog((Component)this);
            if(fileName==null || fileName.equals("")) {
                AdminWise.adminPanel.setDefaultPointer();
                return;
            }
            tree.expandAll();
            templateInfoDlg=new VWTemplateInfoDlg(AdminWise.adminFrame);
        }
        catch(Exception e){}
    }
    //-----------------------------------------------------------
    void BtnClear_actionPerformed(java.awt.event.ActionEvent event) {
        BtnNodeClear_actionPerformed(null);
        BtnDTClear_actionPerformed(null);
        docTypesList=null;
        setEnableMode(MODE_UNSELECTED);
    }
    //-----------------------------------------------------------
    void BtnImport_actionPerformed(java.awt.event.ActionEvent event) {
    	try{
    		AdminWise.adminPanel.setWaitPointer();
    		AdminWise.printToConsole("isRetentionInclude ::::::::"+isRetentionInclude);
    		VWTemplateConnector.importToRoom(docTypeTable.getData(),tree.getAllNodes(),VWTemplateConnector.loadRetentionFromFile(),VWTemplateConnector.loadNotificationFromFile(),(Component)this);
    		BtnClear_actionPerformed(null);
    		setEnableMode(MODE_UNSELECTED);
    	}
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //-----------------------------------------------------------
    /*
    private List importFST(List errorList)
    {
        int count=templateFSList.size();
        for(int i=0;i<count;i++)
        {
            VWTreeNode treeNode=(VWTreeNode)templateFSList.get(i);
            int nodeId=VWTemplateConnector.addUniqueNode(treeNode.getParentId(),treeNode.getName());
            if(nodeId<=0)
            {
                errorList.add("Error while adding node " + treeNode.getName() + "");
                break;
            }
            for(int k=i+1;k<count;k++)
            {
                int parentId=((VWTreeNode)templateFSList.get(k)).getParentId();
                if(parentId==treeNode.getId())
                    ((VWTreeNode)templateFSList.get(k)).setParentId(nodeId);
            }
            treeNode.setId(nodeId);
        }
        File importFile=new File(FSFileName);
        while(!FSFileName.endsWith("."))
            FSFileName=FSFileName.substring(0,FSFileName.length()-1);
        File newFile=new File(FSFileName + "bak2");
        importFile.renameTo(newFile);
        templateFSList=null;
        return errorList;
    }
     */
    //-----------------------------------------------------------
    void BtnNodeNew_actionPerformed(java.awt.event.ActionEvent event) {
    	try{
    		tree.expandPath(tree.getSelectionPath());
    	}catch(Exception ex){
    		
    	}
        tree.addNode(VWTemplateConnector.maxNodeIndex++);
    }
    //-----------------------------------------------------------
    void BtnDTAdd_actionPerformed(java.awt.event.ActionEvent event) {
        VWDlgAddDT addDTDlg = new VWDlgAddDT(AdminWise.adminFrame,loadSelDocTypes());
        addDTDlg.requestFocus();
        if(addDTDlg.getCancelFlag()) return;
        List retValues=addDTDlg.getValues();
        addDTDlg.dispose();
        if(retValues!=null && retValues.size()>0)
            for(int i=0;i<retValues.size();i++)
                docTypeTable.insertData((VWDocTypeRec)retValues.get(i));
        setEnableMode(MODE_SELECT);
    }
    //-----------------------------------------------------------
    private Vector loadSelDocTypes() {
        if(docTypesList==null || docTypesList.size()==0)
            docTypesList=VWTemplateConnector.getDocTypes();
        int[] exceptDocTypes=docTypeTable.getRowDocTypeIds();
        Vector selDocTypesList=new Vector();
        if(exceptDocTypes!=null && exceptDocTypes.length>0) {
            for(int i=0;i<docTypesList.size();i++)
                if(!isDTAlreadyIn(exceptDocTypes,(VWDocTypeRec)docTypesList.get(i)))
                    selDocTypesList.add(docTypesList.get(i));
            return selDocTypesList;
        }
        else {
            return docTypesList;
        }
    }
    //-----------------------------------------------------------
    private boolean isDTAlreadyIn(int[] exceptDocTypes,VWDocTypeRec docType) {
        int count=exceptDocTypes.length;
        for(int j=0;j<count;j++) {
            if(exceptDocTypes[j]==docType.getId()) return true;
        }
        return false;
    }
    //-----------------------------------------------------------
    /**
     * CV10.1 Enhancement Retention template
     * Method created By:- Madhavan
     * @return
     */
    public Vector loadAvailableRetentions() {
		// TODO Auto-generated method stub
    	//Vector availableRetentions=VWRetentionConnector.getRetentionSettings(0);
    	 VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	 Vector retention=new Vector();
         AdminWise.gConnector.ViewWiseClient.getRetentionSettings(room.getId(), -1, retention);
         AdminWise.printToConsole("retention :::::"+retention);
    	
		return retention;
	}
    
    public Vector loadAvailableNotifications(){
    	// TODO Auto-generated method stub
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Vector retention=new Vector();
    	Session session= AdminWise.gConnector.ViewWiseClient.getSession(room.getId());
    	Vector settings = new Vector();
    	AdminWise.gConnector.ViewWiseClient.getAllNotifications(room.getId(), 0, session.user,"-", settings);
    	return settings;
    }
    
    void BtnDTDel_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            int[] selectedRows=docTypeTable.getSelectedRows();
            int count=selectedRows.length;
            for(int i=0;i<count;i++) {
                int row=selectedRows[i]-i;
                docTypeTable.removeData(row);
            }
        }
        catch(Exception e){}
        finally{AdminWise.adminPanel.setDefaultPointer();}
        setEnableMode(MODE_SELECT);
    }
    //-----------------------------------------------------------
    void BtnDTClear_actionPerformed(java.awt.event.ActionEvent event) {
        clearDT(true);
    }
    //-----------------------------------------------------------
    private void clearDT(boolean withPath) {
        docTypeTable.clearData();
        BtnDTClear.setEnabled(false);
        VWTemplateConnector.ClearDTT(withPath);
    }
    //-----------------------------------------------------------
    void BtnNodeClear_actionPerformed(java.awt.event.ActionEvent event) {
        clearNode(true);
    }
    //-----------------------------------------------------------
    private void clearNode(boolean withPath) {
        VWTemplateConnector.ClearFST(withPath);
        createRoomNode();
    }
	    //-----------------------------------------------------------
			/* void BtnNodeDel_actionPerformed(java.awt.event.ActionEvent event) {
			       if(tree.getRowCount()<=1) return;
			      tree.removeNode();
	     }*/
    /**
     * CV10.1 Enhancement :-Multiple delete option in administration template.
     * Modified by :- Apurba.M
     */
    void BtnNodeDel_actionPerformed(java.awt.event.ActionEvent event) {
        int[] selectedRows=tree.getSelectionRows();
        if(tree.getRowCount()<=1) return;
        AdminWise.printToConsole("tree.getSelectionRows() VWTemplatePanel.BtnNodeDel_actionPerformed AdminWise::::"+tree.getSelectionRows().length);
        int count=selectedRows.length;
        for(int i=0;i<count;i++) {
            int row=selectedRows[i];
            AdminWise.printToConsole("row ["+i+"] VWTemplatePanel.BtnNodeDel_actionPerformed::::"+row);
            tree.removeNode();
        }
    }
    //-----------------------------------------------------------
    void BtnNodeExpandAll_actionPerformed(java.awt.event.ActionEvent event) {
        tree.expandAll();
    }
    void ChkSecurityInclude_actionPerformed(java.awt.event.ActionEvent event) {
    	isSecurityInclude = ChkSecurityInclude.isSelected();
    }
    void ChkNodePropertiesInclude_actionPerformed(java.awt.event.ActionEvent event) {
    	isNodePropertiesInclude = ChkNodePropertiesInclude.isSelected();
    }
    void ChkIncludeRetention_actionPerformed(java.awt.event.ActionEvent event){
    	isRetentionInclude=ChkIncludeRetention.isSelected();
    }
    void ChkIncludeNotification_actionPerformed(java.awt.event.ActionEvent event){
    	isNotificationInclude=ChkIncludeNotification.isSelected();
    }
    
    
    
    //-----------------------------------------------------------
   /* public void BtnRetention_actionPerformed(java.awt.event.ActionEvent event) {
 		// TODO Auto-generated method stub
    	VWDlgRetentionSetting settingDlg = new VWDlgRetentionSetting(AdminWise.adminFrame,"hello");
    	settingDlg.setVisible(true);
 	}*/
    //----------------------------------------------------------------------------
    
    private void setEnableMode(int mode) {
        setEnableMode(mode,false);
    }
    //-----------------------------------------------------------
    private void setEnableMode(int mode,boolean force) {
        curMode=mode;
        BtnLoadRoomData.setEnabled(true);
        BtnLoadFromFile.setEnabled(true);
        
        BtnClear.setEnabled(true);
        BtnNodeNew.setEnabled(true);
        BtnDTAdd.setEnabled(true);
        if(VWTemplateConnector.TFileName.equals("")) {
            BtnImport.setEnabled(false);
            BtnExport.setEnabled(true);
        }
        else {
            BtnImport.setEnabled(true);
            BtnExport.setEnabled(false);
        }
        BtnDTDel.setEnabled(true);
        BtnDTClear.setEnabled(true);
        BtnNodeClear.setEnabled(true);
        BtnNodeDel.setEnabled(true);
        BtnNodeExpandAll.setEnabled(true);
        ChkSecurityInclude.setEnabled(true);
        ChkNodePropertiesInclude.setEnabled(true);
        ChkIncludeRetention.setEnabled(true);
        ChkIncludeNotification.setEnabled(true);
        switch (mode) {
            case MODE_SELECT:
                if(docTypeTable.getRowCount()==0) {
                    BtnDTDel.setEnabled(false);
                    BtnDTClear.setEnabled(false);
                }
                if(docTypeTable.getSelectedRow()<0)
                    BtnDTDel.setEnabled(false);
                if(tree.getRowCount() > 1) {
                    BtnNodeClear.setEnabled(true);
                    BtnNodeDel.setEnabled(true);
                }
                BtnClear.setEnabled(BtnNodeClear.isEnabled() || BtnDTClear.isEnabled());
                BtnImport.setEnabled(BtnClear.isEnabled() && !VWTemplateConnector.TFileName.equals(""));
                ChkSecurityInclude.setEnabled(true);
                ChkNodePropertiesInclude.setEnabled(true);
                ChkIncludeRetention.setEnabled(true);
                ChkIncludeNotification.setEnabled(true);
                break;
            case MODE_UNSELECTED:
                BtnExport.setEnabled(false);
                BtnClear.setEnabled(false);
                BtnImport.setEnabled(false);
                BtnClear.setEnabled(false);
                BtnNodeNew.setEnabled(false);
                BtnDTAdd.setEnabled(false);
                BtnDTDel.setEnabled(false);
                BtnDTClear.setEnabled(false);
                BtnNodeClear.setEnabled(false);
                BtnNodeDel.setEnabled(false);
                BtnNodeExpandAll.setEnabled(false);
                ChkSecurityInclude.setEnabled(true);
                ChkNodePropertiesInclude.setEnabled(true);
                ChkIncludeRetention.setEnabled(true);
                ChkIncludeNotification.setEnabled(true);
                break;
            case MODE_UNCONNECT:
                BtnLoadRoomData.setEnabled(false);
                BtnLoadFromFile.setEnabled(false);
                BtnExport.setEnabled(false);
                BtnImport.setEnabled(false);
                BtnClear.setEnabled(false);
                BtnNodeNew.setEnabled(false);
                BtnDTAdd.setEnabled(false);
                BtnDTDel.setEnabled(false);
                BtnDTClear.setEnabled(false);
                BtnNodeClear.setEnabled(false);
                BtnNodeDel.setEnabled(false);
                BtnNodeExpandAll.setEnabled(false);
                ChkSecurityInclude.setEnabled(false);
                ChkNodePropertiesInclude.setEnabled(false);
                ChkIncludeRetention.setEnabled(false);
                ChkIncludeNotification.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
    //-----------------------------------------------------------
    private void stopGridEdit() {
        try{
            docTypeTable.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
    }
    //-----------------------------------------------------------
    public static String loadOpenDialog(Component parent) {
        JFileChooser chooser = new JFileChooser();
        VWFileFilter filter = new VWFileFilter();
        filter.addExtension(TemplateExt);
        filter.setDescription(PRODUCT_NAME + " Template Files");
        chooser.setFileFilter(filter);
        String homePath =VWUtil.getHome();
        if(path!=null && !path.equalsIgnoreCase(""))
        	chooser.setCurrentDirectory(new File(path));
        else if(homePath!=null && !homePath.equalsIgnoreCase(""))
        	chooser.setCurrentDirectory(new File(homePath+"\\Template"));
        else
        	chooser.setCurrentDirectory(new File("c:\\"));
    	int returnVal = chooser.showOpenDialog(parent);
    	if(returnVal == JFileChooser.APPROVE_OPTION) {
    		path=chooser.getSelectedFile().getPath();
    		return path;
    	}
        return "";
    }
    //-----------------------------------------------------------
    public static String loadSaveDialog(Component parent) {
        JFileChooser chooser = new JFileChooser();
        VWFileFilter filter = new VWFileFilter();
        filter.addExtension(TemplateExt);
        filter.setDescription(PRODUCT_NAME + " Template Files");
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new File("c:\\"));
        int returnVal = chooser.showSaveDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String path=chooser.getSelectedFile().getPath();
            return path;
        }
        return "";
    }
    //-----------------------------------------------------------
    private static int gCurRoom=0;
    String localLanguage=AdminWise.getLocaleLanguage();
    private static int curMode=-1;
    private boolean docTypeLoaded=false;
    public static Vector docTypesList=null;
    public VWTemplateInfoDlg templateInfoDlg=null;
    public String fileName="";
    public static String path= "";
    private boolean UILoaded=false;
    public static boolean isSecurityInclude = false;
    public static boolean isNodePropertiesInclude = false;
    public static boolean isRetentionInclude = false;
    public static boolean isNotificationInclude = false;
    //-----------------------------------------------------------
}