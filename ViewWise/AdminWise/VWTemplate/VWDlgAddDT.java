/*
		A basic implementation of the JDialog class.
*/

package ViewWise.AdminWise.VWTemplate;

import java.awt.*;
import javax.swing.*;
import ViewWise.AdminWise.VWUtil.VWCheckList;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.util.List;
import java.util.Vector;
import java.util.LinkedList;
import java.util.prefs.*;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWConstant;

public class VWDlgAddDT extends javax.swing.JDialog implements VWConstant
{
	public VWDlgAddDT(Frame parent,Vector docTypes) 
	{
		super(parent);
                ///parent.setIconImage(VWImages.DocTypesIcon.getImage());
		getContentPane().setBackground(AdminWise.getAWColor());
		setTitle(LBL_ADDDOCTYPE_NAME);
		setModal(true);
		getContentPane().setLayout(null);
		setSize(283,438);
		setVisible(false);
		JLabel1.setText(LBL_LISTDOCTYPE_NAME);
		getContentPane().add(JLabel1);
		JLabel1.setBounds(6,4,194, AdminWise.gBtnHeight);
		getContentPane().add(LstDT);
		LstDT.setBounds(4,28,276,366);
		BtnAdd.setText(BTN_ADD_NAME);
		getContentPane().add(BtnAdd);
		BtnAdd.setBounds(6,410,82,20);
                //BtnAdd.setBackground(java.awt.Color.white);
                BtnAdd.setIcon(VWImages.AddIcon);
		BtnSelectAll.setText(BTN_SELECTALL_NAME);
		getContentPane().add(BtnSelectAll);
		BtnSelectAll.setBounds(100,410,82,20);
                //BtnSelectAll.setBackground(java.awt.Color.white);
                BtnSelectAll.setIcon(VWImages.DeselectAllIcon);
		BtnCancel.setText(BTN_CANCEL_NAME);
		getContentPane().add(BtnCancel);
		BtnCancel.setBounds(194,410,82,20);
                //BtnCancel.setBackground(java.awt.Color.white);
                BtnCancel.setIcon(VWImages.CloseIcon);
                try{
                    loadedList=false;
                    LstDT.loadData(docTypes);
                    loadedList=true;
                }
                catch(Exception e){loadedList=true;}
                if(LstDT.getItemsCount()==0)BtnSelectAll.setEnabled(false);
                SymAction lSymAction = new SymAction();
                BtnAdd.addActionListener(lSymAction);
                BtnSelectAll.addActionListener(lSymAction);
                BtnCancel.addActionListener(lSymAction);
                SymKey aSymKey = new SymKey();
	        addKeyListener(aSymKey);
                LstDT.addKeyListener(aSymKey);
                SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
                setResizable(false);
                getDlgOptions();
                setVisible(true);
		//}}
	}
//------------------------------------------------------------------------------     
  class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
                Object object = event.getSource();
                if (object == VWDlgAddDT.this)
                        Dialog_windowClosing(event);
        }
    }
//------------------------------------------------------------------------------
    void Dialog_windowClosing(java.awt.event.WindowEvent event)
    {
        BtnCancel_actionPerformed(null);
    }
//------------------------------------------------------------------------------
  class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnAdd_actionPerformed(null);
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnAdd)
                        BtnAdd_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
                else if (object == BtnSelectAll)
                        BtnSelectAll_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
	public void setVisible(boolean b)
	{
		if (b) 
                {
                    Dimension d =VWUtil.getScreenSize();
                    setLocation(d.width/4,d.height/4);
                }
		super.setVisible(b);
	}
//------------------------------------------------------------------------------
	public void addNotify()
	{
		// Record the size of the window prior to calling parents addNotify.
		Dimension size = getSize();

		super.addNotify();

		if (frameSizeAdjusted)
			return;
		frameSizeAdjusted = true;

		// Adjust size of frame according to the insets
		Insets insets = getInsets();
		setSize(insets.left + insets.right + size.width, insets.top + insets.bottom + size.height);
	}
//------------------------------------------------------------------------------
	boolean frameSizeAdjusted = false;
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	VWCheckList LstDT = new VWCheckList();
	VWLinkedButton BtnAdd = new VWLinkedButton(0,true);
	VWLinkedButton BtnSelectAll = new VWLinkedButton(0,true);
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
//------------------------------------------------------------------------------
    void BtnAdd_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=false;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        cancelFlag=true;
        this.setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnSelectAll_actionPerformed(java.awt.event.ActionEvent event)
    {
        if(BtnSelectAll.getText().equals(BTN_SELECTALL_NAME))
        {
            LstDT.selectAllItems();
            BtnSelectAll.setText(BTN_DESELECTALL_NAME);
        }
        else
        {
            LstDT.deSelectAllItems();
            BtnSelectAll.setText(BTN_SELECTALL_NAME);
        }
    }
//------------------------------------------------------------------------------
    public List getValues()
    {
        List selIndices=new LinkedList();
        int count=LstDT.getItemsCount();
        for(int i=0;i<count;i++)
            if(LstDT.getItemIsCheck(i))
                selIndices.add(LstDT.getItem(i));
        return selIndices;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        else
        {
            prefs.putInt( "x",50);
            prefs.putInt( "y",50);
        }
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());        
        setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
boolean cancelFlag=true;
boolean loadedList=true;
}