package ViewWise.AdminWise.VWTemplate;

import java.util.Vector;

public class RetentionTemplateData {
	private String retentionname;
	private String retentionSettings;
	private Vector retentionIndex;
	RetentionTemplateData(String retentionname,String retentionSettings,Vector retentionIndex ){
		 this.retentionname=retentionname;
		 this.retentionSettings = retentionSettings;
		 this.retentionIndex = retentionIndex;
	}
	public String getRetentionSettings() {
		return retentionSettings;
	}
	/*public void setRetentionSettings(String retentionSettings) {
		this.retentionSettings = retentionSettings;
	}*/
	public Vector getRetentionIndex() {
		return retentionIndex;
	}
	/*public void setRetentionIndex(Vector retentionIndex) {
		this.retentionIndex = retentionIndex;
	}*/
	/*public String getRetentionname() {
		return retentionname;
	}*/
	/*public void setRetentionname(String retentionname) {
		this.retentionname = retentionname;
	}*/
	public String getRetentionName() {
		return retentionname;
	}
}
