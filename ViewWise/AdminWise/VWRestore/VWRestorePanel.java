package ViewWise.AdminWise.VWRestore;

import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import ViewWise.AdminWise.VWStorage.VWDlgStorageLocation;
import ViewWise.AdminWise.VWFind.VWFindDlg;
import ViewWise.AdminWise.VWFind.VWRestoreFindDlg;

import java.awt.Component;
import javax.swing.JFileChooser;
import javax.swing.filechooser.*;

import com.computhink.common.Document;
import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWCUtil;

import java.io.*;
import ViewWise.AdminWise.VWFind.VWDlgLocation;
import ViewWise.AdminWise.VWMessage.*;
import java.util.prefs.*;

public class VWRestorePanel extends JPanel implements VWMessageConstant, VWConstant {
    public VWRestorePanel() {
        
    }
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWRestore.setLayout(null);
        add(VWRestore,BorderLayout.CENTER);
        VWRestore.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWRestore.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
/*        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);*/

        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnLoad.setText(BTN_LOAD_NAME);
        BtnLoad.setActionCommand(BTN_LOAD_NAME);
        PanelCommand.add(BtnLoad);
        //BtnLoad.setBackground(java.awt.Color.white);
        BtnLoad.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnLoad.setIcon(VWImages.LoadIcon);
        
        top += hGap;
        
        BtnFind.setText(BTN_Find_NAME);
        BtnFind.setActionCommand(BTN_Find_NAME);
        PanelCommand.add(BtnFind);
        //BtnFind.setBackground(java.awt.Color.white);
        BtnFind.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnFind.setIcon(VWImages.FindIcon);
        
        top += hGap + 12;
        BtnClearList.setText(BTN_CLEAR_NAME);
        BtnClearList.setActionCommand(BTN_CLEAR_NAME);
        PanelCommand.add(BtnClearList);
        //BtnClearList.setBackground(java.awt.Color.white);
        BtnClearList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearList.setIcon(VWImages.ClearIcon);
        
        top += hGap;
        BtnClearSelList.setText(BTN_CLEARSELECTED_NAME);
        BtnClearSelList.setActionCommand(BTN_CLEARSELECTED_NAME);
        PanelCommand.add(BtnClearSelList);
        //BtnClearSelList.setBackground(java.awt.Color.white);
        BtnClearSelList.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnClearSelList.setIcon(VWImages.ClearIcon);
        
        top += hGap + 12;
        BtnRestore.setText(BTN_Restore_NAME);
        BtnRestore.setActionCommand(BTN_Restore_NAME);
        PanelCommand.add(BtnRestore);
        //BtnRestore.setBackground(java.awt.Color.white);
        BtnRestore.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnRestore.setIcon(VWImages.RestoreIcon);
        
        top += hGap;
        BtnRestoreTo.setText(BTN_RESTORETO_NAME);
        BtnRestoreTo.setActionCommand(BTN_RESTORETO_NAME);
        PanelCommand.add(BtnRestoreTo);
        //BtnRestoreTo.setBackground(java.awt.Color.white);
        BtnRestoreTo.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnRestoreTo.setIcon(VWImages.RestoreIcon);
        
        top += hGap + 12;
        BtnOptions.setText(BTN_OPTIONS_NAME);
        BtnOptions.setActionCommand(BTN_OPTIONS_NAME);
        PanelCommand.add(BtnOptions);
        //BtnOptions.setBackground(java.awt.Color.white);
        BtnOptions.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnOptions.setIcon(VWImages.OptionsIcon);
        
        top += hGap;
        BtnProperties.setText(BTN_PROPERTIES_NAME);
        BtnProperties.setActionCommand(BTN_PROPERTIES_NAME);
        PanelCommand.add(BtnProperties);
        BtnProperties.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnProperties.setIcon(VWImages.StatisticIcon);
        
        PanelCommand.add(progress);
        progress.setBounds(12,370,144, AdminWise.gSmallBtnHeight);
        progress.setMinimum(0);
        progress.setVisible(false);
        progress.setStringPainted(true);
        PanelTable.setLayout(new BorderLayout());
        VWRestore.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWRestore.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.RestoreImage);
        
        SymAction lSymAction = new SymAction();
        BtnLoad.addActionListener(lSymAction);
        BtnFind.addActionListener(lSymAction);
        BtnClearList.addActionListener(lSymAction);
        BtnClearSelList.addActionListener(lSymAction);
        BtnRestore.addActionListener(lSymAction);
        BtnRestoreTo.addActionListener(lSymAction);
        BtnOptions.addActionListener(lSymAction);
        BtnProperties.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnLoad)
                BtnLoad_actionPerformed(event);
            else if (object == BtnFind)
                BtnFind_actionPerformed(event);
            else if (object == BtnClearList)
                BtnClearList_actionPerformed(event);
            else if (object == BtnClearSelList)
                BtnClearSelList_actionPerformed(event);
            else if (object == BtnRestore)
                BtnRestore_actionPerformed(event);
            else if (object == BtnRestoreTo)
                BtnRestoreTo_actionPerformed(event);
            else if (object == BtnOptions)
                BtnOptions_actionPerformed(event);
            else if (object == BtnProperties)
                BtnProperties_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    VWPicturePanel picPanel = new VWPicturePanel(TAB_RESTORE_NAME, VWImages.RestoreImage);
    javax.swing.JPanel VWRestore = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnLoad = new VWLinkedButton(1);
    VWLinkedButton BtnFind = new VWLinkedButton(1);
    VWLinkedButton BtnClearList = new VWLinkedButton(1);
    VWLinkedButton BtnClearSelList = new VWLinkedButton(1);
    VWLinkedButton BtnRestore = new VWLinkedButton(1);
    VWLinkedButton BtnRestoreTo = new VWLinkedButton(1);
    VWLinkedButton BtnOptions = new VWLinkedButton(1);
    VWLinkedButton BtnProperties = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWRestoreTable Table = new VWRestoreTable();
    javax.swing.JProgressBar progress = new javax.swing.JProgressBar();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWRestore)
                VWRestore_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWRestore_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWRestore) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        restoreFindDlg = null;
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnLoad_actionPerformed(java.awt.event.ActionEvent event) {
        String fileName=loadOpenDialog((Component)this);
        if(fileName==null || fileName.equals("")) return;
        File selFolder=new File(fileName);
        File[] subFolders=selFolder.listFiles();
        int subFolderCount=subFolders.length;
// Added By Mallikarjuna to empty the DateVector   
        VWCUtil.DateVector.removeAllElements ();
//  Added By Mallikarjuna to empty the DateVector        
        int retValue=VWRestoreConnector.checkIsDocBackupDir(selFolder);
        if(retValue>0) {
            String str=selFolder.getName()+VWConstant.SepChar+
            selFolder.getPath()+VWConstant.SepChar+(retValue==2?"Yes":"No");
            Table.insertData(str);
        }
        else {
            for(int i=0;i<subFolderCount;i++) {
                retValue=VWRestoreConnector.checkIsDocBackupDir(subFolders[i]);
                if(retValue>0) {
                    String str=subFolders[i].getName()+VWConstant.SepChar+
                    subFolders[i].getPath()+VWConstant.SepChar+(retValue==2?"Yes":"No");
                    Table.insertData(str);
                }
            }
        }
        setEnableMode(MODE_UNSELECTED);
    }
//  --------------------------------------------------------------------------
    void BtnFind_actionPerformed(java.awt.event.ActionEvent event) {
    	if(restoreFindDlg != null)
    		restoreFindDlg = null;
    	restoreFindDlg = new ViewWise.AdminWise.VWFind.VWRestoreFindDlg(AdminWise.adminFrame);
    	restoreFindDlg.setVisible(true);
    }
    //--------------------------------------------------------------------------
    void BtnClearList_actionPerformed(java.awt.event.ActionEvent event) {
        clearTable();
    }
    //--------------------------------------------------------------------------
    void BtnClearSelList_actionPerformed(java.awt.event.ActionEvent event) {
        Table.deleteSelectedRows();
    }
    //--------------------------------------------------------------------------
    void BtnRestore_actionPerformed(java.awt.event.ActionEvent event) {
        int rowCount=Table.getRowCount();
        // Displayed the message before start the restore process.
        int startRestore = JOptionPane.showConfirmDialog(this,AdminWise.connectorManager.getString("VWRestorePanel.msg_1"),MSG_TITLE,
                JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE,null);
        if (startRestore == JOptionPane.OK_OPTION){
        if(AdminWise.adminPanel.confirmRestoreDocuments)
            if(VWMessage.showWarningDialog((java.awt.Component) this,
            WRN_RESTOTE_1+rowCount+
            WRN_RESTOTE_2+AdminWise.adminPanel.roomPanel.getSelectedRoom().getName()+ "?"
            )!=javax.swing.JOptionPane.YES_OPTION) return;
        VWRestoreConnector.initLogFile();
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Initialize the existing restored document type status.
*/        
        VWRestoreConnector.docTypeStatus = null;
        VWRestoreConnector.docTypeStatus = new Hashtable();
        VWRestoreConnector.docTypeIndexInfo= null;
        VWRestoreConnector.docTypeIndexInfo = new HashMap();
        restoreDocuments(0);
    }
    }
    //--------------------------------------------------------------------------
    void BtnRestoreTo_actionPerformed(java.awt.event.ActionEvent event) {
        int parentNodeId=0;
        if(dlgLocation != null) {
            dlgLocation.loadDlgData();
            dlgLocation.setVisible(true);
        }
        else {
            dlgLocation = new VWDlgLocation(AdminWise.adminFrame,
                LBL_RESTORELOCATION_TITLE);
            dlgLocation.setVisible(true);
        }
        if(dlgLocation.cancelFlag) return;
        parentNodeId=dlgLocation.tree.getSelectedNodeId();
        if(parentNodeId==0 || dlgLocation.tree.getSelectedNodeType()!=Folder_TYPE)
        {
            VWMessage.showMessage((java.awt.Component) this,ERR_LOCATIONERROR);
            return;
        }
        int rowCount=Table.getRowCount();
        VWRestoreConnector.initLogFile();
        VWRestoreConnector.docTypeStatus = null;
        VWRestoreConnector.docTypeStatus = new Hashtable();
        VWRestoreConnector.docTypeIndexInfo= null;
        VWRestoreConnector.docTypeIndexInfo = new HashMap();        
        restoreDocuments(parentNodeId);
    }
    //--------------------------------------------------------------------------
    public int getRowCount() {
        return Table.getRowCount();
    }
    //--------------------------------------------------------------------------
    public void addData(List data) {
        Table.addData(data);
    }
    //--------------------------------------------------------------------------
    public void addData(String[][] data) {
        Table.addData(data);
    }
    //--------------------------------------------------------------------------
    public void insertData(String[][] data) {
        Table.insertData(data);
    }
    //--------------------------------------------------------------------------
    public void insertData(List data) {
        Table.insertData(data);
    }
    //--------------------------------------------------------------------------
    public void clearTable() {
        Table.clearData();
        setEnableMode(MODE_UNSELECTED);
    }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode) {
        BtnLoad.setVisible(true);
        BtnFind.setVisible(true);
        BtnClearList.setVisible(true);
        BtnClearSelList.setVisible(true);
        BtnRestore.setVisible(true);
        BtnRestoreTo.setVisible(true);
        BtnLoad.setEnabled(true);
        BtnFind.setEnabled(true);
        BtnClearList.setEnabled(true);
        BtnClearSelList.setEnabled(true);
        BtnRestore.setEnabled(true);
        BtnRestoreTo.setEnabled(true);
        BtnOptions.setEnabled(true);
        BtnProperties.setEnabled(true);
        switch (mode) {
            case MODE_UNSELECTED:
                BtnClearSelList.setEnabled(false);
                if(Table.getRowCount()==0) {
                    BtnClearList.setEnabled(false);
                    BtnRestore.setEnabled(false);
                    BtnRestoreTo.setEnabled(false);
                    BtnProperties.setEnabled(false);
                }
                break;
            case MODE_SELECT:
                break;
            case MODE_UNCONNECT:
                BtnLoad.setEnabled(false);
                BtnFind.setEnabled(false);
                BtnClearList.setEnabled(false);
                BtnClearSelList.setEnabled(false);
                BtnRestore.setEnabled(false);
                BtnRestoreTo.setEnabled(false);
                BtnOptions.setEnabled(false);
                BtnProperties.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
    //--------------------------------------------------------------------------
    public static String loadOpenDialog(Component parent) {
        JFileChooser chooser = new JFileChooser();
        if(!lastPath.equals(""))
            chooser.setSelectedFile(new File(lastPath));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setCurrentDirectory(new File(AdminWise.adminPanel.backupPath));
        int returnVal = chooser.showOpenDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String path=chooser.getSelectedFile().getPath();
            if(!path.equals("")) lastPath=path;
            return path;
        }
        return "";
    }
    //--------------------------------------------------------------------------
    void BtnOptions_actionPerformed(java.awt.event.ActionEvent event) {
        new VWOptionsDlg(AdminWise.adminFrame);
    }
    //--------------------------------------------------------------------------
    void BtnProperties_actionPerformed(java.awt.event.ActionEvent event) {
    	int selRowCount = Table.getSelectedRowCount();
    	if(selRowCount > 1){
    		VWMessage.showMessage(this,AdminWise.connectorManager.getString("VWRestorePanel.msg_2"));
    		return;
    	}    		
    	showProperties(Table.getRowDocPath(Table.getSelectedRow()));
    }
    //--------------------------------------------------------------------------
    private boolean restoreDocuments(int parentNodeId) {
        SPTable.paintImmediately(SPTable.getVisibleRect());
        boolean err=false;
        boolean cancelRestore = false;
        boolean showDSSCritical = true;
        try{
            AdminWise.adminPanel.setWaitPointer();
            int rowNum=0;
            String restoreDocPath="";
            int count=Table.getRowCount();
            viewProgress(count,"Restore");
            if(AdminWise.adminPanel.restoreWriteInfoLog)
            {
                VWRestoreConnector.writeInfoToLog("Restore " +
                    String.valueOf(count) + " document(s)");
                VWRestoreConnector.writeInfoToLog("Restore type : " +
                    RestoreType[AdminWise.adminPanel.restoreType]);
                VWRestoreConnector.writeInfoToLog("Restore with version revision info : " +
                    String.valueOf(AdminWise.adminPanel.restoreWithVR));
                VWRestoreConnector.writeInfoToLog("Restore with audit trail info : " +
                    String.valueOf(AdminWise.adminPanel.restoreWithAT));
                VWRestoreConnector.writeInfoToLog("Restore with folders : " +
                    String.valueOf(AdminWise.adminPanel.restoreWithFolders));
                VWRestoreConnector.writeInfoToLog("Restore Sequence Seed : " +
                    RestoreSequenceSeed[AdminWise.adminPanel.restoreSequenceSeed]);
            }
            /*
			Issue No / Purpose:  <563/Adminwise-Restore>
			Created by: <Pandiya Raj.M>
			Date: <21 Jul 2006>
			if user cancelled the restore process displayed the Error message.
             */
            /**
             * DocumentTypeNotPermitted condition modified to display the alert message only once for multiple documents
             * and to allow the doctype permitted documents to restore based on the custom doctype set in the cabinet 
             * Modified :- CV83B3(messageCount,docTypeValFlag added newly)
             */
            int messageCount=0;
            for(int i=0;i<count && !cancelRestore;i++) {
                int retVal=0;             
                boolean docTypeValFlag=true;
                try{
                	// Added By Mallikarjuna to Restore Created and Modifed Dates           	
                	VWCUtil.setDocId (i);
                    restoreDocPath=AdminWise.adminPanel.restorePanel.Table.getRowDocPath(rowNum);
                    retVal=VWRestoreConnector.restoreDocument(restoreDocPath,parentNodeId);
                    if (retVal == CancelRestoreProcess){
                    	err = true;
                    	cancelRestore = false;
                        VWRestoreConnector.writeInfoToLog(AdminWise.gConnector.
                                ViewWiseClient.getErrorDescription(retVal));
                        VWMessage.showInfoDialog(VWMessage.ERR_RESTORE_CANCELLED);
                        break;
                    }
                    if(retVal==ViewWiseErrors.dssCriticalError)
                    {
                    	showDSSCritical = false;
                    	JOptionPane.showMessageDialog(null, AdminWise.gConnector.
                    			ViewWiseClient.getErrorDescription(retVal),"Adminwise", JOptionPane.ERROR_MESSAGE);
                    	 break;
                    }
                    if(retVal==ViewWiseErrors.DocumentTypeNotPermitted)
                    {
                    	showDSSCritical = false;
                    	if(messageCount<=0)
                    	JOptionPane.showMessageDialog(null, AdminWise.connectorManager.getString("VWRestorePanel.DocumentTypeNotPermitted"),AdminWise.connectorManager.getString("Administration.Title"), JOptionPane.ERROR_MESSAGE);
                    	messageCount=messageCount+1;
                    	retVal=0;
                    	docTypeValFlag=false;
                    }
                }
                catch(Exception e){
                    VWRestoreConnector.writeErrorToLog(restoreDocPath,e.getMessage());
                    retVal=1;
                }
                if(docTypeValFlag==true)
                setProgressValue(i,"Restore");
                if((retVal>=0)&&(docTypeValFlag==true))
                {
                    Table.deleteRow(rowNum);
                }
                else
                {
                	if(retVal!=ViewWiseErrors.dssCriticalError&&retVal!=ViewWiseErrors.DocumentTypeNotPermitted){
                		VWRestoreConnector.writeInfoToLog(AdminWise.gConnector.
                				ViewWiseClient.getErrorDescription(retVal));
                		err=true;
                		rowNum++;
                	}
                }
            }
            setEnableMode(MODE_UNSELECTED);
        }
        catch(Exception e){}
        finally{
            AdminWise.adminPanel.setDefaultPointer();
            hideProgress();
        }
        String logPath=VWRestoreConnector.writeLogFile();
        if(showDSSCritical){ 
        if(!err) {
            VWMessage.showInfoDialog(VWMessage.MSG_RESTORECOMPLETED);
        }
        else {
            try {
                if(VWMessage.showConfirmDialog((java.awt.Component) this,
                    "Error log at "+logPath+" ,view it?")==javax.swing.JOptionPane.YES_OPTION)
                    java.lang.Runtime.getRuntime().exec("notepad.exe "+logPath);
            }
            catch(java.io.IOException e){}
        }
       }
        return true;
    }
    //--------------------------------------------------------------------------
    private void viewProgress(int max,String action) {
        progress.setMaximum(max);
        progress.setVisible(true);
        progress.setString(" 1 / "+max);
    }
    //--------------------------------------------------------------------------
    private void hideProgress() {
        progress.setVisible(false);
    }
    //--------------------------------------------------------------------------
    private void setProgressValue(int value,String action) {
        progress.setValue(value);
        progress.setString(String.valueOf(value)+" / "+progress.getMaximum());
        progress.paintImmediately(progress.getVisibleRect());
    }
    //--------------------------------------------------------------------------
    public void showProperties(String dirPath) {
    	restorePropertiesDlg = new VWPropertiesDlg(AdminWise.adminFrame, dirPath);
    	restorePropertiesDlg.setVisible(true);		
	}
    //--------------------------------------------------------------------------
    public int checkAvailbaleDSSSpace(int roomId,String movePath,Vector size)
    {
    	Vector result = new Vector();
    	AdminWise.gConnector.ViewWiseClient.getLatestDocId(roomId, result);
    	int docId = Integer.parseInt((String) result.get(0));
    	Document doc = new Document(docId);
    	int retValue = AdminWise.gConnector.ViewWiseClient.checkAvailableSpaceInDSS(roomId, doc,movePath,size);
    	return retValue;
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    public VWRestoreFindDlg restoreFindDlg = null;
    public VWPropertiesDlg restorePropertiesDlg = null;
    public VWDlgLocation dlgLocation = null;
    public VWRestoreInfoDlg restoreInfoDlg=null;
    private static String lastPath="";
    private boolean UILoaded=false;
}
    //--------------------------------------------------------------------------