package ViewWise.AdminWise.VWRestore;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.UIManager;

import com.computhink.common.Index;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWFind.VWReadXML;

public class VWPropertiesDlg extends javax.swing.JDialog implements VWConstant
{

	public VWPropertiesDlg(Frame parent, String dirPath)
	{
		super(parent,AdminWise.connectorManager.getString("VWPropertiesDlg.Title"),true);
		///parent.setIconImage(VWImages.AdminWiseIcon.getImage());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(VWProperties,BorderLayout.CENTER);
		getContentPane().setBackground(AdminWise.getAWColor());
		setSize(400,250);
		setLocationRelativeTo(null);

		//setVisible(true);
		VWProperties.setLayout(null);
		
		PanelCommand.setLayout(null);
		VWProperties.add(PanelCommand);
		PanelCommand.setBounds(0,0,400,20);
		
		creationDate.setText(AdminWise.connectorManager.getString("VWPropertiesDlg.creationDate")+" ");
		creationDate.setBounds(2, 7, 200, 10);
		PanelCommand.add(creationDate);

		modifiedDate.setText(AdminWise.connectorManager.getString("VWPropertiesDlg.modifiedDate")+" ");
		modifiedDate.setBounds(218, 7, 400, 10);
		PanelCommand.add(modifiedDate);

		PanelTable.setLayout(new BorderLayout());
		VWProperties.add(PanelTable);
		PanelTable.setBounds(0,25,394,193);
	
		SPTable.setBounds(0,40,395,129);
		SPTable.setOpaque(true);
		PanelTable.add(SPTable,BorderLayout.CENTER);
		SPTable.getViewport().add(Table);
		Table.getParent().setBackground(java.awt.Color.white);

		setResizable(false);
		//setVisible(true);
		loadData(dirPath);
		
	}
	private void loadData(String dirPath) {
		Table.clearData();
		String docTypeName = "";
		Vector<Index> docTypeIndices = new Vector<Index>();
		Vector<String> dateInfo = new Vector<String>();
		docTypeName = VWReadXML.getDocProperties(dirPath, dateInfo, docTypeIndices);
		setTitle(docTypeName + " - Properties");
		creationDate.setText(AdminWise.connectorManager.getString("VWPropertiesDlg.creationDate")+" "+dateInfo.get(0));
		modifiedDate.setText(AdminWise.connectorManager.getString("VWPropertiesDlg.modifiedDate")+" "+dateInfo.get(1));
		AdminWise.printToConsole("docTypeIndices >>>>>"+docTypeIndices);
		Table.addData(docTypeIndices);
	}
	//------------------------------------------------------------------------------
	javax.swing.JPanel VWProperties = new javax.swing.JPanel();
	javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
	javax.swing.JPanel PanelTable = new javax.swing.JPanel();
	javax.swing.JLabel creationDate = new javax.swing.JLabel();
	javax.swing.JLabel modifiedDate = new javax.swing.JLabel();
	javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
	VWPropertiesTable Table = new VWPropertiesTable();

//	------------------------------------------------------------------------------
	public void clearTable()
	{
		Table.clearData();    
		//setMode(Mode_Unload);    
	}
	//--------------------------------------------------------------------------
	public String getDocumentKey()
	{
		return Table.getKeyValue();
	}
	//--------------------------------------------------------------------------

	public static void main(String args[]){
		String path = "C:\\Documents and Settings\\vijaykumar.s\\Desktop\\Backup Doc\\Document\\Document [TestServer.VW7][3727]";
		VWPropertiesDlg dlg = new VWPropertiesDlg(AdminWise.adminFrame, path);
		/*
		 * JDialog.show() is replaced with JDialog.setVisible() 
		 * as JDialog.show() is deprecated Gurumurthy.T.S 19/12/2013,CV8B5-001
		 */
		dlg.setVisible(true);
	}
}