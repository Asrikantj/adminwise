/*
                      Copyright (c) 1997-2001
                        Computhink Software

                      All rights reserved.
 */
/**
 * VWRestoreConnector<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package ViewWise.AdminWise.VWRestore;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.awt.Component;
import java.util.Hashtable;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWStorage.VWStorageConnector;
import ViewWise.AdminWise.VWUtil.VWSAXParser;
import ViewWise.AdminWise.VWDocType.VWDocTypeRec;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import ViewWise.AdminWise.VWTemplate.VWTemplateConnector;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWRecycle.VWRecycleConnector;
import com.computhink.common.Document;
import com.computhink.common.Index;
import com.computhink.common.Util;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWDocRestore;

public class VWRestoreConnector implements VWConstant {
    public static int checkIsDocBackupDir(File docDir) {
        if(docDir==null) return 0;
        File[] subDir=docDir.listFiles();
        if(subDir==null)return 0;
        boolean withPages=false;
        boolean validFolder=false;
 // Added By Mallikarjuna to Restore Created and Modifed Dates   
        String location = "";String FileName = "";  
        for(int i=0;i<subDir.length;i++)
        {
            if(subDir[i].getName().equalsIgnoreCase(BackupFileName))
            {
            	validFolder=true;
            	location = subDir[i].getPath();
            	FileName = subDir[i].getName();
            	VWCUtil.parseXML (location);
            }
            if(subDir[i].getName().equalsIgnoreCase(BackupPageFolderName))
            {
// Added By Mallikarjuna to Restore Created and Modifed Dates       	
                File pagesFolder=new File(subDir[i].getPath());
                if(pagesFolder.list().length>0) withPages=true;
            }
        }
        if(withPages) return 2;
        if(validFolder) return 1;
        return 0;
    }
    //--------------------------------------------------------------------------
    public static int restoreDocument(String docDir,int parentNodeId) {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        
        AdminWise.adminPanel.restorePanel.progress.setValue(10);
        /*
        System.out.println("restoreType->"+AdminWise.adminPanel.restoreType);
        System.out.println("restoreWithVR->"+AdminWise.adminPanel.restoreWithVR);
        System.out.println("restoreReferences->"+AdminWise.adminPanel.restoreReferences);
        System.out.println("restoreWithFolders->"+AdminWise.adminPanel.restoreWithFolders);
        System.out.println("restoreSequenceSeed->"+AdminWise.adminPanel.restoreSequenceSeed);
         */
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Pass the existing restored document type status.
*/
         
        if(AdminWise.adminPanel.restoreWriteInfoLog)
            errorList.add(AdminWise.connectorManager.getString("VWRestoreConnector.infoRestore")+" " + docDir);
        int retVal=AdminWise.gConnector.ViewWiseClient.restoreDoc(
        room.getId(), docDir, parentNodeId,
        AdminWise.adminPanel.restoreType,
        AdminWise.adminPanel.restoreWithVR,
        AdminWise.adminPanel.restoreReferences,
        AdminWise.adminPanel.restoreWithAT,
        AdminWise.adminPanel.restoreWithFolders,
        AdminWise.adminPanel.restoreSequenceSeed,
        docTypeStatus,docTypeIndexInfo,
 // Added By Mallikarjuna to Restore Created and Modifed Dates 
   	        AdminWise.adminPanel.restoreCreatedDate);
        if(retVal<0) {
        	errorList.add(docDir+AdminWise.gConnector.
        			ViewWiseClient.getErrorDescription(retVal));
        	// Added for, Bug 206 CV8B5-002,Gurumurthy.T.S
        	if(VWDocRestore.backupMsg!=null && VWDocRestore.backupMsg!=""){
        		errorList.add(VWDocRestore.backupMsg);
        		VWDocRestore.backupMsg ="";
        	}
        	
            return retVal;
        }
        errorList.add(AdminWise.connectorManager.getString("VWRestoreConnector.info")+" " + docDir +" "+ AdminWise.connectorManager.getString("VWRestoreConnector.restore"));
        AdminWise.gConnector.setDocATData(AdminWise.adminPanel.roomPanel.getSelectedRoomId(),
        String.valueOf(retVal),"18","From Path="+docDir);
        AdminWise.adminPanel.restorePanel.progress.setValue(100);
        return retVal;
    }
    //--------------------------------------------------------------------------
    public static void initLogFile() {
        errorList=new Vector();
    }
    //--------------------------------------------------------------------------
    public static String writeLogFile() {
        if(errorList.size()==0) return null;
        if(!AdminWise.adminPanel.restoreLogPath.equals("")) {
            File logFile=new File(AdminWise.adminPanel.restoreLogPath);
            if((!logFile.exists() && !logFile.mkdirs())||!logFile.canWrite())
                AdminWise.adminPanel.restoreLogPath="";
        }
        if(!AdminWise.adminPanel.restoreLogPath.equals("")) {
            String path=AdminWise.adminPanel.restoreLogPath+
            File.separator+"VWRestore.log";
            VWUtil.writeListToFile(errorList,path,(Component)AdminWise.adminPanel);
            return path;
        }
        else {
            return VWUtil.writeListToTmpFile((Component)AdminWise.adminPanel,
            errorList,"VWRestore.log");
        }
    }
    //--------------------------------------------------------------------------
    public static void writeErrorToLog(String restoreDocPath,String errMsg) {
        errorList.add(restoreDocPath+ " : "+errMsg);
    }
    //--------------------------------------------------------------------------
    public static void writeInfoToLog(String msg) {
        errorList.add(AdminWise.connectorManager.getString("VWRestoreConnector.info")+" "+msg);
    }
    //--------------------------------------------------------------------------
    private static Vector errorList = new Vector();
    public static Hashtable docTypeStatus = new Hashtable(); 
    public static HashMap docTypeIndexInfo = new HashMap();
}