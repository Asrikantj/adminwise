package ViewWise.AdminWise.VWRestore;

import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import java.awt.Dimension;
import java.awt.event.MouseEvent;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.ColorData;
import ViewWise.AdminWise.VWTable.ColoredTableCellRenderer;

import com.computhink.common.Signature;
import com.computhink.common.Index;
import com.computhink.common.util.VWTableResizer;


public class VWPropertiesTable extends JTable
{
	protected VWPropertiesTableData m_data;
	final static int COL_COUNT=5;
	public VWPropertiesTable(){
		super();
		JTextField readOnlyText=new JTextField();
		setFont(readOnlyText.getFont());
		m_data = new VWPropertiesTableData(this);
		setAutoCreateColumnsFromModel(false);
		setCellSelectionEnabled(false);
		setModel(m_data); 
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		readOnlyText.setEditable(false);
		Dimension tableWith = getPreferredScrollableViewportSize();
		for (int k = 0; k < VWPropertiesTableData.m_columns.length; k++) {
			TableCellRenderer renderer;
			DefaultTableCellRenderer textRenderer =new ColoredTableCellRenderer();
			textRenderer.setHorizontalAlignment(VWPropertiesTableData.m_columns[k].m_alignment);
			renderer = textRenderer;
			//VWPropertiesRowEditor editor=new VWPropertiesRowEditor(this);
			TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWPropertiesTableData.m_columns[k].m_width), 
					renderer, null);
			addColumn(column);   
			setBackground(java.awt.Color.white);
		}
		setRowHeight(18);

//		this property will set the value on lost focus of field - earlier user have to clicks on enter key
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setTableResizable();
		getTableHeader().setReorderingAllowed(false);
	}

	public void setTableResizable() {
//		Resize Table
		new VWTableResizer(this);
	}
	public String getToolTipText(MouseEvent e) {
		String tip = null; 	   	   
		java.awt.Point p = e.getPoint();
		int rowIndex = rowAtPoint(p);
		int colIndex = columnAtPoint(p);
		int realColumnIndex = convertColumnIndexToModel(colIndex);
		tip = getValueAt(rowIndex, colIndex).toString();
		return tip;
	}
//	-----------------------------------------------------------
	public void addData(Vector list)
	{
		if (list==null || list.size()==0)
			return ;
		m_data.setData(list);
		m_data.fireTableDataChanged();
	}
//	-----------------------------------------------------------
	public void addDateInfo(Vector dateInfo)
	{
		if (dateInfo==null || dateInfo.size()==0)
			return ;
		m_data.setData(dateInfo);
		m_data.fireTableDataChanged();
	}
//	-----------------------------------------------------------
	public void insertSignRow(Signature sign)
	{
		m_data.insertRow(new PropertiesRowData(sign));
		m_data.fireTableDataChanged();
		setRowHeight(getRowCount()-1,getRowHeight()*2);
		m_data.fireTableRowsUpdated(getRowCount(),getRowCount());
	}
//	-----------------------------------------------------------
	public Vector getData()
	{
		return m_data.getData();
	}
//	-----------------------------------------------------------
	public Index getRowData(int rowNum)
	{
		return m_data.getRowData(rowNum);
	}
//	-----------------------------------------------------------
	public int getRowType(int rowNum)
	{
		Index row=m_data.getRowData(rowNum);
		return row.getType();
	}
//	-----------------------------------------------------------
	public int getRowIndexId(int rowNum)
	{
		Index row=(Index)m_data.getRowData(rowNum);
		return row.getId();
	}
//	-----------------------------------------------------------
	public int getRowDocTypeId(int rowNum)
	{
//		Index row=m_data.getRowData(rowNum);
		return 0;
	}
	public String getDataForIndexId(int indexId) {

		return m_data.getDataForIndexId(indexId);
	}

	public String getIndexName(int indexId) {
		return m_data.getIndexName(indexId);
	}
//	-------------------------------------------------------------
	public void clearData()
	{
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//	---------------------------------------------------------------
	public int getRowCount()
	{
		if (m_data==null) return 0;
		return m_data.getRowCount();
	}
//	-----------------------------------------------------------
	public String getKeyValue()
	{
		if (m_data==null) return "";
		return m_data.getKeyValue();
	}
//	-----------------------------------------------------------
}
class PropertiesRowData
{
	public Object  m_name;
	public Object  m_value;
	public int m_indexId;
	public int m_docTypeId;
	public int m_indexType;
	public int m_DTKey;
	public int m_DTReq;
	public PropertiesRowData() {
		m_name = "";
		m_value="";
		m_docTypeId=0;
		m_docTypeId=0;
		m_indexType=0;
		m_DTKey=0;
		m_DTReq = 0;
	}
//	-------------------------------------------------------------
	public PropertiesRowData(Object name,Object value,int indexId,
			int docTypeId,int indexType,int DTKey) 
	{
		m_indexType=indexId;
		m_indexType=docTypeId;
		m_indexType=indexType;
		m_DTKey=DTKey;
		m_name=new ColorData(name,m_DTKey==1?ColorData.RED:ColorData.BLACK);
		m_value=new ColorData(value,m_DTKey==1?ColorData.RED:ColorData.BLACK);
	}
//	-----------------------------------------------------------
	public PropertiesRowData(Signature sign) 
	{
		m_name = new ColorData(sign.getUserName()+"["+sign.getSignDate()+"]",ColorData.BLUE);
		//m_value=new IconData(VWImages.loadImage(sign.getSignFilePath()),"");
		m_indexId=0;
		m_DTKey=0;
	}
//	-----------------------------------------------------------
	public PropertiesRowData(Index index)
	{
		m_name = index.getName();
		m_value = index.getValue();
		m_indexId=index.getId();
//		/m_docTypeId=index.docTypeId;
		m_indexType=index.getType();
		m_DTKey=(index.isKey()?1:0);

//		Desc   :Added mandatory field indicators (*) for required and key fields in .
//		Author :Nishad Nambiar

		m_DTReq = (index.isRequired()?1:0);
//		m_name=new ColorData(m_name,m_DTKey==1?ColorData.RED:ColorData.BLACK);
		if(m_DTKey == 1){
			m_name = m_name + "*";
			m_name=new ColorData(m_name,ColorData.RED);
		}
		else if (m_DTReq == 1){
			m_name = m_name + "*";
			m_name=new ColorData(m_name,ColorData.BLACK);
		}
		else{
			m_name=new ColorData(m_name,ColorData.BLACK);
		}
		m_value=new ColorData(m_value,m_DTKey==1?ColorData.RED:ColorData.BLACK);
	}
//	-----------------------------------------------------------
	public PropertiesRowData(String dateInfo){
		StringTokenizer st = new StringTokenizer(dateInfo, "|");
		if(st.hasMoreTokens()){
			m_name = st.nextToken();
			m_value = st.nextToken();
		}
	}
}
//-----------------------------------------------------------
class PropertiesColumnData
{
	public String  m_title;
	float m_width;
	int m_alignment;

	public PropertiesColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//-----------------------------------------------------------
class VWPropertiesTableData extends AbstractTableModel
{
	public static final PropertiesColumnData m_columns[] = {
		new PropertiesColumnData( AdminWise.connectorManager.getString("VWPropertiesTable.PropertiesColumnData_0"),0.3F,JLabel.LEFT ),
		new PropertiesColumnData( AdminWise.connectorManager.getString("VWPropertiesTable.PropertiesColumnData_1"),0.569F,JLabel.LEFT)
	};

	public static final int COL_NAME = 0;
	public static final int COL_VALUE = 1;

	protected VWPropertiesTable m_parent;
	protected Vector m_vector;

	public VWPropertiesTableData(VWPropertiesTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}
//	-----------------------------------------------------------
	public void setData(Vector data){
		m_vector.removeAllElements();
		int count =data.size();
		for(int i=0;i<count;i++)
		{
			if(data.get(i) instanceof Index)
				m_vector.addElement(new PropertiesRowData((Index)data.get(i)));
			else
				m_vector.addElement(new PropertiesRowData((String)data.get(i)));
		}
	}
//	-----------------------------------------------------------
	public void insertRow(PropertiesRowData rowdata) {
		m_vector.addElement(rowdata);
	}
//	-----------------------------------------------------------
	public Vector getData() {
		int count=getRowCount();
		Vector data=new Vector();
		for(int i=0;i<count;i++)
		{
			PropertiesRowData row=(PropertiesRowData)m_vector.elementAt(i);
			Index index=new Index(row.m_indexId);
			index.setName((String)row.m_name);
			index.setValue((String)row.m_value);
//			/index.docTypeId=row.m_docTypeId;
			index.setType(row.m_indexType);
			data.add(index);
		}
		return data;
	}
//	-----------------------------------------------------------
	public String getKeyValue() {
		int count=getRowCount();
		for(int i=0;i<count;i++)
		{
			PropertiesRowData row=(PropertiesRowData)m_vector.elementAt(i);
			ColorData colorData=(ColorData)row.m_value;
			if(colorData.getFColor()==ColorData.RED)
				return row.m_value.toString();
		}
		return "";
	}
//	-----------------------------------------------------------
	public Index getRowData(int rowNum) {
//		int count=getRowCount();
		PropertiesRowData row=(PropertiesRowData)m_vector.elementAt(rowNum);
		Index index=new Index(row.m_indexId);
		index.setName(row.m_name.toString());
		index.setValue(row.m_value.toString());
//		/index.docTypeId=row.m_docTypeId;
		index.setType(row.m_indexType);
		return index;
	}
//	-----------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size(); 
	}
//	-----------------------------------------------------------
	public int getColumnCount() { 
		return m_columns.length; 
	} 
//	-----------------------------------------------------------
	public String getColumnName(int column) { 
		return m_columns[column].m_title; 
	}
//	-----------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return false;
	}

	public String getDataForIndexId(int indexId) {
		int count=getRowCount();
		for(int i=0;i<count;i++)
		{
			PropertiesRowData row=(PropertiesRowData)m_vector.elementAt(i);          
			if (row.m_indexId == indexId)
				return row.m_value.toString();
		}
		return "";
	}

	public String getIndexName(int indexId) {
		int count=getRowCount();
		for(int i=0;i<count;i++)
		{
			PropertiesRowData row=(PropertiesRowData)m_vector.elementAt(i);
			if (row.m_indexId == indexId)
				return row.m_name.toString();
		}
		return "";
	}

//	-----------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		PropertiesRowData row = (PropertiesRowData)m_vector.elementAt(nRow);
		switch (nCol) {
		case COL_NAME: return row.m_name;
		case COL_VALUE: return row.m_value;
		}
		return "";
	}
//	-----------------------------------------------------------
	public void clear(){
		m_vector.removeAllElements();
	}
//	-----------------------------------------------------------
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		PropertiesRowData row = (PropertiesRowData)m_vector.elementAt(nRow);
		String svalue = (value == null ? "" : value.toString());

		switch (nCol) {
		case COL_NAME:
			row.m_name = svalue; 
			break;
		case COL_VALUE:
			row.m_value = svalue;
			break;
		}
//		-----------------------------------------------------------
	}
}