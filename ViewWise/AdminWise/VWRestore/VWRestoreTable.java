/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWRestoreTable<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWRestore;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumnModel;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;

import java.util.Vector;
import java.util.Comparator;
import java.util.Collections;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.VWUtil.VWRecord;
//------------------------------------------------------------------------------
public class VWRestoreTable extends JTable implements VWConstant
{
    protected VWRestoreTableData m_data;
    TableColumn[] ColModel=new TableColumn[4];
    static boolean[] visibleCol={true,true,true,false};
    public VWRestoreTable(){
        super();
        m_data = new VWRestoreTableData(this);
        setAutoCreateColumnsFromModel(false);
        getTableHeader().setReorderingAllowed(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JTextField readOnlyText=new JTextField();
        readOnlyText.setEditable(false);
        Dimension tableWith = getPreferredScrollableViewportSize();

        for (int k = 0; k < VWRestoreTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        ///DefaultTableCellRenderer textRenderer =new DefaultTableCellRenderer();
        DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWRestoreTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWRestoreRowEditor editor=new VWRestoreRowEditor(this);
        TableColumn column = new TableColumn
            (k,Math.round(tableWith.width*VWRestoreTableData.m_columns[k].m_width), 
                renderer, editor);
        addColumn(column);   
        ColModel[k]=column;
        }
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        setVisibleCols();
        setBackground(java.awt.Color.white);
        ///setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ListSelectionModel selectionModel= getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
            if (listSelectionModel.isSelectionEmpty())        
                AdminWise.adminPanel.restorePanel.setEnableMode(MODE_UNSELECTED); 
            else
                AdminWise.adminPanel.restorePanel.setEnableMode(MODE_SELECT);
        }
    });
        setRowHeight(TableRowHeight);
        
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);      
        getColumnModel().getColumn(0).setPreferredWidth(322);
        getColumnModel().getColumn(1).setPreferredWidth(322);
    	getColumnModel().getColumn(2).setPreferredWidth(322);
        setTableResizable();
  }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
  public void addData(List list)
  {
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public int getRowCount()
  {
      if (m_data==null) return 0;
      return m_data.getRowCount();
  }
//------------------------------------------------------------------------------
  public void insertData(String[][] data)
  {
     if(data==null || data.length==0)
        return;
    for(int i=0;i<data.length;i++)
        m_data.insert(new RestoreRowData(data[i]));
     
     m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteRow(int row)
  {
        m_data.delete(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteSelectedRows()
  {
        int[] rows=getSelectedRows();
        int count=rows.length;
        for(int i=0;i<count;i++)    m_data.delete(rows[i]-i);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(List list)
  {
    if(list==null || list.size()==0)
        return;  
    for(int i=0;i<list.size();i++)
        m_data.insert(new RestoreRowData((String)list.get(i)));
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(String str)
  {
    if(str==null || str.equals(""))  return;  
    m_data.insert(new RestoreRowData(str));
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public String[] getRestoreRowData(int rowNum)
  {
        return m_data.getRestoreRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public String getRowDocPath(int rowNum)
  {
        String[] row=m_data.getRestoreRowData(rowNum);
        return row[1];
  }
//------------------------------------------------------------------------------
    private void setVisibleCols()
    {
    TableColumnModel model=getColumnModel();
    
    int count =visibleCol.length;
    int i=0;
    while(i<count)
    {
        if (!visibleCol[i])
        {
            TableColumn column = model.getColumn(i);
            model.removeColumn(column);
            count--;
        }
        else
        {
            i++;
        }
    }
        if(count==0)return;
        Dimension tableWith = getPreferredScrollableViewportSize();
        int colWidth=(int)Math.round(tableWith.getWidth()/count);
        for (i=0;i<count;i++)
        {
            model.getColumn(i).setPreferredWidth(colWidth);
        }
        setColumnModel(model);
}
//------------------------------------------------------------------------------
  class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
		Object object = event.getSource();
		if (object instanceof JTable)
			if(event.getModifiers()==event.BUTTON3_MASK)
		        VWRestoreTable_RightMouseClicked(event);
			else if(event.getModifiers()==event.BUTTON1_MASK)
				VWRestoreTable_LeftMouseClicked(event);
	}
	}
//------------------------------------------------------------------------------
void VWRestoreTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWRestoreTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
    /*
    VWMenu menu=null;
    int roomId=getRowRoomId(row);
    int docId=getRowDocId(row);
    if(VWTableConnector.checkIsDocOpen(roomId,docId))
        if(!AdminWise.gConnector.isDocumentShare(roomId,docId))
        {
            menu=new VWMenu(VWConstant.OpenDocument_TYPE);
            menu.show(this,event.getX(),event.getY()); 
        }
        else
        {
            menu=new VWMenu(VWConstant.OpenShareDocument_TYPE);
            menu.show(this,event.getX(),event.getY()-70); 
        }
     */
}
//------------------------------------------------------------------------------
private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
}
//------------------------------------------------------------------------------
private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
    
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
    //LoadRowData(row);
	AdminWise.adminPanel.restorePanel.showProperties(getRowDocPath(getSelectedRow()));
 }
//------------------------------------------------------------------------------
 private void LoadPropData(int row)
{
}
//------------------------------------------------------------------------------
private void LoadRowData(int row)
{
    /*
    int docId=getRowDocId(row);
    int roomId=getRowRoomId(row);
    String docName=(String)getValueAt(row,1);
    VWTreeConnector.setTreeNodeSelection(docId,roomId,true);
    VWTableConnector.getDocFile(roomId,docId,docName,VWTreeConnector.getFixTreePath());
     **/
}
//------------------------------------------------------------------------------
}
class RestoreRowData
{
  public String  m_DocName;
  public String  m_Path;
  public String  m_Pages;
  public String  m_Status;
  public RestoreRowData() {
    m_DocName="";
    m_Path="";
    m_Pages="";
    m_Status="";
  }
//------------------------------------------------------------------------------
  public RestoreRowData(String docName,String path,String pages) 
  {
    m_DocName=docName.equals(".")?"":docName;
    m_Path=path.equals(".")?"":path;
    m_Pages=pages.equals(".")?"":pages;
    m_Status="";
  }
//------------------------------------------------------------------------------
  public RestoreRowData(String string)
  {
    VWStringTokenizer tokens=new VWStringTokenizer(string,VWConstant.SepChar,false);
    String str=tokens.nextToken();
    m_DocName=str.equals(".")?"":str;
    str=tokens.nextToken();
    m_Path=str.equals(".")?"":str;
    str=tokens.nextToken();
    m_Pages=str.equals(".")?"":str;
    m_Status="";
  }
//------------------------------------------------------------------------------
  public RestoreRowData(String[] data) 
  {
    int i=0;    
    m_DocName=data[i].equals(".")?"":data[i];
    i++;
    m_Path=data[i].equals(".")?"":data[i];
    i++;
    m_Pages=data[i].equals(".")?"":data[i];
    m_Status="";
  }
}
//------------------------------------------------------------------------------
class RestoreColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public RestoreColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWRestoreTableData extends AbstractTableModel
{
  public static final RestoreColumnData m_columns[] = {
    new RestoreColumnData(VWConstant.RestoreColumnNames[0],0.3F,JLabel.LEFT ),
    new RestoreColumnData(VWConstant.RestoreColumnNames[1],0.5F, JLabel.LEFT),
    new RestoreColumnData(VWConstant.RestoreColumnNames[2],0.1F, JLabel.LEFT),
    new RestoreColumnData(VWConstant.RestoreColumnNames[3],0.1F, JLabel.LEFT)
  };
  public static final int COL_DOCNAME=0;
  public static final int COL_PATH=1;
  public static final int COL_PAGES=2;
  public static final int COL_STATUS=3;

  protected VWRestoreTable m_parent;
  protected Vector m_vector;
  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;

  public VWRestoreTableData(VWRestoreTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if(data==null || data.size()==0)
        return;
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(new RestoreRowData((String)data.get(i)));
    }
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    if(data==null || data.length==0)
        return;
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(new RestoreRowData(data[i]));
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][3];
    for(int i=0;i<count;i++)
    {
        int j=0;
        RestoreRowData row=(RestoreRowData)m_vector.elementAt(i);
        data[i][j++]=row.m_DocName;
        data[i][j++]=row.m_Path;
        data[i][j]=row.m_Pages;
    }
    return data;
  }
//------------------------------------------------------------------------------
public String[] getRestoreRowData(int rowNum) {
    int count=getRowCount();
    String[] RestoreRowData=new String[3];
    int j=0;
    RestoreRowData row=(RestoreRowData)m_vector.elementAt(rowNum);
    RestoreRowData[j++]=row.m_DocName;
    RestoreRowData[j++]=row.m_Path;    
    RestoreRowData[j++]=row.m_Pages;
    return RestoreRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    ///return m_columns[column].m_title; 
      String str = m_columns[column].m_title;
    if (column==m_sortCol)
      str += m_sortAsc ? " �" : " �";
    return str;
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public java.lang.Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    RestoreRowData row = (RestoreRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_DOCNAME: return row.m_DocName;
      case COL_PATH: return row.m_Path;
      case COL_PAGES: return row.m_Pages;
      case COL_STATUS: return row.m_Status;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    RestoreRowData row = (RestoreRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
    case COL_DOCNAME: 
        row.m_DocName=svalue;
        break;
    case COL_PATH:
        row.m_Path=svalue;
        break;
    case COL_PAGES:
        row.m_Pages=svalue;  
        break;
    case COL_STATUS:
        row.m_Status=svalue;  
        break;    
    }
  }
//------------------------------------------------------------------------------
  public void insert(int row) {
    if (row < 0)
      row = 0;
    if (row > m_vector.size())
      row = m_vector.size();
    m_vector.insertElementAt(new RestoreRowData(), row);
  }
//------------------------------------------------------------------------------
    public void insert(int row,RestoreRowData RestoreRowData){
    if (row < 0)
      row = 0;
    if (row > m_vector.size())
      row = m_vector.size();
    m_vector.insertElementAt(RestoreRowData, row);
  }
//------------------------------------------------------------------------------
  public void insert(RestoreRowData rowData) {
 
    m_vector.addElement(rowData);
  }
//----------------------------------------------------------------------------
  public boolean delete(int row) {
    if (row < 0 || row >= m_vector.size())
      return false;
    m_vector.remove(row);
      return true;
  }
//----------------------------------------------------------------------------
  public void updateStatus(int row,int status) {
      /*
      if((status > VWRestoreConstant.RestoreErr.length-1) || (status<0))
        ((RestoreRowData)m_vector.get(row)).m_Status="Unknown error";
      else
        ((RestoreRowData)m_vector.get(row)).m_Status=VWRestoreConstant.RestoreErr[status];
       **/
  }
//----------------------------------------------------------------------------
public void clear(){
    m_vector.removeAllElements();
  }
//------------------------------------------------------------------------------
   class ColumnListener extends MouseAdapter
  {
    protected VWRestoreTable m_table;
//------------------------------------------------------------------------------
    public ColumnListener(VWRestoreTable table){
      m_table = table;
    }
//------------------------------------------------------------------------------
    public void mouseClicked(MouseEvent e){
      
        if(e.getModifiers()==e.BUTTON3_MASK)
            selectViewCol(e);
	else if(e.getModifiers()==e.BUTTON1_MASK)
            sortCol(e);
    }
//------------------------------------------------------------------------------
    private void sortCol(MouseEvent e)
    {
        TableColumnModel colModel = m_table.getColumnModel();
        int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
        int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

        if (modelIndex < 0)
        return;
        if (m_sortCol==modelIndex)
        m_sortAsc = !m_sortAsc;
        else
        m_sortCol = modelIndex;

        for (int i=0; i < colModel.getColumnCount();i++) {
        TableColumn column = colModel.getColumn(i);
        column.setHeaderValue(getColumnName(column.getModelIndex()));    
        }
        m_table.getTableHeader().repaint();  
        Collections.sort(m_vector,new RestoreComparator(modelIndex,m_sortAsc));
        m_table.tableChanged(new TableModelEvent(VWRestoreTableData.this)); 
        m_table.repaint();  
    }
//------------------------------------------------------------------------------
    private void selectViewCol(MouseEvent e)
    {
        javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
        for (int i=0; i < m_table.RestoreColumnNames.length; i++){
         javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
                m_table.RestoreColumnNames[i],m_table.visibleCol[i]);
        TableColumn column = m_table.ColModel[i];
        subMenu.addActionListener(new ColumnKeeper(column,VWRestoreTableData.m_columns[i]));
        menu.add(subMenu);
        }
        menu.show(m_table,e.getX(),e.getY());
    }
//------------------------------------------------------------------------------
  class ColumnKeeper implements java.awt.event.ActionListener
  {
    protected TableColumn m_column;
    protected RestoreColumnData  m_colData;

    public ColumnKeeper(TableColumn column,RestoreColumnData colData){
      m_column = column;
      m_colData = colData;
    }
//------------------------------------------------------------------------------
    public void actionPerformed(java.awt.event.ActionEvent e) {
      javax.swing.JCheckBoxMenuItem item=(javax.swing.JCheckBoxMenuItem)e.getSource();
      TableColumnModel model = m_table.getColumnModel();
      boolean found=false;
      int i=0;
      int count=m_table.RestoreColumnNames.length;
      int colCount=model.getColumnCount();
      while (i<count && !found){
        if(m_table.RestoreColumnNames[i].equals(e.getActionCommand()))
            found=true;
        i++;
        }
      i--;
      if (item.isSelected()) {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.addColumn(m_column);
      }
      else {
        if(colCount>1)
        {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.removeColumn(m_column);
        }
      }
      m_table.tableChanged(new javax.swing.event.TableModelEvent(m_table.m_data)); 
      m_table.repaint();
    }
  }
  }
//------------------------------------------------------------------------------
class RestoreComparator implements Comparator
{
    protected int     m_sortCol;
    protected boolean m_sortAsc;
//------------------------------------------------------------------------------
    public RestoreComparator(int sortCol, boolean sortAsc) {
    m_sortCol = sortCol;
    m_sortAsc = sortAsc;
    }
//------------------------------------------------------------------------------
    public int compare(Object o1, Object o2) {
    if(!(o1 instanceof RestoreRowData) || !(o2 instanceof RestoreRowData))
      return 0;
    RestoreRowData s1 = (RestoreRowData)o1;
    RestoreRowData s2 = (RestoreRowData)o2;
    int result = 0;
    double d1, d2;
    switch (m_sortCol) {
      case 0:
        result = s1.m_DocName.toLowerCase().compareTo(s2.m_DocName.toLowerCase());
        break;
      case 1:
        result = s1.m_Path.toLowerCase().compareTo(s2.m_Path.toLowerCase());
        break;
      case 2:
        result = s1.m_Pages.toLowerCase().compareTo(s2.m_Pages.toLowerCase());
        break;
      case 3:
        result = s1.m_Status.toLowerCase().compareTo(s2.m_Status.toLowerCase());
        break;
    }
    if (!m_sortAsc)
      result = -result;
    return result;
    }
//------------------------------------------------------------------------------
    public boolean equals(Object obj) {
    if (obj instanceof RestoreComparator) {
      RestoreComparator compObj = (RestoreComparator)obj;
      return (compObj.m_sortCol==m_sortCol) && 
        (compObj.m_sortAsc==m_sortAsc);
    }
    return false;
    }    
}
//------------------------------------------------------------------------------
}