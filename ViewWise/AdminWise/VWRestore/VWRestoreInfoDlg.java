package ViewWise.AdminWise.VWRestore;

import javax.swing.JPanel;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import java.util.prefs.*;
import java.awt.Component;
import ViewWise.AdminWise.VWMessage.VWMessage;
//------------------------------------------------------------------------------
public class VWRestoreInfoDlg extends javax.swing.JDialog implements  VWConstant
{
    public VWRestoreInfoDlg(Frame parent,int fileCount,int parentId)
    {
        super(parent,LBL_RESTOREDLGINFO_TITLE,false);
        ///parent.setIconImage(VWImages.RestoreIcon.getImage());
        //getContentPane().setLayout(null);
        getContentPane().setBackground(AdminWise.getAWColor());
        setSize(335,220);
        infoPanel.setLayout(null);
        getContentPane().add(infoPanel);
        BtnCancel.setText(BTN_CANCEL_NAME);
//        BtnCancel.setBackground(java.awt.Color.white);
        infoPanel.add(BtnCancel);
        BtnCancel.setBounds(120,144,91,23);
        BtnCancel.setIcon(VWImages.CloseIcon);
        infoPanel.add(PrgrsMain);
        PrgrsMain.setBounds(12,108,303,19);
        infoPanel.add(PrgrsDoc);
        PrgrsDoc.setBounds(12,48,303,19);
        infoPanel.add(LblMain);
        LblMain.setBounds(12,84,303,15);
        infoPanel.add(LblDoc);
        LblDoc.setBounds(12,24,303,15);
        PrgrsMain.setMaximum(fileCount);
        PrgrsMain.setMinimum(0);
        PrgrsDoc.setMaximum(100);
        PrgrsDoc.setMinimum(0);
        LblMain.setText(AdminWise.connectorManager.getString("VWRestoreInfoDlg.LblMain")+fileCount+"]");
        LblDoc.setText(LBL_READDOCINFO);
        SymAction lSymAction = new SymAction();
        BtnCancel.addActionListener(lSymAction);
        getDlgOptions();
        setResizable(false);            
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        setVisible(true);
        new RestoreThread(parentId);
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(java.awt.event.WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWRestoreInfoDlg.this)
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
	//{{DECLARE_CONTROLS
        javax.swing.JPanel infoPanel = new javax.swing.JPanel();
	VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
	javax.swing.JProgressBar PrgrsMain = new javax.swing.JProgressBar();
	javax.swing.JProgressBar PrgrsDoc = new javax.swing.JProgressBar();
	javax.swing.JLabel LblMain = new javax.swing.JLabel();
	javax.swing.JLabel LblDoc = new javax.swing.JLabel();
	//}}
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);
        }
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelAction=true;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
    }
//------------------------------------------------------------------------------
    public void setMainText(String text)
    {
        LblMain.setText(text);
    }
//------------------------------------------------------------------------------
    public void setDocText(String text)
    {
        LblDoc.setText(text);
    }
//------------------------------------------------------------------------------
 private class RestoreThread extends Thread/*SafeThread*/
    {
        RestoreThread(int parentId)
        {
            parentNodeId=parentId;
            setDaemon(true);
            ///setPriority(java.lang.Thread.MAX_PRIORITY);
            start();
        }
        public void run()
        {
            try{
                AdminWise.adminPanel.setWaitPointer();
                int rowNum=0;
                String restoreDocPath="";
                int rowCount=AdminWise.adminPanel.restorePanel.Table.getRowCount();
                for(int i=0;i<rowCount;i++)
                {
                    setMainText(AdminWise.connectorManager.getString("VWRestoreInfoDlg.MainText_0")+(i+1)+AdminWise.connectorManager.getString("VWRestoreInfoDlg.MainText_1")+rowCount+"]");
                    if(cancelAction) 
                        break;
                    int retVal=0;
                    try{
                        restoreDocPath=AdminWise.adminPanel.restorePanel.Table.getRowDocPath(rowNum);
                        retVal=VWRestoreConnector.restoreDocument(restoreDocPath,parentNodeId);
                    }
                    catch(Exception e){
                         VWRestoreConnector.writeErrorToLog(restoreDocPath,e.getMessage());
                        retVal=1;}
                    if(retVal==0)
                        AdminWise.adminPanel.restorePanel.Table.deleteRow(rowNum);
                    else
                        rowNum++;
                    PrgrsMain.setValue(i+1);
                }
            AdminWise.adminPanel.restorePanel.setEnableMode(MODE_UNSELECTED);
            }
            catch(Exception e){}
            finally{
                AdminWise.adminPanel.setDefaultPointer();            
                ///stop();
                saveDlgOptions();
                setVisible(false);
            }
            String logPath=VWRestoreConnector.writeLogFile();
            if(logPath==null)
            {
                VWMessage.showInfoDialog(VWMessage.MSG_RESTORECOMPLETED);
            }
            else
            {
                try
                {
                    if(VWMessage.showConfirmDialog(null,AdminWise.connectorManager.getString("VWRestoreInfoDlg.msg_1")+" "+logPath+" "+
                    		AdminWise.connectorManager.getString("VWRestoreInfoDlg.msg_2"))==javax.swing.JOptionPane.YES_OPTION)
                        java.lang.Runtime.getRuntime().exec("Notepad.exe "+logPath);
                }
                catch(java.io.IOException e){}
            }
        }
        int parentNodeId=0;
 }
//------------------------------------------------------------------------------
    public boolean cancelAction=false;
}