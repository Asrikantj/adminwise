/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWStatisticConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWSecureLink;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;

//------------------------------------------------------------------------------
public class VWSecureLinkConnector implements  VWConstant
{
    public static Vector getSecureLinkData()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            return getSecureLinkData(room);
        return null;
    }
//------------------------------------------------------------------------------
    public static Vector getSecureLinkData(VWRoom room)
    {
        Vector info=new Vector();
        int ret=AdminWise.gConnector.ViewWiseClient.getSecureLinkDataAdminwise(room.getId(),info);
        AdminWise.printToConsole("info after fetching data getSecureLinkData :::: "+info);
        AdminWise.printToConsole("info size after fetching data getSecureLinkData :::: "+info.size());
        String[][] secureLink = new String [info.size()] [];
        if(info==null || info.size()==0) return info;
        for(int i=0;i<info.size();i++){
        	StringTokenizer st = new StringTokenizer(info.get(i).toString(), "\t");
        	int secureId = 0;
        	String documentName = "";
        	String emailId = "";
        	String deliverOn = "";
        	String expiresOn = "";
        	String password = "";
        	String modifyPerm = "";
        	String docLocation = "";
        	int documentId = 0;
        	if(st.hasMoreTokens()){
        		secureId = Integer.parseInt(st.nextToken());
        		documentName = st.nextToken();
        		emailId = st.nextToken();
        		password = st.nextToken();
        		deliverOn = st.nextToken();
        		expiresOn = st.nextToken();
        		modifyPerm = st.nextToken();
        		if(modifyPerm == "0"){
        			modifyPerm = "No";
        		}else if(modifyPerm == "1"){
        			modifyPerm = "Yes";
        		}
        		docLocation = st.nextToken();
        		documentId = Integer.parseInt(st.nextToken());
        	}
        	AdminWise.printToConsole("1 "+secureId);
        	AdminWise.printToConsole("2 "+documentName);
        	AdminWise.printToConsole("3 "+emailId);
        	AdminWise.printToConsole("4 "+deliverOn);
        	AdminWise.printToConsole("5 "+expiresOn);
        	AdminWise.printToConsole("6 "+modifyPerm);
        	AdminWise.printToConsole("7 "+docLocation);
        	AdminWise.printToConsole("8 "+documentId);
//        	secureLink[i][0] = String.valueOf(secureId);
//        	AdminWise.printToConsole(secureLink[i][0]);
//        	secureLink[i][1] = documentName;
//        	AdminWise.printToConsole(secureLink[i][1]);
//        	secureLink[i][2] = emailId;
//        	AdminWise.printToConsole(secureLink[i][2]);
//        	secureLink[i][3] = deliverOn;
//        	AdminWise.printToConsole(secureLink[i][3]);
//        	secureLink[i][4] = expiresOn;      
//        	AdminWise.printToConsole(secureLink[i][4]);
        	
        }
        return info;
    }
    
    public static int expireSelectedLink(int secureLinkId){
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        int ret=AdminWise.gConnector.ViewWiseClient.expireSelectedLink(room.getId(),secureLinkId);
        AdminWise.printToConsole("ret after calling Client expireSelectedLink method :::: "+ret);
    	return ret;
    }
    
    
public static void main(String[] args) {
//	VWSecureLinkConnector.getOptionSettings();
}
//------------------------------------------------------------------------------
}
