/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWSecureLink;

import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.computhink.common.util.VWRefCreator;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

import java.awt.Dimension;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWRoom.VWRoom;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//------------------------------------------------------------------------------
public class VWSecureLinkTable extends JTable implements VWConstant
{
    protected VWSecureLinkTableData m_data;    
    public VWSecureLinkTable(){
        super();
        m_data = new VWSecureLinkTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWSecureLinkTableData.m_columns.length; k++) {
        
        VWTableCellRenderer renderer =new VWTableCellRenderer();
        renderer.setHorizontalAlignment(VWSecureLinkTableData.m_columns[k].m_alignment);
        VWSecureLinkRowEditor editor=new VWSecureLinkRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWSecureLinkTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
	 setRowHeight(TableRowHeight);
	 
	 setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
    	Object object = event.getSource();
    	if (object instanceof JTable)
    		if(event.getModifiers()==event.BUTTON3_MASK)
    			VWSecureLinkTable_RightMouseClicked(event);
    		else if(event.getModifiers()==event.BUTTON1_MASK)
    			VWSecureLinkTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWSecureLinkTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWSecureLinkTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    	loadDocumentInDTC(row);
    }
    private void loadDocumentInDTC(int row)
    {
    	int docId = this.getRowDocLockId(row);
    	AdminWise.printToConsole("docId loadDocumentInDTC :::: "+docId);
        VWRoom vwroom = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        String room = vwroom.getName();
    	try {      
            //create a temp folder to place reference file in it
        	File folder = new File("c:\\VWReferenceFile\\");
            folder.mkdirs();
            //create a reference file to open document
            File vwrFile = VWRefCreator.createVWRef(folder.getPath(), room ,docId);
            
            //open reference file in ViewWise thus opening doc                        
            try{
                Preferences clientPref =  Preferences.userRoot().node(GENERAL_PREF_ROOT);
                //String dtcPath = clientPref.get("AppPath","C:\\Program Files\\ViewWise Client\\");
                String dtcPath = VWUtil.getHome();
                Runtime.getRuntime().exec(dtcPath+"\\System\\ViewWise.exe " + vwrFile.getPath());
            }catch(Exception ex){
            	ex.printStackTrace();
            	//JOptionPane.showMessageDialog(null,"DTC Installed Path not found."+ex.toString());
            }
        }
        catch (IOException ioe) {
        	ioe.printStackTrace();
        }
    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
      m_data.setData(list);
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(Vector data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public String[][] getReportData()
  {
      String[][] repData=m_data.getData();
      return repData;
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new SecureLinkRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
  
  public int getRowDocLockId(int rowNum) {
      String[] row=m_data.getRowData(rowNum);
      return VWUtil.to_Number((row[7]));
  }
//-------------------------------------------------------------------------
  public int getRowNodeId(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[0]);
  }
//------------------------------------------------------------------------------
  public int getRowStatisticId(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[0]);
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class SecureLinkRowData
{
	public int 		m_docId;
    public String   m_Name;
    public String   m_email;
    public String   m_deliver_on;
    public String   m_expire_on;
    public String   m_pwd;
    public String	m_modify_perm;
    public String 	m_doc_location;
    public int 		m_document_id;
    
  public SecureLinkRowData() {
    m_Name="";
    m_email="";
    m_deliver_on="";
    m_expire_on="";
    m_docId = 0;
    m_pwd = "";
    m_modify_perm = "";    
    m_doc_location = "";
    m_document_id = 0;
  }
  //----------------------------------------------------------------------------
  public SecureLinkRowData(int rDocId,String rName,String rEmail,String rDeliveryOn,String rExpireOn, String rModifyPerm, String rDocLocation, int rDocumentId) 
  {
	  	m_docId=rDocId;
	    m_Name=rName;
	    m_email=rEmail;
	    m_deliver_on=rDeliveryOn;
	    m_expire_on=rExpireOn;
	    m_modify_perm = rModifyPerm;
	    m_doc_location = rDocLocation;
	    m_document_id = rDocumentId;
  }
  //----------------------------------------------------------------------------
  public SecureLinkRowData(String str)
  {
        VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
        AdminWise.printToConsole("str before tokenizing SecureLinkRowData :::: "+str);
        if(tokens.hasMoreTokens()){
        	m_docId=Integer.parseInt(tokens.nextToken());
        	m_Name = tokens.nextToken();
    		m_email = tokens.nextToken();
    		tokens.nextToken();
    		m_deliver_on = tokens.nextToken();
    		m_expire_on = tokens.nextToken();
    		m_modify_perm = tokens.nextToken();
    		m_doc_location = tokens.nextToken();
    		if(m_modify_perm.equals("0")){
    			m_modify_perm = "No";
    		} else if(m_modify_perm.equals("1")){
    			m_modify_perm = "Yes";
    		}
    		m_document_id = Integer.parseInt(tokens.nextToken());
    		
    		AdminWise.printToConsole("m_docId ::: "+m_docId);
        	AdminWise.printToConsole("m_Name :::: "+m_Name);
        	AdminWise.printToConsole("m_email :::: "+m_email);
        	AdminWise.printToConsole("m_deliver_on :::: "+m_deliver_on);
        	AdminWise.printToConsole("m_expire_on :::: "+m_expire_on);
        	AdminWise.printToConsole("m_doc_location :::: "+m_doc_location);
        	AdminWise.printToConsole("m_modify_perm :::: "+m_modify_perm);
        	AdminWise.printToConsole("m_document_id :::: "+m_document_id);
        }
        
//        m_Value=tokens.nextToken();
  }
  //----------------------------------------------------------------------------
  public SecureLinkRowData(String[] data) 
  {
	  	int i=0;
	    m_docId= Integer.parseInt(data[i++]);
	    m_Name=data[i++];
	    m_email=data[i++];
	    m_deliver_on=data[i++];
	    m_expire_on=data[i++];
	    m_modify_perm=data[i++];
	    m_doc_location=data[i++];
	    m_document_id=Integer.parseInt(data[i]);
  }
}
//------------------------------------------------------------------------------
class SecureLinkColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public SecureLinkColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWSecureLinkTableData extends AbstractTableModel 
{
  public static final SecureLinkColumnData m_columns[] = {
//    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[],0.0F, JLabel.LEFT),
    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[1],0.5F, JLabel.LEFT),
    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[2],0.5F,JLabel.LEFT),
    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[3],0.5F,JLabel.LEFT),
    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[4],0.5F,JLabel.LEFT),
    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[5],0.5F,JLabel.LEFT),
    new SecureLinkColumnData(VWConstant.SecureLinkColumnNames[6],0.5F,JLabel.LEFT),
  };
  public static final int COL_DOCNAME = 0;
  public static final int COL_EMAIL = 1;
  public static final int COL_DELIVERY_ON = 2;
  public static final int COL_EXPIRE_ON = 3;
  public static final int COL_MODIFY_PERM = 4;
  public static final int COL_DOC_LOCATION = 5;
  
  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;

  protected VWSecureLinkTable m_parent;
  protected Vector m_vector;

  public VWSecureLinkTableData(VWSecureLinkTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    AdminWise.printToConsole("count inside setData :::: "+count);
    for(int i=0;i<count;i++)
    {
    	m_vector.addElement(new SecureLinkRowData((String)data.get(i)));
    }
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
    	SecureLinkRowData row =new SecureLinkRowData(data[i]); 
    	AdminWise.printToConsole("row before adding to table :::: "+row);
        m_vector.addElement(row);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
	int count=getRowCount();
    String[][] data=new String[count][10];
    for(int i=0;i<count;i++)
    {
        SecureLinkRowData row=(SecureLinkRowData)m_vector.elementAt(i);
        data[i][0]= String.valueOf(row.m_docId);
        data[i][1]=row.m_Name;
        data[i][2]=row.m_email;
        data[i][3]=row.m_deliver_on;
        data[i][4]=row.m_expire_on;
        data[i][5]=row.m_modify_perm;
        data[i][6]=row.m_doc_location;
        data[i][7]=String.valueOf(row.m_document_id);
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
	  String[] secureLinkRowData=new String[10];
	    SecureLinkRowData row=(SecureLinkRowData)m_vector.elementAt(rowNum);
	    secureLinkRowData[0]= String.valueOf(row.m_docId);
	    secureLinkRowData[1]=row.m_Name;
	    secureLinkRowData[2]=row.m_email;
	    secureLinkRowData[3]=row.m_deliver_on;
	    secureLinkRowData[4]=row.m_expire_on;
	    secureLinkRowData[5]=row.m_modify_perm;
	    secureLinkRowData[6]=row.m_doc_location;
	    secureLinkRowData[7]=String.valueOf(row.m_document_id);
	    return secureLinkRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    String str = m_columns[column].m_title;
    AdminWise.printToConsole("m_sortCol >>>>>"+m_sortCol+"column >>>>"+column);
    if (column==m_sortCol)
        str += m_sortAsc ? " �" : " �";
    return str;
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    SecureLinkRowData row = (SecureLinkRowData)m_vector.elementAt(nRow);
//    AdminWise.printToConsole("nCol inside getValueAt :::: "+nCol);
    switch (nCol) {
    case COL_DOCNAME:     return row.m_Name;
    case COL_EMAIL:     return row.m_email;
    case COL_DELIVERY_ON:    return row.m_deliver_on;
    case COL_EXPIRE_ON:      return row.m_expire_on;
    case COL_MODIFY_PERM:      return row.m_modify_perm;
    case COL_DOC_LOCATION:      return row.m_doc_location;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
	  if (nRow < 0 || nRow >= getRowCount())
		  return;
	  SecureLinkRowData row = (SecureLinkRowData)m_vector.elementAt(nRow);
	  String svalue = value.toString();
//	  AdminWise.printToConsole("nCol inside setValueAt :::: "+nCol);
	  switch (nCol) {
	  case COL_DOCNAME:
		  row.m_Name = svalue; 
		  break;
	  case COL_EMAIL:
		  row.m_email = svalue;
		  break;
	  case COL_DELIVERY_ON:
		  row.m_deliver_on = svalue; 
		  break;
	  case COL_EXPIRE_ON:
		  row.m_expire_on = svalue;
		  break;
	  case COL_MODIFY_PERM:
		  row.m_modify_perm = svalue;
		  break;
	  case COL_DOC_LOCATION:
		  row.m_doc_location = svalue;
		  break;
	  }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(SecureLinkRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//--------------------------------------------------------------------------
 class SecureLinkComparator implements Comparator {
     protected int     m_sortCol;
     protected boolean m_sortAsc;
 //--------------------------------------------------------------------------
     public SecureLinkComparator(int sortCol, boolean sortAsc) {
         m_sortCol = sortCol;
         m_sortAsc = sortAsc;
     }
 //--------------------------------------------------------------------------
     public int compare(Object o1, Object o2) {
         if(!(o1 instanceof SecureLinkRowData) || !(o2 instanceof SecureLinkRowData))
             return 0;
         SecureLinkRowData s1 = (SecureLinkRowData)o1;
         SecureLinkRowData s2 = (SecureLinkRowData)o2;
         int result = 0;
         double d1, d2;
         switch (m_sortCol) {           
         	case COL_DOCNAME:
                 result = s1.m_Name.toLowerCase().compareTo(s2.m_Name.toLowerCase());
                 break;
             case COL_EMAIL:
                result = s1.m_email.toLowerCase().compareTo(s2.m_email.toLowerCase());
                break;            	
             case COL_DELIVERY_ON:
            	result = s1.m_deliver_on.toLowerCase().compareTo(s2.m_deliver_on.toLowerCase());
                break;
             case COL_EXPIRE_ON:
            	result = s1.m_expire_on.toLowerCase().compareTo(s2.m_expire_on.toLowerCase());  
                break;
             case COL_MODIFY_PERM:
            	result = s1.m_modify_perm.toLowerCase().compareTo(s2.m_modify_perm.toLowerCase());  
                break;
             case COL_DOC_LOCATION:
             	result = s1.m_doc_location.toLowerCase().compareTo(s2.m_doc_location.toLowerCase());
             	break;
         }
         if (!m_sortAsc)
             result = -result;
         return result;
     }
 //--------------------------------------------------------------------------
     public boolean equals(Object obj) {
         if (obj instanceof SecureLinkComparator) {
        	 SecureLinkComparator compObj = (SecureLinkComparator)obj;
             return (compObj.m_sortCol==m_sortCol) &&
             (compObj.m_sortAsc==m_sortAsc);
         }
         return false;
     }
 } 
 class ColumnListener extends MouseAdapter {
     protected VWSecureLinkTable m_table;
 //--------------------------------------------------------------------------
     public ColumnListener(VWSecureLinkTable table){
         m_table = table;
     }
 //--------------------------------------------------------------------------
     public void mouseClicked(MouseEvent e){   
    	 AdminWise.printToConsole("mouseClicked e.getModifiers():"+e.getModifiers()+"e.BUTTON1_MASK >>>>"+e.BUTTON1_MASK);
    	 if(e.getModifiers()==e.BUTTON1_MASK)
             sortCol(e);
     }
 //--------------------------------------------------------------------------
     private void sortCol(MouseEvent e) { 
    	 AdminWise.printToConsole("inside sortCol");
         TableColumnModel colModel = m_table.getColumnModel();
         int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
         int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
         
         if (modelIndex < 0) return;
         if (m_sortCol==modelIndex)
             m_sortAsc = !m_sortAsc;
         else
             m_sortCol = modelIndex;
         for (int i=0; i < colModel.getColumnCount();i++) {
             TableColumn column = colModel.getColumn(i);
             column.setHeaderValue(getColumnName(column.getModelIndex()));
         }
         m_table.getTableHeader().repaint();
         Collections.sort(m_vector, new SecureLinkComparator(modelIndex, m_sortAsc));
         m_table.tableChanged(
         new TableModelEvent(VWSecureLinkTableData.this));
         m_table.repaint();
     }
} 
}