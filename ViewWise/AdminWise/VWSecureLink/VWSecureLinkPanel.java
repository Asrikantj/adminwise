package ViewWise.AdminWise.VWSecureLink;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRecycle.VWRecycleConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWStatistic.VWOptionsDlg;
import ViewWise.AdminWise.VWSecureLink.VWSecureLinkConnector;
import ViewWise.AdminWise.VWSecureLink.VWSecureLinkPanel.SymAction;
import ViewWise.AdminWise.VWSecureLink.VWSecureLinkPanel.SymComponent;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWPrint;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;

public class VWSecureLinkPanel extends JPanel implements VWConstant {

    public VWSecureLinkPanel() {
        
    }
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWStatistic.setLayout(null);
        add(VWStatistic,BorderLayout.CENTER);
        VWStatistic.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWStatistic.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
/*        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);
*/      
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        PanelCommand.add(BtnRefresh);
        //BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnRefresh.setIcon(VWImages.RefreshIcon);
        
        top += hGap + 12;        
        BtnExpire.setText(BTN_SECURE_LINK_EXPIRE);
        BtnExpire.setActionCommand(BTN_SECURE_LINK_EXPIRE);
        PanelCommand.add(BtnExpire);
        BtnExpire.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnExpire.setIcon(VWImages.OptionsIcon);

        PanelTable.setLayout(new BorderLayout());
        VWStatistic.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWStatistic.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.StatisticImage);
        SymAction lSymAction = new SymAction();
        BtnRefresh.addActionListener(lSymAction);
        BtnExpire.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnRefresh)
                BtnRefresh_actionPerformed(event);
            else if (object == BtnExpire)
            	BtnExpire_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    VWPicturePanel picPanel = new VWPicturePanel(TAB_SECURE_LINK, VWImages.StatisticImage);
    javax.swing.JPanel VWStatistic = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnRefresh = new VWLinkedButton(1);
    VWLinkedButton BtnExpire = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWSecureLinkTable Table = new VWSecureLinkTable();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWStatistic)
                VWStatistic_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWStatistic_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWStatistic) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
    	AdminWise.printToConsole("SECURE LINK CLICKED :::: "+newRoom);
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        try {
        	List tableData = VWSecureLinkConnector.getSecureLinkData(newRoom);
        	AdminWise.printToConsole("tableData loadTabData Secure link :::: "+tableData);
        	
            Table.addData(tableData);
        }
        catch(Exception e){};
        setEnableMode(MODE_CONNECT);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnOption_actionPerformed(java.awt.event.ActionEvent event) {
    	new VWOptionsDlg(AdminWise.adminFrame);
    }
    
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event) {
        Table.clearData();
        Table.addData(VWSecureLinkConnector.getSecureLinkData());
        setEnableMode(MODE_CONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnExpire_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            int[] selectedRows=Table.getSelectedRows();
            int count=selectedRows.length;
            AdminWise.printToConsole("count of selected row BtnExpire_actionPerformed :::: "+count);
            AdminWise.printToConsole("Full row BtnExpire_actionPerformed :::: "+selectedRows[0]);
            for(int i=0;i<count;i++) {
                int row=selectedRows[i]-i;
                AdminWise.printToConsole("row BtnExpire_actionPerformed :::: "+row);
                VWSecureLinkConnector.expireSelectedLink(Table.getRowNodeId(row));
                Table.removeData(row);
                List tableData = VWSecureLinkConnector.getSecureLinkData();
            	AdminWise.printToConsole("tableData after clicking on expire button Secure link :::: "+tableData);
            	Table.addData(tableData);
            }
        }
        catch(Exception e){}
        finally {
            AdminWise.adminPanel.setDefaultPointer();
        }
    }
    //--------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event) {
        new VWPrint(Table);
    }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode) {
        BtnRefresh.setVisible(true);
        BtnRefresh.setEnabled(true);
        BtnExpire.setVisible(true);
        BtnExpire.setEnabled(true);
        
        switch (mode) {
            case MODE_CONNECT:
                if(Table.getRowCount()==0) {
                	BtnExpire.setEnabled(false);
                }
                break;
            case MODE_UNCONNECT:
                BtnRefresh.setEnabled(false);
                BtnExpire.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    private boolean UILoaded=false;


}
