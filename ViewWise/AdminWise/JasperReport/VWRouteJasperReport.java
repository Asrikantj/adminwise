package ViewWise.AdminWise.JasperReport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import com.computhink.common.Util;
import com.computhink.vwc.DBConnectionBean;
 
/*import ViewWise.AdminWise.AdminWise;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlReportConfiguration;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;*/

public class VWRouteJasperReport implements Serializable{
public static String home = null; 
	
	static 
    {
        findHome();
    }
	
	public static String getHome()
    {
        return home;
    }

	private static void findHome() {
		String urlPath = "";
		String indexerHome = "";
		String res = "com/computhink/vwc/image/images/vwc.gif";
		String jarres = "/lib/viewwise.jar!";

		int startIndex = 0;
		int endIndex = 0;
		try {
			URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
			urlPath = url.getPath();
		} catch (Exception e) {
			urlPath = "";
		}
		if (!urlPath.equalsIgnoreCase("")) {
			if (urlPath.startsWith("file")) {
				startIndex = 6;
				endIndex = urlPath.length() - res.length() - jarres.length();
			} else {
				startIndex = 1;
				endIndex = urlPath.length() - res.length();
			}
			indexerHome = urlPath.substring(startIndex, endIndex - 1);
			/*
			 * getHome is going wrong in Linux box because of root folder i.e /
			 * So checking OS and and (/) root folder is added. 24 Mar 2008
			 */
			if (Util.getEnvOSName().indexOf("Linux") == -1)
				indexerHome = indexerHome.replace('/', '\\');
			else if (Util.getEnvOSName().indexOf("Linux") != -1 && (!indexerHome.startsWith("/"))) {
				indexerHome = "/" + indexerHome;
			}
		}
		home = cleanUP(indexerHome);
	}
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = str.replace ("%20", " ");
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }
	
	/*public boolean generateStaticJasperReport(int sessionID, String selectedReport, String outputFormat, String dateFrom, String dateTo, String path) {
		AdminWise.printToConsole("inside generateStaticJasperReport");
		//System.out.println("inside generateJasperReport1");
		boolean isReportGenerated = false;
		DBConnectionBean dbConnectionBean = null;
		Connection con = null;
		File jasperFile = null;
		try {
			AdminWise.printToConsole("sessionID ...."+sessionID);
			if (sessionID != 0) {
				AdminWise.printToConsole("Before calling getDBConnection ");
				dbConnectionBean = AdminWise.gConnector.ViewWiseClient.getDBConnection(sessionID);
				if (dbConnectionBean != null) {
					con = dbConnectionBean.getConnection();
				}
				AdminWise.printToConsole("After calling getDBConnection....."+con);
			} else {
				// This condition has included for report generation checking from eclipse
				//step1 load the driver class
				Class.forName("oracle.jdbc.driver.OracleDriver");
	
				// step2 create the connection object
				con = DriverManager.getConnection("jdbc:oracle:thin:@10.4.10.87:1521:orcl", "SIDBI_ROOM", "SIDBI_ROOM");
			}
		} catch (Exception e) {
			AdminWise.printToConsole("Exception in DB connection...."+e);
		}
		String filePath = home + Util.pathSep + "Reports" + Util.pathSep;
		
		filePath = filePath.replace("Contentverse Client", "Contentverse");
		if (con != null) {
			//System.out.println("Connection established with DB.....");
			AdminWise.printToConsole("Connection established with DB.....");
			try {
				AdminWise.printToConsole("Before reading jrxml file...");
				//System.out.println("Before reading jrxml file..."+selectedReport);
				InputStream inputFile =  null;
				AdminWise.printToConsole("selectedReport :"+selectedReport);
				String fileName = null;
				if (selectedReport.equalsIgnoreCase("Pendency Report")) {
					AdminWise.printToConsole("inside pendency...");
					fileName = "PendencyReport.jrxml";
				} else if (selectedReport.equalsIgnoreCase("Workflow Completed Report")){
					fileName = "WorkflowCompletedDocumentsReport.jrxml";
				} else if (selectedReport.equalsIgnoreCase("Cabinet Wise Workflow Documents")){	
					fileName = "CabinetWiseWFDocs.jrxml";
				} else if (selectedReport.equalsIgnoreCase("Page Count For Documents in Route")){	
					fileName = "PageCountForDocsInRoute.jrxml";
				} else if (selectedReport.equalsIgnoreCase("Pendency report - Vertical wise -  Document Type")){	
					fileName = "PendencyVerticalWiseDocument.jrxml";
				} else if (selectedReport.equalsIgnoreCase("Pendency report - Vertical wise")){	
					fileName = "PendencyReportVerticalwise.jrxml";
				}else if (selectedReport.equalsIgnoreCase("Route History Report")){	
					fileName = "RouteHistoryReport.jrxml";
				}else if (selectedReport.equalsIgnoreCase("Route Master History Report")){	
					fileName = "RouteMasterHistoryReport.jrxml";
				}else if (selectedReport.equalsIgnoreCase("Route Task Wise User Count")){	
					fileName = "RouteTaskWiseUserCount.jrxml";
				}else if (selectedReport.equalsIgnoreCase("Task Completed Report")){	
					fileName = "TaskCompletedReport.jrxml";
				}else if (selectedReport.equalsIgnoreCase("Workflow pending documents")){	
					fileName = "WorkflowPendingDocuments.jrxml";
				}
				AdminWise.printToConsole("Jasper report fileName :"+fileName);
				filePath = filePath + fileName;
				AdminWise.printToConsole("Jasper report file path :"+filePath);
				jasperFile = new File(filePath);
				AdminWise.printToConsole("Is japer file exists :"+jasperFile.exists());
				if (jasperFile.exists()) {
					//inputFile = this.getClass().getResourceAsStream(filePath);
					inputFile = new FileInputStream(jasperFile);
				} else {
					inputFile = this.getClass().getResourceAsStream(fileName);
				}
				AdminWise.printToConsole("After reading jrxml file..."+inputFile.available());
				HashMap parameter = null;
				if (!selectedReport.equalsIgnoreCase("Pendency Report")) {
					if (dateFrom != null && dateFrom.trim().length() > 0 && dateTo != null && dateTo.trim().length() > 0) {
						parameter = new HashMap();
						parameter.put("FROM_DATE", dateFrom);
						parameter.put("TO_DATE", dateTo);
						AdminWise.printToConsole("From Date :"+parameter.get("FROM_DATE")+" To Date :"+parameter.get("TO_DATE"));
					}
				}
				//System.out.println("After reading jrxml file...");
				JasperReport jasperReport = JasperCompileManager.compileReport(inputFile);
				AdminWise.printToConsole("After compiling jrxml file...");
				//System.out.println("After compiling jrxml file...");
			    JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, parameter, con);
				AdminWise.printToConsole("After fillReport....");
				AdminWise.printToConsole("Report Location :"+path);
				AdminWise.printToConsole("outputFormat :"+outputFormat);
				//System.out.println("Report Location :"+path);
			    //System.out.println("After fillReport....");
				if (outputFormat.equalsIgnoreCase("pdf")) {
					//path = path+".pdf";
					exportReport(jprint, path);
			    } else if (outputFormat.equalsIgnoreCase("html")) {
			    	//path = path+".html";
			    	exportReportHtml(jprint, path);
			    } else if (outputFormat.equalsIgnoreCase("xls")) {
			    	//path = path+".xls";
			    	exportReportXls(jprint, path);
			    } else if (outputFormat.equalsIgnoreCase("csv")) {
			    	exportReportCsv(jprint, path);
			    }else {
			    	JasperViewer.viewReport(jprint);
			    }
				AdminWise.printToConsole("After viewreport....");
				System.out.println("After viewreport....");
				isReportGenerated = true;
			} catch (Exception e) {
				AdminWise.printToConsole("Exception in jasper generation:::::" + e.getMessage());
				//System.out.println("Exception in jasper generation:::::" + e.fillInStackTrace().getMessage().toString());
			} finally {
				try {
					jasperFile = null;
					if (con != null)
						con.close();
				} catch(Exception e) {
					
				}
			}
		}
		return isReportGenerated;
	}
	
	public boolean generateDynamicJasperReport(Vector<String> resultSet, String columnDescription) {
		boolean isReportGenerated = false;
		DynamicJasperReport dynamicReport = new DynamicJasperReport(new ArrayList<>(resultSet), columnDescription);
		try {
            JasperPrint jp = dynamicReport.getReport();
            JasperViewer jasperViewer = new JasperViewer(jp);
            jasperViewer.setVisible(true);
 
        } catch (Exception ex) {
        	AdminWise.printToConsole("Exception while creating dynamic search report :"+ex.getMessage());
        }
		return isReportGenerated;
	}
	
	public static void exportReportHtml(JasperPrint jp, String path) throws JRException, FileNotFoundException {
		AdminWise.printToConsole("Exporting html report to: " + path);
		File outputFile = new File(path);
	    File parentFile = outputFile.getParentFile();
	    if (parentFile != null)
	        parentFile.mkdirs();
	    FileOutputStream fos = new FileOutputStream(outputFile);
	    PrintWriter out = new PrintWriter(fos);
        JRHtmlExporter exporter = new JRHtmlExporter();
        exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN,  false);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
        exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, out);
        exporter.exportReport();
        out.flush();
        out.close();

        AdminWise.printToConsole("HTML Report exported: " + path);
    }
	
	public static void exportReport(JasperPrint jp, String path) throws JRException, FileNotFoundException {
		AdminWise.printToConsole("Exporting pdf report to: " + path);
    	//JasperExportManager.exportReportToPdf(jp);
        JRPdfExporter exporter = new JRPdfExporter();
        try {
	        File outputFile = new File(path);
	        File parentFile = outputFile.getParentFile();
	        if (parentFile != null)
	            parentFile.mkdirs();
	        AdminWise.printToConsole("parentFile.isExist : " + parentFile.exists());
	        FileOutputStream fos = new FileOutputStream(outputFile);
	
	        SimpleExporterInput simpleExporterInput = new SimpleExporterInput(jp);
	        OutputStreamExporterOutput simpleOutputStreamExporterOutput = new SimpleOutputStreamExporterOutput(fos);
	
	        exporter.setExporterInput(simpleExporterInput);
	        exporter.setExporterOutput(simpleOutputStreamExporterOutput);
	
	        exporter.exportReport();
        } catch (Exception e) {
        	System.out.println("Exception in pdf generation...."+e.getMessage());
        }
        AdminWise.printToConsole("Report exported: " + path);
    }
	
	public static void exportReportXls(JasperPrint jp, String path, SimpleXlsReportConfiguration configuration) throws JRException, FileNotFoundException {
        JRXlsExporter exporter = new JRXlsExporter();

        File outputFile = new File(path);
        File parentFile = outputFile.getParentFile();
        if (parentFile != null)
            parentFile.mkdirs();
        FileOutputStream fos = new FileOutputStream(outputFile);

        exporter.setConfiguration(configuration);

        SimpleExporterInput simpleExporterInput = new SimpleExporterInput(jp);
        OutputStreamExporterOutput simpleOutputStreamExporterOutput = new SimpleOutputStreamExporterOutput(fos);

        exporter.setExporterInput(simpleExporterInput);
        exporter.setExporterOutput(simpleOutputStreamExporterOutput);

        exporter.exportReport();

        System.out.println("Xlsx Report exported: " + path);
    }
	public static void exportReportXls(JasperPrint jp, String path) throws JRException, FileNotFoundException {
        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setDetectCellType(true);
        configuration.setWhitePageBackground(false);
        configuration.setIgnoreGraphics(false);
        configuration.setIgnorePageMargins(true);

        exportReportXls(jp, path, configuration);
	}
	public static void exportReportCsv(JasperPrint jp, String path) {
		try {
			AdminWise.printToConsole("inside exportReportCsv method....");
			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jp));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(new File(path)));
			SimpleCsvExporterConfiguration configuration = new SimpleCsvExporterConfiguration();
			configuration.setRecordDelimiter("\r\n");
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			AdminWise.printToConsole("csv file created....");
		} catch(Exception e) {
			AdminWise.printToConsole("Exception in exportReportCsv :"+e.getMessage());
		}
	}	
	public static void main(String[] args) {
		System.out.println("inside main method....");
		VWRouteJasperReport a = new VWRouteJasperReport();
		//a.generateJasperReport();
		//a.generatePendencyJasperReport(0);
		a.generateStaticJasperReport(0, "PendencyReport", "html", null, null, "D:\\JasperReportOutput");
	}*/
}
