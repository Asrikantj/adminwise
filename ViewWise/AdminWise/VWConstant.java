package ViewWise.AdminWise;

import java.awt.Cursor;
import java.io.File;

import com.computhink.common.Constants;
import com.computhink.common.Notification;
import com.computhink.resource.ResourceManager;
import ViewWise.AdminWise.AdminWise;

import ViewWise.AdminWise.VWUtil.VWRecord;
public interface VWConstant
{

    //Constants
    String RoomFileName="ViewWiseRooms.properties";
    String OptionsFileName="ViewWise.properties";

    int BROWSER_TYPE_APPLETVIEWER = 0;
    int BROWSER_TYPE_EXPLORER = 1;
    int BROWSER_TYPE_NETSCAPE = 2;
    int BROWSER_TYPE_HOTJAVA = 3;
    int BROWSER_TYPE_NETSCAPEPLUGIN = 4;
    int BROWSER_TYPE_MICROSOFTPLUGIN = 5;

    ///String VirtualRoomName="FADISH123";
    Cursor HAND_CURSOR = Cursor.getPredefinedCursor(12);
    Cursor WAIT_CURSOR = Cursor.getPredefinedCursor(3);
    Cursor DEFAULT_CURSOR = Cursor.getPredefinedCursor(0);

    String SepChar=(new Character((char)9)).toString();
    String NewLineChar=(new Character((char)13)).toString() + (new Character((char)10)).toString();
    String DoubleQuotation=(new Character((char)34)).toString();

    String version ="1.0";
    String author="Computhink Inc.";

    //General
    int AllRoom_TYPE = 0;
    int Room_TYPE = 1;
    int Cabinet_TYPE = 2;
    int Drawer_TYPE = 3;
    int Folder_TYPE = 4;
    int CheckOut_TYPE=5;
    int CheckIn_TYPE=6;
    int CheckInSession_TYPE=7;
    int CheckOutSession_TYPE=8;
    int SessionFolder_TYPE=9;
    int Document_TYPE = 10;
    int OpenDocument_TYPE=11;
    int OpenShareDocument_TYPE=12;
    int Viewer_TYPE=13;
    int RefDocument_TYPE=14;

    String mac      = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
    String metal    = "javax.swing.plaf.metal.MetalLookAndFeel";
    String motif    = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

    int SplitterWidth=8;

     int Property_VWVerbosity_Index=1;
    int Property_VWFlushLog_Index=2;
    int Property_VWTraceErrors_Index=3;
    int Property_VWStdOut_Index=4;
    int Property_VWFileSize_Index=5;
    int Property_VWFileCount_Index=6;
    int Property_VWHome_Index=7;

    int Room_Status_UnConnect=-1;
    int Room_Status_Connect=0;
    int Room_Status_Down=-2;

    String Temp_Folder_Name=File.separatorChar + "Temp";
    String System_Folder_Name=File.separatorChar + "System";
    String Ref_Folder_Name=File.separatorChar + "Links";
    String Help_Folder_Name=File.separatorChar + "Help";

    /*Tab Index Id's*/
    //CV10.1 (Grouping 1 � Setup)
    int TAB_DOCTYPE_ID=0;
    int TAB_ARCHIVE_ID=1;
    int TAB_PRINCIPAL_ID=2;
    int TAB_RETENTION_ID = 3;
    int TAB_NOTIFICATION_ID = 4;
    int TAB_ROUTE_ID=5;
    //(Grouping 2 � Monitoring)
    int TAB_AUDITTRAIL_ID=6;
    int TAB_CONUSER_ID=7;
    int TAB_DOCLOCK_ID=8;
    int TAB_RECYCLE_ID=9;
    int TAB_STATISTIC_ID=10;
    int TAB_REDACTIONS_ID=11;
    int TAB_ROUTEREPORT_ID=12;
    int TAB_FORM_TEMPLATE_ID=13;
    int TAB_SECURE_LINK_ID=14;
    
    //(Group 3 -  Administration)
    int TAB_TEMPLATE_ID=15;
    int TAB_BACKUP_ID=16;
    int TAB_RESTORE_ID=17;
  
    //int TAB_PRINCIPAL_ID=11;
    
    //int TAB_SECURITY_ID=13;
    
    //int TAB_RETENTION_ID = 12;
    //int TAB_NOTIFICATION_ID = 13;
    //int TAB_ROUTE_ID=14;
  
        
    //update TOTAL_TAB_COUNT if new TABS are added in future.
    int TOTAL_TAB_COUNT=17;
    String[] YesNoData={"Yes","No"};
    String[] SeqUniqData={"Yes","No","Unique to Room"};
    int MODE_UNSELECTED=0;
    int MODE_SELECT=1;
    int MODE_UPDATE=2;
    int MODE_UNCONNECT=3;
    int MODE_USEDSELECT=4;
    int MODE_CONNECT=5;
    int MODE_LOAD=6;
    int MODE_UNSELECTINDEX=7;
    int MODE_SELECTINDEX=8;
    int MODE_NEW=9;
    int MODE_DISABLETOOLBAR=10;
    int MODE_UNSELECTEDSTORAGE=11;
    int MODE_SELECTEDSTORAGE=12;
    int MODE_EXPAND=13;
    int MODE_COLLAPSE=14;
    int MODE_NEWINDEX=15;
    int MODE_COPY=16;
    int MODE_MOVE=17;
    int MODE_ENABLE=18;
    int MODE_DISABLE=19;
    int MODE_ROUTE_ENABLE=20;
    int MODE_ROUTE_DISABLE=21;
    int MODE_DELETE=22;
    int MODE_UPDATE_KEY=23;
    int MODE_STATIC_REPORT=24;

    int APPLICATION_MODE=0;
    int APPLET_MODE=1;

    int CALLER_ARCHIVE=0;
    int CALLER_BACKUP=1;

    String USER_TYPE="1";
    String GROUP_TYPE="2";


    int TEXT_CONDS_INDEX=0;
    int NUMBER_CONDS_INDEX=1;
    int DATE_CONDS_INDEX=2;
    int YESNO_CONDS_INDEX=3;
    int SELECTION_CONDS_INDEX=4;
    int AUTHOR_CONDS_INDEX=5;
    int AUTHOR_CONDS_FIXFIELDS=6;

    int NO_ERROR=0;
    int SEARCH_EMPTYNAME_ERROR=1;
    int SEARCH_ALREADYEXIST_ERROR=2;
    int EMPTY_SEARCHWITHOUTCONDS_ERROR=3;

    int[][] Conds= {{0,1,7,9},{0,1,2,3,4,5,6,8},{0,1,2,3,4,5,6,8},{0,1,6},{0,1,7,9},{0,1,2,3,4,5,6,8},{10,11}};

    VWRecord[] CondsArr={new VWRecord(-1,"<None>"),
                        new VWRecord(0,"Equal"),
                        new VWRecord(1,"Greater than"),
                        new VWRecord(2,"Less Than"),
                        new VWRecord(3,"Greater than or equal"),
                        new VWRecord(4,"Less than or equal"),
                        new VWRecord(5,"Not equal"),
                        new VWRecord(6,"Like"),
                        new VWRecord(7,"Between"),
                        new VWRecord(8,"In"),
                        new VWRecord(9,"<Any user>"),
                        new VWRecord(10,"<Empty user>")};
    String[][] SystemFielsd={
        {"System Field","Creator Name","0","<Any User>","","0","0","5",""},
        {"System Field","Creation Date","-1","","","0","0","2","yyyyMMdd"}};
    //-----------------------------------------------------------
    //Template

    String TemplateExt="VWT";
    String TemplatesFolder_Name=AdminWise.connectorManager.getString("constant.TemplatesFolder_Name");
    String TemplateFolder_Name=AdminWise.connectorManager.getString("constant.TemplateFolder_Name");

    //-----------------------------------------------------------

    /*Tab Names*/
    //added in property file
    String TAB_RETENTION_NAME=AdminWise.connectorManager.getString("constant.TAB_RETENTION_NAME")+"          ";
    String TAB_NOTIFICATION_NAME=AdminWise.connectorManager.getString("constant.TAB_NOTIFICATION_NAME")+"          ";
    /**
     * @author apurba.m
     * CV 10.1 Enhancement: TAB_FORM_TEMPLATE_NAME added for new form template tab in Adminwise
     */
    String TAB_FORM_TEMPLATE_NAME=AdminWise.connectorManager.getString("constant.TAB_FORM_TEMPLATE_NAME")+"          ";
    String TAB_ROUTE_NAME=ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"            ";
    /**CV10.2 Enhancement - Added for Workflow Report Lable change**/
    String TAB_ROUTE_REPORT_NAME=AdminWise.connectorManager.getString("RouteReport.Report") +"            ";
    String TAB_DOCTYPES_NAME=AdminWise.connectorManager.getString("constant.TAB_DOCTYPES_NAME")+"     ";
    String TAB_ARCHIVE_NAME=AdminWise.connectorManager.getString("constant.TAB_ARCHIVE_NAME")+"     ";
    String TAB_AUDITTRAIL_NAME=AdminWise.connectorManager.getString("constant.TAB_AUDITTRAIL_NAME")+"     ";
    String TAB_USERS_NAME= AdminWise.connectorManager.getString("constant.TAB_USERS_NAME")+"     ";
    String TAB_DOCLOCK_NAME= AdminWise.connectorManager.getString("constant.TAB_DOCLOCK_NAME")+"     ";
    String TAB_RECYCLE_NAME=  AdminWise.connectorManager.getString("constant.TAB_RECYCLE_NAME")+"     ";
    String TAB_STATISTIC_NAME= AdminWise.connectorManager.getString("constant.TAB_STATISTIC_NAME")+"     ";
    String TAB_SECURE_LINK= AdminWise.connectorManager.getString("constant.TAB_SECURE_LINK")+"     ";
    String TAB_PROPERTIES_NAME=AdminWise.connectorManager.getString("constant.TAB_PROPERTIES_NAME")+"     ";
    String TAB_LOGVIEWER_NAME= AdminWise.connectorManager.getString("constant.TAB_LOGVIEWER_NAME")+"     ";
    String TAB_TEMPLATE_NAME= AdminWise.connectorManager.getString("constant.TAB_TEMPLATE_NAME")+"     ";
    String TAB_BACKUP_NAME=AdminWise.connectorManager.getString("constant.TAB_BACKUP_NAME")+"     ";
    String TAB_RESTORE_NAME=AdminWise.connectorManager.getString("constant.TAB_RESTORE_NAME")+"     ";
    String TAB_REDACTIONS_NAME=AdminWise.connectorManager.getString("constant.TAB_REDACTIONS_NAME")+"     ";
    String TAB_PRINCIPAL_NAME=AdminWise.connectorManager.getString("constant.TAB_PRINCIPAL_NAME")+"     ";
    String TAB_SECURITY_NAME=AdminWise.connectorManager.getString("constant.TAB_SECURITY_NAME")+"     ";

    String DOCTYPE_TOOLTIP=AdminWise.connectorManager.getString("constant.DOCTYPE_TOOLTIP");
    String ARCHIVE_TOOLTIP=AdminWise.connectorManager.getString("constant.ARCHIVE_TOOLTIP");
    String AUDITTRAIL_TOOLTIP=AdminWise.connectorManager.getString("constant.AUDITTRAIL_TOOLTIP");
    String USERS_TOOLTIP=AdminWise.connectorManager.getString("constant.USERS_TOOLTIP");
    String DOCLOCK_TOOLTIP=AdminWise.connectorManager.getString("constant.DOCLOCK_TOOLTIP");
    String RECYCLE_TOOLTIP=AdminWise.connectorManager.getString("constant.RECYCLE_TOOLTIP");
    String STATISTIC_TOOLTIP=AdminWise.connectorManager.getString("constant.STATISTIC_TOOLTIP");
    String SECURE_LINK_TOOLTIP = AdminWise.connectorManager.getString("constant.SECURE_LINK_TOOLTIP");
    String PROPERTIES_TOOLTIP=AdminWise.connectorManager.getString("constant.PROPERTIES_TOOLTIP");
    String LOGVIEWER_TOOLTIP=AdminWise.connectorManager.getString("constant.LOGVIEWER_TOOLTIP");
    String TEMPLATE_TOOLTIP=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("constant.TEMPLATE_TOOLTIP");
    String BACKUP_TOOLTIP=ResourceManager.getDefaultManager().getString("CVProduct.Name")+ " "+AdminWise.connectorManager.getString("constant.BACKUP_TOOLTIP");
    String RESTORE_TOOLTIP=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ AdminWise.connectorManager.getString("constant.RESTORE_TOOLTIP");
    String REDACTIONS_TOOLTIP=AdminWise.connectorManager.getString("constant.REDACTIONS_TOOLTIP");
    String PRINCIPAL_TOOLTIP=AdminWise.connectorManager.getString("constant.PRINCIPAL_TOOLTIP");
    String SECURITY_TOOLTIP=AdminWise.connectorManager.getString("constant.SECURITY_TOOLTIP");
    String ROUTE_TOOLTIP=ResourceManager.getDefaultManager().getString("WORKFLOW_NAME");
    String RETENTION_TOOLTIP=AdminWise.connectorManager.getString("constant.RETENTION_TOOLTIP");
    String NOTIFICATION_TOOLTIP=AdminWise.connectorManager.getString("constant.NOTIFICATION_TOOLTIP");
    String FORM_TEMPLATE_TOOLTIP=AdminWise.connectorManager.getString("constant.FORM_TEMPLATE_TOOLTIP");

    String MSG_TITLE=AdminWise.connectorManager.getString("Administration.Title");
    String[] MenuNames={"",//0
    		AdminWise.connectorManager.getString("constant.MenuNames1"),//1
    		AdminWise.connectorManager.getString("constant.MenuNames2"),//2
    		AdminWise.connectorManager.getString("constant.MenuNames3"),//3
    		AdminWise.connectorManager.getString("constant.MenuNames4"),//4
    		AdminWise.connectorManager.getString("constant.MenuNames5"),//5
    		AdminWise.connectorManager.getString("constant.MenuNames6"),//6
    		AdminWise.connectorManager.getString("constant.MenuNames7"),//7
    		AdminWise.connectorManager.getString("constant.MenuNames8")//8
                };
    int Properties_Count=11;

    String BackupFileName="Document.VWD";
    String BackupPageFolderName="Pages";
    String BackupHTMLFileName="Document.HTML";

    int HELP_Document_Type=23;
    int HELP_Storage_Management=8;
    int HELP_Audit_Trail=9;
    int HELP_Connected_Users=10;
    int HELP_Locked_Documents=11;
    int HELP_Recycled_Documents=12;
    int HELP_Room_Statistics=13;
    int HELP_Room_Template=14;
    int HELP_Backup_Restore_Options=15;
    int HELP_Backup=36;
    int HELP_Restore=38;
    int HELP_Users_Groups=17;

    String LBL_ROOMS=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+AdminWise.connectorManager.getString("constant.LBL_ROOMS") ;

    String FILTER_ALL_ROUTES="All "+ AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME") +"s";


    //Button
    //-----------------------------------------------------------
    String BTN_ADDTOLIST_NAME=AdminWise.connectorManager.getString("constant.BTN_ADDTOLIST_NAME");
    //String BTN_CLEARLIST_NAME="Remove All";
    String BTN_CLEARSELECTED_NAME=AdminWise.connectorManager.getString("constant.BTN_CLEARSELECTED_NAME");
    String BTN_MOVE_NAME=AdminWise.connectorManager.getString("constant.BTN_MOVE_NAME");
    String BTN_CANCELMOVE_NAME=AdminWise.connectorManager.getString("constant.BTN_CANCELMOVE_NAME");
    String BTN_COPY_NAME=AdminWise.connectorManager.getString("constant.BTN_COPY_NAME");
    String BTN_CANCELCOPY_NAME=AdminWise.connectorManager.getString("constant.BTN_CANCELCOPY_NAME");
    String BTN_SIZE_NAME=AdminWise.connectorManager.getString("constant.BTN_SIZE_NAME");
    String BTN_LOCATIONDEF_NAME=AdminWise.connectorManager.getString("constant.BTN_LOCATIONDEF_NAME");
    String BTN_OK_NAME=AdminWise.connectorManager.getString("constant.BTN_OK_NAME");
    String BTN_CANCEL_NAME=AdminWise.connectorManager.getString("constant.BTN_CANCEL_NAME");
    String BTN_RESET=AdminWise.connectorManager.getString("constant.BTN_Reset");
    String BTN_NEW_NAME=AdminWise.connectorManager.getString("constant.BTN_NEW_NAME");
    String BTN_SAVE_NAME=AdminWise.connectorManager.getString("constant.BTN_SAVE_NAME");
    String BTN_GENERATE_REPORT=AdminWise.connectorManager.getString("constant.BTN_GENERATE_REPORT");
    String BTN_UPDATE_NAME=AdminWise.connectorManager.getString("constant.BTN_UPDATE_NAME");
    String BTN_DELETE_NAME=AdminWise.connectorManager.getString("constant.BTN_DELETE_NAME");
    String BTN_DEL_NAME=AdminWise.connectorManager.getString("constant.BTN_DEL_NAME");
    String BTN_UPDATE_KEY_NAME=AdminWise.connectorManager.getString("constant.BTN_UPDATE_KEY_NAME");
    String BTN_APPLY_NAME=AdminWise.connectorManager.getString("constant.BTN_APPLY_NAME");
    String BTN_CLOSE_NAME=AdminWise.connectorManager.getString("constant.BTN_CLOSE_NAME");
    String BTN_HELP_NAME=AdminWise.connectorManager.getString("constant.BTN_HELP_NAME");
    String BTN_HELPONLINE_NAME=AdminWise.connectorManager.getString("constant.BTN_HELPONLINE_NAME");
    String BTN_EXPAND_NAME=">>";
    String BTN_COLLAPSE_NAME="<<";
    String BTN_REFRESH_NAME=AdminWise.connectorManager.getString("constant.BTN_REFRESH_NAME");
    String BTN_SECURE_LINK_EXPIRE = AdminWise.connectorManager.getString("constant.BTN_SECURE_LINK_EXPIRE");
    String LBL_ENABLEINHRT_NAME=AdminWise.connectorManager.getString("constant.LBL_ENABLEINHRT_NAME");
    String BTN_PRINT_NAME=AdminWise.connectorManager.getString("constant.BTN_PRINT_NAME");
    String BTN_SETTING_NAME=AdminWise.connectorManager.getString("constant.BTN_SETTING_NAME");
    String BTN_GETDATA_NAME=AdminWise.connectorManager.getString("constant.BTN_GETDATA_NAME");
    String BTN_PURGEDATA_NAME=AdminWise.connectorManager.getString("constant.BTN_PURGEDATA_NAME");
    String BTN_SAVEASHTML_NAME=AdminWise.connectorManager.getString("constant.BTN_SAVEASHTML_NAME");
    String BTN_SAVEAS_NAME=AdminWise.connectorManager.getString("constant.BTN_SAVEAS_NAME");
    String BTN_SAVE_AS_NAME=AdminWise.connectorManager.getString("constant.BTN_SAVE_AS_NAME");
    String BTN_Review_Update=AdminWise.connectorManager.getString("constant.BTN_Review_Update");
    String BTN_VIEW_NAME=AdminWise.connectorManager.getString("constant.BTN_VIEW_NAME");
    String BTN_ARCHIVE_NAME=AdminWise.connectorManager.getString("constant.BTN_ARCHIVE_NAME");
    String BTN_MOREDATA_NAME=AdminWise.connectorManager.getString("constant.BTN_MOREDATA_NAME");
    String BTN_STOP_NAME=AdminWise.connectorManager.getString("constant.BTN_STOP_NAME");
    String BTN_CLEAR_NAME=AdminWise.connectorManager.getString("constant.BTN_CLEAR_NAME");
    String	BTN_RETENTION=AdminWise.connectorManager.getString("constant.BTN_RETENTION");
     
    String BTN_BACKUP_NAME=AdminWise.connectorManager.getString("constant.BTN_BACKUP_NAME");
    String BTN_OPTIONS_NAME=AdminWise.connectorManager.getString("constant.BTN_OPTIONS_NAME");
    String BTN_PROPERTIES_NAME=AdminWise.connectorManager.getString("constant.BTN_PROPERTIES_NAME");
    String BTN_DISCONNECT_NAME=AdminWise.connectorManager.getString("constant.BTN_DISCONNECT_NAME");
    String BTN_DISCONNECTALL_NAME=AdminWise.connectorManager.getString("constant.BTN_DISCONNECTALL_NAME");
    String BTN_ALLOWCON_NAME=AdminWise.connectorManager.getString("constant.BTN_ALLOWCON_NAME");
    String BTN_DISALLOWCON_NAME=AdminWise.connectorManager.getString("constant.BTN_DISALLOWCON_NAME");
    String BTN_CONNECT_NAME=AdminWise.connectorManager.getString("constant.BTN_CONNECT_NAME");
    String BTN_EXECUTE_NAME=AdminWise.connectorManager.getString("constant.BTN_EXECUTE_NAME");
    String BTN_UNLOCK_NAME=AdminWise.connectorManager.getString("constant.BTN_UNLOCK_NAME");
    String BTN_UNLOCKALL_NAME=AdminWise.connectorManager.getString("constant.BTN_UNLOCKALL_NAME");
    String BTN_NEWDT_NAME=AdminWise.connectorManager.getString("constant.BTN_NEWDT_NAME");
    String BTN_VERREV_NAME=AdminWise.connectorManager.getString("constant.BTN_VERREV_NAME");
    String BTN_RETENTION_NAME=AdminWise.connectorManager.getString("constant.BTN_RETENTION_NAME");
    String BTN_ADD_NAME=AdminWise.connectorManager.getString("constant.BTN_ADD_NAME");
    String BTN_REMOVE_NAME=AdminWise.connectorManager.getString("constant.BTN_REMOVE_NAME");
    String BTN_DSN_NAME=AdminWise.connectorManager.getString("constant.BTN_DSN_NAME");
    String BTN_AUTOMAIL_NAME=AdminWise.connectorManager.getString("constant.BTN_AUTOMAIL_NAME");
    String BTN_SEARCH_NAME=AdminWise.connectorManager.getString("constant.BTN_SEARCH_NAME");
    String BTN_SORT_NAME=AdminWise.connectorManager.getString("constant.BTN_SORT_NAME");
    String BTN_NEWINDEX_NAME=AdminWise.connectorManager.getString("constant.BTN_NEWINDEX_NAME");
    String BTN_DBLOOKUP_NAME=AdminWise.connectorManager.getString("constant.BTN_DBLOOKUP_NAME");
    String BTN_FINDNOW_NAME=AdminWise.connectorManager.getString("constant.BTN_FINDNOW_NAME");
    String BTN_FINDINLIST_NAME=AdminWise.connectorManager.getString("constant.BTN_FINDINLIST_NAME");
    String BTN_RESET_NAME=AdminWise.connectorManager.getString("constant.BTN_RESET_NAME");
    String BTN_SAVESEARCH_NAME=AdminWise.connectorManager.getString("constant.BTN_SAVESEARCH_NAME");
    String BTN_SIGN_NAME=AdminWise.connectorManager.getString("constant.BTN_SIGN_NAME");
    String BTN_CHANGE_PASSWORD=AdminWise.connectorManager.getString("constant.BTN_CHANGE_PASSWORD");
    String BTN_EMAILID_NAME=AdminWise.connectorManager.getString("constant.BTN_EMAILID_NAME");
    String BTN_SYNC_NAME=AdminWise.connectorManager.getString("constant.BTN_SYNC_NAME");
    String  BTN_SUB_ADMIN=AdminWise.connectorManager.getString("constant.SUB_ADMIN");
    String BTN_SECURITY_REPORT=AdminWise.connectorManager.getString("constant.BTN_SecurityReport");
    String BTN_CAPTURE_NAME=AdminWise.connectorManager.getString("constant.BTN_CAPTURE_NAME");
    String BTN_START_NAME=AdminWise.connectorManager.getString("constant.BTN_START_NAME");
    String BTN_PURGE_NAME=AdminWise.connectorManager.getString("constant.BTN_PURGE_NAME");
    String BTN_PURGEALL_NAME=AdminWise.connectorManager.getString("constant.BTN_PURGEALL_NAME");
    String BTN_EMPTY_RECYCLE=AdminWise.connectorManager.getString("constant.BTN_EMPTY_RECYCLE");
    String BTN_RESTORE_NAME=AdminWise.connectorManager.getString("constant.BTN_RESTORE_NAME");
    String BTN_RESTORETO_NAME=AdminWise.connectorManager.getString("constant.BTN_RESTORETO_NAME");
    String BTN_GETDOCS_NAME=AdminWise.connectorManager.getString("constant.BTN_GETDOCS_NAME");
    String BTN_REPLACE_NAME=AdminWise.connectorManager.getString("constant.BTN_REPLACE_NAME");
    String BTN_LOAD_NAME=AdminWise.connectorManager.getString("constant.BTN_LOAD_NAME");
    String BTN_Find_NAME=AdminWise.connectorManager.getString("constant.BTN_Find_NAME");
    String BTN_Restore_NAME=AdminWise.connectorManager.getString("constant.BTN_Restore_NAME");
    String BTN_EXPORT_NAME=AdminWise.connectorManager.getString("constant.BTN_EXPORT_NAME");
    String BTN_SAVETOFILE_NAME=AdminWise.connectorManager.getString("constant.BTN_SAVETOFILE_NAME");
    String BTN_IMPORT_NAME=AdminWise.connectorManager.getString("constant.BTN_IMPORT_NAME");
    String BTN_LOADFROMFILE_NAME=AdminWise.connectorManager.getString("constant.BTN_LOADFROMFILE_NAME");
    String BTN_LOADROOMDATA_NAME=AdminWise.connectorManager.getString("constant.BTN_LOADROOMDATA_NAME");
    String BTN_SELECTALL_NAME=AdminWise.connectorManager.getString("constant.BTN_SELECTALL_NAME");
    String BTN_DESELECTALL_NAME=AdminWise.connectorManager.getString("constant.BTN_DESELECTALL_NAME");
    String BTN_LOGIN_NAME=AdminWise.connectorManager.getString("constant.BTN_LOGIN_NAME");
    String BTN_DOCTYPEINDICES_NAME=AdminWise.connectorManager.getString("constant.BTN_DOCTYPEINDICES_NAME");
    String BTN_GENERATE_NAME =AdminWise.connectorManager.getString("constant.BTN_GENERATE_NAME");
    String BTN_SUBADMIN_REMOVE=AdminWise.connectorManager.getString("constant.BTN_SUBADMIN_REMOVE");
    String  BTN_SUBADMIN_SAVE =AdminWise.connectorManager.getString("constant.BTN_SUBADMIN_SAVE");

    String BTN_NEWROUTE_NAME=AdminWise.connectorManager.getString("constant.BTN_NEWROUTE_NAME")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME");
    String BTN_DOCINROUTE_NAME=AdminWise.connectorManager.getString("constant.BTN_DOCINROUTE_NAME")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME");
    String BTN_ASSIGNROUTE = AdminWise.connectorManager.getString("WorkFlow.BTN_ASSIGNROUTE")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"...";
    String BTN_TEMPLATENAME=AdminWise.connectorManager.getString("WorkFlow.BTN_ASSIGNTEMPLATE");
    String BTN_ACCEPTNAME = AdminWise.connectorManager.getString("constant.BTN_ACCEPTNAME");
    String BTN_REJECTNAME = AdminWise.connectorManager.getString("constant.BTN_REJECTNAME");
    String BTN_SUMMARYNAME = AdminWise.connectorManager.getString("constant.BTN_SUMMARYNAME");
    String BTN_ROUTECOMPLETED_NAME = ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("constant.BTN_ROUTECOMPLETED_NAME");
    String BTN_TASKCOMPLETED_NAME = AdminWise.connectorManager.getString("constant.BTN_TASKCOMPLETED_NAME");
    String BTN_ENDROUTENAME = AdminWise.connectorManager.getString("constant.BTN_ENDROUTENAME")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"...";
    
    String BTN_NEWRETENTION_NAME =AdminWise.connectorManager.getString("constant.BTN_NEWRETENTION_NAME");
    String BTN_COMPLETEDDOCS_NAME =AdminWise.connectorManager.getString("constant.BTN_COMPLETEDDOCS_NAME");
    String LBL_AVAILABLERETENTIONS =AdminWise.connectorManager.getString("constant.LBL_AVAILABLERETENTIONS");
    String BTN_FREEZEDOCS_NAME =AdminWise.connectorManager.getString("constant.BTN_FREEZEDOCS_NAME") ;
    
    
    //--------------------- Notificaiton ---------------------
    
    String BTN_NOTIFICATION_NAME =AdminWise.connectorManager.getString("constant.BTN_NOTIFICATION_NAME");
    String BTN_NEWREPORT_NAME =AdminWise.connectorManager.getString("constant.BTN_NEWREPORT_NAME");
    String BTN_GENERATEREPORT =AdminWise.connectorManager.getString("constant.BTN_GENERATEREPORT");
    String BTN_PREVIEWREPORT =AdminWise.connectorManager.getString("constant.BTN_PRVWREPORT");
    String BTN_GENERATECVREPORT =AdminWise.connectorManager.getString("constant.BTN_GENERATECVREPORT");
    
    String LBL_USERS_LIST =AdminWise.connectorManager.getString("constant.LBL_USERS_LIST");
    String LBL_AVAILABLEReport =AdminWise.connectorManager.getString("constant.LBL_AVAILABLEReport");
    String LBL_WFREPORTNAME=AdminWise.connectorManager.getString("constant.LBL_WFREPORT_NAME");
    
    //------------------------ Form Template ----------------------
    String BTN_NEW_TEMPLATE_NAME =AdminWise.connectorManager.getString("constant.BTN_NEW_FORM_TEMPLATE_NAME");
    String LBL_AVAILABLE_TEMPLATES = AdminWise.connectorManager.getString("constant.LBL_AVAILABLE_TEMPLATES");
    String LBL_FORM_TEMPLATE_NAME = AdminWise.connectorManager.getString("constant.LBL_FORM_TEMPLATE_NAME");
    String SAVE_FORM_TEMPLATE = AdminWise.connectorManager.getString("constant.SAVE_FORM_TEMPLATE");
    String CANCEL_FORM_TEMPLATE = AdminWise.connectorManager.getString("constant.CANCEL_FORM_TEMPLATE");
    //-----------------------Labels-----------------------------

    // Document Routing 
    //Removed unnecssary column - COL_COMMENT
    String[] RouteColNames={AdminWise.connectorManager.getString("constant.RouteColNames_0"),ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" Name", AdminWise.connectorManager.getString("constant.RouteColNames_2"), AdminWise.connectorManager.getString("constant.RouteColNames_3"),
    		AdminWise.connectorManager.getString("constant.RouteColNames_4"),AdminWise.connectorManager.getString("constant.RouteColNames_5") ,AdminWise.connectorManager.getString("constant.RouteColNames_6") , AdminWise.connectorManager.getString("constant.RouteColNames_7")};
    String LBL_TRIGGER_AT = AdminWise.connectorManager.getString("constant.LBL_TRIGGER_AT")+" ";
    String LBL_ROOM_LOCATION =AdminWise.connectorManager.getString("constant.LBL_ROOM_LOCATION")+" " ;
    String LBL_UNC_PATH = AdminWise.connectorManager.getString("constant.LBL_UNC_PATH")+" ";
    String LBL_ROUTINGTITLE = "  "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" "+AdminWise.connectorManager.getString("constant.LBL_ROUTINGTITLE")+" ";
    String LBL_ROUTENAME = ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("constant.LBL_ROUTENAME");
    String LBL_UNC_PATH_INFO = AdminWise.connectorManager.getString("constant.LBL_UNC_PATH_INFO");
    String LBL_ROUTEDESC = ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("constant.LBL_ROUTEDESC")+" ";
    String LBL_ROUTEINVALID = AdminWise.connectorManager.getString("constant.LBL_ROUTEINVALID")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("constant.LBL_ROUTEDESC");
    String LBL_ROUTESTART = AdminWise.connectorManager.getString("constant.LBL_ROUTESTART")+" ";
    String LBL_ROUTEREADONLY =" "+AdminWise.connectorManager.getString("constant.LBL_ROUTEREADONLY")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME");
    String LBL_PROCESS =AdminWise.connectorManager.getString("constant.LBL_PROCESS")+" " ;
    String LBL_SERIAL =AdminWise.connectorManager.getString("constant.LBL_SERIAL") ;
    String LBL_PARALLEL =AdminWise.connectorManager.getString("constant.LBL_PARALLEL") ;
    String LBL_AVAILABLEUSERS =AdminWise.connectorManager.getString("constant.LBL_AVAILABLEUSERS") ;
    String LBL_APPROVERS = AdminWise.connectorManager.getString("constant.LBL_APPROVERS");
    String LBL_ACTION =AdminWise.connectorManager.getString("constant.LBL_ACTION")+" ";
    String LBL_AVAILABLEROUTES = AdminWise.connectorManager.getString("constant.LBL_AVAILABLEROUTES")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"s";
    String LBL_ASSIGNEDROUTES = AdminWise.connectorManager.getString("constant.LBL_ASSIGNEDROUTES")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +"s";
    
    String[] lstStartWhen = {AdminWise.connectorManager.getString("constant.lstStartWhen0"),AdminWise.connectorManager.getString("constant.lstStartWhen1")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"),AdminWise.connectorManager.getString("constant.lstStartWhen2")};
    String[] lstAction = {AdminWise.connectorManager.getString("constant.lstAction0"),
    		AdminWise.connectorManager.getString("constant.lstAction1"),
    		AdminWise.connectorManager.getString("constant.lstAction2"),
    		AdminWise.connectorManager.getString("constant.lstAction3"),
    		AdminWise.connectorManager.getString("constant.lstAction4")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"),
    		/*"Generate status report",*/
    		AdminWise.connectorManager.getString("constant.lstAction5"),
    		AdminWise.connectorManager.getString("constant.lstAction6"),
    		AdminWise.connectorManager.getString("constant.lstAction7")};
    String LBL_StartLocation =AdminWise.connectorManager.getString("constant.LBL_StartLocation");
    String LBL_ActionLocation = AdminWise.connectorManager.getString("constant.LBL_ActionLocation");
    String LBL_ASSIGNROUTESETTING_NAME = AdminWise.connectorManager.getString("constant.LBL_ASSIGNROUTESETTING_NAME")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" ";
    
    String LBL_ASSIGN_TEMPLATE=AdminWise.connectorManager.getString("constant.LBL_ASSIGNTEMPLATE");
    //Template
    //-----------------------------------------------------------

    String LBL_NAVIGATORDATA_NAME=" "+AdminWise.connectorManager.getString("constant.LBL_NAVIGATORDATA_NAME");
    String LBL_DOCTYPES_NAME=" "+AdminWise.connectorManager.getString("constant.LBL_DOCTYPES_NAME");
    String LBL_ADDDOCTYPE_NAME=AdminWise.connectorManager.getString("constant.LBL_ADDDOCTYPE_NAME");
    String LBL_LISTDOCTYPE_NAME=AdminWise.connectorManager.getString("constant.LBL_LISTDOCTYPE_NAME");

    String TOOLTIP_NODENEW=AdminWise.connectorManager.getString("constant.TOOLTIP_NODENEW");
    String TOOLTIP_NODEDEL=AdminWise.connectorManager.getString("constant.TOOLTIP_NODEDEL");
    String TOOLTIP_NODECLEAR=AdminWise.connectorManager.getString("constant.TOOLTIP_NODECLEAR");
    String TOOLTIP_DTADD=AdminWise.connectorManager.getString("constant.TOOLTIP_DTADD");
    String TOOLTIP_DTDEL=AdminWise.connectorManager.getString("constant.TOOLTIP_DTDEL");
    String TOOLTIP_DTCLEAR=AdminWise.connectorManager.getString("constant.TOOLTIP_DTCLEAR");
    String TOOLTIP_NODEEXPANDALL=AdminWise.connectorManager.getString("constant.TOOLTIP_NODEEXPANDALL");
    //-----------------------------------------------------------
     //Room
    //-----------------------------------------------------------
    String NOROOMSELECTED_ITEM=AdminWise.connectorManager.getString("constant.NOROOMSELECTED_ITEM");

    String LBL_ROOMS_NAME=ResourceManager.getDefaultManager().getString("CVProduct.Name")+ " "+AdminWise.connectorManager.getString("constant.LBL_ROOMS_NAME");
    String LBL_SERVERS_NAME=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ AdminWise.connectorManager.getString("constant.LBL_SERVERS_NAME");
    String LBL_CREATENEWROOM_NAME=AdminWise.connectorManager.getString("constant.LBL_CREATENEWROOM_NAME");

    String LBL_NOROOMS_FOUND=AdminWise.connectorManager.getString("constant.LBL_NOROOMS_FOUND");
    String LBL_REFRESH=AdminWise.connectorManager.getString("constant.LBL_REFRESH");

    String[][] roomData={{AdminWise.connectorManager.getString("constant.roomData0"),""},
        {AdminWise.connectorManager.getString("constant.roomData1"),""},
        {AdminWise.connectorManager.getString("constant.roomData2"),"0"}
    };
    //-----------------------------------------------------------
    //Retentions
    //-----------------------------------------------------------
    String[] RetentionsColumnNames={AdminWise.connectorManager.getString("constant.RetentionsColumnNames0"),AdminWise.connectorManager.getString("constant.RetentionsColumnNames1"),AdminWise.connectorManager.getString("constant.RetentionsColumnNames2"),
    		AdminWise.connectorManager.getString("constant.RetentionsColumnNames3"),AdminWise.connectorManager.getString("constant.RetentionsColumnNames4")};
    String[] RetentionsColumnWidths={"20","20","20","20","20"};
    String LBL_RETENTIONLOCATION =AdminWise.connectorManager.getString("constant.LBL_RETENTIONLOCATION")+" ";
    String LBL_RETENTIONNAME =AdminWise.connectorManager.getString("constant.LBL_RETENTIONNAME")+" ";
    String LBL_RETENTON_ACTION = AdminWise.connectorManager.getString("constant.LBL_RETENTON_ACTION")+" ";
    String LBL_NOTIFY_BEFORE1 = AdminWise.connectorManager.getString("constant.LBL_NOTIFY_BEFORE1");
    String LBL_NOTIFY_BEFORE2 = AdminWise.connectorManager.getString("constant.LBL_NOTIFY_BEFORE2");
    String CHK_RETENTION_POLICYE =AdminWise.connectorManager.getString("constant.CHK_RETENTION_POLICYE");
    String LBL_RETENTION_NAME =AdminWise.connectorManager.getString("constant.LBL_RETENTION_NAME");
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    //Notification
    //-----------------------------------------------------------
    String[] NotificationColumnNames={AdminWise.connectorManager.getString("constant.NotificationColumnNames0"),AdminWise.connectorManager.getString("constant.NotificationColumnNames1"),
    		AdminWise.connectorManager.getString("constant.NotificationColumnNames2"),AdminWise.connectorManager.getString("constant.NotificationColumnNames3")};
    String[] NotificationColumnWidths={"20","20","20","20"};
    String LBL_USERS_NAME = "<" +ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("Notification.LBL_USERS_NAME")+">";
    String LBL_AVAILABLE_ROUTEREPORT = "<" +AdminWise.connectorManager.getString("RouteReport.LBL_AVAILABLE") +" "+AdminWise.connectorManager.getString("RouteReport.LBL_REPORTS")+">";
    //-----------------------------------------------------------
    
    
    //-----------------------------------------------------------
     //Restore
    //-----------------------------------------------------------

    String LBL_RESTOREOPTIONS_NAME=AdminWise.connectorManager.getString("constant.LBL_RESTOREOPTIONS_NAME");
    String LBL_RESTOREDLGINFO_TITLE=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("constant.LBL_RESTOREDLGINFO_TITLE");
    String LBL_RESTORELOCATION_TITLE=AdminWise.connectorManager.getString("constant.LBL_RESTORELOCATION_TITLE");
    String LBL_READDOCINFO=AdminWise.connectorManager.getString("constant.LBL_READDOCINFO");
    //-----------------------------------------------------------
     //Redactions
    //-----------------------------------------------------------
    String LBL_REDACTIONCREATOR_NAME=AdminWise.connectorManager.getString("constant.LBL_REDACTIONCREATOR_NAME");
    String LBL_AllUSERS_NAME=AdminWise.connectorManager.getString("constant.LBL_AllUSERS_NAME");
    String LBL_CHANGEPASSWORD_NAME=AdminWise.connectorManager.getString("constant.LBL_CHANGEPASSWORD_NAME");
    String LBL_NEWPASSWORD=AdminWise.connectorManager.getString("constant.LBL_NEWPASSWORD");
    String LBL_CONFIRMPASSWORD=AdminWise.connectorManager.getString("constant.LBL_CONFIRMPASSWORD");
    String LBL_ALLPASSWORD=AdminWise.connectorManager.getString("constant.LBL_ALLPASSWORD");
    String LBL_SPECIFICPASSWORD=AdminWise.connectorManager.getString("constant.LBL_SPECIFICPASSWORD");

    //-----------------------------------------------------------
     //Restore
    //-----------------------------------------------------------

    String LBL_RESTORETO_NAME=AdminWise.connectorManager.getString("constant.LBL_RESTORETO_NAME");
    String LBL_PURGEDOCUMENT_NAME=AdminWise.connectorManager.getString("constant.LBL_PURGEDOCUMENT_NAME");
    String LBL_PURGE_NAME=AdminWise.connectorManager.getString("constant.LBL_PURGE_NAME");
    String LBL_RESTORE_NAME=AdminWise.connectorManager.getString("constant.LBL_RESTORE_NAME");
    String LBL_RECYCLEDDOC_NAME=AdminWise.connectorManager.getString("constant.LBL_RECYCLEDDOC_NAME");

    //-----------------------------------------------------------
     //Signature
    //-----------------------------------------------------------
    String LBL_SEARCHPRINCIPALS_NAME=AdminWise.connectorManager.getString("constant.LBL_SEARCHPRINCIPALS_NAME");
    String LBL_LISTPRINCIPALS_NAME=AdminWise.connectorManager.getString("constant.LBL_LISTPRINCIPALS_NAME");
    String LBL_ADDPRINCIPAL_NAME=AdminWise.connectorManager.getString("constant.LBL_ADDPRINCIPAL_NAME");
    String LBL_SEARCHFILTER_TEXT=AdminWise.connectorManager.getString("constant.LBL_SEARCHFILTER_TEXT");
    String LBL_SIGN_NAME=AdminWise.connectorManager.getString("constant.LBL_SIGN_NAME");

    String LBL_DEVICENOTREADY=AdminWise.connectorManager.getString("constant.LBL_DEVICENOTREADY");
    String LBL_SIGCAPTURED=AdminWise.connectorManager.getString("constant.LBL_SIGCAPTURED");
    String LBL_SIGTCAPTURED=AdminWise.connectorManager.getString("constant.LBL_SIGTCAPTURED");
    String LBL_CLEARANDRESIGN=AdminWise.connectorManager.getString("constant.LBL_CLEARANDRESIGN");

    //-----------------------------------------------------------
     //Find
    //-----------------------------------------------------------
        String LBL_DOCTYPES=AdminWise.connectorManager.getString("constant.LBL_DOCTYPES")+"     " ;
        String LBL_AUTHORS=AdminWise.connectorManager.getString("constant.LBL_AUTHORS")+"  ";
        String LBL_AUTHOR=AdminWise.connectorManager.getString("constant.LBL_AUTHOR")+"  ";
        String LBL_DOCCREATE=AdminWise.connectorManager.getString("constant.LBL_DOCCREATE")+" ";
        String LBL_DOCMOD= AdminWise.connectorManager.getString("constant.LBL_DOCMOD") ;
        String LBL_LOCATION=AdminWise.connectorManager.getString("constant.LBL_LOCATION") ;
        String LBL_BACKUP_LOCATION=AdminWise.connectorManager.getString("constant.LBL_BACKUP_LOCATION");
        String LBL_VERSIONS=AdminWise.connectorManager.getString("constant.LBL_VERSIONS") ;
        String LBL_COMMENT= AdminWise.connectorManager.getString("constant.LBL_COMMENT");
        String LBL_INDEXTEXT=AdminWise.connectorManager.getString("constant.LBL_INDEXTEXT");
        String LBL_CONTAINTSTEXT=AdminWise.connectorManager.getString("constant.LBL_CONTAINTSTEXT");
        String LBL_NODOCTYPE_COND=AdminWise.connectorManager.getString("constant.LBL_NODOCTYPE_COND");
        String LBL_NOLOCATION_COND=AdminWise.connectorManager.getString("constant.LBL_NOLOCATION_COND");
        String LBL_NOAUTHOR_COND=AdminWise.connectorManager.getString("constant.LBL_NOAUTHOR_COND");
        String LBL_NODATE_COND=AdminWise.connectorManager.getString("constant.LBL_NODATE_COND");
        String LBL_LOCATION_NAME=AdminWise.connectorManager.getString("constant.LBL_LOCATION_NAME");
        String LBL_SELECTIONINDEX_NAME=AdminWise.connectorManager.getString("constant.LBL_SELECTIONINDEX_NAME");
        String LBL_FIND_NAME=AdminWise.connectorManager.getString("constant.LBL_FIND_NAME");
        String LBL_CHUNKSIZE=AdminWise.connectorManager.getString("constant.LBL_CHUNKSIZE");
        String LBL_INDEX_TEXT=AdminWise.connectorManager.getString("constant.LBL_INDEX_TEXT");

        String LBL_NO_MASK_NAME=AdminWise.connectorManager.getString("constant.LBL_NO_MASK_NAME");
        String LBL_IN_MASK_NAME=AdminWise.connectorManager.getString("constant.LBL_IN_MASK_NAME");

        String CHK_APPENDHITLIST_NAME=AdminWise.connectorManager.getString("constant.CHK_APPENDHITLIST_NAME");
        String LBL_DOCTYPE_NAME=AdminWise.connectorManager.getString("constant.LBL_DOCTYPE_NAME");
        String LBL_DOCTYPEINDICES_NAME=AdminWise.connectorManager.getString("constant.LBL_DOCTYPEINDICES_NAME");
        String LBL_DOCTYPEINDICES_NAME_1=" "+AdminWise.connectorManager.getString("constant.LBL_DOCTYPEINDICES_NAME_1");

        String LBL_NODATAFOUND=AdminWise.connectorManager.getString("constant.LBL_NODATAFOUND");
        String LBL_NOUSERFOUND=AdminWise.connectorManager.getString("constant.LBL_NOUSERFOUND");
        String LBL_FIND_TITLE=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+ AdminWise.connectorManager.getString("constant.LBL_FIND_TITLE");
        String LBL_FIND_BACKUP_TITLE=AdminWise.connectorManager.getString("constant.LBL_FIND_BACKUP_TITLE");
        String LBL_SELECTALL=AdminWise.connectorManager.getString("constant.LBL_SELECTALL");
        String LBL_DESELECTALL=AdminWise.connectorManager.getString("constant.LBL_DESELECTALL");
        String LBL_DOCUMENTTYPE=AdminWise.connectorManager.getString("constant.LBL_DOCUMENTTYPE");
        String LBL_CREATOR=AdminWise.connectorManager.getString("constant.LBL_CREATOR");

        String LBL_DOCTYPE_NAME_1=AdminWise.connectorManager.getString("constant.LBL_DOCTYPE_NAME_1");

        String LBL_DOCTYPEINDICES=AdminWise.connectorManager.getString("constant.LBL_DOCTYPEINDICES");
        String NEW_NAME=AdminWise.connectorManager.getString("constant.NEW_NAME");
        String SEARCH_CAPTION=AdminWise.connectorManager.getString("constant.SEARCH_CAPTION");
    //-----------------------------------------------------------
     //Options
    //-----------------------------------------------------------

    String LBL_OPTIONS_NAME=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+AdminWise.connectorManager.getString("LBL_OPTIONS_NAME.AdminOptions");

    //-----------------------------------------------------------
     //DSN
    //-----------------------------------------------------------

    String LBL_DSNINFO_NAME=AdminWise.connectorManager.getString("constant.LBL_DSNINFO_NAME");
    String LBL_SQLSTATEMENT_NAME=AdminWise.connectorManager.getString("constant.LBL_SQLSTATEMENT_NAME");
    String LBL_DSNNAME_NAME=AdminWise.connectorManager.getString("constant.LBL_DSNNAME_NAME");
    String LBL_USER_NAME=AdminWise.connectorManager.getString("constant.LBL_USER_NAME");
    String LBL_PASSWORD=AdminWise.connectorManager.getString("constant.LBL_PASSWORD");
    String LBL_TABLE_NAME=AdminWise.connectorManager.getString("constant.LBL_TABLE_NAME");
    String LBL_FIELD_NAME=AdminWise.connectorManager.getString("constant.LBL_FIELD_NAME");
    String LBL_DBLINK_NAME=AdminWise.connectorManager.getString("constant.LBL_DBLINK_NAME");
    String LBL_DSNStatus = AdminWise.connectorManager.getString("constant.LBL_DSNStatus");

    //-----------------------------------------------------------
     //Con User
    //-----------------------------------------------------------

    String[] ClientType={"","Desktop Client","Web Client", Constants.PRODUCT_NAME + " Admin",
        "AIP Client","eWork Connector","Web Client","","Indexer Client","Desktop Client","Web Client", "", "SDK Client","ARS Client", "DWS Client", "Web Client", "EAS Client", "WebAccess Client", "VNS Client",
        "Custom Label WebAccess Client","Professional WebAccess Client","RightFax Client","WebAccess Concurrent Client","Professional Concurrent WebAccess Client","CSS Client","Content Sentinel WebAccess Client","WebAccess Named Client","Sub Administrator"};
    String CONUSER_TITLE_NAME=AdminWise.connectorManager.getString("constant.TAB_USERS_NAME");

    String WRN_DISCONNECT_1=AdminWise.connectorManager.getString("constant.WRN_DISCONNECT")+
        VWConstant.NewLineChar+AdminWise.connectorManager.getString("constant.WRN_DISCONNECT_1")+" ";
    String WRN_DISCONNECT_2=" "+AdminWise.connectorManager.getString("constant.WRN_DISCONNECT_2");

   String WRN_DISCONNECT=AdminWise.connectorManager.getString("constant.WRN_DISCONNECT")+
        VWConstant.NewLineChar+
           AdminWise.connectorManager.getString("constant.WRN_DISCONNECT_3")+" ";

   String WRN_DISALLOW_1=AdminWise.connectorManager.getString("constant.WRN_DISALLOW_1")+" ";
   String WRN_DISALLOW_2=AdminWise.connectorManager.getString("constant.WRN_DISALLOW_2");

    //-----------------------------------------------------------
    //Auto Mail
    //-----------------------------------------------------------

    String LBL_SUBJECT_NAME=AdminWise.connectorManager.getString("constant.LBL_SUBJECT_NAME");
    String LBL_AVAILABLEINDEX_NAME=AdminWise.connectorManager.getString("constant.LBL_AVAILABLEINDEX_NAME");
    String LBL_AUTOMAILOPTIONS_NAME=AdminWise.connectorManager.getString("constant.LBL_AUTOMAILOPTIONS_NAME");

     //-----------------------------------------------------------
     //Lock Document
    //-----------------------------------------------------------
    String WRN_UNLOCKDOC_1=AdminWise.connectorManager.getString("constant.WRN_UNLOCKDOC_1")+
        VWConstant.NewLineChar+AdminWise.connectorManager.getString("constant.WRN_UNLOCKDOC_1_1")+" ";
    String WRN_UNLOCKDOC_2=" "+AdminWise.connectorManager.getString("constant.WRN_UNLOCKDOC_2");

    String WRN_UNLOCKDOCS=AdminWise.connectorManager.getString("constant.WRN_UNLOCKDOC_1")+
        VWConstant.NewLineChar+AdminWise.connectorManager.getString("constant.WRN_UNLOCKDOCS")+" ";

     //-----------------------------------------------------------
     //Backup Document
    //-----------------------------------------------------------
    String LBL_SIZE0_NAME="0 KB";
    String LBL_SIZEK_NAME=" KB";
    String LBL_SIZEM_NAME=" MB";

    String LBL_BACKAUPOPTIONS_NAME=AdminWise.connectorManager.getString("constant.LBL_BACKAUPOPTIONS_NAME");
    String LBL_OPTIONS =AdminWise.connectorManager.getString("constant.LBL_OPTIONS");

    String LBL_COPYTO_NAME=AdminWise.connectorManager.getString("constant.LBL_COPYTO_NAME");
    String LBL_STAGINGPATH_NAME=AdminWise.connectorManager.getString("constant.LBL_STAGINGPATH_NAME");
    String LBL_STORAGELOCATION_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGELOCATION_NAME");
    String LBL_STORAGELABEL_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGELABEL_NAME");
    String LBL_DOCCOPY_NAME=AdminWise.connectorManager.getString("constant.LBL_DOCCOPY_NAME");
    String LBL_DOCMOVE_NAME=AdminWise.connectorManager.getString("constant.LBL_DOCMOVE_NAME");
    ////String LBL_Extended_NAME="Extended Retrieval";
    String LBL_PATH_NAME=AdminWise.connectorManager.getString("constant.LBL_PATH_NAME");
    String LBL_UNCPATH_NAME=AdminWise.connectorManager.getString("constant.LBL_UNCPATH_NAME");
    String LBL_EXPLORER_NAME=ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+ AdminWise.connectorManager.getString("constant.LBL_EXPLORER_NAME");
    String LBL_STORAGENAMES_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGENAMES_NAME");
    String LBL_STORAGEREFRESH_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGEREFRESH_NAME");
    String LBL_STORAGE_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGE_NAME");
    String LBL_BACKUPINFO_NAME=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ AdminWise.connectorManager.getString("constant.LBL_BACKUPINFO_NAME");

    //-----------------------------------------------------------
     //Archive
    //-----------------------------------------------------------

    String Err_Path_NotWritable_1=AdminWise.connectorManager.getString("constant.Err_Path_NotWritable_1")+" ";
    String Err_Path_NotWritable_2=" "+AdminWise.connectorManager.getString("constant.Err_Path_NotWritable_2");

    String Wrn_Locked_Copy=AdminWise.connectorManager.getString("constant.Wrn_Locked_Copy")+" "+ VWConstant.NewLineChar +
    		AdminWise.connectorManager.getString("constant.Wrn_Locked_Copy_1");
    String Wrn_overwrite_1=AdminWise.connectorManager.getString("constant.Wrn_overwrite_1")+" ";
    String Wrn_overwrite_2=" "+VWConstant.NewLineChar +AdminWise.connectorManager.getString("constant.Wrn_overwrite_2")+" ";
    String Wrn_overwrite_3=VWConstant.NewLineChar +AdminWise.connectorManager.getString("constant.Wrn_overwrite_3") ;
    String Wrn_Locked_Archive=AdminWise.connectorManager.getString("constant.Wrn_Locked_Archive")+" "+ VWConstant.NewLineChar +
    		AdminWise.connectorManager.getString("constant.Wrn_Locked_Archive_1");

    String Msg_Move_Success_1=AdminWise.connectorManager.getString("constant.Msg_Move_Success_1");
    String Msg_Move_Success_2=AdminWise.connectorManager.getString("constant.Msg_Move_Success_2");
    String Msg_Copy_Success_1=AdminWise.connectorManager.getString("constant.Msg_Move_Success_1");
    String Msg_Copy_Success_2=AdminWise.connectorManager.getString("constant.Msg_Copy_Success_2");
    String Msg_Move_Confirm=AdminWise.connectorManager.getString("constant.Msg_Move_Confirm");

    String LBL_STORAGESERVER_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGESERVER_NAME");

    String LBL_STORAGE_LOCATION_NAME=AdminWise.connectorManager.getString("constant.LBL_STORAGE_LOCATION_NAME");
    String LBL_PORT_NAME=AdminWise.connectorManager.getString("constant.LBL_PORT_NAME");
    String LBL_SERVER_NAME=AdminWise.connectorManager.getString("constant.LBL_SERVER_NAME");
    String LBL_SELECT_NAME=AdminWise.connectorManager.getString("constant.LBL_SELECT_NAME");
     String LBL_MOVE_NAME=AdminWise.connectorManager.getString("constant.LBL_MOVE_NAME")+" ";
     String LBL_COPY_NAME=AdminWise.connectorManager.getString("constant.LBL_COPY_NAME")+" ";
     String LBL_STORAGELOCATION=AdminWise.connectorManager.getString("constant.LBL_STORAGELOCATION");

    String Progress_Copy=AdminWise.connectorManager.getString("constant.Progress_Copy")+" ";
    String Progress_Move=AdminWise.connectorManager.getString("constant.Progress_Move")+" ";

    //-----------------------------------------------------------
     //Audit Trail
    //-----------------------------------------------------------

    String LBL_AT_NAME=AdminWise.connectorManager.getString("constant.LBL_AT_NAME");
    String LBL_ATSETTING_NAME=AdminWise.connectorManager.getString("constant.LBL_ATSETTING_NAME");
    String LBL_OBJECT_NAME=AdminWise.connectorManager.getString("constant.LBL_OBJECT_NAME");
    String LBL_EVENT_NAME=AdminWise.connectorManager.getString("constant.LBL_EVENT_NAME");
    String LBL_USERNAME_NAME=AdminWise.connectorManager.getString("constant.LBL_USERNAME_NAME");
    String LBL_CLIENTIP_NAME=AdminWise.connectorManager.getString("constant.LBL_CLIENTIP_NAME");
    String LBL_DATEFROM_NAME=AdminWise.connectorManager.getString("constant.LBL_DATEFROM_NAME");
    String LBL_DATETO_NAME=AdminWise.connectorManager.getString("constant.LBL_DATETO_NAME");
    String LBL_RETENTION_PERIOD_NAME=AdminWise.connectorManager.getString("constant.LBL_DATETO_NAME");
    String LBL_CHUNKSIZE_NAME=AdminWise.connectorManager.getString("constant.LBL_CHUNKSIZE_NAME");
    String LBL_OBJECT_EVENT_NAME=AdminWise.connectorManager.getString("constant.LBL_OBJECT_EVENT_NAME");
    String LBL_USER_CLIENT_NAME=AdminWise.connectorManager.getString("constant.LBL_USER_CLIENT_NAME");
    String LBL_DATE_NAME=AdminWise.connectorManager.getString("constant.LBL_DATE_NAME");
    String LBL_ARCHIVE_NAME=AdminWise.connectorManager.getString("constant.LBL_ARCHIVE_NAME")+" ";
    String LBL_ALLUSERS_NAME=AdminWise.connectorManager.getString("constant.LBL_ALLUSERS_NAME");
    String LBL_DATACHUNK_NAME=AdminWise.connectorManager.getString("constant.LBL_DATACHUNK_NAME");


    String MSG_DELETEINDEX=AdminWise.connectorManager.getString("constant.MSG_DELETEINDEX")+" ";
    String MSG_UNUSEDINDEX_1=AdminWise.connectorManager.getString("constant.MSG_UNUSEDINDEX_1");
    String MSG_UNUSEDINDEX_2=AdminWise.connectorManager.getString("constant.MSG_UNUSEDINDEX_2");
    String MSG_UNUSEDINDEX_3=AdminWise.connectorManager.getString("constant.MSG_UNUSEDINDEX_3");

    String MSG_CLEARINDICES=AdminWise.connectorManager.getString("constant.MSG_CLEARINDICES");

    String CHK_SIC_NAME=AdminWise.connectorManager.getString("constant.CHK_SIC_NAME");

    String LBL_INDICESLIST_NAME="  "+AdminWise.connectorManager.getString("constant.LBL_INDICESLIST_NAME");
    String LBL_INDEXPROP_NAME="  "+AdminWise.connectorManager.getString("constant.LBL_INDEXPROP_NAME");
    String LBL_REFRESH_NAME=AdminWise.connectorManager.getString("constant.LBL_REFRESH_NAME");
    String LBL_VRSETTING_NAME=AdminWise.connectorManager.getString("constant.LBL_VRSETTING_NAME");
    String LBL_RETENTIONSETTING_NAME=AdminWise.connectorManager.getString("constant.LBL_RETENTIONSETTING_NAME");
    String TOOLTIP_INDEXNEW=AdminWise.connectorManager.getString("constant.TOOLTIP_INDEXNEW");
    String TOOLTIP_INDEXADD=AdminWise.connectorManager.getString("constant.TOOLTIP_INDEXADD");
    String TOOLTIP_INDEXDEL=AdminWise.connectorManager.getString("constant.TOOLTIP_INDEXDEL");
    String TOOLTIP_INDEXCLEAR=AdminWise.connectorManager.getString("constant.TOOLTIP_INDEXCLEAR");
    String TOOLTIP_INDEXUP=AdminWise.connectorManager.getString("constant.TOOLTIP_INDEXUP");
    String TOOLTIP_INDEXDOWN=AdminWise.connectorManager.getString("constant.TOOLTIP_INDEXDOWN");
    String LBL_ADDINDEX_NAME=AdminWise.connectorManager.getString("constant.LBL_ADDINDEX_NAME");
    String LBL_LISTINDEX_NAME=AdminWise.connectorManager.getString("constant.LBL_LISTINDEX_NAME");
    String LBL_SELECTIONINDEXVALUES_NAME=AdminWise.connectorManager.getString("constant.LBL_SELECTIONINDEXVALUES_NAME");

    String LBL_IndexerInfo=Constants.PRODUCT_NAME + " add document pages to indexer queue";
    String LBL_ReadDocInfo=AdminWise.connectorManager.getString("constant.LBL_ReadDocInfo");

    String LBL_INITVER=AdminWise.connectorManager.getString("constant.LBL_INITVER");
    String LBL_INITREV=AdminWise.connectorManager.getString("constant.LBL_INITREV");
    String LBL_INCVER=AdminWise.connectorManager.getString("constant.LBL_INCVER");
    String LBL_INCREV=AdminWise.connectorManager.getString("constant.LBL_INCREV");
    String LBL_SETTING=AdminWise.connectorManager.getString("constant.LBL_SETTING");
    String LBL_OPTION_1=AdminWise.connectorManager.getString("constant.LBL_OPTION_1");
    String LBL_OPTION_2=AdminWise.connectorManager.getString("constant.LBL_OPTION_2");
    String LBL_OPTION_3=AdminWise.connectorManager.getString("constant.LBL_OPTION_3");
    String LBL_VERREV_ENABLE=AdminWise.connectorManager.getString("constant.LBL_VERREV_ENABLE");
    /*
     * Retention setting dialog labels - Valli
     */
    String LBL_RETENTION_ENABLE=AdminWise.connectorManager.getString("constant.LBL_RETENTION_ENABLE");
    String LBL_DOCEXPAFT=AdminWise.connectorManager.getString("constant..LBL_DOCEXPAFT");
    String LBL_DAYS=AdminWise.connectorManager.getString("constant.LBL_DAYS");
    String LBL_MOVETO=AdminWise.connectorManager.getString("constant.LBL_MOVETO");
    String LBL_SAVEIN=AdminWise.connectorManager.getString("constant.LBL_SAVEIN");
	String LBL_DOCEXPSTARTS=AdminWise.connectorManager.getString("constant.LBL_DOCEXPSTARTS");
	String LBL_DISPOSALACTIONS=AdminWise.connectorManager.getString("constant.LBL_DISPOSALACTIONS");
	String CHK_INCLUDE_SECURITY=AdminWise.connectorManager.getString("constant.CHK_INCLUDE_SECURITY");
	String CHK_INCLUDE_SECURITY_TOOLTIP=AdminWise.connectorManager.getString("constant.CHK_INCLUDE_SECURITY_TOOLTIP");
	String CHK_INCLUDE_NODE_PROPERTIES=AdminWise.connectorManager.getString("constant.CHK_INCLUDE_NODE_PROPERTIES");
	String CHK_INCLUDE_RETENTION=AdminWise.connectorManager.getString("constant.CHK_INCLUDE_RETENTION");
	String CHK_INCLUDE_NOTIFICATION=AdminWise.connectorManager.getString("constant.CHK_INCLUDE_NOTIFICATION");
	String CHK_INCLUDE_NODE_PROPERTIES_TOOLTIP=AdminWise.connectorManager.getString("constant.CHK_INCLUDE_NODE_PROPERTIES_TOOLTIP");

    //--------------------------Table Header------------------
    //Template
    //-----------------------------------------------------------
        String[] TemplateColumnNames={AdminWise.connectorManager.getString("constant.TemplateColumnNames0"),AdminWise.connectorManager.getString("constant.TemplateColumnNames1"),
        		AdminWise.connectorManager.getString("constant.TemplateColumnNames2"),AdminWise.connectorManager.getString("constant.TemplateColumnNames3")};
    //-----------------------------------------------------------
    //Statistics
    //-----------------------------------------------------------
String[][] Statistic_Names_Old={
        {"Server seats","0"},
        {"Desktop Concurrent seats","0"},
        {"Desktop Named seats","0"},
        {"Web Top Concurrent seats","0"},
        {"Web Top Named seats","0"},
        {"Web Lite Concurrent seats","0"},
        {"Web Lite Named seats","0"},
        {"Administration seats","0"},
        {"AIP seats","0"},
        {"EAS seats","0"},
        {"Indexer seats","0"},
        {"ARS seats","0"},
        //{"Other seats","0"},
        {"SDK seats","0"},
        {"DWS seats","0"},
        {"Web Access seats","0"},
        {"Notification seats","0"},
        {"Number Of Folders","0"},
        {"Number Of Documents","0"},
        {"Number Of Document Types","0"},
        {"Number Of Index Fields","0"},
        {"Number Of Document Indices","0"},
        {"Number Of Storage Locations","0"},
        {"Database Version","0"},
        //{"GroupWise Connector","Disable"},
        {"Indexer Server","Disable"},
        {"Script Version","0"}};
        String[] StatisticColumnNames={AdminWise.connectorManager.getString("constant.StatisticColumnNames0"),AdminWise.connectorManager.getString("constant.StatisticColumnNames1")};
        String[] StatisticColumnWidths={"50","50"};
        
        String[] SecureLinkColumnNames={AdminWise.connectorManager.getString("constant.SecureLinkColumnNames0"),
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames1"), 
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames2"), 
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames3"), 
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames4"),
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames5"),
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames6"),
        		AdminWise.connectorManager.getString("constant.SecureLinkColumnNames7")};
        String[] SecureLinkColumnWidths={"0","30","30","20","20","20","20","0"};
        
        String[][] Statistic_Names = {
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_0"), "0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_1"), "0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_2"), "0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_3"), "0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_4"), "0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_5"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_6"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_7"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_8"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_9"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_10"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_11"),"0"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_12"),"Disable"},
        		{AdminWise.connectorManager.getString("constant.Statistic_Names_13"),"0"}
        };
    //-----------------------------------------------------------
    //Restore
    //-----------------------------------------------------------
        String[] RestoreColumnNames = {AdminWise.connectorManager.getString("constant.RestoreColumnNames0"),AdminWise.connectorManager.getString("constant.RestoreColumnNames1"),
        		AdminWise.connectorManager.getString("constant.RestoreColumnNames2"),AdminWise.connectorManager.getString("constant.RestoreColumnNames3")};
    String[] ROptionColumnNames = {AdminWise.connectorManager.getString("constant.ROptionColumnNames0"),AdminWise.connectorManager.getString("constant.ROptionColumnNames1")};
    String[] RestoreType={AdminWise.connectorManager.getString("constant.RestoreType0"),AdminWise.connectorManager.getString("constant.RestoreType1"),
    		AdminWise.connectorManager.getString("constant.RestoreType2"),
    		AdminWise.connectorManager.getString("constant.RestoreType3")};
    String[] RestoreSequenceSeed={AdminWise.connectorManager.getString("constant.RestoreSequenceSeed0"),AdminWise.connectorManager.getString("constant.RestoreSequenceSeed1")};
    String[][] restoreOptions={{AdminWise.connectorManager.getString("constant.restoreOptions0"),""},
        {AdminWise.connectorManager.getString("constant.restoreOptions1"),AdminWise.connectorManager.getString("constant.restoreOptions2")},
        {AdminWise.connectorManager.getString("constant.restoreOptions3"),"Yes"},
        {AdminWise.connectorManager.getString("constant.restoreOptions4"),"Yes"},
        {AdminWise.connectorManager.getString("constant.restoreOptions5"),"Yes"},
        {AdminWise.connectorManager.getString("constant.restoreOptions6"),AdminWise.connectorManager.getString("constant.restoreOptions7")},
        {AdminWise.connectorManager.getString("constant.restoreOptions8"),"No"},
//      Added By Mallikarjuna to Restore Created and Modifed Dates   
        {AdminWise.connectorManager.getString("constant.restoreOptions9"), "No"}
        };
    //-----------------------------------------------------------
    //Redactions
    //-----------------------------------------------------------
    String[] RedactionsColumnNames={AdminWise.connectorManager.getString("constant.RedactionsColumnNames0"),AdminWise.connectorManager.getString("constant.RedactionsColumnNames1"),
    		AdminWise.connectorManager.getString("constant.RedactionsColumnNames2"),AdminWise.connectorManager.getString("constant.RedactionsColumnNames3")};
    String[] RedactionsColumnWidths={"20","20","20","20","20"};
    //-----------------------------------------------------------
    //Recycle
    //-----------------------------------------------------------
    String[] RecycleColumnNames={AdminWise.connectorManager.getString("constant.RecycleColumnNames0"),AdminWise.connectorManager.getString("constant.RecycleColumnNames1"),
    		AdminWise.connectorManager.getString("constant.RecycleColumnNames2"),AdminWise.connectorManager.getString("constant.RecycleColumnNames3"),AdminWise.connectorManager.getString("constant.RecycleColumnNames4")};
    String[] RecycleColumnWidths={"20","15","25","10","10","10"};

    //-----------------------------------------------------------
    //Signature
    //-----------------------------------------------------------

    String[] SignatureColumnNames={AdminWise.connectorManager.getString("constant.SignatureColumnNames0"),AdminWise.connectorManager.getString("constant.SignatureColumnNames1"),
    		AdminWise.connectorManager.getString("constant.SignatureColumnNames2"),AdminWise.connectorManager.getString("constant.SignatureColumnNames3")};
    String[] SignatureColumnWidths={"40","10","25","25"};

    String[] SignColumnNames={"No.",AdminWise.connectorManager.getString("constant.SignColumnNames1")};
    String[] SignColumnWidths={"10","90"};
    String[] SignType={AdminWise.connectorManager.getString("constant.SignType_1"),AdminWise.connectorManager.getString("constant.SignType_2")
    		,AdminWise.connectorManager.getString("constant.SignType_3")};

    String Signatures[]={AdminWise.connectorManager.getString("constant.Signatures0"),AdminWise.connectorManager.getString("constant.Signatures1"),
    		AdminWise.connectorManager.getString("constant.Signatures2"),AdminWise.connectorManager.getString("constant.Signatures3"),AdminWise.connectorManager.getString("constant.Signatures4"),
    		AdminWise.connectorManager.getString("constant.Signatures5"),AdminWise.connectorManager.getString("constant.Signatures6")};
    //-----------------------------------------------------------
    //Options
    //-----------------------------------------------------------
    	//CV10.1 Commented the option2,option5,option18,option19,option20 as it is not used.
        String[][] options={{AdminWise.connectorManager.getString("constant.options0"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options1"),"Yes"},
       // {AdminWise.connectorManager.getString("constant.options2"),"Yes"},
		
        {AdminWise.connectorManager.getString("constant.options3"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options4"),"Yes"},
        //{AdminWise.connectorManager.getString("constant.options5"),"Yes"},
		
        {AdminWise.connectorManager.getString("constant.options6"),"1000"},
        {AdminWise.connectorManager.getString("constant.options7"),"1000"},
        {AdminWise.connectorManager.getString("constant.options8"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options9"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options10"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options11"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options12"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options13"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options14"),"Yes"},
        //{AdminWise.connectorManager.getString("constant.options15"),"Yes"},
		
        {AdminWise.connectorManager.getString("constant.options16"),"Yes"},
        {AdminWise.connectorManager.getString("constant.options17"),"Yes"},
       // {AdminWise.connectorManager.getString("constant.options18"),"No"},
		
       // {AdminWise.connectorManager.getString("constant.options19"),""},
		
        //{AdminWise.connectorManager.getString("constant.options20"),""}
		
    };
    String[][] ATOptions={{AdminWise.connectorManager.getString("constant.ATOptions"),""}/*, {"MaximumNoOfLines",""}*/};
    String[] OptionColumnNames = {AdminWise.connectorManager.getString("constant.OptionColumnNames0"),AdminWise.connectorManager.getString("constant.OptionColumnNames1")};

    //-----------------------------------------------------------
     //Con User
    //-----------------------------------------------------------

    /* Issue no - 672
     * Issue Description -  Add a column showing time and date connected be added to the connected user tab.
     * 						Add a column showing the session id of the connected user be added to the connected user tab.
     * Developer - Nebu Alex 
     */
    String[] ConUserColNames={AdminWise.connectorManager.getString("constant.ConUserColNames0"),AdminWise.connectorManager.getString("constant.ConUserColNames1"),AdminWise.connectorManager.getString("constant.ConUserColNames2"),
    		AdminWise.connectorManager.getString("constant.ConUserColNames3"),AdminWise.connectorManager.getString("constant.ConUserColNames4")};
    String[] ConUserColWidths={"20","20","20","20","20"};
    // End of Issue 672
    //-----------------------------------------------------------
     //Auto Mail
    //-----------------------------------------------------------
    String[][] autoMailNames={{AdminWise.connectorManager.getString("constant.autoMailNames0"),"","0"},
        {AdminWise.connectorManager.getString("constant.autoMailNames1"),"","0"},
        {AdminWise.connectorManager.getString("constant.autoMailNames2"),"","0"}
    };

    //-----------------------------------------------------------
     //Lock Document
    //-----------------------------------------------------------
    String[] LockColNames={AdminWise.connectorManager.getString("constant.LockColNames0"),AdminWise.connectorManager.getString("constant.LockColNames1"),
    		AdminWise.connectorManager.getString("constant.LockColNames2"),AdminWise.connectorManager.getString("constant.LockColNames3"),AdminWise.connectorManager.getString("constant.LockColNames4")};
    String[] LockColWidths={"20","20","20","20","20"};

    //-----------------------------------------------------------
     //Backup
    //-----------------------------------------------------------
    String[] BackupColumnNames = {AdminWise.connectorManager.getString("constant.BackupColumnNames0"),AdminWise.connectorManager.getString("constant.BackupColumnNames1"),AdminWise.connectorManager.getString("constant.BackupColumnNames2"),
    		AdminWise.connectorManager.getString("constant.BackupColumnNames3"),AdminWise.connectorManager.getString("constant.BackupColumnNames4"),
    		AdminWise.connectorManager.getString("constant.BackupColumnNames5"),AdminWise.connectorManager.getString("constant.BackupColumnNames6"),AdminWise.connectorManager.getString("constant.BackupColumnNames7")};
    String[] BOptionColumnNames = {AdminWise.connectorManager.getString("constant.BOptionColumnNames0"),AdminWise.connectorManager.getString("constant.BOptionColumnNames1")};

    String[] backupErr={"","",AdminWise.connectorManager.getString("constant.backupErr0"),AdminWise.connectorManager.getString("constant.backupErr1"),AdminWise.connectorManager.getString("constant.backupErr2"),
    		AdminWise.connectorManager.getString("constant.backupErr3"),AdminWise.connectorManager.getString("constant.backupErr4"),AdminWise.connectorManager.getString("constant.backupErr5")};
    String[][] backupOptions={{AdminWise.connectorManager.getString("constant.backupOptions0"),""},
        {AdminWise.connectorManager.getString("constant.backupOptions1"),"Copy"},
        {AdminWise.connectorManager.getString("constant.backupOptions2"),"No"},
        {AdminWise.connectorManager.getString("constant.backupOptions3"),"No"},
        {AdminWise.connectorManager.getString("constant.backupOptions4"),"No"},
        {AdminWise.connectorManager.getString("constant.backupOptions5"),"No"},
        {AdminWise.connectorManager.getString("backupOptions.Withmini")+" "+ ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+AdminWise.connectorManager.getString("backupOptions.viewer="),"No"},
        //{"With unencrypted document pages","No"},
        /*Issue No - 664
         * Issue Description -The feature removes the Document Encryption Key
         *  that can be applied at room creation time, not the page level or 
         *  'default' encryption.  The option should state something to the
         *   effect of 'remove Document Encryption Key (if set)' 
         * Developer - Nebu Alex
         * **/
        {AdminWise.connectorManager.getString("constant.backupOptions6"),"No"},
        // End of fix
        {AdminWise.connectorManager.getString("constant.backupOptions7"),"No"},
        };
    String[] BackupType={AdminWise.connectorManager.getString("constant.BackupType0"),AdminWise.connectorManager.getString("constant.BackupType1")};

    //-----------------------------------------------------------
     //Archive
    //-----------------------------------------------------------
    String[] optionsPC={AdminWise.connectorManager.getString("constant.optionsPC0"),AdminWise.connectorManager.getString("constant.optionsPC1")};
    String[] optionsYANA={"Yes","Yes to All","No","No to All"};
    String[] ArchiveColumnNames = {AdminWise.connectorManager.getString("constant.ArchiveColumnNames0"),AdminWise.connectorManager.getString("constant.ArchiveColumnNames1"),AdminWise.connectorManager.getString("constant.ArchiveColumnNames2"),
    		AdminWise.connectorManager.getString("constant.ArchiveColumnNames3"),AdminWise.connectorManager.getString("constant.ArchiveColumnNames4")};

    //-----------------------------------------------------------
     //Audit Trail
    //-----------------------------------------------------------
    String[] ATColumnNames = {AdminWise.connectorManager.getString("atTable.ATColumnNames0"),AdminWise.connectorManager.getString("atTable.ATColumnNames1"),AdminWise.connectorManager.getString("atTable.ATColumnNames2"),AdminWise.connectorManager.getString("atTable.ATColumnNames3"),AdminWise.connectorManager.getString("atTable.ATColumnNames4"),AdminWise.connectorManager.getString("atTable.ATColumnNames5"),
    		AdminWise.connectorManager.getString("atTable.ATColumnNames6"),AdminWise.connectorManager.getString("atTable.ATColumnNames7")};

    String[] ATColumnWidths = {"20","10","20","10","10","10","10","10"};
    VWRecord[] ATActions={
    new VWRecord(8,AdminWise.connectorManager.getString("constant.ATActions0")),
    new VWRecord(1,AdminWise.connectorManager.getString("constant.ATActions1")),
    new VWRecord(2,AdminWise.connectorManager.getString("constant.ATActions2")),
    new VWRecord(3,AdminWise.connectorManager.getString("constant.ATActions3")),
    new VWRecord(10,AdminWise.connectorManager.getString("constant.ATActions4")),
    new VWRecord(4,AdminWise.connectorManager.getString("constant.ATActions5")),
    new VWRecord(5,AdminWise.connectorManager.getString("constant.ATActions6")),
    new VWRecord(9,AdminWise.connectorManager.getString("constant.ATActions7")),
    new VWRecord(6,AdminWise.connectorManager.getString("constant.ATActions8")),
    new VWRecord(33,AdminWise.connectorManager.getString("constant.ATActions9")),
    new VWRecord(7,AdminWise.connectorManager.getString("constant.ATActions10")),
    new VWRecord(34,AdminWise.connectorManager.getString("constant.ATActions11")),
    /*new VWRecord(11,"Fax"),*/
    new VWRecord(12,AdminWise.connectorManager.getString("constant.ATActions12")),
    new VWRecord(13,AdminWise.connectorManager.getString("constant.ATActions13")),
    new VWRecord(14,AdminWise.connectorManager.getString("constant.ATActions14")),
    new VWRecord(15,AdminWise.connectorManager.getString("constant.ATActions15")),
    new VWRecord(16,AdminWise.connectorManager.getString("constant.ATActions16")),
    new VWRecord(17,AdminWise.connectorManager.getString("constant.ATActions17")),
    new VWRecord(18,AdminWise.connectorManager.getString("constant.ATActions18")),
    new VWRecord(19,AdminWise.connectorManager.getString("constant.ATActions19")),
    new VWRecord(20,AdminWise.connectorManager.getString("constant.ATActions20")),
    new VWRecord(21,AdminWise.connectorManager.getString("constant.ATActions21")),
    new VWRecord(22,AdminWise.connectorManager.getString("constant.ATActions22")),
    new VWRecord(23,AdminWise.connectorManager.getString("constant.ATActions23")),
    new VWRecord(24,AdminWise.connectorManager.getString("constant.ATActions24")),
    new VWRecord(25,AdminWise.connectorManager.getString("constant.ATActions25")),
    new VWRecord(26,AdminWise.connectorManager.getString("constant.ATActions26")),
    /*new VWRecord(27,"Cancel checkin"),*/
    new VWRecord(28,AdminWise.connectorManager.getString("constant.ATActions27")),
    new VWRecord(29,AdminWise.connectorManager.getString("constant.ATActions28")),
    new VWRecord(30,AdminWise.connectorManager.getString("constant.ATActions29")),
    new VWRecord(31,AdminWise.connectorManager.getString("constant.ATActions30")),
    new VWRecord(32,AdminWise.connectorManager.getString("constant.ATActions31")),
    new VWRecord(35,AdminWise.connectorManager.getString("constant.ATActions32")),
    new VWRecord(36,AdminWise.connectorManager.getString("constant.ATActions33")),
    new VWRecord(37,AdminWise.connectorManager.getString("constant.ATActions34")),
    new VWRecord(38,AdminWise.connectorManager.getString("constant.ATActions35")),
    new VWRecord(40,AdminWise.connectorManager.getString("constant.ATActions36")),
    new VWRecord(41,AdminWise.connectorManager.getString("constant.ATActions37")),

    new VWRecord(48,AdminWise.connectorManager.getString("constant.ATActions38")),
    new VWRecord(49,AdminWise.connectorManager.getString("constant.ATActions39")),
    new VWRecord(50,AdminWise.connectorManager.getString("constant.ATActions40")),
    new VWRecord(42,AdminWise.connectorManager.getString("constant.ATActions41")),
	/* Purpose:  Added to catch Freeze and UnFreeze document events in Audit
	 * Name:	C.Shanmugavalli		Date:	11 Sep 2006
	 */
	new VWRecord(43,AdminWise.connectorManager.getString("constant.ATActions42")),
	new VWRecord(44,AdminWise.connectorManager.getString("constant.ATActions43")),
	/* Purpose:  Audit Policy creation, modification and deletion
	 */

	new VWRecord(51,AdminWise.connectorManager.getString("constant.ATActions44")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
	new VWRecord(52,AdminWise.connectorManager.getString("constant.ATActions45")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
	new VWRecord(53,AdminWise.connectorManager.getString("constant.ATActions46")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
    new VWRecord(45,AdminWise.connectorManager.getString("constant.ATActions47")),
	new VWRecord(46,AdminWise.connectorManager.getString("constant.ATActions48")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
	new VWRecord(47,AdminWise.connectorManager.getString("constant.ATActions49")),
    new VWRecord(55,AdminWise.connectorManager.getString("constant.ATActions50")),
    new VWRecord(56,AdminWise.connectorManager.getString("constant.ATActions51")),
    new VWRecord(57,AdminWise.connectorManager.getString("constant.ATActions52")),
    new VWRecord(58,AdminWise.connectorManager.getString("constant.ATActions53")),
    new VWRecord(59,AdminWise.connectorManager.getString("constant.ATActions54")),
    new VWRecord(60,AdminWise.connectorManager.getString("constant.ATActions55")),
    new VWRecord(61,AdminWise.connectorManager.getString("constant.ATActions56")),
    new VWRecord(62,AdminWise.connectorManager.getString("constant.ATActions57")),
    new VWRecord(63,AdminWise.connectorManager.getString("constant.ATActions58")),
    new VWRecord(64,AdminWise.connectorManager.getString("constant.ATActions59")),
    new VWRecord(65,AdminWise.connectorManager.getString("constant.ATActions60")),
    new VWRecord(66,AdminWise.connectorManager.getString("constant.ATActions61")),
    new VWRecord(67,AdminWise.connectorManager.getString("constant.ATActions62")),
    new VWRecord(68,AdminWise.connectorManager.getString("constant.ATActions63")),
    new VWRecord(69,AdminWise.connectorManager.getString("constant.ATActions64")),
    new VWRecord(70,AdminWise.connectorManager.getString("constant.ATActions65")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"))
	//If any changes in DRS audit catch options - need to change in VWCheckList also for master switch off - Valli 
	
    };
    String[] ATStrActions={"",AdminWise.connectorManager.getString("constant.ATStrActions0"),AdminWise.connectorManager.getString("constant.ATStrActions1"),
    		AdminWise.connectorManager.getString("constant.ATStrActions2"),AdminWise.connectorManager.getString("constant.ATStrActions3"),AdminWise.connectorManager.getString("constant.ATStrActions4"),
    		AdminWise.connectorManager.getString("constant.ATStrActions5"),AdminWise.connectorManager.getString("constant.ATStrActions6"),AdminWise.connectorManager.getString("constant.ATStrActions7"),
    		AdminWise.connectorManager.getString("constant.ATStrActions8"),AdminWise.connectorManager.getString("constant.ATStrActions9"),"",AdminWise.connectorManager.getString("constant.ATStrActions10"),
    		AdminWise.connectorManager.getString("constant.ATStrActions11"),AdminWise.connectorManager.getString("constant.ATStrActions12"),AdminWise.connectorManager.getString("constant.ATStrActions13"),AdminWise.connectorManager.getString("constant.ATStrActions14"),
    		AdminWise.connectorManager.getString("constant.ATStrActions15"),AdminWise.connectorManager.getString("constant.ATStrActions16"),AdminWise.connectorManager.getString("constant.ATStrActions17"),AdminWise.connectorManager.getString("constant.ATStrActions18"),
    		AdminWise.connectorManager.getString("constant.ATStrActions19"),AdminWise.connectorManager.getString("constant.ATStrActions20"),AdminWise.connectorManager.getString("constant.ATStrActions21"),AdminWise.connectorManager.getString("constant.ATStrActions22"),AdminWise.connectorManager.getString("constant.ATStrActions23"),
    		AdminWise.connectorManager.getString("constant.ATStrActions24"),AdminWise.connectorManager.getString("constant.ATStrActions25"),AdminWise.connectorManager.getString("constant.ATStrActions26"),AdminWise.connectorManager.getString("constant.ATStrActions27"),
    		AdminWise.connectorManager.getString("constant.ATStrActions28"),AdminWise.connectorManager.getString("constant.ATStrActions29"),AdminWise.connectorManager.getString("constant.ATStrActions30"),AdminWise.connectorManager.getString("constant.ATStrActions31"),
    		AdminWise.connectorManager.getString("constant.ATStrActions32"),AdminWise.connectorManager.getString("constant.ATStrActions33"),AdminWise.connectorManager.getString("constant.ATStrActions34"),AdminWise.connectorManager.getString("constant.ATStrActions35"),
    		AdminWise.connectorManager.getString("constant.ATStrActions36"),"",AdminWise.connectorManager.getString("constant.ATStrActions37"),AdminWise.connectorManager.getString("constant.ATStrActions38"),AdminWise.connectorManager.getString("constant.ATStrActions39"),AdminWise.connectorManager.getString("constant.ATStrActions40"),
    		AdminWise.connectorManager.getString("constant.ATStrActions41"),AdminWise.connectorManager.getString("constant.ATStrActions42"), AdminWise.connectorManager.getString("VWUtil/VWCheckList.listData_3")+" "+AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME").toLowerCase(),AdminWise.connectorManager.getString("constant.ATStrActions43"),AdminWise.connectorManager.getString("constant.ATStrActions44"),AdminWise.connectorManager.getString("constant.ATStrActions45"), AdminWise.connectorManager.getString("constant.ATStrActions46"), AdminWise.connectorManager.getString("constant.BTN_NEW_NAME")+" "+AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME"), AdminWise.connectorManager.getString("constant.BTN_UPDATE_NAME")+" "+AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME"), AdminWise.connectorManager.getString("constant.BTN_SYNC_NAME")+" "+AdminWise.connectorManager.getString("VWAdminWise.WORKFLOW_MODULE_NAME"),"",
    		AdminWise.connectorManager.getString("constant.ATStrActions55"),AdminWise.connectorManager.getString("constant.ATStrActions56"),AdminWise.connectorManager.getString("constant.ATStrActions57"),AdminWise.connectorManager.getString("constant.ATStrActions58"),AdminWise.connectorManager.getString("constant.ATStrActions59"),
    		AdminWise.connectorManager.getString("constant.ATStrActions60"),AdminWise.connectorManager.getString("constant.ATStrActions61"),
    		AdminWise.connectorManager.getString("constant.ATStrActions62"),AdminWise.connectorManager.getString("constant.ATStrActions63"),
    		AdminWise.connectorManager.getString("constant.ATStrActions64"),AdminWise.connectorManager.getString("constant.ATStrActions65"),
    		AdminWise.connectorManager.getString("constant.ATStrActions66"),AdminWise.connectorManager.getString("constant.ATStrActions67"),AdminWise.connectorManager.getString("constant.ATStrActions68"),AdminWise.connectorManager.getString("constant.ATStrActions69"),AdminWise.connectorManager.getString("constant.ATActions65")};

    VWRecord[] ATObjects={new VWRecord(0,AdminWise.connectorManager.getString("constant.ATObjects0")),new VWRecord(2,AdminWise.connectorManager.getString("constant.ATObjects2")),new VWRecord(3,AdminWise.connectorManager.getString("constant.ATObjects3")),
        new VWRecord(4,AdminWise.connectorManager.getString("constant.ATObjects4")), new VWRecord(5,AdminWise.connectorManager.getString("constant.ATObjects5")),
        new VWRecord(6,AdminWise.connectorManager.getString("constant.ATObjects6")), new VWRecord(7,AdminWise.connectorManager.getString("constant.ATObjects7")), new VWRecord(8,AdminWise.connectorManager.getString("constant.ATObjects8")), new VWRecord(9,AdminWise.connectorManager.getString("constant.ATObjects9")), new VWRecord(10,ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),new VWRecord(11,AdminWise.connectorManager.getString("constant.ATObjects11"))};
    String[] ATStrObjects={"","",AdminWise.connectorManager.getString("constant.ATStrObjects2"),AdminWise.connectorManager.getString("constant.ATStrObjects3"),AdminWise.connectorManager.getString("constant.ATStrObjects4"),AdminWise.connectorManager.getString("constant.ATStrObjects5"),
    		AdminWise.connectorManager.getString("constant.ATStrObjects6"),AdminWise.connectorManager.getString("constant.ATStrObjects7"),AdminWise.connectorManager.getString("constant.ATStrObjects8"),AdminWise.connectorManager.getString("constant.ATStrObjects9"), ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"),AdminWise.connectorManager.getString("constant.ATStrObjects11")};
    
    String allEvents = AdminWise.connectorManager.getString("constant.allEvents");
    String allObjects = AdminWise.connectorManager.getString("constant.allObjects");
    
    VWRecord[][] ATEvents={{new VWRecord(0,AdminWise.connectorManager.getString("constant.ATEvents0"))},
    {},
    {new VWRecord(0,AdminWise.connectorManager.getString("constant.ATEvents0")),
    new VWRecord(8,AdminWise.connectorManager.getString("constant.ATEvents8")),
    new VWRecord(1,AdminWise.connectorManager.getString("constant.ATEvents1")),
    new VWRecord(2,AdminWise.connectorManager.getString("constant.ATEvents2")),
    new VWRecord(3,AdminWise.connectorManager.getString("constant.ATEvents3")),
    new VWRecord(10,AdminWise.connectorManager.getString("constant.ATEvents10")),
    new VWRecord(4,AdminWise.connectorManager.getString("constant.ATEvents4")),
    new VWRecord(5,AdminWise.connectorManager.getString("constant.ATEvents5")),
    new VWRecord(6,AdminWise.connectorManager.getString("constant.ATEvents6")),
    new VWRecord(33,AdminWise.connectorManager.getString("constant.ATEvents33")),
    new VWRecord(7,AdminWise.connectorManager.getString("constant.ATEvents7")),
    new VWRecord(34,AdminWise.connectorManager.getString("constant.ATEvents34")),
    new VWRecord(9,AdminWise.connectorManager.getString("constant.ATEvents9")),
    /*new VWRecord(11,"Fax document"),*/
    new VWRecord(12,AdminWise.connectorManager.getString("constant.ATEvents12")),
    new VWRecord(13,AdminWise.connectorManager.getString("constant.ATEvents13")),
    new VWRecord(14,AdminWise.connectorManager.getString("constant.ATEvents14")),
    new VWRecord(15,AdminWise.connectorManager.getString("constant.ATEvents15")),
    new VWRecord(16,AdminWise.connectorManager.getString("constant.ATEvents16")),
    new VWRecord(26,AdminWise.connectorManager.getString("constant.ATEvents26")),
    /*new VWRecord(27,"Cancel checkin Document"),*/
    new VWRecord(17,AdminWise.connectorManager.getString("constant.ATEvents17")),
    new VWRecord(18,AdminWise.connectorManager.getString("constant.ATEvents18")),
    new VWRecord(19,AdminWise.connectorManager.getString("constant.ATEvents19")),
    new VWRecord(40,AdminWise.connectorManager.getString("constant.ATEvents40")),
     new VWRecord(41,AdminWise.connectorManager.getString("constant.ATEvents41")),
     new VWRecord(55,AdminWise.connectorManager.getString("constant.ATEvents55")),
     new VWRecord(56,AdminWise.connectorManager.getString("constant.ATEvents56")),
      new VWRecord(62,AdminWise.connectorManager.getString("constant.ATEvents62")),
      new VWRecord(63,AdminWise.connectorManager.getString("constant.ATEvents63")),	 
      new VWRecord(64,AdminWise.connectorManager.getString("constant.ATEvents64")),
      new VWRecord(65,AdminWise.connectorManager.getString("constant.ATEvents65")),
    new VWRecord(66,AdminWise.connectorManager.getString("constant.ATEvents66")),
    new VWRecord(67,AdminWise.connectorManager.getString("constant.ATEvents67")),
    new VWRecord(68,AdminWise.connectorManager.getString("constant.ATEvents68")),
    new VWRecord(69,AdminWise.connectorManager.getString("constant.ATEvents69"))
	 /*new VWRecord(45,"Document processed by DRS"),
	 new VWRecord(46,"Document sent in route"),
	 new VWRecord(47,"Document Approved/Rejected")*/},
    {new VWRecord(0,AdminWise.connectorManager.getString("constant.ATEvents0")),
    new VWRecord(21,AdminWise.connectorManager.getString("constant.ATEvents21")),
    new VWRecord(22,AdminWise.connectorManager.getString("constant.ATEvents22")),
    new VWRecord(23,AdminWise.connectorManager.getString("constant.ATEvents23")),
    new VWRecord(24,AdminWise.connectorManager.getString("constant.ATEvents24")),
    new VWRecord(25,AdminWise.connectorManager.getString("constant.ATEvents25")),
    new VWRecord(57,AdminWise.connectorManager.getString("constant.ATEvents57")),
    new VWRecord(58,AdminWise.connectorManager.getString("constant.ATEvents58"))},
    {new VWRecord(20,AdminWise.connectorManager.getString("constant.ATEvents20"))},
    {new VWRecord(28,AdminWise.connectorManager.getString("constant.ATEvents28")),
    new VWRecord(29,AdminWise.connectorManager.getString("constant.ATEvents29"))},
    {new VWRecord(30,AdminWise.connectorManager.getString("constant.ATEvents30")),
     new VWRecord(31,AdminWise.connectorManager.getString("constant.ATEvents31")),
     new VWRecord(32,AdminWise.connectorManager.getString("constant.ATEvents32"))},
    {new VWRecord(35,AdminWise.connectorManager.getString("constant.ATEvents35")),
     new VWRecord(36,AdminWise.connectorManager.getString("constant.ATEvents36")),
     new VWRecord(37,AdminWise.connectorManager.getString("constant.ATEvents37"))},
     {new VWRecord(38,AdminWise.connectorManager.getString("constant.ATEvents38"))},
     {
    	 new VWRecord(48,AdminWise.connectorManager.getString("constant.ATEvents48")),
    	 new VWRecord(49,AdminWise.connectorManager.getString("constant.ATEvents49")),
    	 new VWRecord(50,AdminWise.connectorManager.getString("constant.ATEvents50")),
    	 new VWRecord(42,AdminWise.connectorManager.getString("constant.ATEvents42")),
    	 new VWRecord(43,AdminWise.connectorManager.getString("constant.ATEvents43")),
    	 new VWRecord(44,AdminWise.connectorManager.getString("constant.ATEvents44"))
     },
     {
    	 new VWRecord(51,AdminWise.connectorManager.getString("constant.ATEvents51")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
    	 new VWRecord(52,AdminWise.connectorManager.getString("constant.ATEvents52")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
    	 new VWRecord(53,AdminWise.connectorManager.getString("constant.ATEvents53")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
    	 new VWRecord(45,AdminWise.connectorManager.getString("constant.ATEvents45")),
    	 new VWRecord(46,AdminWise.connectorManager.getString("constant.ATEvents46")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")),
    	 new VWRecord(47,AdminWise.connectorManager.getString("constant.ATEvents47")),
    	 new VWRecord(73,AdminWise.connectorManager.getString("constant.ATEvents54")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME"))
    },// Route Related Changes
    {//CV10 Enhancement Added for adding Add SubAdmin,Modify SubAdmin and Delete SubAdmin Roles mapping to 11 in db
    new VWRecord(59,AdminWise.connectorManager.getString("constant.ATEvents59")),
    new VWRecord(60,AdminWise.connectorManager.getString("constant.ATEvents60")),
    new VWRecord(61,AdminWise.connectorManager.getString("constant.ATEvents61"))}
    };

    /*
     * Automated Retention Services actions list - Valli
     */
    /**
     * Altering the ARSActions and ARSActionWithoutPurge need the code change in Retention
     * Vijaypriya.B.K on November 23
     */
    VWRecord[] ARSActions={new VWRecord(0,AdminWise.connectorManager.getString("constant.ARSActions0")),
    						new VWRecord(1,AdminWise.connectorManager.getString("constant.ARSActions1")),
    						new VWRecord(2,AdminWise.connectorManager.getString("constant.ARSActions2")),
							new VWRecord(3,AdminWise.connectorManager.getString("constant.ARSActions3")+" " + ResourceManager.getDefaultManager().getString("CVProduct.Name") + "."),
							new VWRecord(4,AdminWise.connectorManager.getString("constant.ARSActions4")+" "+ ResourceManager.getDefaultManager().getString("CVProduct.Name") + "."),
							new VWRecord(5,AdminWise.connectorManager.getString("constant.ARSActions5")+" " + ResourceManager.getDefaultManager().getString("CVProduct.Name") + ".")
							};
    //if PurgeDocs registry is false should not display delete and purge option
    VWRecord[] ARSActionsWithoutPurge={new VWRecord(0,AdminWise.connectorManager.getString("constant.ARSActions0")),
			new VWRecord(1,AdminWise.connectorManager.getString("constant.ARSActions1")),
			new VWRecord(2,AdminWise.connectorManager.getString("constant.ARSActions2")),
			new VWRecord(3,AdminWise.connectorManager.getString("constant.ARSActions3")+" "+ ResourceManager.getDefaultManager().getString("CVProduct.Name") + "."),
			new VWRecord(5,AdminWise.connectorManager.getString("constant.ARSActions5")+" " + ResourceManager.getDefaultManager().getString("CVProduct.Name") + ".")
			};

    //-----------------------------------------------------------
    VWRecord[] DRSTaskSignatureList={new VWRecord(0,AdminWise.connectorManager.getString("constant.DRSTaskSignatureList0")),
									 new VWRecord(1,AdminWise.connectorManager.getString("constant.DRSTaskSignatureList1")),
									 new VWRecord(2,AdminWise.connectorManager.getString("constant.DRSTaskSignatureList2")),
									};
    String []SyncOption ={"Sync","Yes","Cancel"};
	//-----------------------------------------------------------
			
	//-----------------------------------------------------------
    //Doc Type
    //-----------------------------------------------------------
    String[] VWIndexTypes={AdminWise.connectorManager.getString("constant.VWIndexTypes0"),AdminWise.connectorManager.getString("constant.VWIndexTypes1"),AdminWise.connectorManager.getString("constant.VWIndexTypes2"),
    		AdminWise.connectorManager.getString("constant.VWIndexTypes3"),AdminWise.connectorManager.getString("constant.VWIndexTypes4"),AdminWise.connectorManager.getString("constant.VWIndexTypes5")};
        String[] VWIndexNumericMask={AdminWise.connectorManager.getString("constant.VWIndexNumericMask0"),AdminWise.connectorManager.getString("constant.VWIndexNumericMask1"),AdminWise.connectorManager.getString("constant.VWIndexNumericMask2"),
        		AdminWise.connectorManager.getString("constant.VWIndexNumericMask3"),AdminWise.connectorManager.getString("constant.VWIndexNumericMask4")};
        String[] VWIndexFieldType={AdminWise.connectorManager.getString("constant.VWIndexFieldType0"),AdminWise.connectorManager.getString("constant.VWIndexFieldType1"),AdminWise.connectorManager.getString("constant.VWIndexFieldType2")};
        String[] DateValueTypes={AdminWise.connectorManager.getString("constant.DateValueTypes0"),AdminWise.connectorManager.getString("constant.DateValueTypes1"),AdminWise.connectorManager.getString("constant.DateValueTypes2")};
        String[] DateMasks={"M/d/yyyy","M/d/yy","MM/dd/yy","MM/dd/yyyy","dd/MM/yyyy",
        "yy/MM/dd","yyyy-MM-dd","dd-MMM-yy","dddd, MMMM dd, yyyy","MMMM dd, yyyy",
        "dddd, dd MMMM, yyyy","dd MMMM, yyyy", "MMM-dd-yyyy","yyyy-MM-dd HH:mm:ss"};

        String[] IndexTextFields={AdminWise.connectorManager.getString("constant.IndexTextFields0"),AdminWise.connectorManager.getString("constant.IndexTextFields1"),AdminWise.connectorManager.getString("constant.IndexTextFields2"),AdminWise.connectorManager.getString("constant.IndexTextFields3")};
        String[] IndexNoteFields={AdminWise.connectorManager.getString("constant.IndexNoteFields0"),AdminWise.connectorManager.getString("constant.IndexNoteFields1")};
        String[] IndexBoolFields={AdminWise.connectorManager.getString("constant.IndexBoolFields0"),AdminWise.connectorManager.getString("constant.IndexBoolFields1")};
        String[] IndexDateFields={AdminWise.connectorManager.getString("constant.IndexDateFields0"),AdminWise.connectorManager.getString("constant.IndexDateFields1"),AdminWise.connectorManager.getString("constant.IndexDateFields2"),AdminWise.connectorManager.getString("constant.IndexDateFields3")};
//        String[] IndexSelectionFields={AdminWise.connectorManager.getString("constant.IndexSelectionFields0"),AdminWise.connectorManager.getString("constant.IndexSelectionFields1"),AdminWise.connectorManager.getString("constant.IndexSelectionFields2"),AdminWise.connectorManager.getString("constant.IndexSelectionFields3"),};
        String[] IndexSelectionFields={AdminWise.connectorManager.getString("constant.IndexSelectionFields0"),AdminWise.connectorManager.getString("constant.IndexSelectionFields1"),AdminWise.connectorManager.getString("constant.IndexSelectionFields2"),AdminWise.connectorManager.getString("constant.IndexSelectionFields3"),AdminWise.connectorManager.getString("constant.IndexSelectionFields4")};
//        String[] IndexSelectionFields={AdminWise.connectorManager.getString("constant.IndexSelectionFields0"),AdminWise.connectorManager.getString("constant.IndexSelectionFields1"),AdminWise.connectorManager.getString("constant.IndexSelectionFields2"),AdminWise.connectorManager.getString("constant.IndexSelectionFields3"),};
        String[] IndexSequenceFields={AdminWise.connectorManager.getString("constant.IndexSequenceFields0"),AdminWise.connectorManager.getString("constant.IndexSequenceFields1"),AdminWise.connectorManager.getString("constant.IndexSequenceFields2"),
        		AdminWise.connectorManager.getString("constant.IndexSequenceFields3"),AdminWise.connectorManager.getString("constant.IndexSequenceFields4"),AdminWise.connectorManager.getString("constant.IndexSequenceFields5")};
        String[][] IndexNumberFields={{"Mask","Read Only","Unique","Default Value"},
            {"Mask","Decimal Places","Enclose Negatives In Parentheses",
             "Use Thousands Separator","Read Only","Unique","Default Value"},
            {"Mask","Decimal Places","Currency Symbol","Enclose Negatives In Parentheses",
             "Use Thousands Separator","Read Only","Unique","Default Value"},
            {"Mask","Decimal Places","Read Only","Unique","Default Value"},
            {"Mask","Decimal Places","Read Only","Unique","Default Value"}};

       String[] DefaultTextIndex={"",VWConstant.YesNoData[1],VWConstant.YesNoData[1],""};
       String[] DefaultBooleanIndex={VWConstant.YesNoData[1],VWConstant.YesNoData[0]};
       String[] DefaultDateIndex={DateMasks[0],VWConstant.YesNoData[1],DateValueTypes[0],""};
       String[] DefaultSequenceIndex={"","1","",VWConstant.YesNoData[1],VWConstant.YesNoData[1],""};
       String[] DefaultUniqueSequenceIndex={"","1","",VWConstant.SeqUniqData[1],VWConstant.SeqUniqData[1],""};
//     String[] DefaultSelectionIndex={"",VWConstant.YesNoData[1],VWConstant.YesNoData[1],""};
       String[] DefaultSelectionIndex={"",VWConstant.YesNoData[1],VWConstant.YesNoData[1],"",VWConstant.YesNoData[1]};
       String[] DefaultNumberIndex={VWIndexNumericMask[0],VWConstant.YesNoData[1],VWConstant.YesNoData[1],"0"};
       String[] DefaultDispalyFields={"0","0","0"};
       String[] IndexPropColumnNames = {AdminWise.connectorManager.getString("constant.IndexPropColumnNames0"),AdminWise.connectorManager.getString("constant.IndexPropColumnNames1")};
       int TableRowHeight = 20;
       public static int APPROVER_COL_INDEX=2;
       
       String[] RouteConditionsColNames={AdminWise.connectorManager.getString("constant.RouteConditionsColNames0"),AdminWise.connectorManager.getString("constant.RouteConditionsColNames1"),AdminWise.connectorManager.getString("constant.RouteConditionsColNames2"),AdminWise.connectorManager.getString("constant.RouteConditionsColNames3"),
    		   AdminWise.connectorManager.getString("constant.RouteConditionsColNames4"),AdminWise.connectorManager.getString("constant.RouteConditionsColNames5"), AdminWise.connectorManager.getString("constant.RouteConditionsColNames6")};//, "Received On","Processed On", "Status", "Comments"};
       public static final int COND_GREATER = 0;
       public static final int COND_GREATER_OR_EQUAL = 1;       
       public static final int COND_LESS = 2;       
       public static final int COND_LESS_OR_EQUAL = 3;
       public static final int COND_EQUAL = 4;
       public static final int COND_NOT_EQUAL = 5;
       public static final int COND_BETWEEN = 6;
       public static final int COND_STARTS_AFTER = 7;
       public static final int COND_STARTS_BEFORE = 8;
       public static final String COND_BETWEEN_STR = AdminWise.connectorManager.getString("constant.COND_BETWEEN_STR");
       public static final String COND_STARTS_AFTER_STR =AdminWise.connectorManager.getString("constant.COND_STARTS_AFTER_STR");
       public static final String COND_STARTS_BEFORE_STR =AdminWise.connectorManager.getString("constant.COND_STARTS_BEFORE_STR");
       //public static final int COND_IN = 8;
       //public static final int COND_LIKE = 6;
       String LBL_DOCS_IN_ROUTE_TABLE_HEADER =AdminWise.connectorManager.getString("constant.LBL_DOCS_IN_ROUTE_TABLE_HEADER")+" "+ResourceManager.getDefaultManager().getString("WORKFLOW_NAME");
       String LBL_DOCS_COMPLETED_ROUTE_TABLE_HEADER = ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" "+AdminWise.connectorManager.getString("constant.LBL_DOCS_COMPLETED_ROUTE_TABLE_HEADER");
       String LBL_TASK_COMPLETED_DOCS_TABLE_HEADER = AdminWise.connectorManager.getString("constant.LBL_TASK_COMPLETED_DOCS_TABLE_HEADER");
       public static final String LOCALE_DISPLAY_NAME = AdminWise.connectorManager.getString("constant.LOCALE_DISPLAY_NAME");
       
       // For opening document in DTC
       String GENERAL_PREF_ROOT = "/computhink/" + Constants.PRODUCT_NAME.toLowerCase() + "/GENERAL";
      
       //DRS 
       public static final int INVALIDROUTE = 1;
       public static final int VALIDROUTE = 0;

       public static final int notify_Recycle_Empty = 81;
       public static final int notify_Recycle_purgeDocs = 82;
       public static final int notify_Recycle_RestoreDocs = 83;

       public static final int notify_AT_UpdateSet = 76;

       public static final int notify_Retention_Update = 97;
       public static final int notify_Retention_Delete = 98;

       public static final int notify_Storage_LocationMod = 71;
       public static final int notify_Storage_LocationMov = 72;

       public static final String SELECT_MODULE_NAME= AdminWise.connectorManager.getString("constant.SELECT_MODULE_NAME");
       public static final String NOTIFI_TITLE = AdminWise.connectorManager.getString("constant.NOTIFI_TITLE");

       //Module Node type
       public static final int nodeType_Folder = 0;
       public static final int nodeType_Document = 1;
       public static final int nodeType_DT = 2;
       public static final int nodeType_Storage = 3;
       public static final int nodeType_AT = 4; 
       public static final int nodeType_RecycledDocs = 5;
       public static final int nodeType_RestoreDocs = 6;
       public static final int nodeType_Redaction =  7;
       public static final int nodeType_Retention = 8;
       public static final int nodeType_Route=9;
       
       String linkDataTypes[] = {AdminWise.connectorManager.getString("constant.linkDataTypes0"), AdminWise.connectorManager.getString("constant.linkDataTypes1"),
    		   AdminWise.connectorManager.getString("constant.linkDataTypes2"),AdminWise.connectorManager.getString("constant.linkDataTypes3") };
       String Msg_TProp_Changed =AdminWise.connectorManager.getString("constant.Msg_TProp_Changed");
       
       
       //Task Escalation enhancement
       public String ADD_MENU_ITEM = AdminWise.connectorManager.getString("constant.ADD_MENU_ITEM");
       public String REMOVE_MENU_ITEM = AdminWise.connectorManager.getString("constant.REMOVE_MENU_ITEM");
       
       //Validation for Approver List Table
		int routeUserCheck = -100;//Check for empty route user
		String routeUserCheckMsg = AdminWise.connectorManager.getString("routeUserCheckMsg.Select")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("routeUserCheckMsg.users");
		
		int actionEnabledRouteUserCheck = -101;//Check for action enabled user
		String actionEnabledRouteUserCheckMsg = AdminWise.connectorManager.getString("constant.actionEnabledRouteUserCheckMsg");
		
		int routeUserEscalatorMatch = -102;// checkApproverAndEscalationUserMatch
		String routeUserEscalatorMatchMsg = AdminWise.connectorManager.getString("constant.routeUserEscalatorMatchMsg0")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+AdminWise.connectorManager.getString("constant.routeUserEscalatorMatchMsg1");
		
		int duplicateRouteUser = -103; //checkForDuplicateUserAndGroup
		String duplicateRouteUserMsg = AdminWise.connectorManager.getString("constant.duplicateRouteUserMsg0")+" "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" " +AdminWise.connectorManager.getString("constant.duplicateRouteUserMsg1");
		
		int expirationGreater = -104; //checkForExpirationTimeGreaterThanRemainderInterval
		String expirationGreaterMsg = AdminWise.connectorManager.getString("constant.expirationGreaterMsg");
		
		int escalationUserCheck = -105; //checkForEscalationUserExist if ExpirationTimeGreaterThanRemainderInterval
		String escalationUserCheckMsg = AdminWise.connectorManager.getString("constant.escalationUserCheckMsg");
		
		int selectExpirationTime = -106; //checkForExpirationTime if Escalation user got selected.
		String selectExpirationTimeMsg = AdminWise.connectorManager.getString("constant.selectExpirationTimeMsg");
		
		int selListDSNInfo = 1;
		int DBLookupDSNInfo = 0;
		public static final String AUTOACCEPT = AdminWise.connectorManager.getString("constant.AUTOACCEPT");
		public static final String AUTOREJECT = AdminWise.connectorManager.getString("constant.AUTOREJECT");
		public static final String NONE = AdminWise.connectorManager.getString("constant.NONE");
		public static final String DYNAMIC_USER = AdminWise.connectorManager.getString("constant.DYNAMIC_USER");
		
	    //Changing Password Constants
	    String TXT_OLDPASSWORD_NAME =AdminWise.connectorManager.getString("constant.TXT_OLDPASSWORD_NAME");
	    String TXT_NEWPASSWORD_NAME =AdminWise.connectorManager.getString("constant.TXT_NEWPASSWORD_NAME");
	    String TXT_CONFIRMPASSWORD_NAME =AdminWise.connectorManager.getString("constant.TXT_CONFIRMPASSWORD_NAME");
	    String TXT_SECURITY_LOCATION =AdminWise.connectorManager.getString("constant.TXT_SECURITY_LOCATION");
	    String TXT_REPORT_LOCATION =AdminWise.connectorManager.getString("constant.TXT_REPORT_LOCATION");
	    String BTN_SAVE_AS_HTML=AdminWise.connectorManager.getString("constant.BTN_SAVE_AS_HTML");
	    /**@author apurba.m
	     * CV10.1 Enhancement: BTN_ADDINDEX_NAME variable added for adding custom index button
	     */
	    String BTN_ADDINDEX_NAME=AdminWise.connectorManager.getString("constant.BTN_CUSTOM_REPORT");
	    public static final String SSCHEME_ADS = "ADS";
	    /**
	     * CV10.2 Enhancement - General type report added in administration workflow tab
	     */
	    String[] lstReportType = {AdminWise.connectorManager.getString("constant.lstReportType0"),
	    		AdminWise.connectorManager.getString("constant.lstReportType1")};
	    public static final String reportType = AdminWise.connectorManager.getString("constant.reportType");
	    String LBL_CUSTOMCOLUMN=AdminWise.connectorManager.getString("constant.LBL_CUSTOMLIST")+"  ";
	    String LBL_COLUMN_COND=AdminWise.connectorManager.getString("constant.LBL_NOCOLUMN_COND");
	    String LBL_CABINET_LOCATION = AdminWise.connectorManager.getString("constant.Lbl.In");
}
