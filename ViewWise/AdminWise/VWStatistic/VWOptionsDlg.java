package ViewWise.AdminWise.VWStatistic;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import ViewWise.AdminWise.AdminWise;

import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWConstant;
import java.awt.Frame;
import java.util.Vector;
import java.util.prefs.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
//------------------------------------------------------------------------------
public class VWOptionsDlg extends javax.swing.JDialog implements VWConstant
{
    public VWOptionsDlg(Frame parent)
    {
        super(parent,LBL_OPTIONS,true);
        ///parent.setIconImage(VWImages.AdminWiseIcon.getImage());
        getContentPane().setBackground(AdminWise.getAWColor());
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(VWOptions,BorderLayout.CENTER);
        setVisible(false);
        VWOptions.setLayout(null);
        VWOptions.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWOptions.add(PanelCommand);
        PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
        PanelCommand.add(Pic);
        Pic.setBounds(8,12,152,132);
        BtnSave.setText(BTN_SAVE_NAME);
        BtnSave.setActionCommand(BTN_SAVE_NAME);
        PanelCommand.add(BtnSave);
//        BtnSave.setBackground(java.awt.Color.white);
        BtnSave.setBounds(12,160,144, AdminWise.gBtnHeight);
        BtnSave.setIcon(VWImages.SaveIcon);
        BtnCancel.setText(BTN_CANCEL_NAME);
        BtnCancel.setActionCommand(BTN_CANCEL_NAME);
        PanelCommand.add(BtnCancel);
//        BtnCancel.setBackground(java.awt.Color.white);
        BtnCancel.setBounds(12,195,144, AdminWise.gBtnHeight);
        BtnCancel.setIcon(VWImages.CloseIcon);
        PanelTable.setLayout(new BorderLayout());
        VWOptions.add(PanelTable);
        PanelTable.setBounds(168,0,426+100,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        SymComponent aSymComponent = new SymComponent();
        VWOptions.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.OptionsImage);
        SymAction lSymAction = new SymAction();
        BtnSave.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        Table.addData(getOptions());
        
        SymKey aSymKey = new SymKey();
        BtnSave.addKeyListener(aSymKey);
        BtnCancel.addKeyListener(aSymKey);
        Table.addKeyListener(aSymKey);
        SymWindow aSymWindow = new SymWindow();
        addWindowListener(aSymWindow);
        getDlgOptions();
        setResizable(false);
        setVisible(true);
    }
//------------------------------------------------------------------------------
    class SymWindow extends java.awt.event.WindowAdapter
    {
        public void windowClosing(WindowEvent event)
        {
            Object object = event.getSource();
            if (object == VWOptionsDlg.this)
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
                BtnSave_actionPerformed(null);
            else if (event.getKeyText(event.getKeyChar()).equals("Escape"))
                BtnCancel_actionPerformed(null);
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
        {
            public void actionPerformed(java.awt.event.ActionEvent event)
            {
                Object object = event.getSource();
                if (object == BtnSave)
                        BtnSave_actionPerformed(event);		
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);		
            }
        }
//------------------------------------------------------------------------------
    javax.swing.JPanel VWOptions = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnSave = new VWLinkedButton(0,true);
    VWLinkedButton BtnCancel = new VWLinkedButton(0,true);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWOptionsTable Table = new VWOptionsTable();
//------------------------------------------------------------------------------        
class SymComponent extends java.awt.event.ComponentAdapter
{
        public void componentResized(java.awt.event.ComponentEvent event)
        {
                Object object = event.getSource();
                if (object == VWOptions)
                        VWOptions_componentResized(event);
        }
}
//------------------------------------------------------------------------------
void VWOptions_componentResized(java.awt.event.ComponentEvent event)
{
    if(event.getSource()==VWOptions)
    {
        Dimension mainDimension=this.getSize();
        Dimension panelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        panelDimension.width=mainDimension.width-commandDimension.width;
        //PanelTable.setSize(panelDimension);
        PanelCommand.setSize(commandDimension);
        PanelTable.doLayout();
        AdminWise.adminPanel.MainTab.repaint();
    }
}
//------------------------------------------------------------------------------       
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event)
    {
        try{
            Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
        saveOptions();       
        setVisible(false);
    }
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        saveDlgOptions();
        setVisible(false);
    }    
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
        if(!AdminWise.adminPanel.saveDialogPos) return;
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", this.getX());
        prefs.putInt("y", this.getY());
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        this.setLocation(prefs.getInt("x",50),prefs.getInt("y",50));
        //setSize(600,460+50+30+20);
        setSize(600+100,460);
        this.SPTable.setVisible(true);
    }
//------------------------------------------------------------------------------
    private void saveOptions()
    {
        //AdminWise.adminPanel.saveBackupOptions(Table.getData());
    	VWStatisticConnector.setOptionSettings(Table.getData());
    }
//------------------------------------------------------------------------------
    private Vector getOptions()
    {
        return VWStatisticConnector.getOptionSettings();
    }
//------------------------------------------------------------------------------
 private static int gCurRoom=0;
 public static void main(String[] args) {
	 JFrame frame = new JFrame();
	 frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	 JButton btn = new JButton(AdminWise.connectorManager.getString("VWOptionsDlg.btn"));
	 btn.setBounds(0, 0, 80, 22);
	 frame.getContentPane().add(btn);
	 frame.setVisible(true);
	 frame.setSize(200, 100);
	 btn.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new VWOptionsDlg(new JFrame());
		} 
	 });
 }
}