package ViewWise.AdminWise.VWStatistic;

import java.util.ArrayList;

public class VWOption {
private int id;
private String optionName;
private String optionValue;
private ArrayList optionList;
private int optionType;
private boolean enabled;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public ArrayList getOptionList() {
	return optionList;
}
public void setOptionList(ArrayList optionList) {
	this.optionList = optionList;
}
public String getOptionName() {
	return optionName;
}
public void setOptionName(String optionName) {
	this.optionName = optionName;
}
public String getOptionValue() {
	return optionValue;
}
public void setOptionValue(String optionValue) {
	this.optionValue = optionValue;
}
public int getOptionType() {
	return optionType;
}
public void setOptionType(int optionType) {
	this.optionType = optionType;
}
public boolean isEnabled() {
	return enabled;
}
public void setEnabled(boolean enabled) {
	this.enabled = enabled;
}

}
