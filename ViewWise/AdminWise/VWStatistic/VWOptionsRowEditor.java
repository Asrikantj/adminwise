/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWSimpleSearchRowEditor<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWStatistic;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;

import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.resource.ResourceManager;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;

public class VWOptionsRowEditor implements TableCellEditor,VWConstant{
  
  protected TableCellEditor editor,defaultEditor;
  protected VWOptionsTable gTable=null;
//------------------------------------------------------------------------------
  public VWOptionsRowEditor(VWOptionsTable table){
    JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;
  }
//------------------------------------------------------------------------------
  public Component getTableCellEditorComponent(JTable table,Object value,
        boolean isSelected, int row, int column){    
    VWOption rowOption = getOption(row);
    if (rowOption.getOptionList() != null && rowOption.getOptionList().size() > 1){
    	editor=new DefaultCellEditor(new VWComboBox(rowOption.getOptionList()));    	
    }else{
    	editor = defaultEditor;
    }
    return editor.getTableCellEditorComponent(table,
             value, isSelected, row, column);
  }
//------------------------------------------------------------------------------
  public Object getCellEditorValue(){
    return editor.getCellEditorValue();
  }
//------------------------------------------------------------------------------
  public boolean stopCellEditing(){
    return editor.stopCellEditing();
  }
//------------------------------------------------------------------------------
  public void cancelCellEditing(){
    editor.cancelCellEditing();
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(EventObject anEvent){
      int column=-1,row=-1;
      //CV10 dutch localization to fix enabling the statistic option table column edit.
      AdminWise.printToConsole("SignatureAnnotation::::::"+AdminWise.connectorManager.getString("StatisticsOption_SignatureAnnotation"));
      String signAnnotation = getOptionByOptionName("Set signature annotation setting").getOptionValue();
      if (anEvent instanceof MouseEvent) {
	    Point point=((MouseEvent)anEvent).getPoint();
            column = gTable.columnAtPoint(point);
            row = gTable.rowAtPoint(point);
            //System.out.println("is CEll Editable " + row + " : " + getOption(row).isEnabled());
            if ( column != 1) return false; 
            if (signAnnotation.equalsIgnoreCase("system")) return true;
            if (signAnnotation.equalsIgnoreCase("client") && !getOption(row).getOptionName().toLowerCase().startsWith("signature")) return true;            
        }
        return false;
  }
//------------------------------------------------------------------------------
  public void addCellEditorListener(CellEditorListener l)
  {
    editor.addCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public void removeCellEditorListener(CellEditorListener l)
  {
    editor.removeCellEditorListener(l);
  }
//------------------------------------------------------------------------------
  public boolean shouldSelectCell(EventObject anEvent)
  {
    return editor.shouldSelectCell(anEvent);
  }
//------------------------------------------------------------------------------
  private VWOption getOption(int row)
  {
        return gTable.getRowData(row);
  }
  
  private VWOption getOptionByOptionName(String optionName)
  {
        return gTable.getRowDataByOptionName(optionName);
  }
//------------------------------------------------------------------------------
}
  