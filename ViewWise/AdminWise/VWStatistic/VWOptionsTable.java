/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWStatistic;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import com.computhink.common.Index;

import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
//------------------------------------------------------------------------------
public class VWOptionsTable extends JTable implements VWConstant
{
    protected VWOptionsTableData m_data;    
    public VWOptionsTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWOptionsTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Dimension tableWith=getPreferredScrollableViewportSize();
        for (int k=0;k<VWOptionsTableData.m_columns.length;k++) 
        {
            VWTableCellRenderer renderer=new VWTableCellRenderer();
            renderer.setHorizontalAlignment(VWOptionsTableData.m_columns[k].m_alignment);
            VWOptionsRowEditor editor=new VWOptionsRowEditor(this);
            TableColumn column=new TableColumn(k,Math.round(tableWith.width*VWOptionsTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse=new SymMouse();
	addMouseListener(aSymMouse);
	setRowHeight(TableRowHeight);
	
	setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
            Object object = event.getSource();
            if (object instanceof JTable)
                    if(event.getModifiers()==event.BUTTON3_MASK)
                    VWOptionsTable_RightMouseClicked(event);
                    else if(event.getModifiers()==event.BUTTON1_MASK)
                            VWOptionsTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWOptionsTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWOptionsTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
  public void addData(Vector data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }

//------------------------------------------------------------------------------
  public Vector<VWOption> getData()
  {
        return m_data.getModifiedData();
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new OptionsRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
/*  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public String getRowValue(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return row[1];
  }*/
//------------------------------------------------------------------------------
  public void setRowValue(int rowNum,String value)
  {
    m_data.setRowData(rowNum,value);
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
  public VWOption getRowData(int rowNum)
  {
      return m_data.getRowData(rowNum);
  }
  
  public VWOption getRowDataByOptionName(String optionName)
  {
      return m_data.getRowDataByOptionName(optionName);
  }
  
//------------------------------------------------------------------------------
}
class OptionsRowData
{
    public String   m_OptionName;
    public String   m_OptionValue;
    public ArrayList m_OptionList;
    public int m_OptionId;
    public int m_OptionType;
    public boolean m_Enabled;
    
  public OptionsRowData() {
    m_OptionName="";
    m_OptionValue="";
  }
  //----------------------------------------------------------------------------
  public OptionsRowData(VWOption option) 
  {
	  	m_OptionId = option.getId();
        m_OptionName = option.getOptionName();
        m_OptionValue = option.getOptionValue();
        m_OptionList = option.getOptionList();
        m_OptionType = option.getOptionType();
        m_Enabled = option.isEnabled();
        System.out.println("Option List is " + m_OptionList.size() + " ::: " + m_OptionList + " : " + m_OptionType);
  }
  
  public OptionsRowData(String optionName,String optionValue) 
  {
        m_OptionName=optionName;
        m_OptionValue=optionValue;
  }
  //----------------------------------------------------------------------------
  public OptionsRowData(String[] data) 
  {
    int i=0;
    m_OptionName=data[i++];
    m_OptionValue=data[i];
  }
}
//------------------------------------------------------------------------------
class OptionsColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public OptionsColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWOptionsTableData extends AbstractTableModel 
{
  public static final OptionsColumnData m_columns[] = {
    new OptionsColumnData( VWConstant.BOptionColumnNames[0],0.6F, JLabel.LEFT),
    new OptionsColumnData( VWConstant.BOptionColumnNames[1],0.6F,JLabel.LEFT)};
    
  public static final int COL_OPTIONNAME = 0;
  public static final int COL_OPTIONVALUE = 1;

  protected VWOptionsTable m_parent;
  protected Vector m_vector;

  public VWOptionsTableData(VWOptionsTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        OptionsRowData row =new OptionsRowData(data[i]); 
        m_vector.addElement(row);
    }
  }

public void setData(Vector data) {
    m_vector.removeAllElements();
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        OptionsRowData row =new OptionsRowData((VWOption)data.get(i)); 
        m_vector.addElement(row);
    }
  }

//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][2];
    for(int i=0;i<count;i++)
    {
        OptionsRowData row=(OptionsRowData)m_vector.elementAt(i);
        data[i][0]=row.m_OptionName;
        data[i][1]=row.m_OptionValue;
    }
    return data;
  }

public Vector getModifiedData() {
    int count=getRowCount();
    Vector<VWOption> output = new Vector<VWOption>(); 
    for(int i=0;i<count;i++)
    {
        OptionsRowData row=(OptionsRowData)m_vector.elementAt(i);
        VWOption option = new VWOption();
        option.setId(row.m_OptionId);
        option.setOptionName(row.m_OptionName);
        option.setOptionValue(row.m_OptionValue.trim());
        //option.setOptionList(row.m_OptionList);
        //option.setOptionType(row.m_OptionType);
        output.add(option);
    }
    return output;
  }

//------------------------------------------------------------------------------


/*  public String[] getRowData(int rowNum) {
    String[] OptionsRowData=new String[2];
    OptionsRowData row=(OptionsRowData)m_vector.elementAt(rowNum);
    OptionsRowData[0]=row.m_OptionName;
    OptionsRowData[1]=row.m_OptionValue;
    return OptionsRowData;
  }*/
public VWOption getRowData(int rowNum) {
	VWOption option = new VWOption();
    
    OptionsRowData row=(OptionsRowData)m_vector.elementAt(rowNum);
    option.setId(row.m_OptionId);
    option.setOptionName(row.m_OptionName);
    option.setOptionValue(row.m_OptionValue.trim());
    option.setEnabled(row.m_Enabled);
    option.setOptionType(row.m_OptionType);
    option.setOptionList(row.m_OptionList);
    
    return option;
}


public VWOption getRowDataByOptionName(String optionName) {
	VWOption option = new VWOption();

	for (int i = 0 ; i < m_vector.size(); i++){
		OptionsRowData row=(OptionsRowData)m_vector.elementAt(i);
		if (row.m_OptionName.equalsIgnoreCase(optionName.trim())){
			option.setId(row.m_OptionId);
			option.setOptionName(row.m_OptionName);
			option.setOptionValue(row.m_OptionValue.trim());
			option.setEnabled(row.m_Enabled);
			option.setOptionType(row.m_OptionType);
			option.setOptionList(row.m_OptionList);
			return option;
		}    
	}
	return null;
}


//------------------------------------------------------------------------------
  public void setRowData(int rowNum,String value) {
    ((OptionsRowData)m_vector.elementAt(rowNum)).m_OptionValue=value;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    OptionsRowData row = (OptionsRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_OPTIONNAME:    return row.m_OptionName;
      case COL_OPTIONVALUE:   return row.m_OptionValue;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    OptionsRowData row = (OptionsRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_OPTIONNAME:
        row.m_OptionName = svalue; 
        break;
      case COL_OPTIONVALUE:
        row.m_OptionValue = svalue;
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(OptionsRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
}