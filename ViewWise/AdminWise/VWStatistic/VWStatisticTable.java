/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWStatistic;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import  javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWMenu.VWMenu;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWTable.VWTableCellRenderer;
import ViewWise.AdminWise.VWUtil.VWUtil;
//------------------------------------------------------------------------------
public class VWStatisticTable extends JTable implements VWConstant
{
    protected VWStatisticTableData m_data;    
    public VWStatisticTable(){
        super();
        m_data = new VWStatisticTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWStatisticTableData.m_columns.length; k++) {
        
        VWTableCellRenderer renderer =new VWTableCellRenderer();
        renderer.setHorizontalAlignment(VWStatisticTableData.m_columns[k].m_alignment);
        VWStatisticRowEditor editor=new VWStatisticRowEditor(this);
        TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWStatisticTableData.m_columns[k].m_width),renderer,editor);
        addColumn(column);
        }
        setRowSelectionAllowed(true);
        setBackground(java.awt.Color.white);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
	 setRowHeight(TableRowHeight);
	 
	 setTableResizable();
 }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
            Object object = event.getSource();
            if (object instanceof JTable)
                    if(event.getModifiers()==event.BUTTON3_MASK)
                    VWStatisticTable_RightMouseClicked(event);
                    else if(event.getModifiers()==event.BUTTON1_MASK)
                            VWStatisticTable_LeftMouseClicked(event);
    }
}
//------------------------------------------------------------------------------
void VWStatisticTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWStatisticTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {
        /*
        VWMenu menu=null;
        if(getSelectedRow()==-1) return;

        menu=new VWMenu(VWConstant.RefDocument_TYPE);
        menu.show(this,event.getX(),event.getY());
        */
    }
//------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//------------------------------------------------------------------------------
  public void addData(List list)
  {
      m_data.setData(list);
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void addData(String[][] data)
  {
        m_data.setData(data);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[][] getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public String[][] getReportData()
  {
      String[][] repData=m_data.getData();
      return repData;
  }
//------------------------------------------------------------------------------
  public void removeData(int row) {
        m_data.remove(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
    public void insertData(String[][] data)
  {
    for(int i=0;i<data.length;i++)
    {
        m_data.insert(new StatisticRowData(data[i]));
    }
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowStatisticId(int rowNum)
  {
        String[] row=m_data.getRowData(rowNum);
        return VWUtil.to_Number(row[0]);
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
}
class StatisticRowData
{
    public String   m_Name;
    public String   m_Value;
  public StatisticRowData() {
    m_Name="";
    m_Value="";
  }
  //----------------------------------------------------------------------------
  public StatisticRowData(String name,String value) 
  {
        m_Name=name;
        m_Value=value;
  }
  //----------------------------------------------------------------------------
  public StatisticRowData(String str)
  {
        VWStringTokenizer tokens=new VWStringTokenizer(str,VWConstant.SepChar,false);
        m_Name=tokens.nextToken();
        m_Value=tokens.nextToken();
  }
  //----------------------------------------------------------------------------
  public StatisticRowData(String[] data) 
  {
    int i=0;
    m_Name=data[i++];
    m_Value=data[i];
  }
}
//------------------------------------------------------------------------------
class StatisticColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public StatisticColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWStatisticTableData extends AbstractTableModel 
{
  public static final StatisticColumnData m_columns[] = {
    new StatisticColumnData(VWConstant.StatisticColumnNames[0],0.5F, JLabel.LEFT),
    new StatisticColumnData(VWConstant.StatisticColumnNames[1],0.5F,JLabel.LEFT),
  };
  public static final int COL_NAME = 0;
  public static final int COL_VALUE = 1;

  protected VWStatisticTable m_parent;
  protected Vector m_vector;

  public VWStatisticTableData(VWStatisticTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(List data) {
    m_vector.removeAllElements();
    if (data==null) return;
    int count =data.size();
    for(int i=0;i<count;i++)
    {
        m_vector.addElement(new StatisticRowData((String)data.get(i)));
    }
  }
//------------------------------------------------------------------------------
public void setData(String[][] data) {
    m_vector.removeAllElements();
    int count =data.length;
    for(int i=0;i<count;i++)
    {
        StatisticRowData row =new StatisticRowData(data[i]); 
        m_vector.addElement(row);
    }
  }
//------------------------------------------------------------------------------
public String[][] getData() {
    int count=getRowCount();
    String[][] data=new String[count][2];
    for(int i=0;i<count;i++)
    {
        StatisticRowData row=(StatisticRowData)m_vector.elementAt(i);
        data[i][0]=row.m_Name;
        data[i][1]=row.m_Value;
    }
    return data;
  }
//------------------------------------------------------------------------------
  public String[] getRowData(int rowNum) {
    String[] StatisticRowData=new String[2];
    StatisticRowData row=(StatisticRowData)m_vector.elementAt(rowNum);
    StatisticRowData[0]=row.m_Name;
    StatisticRowData[1]=row.m_Value;
    return StatisticRowData;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    StatisticRowData row = (StatisticRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_NAME:    return row.m_Name;
      case COL_VALUE:   return row.m_Value;
    }
    return "";
  }
//------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    StatisticRowData row = (StatisticRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_NAME:
        row.m_Name = svalue; 
        break;
      case COL_VALUE:
        row.m_Value = svalue;
        break;
    }
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
    public void insert(StatisticRowData AdvanceSearchRowData) 
    {
        m_vector.addElement(AdvanceSearchRowData);
    }
//------------------------------------------------------------------------------
 public boolean remove(int row){
    if (row < 0 || row >= m_vector.size())
      return false;
        m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
}