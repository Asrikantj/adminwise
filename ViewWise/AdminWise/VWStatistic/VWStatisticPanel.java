package ViewWise.AdminWise.VWStatistic;

import java.awt.Dimension;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Container;
import ViewWise.AdminWise.AdminWise;
import javax.swing.border.EtchedBorder;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWLinkedButton;
import ViewWise.AdminWise.VWUtil.VWSaveAsHtml;
import ViewWise.AdminWise.VWPanel.VWPicturePanel;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWConstant;
import  java.util.Vector;
import ViewWise.AdminWise.VWUtil.VWPrint;

public class VWStatisticPanel extends JPanel implements VWConstant {
    public VWStatisticPanel() {
        
    }
    public void setupUI() {
    	int top = 116; int hGap = 28;
        if(UILoaded) return;
        setLayout(new BorderLayout());
        VWStatistic.setLayout(null);
        add(VWStatistic,BorderLayout.CENTER);
        VWStatistic.setBounds(0,0,605,433);
        PanelCommand.setLayout(null);
        VWStatistic.add(PanelCommand);
        PanelCommand.setBackground(AdminWise.getAWColor());
        PanelCommand.setBounds(0,0,168,432);
        Pic.setIconTextGap(0);
        Pic.setAutoscrolls(true);
/*        PanelCommand.add(Pic);
        Pic.setBounds(8,4,149,92);
*/      
        PanelCommand.add(picPanel);
        picPanel.setBounds(8,16,149,92);
        
        BtnRefresh.setText(BTN_REFRESH_NAME);
        BtnRefresh.setActionCommand(BTN_REFRESH_NAME);
        PanelCommand.add(BtnRefresh);
        //BtnRefresh.setBackground(java.awt.Color.white);
        BtnRefresh.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnRefresh.setIcon(VWImages.RefreshIcon);

        top += hGap + 12;        
        BtnOption.setText(BTN_OPTIONS_NAME);
        BtnOption.setActionCommand(BTN_OPTIONS_NAME);
        PanelCommand.add(BtnOption);
        BtnOption.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnOption.setIcon(VWImages.OptionsIcon);

        
        top += hGap + 12;
        BtnSaveAs.setText(BTN_SAVEAS_NAME);
        BtnSaveAs.setActionCommand(BTN_SAVEAS_NAME);
        PanelCommand.add(BtnSaveAs);
        //BtnSaveAs.setBackground(java.awt.Color.white);
        BtnSaveAs.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnSaveAs.setIcon(VWImages.SaveAsIcon);
        
        top += hGap;
        BtnPrint.setText(BTN_PRINT_NAME);
        BtnPrint.setActionCommand(BTN_PRINT_NAME);
        PanelCommand.add(BtnPrint);
        //BtnPrint.setBackground(java.awt.Color.white);
        BtnPrint.setBounds(12,top,144, AdminWise.gBtnHeight);
        BtnPrint.setIcon(VWImages.PrintIcon);
        
        PanelTable.setLayout(new BorderLayout());
        VWStatistic.add(PanelTable);
        PanelTable.setBounds(168,0,433,429);
        SPTable.setOpaque(true);
        PanelTable.add(SPTable,BorderLayout.CENTER);
        SPTable.getViewport().add(Table);
        Table.getParent().setBackground(java.awt.Color.white);
        setEnableMode(MODE_UNCONNECT);
        SymComponent aSymComponent = new SymComponent();
        VWStatistic.addComponentListener(aSymComponent);
        Pic.setIcon(VWImages.StatisticImage);
        SymAction lSymAction = new SymAction();
        BtnRefresh.addActionListener(lSymAction);
        BtnOption.addActionListener(lSymAction);
        BtnSaveAs.addActionListener(lSymAction);
        BtnPrint.addActionListener(lSymAction);
        repaint();
        doLayout();
        UILoaded=true;
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnRefresh)
                BtnRefresh_actionPerformed(event);
            else if (object == BtnSaveAs)
                BtnSaveAs_actionPerformed(event);
            else if (object == BtnPrint)
                BtnPrint_actionPerformed(event);
            else if (object == BtnOption)
                BtnOption_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    VWPicturePanel picPanel = new VWPicturePanel(TAB_STATISTIC_NAME, VWImages.StatisticImage);
    javax.swing.JPanel VWStatistic = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    VWLinkedButton BtnRefresh = new VWLinkedButton(1);
    VWLinkedButton BtnOption = new VWLinkedButton(1);
    VWLinkedButton BtnSaveAs = new VWLinkedButton(1);
    VWLinkedButton BtnPrint = new VWLinkedButton(1);
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JScrollPane SPTable = new javax.swing.JScrollPane();
    VWStatisticTable Table = new VWStatisticTable();
    //--------------------------------------------------------------------------
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == VWStatistic)
                VWStatistic_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWStatistic_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource()==VWStatistic) {
            Dimension mainDimension=this.getSize();
            Dimension panelDimension=mainDimension;
            Dimension commandDimension=PanelCommand.getSize();
            commandDimension.height=mainDimension.height;
            panelDimension.width=mainDimension.width-commandDimension.width;
            PanelTable.setSize(panelDimension);
            PanelCommand.setSize(commandDimension);
            PanelTable.doLayout();
            AdminWise.adminPanel.MainTab.repaint();
        }
    }
    //--------------------------------------------------------------------------
    public void loadTabData(VWRoom newRoom) {
        if(newRoom.getConnectStatus()!=Room_Status_Connect) {
            Table.clearData();
            return;
        }
        if(newRoom.getId()==gCurRoom)return;
        gCurRoom=newRoom.getId();
        Table.clearData();
        try {
            Table.addData(VWStatisticConnector.getStatistic(newRoom));
        }
        catch(Exception e){};
        setEnableMode(MODE_CONNECT);
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        Table.clearData();
        gCurRoom=0;
        setEnableMode(MODE_UNCONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnOption_actionPerformed(java.awt.event.ActionEvent event) {
    	new VWOptionsDlg(AdminWise.adminFrame);
    }
    
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event) {
        Table.clearData();
        Table.addData(VWStatisticConnector.getStatistic());
        setEnableMode(MODE_CONNECT);
    }
    //--------------------------------------------------------------------------
    void BtnSaveAs_actionPerformed(java.awt.event.ActionEvent event) {
        try{
            AdminWise.adminPanel.setWaitPointer();
            VWSaveAsHtml.saveArrayInFile(Table.getReportData(),StatisticColumnNames,StatisticColumnWidths,
            null,(java.awt.Component)this,TAB_STATISTIC_NAME);
        }
        catch(Exception e){}
        finally {
            AdminWise.adminPanel.setDefaultPointer();
        }
    }
    //--------------------------------------------------------------------------
    void BtnPrint_actionPerformed(java.awt.event.ActionEvent event) {
        new VWPrint(Table);
    }
    //--------------------------------------------------------------------------
    public void setEnableMode(int mode) {
        BtnRefresh.setVisible(true);
        BtnRefresh.setEnabled(true);
        BtnOption.setVisible(true);
        BtnOption.setEnabled(true);
        BtnSaveAs.setVisible(true);
        BtnSaveAs.setEnabled(true);
        BtnPrint.setVisible(true);
        BtnPrint.setEnabled(true);        
        
        switch (mode) {
            case MODE_CONNECT:
                if(Table.getRowCount()==0) {
                    BtnSaveAs.setEnabled(false);
                    BtnPrint.setEnabled(false);
                }
                break;
            case MODE_UNCONNECT:
                BtnRefresh.setEnabled(false);
                BtnOption.setEnabled(false);
                BtnSaveAs.setEnabled(false);
                BtnPrint.setEnabled(false);
                gCurRoom=0;
                break;
        }
    }
    //--------------------------------------------------------------------------
    private static int gCurRoom=0;
    private boolean UILoaded=false;
}