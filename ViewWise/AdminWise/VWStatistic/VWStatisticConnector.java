/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWStatisticConnector<br>
 *
 * @version     $Revision: 1.4 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWStatistic;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.LinkedList;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWMessage.VWMessage;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.VWUtil.VWUtil;
import ViewWise.AdminWise.VWConstant;

//------------------------------------------------------------------------------
public class VWStatisticConnector implements  VWConstant
{
    public static String[][] getStatistic()
    {
        VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        if(room != null && room.getConnectStatus()==Room_Status_Connect)
            return getStatistic(room);
        return null;
    }
//------------------------------------------------------------------------------
    public static String[][] getStatistic(VWRoom room)
    {
        String[][] Statistic=Statistic_Names;
        Vector info=new Vector();
        int ret=AdminWise.gConnector.ViewWiseClient.getRoomStatistics(room.getId(),info);
        if(info==null || info.size()==0) return Statistic;
        for(int i=0;i<info.size();i++){
        	AdminWise.printToConsole("(String)info.get(i)  STATS :::: "+(String)info.get(i));
        	Statistic[i][1]=(String)info.get(i);
        }
        return Statistic;
    }
    public static Vector getOptionSettings(){    	
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
    	Vector<VWOption> result = new Vector<VWOption>();
        Vector info=new Vector();
        int ret=AdminWise.gConnector.ViewWiseClient.getOptionSettings(room.getId(),info);
/*        info.add("9	Include Percentage in Text Search	No	Enable	Yes	No");
        info.add("10	Include Percentage in Index Search	BEGIN	Enable	Begin	End	Both	None");
        info.add("86	Signature Apply All Settings	No	Disable	Yes	No");
*/        
        //9 Include Percentage in Text Search YesYes ; No
        //9	Include Percentage in Text Search	YesYes 	 No
        for ( int i = 0 ; i < info.size(); i++){
        	String data = info.get(i).toString();
        	StringTokenizer tokens = new StringTokenizer(data, "\t");
        	VWOption option = new VWOption();
        	ArrayList<String> list = new ArrayList<String>(); 
        	option.setId(VWUtil.to_Number(tokens.nextToken()));
        	option.setOptionName(tokens.nextToken());
        	option.setOptionValue(tokens.nextToken().trim());
        	if (tokens.nextToken().equals("Disable")){
        		option.setEnabled(false);
        	}
        	else{
        		option.setEnabled(true);
        	}
        	try{
        	while(tokens.hasMoreTokens()){
        		list.add((String)tokens.nextToken());	
        	}
        	}catch (Exception e) {
				// TODO: handle exception
        		e.printStackTrace();
			}
        	if (option.getOptionList() != null && option.getOptionList().size() > 1){
        		option.setOptionType(1);
        	}else {
        		option.setOptionType(0);
        	}
        	option.setOptionList(list);
        	result.add(option);
        	//System.out.println("Recordd is  " + option.isEnabled());
        }
    	return result;
    	
    }
    public static int setOptionSettings(Vector<VWOption> data){   	
    	VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
        Vector info=new Vector();
        String params = data.size() + "\t";
        for (int i = 0; i < data.size(); i++){
        	VWOption option = data.get(i);
        	params += option.getId() + "\t" + option.getOptionValue().trim() + "\t" ;
        }        
        int ret=AdminWise.gConnector.ViewWiseClient.setOptionSettings(room.getId(),params);        
    	return ret;
    	
    }
public static void main(String[] args) {
	VWStatisticConnector.getOptionSettings();
}
//------------------------------------------------------------------------------
}
