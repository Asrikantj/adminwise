package ViewWise.AdminWise.VWTable;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.computhink.common.Index;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWTriggerIndexDialog;
import ViewWise.AdminWise.VWUtil.VWTableResizer;

public class VWTriggerIndexTable extends JTable implements VWConstant{
	
	private static final long serialVersionUID = 6720197168896205626L;
	VWTriggerIndexDialog triggerIndexDlg;
	VWTriggerIndexTableData m_data;
	
	public VWTriggerIndexTable(VWTriggerIndexDialog triggerIndexDlg){
		super();
		this.triggerIndexDlg = triggerIndexDlg;
		getTableHeader().setReorderingAllowed(false);
		m_data = new VWTriggerIndexTableData(this);
		setCellSelectionEnabled(false);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Dimension tableWith = getPreferredScrollableViewportSize();
		
		for (int k = 0; k < VWTriggerIndexTableData.m_columns.length; k++){
		    TableCellRenderer renderer;
		    DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
		    textRenderer.setHorizontalAlignment(VWTriggerIndexTableData.m_columns[k].m_alignment);
		    renderer = textRenderer;
		    VWTriggerIndexRowEditor editor=new VWTriggerIndexRowEditor(this);
		    TableColumn column = new TableColumn(k,Math.round(tableWith.width*
			    VWTriggerIndexTableData.m_columns[k].m_width),renderer,editor);
		    addColumn(column);   
		}
		
		JTableHeader header = getTableHeader();
		header.setUpdateTableInRealTime(false);
		setRowHeight(TableRowHeight);
		setTableResizable();		
	}

	public void setTableResizable() {
		new VWTableResizer(this);
	}
	
	public void addData(Vector data){
		m_data.addData(data);
		m_data.fireTableDataChanged();		
	}
	
	public Vector getData(){
		if(isEditing()){
			getCellEditor().stopCellEditing();
		}
		return m_data.getData();
	}
	
	public void clearData(){
		m_data.clear();
	}
	
	public boolean getScrollableTracksViewportHeight(){
		return getPreferredSize().height < getParent().getHeight();
	}

	
}

class VWTriggerIndexTableData extends AbstractTableModel{
	
	private static final long serialVersionUID = 1L;
	Vector m_vector;
	VWTriggerIndexTable m_parent;
	
	public static final int COL_TINDEX_NAME = 0;
	public static final int COL_TINDEX_VALUE = 1;

	public static final VWTriggerIndexColumnData[] m_columns = {
		new VWTriggerIndexColumnData(AdminWise.connectorManager.getString("VWTriggerIndexTable.VWTriggerIndexColumnData_1"), 0.2F, JLabel.LEFT),
		new VWTriggerIndexColumnData(AdminWise.connectorManager.getString("VWTriggerIndexTable.VWTriggerIndexColumnData_2"), 0.2F, JLabel.LEFT)};
	
	public VWTriggerIndexTableData(VWTriggerIndexTable parent){
		m_parent = parent;
		m_vector = new Vector();
	}
	public void clear() {
		m_vector.removeAllElements();
	}
	public void addData(Vector data) {
		if(data == null || data.size() <= 0)
			return;
		
		m_vector.removeAllElements();
		for(int i=0; i<data.size(); i++){
			Index tIdx = (Index)data.get(i);
			m_vector.addElement(new VWTriggerIndexRowData(tIdx));
		}
	}
	public Vector getData(){
		int count = getRowCount();
		Vector data = new Vector();
		
		for(int i=0; i<count; i++){
			VWTriggerIndexRowData rowData = (VWTriggerIndexRowData) m_vector.elementAt(i);
			Index tIdx = new Index(rowData.m_tIndexId);
			tIdx.setName(rowData.m_tIndexName);
			tIdx.setValue(rowData.m_tIndexValue);
			tIdx.setTriggerDataType(rowData.m_tIndexDataType);
			
			data.add(tIdx);			
		}
		return data;
	}
	public int getColumnCount() {
		return m_columns.length;
	}
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size();
	}
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex < 0 || rowIndex >= getRowCount())
		    return "";
		VWTriggerIndexRowData row = (VWTriggerIndexRowData)m_vector.elementAt(rowIndex);
		switch (columnIndex) {
		case COL_TINDEX_NAME: return row.m_tIndexName;
		case COL_TINDEX_VALUE: return row.m_tIndexValue;
		}
		return "";
	}
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		if (rowIndex < 0 || rowIndex >= getRowCount() || value==null)
			return;
		VWTriggerIndexRowData row = (VWTriggerIndexRowData)m_vector.elementAt(rowIndex);
		String svalue = value.toString();

		switch (columnIndex) {
		case COL_TINDEX_NAME:
			row.m_tIndexName= svalue; 
			break;
		case COL_TINDEX_VALUE:
			row.m_tIndexValue= svalue; 
			break;
		}
	}
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return true;
	}
	public String getColumnName(int columnIndex){
		return m_columns[columnIndex].m_title;
	}
}

class VWTriggerIndexRowData {
	public int m_tIndexId;
	public int m_tIndexDataType;
	public String m_tIndexName = "";
	public String m_tIndexValue = "";
	
	
	public VWTriggerIndexRowData(Index tIdx){
		m_tIndexId = tIdx.getId();
		m_tIndexName = tIdx.getName();
		m_tIndexValue = tIdx.getValue();
		m_tIndexDataType = tIdx.getTriggerDataType();		
	}
}

class VWTriggerIndexColumnData
{
    public String  m_title;
    float m_width;
    int m_alignment;

    public VWTriggerIndexColumnData(String title, float width, int alignment) {
    	m_title = title;
    	m_width = width;
    	m_alignment = alignment;
    }
}