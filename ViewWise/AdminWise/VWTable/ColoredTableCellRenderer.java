package ViewWise.AdminWise.VWTable;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.border.*;
import java.awt.Component;
import java.awt.Color;
import java.awt.Rectangle;
import ViewWise.AdminWise.VWDocType.VWIndexRec;

public class ColoredTableCellRenderer extends DefaultTableCellRenderer
{
  public void setValue(Object value) {
      if(isSelectedFalg)
      {
            setForeground(Color.white);
            super.setValue(value);
      }
      else
      {
        if (value instanceof ColorData) {
          ColorData cvalue = (ColorData)value;
          setForeground(cvalue.getFColor());
          setText(cvalue.getData().toString());
        }
        else if (value instanceof IconData) 
        {
          IconData ivalue = (IconData)value;
          setIcon(ivalue.m_icon);
          setText(ivalue.m_data.toString());
        }
        else{
            super.setForeground(UIManager.getColor("Table.focusCellForeground"));
            super.setValue(value);
        }
      }
  }
//------------------------------------------------------------------------------
  public Component getTableCellRendererComponent(JTable table,Object value,
    boolean isSelected,boolean hasFocus,int row,int column) 
    {
        isSelectedFalg=isSelected;
        if(row%2==0)
            ///setBackground(Color.lightGray);
            setBackground(new Color(15658734));
        else
            setBackground(Color.white);
        return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }
    private boolean isSelectedFalg=false;
}
