package ViewWise.AdminWise.VWTable;

import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;

import com.computhink.common.Index;

import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;

/**
 * each row TableCellEditor
 * 
 * @version 1.0 07/24/2009
 * @author <a href="mailto:pandiyaraj.m@syntaxsoft.com">Pandiya Raj.M</a>
 */

public class VWIndexMappingRowEditor implements TableCellEditor, VWConstant {

    protected TableCellEditor editor, defaultEditor;

    protected VWIndexMappingTable gTable = null;

    protected JTextField textEditor = new JTextField();

    /**
     * Constructs a EachRowEditor. create default editor
     * 
     * @see TableCellEditor
     * @see DefaultCellEditor
     */
    public VWIndexMappingRowEditor(VWIndexMappingTable table) {
	textEditor.setBorder(null);
	defaultEditor = new DefaultCellEditor(textEditor);
	gTable = table;
    }

    /**
     * @param row
     *                table row
     * @param editor
     *                table cell editor
     */
    public Component getTableCellEditorComponent(JTable table, Object value,
	    boolean isSelected, int row, int column) {
    if (column == gTable.m_data.COL_EXTERNALDBCOL) {
	    VWComboBox comboBox  = new VWComboBox(gTable.getExternalColumnList());
	    comboBox.setEditable(true);
	    editor = new DefaultCellEditor(comboBox);
	}else if (column == gTable.m_data.COL_LINK_DATATYPE){
		VWComboBox comboBox = new VWComboBox(gTable.getLinkDataTypes());
		comboBox.setEditable(false);
		editor = new DefaultCellEditor(comboBox);
	}
    else
	    editor = defaultEditor;
	return editor.getTableCellEditorComponent(table, value, isSelected,
		row, column);
    }

    public Object getCellEditorValue() {
    	return editor.getCellEditorValue();
    }

    public boolean stopCellEditing() {
	return editor.stopCellEditing();
    }

    public void cancelCellEditing() {
	editor.cancelCellEditing();
    }

    public boolean isCellEditable(EventObject anEvent) {
    	int column = 0;
    	int row = 0;
    	if (anEvent instanceof MouseEvent) {
    		Point point = ((MouseEvent) anEvent).getPoint();
    		column = gTable.columnAtPoint(point);
    		row = gTable.rowAtPoint(point);
    	}
    	if (column <= gTable.m_data.COL_INDEXNAME)
    		return false;
    	/**
    	 * Code added for making the cell non editable other than Date datatype.
    	 * Vijaypriya.B.K 04 August 2009
    	 */
    	if (column == gTable.m_data.COL_MASK){
    		int typeValue = gTable.getRowType(row);
    		if(typeValue != Index.IDX_DATE){
    			return false;
    		}
    	}
    	return true;
    }

    public void addCellEditorListener(CellEditorListener l) {
	editor.addCellEditorListener(l);
    }

    public void removeCellEditorListener(CellEditorListener l) {
	editor.removeCellEditorListener(l);
    }

    public boolean shouldSelectCell(EventObject anEvent) {
	return editor.shouldSelectCell(anEvent);
    }
}