package ViewWise.AdminWise.VWTable;

/*
Copyright (c) 1997-2001
   Computhink Software

All rights reserved.
 */

import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;

import java.awt.Color;
import java.awt.Dimension;

import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import java.awt.Point;

import ViewWise.AdminWise.VWDocType.VWExternalDBLookup;
import ViewWise.AdminWise.VWDocType.VWIndexRec;
import ViewWise.AdminWise.VWImages.VWImages;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWRoute.IconData;

import com.computhink.common.Index;
import com.computhink.common.DocType;
//------------------------------------------------------------------------------
public class VWIndexMappingTable extends JTable implements VWConstant
{
    protected VWIndexMappingTableData m_data;
    private Vector externalColumnList = new Vector();
    final static int COL_COUNT=3;
    VWExternalDBLookup dbLookupDlg;
    public VWIndexMappingTable(VWExternalDBLookup dbLookupDlg){
	super();
	new VWImages();
	this.dbLookupDlg = dbLookupDlg;
	getTableHeader().setReorderingAllowed(false);
	m_data = new VWIndexMappingTableData(this);
	setCellSelectionEnabled(false);
	setAutoCreateColumnsFromModel(false);
	setModel(m_data);
	setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	JTextField readOnlyText=new JTextField();
	readOnlyText.setEditable(false);
	Dimension tableWith = getPreferredScrollableViewportSize();

	for (int k = 0; k < VWIndexMappingTableData.m_columns.length; k++){
	    TableCellRenderer renderer;
	    DefaultTableCellRenderer textRenderer =new VWTableCellRenderer();
	    textRenderer.setHorizontalAlignment(VWIndexMappingTableData.m_columns[k].m_alignment);
	    renderer = textRenderer;
	    VWIndexMappingRowEditor editor=new VWIndexMappingRowEditor(this);
	    TableColumn column = new TableColumn(k,Math.round(tableWith.width*
		    VWIndexMappingTableData.m_columns[k].m_width),renderer,editor);
	    addColumn(column);   
	}
	JTableHeader header = getTableHeader();
	header.setUpdateTableInRealTime(false);
	SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
	setBackground(java.awt.Color.white);
//	/setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	setRowHeight(TableRowHeight);
	setToolTipText(AdminWise.connectorManager.getString("VWIndexMappingTable.setToolTipText"));
	setTableResizable();
    }

    public void setExternalColumnList(Vector data){
	externalColumnList.removeAllElements();
	externalColumnList.addAll(data);
    }

    public Vector getExternalColumnList(){
	if (externalColumnList == null || (externalColumnList != null && externalColumnList.size() == 0)){
	    externalColumnList = new Vector();
	    externalColumnList.add("");
	}
	return externalColumnList;
    }
    
    public String[] getLinkDataTypes(){
    	return linkDataTypes;
    }

    public void setTableResizable() {
//	Resize Table
	new VWTableResizer(this);
    }
//  ------------------------------------------------------------------------------
    public void addData(Vector conds)
    {
	m_data.setData(conds);
	m_data.fireTableDataChanged();
    }
//  ------------------------------------------------------------------------------
    public void insertData(DocType docType)
    {
	if(docType.getIndices()==null) return;  

	Vector resultIndices = docType.getIndices();
	for (int i = 0 ; i < resultIndices.size(); i++){
	    Index idx = (Index)resultIndices.get(i);	
	    if (idx.getType() != Index.IDX_SEQUENCE && idx.getType() != Index.IDX_BOOLEAN)
	    {
		if (idx.getType() == Index.IDX_DATE && idx.getInfo().trim().endsWith("1")) continue;
		m_data.insert(new VWIndexMappingRowData(idx.getName(),idx.getExternalColumn(),(idx.getType() == Index.IDX_DATE)?idx.getMask():"",idx.isTriggerEnabled(),linkDataTypes[idx.getTriggerDataType()],idx.isReadOnly()));
	    }
	}

	/*for(int i=0;i<docType.getIndices().size();i++)
	    m_data.insert(new VWIndexMappingRowData((VWIndexRec)docType.getIndices().
		    get(i),"",""));*/
	m_data.fireTableDataChanged();
    }
//  ------------------------------------------------------------------------------
    public void insertData(Vector conds)
    {
	if(conds==null) return;  
	/*	for(int i=0;i<conds.size();i++)
	    m_data.insert(new AdvanceSearchRowData((SearchCond)conds.get(i)));
	 */	m_data.fireTableDataChanged();
    }
//  ------------------------------------------------------------------------------
    public void delData(int docTypeId)
    {
	m_data.delData(docTypeId);
	m_data.fireTableDataChanged();
    }
//  ------------------------------------------------------------------------------
    public void clearUserData()
    {
	int count=getRowCount();
	for(int i=0;i<count;i++)
	    m_data.setValueAt(CondsArr[0].getName(),i,2);
	m_data.fireTableDataChanged();
    }
//  ------------------------------------------------------------------------------
    public void clearData()
    {
	m_data.clear();
	m_data.fireTableDataChanged();
    }
//  ------------------------------------------------------------------------------
    public Vector getData()
    {
	return m_data.getData();
    }
    //--------------------------------------------------------------------------
    public int getRowType(int row)
    {
    	return m_data.getRowType(row);
    }
//  ------------------------------------------------------------------------------
    class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
	    Object object = event.getSource();
	    if (object instanceof JTable)
		if(event.getModifiers()==event.BUTTON3_MASK)
		    VWAdvanceSearchTable_RightMouseClicked(event);
		else if(event.getModifiers()==event.BUTTON1_MASK)
		    VWAdvanceSearchTable_LeftMouseClicked(event);
	}
    }
//  ------------------------------------------------------------------------------
    void VWAdvanceSearchTable_LeftMouseClicked(java.awt.event.MouseEvent event)
    {
	Point origin = event.getPoint();
	int row = rowAtPoint(origin);
	int column = columnAtPoint(origin);
	if (row == -1 || column == -1)
	    return; // no cell found
	if(event.getClickCount() == 1)
	    lSingleClick(event,row,column);
	else if(event.getClickCount() == 2)
	    lDoubleClick(event,row,column);
    }
//  ------------------------------------------------------------------------------
    void VWAdvanceSearchTable_RightMouseClicked(java.awt.event.MouseEvent event)
    {
	Point origin = event.getPoint();
	int row = rowAtPoint(origin);
	int column = columnAtPoint(origin);
	if (row == -1 || column == -1)
	    return; // no cell found
	if(event.getClickCount() == 1)
	    rSingleClick(event,row,column);
	else if(event.getClickCount() == 2)
	    rDoubleClick(event,row,column);
    }
//  ------------------------------------------------------------------------------
    private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//  ------------------------------------------------------------------------------
    private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
    {

    }
//  ------------------------------------------------------------------------------
    private void lSingleClick(java.awt.event.MouseEvent event,int row,int col){
    	if(isEnabled()){
    		if(col == m_data.COL_TRIGGER){    		
    			VWIndexMappingRowData rowData = (VWIndexMappingRowData)m_data.m_vector.elementAt(row);
    			if(!rowData.m_isReadOnly || (rowData.m_isReadOnly && rowData.m_type == Index.IDX_SELECTION)){
    				rowData.m_trigger = !rowData.m_trigger;    
    				repaint();
    				dbLookupDlg.setSaveEnabled(true);
    			}
    		}    		    		
    	}
    }
//  ------------------------------------------------------------------------------
    private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col){
    }
//  ------------------------------------------------------------------------------

}
class VWIndexMappingRowData implements VWConstant
{   
	public boolean m_trigger;
	public String  m_indexName;
    public int  m_indexid;
    public String  m_externalDBField;
    public String  m_indexMask;
    public int m_type;
    public String m_linkDataType;
    public boolean m_isReadOnly;
    public VWIndexMappingRowData() {
	m_indexName = "";
	m_externalDBField = "";
	m_indexMask="";
    }
//  ----------------------------------------------------------------------------
    public VWIndexMappingRowData(String indexName, String externalField, 
    		String indexMask, boolean isTrigger, String linkDataType, boolean isReadOnly) 
    {
    	m_indexName = indexName;
    	m_externalDBField = externalField;
    	m_indexMask=indexMask.equals(".")?"":indexMask;
    	m_trigger = isTrigger;
    	m_linkDataType = linkDataType;
    	m_isReadOnly = isReadOnly;
    }

    public VWIndexMappingRowData(VWIndexRec index, String externalField, 
	    String indexMask) 
    {
	m_indexName = index.getName();
	m_externalDBField = externalField;
	m_indexMask=indexMask.equals(".")?"":indexMask;
	m_type = index.getType();
	m_trigger = index.isTriggerEnabled();
	m_linkDataType = linkDataTypes[index.getTriggerDataType()];
	m_isReadOnly = index.getReadOnly();
    }
//  ----------------------------------------------------------------------------
    public VWIndexMappingRowData(VWIndexRec rowData)
    {
	try{	
	    m_indexName = rowData.getName();	
	    m_indexid = rowData.getId();
	    m_indexMask = ""; //rowData.getMask();
	    m_type = rowData.getType();
	    m_trigger = rowData.isTriggerEnabled();
	    m_linkDataType = linkDataTypes[rowData.getTriggerDataType()];
	    m_isReadOnly = rowData.getReadOnly();
	}catch(Exception ex){

	}
    }  
}
//------------------------------------------------------------------------------
class VWIndexMappingColumnData
{
    public String  m_title;
    float m_width;
    int m_alignment;

    public VWIndexMappingColumnData(String title, float width, int alignment) {
	m_title = title;
	m_width = width;
	m_alignment = alignment;
    }
}
//------------------------------------------------------------------------------
class VWIndexMappingTableData extends AbstractTableModel implements VWConstant
{
    public static final VWIndexMappingColumnData m_columns[] = {
    new VWIndexMappingColumnData( AdminWise.connectorManager.getString("VWIndexMappingTable.VWIndexMappingColumnData_1"),0.12F, JLabel.LEFT),
	new VWIndexMappingColumnData( AdminWise.connectorManager.getString("VWIndexMappingTable.VWIndexMappingColumnData_2"),0.3F, JLabel.LEFT),
	new VWIndexMappingColumnData( AdminWise.connectorManager.getString("VWIndexMappingTable.VWIndexMappingColumnData_3"),0.4F, JLabel.LEFT),	
	new VWIndexMappingColumnData( AdminWise.connectorManager.getString("VWIndexMappingTable.VWIndexMappingColumnData_4"),0.2F,JLabel.LEFT),
    new VWIndexMappingColumnData( AdminWise.connectorManager.getString("VWIndexMappingTable.VWIndexMappingColumnData_5"),0.2F,JLabel.LEFT)
    };

    public static final int COL_TRIGGER = 0;
    public static final int COL_INDEXNAME = 1;
    public static final int COL_EXTERNALDBCOL= 2;
    public static final int COL_MASK = 3;   
    public static final int COL_LINK_DATATYPE = 4;


    protected VWIndexMappingTable m_parent;
    protected Vector m_vector;

    public VWIndexMappingTableData(VWIndexMappingTable parent) {
	m_parent = parent;
	m_vector = new Vector();
    }
//  ------------------------------------------------------------------------------
    public void setData(Vector conds) {
	m_vector.removeAllElements();
	int count =conds.size();
	for(int i=0;i<count;i++){
	    VWIndexRec idx = (VWIndexRec)conds.get(i);
	    if (idx.getType() != Index.IDX_SEQUENCE && idx.getType() != Index.IDX_BOOLEAN)
	    {
		if (idx.getType() == Index.IDX_DATE && idx.getInfo().trim().endsWith("1")) continue;
		m_vector.addElement(new VWIndexMappingRowData((VWIndexRec)conds.get(i)));
	    }
	}
    }
//  ------------------------------------------------------------------------------
    public void delData(int docTypeId) {
	/*	for(int i=0;i<m_vector.size();i++)
	{
	    VWIndexMappingRowData row=(VWIndexMappingRowData)m_vector.elementAt(i);
	    if(row.m_docTypeId==docTypeId)
	    {
		m_vector.remove(i);
		i--;
	    }
	}*/
    }
//  ------------------------------------------------------------------------------
    public Vector getData() {
    	int count=getRowCount();
    	Vector indicesData =new Vector();
    	for(int i=0;i<count;i++)
    	{
    		VWIndexMappingRowData row=(VWIndexMappingRowData)m_vector.elementAt(i);
    		if (row.m_externalDBField == null) row.m_externalDBField = "";
    		Index idx = new Index(row.m_indexid);
    		idx.setName(row.m_indexName);
    		idx.setType(row.m_type);
    		idx.setExternalColumn(row.m_externalDBField);
    		if(row.m_type==Index.IDX_DATE){
    			if (row.m_indexMask == null) 
    				idx.setMask("");
    			else
    				idx.setMask(row.m_indexMask);
    		}else{
    			idx.setMask("");	    	
    		}
    		idx.setTriggerEnabled(row.m_trigger);
    		idx.setTriggerDataType(getSelectedLinkDataType(row.m_linkDataType));
    		indicesData.add(idx);
    	}
    	return indicesData;
    }
    
    private int getSelectedLinkDataType(String dataType) {
    	for(int i=0; i<linkDataTypes.length; i++){
    		if(dataType.equalsIgnoreCase(linkDataTypes[i]))
    			return i;
    	}
    	return 0;
    }
	
    public void updateCondition(int row){
    	VWIndexMappingRowData rowData =(VWIndexMappingRowData)m_vector.elementAt(row);
    	rowData.m_trigger = !rowData.m_trigger;
    }
    
	private ImageIcon getTriggerDataIcon(VWIndexMappingRowData row)
	{
		if(row.m_trigger)
			return VWImages.ApproverActionYesIcon;
		else
			return VWImages.ApproverActionNoIcon;
	}
//  ------------------------------------------------------------------------------
    public int getRowCount() {
	return m_vector==null ? 0 : m_vector.size(); 
    }
//  --------------------------------------------------------------------------
    public int getRowType(int row){
    	VWIndexMappingRowData rowObj=(VWIndexMappingRowData)m_vector.elementAt(row);
    	return rowObj.m_type;
    }
//  ------------------------------------------------------------------------------
    public int getColumnCount() { 
	return m_columns.length; 
    } 
//  ------------------------------------------------------------------------------
    public String getColumnName(int column) { 
	return m_columns[column].m_title; 
    }
//  ---------------------------------------------------------------------------------------
    public boolean isCellEditable(int nRow, int nCol) {
	return true;
    }
//  ---------------------------------------------------------------------------------------
    public Object getValueAt(int nRow, int nCol) {
	if (nRow < 0 || nRow>=getRowCount())
	    return "";
	VWIndexMappingRowData row = (VWIndexMappingRowData)m_vector.elementAt(nRow);
	switch (nCol) {
	case COL_INDEXNAME: return row.m_indexName;
	case COL_EXTERNALDBCOL: return row.m_externalDBField;
	case COL_MASK: return row.m_indexMask;	
	case COL_TRIGGER: return new IconData(getTriggerDataIcon(row) , "", Color.BLACK);
	case COL_LINK_DATATYPE: return row.m_linkDataType;
	}
	return "";
    }
//  ---------------------------------------------------------------------------------------
    public void setValueAt(Object value, int nRow, int nCol) {
	if (nRow < 0 || nRow >= getRowCount()||value==null)
	    return;
	VWIndexMappingRowData row = (VWIndexMappingRowData)m_vector.elementAt(nRow);
	String svalue = value.toString();

	switch (nCol) {
	case COL_INDEXNAME:
	    row.m_indexName = svalue; 
	    break;
	case COL_EXTERNALDBCOL:
	    row.m_externalDBField = svalue; 
	    break;
	case COL_MASK:
	    row.m_indexMask = svalue; 
	    break;
	case COL_TRIGGER:
		if(svalue.equalsIgnoreCase("true"))
			row.m_trigger = true;
		else 
			row.m_trigger = false;
		break;
	case COL_LINK_DATATYPE:
		row.m_linkDataType = svalue; 
	    break;

	}
//	------------------------------------------------------------------------------
    }
    public void insert(int row) {
	if (row < 0)
	    row = 0;
	if (row > m_vector.size())
	    row = m_vector.size();
	m_vector.insertElementAt(new VWIndexMappingRowData(), row);
    }
//  ------------------------------------------------------------------------------
    public void insert(int row,VWIndexMappingRowData IndexMappingRowData){
	if (row < 0)
	    row = 0;
	if (row > m_vector.size())
	    row = m_vector.size();
	m_vector.insertElementAt(IndexMappingRowData, row);
    }
//  ------------------------------------------------------------------------------
    public void insert(VWIndexMappingRowData rowData) {
	int count = m_vector.size();
	boolean found=false;
	for(int i=0;i<count;i++) {
	    VWIndexMappingRowData row=(VWIndexMappingRowData) m_vector.get(i);
	    if(row.m_indexName.equalsIgnoreCase(rowData.m_indexName)) {
		row.m_externalDBField = rowData.m_externalDBField;
		row.m_indexMask = rowData.m_indexMask;
		row.m_trigger = rowData.m_trigger;
		row.m_linkDataType = rowData.m_linkDataType;
		row.m_isReadOnly = rowData.m_isReadOnly;
		found=true;
		break;
	    }
	}
	if(!found)  m_vector.addElement(rowData);
    }
//  ------------------------------------------------------------------------------
    public boolean delete(int row) {
	if (row < 0 || row >= m_vector.size())
	    return false;
	m_vector.remove(row);
	return true;
    }
//  ------------------------------------------------------------------------------
    public void clear()
    {
	m_vector.removeAllElements();
    }
//  ------------------------------------------------------------------------------
}

