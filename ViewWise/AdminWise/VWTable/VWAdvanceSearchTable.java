/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWAdvanceSearchTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWTable;

import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.Border;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.DefaultComboBoxModel;
import java.awt.Dimension;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWConstant;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWUtil.VWTableResizer;
import ViewWise.AdminWise.VWUtil.VWUtil;
import com.computhink.common.SearchCond;
import com.computhink.common.Index;
import com.computhink.common.DocType;
//------------------------------------------------------------------------------
public class VWAdvanceSearchTable extends JTable implements VWConstant
{
    protected VWAdvanceSearchTableData m_data;
    protected VWCondsCombo CondsCmb=new VWCondsCombo();
    final static int COL_COUNT=9;
    public VWAdvanceSearchTable(){
        super();
        getTableHeader().setReorderingAllowed(false);
        m_data = new VWAdvanceSearchTableData(this);
        setCellSelectionEnabled(false);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JTextField readOnlyText=new JTextField();
        readOnlyText.setEditable(false);
        Dimension tableWith = getPreferredScrollableViewportSize();

        for (int k = 0; k < VWAdvanceSearchTableData.m_columns.length; k++){
            TableCellRenderer renderer;
            DefaultTableCellRenderer textRenderer =new DefaultTableCellRenderer();
            textRenderer.setHorizontalAlignment(VWAdvanceSearchTableData.m_columns[k].m_alignment);
            renderer = textRenderer;
            VWAdvanceSearchRowEditor editor=new VWAdvanceSearchRowEditor(this);
            TableColumn column = new TableColumn(k,Math.round(tableWith.width*
                VWAdvanceSearchTableData.m_columns[k].m_width),renderer,editor);
            addColumn(column);   
        }
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        SymMouse aSymMouse = new SymMouse();
	addMouseListener(aSymMouse);
        setBackground(java.awt.Color.white);
        ///setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setRowHeight(TableRowHeight);
        
        setTableResizable();
  }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
//------------------------------------------------------------------------------
  public void addData(Vector conds)
  {
        m_data.setData(conds);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(DocType docType)
  {
    if(docType.getIndices()==null) return;  
    for(int i=0;i<docType.getIndices().size();i++)
        m_data.insert(new AdvanceSearchRowData((Index)docType.getIndices().
            get(i),docType.getId(),docType.getName()));
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void insertData(Vector conds)
  {
    if(conds==null) return;  
    for(int i=0;i<conds.size();i++)
        m_data.insert(new AdvanceSearchRowData((SearchCond)conds.get(i)));
    m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void delData(int docTypeId)
  {
     m_data.delData(docTypeId);
     m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearUserData()
  {
      int count=getRowCount();
      for(int i=0;i<count;i++)
            m_data.setValueAt(CondsArr[0].getName(),i,2);
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
        m_data.clear();
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public Vector getData()
  {
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public SearchCond getAdvanceSearchRowData(int rowNum)
  {
        return m_data.getAdvanceSearchRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public int getRowType(int rowNum)
  {
        SearchCond row=m_data.getAdvanceSearchRowData(rowNum);
        return row.getIndexType();
  }
//------------------------------------------------------------------------------
  public String getRowMask(int rowNum)
  {
        SearchCond row=m_data.getAdvanceSearchRowData(rowNum);
        return row.getIndexMask();
  }
//------------------------------------------------------------------------------
  public int getRowCond(int rowNum)
  {
        SearchCond row=m_data.getAdvanceSearchRowData(rowNum);
        return row.getCond();
  }
//------------------------------------------------------------------------------
  public int getRowTypeId(int rowNum)
  {
        SearchCond row=m_data.getAdvanceSearchRowData(rowNum);
        return row.getDocTypeId();
  }
//------------------------------------------------------------------------------
  public int getRowIndexId(int rowNum)
  {
        SearchCond row=m_data.getAdvanceSearchRowData(rowNum);
        return row.getIndexId();
  }
//------------------------------------------------------------------------------
  class SymMouse extends java.awt.event.MouseAdapter{
	public void mouseClicked(java.awt.event.MouseEvent event)
	{
            Object object = event.getSource();
            if (object instanceof JTable)
                if(event.getModifiers()==event.BUTTON3_MASK)
                    VWAdvanceSearchTable_RightMouseClicked(event);
                else if(event.getModifiers()==event.BUTTON1_MASK)
                    VWAdvanceSearchTable_LeftMouseClicked(event);
	}
	}
//------------------------------------------------------------------------------
void VWAdvanceSearchTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWAdvanceSearchTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
            rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
{

}
//------------------------------------------------------------------------------
private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{

}
//------------------------------------------------------------------------------
private void lSingleClick(java.awt.event.MouseEvent event,int row,int col){
   CondsCmb.setData(getRowType(row));
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col){
 }
//------------------------------------------------------------------------------

}
class AdvanceSearchRowData
{
  public String  m_docType;
  public String  m_indexName;
  public int     m_cond;
  public String  m_value1;
  public String  m_value2;
  public int     m_docTypeId;
  public int     m_indexId;
  public int     m_indexType;
  public String  m_indexMask;
  public AdvanceSearchRowData() {
    m_docType = "";
    m_indexName = "";
    m_cond = 0;
    m_value1 = "";
    m_value1 = "";
    m_docTypeId = 0;
    m_indexId = 0;
    m_indexType=0;
    m_indexMask="";
  }
  //----------------------------------------------------------------------------
  public AdvanceSearchRowData(String docType, String indexName, int cond, 
   String value1, String value2,int docTypeId,int indexId,int indexType,
   String indexMask,int dtIndexId) 
  {
    m_docType = docType;
    m_indexName = indexName;
    m_cond = cond;
    m_value1 = value1;
    m_value1 = value2;
    m_docTypeId = docTypeId;
    m_indexId = indexId;
    m_indexType=indexType;
    m_indexMask=indexMask.equals(".")?"":indexMask;
  }
//----------------------------------------------------------------------------
  public AdvanceSearchRowData(SearchCond cond)
  {   
    m_docType=cond.getDocType();
    m_indexName=cond.getIndex();
    m_cond=cond.getCond();
    m_value1=cond.getValue1();
    m_value2=cond.getValue2();
    m_docTypeId=cond.getDocTypeId();
    m_indexId=cond.getIndexId();
    m_indexType=cond.getIndexType();
    m_indexMask=cond.getIndexMask();
  }
//----------------------------------------------------------------------------
  public AdvanceSearchRowData(Index index,int docTypeId,String docType)
  {   
    m_docType=docType;
    m_indexName=index.getName();
    m_cond=-1;
    m_value1="";
    m_value2="";
    m_docTypeId=docTypeId;
    m_indexId=index.getId();
    m_indexType=index.getType();
    m_indexMask=index.getMask();
  }  
}
//------------------------------------------------------------------------------
class AdvanceSearchColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public AdvanceSearchColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWAdvanceSearchTableData extends AbstractTableModel implements VWConstant
{
  public static final AdvanceSearchColumnData m_columns[] = {
    new AdvanceSearchColumnData( AdminWise.connectorManager.getString("VWAdvanceSearchTable.AdvanceSearchColumnData_0"),0.2F,JLabel.LEFT ),
    new AdvanceSearchColumnData( AdminWise.connectorManager.getString("VWAdvanceSearchTable.AdvanceSearchColumnData_1"),0.2F, JLabel.LEFT),
    new AdvanceSearchColumnData( AdminWise.connectorManager.getString("VWAdvanceSearchTable.AdvanceSearchColumnData_2"),0.2F, JLabel.LEFT),
    new AdvanceSearchColumnData( AdminWise.connectorManager.getString("VWAdvanceSearchTable.AdvanceSearchColumnData_3"),0.3F,JLabel.LEFT),
    new AdvanceSearchColumnData( AdminWise.connectorManager.getString("VWAdvanceSearchTable.AdvanceSearchColumnData_4"),0.3F,JLabel.LEFT)
  };

  public static final int COL_DOCTYPE = 0;
  public static final int COL_INDEXNAME = 1;
  public static final int COL_CONDS = 2;
  public static final int COL_VALUE1 = 3;
  public static final int COL_VALUE2 = 4;

  protected VWAdvanceSearchTable m_parent;
  protected Vector m_vector;

  public VWAdvanceSearchTableData(VWAdvanceSearchTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
//------------------------------------------------------------------------------
public void setData(Vector conds) {
    m_vector.removeAllElements();
    int count =conds.size();
    for(int i=0;i<count;i++)
        m_vector.addElement(new AdvanceSearchRowData((SearchCond)conds.get(i)));
  }
//------------------------------------------------------------------------------
public void delData(int docTypeId) {
    for(int i=0;i<m_vector.size();i++)
    {
        AdvanceSearchRowData row=(AdvanceSearchRowData)m_vector.elementAt(i);
        if(row.m_docTypeId==docTypeId)
        {
           m_vector.remove(i);
           i--;
        }
    }
  }
//------------------------------------------------------------------------------
public Vector getData() {
    int count=getRowCount();
    Vector conds=new Vector();
    for(int i=0;i<count;i++)
    {
        AdvanceSearchRowData row=(AdvanceSearchRowData)m_vector.elementAt(i);
        if(row.m_cond < 0 || row.m_value1.equals("") && row.m_value2.equals(""))
            continue;
        SearchCond cond=new SearchCond();
        cond.setDocType(row.m_docType);
        cond.setIndex(row.m_indexName);
        cond.setCond(row.m_cond);
        if(row.m_indexType==DATE_CONDS_INDEX)
        {        	
            cond.setValue1(VWUtil.getFormatedDate(row.m_value1,"yyyyMMdd"));
            cond.setValue2(VWUtil.getFormatedDate(row.m_value2,"yyyyMMdd"));
        }
        else
        {
            cond.setValue1(row.m_value1);
            cond.setValue2(row.m_value2);
        }
        cond.setDocTypeId(row.m_docTypeId);
        cond.setIndexId(row.m_indexId);
        cond.setIndexType(row.m_indexType);
        cond.setIndexMask(row.m_indexMask);
        conds.add(cond);
    }
    return conds;
  }
//------------------------------------------------------------------------------
public SearchCond getAdvanceSearchRowData(int rowNum) {
    AdvanceSearchRowData row=(AdvanceSearchRowData)m_vector.elementAt(rowNum);
    SearchCond cond=new SearchCond();
    cond.setDocType(row.m_docType);
    cond.setIndex(row.m_indexName);
    cond.setCond(row.m_cond);
    cond.setValue1(row.m_value1);
    cond.setValue2(row.m_value2);
    cond.setDocTypeId(row.m_docTypeId);
    cond.setIndexId(row.m_indexId);
    cond.setIndexType(row.m_indexType);
    cond.setIndexMask(row.m_indexMask);
    return cond;
  }
//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
//---------------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
    return true;
  }
//---------------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    AdvanceSearchRowData row = (AdvanceSearchRowData)m_vector.elementAt(nRow);
    switch (nCol) {
      case COL_DOCTYPE: return row.m_docType;
      case COL_INDEXNAME: return row.m_indexName;
      case COL_CONDS:
        for(int i=0;i<CondsArr.length;i++)
            if(CondsArr[i].getId()==row.m_cond)
                return CondsArr[i].getName();
      case COL_VALUE1:
            if(row.m_cond!=7)row.m_value2="";
            if(row.m_cond==-1 || row.m_cond==9 || row.m_cond==10)row.m_value1="";
            if(row.m_indexType==DATE_CONDS_INDEX)
            	//return VWUtil.getFormatedDate(row.m_value1,row.m_indexMask);
            	// Set the default mask "MM/dd/yyyy. If it is send same date format. popup calendar will work fine.
            	return VWUtil.getFormatedDate(row.m_value1,"MM/dd/yyyy");
            return row.m_value1;
      case COL_VALUE2:
          if(row.m_indexType==DATE_CONDS_INDEX)
                    //return VWUtil.getFormatedDate(row.m_value2,row.m_indexMask);
					// Set the default mask "MM/dd/yyyy. If it is send same date format. popup calendar will work fine.                    
        	  		return VWUtil.getFormatedDate(row.m_value2,"MM/dd/yyyy");
          return row.m_value2;
    }
    return "";
  }
//---------------------------------------------------------------------------------------
  public void setValueAt(Object value, int nRow, int nCol) {
    if (nRow < 0 || nRow >= getRowCount()||value==null)
      return;
    AdvanceSearchRowData row = (AdvanceSearchRowData)m_vector.elementAt(nRow);
    String svalue = value.toString();

    switch (nCol) {
      case COL_DOCTYPE: 
        row.m_docType = svalue; 
        break;
      case COL_INDEXNAME:
          row.m_indexName = svalue; 
        break;
      case COL_CONDS:
        int index =VWCondsCombo.getType();
        for (int k=0; k < CondsArr.length; k++)
          if (svalue.equals(CondsArr[k].getName())) {
            row.m_cond = CondsArr[k].getId();
            if (CondsArr[k].getId()==-1) this.fireTableDataChanged();
            break;
          }
        break;
      case COL_VALUE1:
          if(row.m_cond==8) 
            {
                /*Fix in values*/
                if(!svalue.startsWith("'")) svalue="'"+svalue;
                if(!svalue.endsWith("'")) svalue+="'";
                int count=svalue.length();
                String tmp="";
                String chr="";
                for(int i=0;i<count;i++)
                {
                    chr=svalue.substring(i,i+1);
                    if(chr.equals(",") && (!svalue.substring(i-1,i).equals("'") || 
                        !svalue.substring(i+1,i+2).equals("'")))
                    {
                        if(!svalue.substring(i-1,i).equals("'")) tmp+="'";
                        tmp+=",";
                        if(!svalue.substring(i+1,i+2).equals("'")) tmp+="'";
                    }
                    else
                    {
                        tmp+=chr;
                    }
                }
                svalue=tmp;
            }
            row.m_value1 = svalue; 
        break;
      case COL_VALUE2:
        row.m_value2 = svalue; 
        break;
    }
//------------------------------------------------------------------------------
  }
  public void insert(int row) {
    if (row < 0)
      row = 0;
    if (row > m_vector.size())
      row = m_vector.size();
    m_vector.insertElementAt(new AdvanceSearchRowData(), row);
  }
//------------------------------------------------------------------------------
public void insert(int row,AdvanceSearchRowData AdvanceSearchRowData){
    if (row < 0)
      row = 0;
    if (row > m_vector.size())
      row = m_vector.size();
    m_vector.insertElementAt(AdvanceSearchRowData, row);
  }
//------------------------------------------------------------------------------
  public void insert(AdvanceSearchRowData rowData) {
        int count = m_vector.size();
        boolean found=false;
        for(int i=0;i<count;i++) {
            AdvanceSearchRowData row=(AdvanceSearchRowData) m_vector.get(i);
            if(row.m_docTypeId==rowData.m_docTypeId &&
            row.m_indexId==rowData.m_indexId) {
                row.m_cond=rowData.m_cond;
                row.m_value1=rowData.m_value1;
                row.m_value2=rowData.m_value2;
                found=true;
                break;
            }
        }
        if(!found)  m_vector.addElement(rowData);
    }
//------------------------------------------------------------------------------
  public boolean delete(int row) {
    if (row < 0 || row >= m_vector.size())
      return false;
    m_vector.remove(row);
      return true;
  }
//------------------------------------------------------------------------------
    public void clear()
    {
        m_vector.removeAllElements();
    }
//------------------------------------------------------------------------------
}

