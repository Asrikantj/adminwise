package ViewWise.AdminWise.VWTable;

import javax.swing.ImageIcon;

class IconData
{
  public ImageIcon  m_icon;
  public Object m_data;

  public IconData(ImageIcon icon, Object data) {
    m_icon = icon;
    m_data = data;
  }
  public String toString() {
    return m_data.toString();
  }
}