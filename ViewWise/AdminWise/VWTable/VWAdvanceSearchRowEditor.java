package ViewWise.AdminWise.VWTable;
import java.util.EventObject;
import java.awt.Component;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWFind.VWFindConnector;
import ViewWise.AdminWise.VWUtil.VWDateComboBox;
import ViewWise.AdminWise.VWUtil.VWComboBox;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWDocType.VWDocTypeConnector;
import java.util.List;
import ViewWise.AdminWise.VWUtil.VWUtil;
import java.awt.Frame;
import ViewWise.AdminWise.VWMask.VWMaskField;
/**
  * each row TableCellEditor
  *
  * @version 1.0 10/20/98
  * @author <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
  */

public class VWAdvanceSearchRowEditor implements TableCellEditor,VWConstant {
  
  protected TableCellEditor editor, defaultEditor;
  protected VWAdvanceSearchTable gTable=null;
  protected JTextField textEditor=new JTextField();
  private VWMaskField gMaskEdit=null;
  /**
   * Constructs a EachRowEditor.
   * create default editor 
   *
   * @see TableCellEditor
   * @see DefaultCellEditor
   */ 
  public VWAdvanceSearchRowEditor(VWAdvanceSearchTable table) {
      textEditor.setBorder(null);
      defaultEditor = new DefaultCellEditor(textEditor);
      gTable=table;
  } 
  /**
   * @param row    table row
   * @param editor table cell editor
   */
  public Component getTableCellEditorComponent(JTable table,
      Object value, boolean isSelected, int row, int column) {

    int type = gTable.getRowType(row);
    String mask=gTable.getRowMask(row);
    int cond = gTable.getRowCond(row);
    int indexId=gTable.getRowIndexId(row);
    if (column==2)
    {
        int count=Conds[type].length;
        VWRecord[] data=new VWRecord[count];
        for(int i=0;i<count;i++) data[i]=CondsArr[Conds[type][i]];
        editor=new DefaultCellEditor(new VWComboBox(data));
    }
    else if(column==3 && type==YESNO_CONDS_INDEX)
        editor=new DefaultCellEditor(new VWComboBox(YesNoData));
    else if(column==3 && type==SELECTION_CONDS_INDEX && cond==0)
    {   
        List lstValues=VWDocTypeConnector.getSelectionValues(indexId);
        editor=new DefaultCellEditor(new VWComboBox(VWUtil.listTo1DArrOfString(lstValues)));
    }
    else if((column==3 || column==4) && type==DATE_CONDS_INDEX)
    {
     VWDateComboBox dateCombo=new VWDateComboBox();
     //dateCombo.setDateFormat(mask);
	// Set the default mask "MM/dd/yyyy.  Previouslt mask is commented. so popup calendar not displayed correctly.     
	 dateCombo.setDateFormat("MM/dd/yyyy");     
     dateCombo.setToolTipText(mask);
     dateCombo.setEditable(true);
     editor=new DefaultCellEditor(dateCombo);
    }
    else if((column==3 || column==4) && type==NUMBER_CONDS_INDEX)
    {
        gMaskEdit=new VWMaskField(mask,VWMaskField.NUMBER_TYPE,true);
        editor= new DefaultCellEditor(gMaskEdit);
        gMaskEdit.setText(gMaskEdit.getActualValue(value));
    }
    /*
    else if(cond==8)
    {
        //textEditor.setToolTipText(IN_MASK_NAME);
        new VWDlgSelectionIndex((Frame)AdminWise.adminPanel.archivePanel.
        findDlg.findPanel.docTypeIndicesDlg.getParent(),indexId); 
    }
    */
    else
    {
        if(cond==8)
        {
            textEditor.setToolTipText(LBL_IN_MASK_NAME);
        }
        else if(mask==null || mask.equals("")) 
            textEditor.setToolTipText(LBL_NO_MASK_NAME);
        else
            textEditor.setToolTipText(mask);
        editor = new DefaultCellEditor(textEditor);    
    } 
    return editor.getTableCellEditorComponent(table,
             value, isSelected, row, column);
  }
  public Object getCellEditorValue() {
    return editor.getCellEditorValue();
  }
  public boolean stopCellEditing() {
    return editor.stopCellEditing();
  }
  public void cancelCellEditing() {
    editor.cancelCellEditing();
  }
  public boolean isCellEditable(EventObject anEvent) {
    int column =0;
    int row =0;
    if (anEvent instanceof MouseEvent) {
	    Point point=((MouseEvent)anEvent).getPoint();
        column = gTable.columnAtPoint(point);
        row = gTable.rowAtPoint(point);
    }
    boolean ret=false;
    int type = gTable.getRowType(row);
    int cond = gTable.getRowCond(row);
    if(cond>=0 && column==3)
        ret=true;
    if (cond==7 && column==4)
        ret=true;
    if ((cond==9 || cond==10) && column==3)
        ret=false;
    if(column==2)
        ret=true;
    return ret ;///&& ((MouseEvent)anEvent).getClickCount() >= clickCountToStart;
  }
  public void addCellEditorListener(CellEditorListener l) {
    editor.addCellEditorListener(l);
  }
  public void removeCellEditorListener(CellEditorListener l) {
    editor.removeCellEditorListener(l);
  }
  public boolean shouldSelectCell(EventObject anEvent) {
    return editor.shouldSelectCell(anEvent);
  }
} 