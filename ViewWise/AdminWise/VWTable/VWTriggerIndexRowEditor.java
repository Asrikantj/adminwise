package ViewWise.AdminWise.VWTable;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

public class VWTriggerIndexRowEditor implements TableCellEditor{
	
	protected TableCellEditor editor, defaultEditor;
	protected VWTriggerIndexTable gTable;
	protected JTextField textField = new JTextField(); 
	
	public VWTriggerIndexRowEditor(VWTriggerIndexTable table){
		textField.setBorder(null);
		defaultEditor = new DefaultCellEditor(textField);
		gTable = table;		
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		editor = defaultEditor;
		return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

	public void addCellEditorListener(CellEditorListener l) {
		editor.addCellEditorListener(l);		
	}

	public void cancelCellEditing() {
		editor.cancelCellEditing();
	}

	public Object getCellEditorValue() {
		return editor.getCellEditorValue();
	}

	public boolean isCellEditable(EventObject anEvent) {
		int row = 0, column = 0;
		
		if(anEvent instanceof MouseEvent){
			Point point = ((MouseEvent)anEvent).getPoint();
			row = gTable.rowAtPoint(point);
			column = gTable.columnAtPoint(point);
		}
		
		if(column == gTable.m_data.COL_TINDEX_NAME)
			return false;
		else
			return true;
	}

	public void removeCellEditorListener(CellEditorListener l) {
		editor.removeCellEditorListener(l);
	}

	public boolean shouldSelectCell(EventObject anEvent) {
		return editor.shouldSelectCell(anEvent);
	}

	public boolean stopCellEditing() {
		return editor.stopCellEditing();
	}
	
}