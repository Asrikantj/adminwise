package ViewWise.AdminWise.VWTable;

import ViewWise.AdminWise.VWConstant;
import javax.swing.DefaultComboBoxModel;
import ViewWise.AdminWise.VWUtil.VWRecord;
import ViewWise.AdminWise.VWUtil.VWComboBox;

class VWCondsCombo extends VWComboBox implements VWConstant
  {
    public VWCondsCombo()
    {
        super();
        setEditable(false);
    }
    public VWCondsCombo(int type)
    {
        this();
        setData(type);
    }
    public void setData(int type)
    {
        int count=Conds[type].length;
        VWRecord[] data=new VWRecord[count];
        for(int i=0;i<count;i++)
        {
           data[i]=CondsArr[Conds[type][i]]; 
        }
        setModel(new DefaultComboBoxModel(data));
        gType=type;
    }
    public static int getType()
    {
        return gType;
    }
    
    public static int gType=0;
  }