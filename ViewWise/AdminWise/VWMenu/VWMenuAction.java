/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package ViewWise.AdminWise.VWMenu;

import java.awt.event.*;
import javax.swing.*;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import ViewWise.AdminWise.VWTree.VWTreeNode;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWUtil.VWDataStorage;
import ViewWise.AdminWise.VWPanel.VWAdminPanel;
import java.awt.Frame;
import ViewWise.AdminWise.AdminWise;

public class VWMenuAction  extends AbstractAction implements VWConstant{

/*
VWMenuAction() {
    super("Action",printIcon);
    ///putValue(Action.SHORT_DESCRIPTION,"Key");
 }
 */
VWMenuAction(String cmdName,Icon icon,int cmdId) {
    super(cmdName,icon);
    menuId=cmdId;
 }
//------------------------------------------------------------------------------
 public void actionPerformed(ActionEvent actionEvent) {
     String command=actionEvent.getActionCommand();
        
        if(menuId==5){
            VWTreeConnector.loadVWRooms();
        }
        if(menuId == 6){
        	AdminWise.printToConsole("Executing Tree");
        	try{
        	VWTreeNode node=(VWTreeNode)VWTreeConnector.gActionTreePath.getLastPathComponent();
        	AdminWise.printToConsole("node " + node.getId() + " : " + node.getRoomId());
        	VWTreeConnector.openSecuriy(node.getRoomId(), node.getId());
        	}catch(Exception ex){
        		AdminWise.printToConsole("Exception is " + ex.getMessage());	
        	}
        }
        if(menuId == 7){
            VWTreeConnector.NewNode(Folder_TYPE);
        }
        if (menuId == 8){
            VWTreeConnector.DynamicNode(5);
        }
 }
 private int menuId=0;
}
