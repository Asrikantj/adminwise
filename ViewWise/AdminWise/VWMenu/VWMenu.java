/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWConnector<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWMenu;

import javax.swing.JPopupMenu;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import ViewWise.AdminWise.VWUtil.VWStringTokenizer;
import javax.swing.Action;
import ViewWise.AdminWise.VWConstant;
import ViewWise.AdminWise.VWTree.VWTreeConnector;
import ViewWise.AdminWise.VWRoom.VWRoom;
import ViewWise.AdminWise.AdminWise;
import ViewWise.AdminWise.VWUtil.VWUtil;

public class VWMenu extends javax.swing.JPopupMenu implements VWConstant
{
    public static String Menu_Type_AllRoom="11,20,25,20,28,20,29";
    public static String Menu_Type_Room_Connect="5,0";
    public static String Menu_Type_Room_DisConnect="19,20,4,20,30";
    public static String Menu_Type_Cabinet="19,20,4,20,11";
    public static String Menu_Type_Cabinet_WC="19,20,4,20,11,20,22";
    public static String Menu_Type_Drawer="19,20,4,20,11";
    public static String Menu_Type_Drawer_WC="19,20,4,20,11,20,22";
    public static String Menu_Type_Folder="21,20,19,20,4,20,11";  
    public static String Menu_Type_Folder_WC="21,20,19,20,4,20,11,20,22";  
    public static String Menu_Type_Document="";
    public static String Menu_Type_OpenDocument="24";
    public static String Menu_Type_OpenDocument_WS="12,13,14,15,16";
    public static String Menu_Type_Viewer="17,20,18";
    public static String Menu_Type_CheckInSession="23,20,11,20,27";
    public static String Menu_Type_CheckOutSession="11,20,27";
    public static String Menu_Type_SessionFolder="21,20,19,20,4,20,11"; 
    public static String Menu_Type_RefDocument_Single="32,20,33,20,34"; 
    public static String Menu_Type_RefDocument_Multi="33,20,34"; 
    public static String Menu_Type_Security="6";
    public static String Menu_Type_DRS_ENDTask="7,8";
//------------------------------------------------------------------------------
    public VWMenu()
    {
        new JPopupMenu("Tree Actions");
        CreateMenuItems();
}
//------------------------------------------------------------------------------
    public VWMenu(String type)
    {
        this();
        AddMenus(type);
    }
//------------------------------------------------------------------------------
public VWMenu(int type)
    {
        this();
        String Type="";
        switch(type)
        {
            case Room_TYPE:
                VWRoom room = AdminWise.adminPanel.roomPanel.getSelectedRoom();
                if(room != null && room.getConnectStatus()==Room_Status_Connect)
                    Type=Menu_Type_Room_Connect;
                else
                    Type=Menu_Type_Room_DisConnect;
                break;
            case Cabinet_TYPE:
            		Type=Menu_Type_Security;
            		break;
            case Drawer_TYPE:
            		Type=Menu_Type_Security;
            		break;
            case Folder_TYPE:
            		Type=Menu_Type_DRS_ENDTask;
            		break;
        }
        AddMenus(Type);
    }
//------------------------------------------------------------------------------    
    private void CreateMenuItems()
    {
        int MenuCount = MenuNames.length;
        for (int i=0;i<MenuCount;i++)
        {
            VWMenuAction menuItem=null;
            if(!MenuNames[i].equals("")){
                menuItem=new VWMenuAction(MenuNames[i],null,i);
            }
            menuItems[i]=menuItem;
        }
    }
//------------------------------------------------------------------------------
    private void AddMenus(String strType)
    {
        VWStringTokenizer tokens = new VWStringTokenizer(strType,",",false);
        while (tokens.hasMoreTokens())
        {
            int menuItem=VWUtil.to_Number(tokens.nextToken());
            if(MenuNames[menuItem].equals(""))
            {
                addSeparator();
            }
            else
            {
                add(menuItems[menuItem]);
            }
        }
    }
//------------------------------------------------------------------------------
    public static String getAppId(String appName)
    {
        for(int i= 0;i<appNames.length;i++)
            if(appNames[i].equals(appName)) return String.valueOf(i+1);
        return "0";
    }
//------------------------------------------------------------------------------
    private VWMenuAction[] menuItems=new VWMenuAction[MenuNames.length];
    private static String[] appNames=null;
}