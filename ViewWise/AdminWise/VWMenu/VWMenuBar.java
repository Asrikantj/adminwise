/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWConnector<br>
 *
 * @version     $Revision: 1.4 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/

package ViewWise.AdminWise.VWMenu;

import javax.swing.JMenuBar;
import javax.swing.JMenu;

import ViewWise.AdminWise.AdminWise;
import com.computhink.resource.ResourceManager;
import com.computhink.common.Constants;
public class VWMenuBar extends JMenuBar implements Constants{
VWMenuBar()
{
    new JMenuBar();
	add(new JMenu(AdminWise.connectorManager.getString("VWMenuBar.menu_1")));
	add(new JMenu(AdminWise.connectorManager.getString("VWMenuBar.menu_2")));
	
    JMenu toolsMenu = new JMenu(AdminWise.connectorManager.getString("VWMenuBar.menu_3"));
	toolsMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_4"));
	toolsMenu.addSeparator();
	JMenu adminMenu = new JMenu(AdminWise.connectorManager.getString("VWMenuBar.menu_5"));
	adminMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_6"));
	adminMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_7"));
	adminMenu.addSeparator();
	adminMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_8"));
	adminMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_9"));
	toolsMenu.add(adminMenu);
	toolsMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_10"));
	add(toolsMenu);

	JMenu helpMenu = new JMenu(AdminWise.connectorManager.getString("VWMenuBar.menu_11"));
	helpMenu.add(AdminWise.connectorManager.getString("VWMenuBar.help_1")+ ResourceManager.getDefaultManager().getString("CVProduct.Name") + AdminWise.connectorManager.getString("VWMenuBar.help_2"));
	helpMenu.addSeparator();
	helpMenu.add(AdminWise.connectorManager.getString("VWMenuBar.menu_12"));
	add(helpMenu);
}
	//{{DECLARE_CONTROLS
	//}}
}